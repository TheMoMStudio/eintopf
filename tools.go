//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

//go:build tools
// +build tools

package eintopf

import (
	_ "github.com/cortesi/modd/cmd/modd"
	_ "github.com/go-swagger/go-swagger/cmd/swagger"
)
