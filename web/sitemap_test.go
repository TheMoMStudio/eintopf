// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"encoding/xml"
	"strings"
	"testing"
	"time"

	"eintopf.info/service/event"
	"eintopf.info/service/group"
	"eintopf.info/service/place"
)

func TestSitemap(t *testing.T) {
	tests := []routeTest{
		{
			name: "sitemap listing of published event",
			tz:   time.UTC,
			uri:  "/sitemap.xml",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
				},
			},
			status: 200,
			contentTestFunc: func(t *testing.T, content string) {
				content = strings.TrimSpace(content)
				if !assertSitemapEntry(t, content, "/event/0") {
					t.Error("/event/0 should belisted")
				}
			},
		},
		{
			name: "sitemap listing of unpublished event",
			tz:   time.UTC,
			uri:  "/sitemap.xml",
			events: []*event.NewEvent{
				{
					Published: false,
					Name:      "foo",
				},
			},
			status: 200,
			contentTestFunc: func(t *testing.T, content string) {
				content = strings.TrimSpace(content)
				if assertSitemapEntry(t, content, "/event/0") {
					t.Error("/event/0 should not belisted")
				}
			},
		},
		{
			name: "sitemap listing of published group",
			tz:   time.UTC,
			uri:  "/sitemap.xml",
			groups: []*group.NewGroup{
				{
					Published: true,
					Name:      "Foo",
				},
			},
			status: 200,
			contentTestFunc: func(t *testing.T, content string) {
				content = strings.TrimSpace(content)
				if !assertSitemapEntry(t, content, "/gruppe/0") {
					t.Error("/gruppe/0 should belisted")
				}
			},
		},
		{
			name: "sitemap listing of unpublished group",
			tz:   time.UTC,
			uri:  "/sitemap.xml",
			groups: []*group.NewGroup{
				{
					Published: false,
					Name:      "Foo",
				},
			},
			status: 200,
			contentTestFunc: func(t *testing.T, content string) {
				content = strings.TrimSpace(content)
				if assertSitemapEntry(t, content, "/gruppe/0") {
					t.Error("/gruppe/0 should not belisted")
				}
			},
		},
		{
			name: "sitemap listing of published place",
			tz:   time.UTC,
			uri:  "/sitemap.xml",
			places: []*place.NewPlace{
				{
					Published: true,
					Name:      "Foo",
				},
			},
			status: 200,
			contentTestFunc: func(t *testing.T, content string) {
				content = strings.TrimSpace(content)
				if !assertSitemapEntry(t, content, "/ort/0") {
					t.Error("/ort/0 should belisted")
				}
			},
		},
		{
			name: "sitemap listing of unpublished place",
			tz:   time.UTC,
			uri:  "/sitemap.xml",
			places: []*place.NewPlace{
				{
					Published: false,
					Name:      "Foo",
				},
			},
			status: 200,
			contentTestFunc: func(t *testing.T, content string) {
				content = strings.TrimSpace(content)
				if assertSitemapEntry(t, content, "/ort/0") {
					t.Error("/ort/0 should not belisted")
				}
			},
		},
		{
			name: "count sitemap listings",
			tz:   time.UTC,
			uri:  "/sitemap.xml",
			places: []*place.NewPlace{
				{
					Published: false,
					Name:      "Foo",
				},
				{
					Published: true,
					Name:      "Bar",
				},
			},
			groups: []*group.NewGroup{
				{
					Published: true,
					Name:      "Foo",
				},
				{
					Published: true,
					Name:      "Bar",
				},
			},
			events: []*event.NewEvent{
				{
					Published: false,
					Name:      "Foo",
				},
				{
					Published: false,
					Name:      "Bar",
				},
			},
			status: 200,
			contentTestFunc: func(t *testing.T, content string) {
				content = strings.TrimSpace(content)
				var xmlData UrlSet
				xml.Unmarshal([]byte(content), &xmlData)
				if (len(xmlData.Url) - len(staticRoutes)) != 3 {
					t.Errorf("content should be '3', got '%d'", len(xmlData.Url))
					return
				}
			},
		},
	}

	testRoutes(t, tests)
}

func assertSitemapEntry(t *testing.T, content string, needle string) bool {
	t.Helper()
	var xmlData UrlSet
	xml.Unmarshal([]byte(content), &xmlData)
	foundNeedle := false
	for _, url := range xmlData.Url {
		if strings.Contains(url.Loc, needle) {
			foundNeedle = true
		}
	}
	return foundNeedle
}
