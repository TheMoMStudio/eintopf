//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"testing"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/google/go-cmp/cmp"
	"github.com/iand/microdata"

	"eintopf.info/service/event"
	"eintopf.info/service/place"
)

func TestPlacePage(t *testing.T) {
	tests := []routeTest{
		{
			name: "linkDisplay",
			tz:   time.UTC,
			uri:  "/ort/0",
			places: []*place.NewPlace{
				{
					Published: true,
					Name:      "foo",
					Description: `EVENTBESCHREIBUNG https://www.staatsgalerie.de/de\n adsadsad
					sdadssadl dasd https://eintopf.info/ d sadad sad d,
					sd https://www.diesisteinmegalangerteststringdersolangistdasserabgekuerztwerdenmusssadasdsaadsasdsadasdasdasdasdsaddddda.de
					sadasdasd dddsadsd
									s`,
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				expectedRef := []string{"https://www.staatsgalerie.de/de", "https://eintopf.info/",
					"https://www.diesisteinmegalangerteststringdersolangistdasserabgekuerztwerdenmusssadasdsaadsasdsadasdasdasdasdsaddddda.de"}
				expectedLinkText := []string{"https://www.staatsgalerie.de/de", "https://eintopf.info/",
					"https://www.diesisteinmegalangerteststringdersolangistdasserabgekuerztwerdenmusssadasdsaadsasdsadasd..."}
				assertLinkInDesc(t, doc, expectedRef, expectedLinkText)
			},
		},
		{
			name: "NotFound",
			tz:   time.UTC,
			uri:  "/ort/12345",
			places: []*place.NewPlace{
				{
					Published: true,
					Name:      "foo",
				},
			},
			status: 404,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				if diff := cmp.Diff("404 Ort wurde nicht gefunden", doc.Find(".error").Text()); diff != "" {
					t.Errorf(".error mismatch (-want +got):\n%s", diff)
				}
			},
		},
		{
			name: "Works",
			tz:   time.UTC,
			uri:  "/ort/0",
			places: []*place.NewPlace{
				{
					Published: true,
					Name:      "foo",
					Lat:       1,
					Lng:       2,
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				if diff := cmp.Diff(2, len(doc.Find("h1").Nodes)); diff != "" {
					t.Errorf("h1 mismatch (-want +got):\n%s", diff)
				}
				if diff := cmp.Diff("foofoo", doc.Find("h1").Text()); diff != "" {
					t.Errorf("h1 text mismatch (-want +got):\n%s", diff)
				}

				assertMap(t, doc)
			},
		},
		{
			name: "NotPublished",
			tz:   time.UTC,
			uri:  "/ort/0",
			places: []*place.NewPlace{
				{
					Published: false,
					Name:      "foo",
				},
			},
			status: 200,
		},
		{
			name: "MetaNoIndexNotPublished",
			tz:   time.UTC,
			uri:  "/ort/0",
			places: []*place.NewPlace{
				{
					Published: false,
					Name:      "foo",
				},
			},
			status: 200,
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				assertRobotsIndex(t, doc, "noindex, nofollow")
			},
		},
		{
			name: "MetaIndexPublished",
			tz:   time.UTC,
			uri:  "/ort/0",
			places: []*place.NewPlace{
				{
					Published: true,
					Name:      "foo",
				},
			},
			status: 200,
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				assertRobotsIndex(t, doc, "index, follow")
			},
		},
		{
			name:      "MetaDefault",
			tz:        time.UTC,
			uri:       "/ort/0",
			theme:     "meta",
			themePath: "testdata/themes/",
			places: []*place.NewPlace{
				{
					Published: true,
					Name:      "Foo",
				},
			},
			status: 200,
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				assertRobotsIndex(t, doc, "index, follow")
				assertMeta(t, doc, MetaTags{
					Title:       "Foo | Custom Sitename",
					Description: "Custom Meta Description",
					Image:       "/assets/images/logo.png",
				})
			},
		},
		{
			name: "MetaContent",
			tz:   time.UTC,
			uri:  "/ort/0",
			places: []*place.NewPlace{
				{
					Published:   true,
					Name:        "Foo",
					Description: "Foo bar",
					Image:       "Foobar.png",
				},
			},
			status: 200,
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				assertRobotsIndex(t, doc, "index, follow")
				assertMeta(t, doc, MetaTags{
					Title:       "Foo | Eintopf Stuttgart",
					Description: "Foo bar",
					Image:       "/api/v1/media/Foobar.png",
				})
			},
		},
		{
			name: "WithoutMap NoLatLng",
			tz:   time.UTC,
			uri:  "/ort/0",
			places: []*place.NewPlace{
				{
					Published: true,
					Name:      "bar",
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertNoMap(t, doc)
			},
		},
		{
			name: "PastEvents",
			uri:  "/ort/0",
			places: []*place.NewPlace{
				{
					Published: true,
					Name:      "place1",
				},
				{
					Published: true,
					Name:      "place2",
				},
			},
			events: []*event.NewEvent{
				// Included
				{
					Published: true,
					Name:      "past",
					Location:  "id:0",
					Start:     time.Now().Add(-1 * time.Hour * 24),
				},
				{
					Published: true,
					Name:      "multiday",
					Location:  "id:0",
					Start:     time.Now().Add(-5 * time.Hour * 24),
					End:       tptr(time.Now().Add(-2 * time.Hour * 24)),
				},
				// Not included
				{
					Published: true,
					Name:      "another location",
					Location:  "id:1",
					Start:     time.Now().Add(-1 * time.Hour * 24),
				},
				{
					Published: true,
					Name:      "future",
					Location:  "id:0",
					Start:     time.Now().Add(time.Hour * 24),
				},
				{
					Published: false,
					Name:      "not published",
					Location:  "id:0",
					Start:     time.Now().Add(-1 * time.Hour * 24),
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertPastEvents(t, doc, []string{"past", "multiday"})
			},
		},
		{
			name: "FutureEvents",
			uri:  "/ort/0",
			places: []*place.NewPlace{
				{
					Published: true,
					Name:      "place1",
				},
				{
					Published: true,
					Name:      "place2",
				},
			},
			events: []*event.NewEvent{
				// Included
				{
					Published: true,
					Name:      "future",
					Location:  "id:0",
					Start:     time.Now().Add(time.Hour * 24),
				},
				{
					Published: true,
					Name:      "multiday",
					Location:  "id:0",
					Start:     time.Now().Add(time.Hour * 24),
					End:       tptr(time.Now().Add(time.Hour * 24 * 5)),
				},
				// Not included
				{
					Published: true,
					Name:      "another location",
					Location:  "id:1",
					Start:     time.Now().Add(time.Hour * 24),
				},
				{
					Published: true,
					Name:      "past",
					Location:  "id:0",
					Start:     time.Now().Add(-2 * time.Hour * 24),
				},
				{
					Published: false,
					Name:      "not published",
					Location:  "id:0",
					Start:     time.Now().Add(time.Hour * 24),
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertFutureEvents(t, doc, []string{"future", "multiday"})
			},
		},
		{
			name: "Schema.org",
			tz:   time.UTC,
			uri:  "/ort/0",
			places: []*place.NewPlace{
				{
					Published:   true,
					Name:        "Foo",
					Description: "Foo bar",
					Address:     "nice address",
					Lat:         1.312,
					Lng:         13.12,
				},
			},
			status: 200,
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				coordinatesMicrodata := microdata.NewItem()
				coordinatesMicrodata.AddType("https://schema.org/GeoCoordinates")
				coordinatesMicrodata.AddString("latitude", "1.312")
				coordinatesMicrodata.AddString("longitude", "13.12")

				placeMicrodata := microdata.NewItem()
				placeMicrodata.AddType("https://schema.org/Place")
				placeMicrodata.AddString("name", "Foo")
				placeMicrodata.AddString("description", "Foo bar")
				placeMicrodata.AddString("address", "nice address")
				placeMicrodata.AddItem("geo", coordinatesMicrodata)

				wantMicrodata := microdata.NewMicrodata()
				wantMicrodata.AddItem(placeMicrodata)

				assertMicroData(tt, doc, wantMicrodata)
			},
		},
	}

	testRoutes(t, tests)
}
