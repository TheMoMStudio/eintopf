//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"testing"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/google/go-cmp/cmp"
	"github.com/iand/microdata"

	"eintopf.info/service/place"
)

func TestPlaceListPage(t *testing.T) {
	tests := []routeTest{
		{
			name: "Works",
			tz:   time.UTC,
			uri:  "/orte",
			places: []*place.NewPlace{
				{
					Published: true,
					Name:      "foo",
				},
				{
					Published: true,
					Name:      "Bar",
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				if diff := cmp.Diff(2, len(doc.Find("h2").Nodes)); diff != "" {
					t.Errorf("h2 mismatch (-want +got):\n%s", diff)
				}
				if diff := cmp.Diff("Barfoo", doc.Find("h2").Text()); diff != "" {
					t.Errorf("h2 text mismatch (-want +got):\n%s", diff)
				}
			},
		},
		{
			name: "Schema.org",
			tz:   time.UTC,
			uri:  "/orte",
			places: []*place.NewPlace{
				{
					Published:   true,
					Name:        "Foo",
					Description: "Foo bar",
					Address:     "nice address",
					Lat:         1.312,
					Lng:         13.12,
				},
			},
			status: 200,
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				placeMicrodata := microdata.NewItem()
				placeMicrodata.AddType("https://schema.org/Place")
				placeMicrodata.AddString("name", "Foo")
				placeMicrodata.AddString("url", "http://localhost/ort/0")

				wantMicrodata := microdata.NewMicrodata()
				wantMicrodata.AddItem(placeMicrodata)

				assertMicroData(tt, doc, wantMicrodata)
			},
		},
	}

	testRoutes(t, tests)
}
