// Copyright (C) 2024 The Eintopf authors
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/xerror"
	"eintopf.info/internal/xtime"
)

func (renderer *Renderer) TopicPage(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	if id == "" {
		renderer.errorPage(w, r, xerror.NotFoundError{Err: fmt.Errorf("topic not found: empty id"), Type: "topic"})
		return
	}

	topic, err := renderer.topicStore.FindByID(r.Context(), id)
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}
	if topic == nil {
		renderer.errorPage(w, r, xerror.NotFoundError{Err: fmt.Errorf("topic not found"), Type: "topic"})
		return
	}

	options := eventListOptionsFromRequest(r, renderer.tz)
	options = options.SetTopic(topic.Name)
	pages, err := renderer.getEventListPagination(r.Context(), xtime.Date(time.Now()), options)
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}
	options = options.setPages(pages)

	result, err := renderer.eventSearch.Search(r.Context(), options.eventSearchOptions(false))
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}
	err = renderer.renderPage(w, r, "topic", map[string]interface{}{
		"SearchAction": options.URL(),

		"Options": options,

		"Today": time.Now(),

		"Days":  groupEventsByDays(result.Events),
		"Topic": topic,
	}, nil)
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}
}
