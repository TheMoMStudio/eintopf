//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"

	stdHtml "html"

	"html/template"
	htmltemplate "html/template"
	"io"
	"io/fs"
	"log"
	"net/http"
	"net/url"
	"path"
	"regexp"
	"strings"
	"time"

	"eintopf.info"
	"eintopf.info/internal/xerror"
	"eintopf.info/service/eventsearch"
	"eintopf.info/service/search"
	"eintopf.info/service/taxonomy"

	"github.com/goodsign/monday"
	"github.com/microcosm-cc/bluemonday"
	"github.com/yuin/goldmark"
	meta "github.com/yuin/goldmark-meta"
	"github.com/yuin/goldmark/parser"
	"github.com/yuin/goldmark/renderer/html"
)

/* variables are set from make environment with information from git */
var (
	vNumber     string = "unknown version"
	vCommitHash string = "n/a"
)

type Renderer struct {
	search        search.Service
	eventSearch   eventsearch.Service
	categoryStore taxonomy.CategoryStorer
	topicStore    taxonomy.TopicStorer

	timestamp time.Time // Time of server start. Used to cache assets.
	baseURL   string
	tz        *time.Location

	theme eintopf.Theme
	data  map[string]any
	funcs htmltemplate.FuncMap
}

type MetaTags struct {
	Title        string
	Description  string
	Image        string
	NotPublished bool
}

func NewRenderer(search search.Service, eventSearch eventsearch.Service, categoryStore taxonomy.CategoryStorer, topicStore taxonomy.TopicStorer, theme eintopf.Theme, baseURL string, tz *time.Location) *Renderer {
	markdown := goldmark.New(
		goldmark.WithRendererOptions(
			html.WithUnsafe(),
		),
		goldmark.WithExtensions(
			meta.Meta,
		),
	)

	data := map[string]any{
		"BaseURL":       baseURL,
		"VersionNumber": vNumber,
		"CommitHash":    vCommitHash,
	}

	funcs := htmltemplate.FuncMap{
		"formatDate": func(t time.Time, format string) string {
			return monday.Format(t, format, monday.LocaleDeDE)
		},
		"formatInterval": formatInterval,
		"sameDay":        sameDay,
		"isToday": func(d time.Time) bool {
			return sameDay(d, time.Now())
		},
		"isTomorrow": func(d time.Time) bool {
			return sameDay(d, time.Now().AddDate(0, 0, 1))
		},
		"joinPath": func(base string, strings ...string) string {
			path, _ := url.JoinPath(base, strings...)
			return path
		},
		"dict": func(values ...interface{}) (map[string]interface{}, error) {
			if len(values)%2 != 0 {
				return nil, fmt.Errorf("invalid dict call")
			}
			dict := make(map[string]interface{}, len(values)/2)
			for i := 0; i < len(values); i += 2 {
				key, ok := values[i].(string)
				if !ok {
					return nil, fmt.Errorf("dict keys must be strings")
				}
				dict[key] = values[i+1]
			}
			return dict, nil
		},
		"split": strings.Split,
		"minus": func(a, b int) int {
			return a - b
		},
		"attify": func(str string) string {
			return strings.ReplaceAll(str, "@", " [ at ] ")
		},
		"placesToMapMarkers":       placesToMapMarkers,
		"placeDocumentToMapMarker": placeDocumentToMapMarker,
		"placeToMapMarker":         placeToMapMarker,
		"eventToMapMarker":         eventToMapMarker,
		"getEventCoordinates":      getEventCoordinates,
		"json": func(a any) string {
			data, _ := json.Marshal(a)
			return string(data)
		},
		"markdown": func(filePath string) htmltemplate.HTML {
			markdownContent, err := GetMarkdownFileContent(theme.FS(), filePath)
			if err != nil {
				return htmltemplate.HTML("Could not read markdown file path!")
			}
			var buf bytes.Buffer
			if err := markdown.Convert(markdownContent, &buf); err != nil {
				log.Printf("markdown: %s", err)
				return ""
			}

			//TODO discuss this
			markdownSani := bluemonday.UGCPolicy()
			markdownSani.AllowElements("button")
			markdownSani.AllowAttrs("class").OnElements("button", "img", "div")

			return htmltemplate.HTML(markdownSani.Sanitize(buf.String()))
		},
		"markdownMeta": func(filePath string, metaKey string) string {
			markdownContent, err := GetMarkdownFileContent(theme.FS(), filePath)
			if err != nil {
				return "Could not read markdown file path!"
			}
			var buf bytes.Buffer
			context := parser.NewContext()
			if err := markdown.Convert(markdownContent, &buf, parser.WithContext(context)); err != nil {
				log.Printf("markdownMeta: %s", err)
				return ""
			}
			metaData := meta.Get(context)
			title := metaData[metaKey]
			if title == nil {
				return ""
			}

			return bluemonday.StrictPolicy().Sanitize(title.(string))
		},
		"lastModified": func() int64 {
			return theme.LastModified().UnixMilli()
		},
	}

	return &Renderer{
		search:        search,
		eventSearch:   eventSearch,
		categoryStore: categoryStore,
		topicStore:    topicStore,
		baseURL:       baseURL,
		tz:            tz,
		timestamp:     time.Now(),
		theme:         theme,
		data:          data,
		funcs:         funcs,
	}
}

func (renderer *Renderer) Render(ctx context.Context, w io.Writer, templates []string, data map[string]any, funcs htmltemplate.FuncMap, includeHelper bool) error {
	if funcs == nil {
		funcs = make(htmltemplate.FuncMap)
	}
	for k, v := range renderer.funcs {
		funcs[k] = v
	}

	if data == nil {
		data = make(map[string]any)
	}
	for k, v := range renderer.data {
		data[k] = v
	}

	return renderer.theme.Render(
		ctx,
		w,
		templates,
		data,
		funcs,
		includeHelper,
	)
}

func (renderer *Renderer) LastModified() time.Time {
	return renderer.timestamp
}

func GetMarkdownFileContent(themeFS fs.FS, filePath string) ([]byte, error) {
	f, err := themeFS.Open(path.Join("content", filePath+".md"))
	if err != nil {
		return nil, err
	}
	defer f.Close()

	data, err := io.ReadAll(f)
	if err != nil {
		return nil, err
	}
	if len(data) == 0 {
		return nil, fmt.Errorf("empty file: %s", path.Join("content", filePath+".md"))
	}
	return data, err
}

// renderAsset renders the given page as asset to the given io.Writer.
func (renderer *Renderer) renderAsset(w io.Writer, r *http.Request, asset string, data map[string]any, funcs htmltemplate.FuncMap) error {
	if data == nil {
		data = make(map[string]any)
	}
	return renderer.Render(r.Context(), w, []string{asset}, data, funcs, true)
}

// renderPage renders the given page as html to the given io.Writer.
func (renderer *Renderer) renderPage(w io.Writer, r *http.Request, page string, data map[string]any, funcs htmltemplate.FuncMap) error {
	if data == nil {
		data = make(map[string]any)
	}

	data["PageClass"] = fmt.Sprintf("%spage", page)
	data["URI"] = r.URL.Path

	if data["Meta"] == nil {
		data["Meta"] = MetaTags{}
	}

	if funcs == nil {
		funcs = make(htmltemplate.FuncMap)
	}
	funcs["linkify"] = linkify

	return renderer.Render(r.Context(), w, []string{"page", "globals", page}, data, funcs, true)
}

func (renderer *Renderer) errorPage(w http.ResponseWriter, r *http.Request, err error) {
	log.Printf("requestID=%s error=%s", r.Context().Value("requestID"), err)

	code := 500
	msg := "500 Etwas ist schief gelaufen"
	if xerror.IsNotFoundError(err) {
		code = 404
		msg = "404 Seite nicht gefunden"
		notFoundError := err.(xerror.NotFoundError)
		if notFoundError.Type == "event" {
			msg = "404 Veranstaltung wurde nicht gefunden"
		}
		if notFoundError.Type == "group" {
			msg = "404 Gruppe wurde nicht gefunden"
		}
		if notFoundError.Type == "place" {
			msg = "404 Ort wurde nicht gefunden"
		}
		if notFoundError.Type == "category" {
			msg = "404 Kategorie wurde nicht gefunden"
		}
		if notFoundError.Type == "topic" {
			msg = "404 Thema wurde nicht gefunden"
		}
	}

	// Don't cache error pages.
	w.Header().Del("Last-Modified")
	w.Header().Del("Cache-Control")

	w.WriteHeader(code)
	err2 := renderer.renderPage(w, r, "error", map[string]any{
		"Code": code,
		"Msg":  msg,
	}, nil)
	if err2 != nil {
		log.Printf("error page: %s, original error: %s", err2, err)
	}
}

func (renderer *Renderer) errorPartial(w http.ResponseWriter, r *http.Request, err error, msg string) {
	log.Printf("requestID=%s error=%s", r.Context().Value("requestID"), err)

	code := 500

	if xerror.IsNotFoundError(err) {
		code = 404
	}
	if msg == "" {
		msg = fmt.Sprintf("%d Etwas ist schief gelaufen", code)
	}

	// Don't cache error pages.
	w.Header().Del("Last-Modified")
	w.Header().Del("Cache-Control")

	w.WriteHeader(code)
	err2 := renderer.theme.Render(r.Context(), w, []string{"partial/error"}, map[string]any{
		"Msg": msg,
	}, nil, true)
	if err2 != nil {
		log.Printf("error on error template: %s", err)
	}
}

func (renderer *Renderer) asset(w http.ResponseWriter, r *http.Request) {
	// Remove /assets prefix, since the asset file server is already in that
	// directory.
	r.URL.Path = strings.TrimPrefix(r.URL.Path, "/assets")

	http.FileServer(http.FS(renderer.theme.AssetFS())).ServeHTTP(w, r)
}

func linkify(input string) template.HTML {
	maxLinkLength := 100
	urlRegex := regexp.MustCompile(`(http|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])`)
	return template.HTML(urlRegex.ReplaceAllStringFunc(input, func(url string) string {
		safeURL := stdHtml.EscapeString(url)
		linkText := safeURL
		if len(safeURL) > maxLinkLength {
			linkText = safeURL[:maxLinkLength] + "..."
		}
		return `<a href="` + safeURL + `" target="_blank">` + linkText + `</a>`
	}))
}
