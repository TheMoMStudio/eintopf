//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"html/template"
	"net/http"
	"sort"
	"strings"

	"eintopf.info/service/indexo"
	"eintopf.info/service/search"
)

// GroupListPage renders the grouplist page.
func (renderer *Renderer) GroupListPage(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query().Get("query")
	result, err := renderer.search.Search(r.Context(), &search.Options{
		Query: query,
		Sort:  "name",
		Filters: []search.Filter{
			&search.TermsFilter{Field: "type", Terms: []string{"group"}},
			&search.BoolFilter{Field: "listed", Value: true},
		},
	})
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}

	alphabet := defaultAlphabet()
	groups := []GroupDocument{}
	for _, hit := range result.Hits {
		g := GroupDocument{}
		err := hit.Unmarshal(&g)
		if err != nil {
			renderer.errorPage(w, r, err)
			return
		}
		groups = append(groups, g)

		for _, letter := range alphabet {
			if []rune(letter.Letter)[0] == g.StartLetterRune() {
				letter.HasItems = true
				break
			}
		}
	}
	sort.Slice(groups, func(i, j int) bool {
		return strings.ToLower(groups[i].Name) < strings.ToLower(groups[j].Name)
	})

	currentLetter := rune('A')

	err = renderer.renderPage(w, r, "grouplist", map[string]any{
		"Query":    query,
		"Groups":   groups,
		"Alphabet": alphabet,
	}, template.FuncMap{
		"isCurrentLetter": func(letter rune) bool {
			if currentLetter <= letter {
				currentLetter = currentLetter + letter - currentLetter + 1
				return true
			}
			return false
		},
	})
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}
}

type GroupDocument indexo.GroupDocument

func (g GroupDocument) StartLetter() string {
	return strings.ToUpper(string([]rune(g.Name)[0]))
}

func (g GroupDocument) StartLetterRune() rune {
	return []rune(g.StartLetter())[0]
}

type Letter struct {
	Letter   string
	HasItems bool
}

func defaultAlphabet() []*Letter {
	return []*Letter{
		{Letter: "A"},
		{Letter: "B"},
		{Letter: "C"},
		{Letter: "D"},
		{Letter: "E"},
		{Letter: "F"},
		{Letter: "G"},
		{Letter: "H"},
		{Letter: "I"},
		{Letter: "J"},
		{Letter: "K"},
		{Letter: "L"},
		{Letter: "M"},
		{Letter: "N"},
		{Letter: "O"},
		{Letter: "P"},
		{Letter: "Q"},
		{Letter: "R"},
		{Letter: "S"},
		{Letter: "T"},
		{Letter: "U"},
		{Letter: "V"},
		{Letter: "W"},
		{Letter: "X"},
		{Letter: "Y"},
		{Letter: "Z"},
	}
}
