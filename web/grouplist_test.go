//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"testing"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/google/go-cmp/cmp"
	"github.com/iand/microdata"

	"eintopf.info/service/group"
)

func TestGroupListPage(t *testing.T) {
	tests := []routeTest{
		{
			name: "Works",
			tz:   time.UTC,
			uri:  "/gruppen",
			groups: []*group.NewGroup{
				{
					Published: true,
					Name:      "foo",
				},
				{
					Published: true,
					Name:      "Bar",
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				if diff := cmp.Diff(2, len(doc.Find("h2").Nodes)); diff != "" {
					t.Errorf("h2 mismatch (-want +got):\n%s", diff)
				}
				if diff := cmp.Diff("Barfoo", doc.Find("h2").Text()); diff != "" {
					t.Errorf("h2 text mismatch (-want +got):\n%s", diff)
				}
			},
		},
		{
			name: "Schema.org",
			tz:   time.UTC,
			uri:  "/gruppen",
			groups: []*group.NewGroup{
				{
					Published:   true,
					Name:        "Foo",
					Description: "Foo bar",
				},
			},
			status: 200,
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				groupMicrodata := microdata.NewItem()
				groupMicrodata.AddType("https://schema.org/Organization")
				groupMicrodata.AddString("name", "Foo")
				groupMicrodata.AddString("url", "http://localhost/gruppe/0")

				wantMicrodata := microdata.NewMicrodata()
				wantMicrodata.AddItem(groupMicrodata)

				assertMicroData(tt, doc, wantMicrodata)
			},
		},
	}

	testRoutes(t, tests)
}
