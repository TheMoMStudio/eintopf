// Copyright (C) 2024 The Eintopf authors
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"testing"
	"time"

	"eintopf.info/service/event"
	"eintopf.info/service/taxonomy"
	"github.com/PuerkitoBio/goquery"
)

func TestTopic(t *testing.T) {
	tests := []routeTest{
		{
			name:   "NotFound",
			uri:    "/kategorie/bar",
			status: 404,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertNodes(t, doc, ".error", []string{"404 Kategorie wurde nicht gefunden"})
			},
		},
		{
			name: "No Events",
			uri:  "/thema/0",
			topics: []*taxonomy.NewTopic{
				{
					Name:        "foo",
					Headline:    "Foo!",
					Description: "fo fo fo",
				},
			},
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "nice event",
					Start:     time.Now().AddDate(1, 0, 0),
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertNodes(t, doc, "h1", []string{"Foo!"})
				assertNodes(t, doc, "h3 span[itemprop='name']", []string{})
			},
		},
		{
			name: "Finds Topic With Events",
			uri:  "/thema/0",
			topics: []*taxonomy.NewTopic{
				{
					Name:        "foo",
					Headline:    "Foo!",
					Description: "fo fo fo",
				},
			},
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "nice event",
					Topic:     "0",
					Start:     time.Now().AddDate(1, 0, 0),
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertNodes(t, doc, "h1", []string{"Foo!"})
				assertNodes(t, doc, "h3 span[itemprop='name']", []string{"nice event"})
			},
		},
	}

	testRoutes(t, tests)
}
