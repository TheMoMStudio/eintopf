//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"context"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"github.com/iand/microdata"

	"eintopf.info/internal/xtime"
	"eintopf.info/service/auth"
	"eintopf.info/service/event"
	"eintopf.info/service/eventsearch"
	"eintopf.info/service/group"
	"eintopf.info/service/indexo"
	"eintopf.info/service/oqueue"
	"eintopf.info/service/place"
	"eintopf.info/service/revent"
	"eintopf.info/service/search"
	"eintopf.info/service/taxonomy"
	"eintopf.info/test"
)

func TestEventListPage(t *testing.T) {
	tests := []routeTest{
		{
			name: "OnlyRepeatingFilter",
			tz:   time.UTC,
			uri:  "?repeating=true",
			revents: []*revent.NewRepeatingEvent{
				{
					Name:        "foo",
					Organizers:  []string{"a"},
					Description: "e",
					Intervals: []revent.Interval{
						{
							Type:     revent.IntervalDay,
							Interval: 1,
						},
					},
					Start: time.Now(),
					End:   time.Now().Add(time.Hour * 1),
				},
			},
			events: []*event.NewEvent{
				{
					Name:      "bar1",
					Published: true,
					Parent:    revent.ID("0"),
					Start:     time.Now(),
				},
				{
					Name:      "bar2",
					Published: true,
					Parent:    revent.ID("0"),
					Start:     time.Now().Add(time.Hour * 24),
				},
				{
					Name:      "foo",
					Published: true,
					Start:     time.Now(),
				},
			},
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				if diff := cmp.Diff(2, len(doc.Find(".event").Nodes)); diff != "" {
					t.Errorf(".event mismatch (-want +got):\n%s", diff)
				}
			},
			status: 200,
		},
		{
			name: "NoRepeatingFilter",
			tz:   time.UTC,
			uri:  "?repeating=false",
			revents: []*revent.NewRepeatingEvent{
				{
					Name:        "foo",
					Organizers:  []string{"a"},
					Description: "e",
					Intervals: []revent.Interval{
						{
							Type:     revent.IntervalDay,
							Interval: 1,
						},
					},
					Start: time.Now(),
					End:   time.Now().Add(time.Hour * 1),
				},
			},
			events: []*event.NewEvent{
				{
					Name:      "bar1",
					Published: true,
					Parent:    revent.ID("0"),
					Start:     time.Now(),
				},
				{
					Name:      "bar2",
					Published: true,
					Parent:    revent.ID("0"),
					Start:     time.Now().Add(time.Hour * 24),
				},
				{
					Name:      "foo",
					Published: true,
					Start:     time.Now(),
				},
			},
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				if diff := cmp.Diff(1, len(doc.Find(".event").Nodes)); diff != "" {
					t.Errorf(".event mismatch (-want +got):\n%s", diff)
				}
			},
			status: 200,
		},
		{
			name: "Date",
			tz:   time.UTC,
			uri:  "?date=2022-05-07",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
					Start:     time.Date(2022, 5, 7, 14, 0, 0, 0, time.UTC),
				},
				{
					Published: true,
					Name:      "bar",
					Start:     time.Date(2022, 5, 7, 12, 0, 0, 0, time.UTC),
				},
				{
					Published: true,
					Name:      "not shown",
					Start:     time.Date(2022, 4, 7, 0, 0, 0, 0, time.UTC),
				},
			},
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				if diff := cmp.Diff(2, len(doc.Find(".event").Nodes)); diff != "" {
					t.Errorf(".event mismatch (-want +got):\n%s", diff)
				}
			},
			status: 200,
		},
		{
			name: "Date&Calendar",
			tz:   time.UTC,
			uri:  "?date=2022-04-07&calendar=2022-04",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
					Start:     time.Date(2022, 5, 7, 14, 0, 0, 0, time.UTC),
				},
				{
					Published: true,
					Name:      "bar",
					Start:     time.Date(2022, 5, 7, 12, 0, 0, 0, time.UTC),
				},
				{
					Published: true,
					Name:      "baz",
					Start:     time.Date(2022, 4, 7, 0, 0, 0, 0, time.UTC),
				},
				{
					Published: true,
					Name:      "bat",
					Start:     time.Date(2022, 4, 8, 0, 0, 0, 0, time.UTC),
				},
			},
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				if diff := cmp.Diff(1, len(doc.Find(".event").Nodes)); diff != "" {
					t.Errorf(".event mismatch (-want +got):\n%s", diff)
				}

				testEventlistCalendar(t, doc, ".right")
				testEventlistCalendar(t, doc, ".tabs")
			},
			status: 200,
		},
		{
			name: "Location",
			events: []*event.NewEvent{
				{
					Published: true,
					Start:     time.Now(),
					Location:  "bar",
				},
			},
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertInfoLocation(t, doc, "bar", "", "")
			},
			status: 200,
		},
		{
			name: "OwnLocation",
			events: []*event.NewEvent{
				{
					Published: true,
					Start:     time.Now(),
					Location:  "foo",
					Address:   "bar",
				},
			},
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertInfoLocation(t, doc, "foo", "", "bar")
			},
			status: 200,
		},
		{
			name: "Location2Hidden",
			events: []*event.NewEvent{
				{
					Published: true,
					Start:     time.Now(),
					Location:  "bar",
					Location2: "bar2",
				},
			},
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertInfoLocation(t, doc, "bar", "", "")
			},
			status: 200,
		},
		{
			name: "Location2Hidden own location",
			events: []*event.NewEvent{
				{
					Published: true,
					Start:     time.Now(),
					Location:  "bar",
					Location2: "bar2",
					Address:   "foo",
				},
			},
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertInfoLocation(t, doc, "bar", "", "foo")
			},
			status: 200,
		},
		{
			name: "SubEvent",
			uri:  "?date=2022-05-08",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
					Start:     time.Date(2022, 5, 7, 14, 0, 0, 0, time.UTC),
					End:       tptr(time.Date(2022, 5, 10, 14, 0, 0, 0, time.UTC)),
				},
				{
					Published: true,
					Name:      "sub-foo",
					Parent:    "id:0",
					Start:     time.Date(2022, 5, 8, 12, 0, 0, 0, time.UTC),
				},
			},
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				if diff := cmp.Diff(2, len(doc.Find(".event").Nodes)); diff != "" {
					t.Errorf(".event mismatch (-want +got):\n%s", diff)
				}

				if diff := cmp.Diff(1, len(doc.Find(".program").Nodes)); diff != "" {
					t.Errorf(".program mismatch (-want +got):\n%s", diff)
				}
			},
			status: 200,
		},
		{
			name: "SubEvent2",
			uri:  "?date=2022-05-09",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
					Start:     time.Date(2022, 5, 7, 14, 0, 0, 0, time.UTC),
					End:       tptr(time.Date(2022, 5, 10, 14, 0, 0, 0, time.UTC)),
				},
				{
					Published: true,
					Name:      "sub-foo",
					Parent:    "id:0",
					Start:     time.Date(2022, 5, 8, 12, 0, 0, 0, time.UTC),
				},
			},
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				if diff := cmp.Diff(1, len(doc.Find(".event").Nodes)); diff != "" {
					t.Errorf(".event mismatch (-want +got):\n%s", diff)
				}

				if diff := cmp.Diff(0, len(doc.Find(".program").Nodes)); diff != "" {
					t.Errorf(".program mismatch (-want +got):\n%s", diff)
				}
			},
			status: 200,
		},
		{
			name: "SubEvent3",
			uri:  "",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
					Start:     time.Now(),
					End:       tptr(time.Now().Add(time.Hour * 24 * 4)),
				},
				{
					Published: true,
					Name:      "sub-foo",
					Parent:    "id:0",
					Start:     time.Now().Add(time.Hour * 24 * 2),
				},
			},
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				if diff := cmp.Diff(6, len(doc.Find(".event").Nodes)); diff != "" {
					t.Errorf(".event mismatch (-want +got):\n%s", diff)
				}

				if diff := cmp.Diff(1, len(doc.Find(".program").Nodes)); diff != "" {
					t.Errorf(".program mismatch (-want +got):\n%s", diff)
				}
			},
			status: 200,
		},
		{
			name: "Canceled",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
					Start:     time.Now(),
				},
			},
			transform: func(eventService event.Storer, groupService group.Service, placeService place.Service) error {
				_, err := eventService.Update(context.Background(), &event.Event{
					ID:        "0",
					Published: true,
					Canceled:  true,
					Name:      "foo",
					Start:     time.Now(),
				})
				return err
			},
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				if diff := cmp.Diff(1, len(doc.Find(".event").Nodes)); diff != "" {
					t.Errorf(".event mismatch (-want +got):\n%s", diff)
				}

				// time and title should have canceled class
				if diff := cmp.Diff(2, len(doc.Find(".canceled").Nodes)); diff != "" {
					t.Errorf(".canceled mismatch (-want +got):\n%s", diff)
				}
			},
			status: 200,
		},
		{
			name: "Microdata",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
					Start:     time.Date(2122, 10, 5, 14, 30, 0, 0, time.UTC),
				},
				{
					Published:  true,
					Name:       "bar",
					Start:      time.Date(2122, 10, 5, 14, 40, 0, 0, time.UTC),
					Location:   "id:0",
					Organizers: []string{"id:0", "another organizer"},
				},
			},
			places: []*place.NewPlace{
				{
					Published: true,
					Name:      "Nice Place",
				},
			},
			groups: []*group.NewGroup{
				{
					Published: true,
					Name:      "Nice Group",
				},
			},
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				wantMicrodata := microdata.NewMicrodata()

				event1Microdata := microdata.NewItem()
				event1Microdata.AddType("https://schema.org/Event")
				event1Microdata.AddString("name", "foo")
				event1Microdata.AddString("startDate", "2122-10-05T14:30:00Z")
				event1Microdata.AddString("url", "http://localhost/event/0")
				wantMicrodata.AddItem(event1Microdata)

				placeMicrodata := microdata.NewItem()
				placeMicrodata.AddType("https://schema.org/Place")
				placeMicrodata.AddString("name", "Nice Place")
				placeMicrodata.AddString("url", "http://localhost/ort/0")

				groupMicrodata := microdata.NewItem()
				groupMicrodata.AddType("https://schema.org/Organization")
				groupMicrodata.AddString("name", "Nice Group")
				groupMicrodata.AddString("url", "http://localhost/gruppe/0")

				event2Microdata := microdata.NewItem()
				event2Microdata.AddType("https://schema.org/Event")
				event2Microdata.AddString("name", "bar")
				event2Microdata.AddString("startDate", "2122-10-05T14:40:00Z")
				event2Microdata.AddString("url", "http://localhost/event/1")
				event2Microdata.AddItem("organizer", groupMicrodata)
				event2Microdata.AddString("organizer", "another organizer")
				event2Microdata.AddItem("location", placeMicrodata)

				wantMicrodata.AddItem(event2Microdata)

				assertMicroData(t, doc, wantMicrodata)
			},
			status: 200,
		},
		{
			name: "index with one tag",
			uri:  "?tags=foo",
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertRobotsIndex(t, doc, "")
			},
			status: 200,
		},
		{
			name: "no index with two tags",
			uri:  "?tags=foo&tags=bar",
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertRobotsIndex(t, doc, "noindex, nofollow")
			},
			status: 200,
		},
		{
			name: "no index with date filter",
			uri:  "?date=2022-05-07",
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertRobotsIndex(t, doc, "noindex, nofollow")
			},
			status: 200,
		},
	}

	testRoutes(t, tests)
}

func testEventlistCalendar(t *testing.T, doc *goquery.Document, classPrefix string) {
	t.Helper()
	if diff := cmp.Diff(1, len(doc.Find(classPrefix+" .calendar .current").Nodes)); diff != "" {
		t.Errorf(classPrefix+" .calendar .current (-want +got):\n%s", diff)
	}
	if diff := cmp.Diff(2, len(doc.Find(classPrefix+" .calendar .has-events").Nodes)); diff != "" {
		t.Errorf(classPrefix+" .calendar .has-events (-want +got):\n%s", diff)
	}
	if diff := cmp.Diff(5, len(doc.Find(classPrefix+" .calendar .not-in-month").Nodes)); diff != "" {
		t.Errorf(classPrefix+" .calendar .not-in-month (-want +got):\n%s", diff)
	}
	if diff := cmp.Diff(30, len(doc.Find(classPrefix+" .calendar a").Nodes)); diff != "" {
		t.Errorf(classPrefix+" .calendar a (-want +got):\n%s", diff)
	}
}

func TestEventListOptionsFromRequest(t *testing.T) {
	tz, _ := time.LoadLocation("Europe/Berlin")

	tests := []struct {
		name    string
		url     string
		options eventListOptions
		tz      *time.Location
	}{
		{
			name: "EmptyRequest",
			url:  "/",
			options: eventListOptions{
				Calendar: xtime.Date(time.Now()),
			},
			tz: time.UTC,
		},
		{
			name: "Date",
			url:  "/?date=2022-05-07",
			options: eventListOptions{
				Date:     time.Date(2022, 5, 7, 0, 0, 0, 0, time.UTC),
				Calendar: xtime.Date(time.Now()),
			},
			tz: time.UTC,
		},
		{
			name: "Calendar",
			url:  "/?calendar=2022-05",
			options: eventListOptions{
				Calendar: time.Date(2022, 5, 1, 0, 0, 0, 0, time.UTC),
			},
			tz: time.UTC,
		},
		{
			name: "DateAndCalendar",
			url:  "/?date=2022-04-22&calendar=2022-05",
			options: eventListOptions{
				Date:     time.Date(2022, 4, 22, 0, 0, 0, 0, time.UTC),
				Calendar: time.Date(2022, 5, 1, 0, 0, 0, 0, time.UTC),
			},
			tz: time.UTC,
		},
		{
			name: "DateMinDateMax",
			url:  "/?dateMin=2022-04-22&dateMax=2022-05-22",
			options: eventListOptions{
				MinDate:  time.Date(2022, 4, 22, 0, 0, 0, 0, time.UTC),
				MaxDate:  time.Date(2022, 5, 22, 0, 0, 0, 0, time.UTC),
				Calendar: xtime.Date(time.Now()),
			},
			tz: time.UTC,
		},
		{
			name: "DateMinDateMaxTimeZone",
			url:  "/?dateMin=2022-04-22&dateMax=2022-05-22",
			options: eventListOptions{
				MinDate:  time.Date(2022, 4, 22, 0, 0, 0, 0, time.UTC),
				MaxDate:  time.Date(2022, 5, 22, 0, 0, 0, 0, time.UTC),
				Calendar: xtime.Date(time.Now()),
			},
			tz: tz,
		},
		{
			name: "InvalidDate",
			url:  "/?date=19-0&calendar=1-0&dateMin=12-0",
			options: eventListOptions{
				Date:     time.Time{},
				MinDate:  time.Time{},
				MaxDate:  time.Time{},
				Calendar: xtime.Date(time.Now()),
			},
			tz: tz,
		},
	}
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			req := httptest.NewRequest("GET", tc.url, nil)
			options := eventListOptionsFromRequest(req, tc.tz)
			if diff := cmp.Diff(tc.options, options, cmpopts.IgnoreUnexported(eventListOptions{})); diff != "" {
				t.Errorf("options mismatch (-want +got):\n%s", diff)
			}
		})
	}
}

func TestEventListOptionsToURL(t *testing.T) {
	trueVal  := true
	falseVal := false
	tests := []struct {
		name    string
		options eventListOptions
		url     string
	}{
		{
			name: "repeatingTrue",
			options: eventListOptions{
				Date:         time.Now(),
				Calendar:     time.Now(),
				repeating:     &trueVal,
			},
			url: "/?repeating=true",
		},
		{
			name: "repeatingFalse",
			options: eventListOptions{
				Date:         time.Now(),
				Calendar:     time.Now(),
				repeating:    &falseVal,
			},
			url: "/?repeating=false",
		},
		{
			name: "ToggleRepeatingFilter",
			options: eventListOptions{
				Date:         time.Now(),
				Calendar:     time.Now(),
				repeating:    nil,
			}.ToggleOnlyRepeatingFilter(),
			url: "/?repeating=true",
		},
		{
			name: "ToggleRepeatingFilter",
			options: eventListOptions{
				Date:         time.Now(),
				Calendar:     time.Now(),
				repeating:    nil,
			}.ToggleNoRepeatingFilter(),
			url: "/?repeating=false",
		},
		{
			name: "DateAndCalendar",
			options: eventListOptions{
				Date:     time.Date(2022, 4, 22, 0, 0, 0, 0, time.UTC),
				Calendar: time.Date(2022, 5, 1, 0, 0, 0, 0, time.UTC),
			},
			url: "/?calendar=2022-05&date=2022-04-22",
		},
		{
			name:    "ToggleDate",
			options: eventListOptions{}.ToggleDate(time.Date(2022, 4, 22, 0, 0, 0, 0, time.UTC)),
			url:     "/?date=2022-04-22",
		},
		{
			name: "ToggleTagRemovesTag1",
			options: eventListOptions{
				Date:     time.Now(),
				Calendar: time.Now(),
				Tags:     []string{"foo"},
			}.ToggleTag("foo"),
			url: "/",
		},
		{
			name: "ToggleTagRemovesTag2",
			options: eventListOptions{
				Date:     time.Now(),
				Calendar: time.Now(),
				Tags:     []string{"bar", "foo"},
			}.ToggleTag("foo"),
			url: "/?tags=bar",
		},
		{
			name: "ToggleTagAddsTag",
			options: eventListOptions{
				Date:     time.Now(),
				Calendar: time.Now(),
				Tags:     []string{"bar"},
			}.ToggleTag("foo"),
			url: "/?tags=bar&tags=foo",
		},
		{
			name: "ToggleTagAddsTag2",
			options: eventListOptions{
				Date:     time.Date(2023, 4, 2, 0, 0, 0, 0, time.UTC),
				Calendar: time.Now(),
				Tags:     []string{"bar"},
			}.ToggleTag("foo"),
			url: "/?date=2023-04-02&tags=bar&tags=foo",
		},
		{
			name: "SetTagRemovesExistingTags",
			options: eventListOptions{
				Date:     time.Now(),
				Calendar: time.Now(),
				Tags:     []string{"bar", "foo"},
			}.SetTag("x"),
			url: "/?tags=x",
		},
	}
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			url := tc.options.URL()
			if diff := cmp.Diff(tc.url, url); diff != "" {
				t.Errorf("url mismatch (-want +got):\n%s", diff)
			}
		})
	}
}

func TestEventListOptionsDateMinMax(t *testing.T) {
	tests := []struct {
		name    string
		options eventListOptions
		min     time.Time
		max     time.Time
	}{
		{
			name:    "Empty",
			options: eventListOptions{tz: time.UTC},
			min:     xtime.Date(xtime.NowInLocation(time.UTC)),
			max:     time.Time{},
		},
		{
			name: "Date",
			options: eventListOptions{
				tz:    time.UTC,
				Date:  time.Date(2022, 4, 22, 0, 0, 0, 0, time.UTC),
				Pages: nil,
			},
			min: time.Date(2022, 4, 22, 0, 0, 0, 0, time.UTC),
			max: time.Date(2022, 4, 22, 23, 59, 59, 0, time.UTC),
		},
		{
			name: "DateMinDatMax",
			options: eventListOptions{
				tz:      time.UTC,
				MinDate: time.Date(2022, 3, 22, 0, 0, 0, 0, time.UTC),
				MaxDate: time.Date(2022, 7, 22, 0, 0, 0, 0, time.UTC),
				Pages:   nil,
			},
			min: time.Date(2022, 3, 22, 0, 0, 0, 0, time.UTC),
			max: time.Date(2022, 7, 22, 23, 59, 59, 0, time.UTC),
		},
		{
			name: "Page0",
			options: eventListOptions{
				tz:   time.UTC,
				Page: 0,
				Pages: []Page{
					{
						Start: time.Date(2022, 4, 22, 0, 0, 0, 0, time.UTC),
						End:   time.Date(2022, 4, 22, 0, 0, 0, 0, time.UTC),
					},
					{
						Start: time.Date(2022, 5, 22, 0, 0, 0, 0, time.UTC),
						End:   time.Date(2022, 7, 22, 0, 0, 0, 0, time.UTC),
					},
				},
			},
			min: time.Date(2022, 4, 22, 0, 0, 0, 0, time.UTC),
			max: time.Date(2022, 4, 22, 23, 59, 59, 0, time.UTC),
		},
		{
			name: "Page1",
			options: eventListOptions{
				tz:   time.UTC,
				Page: 1,
				Pages: []Page{
					{
						Start: time.Date(2022, 4, 22, 0, 0, 0, 0, time.UTC),
						End:   time.Date(2022, 4, 22, 0, 0, 0, 0, time.UTC),
					},
					{
						Start: time.Date(2022, 5, 22, 0, 0, 0, 0, time.UTC),
						End:   time.Date(2022, 7, 22, 0, 0, 0, 0, time.UTC),
					},
				},
			},
			min: time.Date(2022, 5, 22, 0, 0, 0, 0, time.UTC),
			max: time.Date(2022, 7, 22, 23, 59, 59, 0, time.UTC),
		},
	}
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			min := timeWithoutMilliseconds(tc.options.DateMin(true))
			max := timeWithoutMilliseconds(tc.options.DateMax(true))
			if min != tc.min {
				t.Errorf("invalid min date\nwant: %s\ngot:  %s", tc.min.String(), min.String())
			}
			if max != tc.max {
				t.Errorf("invalid max date\nwant: %s\ngot:  %s", tc.max.String(), max.String())
			}
		})
	}
}

func TestEventListOptionsShouldIndex(t *testing.T) {
	tests := []struct {
		name        string
		options     eventListOptions
		shouldIndex bool
	}{
		{
			name: "Date",
			options: eventListOptions{
				Date: time.Date(2022, 4, 22, 0, 0, 0, 0, time.UTC),
			},
			shouldIndex: false,
		},
		{
			name: "Calendar",
			options: eventListOptions{
				Calendar: time.Date(2022, 5, 1, 0, 0, 0, 0, time.UTC),
			},
			shouldIndex: false,
		},
		{
			name: "DateAndCalendar",
			options: eventListOptions{
				Date:     time.Date(2022, 4, 22, 0, 0, 0, 0, time.UTC),
				Calendar: time.Date(2022, 5, 1, 0, 0, 0, 0, time.UTC),
			},
			shouldIndex: false,
		},
		{
			name: "One tag",
			options: eventListOptions{
				Tags: []string{"foo"},
			},
			shouldIndex: true,
		},
		{
			name: "Two tags",
			options: eventListOptions{
				Tags: []string{"foo", "bar"},
			},
			shouldIndex: false,
		},
		{
			name: "One tag and date",
			options: eventListOptions{
				Date: time.Date(2022, 4, 22, 0, 0, 0, 0, time.UTC),
				Tags: []string{"foo"},
			},
			shouldIndex: false,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			shouldIndex := tc.options.ShouldIndex()
			if tc.shouldIndex != shouldIndex {
				t.Errorf("want %t changes, got %t", tc.shouldIndex, shouldIndex)
			}
		})
	}
}

func timeWithoutMilliseconds(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second(), 0, time.UTC)
}

func TestGetEventListPagination(t *testing.T) {
	events := []*event.NewEvent{
		{ // 6 Days
			Published: true,
			Name:      "a",
			Start:     time.Date(2022, 5, 2, 12, 0, 0, 0, time.UTC),
			End:       tp(time.Date(2022, 5, 7, 14, 0, 0, 0, time.UTC)),
		},
		{ // 1 Day
			Published: true,
			Name:      "b",
			Start:     time.Date(2022, 5, 2, 13, 0, 0, 0, time.UTC),
			End:       tp(time.Date(2022, 5, 2, 14, 0, 0, 0, time.UTC)),
		},
		{ // 4 Days
			Published: true,
			Name:      "c",
			Start:     time.Date(2022, 5, 3, 13, 0, 0, 0, time.UTC),
			End:       tp(time.Date(2022, 5, 6, 14, 0, 0, 0, time.UTC)),
		},
		{ // 1 Day
			Published: true,
			Name:      "d",
			Start:     time.Date(2022, 5, 4, 13, 0, 0, 0, time.UTC),
			End:       tp(time.Date(2022, 5, 4, 14, 0, 0, 0, time.UTC)),
		},
		{ // 9 Days
			Published: true,
			Name:      "e",
			Start:     time.Date(2022, 5, 3, 13, 0, 0, 0, time.UTC),
			End:       tp(time.Date(2022, 5, 12, 13, 0, 0, 0, time.UTC)),
		},
	}

	index, cleanupIndex := test.CreateBleveTestIndex("TestGetEventListPagination")
	t.Cleanup(cleanupIndex)
	searchService, err := search.NewService(index, time.Second, 1, 1, time.UTC)
	if err != nil {
		t.Fatalf("search: %s", err)
	}

	eventService := event.NewService(event.NewMemoryStore(), nil)
	groupService := group.NewService(group.NewMemoryStore())
	placeService := place.NewService(place.NewMemoryStore())
	categoryService := taxonomy.NewCategoryMemoryStore()
	topicService := taxonomy.NewTopicMemoryStore()

	queue := oqueue.NewQueue()
	indexoServcice := indexo.NewService(queue, searchService, eventService, groupService, placeService)

	ctx := auth.ContextWithID(context.Background(), "1")
	for _, e := range events {
		if _, err := eventService.Create(ctx, e); err != nil {
			t.Fatalf("create event: %s", err)
		}
	}
	eventsearchService := eventsearch.NewService(searchService, eventService, groupService, placeService, nil, categoryService, topicService)
	eventsearchService.Reindex(context.Background())
	indexoServcice.Reindex(context.Background())

	renderer := NewRenderer(searchService, eventsearchService, categoryService, topicService, nil, "", time.UTC)
	pages, err := renderer.getEventListPagination(context.Background(), time.Date(2022, 5, 2, 12, 0, 0, 0, time.UTC), eventListOptions{})
	if err != nil {
		t.Fatal(err)
	}

	wantPages := []Page{
		{
			Start: time.Date(2022, 5, 2, 0, 0, 0, 0, time.UTC),
			End:   time.Date(2022, 5, 6, 0, 0, 0, 0, time.UTC),
		},
		{
			Start: time.Date(2022, 5, 7, 0, 0, 0, 0, time.UTC),
			End:   time.Date(2022, 5, 12, 0, 0, 0, 0, time.UTC),
		},
	}

	if diff := cmp.Diff(wantPages, pages); diff != "" {
		t.Errorf("pages mismatch (-want +got):\n%s", diff)
	}
}

func TestGroupEventsByDay(t *testing.T) {
	event1 := &eventsearch.EventDocument{
		ID:       "1",
		Date:     time.Date(2022, 5, 2, 12, 0, 0, 0, time.UTC),
		Start:    time.Date(2022, 5, 2, 12, 0, 0, 0, time.UTC),
		End:      tp(time.Date(2022, 5, 2, 14, 0, 0, 0, time.UTC)),
		MultiDay: -1,
	}
	event2 := &eventsearch.EventDocument{
		ID:       "2",
		Date:     time.Date(2022, 5, 2, 13, 0, 0, 0, time.UTC),
		Start:    time.Date(2022, 5, 2, 13, 0, 0, 0, time.UTC),
		End:      tp(time.Date(2022, 5, 2, 14, 0, 0, 0, time.UTC)),
		MultiDay: -1,
	}
	noEndDate := &eventsearch.EventDocument{
		ID:       "3",
		Date:     time.Date(2022, 5, 2, 13, 0, 0, 0, time.UTC),
		Start:    time.Date(2022, 5, 2, 13, 0, 0, 0, time.UTC),
		MultiDay: -1,
	}
	multidayEventDay1 := &eventsearch.EventDocument{
		ID:       "4",
		Date:     time.Date(2022, 5, 2, 13, 0, 0, 0, time.UTC),
		Start:    time.Date(2022, 5, 2, 13, 0, 0, 0, time.UTC),
		End:      tp(time.Date(2022, 5, 4, 13, 0, 0, 0, time.UTC)),
		MultiDay: 1,
	}
	multidayEventDay2 := &eventsearch.EventDocument{
		ID:       "4",
		Date:     time.Date(2022, 5, 3, 13, 0, 0, 0, time.UTC),
		Start:    time.Date(2022, 5, 2, 13, 0, 0, 0, time.UTC),
		End:      tp(time.Date(2022, 5, 4, 13, 0, 0, 0, time.UTC)),
		MultiDay: 2,
	}
	multidayEventDay3 := &eventsearch.EventDocument{
		ID:       "4",
		Date:     time.Date(2022, 5, 4, 13, 0, 0, 0, time.UTC),
		Start:    time.Date(2022, 5, 2, 13, 0, 0, 0, time.UTC),
		End:      tp(time.Date(2022, 5, 4, 13, 0, 0, 0, time.UTC)),
		MultiDay: 3,
	}

	tests := []struct {
		name   string
		events []*eventsearch.EventDocument
		days   map[time.Time][]*eventsearch.EventDocument
	}{
		{
			name:   "simple",
			events: []*eventsearch.EventDocument{event1, event2, noEndDate},
			days: map[time.Time][]*eventsearch.EventDocument{
				date(2022, 5, 2, time.UTC): {event1, event2, noEndDate},
			},
		},
		{
			name: "multiday",
			events: []*eventsearch.EventDocument{
				event1,
				event2,
				multidayEventDay1,
				multidayEventDay2,
				multidayEventDay3,
			},
			days: map[time.Time][]*eventsearch.EventDocument{
				date(2022, 5, 2, time.UTC): {multidayEventDay1, event1, event2},
				date(2022, 5, 3, time.UTC): {multidayEventDay2},
				date(2022, 5, 4, time.UTC): {multidayEventDay3},
			},
		},
	}
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			days := groupEventsByDays(tc.events)
			if diff := cmp.Diff(tc.days, days, cmpopts.IgnoreUnexported(eventsearch.EventDocument{})); diff != "" {
				t.Errorf("days mismatch (-want +got):\n%s", diff)
			}
		})
	}
}

func tp(t time.Time) *time.Time {
	return &t
}

func TestCalendarMonth(t *testing.T) {
	tests := []struct {
		name            string
		year            int
		month           time.Month
		datesWithEvents search.TermsBucket
		tz              *time.Location
		days            []calendarDay
	}{
		{
			name:            "simple",
			year:            2022,
			month:           7,
			datesWithEvents: search.TermsBucket{},
			tz:              time.UTC,
			days: []calendarDay{
				{Date: date(2022, 6, 27, time.UTC)},
				{Date: date(2022, 6, 28, time.UTC)},
				{Date: date(2022, 6, 29, time.UTC)},
				{Date: date(2022, 6, 30, time.UTC)},
				{Date: date(2022, 7, 1, time.UTC), InMonth: true},
				{Date: date(2022, 7, 2, time.UTC), InMonth: true},
				{Date: date(2022, 7, 3, time.UTC), InMonth: true},
				{Date: date(2022, 7, 4, time.UTC), InMonth: true},
				{Date: date(2022, 7, 5, time.UTC), InMonth: true},
				{Date: date(2022, 7, 6, time.UTC), InMonth: true},
				{Date: date(2022, 7, 7, time.UTC), InMonth: true},
				{Date: date(2022, 7, 8, time.UTC), InMonth: true},
				{Date: date(2022, 7, 9, time.UTC), InMonth: true},
				{Date: date(2022, 7, 10, time.UTC), InMonth: true},
				{Date: date(2022, 7, 11, time.UTC), InMonth: true},
				{Date: date(2022, 7, 12, time.UTC), InMonth: true},
				{Date: date(2022, 7, 13, time.UTC), InMonth: true},
				{Date: date(2022, 7, 14, time.UTC), InMonth: true},
				{Date: date(2022, 7, 15, time.UTC), InMonth: true},
				{Date: date(2022, 7, 16, time.UTC), InMonth: true},
				{Date: date(2022, 7, 17, time.UTC), InMonth: true},
				{Date: date(2022, 7, 18, time.UTC), InMonth: true},
				{Date: date(2022, 7, 19, time.UTC), InMonth: true},
				{Date: date(2022, 7, 20, time.UTC), InMonth: true},
				{Date: date(2022, 7, 21, time.UTC), InMonth: true},
				{Date: date(2022, 7, 22, time.UTC), InMonth: true},
				{Date: date(2022, 7, 23, time.UTC), InMonth: true},
				{Date: date(2022, 7, 24, time.UTC), InMonth: true},
				{Date: date(2022, 7, 25, time.UTC), InMonth: true},
				{Date: date(2022, 7, 26, time.UTC), InMonth: true},
				{Date: date(2022, 7, 27, time.UTC), InMonth: true},
				{Date: date(2022, 7, 28, time.UTC), InMonth: true},
				{Date: date(2022, 7, 29, time.UTC), InMonth: true},
				{Date: date(2022, 7, 30, time.UTC), InMonth: true},
				{Date: date(2022, 7, 31, time.UTC), InMonth: true},
			},
		},
		{
			name:  "with events",
			year:  2022,
			month: 7,
			datesWithEvents: search.TermsBucket{
				search.Term{Term: "2022-06-28T00:00:00Z"},
				search.Term{Term: "2022-07-01T00:00:00Z"},
				search.Term{Term: "2022-07-05T00:00:00Z"},
				search.Term{Term: "2022-07-05T00:00:00Z"},
				search.Term{Term: "2022-07-20T00:00:00Z"},
				search.Term{Term: "2022-07-28T00:00:00Z"},
			},
			tz: time.UTC,
			days: []calendarDay{
				{Date: date(2022, 6, 27, time.UTC)},
				{Date: date(2022, 6, 28, time.UTC), HasEvents: true},
				{Date: date(2022, 6, 29, time.UTC)},
				{Date: date(2022, 6, 30, time.UTC)},
				{Date: date(2022, 7, 1, time.UTC), InMonth: true, HasEvents: true},
				{Date: date(2022, 7, 2, time.UTC), InMonth: true},
				{Date: date(2022, 7, 3, time.UTC), InMonth: true},
				{Date: date(2022, 7, 4, time.UTC), InMonth: true},
				{Date: date(2022, 7, 5, time.UTC), InMonth: true, HasEvents: true},
				{Date: date(2022, 7, 6, time.UTC), InMonth: true},
				{Date: date(2022, 7, 7, time.UTC), InMonth: true},
				{Date: date(2022, 7, 8, time.UTC), InMonth: true},
				{Date: date(2022, 7, 9, time.UTC), InMonth: true},
				{Date: date(2022, 7, 10, time.UTC), InMonth: true},
				{Date: date(2022, 7, 11, time.UTC), InMonth: true},
				{Date: date(2022, 7, 12, time.UTC), InMonth: true},
				{Date: date(2022, 7, 13, time.UTC), InMonth: true},
				{Date: date(2022, 7, 14, time.UTC), InMonth: true},
				{Date: date(2022, 7, 15, time.UTC), InMonth: true},
				{Date: date(2022, 7, 16, time.UTC), InMonth: true},
				{Date: date(2022, 7, 17, time.UTC), InMonth: true},
				{Date: date(2022, 7, 18, time.UTC), InMonth: true},
				{Date: date(2022, 7, 19, time.UTC), InMonth: true},
				{Date: date(2022, 7, 20, time.UTC), InMonth: true, HasEvents: true},
				{Date: date(2022, 7, 21, time.UTC), InMonth: true},
				{Date: date(2022, 7, 22, time.UTC), InMonth: true},
				{Date: date(2022, 7, 23, time.UTC), InMonth: true},
				{Date: date(2022, 7, 24, time.UTC), InMonth: true},
				{Date: date(2022, 7, 25, time.UTC), InMonth: true},
				{Date: date(2022, 7, 26, time.UTC), InMonth: true},
				{Date: date(2022, 7, 27, time.UTC), InMonth: true},
				{Date: date(2022, 7, 28, time.UTC), InMonth: true, HasEvents: true},
				{Date: date(2022, 7, 29, time.UTC), InMonth: true},
				{Date: date(2022, 7, 30, time.UTC), InMonth: true},
				{Date: date(2022, 7, 31, time.UTC), InMonth: true},
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			days := calendarMonth(tc.year, tc.month, tc.datesWithEvents, tc.tz)
			if diff := cmp.Diff(tc.days, days); diff != "" {
				t.Errorf("days mismatch (-want +got):\n%s", diff)
			}
		})
	}
}
