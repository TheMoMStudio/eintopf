//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"fmt"
	"net/http"
	"net/url"
	"text/template"

	"eintopf.info/internal/xerror"
	"eintopf.info/internal/xtime"
	"eintopf.info/service/eventsearch"
	"eintopf.info/service/search"

	"github.com/go-chi/chi/v5"
)

// PlacePage renders a place page.
func (renderer *Renderer) PlacePage(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	if id == "" {
		renderer.errorPage(w, r, xerror.NotFoundError{Err: fmt.Errorf("place not found: empty id"), Type: "place"})
		return
	}
	id, err := url.QueryUnescape(id)
	if err != nil {
		renderer.errorPage(w, r, xerror.NotFoundError{Err: fmt.Errorf("place not found: invalid id=%s", id), Type: "place"})
		return
	}
	result, err := renderer.search.Search(r.Context(), &search.Options{
		Filters: []search.Filter{
			&search.TermsFilter{Field: "type", Terms: []string{"place"}},
			&search.TermsFilter{Field: "id", Terms: []string{id}},
		},
	})
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}
	if len(result.Hits) != 1 {
		renderer.errorPage(w, r, xerror.NotFoundError{Err: fmt.Errorf("place not found: id=%s", id), Type: "place"})
		return
	}

	place := PlaceDocument{}
	err = result.Hits[0].Unmarshal(&place)
	if err != nil {
		renderer.errorPage(w, r, fmt.Errorf("unmarshal place: id=%s: %w", id, err))
		return
	}

	pastEvents, err := renderer.eventSearch.Search(r.Context(), eventsearch.Options{
		Sort:           "date",
		SortDescending: true,
		PageSize:       5,
		Filters: []eventsearch.Filter{
			eventsearch.ListedFilter{Listed: true},
			eventsearch.PlaceIDFilter{PlaceID: id},
			eventsearch.StartRangeFilter{StartMax: xtime.NowInLocation(renderer.tz)},
			eventsearch.MultidayRangeFilter{MultidayMax: iptr(1)},
		},
	})
	if err != nil {
		renderer.errorPage(w, r, fmt.Errorf("search past events: placeID=%s: %w", id, err))
		return
	}

	futureEvents, err := renderer.eventSearch.Search(r.Context(), eventsearch.Options{
		Sort:     "date",
		PageSize: 5,
		Filters: []eventsearch.Filter{
			eventsearch.ListedFilter{Listed: true},
			eventsearch.PlaceIDFilter{PlaceID: id},
			eventsearch.StartRangeFilter{StartMin: xtime.NowInLocation(renderer.tz)},
			eventsearch.MultidayRangeFilter{MultidayMax: iptr(1)},
		},
	})
	if err != nil {
		renderer.errorPage(w, r, fmt.Errorf("search future events: placeID=%s: %w", id, err))
		return
	}

	metaTags := MetaTags{Title: place.Name, Description: place.Description, Image: place.Image, NotPublished: !place.Published}

	err = renderer.renderPage(w, r, "place", map[string]any{
		"Place":        place,
		"Options":      eventListOptions{},
		"PastEvents":   pastEvents.Events,
		"FutureEvents": futureEvents.Events,
		"Meta":         metaTags,
	}, template.FuncMap{
		"placeShouldRenderMap": func(p PlaceDocument) bool {
			if p.Lat == 0 || p.Lng == 0 {
				return false
			}
			return true
		},
	})
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}
}
