//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"encoding/json"
	"encoding/xml"
	"net/url"
	"strings"
	"testing"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/google/go-cmp/cmp"
	"github.com/iand/microdata"
)

func TestAssets(t *testing.T) {
	testRoutes(t, []routeTest{
		{
			name:   "override favicon",
			uri:    "/favicon.ico",
			status: 200,
			contentTestFunc: func(t *testing.T, content string) {
				content = strings.TrimSpace(content)
				if content != "foo" {
					t.Errorf("content should be 'foo', got '%s'", content)
				}
			},
			theme:     "foo",
			themePath: "testdata/themes/",
		},
		{
			name:   "overwrite of robots.txt",
			uri:    "/robots.txt",
			status: 200,
			contentTestFunc: func(t *testing.T, content string) {
				content = strings.TrimSpace(content)

				if content != "Sitemap: https://example.com/sitemap.xml" {
					t.Errorf("content should be 'Sitemap: https://example.com/sitemap.xml', got '%s'", content)
				}
			},
			theme:     "foo",
			themePath: "testdata/themes/",
		},
		{
			name:   "static content of sitemap",
			uri:    "/sitemap.xml",
			status: 200,
			contentTestFunc: func(t *testing.T, content string) {
				var xmlData UrlSet
				xml.Unmarshal([]byte(content), &xmlData)

				staticRoutes := []string{"", "/gruppen", "/orte", "/about"}

				if len(xmlData.Url) != 4 {
					t.Errorf("content should be '4', got '%d'", len(xmlData.Url))
					return
				}

				for i, url := range xmlData.Url {
					// skip root route because port of localhost is not kown in test
					if i == 0 {
						continue
					}
					if !strings.Contains(url.Loc, staticRoutes[i]) {
						t.Errorf("content should be '%s', got '%s'", staticRoutes[i], url.Loc)
					}
				}
			},
			theme:     "foo",
			themePath: "testdata/themes/",
		},
		{
			name:      "custom asset found",
			uri:       "/assets/foo.txt",
			status:    200,
			theme:     "foo",
			themePath: "testdata/themes/",
		},
		{
			name:      "custom asset not found",
			uri:       "/foo.txt",
			status:    404,
			theme:     "foo",
			themePath: "testdata/themes/",
		},
		{
			name:      "page not found",
			uri:       "/fooo",
			status:    404,
			theme:     "foo",
			themePath: "testdata/themes/",
			contentTestFunc: func(t *testing.T, content string) {
				if !strings.Contains(content, "404 Seite nicht gefunden") {
					t.Error("should contain '404 Seite nicht gefunden'")
				}

				if strings.Contains(content, "500") {
					t.Error("should not contain '500'")
				}
			},
		},
		{
			name:      "invalid url",
			uri:       "/../about.go",
			status:    404,
			theme:     "foo",
			themePath: "testdata/themes/",
		},
		{
			name:      "imprint",
			uri:       "/imprint",
			status:    200,
			theme:     "foo",
			themePath: "testdata/themes/",
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				assertRobotsIndex(t, doc, "noindex, nofollow")
			},
		},
		{
			name:      "dtaprotection",
			uri:       "/dataprotection",
			status:    200,
			theme:     "foo",
			themePath: "testdata/themes/",
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				assertRobotsIndex(t, doc, "noindex, nofollow")
			},
		},
	})
}

func TestOverridePage(t *testing.T) {
	testRoutes(t, []routeTest{
		{
			name:   "override page.tpl and eventlist.tpl",
			uri:    "/",
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				if len(doc.Find("header").Nodes) != 1 {
					t.Errorf("header not found")
				}
				if !strings.Contains(doc.Text(), "foo header") {
					t.Errorf("expected 'foo header' from beforeHeader, got: '%s'", doc.Text())
				}
				if doc.Find("title").Text() != "foo title" {
					t.Errorf("expected title to be 'custom foo title', got: '%s'", doc.Find("title").Text())
				}
				if !strings.Contains(doc.Find("main").Text(), "foo eventlist") {
					t.Errorf("expected 'foo eventlist' in main, got: '%s'", doc.Find("main").Text())
				}
			},
			theme:     "foo",
			themePath: "testdata/themes/",
		},
		{
			name:   "multiple custom themes",
			uri:    "/",
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				if len(doc.Find("header").Nodes) != 1 {
					t.Errorf("header not found")
				}
				if !strings.Contains(doc.Text(), "foo header") {
					t.Errorf("expected 'foo header' from beforeHeader, got: '%s'", doc.Text())
				}
				if doc.Find("title").Text() != "foo title" {
					t.Errorf("expected title to be 'custom foo title', got: '%s'", doc.Find("title").Text())
				}
				if !strings.Contains(doc.Find("main").Text(), "bar eventlist") {
					t.Errorf("expected 'bar eventlist' in main, got: '%s'", doc.Find("main").Text())
				}
			},
			theme:     "bar",
			themePath: "testdata/themes/",
		},
	})
}

func TestCustomPage(t *testing.T) {
	testRoutes(t, []routeTest{
		{
			name:   "new custom page",
			tz:     time.UTC,
			uri:    "/foo",
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				if !strings.Contains(doc.Text(), "foo") {
					t.Errorf("expected foo in custom page 'foo', got: '%s'", doc.Text())
				}
			},
			theme:     "foo",
			themePath: "testdata/themes/",
		},
		{
			name:   "nested url",
			tz:     time.UTC,
			uri:    "/bar/bar",
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				if doc.Text() != "bar\n" {
					t.Errorf("expected custom page to have content 'bar', got: '%s'", doc.Text())
				}
			},
			theme:     "foo",
			themePath: "testdata/themes/",
		},
		{
			name:      "invalid url",
			tz:        time.UTC,
			uri:       "/../bar",
			status:    404,
			theme:     "foo",
			themePath: "testdata/themes/",
		},
		{
			name:      "not found",
			tz:        time.UTC,
			uri:       "/foobar",
			status:    404,
			theme:     "foo",
			themePath: "testdata/themes/",
		},
	})
}

func assertNodes(t *testing.T, doc *goquery.Document, node string, text []string) {
	t.Helper()

	if diff := cmp.Diff(len(text), len(doc.Find(node).Nodes)); diff != "" {
		t.Errorf("%s mismatch (-want +got):\n%s", node, diff)
	}
	if diff := cmp.Diff(strings.Join(text, ""), doc.Find(node).Text()); diff != "" {
		t.Errorf("%s text mismatch (-want +got):\n%s", node, diff)
	}
}

func assertMap(t *testing.T, doc *goquery.Document) {
	t.Helper()

	if len(doc.Find("#map").Nodes) == 0 {
		t.Errorf("map not found")
	}
}

func assertNoMap(t *testing.T, doc *goquery.Document) {
	t.Helper()

	if len(doc.Find("#map").Nodes) != 0 {
		t.Errorf("map found, but should not be there")
	}
}

func assertLinkInDesc(t *testing.T, doc *goquery.Document,
	expectedRef []string, expectedLinkText []string) {

	links := doc.Selection.Find("a")
	links.Each(func(i int, s *goquery.Selection) {
		href, exists := s.Attr("href")
		if exists {
			linkText := ""
			s.Contents().Each(func(i int, sel *goquery.Selection) {
				if goquery.NodeName(sel) == "#text" { // Check if the node is a text node
					text := strings.TrimSpace(sel.Text())
					if text != "" {
						linkText += text + " " // Append text with a space
					}
				}
			})
			linkText = strings.TrimSpace(linkText)

			for j, link := range expectedRef {
				if href == link {
					expectedRef = append(expectedRef[:j], expectedRef[j+1:]...)
				}
			}
			for j, link := range expectedLinkText {
				if linkText == link {
					expectedLinkText = append(expectedLinkText[:j], expectedLinkText[j+1:]...)
				}
			}
		}
	})

	if len(expectedRef) > 0 {
		t.Errorf("Not all links found: %v", expectedRef)
	}

	if len(expectedLinkText) > 0 {
		t.Errorf("Link doesnt match expected text: %v", expectedLinkText)
	}
}

func assertFutureEvents(t *testing.T, doc *goquery.Document, names []string) {
	t.Helper()
	text := doc.Find(".future-events h3").Text()
	if diff := cmp.Diff(len(names), len(doc.Find(".future-events h3").Nodes)); diff != "" {
		t.Errorf(".future-events h3 (-want +got):\n%s\n%s", diff, text)
	}
	for _, name := range names {
		if !strings.Contains(text, name) {
			t.Errorf("%s not in %s", name, text)
		}
	}
}

func assertPastEvents(t *testing.T, doc *goquery.Document, names []string) {
	t.Helper()
	text := doc.Find(".past-events h3").Text()
	if diff := cmp.Diff(len(names), len(doc.Find(".past-events h3").Nodes)); diff != "" {
		t.Errorf(".past-events h3 (-want +got):\n%s\n%s", diff, text)
	}
	for _, name := range names {
		if !strings.Contains(text, name) {
			t.Errorf("%s not in %s", name, text)
		}
	}
}

func assertMeta(t *testing.T, doc *goquery.Document, meta MetaTags) {
	t.Helper()

	title, exists := doc.Find(`[property="og:title"]`).Attr("content")
	if !exists {
		t.Errorf(`[property="og:title"] does not exist!\n`)
	}
	if diff := cmp.Diff(meta.Title, title); diff != "" {
		t.Errorf(`[property="og:title"] mismatch (-want +got):\n%s`, diff)
	}

	description, exists := doc.Find(`[property="og:description"]`).Attr("content")
	if !exists {
		t.Errorf(`[property="og:description"] does not exist!\n`)
	}
	if diff := cmp.Diff(meta.Description, description); diff != "" {
		t.Errorf(`[property="og:description"] mismatch (-want +got):\n%s`, diff)
	}

	image, exists := doc.Find(`[property="og:image"]`).Attr("content")
	if !exists {
		t.Errorf(`[property="og:image"] does not exist!\n`)
	}
	if !strings.Contains(image, meta.Image) {
		t.Errorf(`[property="og:image"] mismatch (-want +got):\n -%s +%s`, meta.Image, image)
	}
}

func assertRobotsIndex(t *testing.T, doc *goquery.Document, robotsContent string) {
	t.Helper()

	content, exists := doc.Find(`[name="robots"]`).Attr("content")
	if !exists {
		t.Errorf(`[name="robots"] does not exist!\n`)
	}
	if diff := cmp.Diff(robotsContent, content); diff != "" {
		t.Errorf(`[name="robots"] mismatch (-want +got):\n%s`, diff)
	}
}

func assertMicroData(t *testing.T, doc *goquery.Document, want *microdata.Microdata) {
	t.Helper()

	html, err := doc.Html()
	if err != nil {
		t.Error(err)
	}
	baseUrl, _ := url.Parse("http://localhost/")
	p := microdata.NewParser(strings.NewReader(html), baseUrl)
	data, err := p.Parse()
	if err != nil {
		t.Error(err)
	}

	pageMicrodataJSON, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		t.Errorf(`JSON marshal failed for pageMicrodataJSON: %s`, err)
	}

	wantJSON, err := json.MarshalIndent(want, "", "  ")
	if err != nil {
		t.Errorf(`JSON marshal failed for testMicrodataJSON`)
	}

	if diff := cmp.Diff(string(wantJSON), string(pageMicrodataJSON)); diff != "" {
		t.Errorf(`schema event content mismatch (-want +got):\n%s`, diff)
	}
}
