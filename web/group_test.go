//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"testing"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/google/go-cmp/cmp"
	"github.com/iand/microdata"

	"eintopf.info/service/event"
	"eintopf.info/service/group"
)

func TestGroupPage(t *testing.T) {
	tests := []routeTest{
		{
			name: "linkDisplayTest",
			tz:   time.UTC,
			uri:  "/gruppe/0",
			groups: []*group.NewGroup{
				{
					Published: true,
					Name:      "linkDisplayTest",
					Description: `EVENTBESCHREIBUNG https://www.staatsgalerie.de/de\n adsadsad
					sdadssadl dasd https://eintopf.info/ d sadad sad d,
					sd https://www.diesisteinmegalangerteststringdersolangistdasserabgekuerztwerdenmusssadasdsaadsasdsadasdasdasdasdsaddddda.de
					sadasdasd dddsadsd
									s`,
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				expectedRef := []string{"https://www.staatsgalerie.de/de", "https://eintopf.info/",
					"https://www.diesisteinmegalangerteststringdersolangistdasserabgekuerztwerdenmusssadasdsaadsasdsadasdasdasdasdsaddddda.de"}
				expectedLinkText := []string{"https://www.staatsgalerie.de/de", "https://eintopf.info/",
					"https://www.diesisteinmegalangerteststringdersolangistdasserabgekuerztwerdenmusssadasdsaadsasdsadasd..."}
				assertLinkInDesc(t, doc, expectedRef, expectedLinkText)
			},
		},
		{
			name: "NotFound",
			tz:   time.UTC,
			uri:  "/gruppe/12345",
			groups: []*group.NewGroup{
				{
					Published: true,
					Name:      "foo",
				},
			},
			status: 404,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				if diff := cmp.Diff("404 Gruppe wurde nicht gefunden", doc.Find(".error").Text()); diff != "" {
					t.Errorf(".error mismatch (-want +got):\n%s", diff)
				}
			},
		},
		{
			name: "Works",
			tz:   time.UTC,
			uri:  "/gruppe/0",
			groups: []*group.NewGroup{
				{
					Published: true,
					Name:      "foo",
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				if diff := cmp.Diff(2, len(doc.Find("h2").Nodes)); diff != "" {
					t.Errorf("h2 mismatch (-want +got):\n%s", diff)
				}
				if diff := cmp.Diff("foofoo", doc.Find("h2").Text()); diff != "" {
					t.Errorf("h2 text mismatch (-want +got):\n%s", diff)
				}
			},
		},
		{
			name: "NotPublished",
			uri:  "/gruppe/0",
			groups: []*group.NewGroup{
				{
					Published: false,
					Name:      "foo",
				},
			},
			status: 200,
		},
		{
			name: "MetaNoIndexNotPublished",
			tz:   time.UTC,
			uri:  "/gruppe/0",
			groups: []*group.NewGroup{
				{
					Published: false,
					Name:      "foo",
				},
			},
			status: 200,
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				assertRobotsIndex(t, doc, "noindex, nofollow")
			},
		},
		{
			name: "MetaIndexPublished",
			tz:   time.UTC,
			uri:  "/gruppe/0",
			groups: []*group.NewGroup{
				{
					Published: true,
					Name:      "foo",
				},
			},
			status: 200,
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				assertRobotsIndex(t, doc, "index, follow")
			},
		},
		{
			name:      "MetaDefault",
			tz:        time.UTC,
			uri:       "/gruppe/0",
			theme:     "meta",
			themePath: "testdata/themes/",
			groups: []*group.NewGroup{
				{
					Published: true,
					Name:      "Foo",
				},
			},
			status: 200,
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				assertRobotsIndex(t, doc, "index, follow")
				assertMeta(t, doc, MetaTags{
					Title:       "Foo | Custom Sitename",
					Description: "Custom Meta Description",
					Image:       "/assets/images/logo.png",
				})
			},
		},
		{
			name: "MetaContent",
			tz:   time.UTC,
			uri:  "/gruppe/0",
			groups: []*group.NewGroup{
				{
					Published:   true,
					Name:        "Foo",
					Description: "Foo bar",
					Image:       "Foobar.png",
				},
			},
			status: 200,
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				assertRobotsIndex(t, doc, "index, follow")
				assertMeta(t, doc, MetaTags{
					Title:       "Foo | Eintopf Stuttgart",
					Description: "Foo bar",
					Image:       "/api/v1/media/Foobar.png",
				})
			},
		},
		{
			name: "PastEvents",
			uri:  "/gruppe/0",
			groups: []*group.NewGroup{
				{
					Published: true,
					Name:      "group1",
				},
				{
					Published: true,
					Name:      "group2",
				},
			},
			events: []*event.NewEvent{
				// Included
				{
					Published:  true,
					Name:       "past",
					Organizers: []string{"id:0"},
					Start:      time.Now().Add(-1 * time.Hour * 24),
				},
				{
					Published:  true,
					Name:       "multiday",
					Organizers: []string{"id:0"},
					Start:      time.Now().Add(-1 * time.Hour * 24 * 5),
					End:        tptr(time.Now().Add(-1 * time.Hour * 24)),
				},
				// Not included
				{
					Published:  true,
					Name:       "another group",
					Organizers: []string{"id:1"},
					Start:      time.Now().Add(-1 * time.Hour * 24),
				},
				{
					Published:  true,
					Name:       "future",
					Organizers: []string{"id:0"},
					Start:      time.Now().Add(time.Hour * 24),
				},
				{
					Published:  false,
					Name:       "not published",
					Organizers: []string{"id:0"},
					Start:      time.Now().Add(-1 * time.Hour * 24),
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertPastEvents(t, doc, []string{"past", "multiday"})
			},
		},
		{
			name: "FutureEvents",
			uri:  "/gruppe/0",
			groups: []*group.NewGroup{
				{
					Published: true,
					Name:      "group1",
				},
				{
					Published: true,
					Name:      "group2",
				},
			},
			events: []*event.NewEvent{
				// Included
				{
					Published:  true,
					Name:       "future",
					Organizers: []string{"id:0"},
					Start:      time.Now().Add(time.Hour * 24),
				},
				{
					Published:  true,
					Name:       "multiday",
					Organizers: []string{"id:0"},
					Start:      time.Now().Add(time.Hour * 24),
					End:        tptr(time.Now().Add(time.Hour * 24 * 5)),
				},
				// Not included
				{
					Published:  true,
					Name:       "another group",
					Organizers: []string{"id:1"},
					Start:      time.Now().Add(time.Hour * 24),
				},
				{
					Published:  true,
					Name:       "past",
					Organizers: []string{"id:0"},
					Start:      time.Now().Add(-1 * time.Hour * 24),
				},
				{
					Published:  false,
					Name:       "not published",
					Organizers: []string{"id:0"},
					Start:      time.Now().Add(time.Hour * 24),
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertFutureEvents(t, doc, []string{"future", "multiday"})
			},
		},
		{
			name: "Schema.org",
			tz:   time.UTC,
			uri:  "/gruppe/0",
			groups: []*group.NewGroup{
				{
					Published:   true,
					Name:        "Foo",
					Description: "Foo bar",
				},
			},
			status: 200,
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				groupMicrodata := microdata.NewItem()
				groupMicrodata.AddType("https://schema.org/Organization")
				groupMicrodata.AddString("name", "Foo")
				groupMicrodata.AddString("description", "Foo bar")

				wantMicrodata := microdata.NewMicrodata()
				wantMicrodata.AddItem(groupMicrodata)

				assertMicroData(tt, doc, wantMicrodata)
			},
		},
	}

	testRoutes(t, tests)
}
