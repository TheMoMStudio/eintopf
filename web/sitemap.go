// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"context"
	"encoding/xml"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"

	"eintopf.info/service/search"
)

func (renderer *Renderer) renderSitemap(w http.ResponseWriter, r *http.Request) {
	urls := renderer.staticSitemapURLs()
	urls = append(urls, renderer.dynamicSitemapURLs(r.Context())...)
	sitemap := UrlSet{Url: urls, Xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9"}
	content, _ := xml.MarshalIndent(sitemap, "", " ")
	var xmlHeader = []byte(xml.Header)
	w.Header().Add("Content-Type", "application/xml")
	w.Header().Add("Content-Disposition", "inline; filename=sitemap.xml")
	_, err := w.Write(append(xmlHeader, content...))

	if err != nil {
		fmt.Println(err)
	}
}

type Url struct {
	XMLName xml.Name `xml:"url"`
	Loc     string   `xml:"loc"`
}

type UrlSet struct {
	XMLName xml.Name `xml:"urlset"`
	Xmlns   string   `xml:"xmlns,attr"`
	Url     []Url    `xml:"url"`
}

const timeFormat = "2006-01-02"

var staticRoutes = []Url{
	{
		Loc: "",
	},
	{
		Loc: "/gruppen",
	},
	{
		Loc: "/orte",
	},
	{
		Loc: "/about",
	},
}

func (renderer *Renderer) staticSitemapURLs() []Url {
	var urls []Url
	for _, route := range staticRoutes {
		url, err := url.JoinPath(renderer.baseURL, route.Loc)
		if err != nil {
			log.Printf("could not join path: %s", err)
		}
		urls = append(urls, Url{
			Loc: strings.TrimRight(url, "/"),
		})
	}
	return urls
}

func (renderer *Renderer) dynamicSitemapURLs(ctx context.Context) []Url {
	var urls []Url
	entries, err := renderer.search.Search(ctx, &search.Options{
		Sort: "type",
		Filters: []search.Filter{
			&search.BoolFilter{Field: "published", Value: true},
		},
	})
	if err != nil {
		log.Printf("could not find entries!")
		return nil
	}
	for _, entry := range entries.Hits {
		// skip all multi day events, excluding the original one
		if strings.Contains(entry.ID, "_") {
			continue
		}
		var routeName string
		switch entry.Type {
		case "group":
			routeName = "gruppe"
		case "place":
			routeName = "ort"
		case "event":
			routeName = "event"
		}
		if routeName == "" {
			log.Printf("sitemap: unknown entry type %s", entry.Type)
			continue
		}
		url, err := url.JoinPath(renderer.baseURL, routeName, entry.ID)
		if err != nil {
			log.Printf("sitemap: could not generate sitemap url for entry %s", entry.ID)
			continue
		}
		urls = append(urls, Url{
			Loc: url,
		})
	}
	return urls
}
