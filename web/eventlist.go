//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/goodsign/monday"

	"eintopf.info/internal/crud"
	"eintopf.info/internal/xhttp"
	"eintopf.info/internal/xtime"
	"eintopf.info/service/eventsearch"
	"eintopf.info/service/revent"
	"eintopf.info/service/search"
	"eintopf.info/service/taxonomy"
)

// EventListPage renders the eventlist page.
func (renderer *Renderer) EventListPage(w http.ResponseWriter, r *http.Request) {
	options := eventListOptionsFromRequest(r, renderer.tz)
	pages, err := renderer.getEventListPagination(r.Context(), xtime.Date(time.Now()), options)
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}
	options = options.setPages(pages)

	result, err := renderer.eventSearch.Search(r.Context(), options.eventSearchOptions(true))
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}

	daysWithEvents, ok := result.Buckets["day"].(search.TermsBucket)
	if !ok {
		renderer.errorPage(w, r, fmt.Errorf("invalid day aggregation"))
		return
	}
	tags, ok := result.Buckets["tags"].(search.TermsBucket)
	if !ok {
		renderer.errorPage(w, r, fmt.Errorf("invalid tags aggregation"))
		return
	}

	categories, _, err := renderer.categoryStore.Find(r.Context(), &crud.FindParams[taxonomy.Filters]{Sort: "name", Order: "ASC"})
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}

	topics, _, err := renderer.topicStore.Find(r.Context(), &crud.FindParams[taxonomy.Filters]{Sort: "name", Order: "ASC"})
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}

	err = renderer.renderPage(w, r, "eventlist", map[string]interface{}{
		"SearchAction": options.URL(),

		"Options": options,

		"Today": time.Now(),

		"Days": groupEventsByDays(result.Events),
		"Tags": tags,

		"Calendar": calendarMonth(options.Calendar.Year(), options.Calendar.Month(), daysWithEvents, time.UTC),

		"Categories": categories,
		"Topics":     topics,
	}, nil)
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}
}

// pageSize definesthe minimum number of events per page.
const pageSize = 15

// Page is a range of days. A page contains a minimum of pageSize events.
type Page struct {
	// Start date of the page
	Start time.Time `json:"start"`
	// End date of the page
	End time.Time `json:"end"`
}

func (p Page) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`{"start": "%s","end":"%s"}`, p.Start.Format("2006-01-02"), p.End.Format("2006-01-02"))), nil
}

// Algorithm for pagniated eventlist:
//
// Problems:
// - multiday events
// - page ends in the middle of the day when paginating with fixed page size
func (renderer *Renderer) getEventListPagination(ctx context.Context, dateMin time.Time, options eventListOptions) ([]Page, error) {
	result, err := renderer.eventSearch.Search(ctx, eventsearch.Options{
		Query:    options.Query,
		Sort:     "",
		Page:     0,
		PageSize: 1,
		Aggregations: eventsearch.Aggregations{
			"day": eventsearch.DayTermsAggregation{
				Filters: eventsearch.Filters{
					eventsearch.ListedFilter{Listed: true},
					eventsearch.TagsFilter{Tags: options.Tags},
					eventsearch.CategorysFilter{Categorys: options.Categories},
					eventsearch.TopicsFilter{Topics: options.Topics},
					eventsearch.DateRangeFilter{DateMin: dateMin},
				},
			},
		},
	})
	if err != nil {
		return nil, err
	}

	rawDays, ok := result.Buckets["day"].(search.TermsBucket)
	if !ok {
		return nil, fmt.Errorf("invalid day aggregation")
	}

	type day struct {
		Date   time.Time
		Events int
	}

	days := []day{}

	for _, d := range rawDays {
		date, err := time.Parse(time.RFC3339, d.Term)
		if err != nil {
			return nil, fmt.Errorf("failed to parse date '%s': %s", d.Term, err)
		}
		days = append(days, day{Date: date, Events: d.Count})
	}

	pages := []Page{}
	i := 0
	start := time.Time{}
	for _, d := range days {
		if start.IsZero() {
			start = d.Date
		}

		i += d.Events
		if i >= pageSize {
			i = 0
			pages = append(pages, Page{Start: start, End: d.Date})
			start = time.Time{}
		}
	}
	if i > 0 {
		pages = append(pages, Page{Start: start, End: days[len(days)-1].Date})
	}

	return pages, nil
}

const partialEventListErrorMsg = "Weitere Events konnten nicht automatisch geladen werden, nutze bitte die Buttons um mehr Events zu sehen."

// PartialEventList renders a paginated part of the eventlist.
// This is used by the infinite scroll javascript.
func (renderer *Renderer) PartialEventList(w http.ResponseWriter, r *http.Request) {
	options := eventListOptionsFromRequest(r, renderer.tz)
	result, err := renderer.eventSearch.Search(r.Context(), options.eventSearchOptions(false))
	if err != nil {
		renderer.errorPartial(w, r, err, partialEventListErrorMsg)
		return
	}

	err = renderer.Render(r.Context(), w, []string{"partial/eventlist"}, map[string]any{
		"Options": options,
		"Days":    groupEventsByDays(result.Events),
	}, nil, true)
	if err != nil {
		renderer.errorPartial(w, r, err, partialEventListErrorMsg)
		return
	}
}

// PartialCalendar renders the calendar container
// This is used to reload the calendar widget with javascript.
func (renderer *Renderer) PartialCalendar(w http.ResponseWriter, r *http.Request) {
	options := eventListOptionsFromRequest(r, renderer.tz)

	result, err := renderer.eventSearch.Search(r.Context(), options.eventSearchOptions(true))
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}
	datesWithEvents, ok := result.Buckets["day"].(search.TermsBucket)
	if !ok {
		renderer.errorPage(w, r, fmt.Errorf("invalid day aggregation"))
		return
	}

	err = renderer.Render(r.Context(), w, []string{"partial/calendar"}, map[string]any{
		"Today":    time.Now(),
		"Options":  options,
		"Calendar": calendarMonth(options.Calendar.Year(), options.Calendar.Month(), datesWithEvents, time.UTC),
	}, nil, true)
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}
}

// eventListOptions are options that change the result of the event list page.
type eventListOptions struct {
	Query      string    // Query defines a search query.
	Date       time.Time // Date is the currently selected date.
	MaxDate    time.Time
	MinDate    time.Time
	Calendar   time.Time // Calendar defines the selected month on the calendar picker.
	Categories []string
	Topics     []string
	Tags       []string // Tags is a list of tags, that filter the event list.
	Page       int      // Page is the current page. The fist page starts at 0.
	Pages      []Page
	repeating  *bool

	originalDate time.Time // originalDate keeps track of the date as set in the request.

	tz *time.Location
}

// eventListOptionsFromRequest takes an http request object to create a new eventListOptions.
// A valid request uri might look light this:
//   - /?date=2006-01-02&calendar=2006-01&tags=foo&tags=bar
//
// The function is best effort, all errors are ignored.
func eventListOptionsFromRequest(r *http.Request, tz *time.Location) eventListOptions {
	date, _ := xhttp.ReadQueryTime(r, "date", "2006-01-02", time.Time{}, time.UTC)
	dateMin, _ := xhttp.ReadQueryTime(r, "dateMin", "2006-01-02", time.Time{}, time.UTC)
	dateMax, _ := xhttp.ReadQueryTime(r, "dateMax", "2006-01-02", time.Time{}, time.UTC)
	calendar, err := xhttp.ReadQueryTime(r, "calendar", "2006-01", xtime.Date(time.Now()), time.UTC)
	if err != nil {
		calendar = xtime.Date(time.Now())
	}
	repeating, _ := xhttp.ReadQueryBool(r, "repeating")

	page, err := xhttp.ReadQueryInt(r, "page")
	return eventListOptions{
		Query:        r.URL.Query().Get("query"),
		Date:         date,
		MinDate:      dateMin,
		MaxDate:      dateMax,
		Calendar:     calendar,
		Tags:         r.URL.Query()["tags"],
		Page:         page,
		originalDate: date,
		tz:           tz,
		repeating:    repeating,
	}
}

// eventSearchOptions converts eventListOptions to eventsearch.Options
func (e eventListOptions) eventSearchOptions(aggregate bool) eventsearch.Options {
	var aggregations eventsearch.Aggregations
	if aggregate {
		aggregations = eventsearch.Aggregations{
			"tags": eventsearch.TagTermsAggregation{
				Filters: eventsearch.Filters{
					eventsearch.ListedFilter{Listed: true},
					eventsearch.DateRangeFilter{
						DateMin: e.DateMin(false),
						DateMax: e.DateMax(false),
					},
					eventsearch.CategorysFilter{Categorys: e.Categories},
					eventsearch.TopicsFilter{Topics: e.Topics},
				},
			},
			"day": eventsearch.DayTermsAggregation{
				Filters: eventsearch.Filters{
					eventsearch.ListedFilter{Listed: true},
					eventsearch.TagsFilter{Tags: e.Tags},
					eventsearch.CategorysFilter{Categorys: e.Categories},
					eventsearch.TopicsFilter{Topics: e.Topics},
				},
			},
		}
	}

	filters := []eventsearch.Filter{
		eventsearch.ListedFilter{Listed: true},
		eventsearch.DateRangeFilter{
			DateMin: e.DateMin(true),
			DateMax: e.DateMax(true),
		},
		eventsearch.TagsFilter{Tags: e.Tags},
		eventsearch.CategorysFilter{Categorys: e.Categories},
		eventsearch.TopicsFilter{Topics: e.Topics},
		eventsearch.MultidayRangeFilter{MultidayMin: iptr(-1)},
	}

	if e.repeating != nil {
		filters = append(filters, eventsearch.RepeatingFilter{Repeating: *e.repeating})
	}

	return eventsearch.Options{
		Query: e.Query,
		Sort:  "date",
		Filters: filters,
		Aggregations: aggregations,
	}
}

// ShouldIndex returns false if any default value was changed or if more than two tags were set.
func (e eventListOptions) ShouldIndex() bool {
	if e.Query != "" {
		return false
	}
	if !e.CalendarIsDefault() {
		return false
	}
	if !e.DateIsDefault() {
		return false
	}
	if !e.MinDate.IsZero() {
		return false
	}
	if !e.MaxDate.IsZero() {
		return false
	}
	if e.Page > 0 {
		return false
	}
	return len(e.Tags) <= 1
}

func (e eventListOptions) setPages(pages []Page) eventListOptions {
	e.Pages = pages
	return e
}

// DateMin returns the start of the current date range.
func (e eventListOptions) DateMin(pagination bool) time.Time {
	if !e.MinDate.IsZero() {
		return xtime.InLocation(xtime.StartOfDay(e.MinDate), e.tz)
	}
	if !e.Date.IsZero() {
		return xtime.InLocation(xtime.StartOfDay(e.Date), e.tz)
	}
	if pagination && e.Pages != nil && len(e.Pages) > e.Page {
		page := e.Pages[e.Page]
		return xtime.InLocation(xtime.StartOfDay(page.Start), e.tz)
	}
	return xtime.InLocation(xtime.StartOfDay(time.Now()), e.tz)
}

// DateMax returns the end of the current date range.
func (e eventListOptions) DateMax(pagination bool) time.Time {
	if !e.MaxDate.IsZero() {
		return xtime.InLocation(xtime.EndOfDay(e.MaxDate), e.tz)
	}
	if !e.Date.IsZero() {
		return xtime.InLocation(xtime.EndOfDay(e.Date), e.tz)
	}
	if pagination && e.Pages != nil && len(e.Pages) > e.Page {
		page := e.Pages[e.Page]
		return xtime.InLocation(xtime.EndOfDay(page.End), e.tz)
	}
	return time.Time{}
}

// ToggleDate sets the date option. If the date is the same as the current
// date, set the date to now.
// This resets the page to 0.
// It returns a new copy of options.
func (e eventListOptions) ToggleDate(date time.Time) eventListOptions {
	if sameDay(date, e.Date) {
		e.Date = time.Now()
	} else {
		e.Date = date
	}
	e.Page = 0
	return e
}

// DateIsDefault returns true, when the date is zero or today.
func (e eventListOptions) DateIsDefault() bool {
	return e.Date.IsZero() || sameDay(time.Now(), e.Date)
}

// SetCalendar sets the calendar option and returns a new copy of options.
// This resets the page to 0.
func (e eventListOptions) SetCalendar(calendar time.Time) eventListOptions {
	e.Page = 0
	e.Calendar = calendar
	return e
}

// CalendarIsDefault returns true, when the date is zero or in the current month.
func (e eventListOptions) CalendarIsDefault() bool {
	return e.Calendar.IsZero() || sameMonth(e.Calendar, time.Now())
}

// PrevCalendar sets the calendar to the previous month and returns a copy of
// options.
func (e eventListOptions) PrevCalendar() eventListOptions {
	e.Calendar = e.Calendar.AddDate(0, -1, 0)
	return e
}

// NextCalendar sets the calendar to the next month and returns a copy of options.
func (e eventListOptions) NextCalendar() eventListOptions {
	e.Calendar = e.Calendar.AddDate(0, 1, 0)
	return e
}

func (e eventListOptions) SetCategory(category string) eventListOptions {
	e.Categories = []string{category}
	return e
}

func (e eventListOptions) SetTopic(topic string) eventListOptions {
	e.Topics = []string{topic}
	return e
}

// SetTag sets the tag option and returns a new copy of options.
// This resets the page to 0.
func (e eventListOptions) SetTag(tag string) eventListOptions {
	e.Page = 0
	e.Tags = []string{tag}
	return e
}

// ToggleTag adds/removes the given tag from the tag option and returns a new
// copy of options.
// This resets the page to 0.
func (e eventListOptions) ToggleTag(tag string) eventListOptions {
	e.Page = 0
	tags := make([]string, len(e.Tags))
	copy(tags, e.Tags)
	for i, t := range tags {
		if t == tag {
			e.Tags = append(tags[:i], tags[i+1:]...)
			return e
		}
	}
	e.Tags = append(tags, tag)
	return e
}

// ToggleOnlyRepeatingFilter adds/removes the only repeating filter option
// and returns a new copy of options.
// This resets the page to 0.
func (e eventListOptions) ToggleOnlyRepeatingFilter() eventListOptions {
	e.Page = 0

	if !e.HasOnlyRepeatingFilter() {
		// activate filter to show only repeating events
		value := true
		e.repeating = &value
	} else {
		// set to default --> deactivate repeating filter
		e.repeating = nil
	}
	return e
}

// ToggleNoRepeatingFilter adds/removes the no repeating filter option
// and returns a new copy of options.
// This resets the page to 0.
func (e eventListOptions) ToggleNoRepeatingFilter() eventListOptions {
	e.Page = 0

	if !e.HasNoRepeatingFilter() {
		// activate filter to show only events that are not repeating
		value := false
		e.repeating = &value
	} else {
		// set to default --> deactivate repeating filter
		e.repeating = nil
	}
	return e
}

// HasOnlyRepeatingFilter returns true, if the only repeating
// events filter is set.
func (e eventListOptions) HasOnlyRepeatingFilter() bool {
	if(e.repeating!=nil){
		return *e.repeating
	}
	return false
}

// HasOnlyRepeatingFilter returns true, if the no repeating
// events filter is set.
func (e eventListOptions) HasNoRepeatingFilter() bool {
	if(e.repeating!=nil){
		return !*e.repeating
	}
	return false
}

// HasTag returns true, if the given tag is set.
func (e eventListOptions) HasTag(tag string) bool {
	for _, t := range e.Tags {
		if t == tag {
			return true
		}
	}
	return false
}

// PrevPage decreases the page by one and returns a new copy of options.
func (e eventListOptions) PrevPage() eventListOptions {
	e.Page -= 1
	return e
}

func (e eventListOptions) HasPrevPage() bool {
	if e.Pages == nil {
		return false
	}
	return e.Page > 0
}

// NextPage increases the page by one and returns a new copy of options.
func (e eventListOptions) NextPage() eventListOptions {
	e.Page += 1
	return e
}

func (e eventListOptions) HasNextPage() bool {
	if e.Pages == nil {
		return false
	}
	return e.Page < len(e.Pages)-1
}

// URL converts the options to a url.
// It only adds an option value, when it is not its zero/default value.
func (e eventListOptions) URL() string {
	u := url.URL{}
	u.Path = "/"
	q := u.Query()
	if e.Query != "" {
		q.Set("query", e.Query)
	}
	if !e.Date.IsZero() && !sameDay(e.Date, time.Now()) {
		q.Set("date", e.Date.Format("2006-01-02"))
	}
	if !e.CalendarIsDefault() {
		q.Set("calendar", e.Calendar.Format("2006-01"))
	}
	for _, tag := range e.Tags {
		q.Add("tags", tag)
	}
	if e.Page > 0 {
		q.Add("page", strconv.Itoa(e.Page))
	}
	if(e.repeating != nil){
		q.Add("repeating", strconv.FormatBool(*e.repeating) )
	}
	u.RawQuery = q.Encode()
	u.RawQuery, _ = url.QueryUnescape(q.Encode())
	return u.String()
}

// groupEventsByDays maps a list of events to days to a set of days with events.
// Multiday events get inserted at the start of each day.
func groupEventsByDays(events []*eventsearch.EventDocument) map[time.Time][]*eventsearch.EventDocument {
	days := map[time.Time][]*eventsearch.EventDocument{}
	for _, e := range events {
		d := date(e.Date.Year(), e.Date.Month(), e.Date.Day(), time.UTC)

		if days[d] == nil {
			days[d] = []*eventsearch.EventDocument{}
		}
		if e.MultiDay > 0 {
			// Insert multiday events at the beginning of the day
			days[d] = append([]*eventsearch.EventDocument{e}, days[d]...)
		} else {
			days[d] = append(days[d], e)
		}
	}
	return days
}

func date(year int, month time.Month, day int, tz *time.Location) time.Time {
	return time.Date(year, month, day, 0, 0, 0, 0, tz)
}

func sameDay(t1, t2 time.Time) bool {
	return t1.Year() == t2.Year() && t1.Month() == t2.Month() && t1.Day() == t2.Day()
}

func sameMonth(t1, t2 time.Time) bool {
	return t1.Year() == t2.Year() && t1.Month() == t2.Month()
}

type calendarDay struct {
	Date      time.Time // Date of the day
	InMonth   bool      // InMonth is true when the day is in the selected month
	HasEvents bool      // HasEvents is true when there are events for this day
}

// calendarMonth creates a list of days for a given month and year. The days
// will start the monday of the first week of the month and end with the sunday
// of the last week of the month.
func calendarMonth(year int, month time.Month, datesWithEvents search.TermsBucket, tz *time.Location) []calendarDay {
	calendar := make([]calendarDay, 0)

	d := time.Date(year, month, 0, 0, 0, 0, 0, tz)
	// Find first monday from start of the month
	for d.Weekday() != time.Monday {
		d = d.Add(-time.Hour * 24)
	}
	lastMonth := d.Month()

	for d.Month() == lastMonth || d.Month() == month {
		hasEvents := false
		n := date(d.Year(), d.Month(), d.Day(), time.UTC)
		dateString := n.Format(search.DateLayout)
		for _, dateWithEvent := range datesWithEvents {
			if dateWithEvent.Term == dateString {
				hasEvents = true
				break
			}
		}
		calendar = append(calendar, calendarDay{
			Date:      d,
			InMonth:   month == d.Month(),
			HasEvents: hasEvents,
		})
		d = d.Add(time.Hour * 24)
	}

	for d.Weekday() != time.Monday {
		calendar = append(calendar, calendarDay{
			Date:      d,
			InMonth:   false,
			HasEvents: false,
		})
		d = d.Add(time.Hour * 24)
	}

	return calendar
}

func formatInterval(interval revent.Interval) string {
	switch interval.Type {
	case revent.IntervalDay:
		if interval.Interval == 1 {
			return "Jeden Tag"
		}
		return fmt.Sprintf("Jeden %s Tag", repeatingToString(interval.Interval, true))
	case revent.IntervalWeek:
		if interval.Interval == 1 {
			return fmt.Sprintf("Jede Woche %s", weekDayString(interval.WeekDay))
		}
		return fmt.Sprintf("Jede %s Woche %s", repeatingToString(interval.Interval, false), weekDayString(interval.WeekDay))
	case revent.IntervalMonth:
		return fmt.Sprintf("Jeden %s %s im Monat", repeatingToString(interval.Interval, true), weekDayString(interval.WeekDay))
	default:
		return ""
	}
}

func weekDayString(weekDay time.Weekday) string {
	// Convert weekday format to german convention.
	if weekDay == 0 {
		weekDay = 6
	} else {
		weekDay -= 1
	}
	return monday.GetLongDays(monday.LocaleDeDE)[weekDay]
}

func repeatingToString(interval int, suffixN bool) string {
	str := ""
	switch interval {
	case 1:
		str = "erste"
	case 2:
		str = "zweite"
	case 3:
		str = "dritte"
	case 4:
		str = "vierte"
	case 5:
		str = "fünfte"
	case 6:
		str = "sechste"
	case 7:
		str = "siebte"
	}
	if suffixN {
		return str + "n"
	}
	return str
}
