//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"context"
	"regexp"
	"strings"
	"testing"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/google/go-cmp/cmp"
	"github.com/iand/microdata"

	"eintopf.info/service/event"
	"eintopf.info/service/group"
	"eintopf.info/service/place"
	"eintopf.info/service/revent"
)

func TestEventPage(t *testing.T) {
	tests := []routeTest{
		{
			name: "linkDisplay",
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "linkDisp",
					Location:  "bar",
					Description: `EVENTBESCHREIBUNG https://www.staatsgalerie.de/de\n adsadsad
					sdadssadl dasd https://eintopf.info/ d sadad sad d,
					sd https://www.diesisteinmegalangerteststringdersolangistdasserabgekuerztwerdenmusssadasdsaadsasdsadasdasdasdasdsaddddda.de
					sadasdasd dddsadsd
									s`,
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				expectedRef := []string{"https://www.staatsgalerie.de/de", "https://eintopf.info/",
					"https://www.diesisteinmegalangerteststringdersolangistdasserabgekuerztwerdenmusssadasdsaadsasdsadasdasdasdasdsaddddda.de"}
				expectedLinkText := []string{"https://www.staatsgalerie.de/de", "https://eintopf.info/",
					"https://www.diesisteinmegalangerteststringdersolangistdasserabgekuerztwerdenmusssadasdsaadsasdsadasd..."}
				assertLinkInDesc(t, doc, expectedRef, expectedLinkText)
			},
		},
		{
			name: "NotFound",
			tz:   time.UTC,
			uri:  "/event/12345",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
				},
			},
			status: 404,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertNodes(t, doc, ".error", []string{"404 Veranstaltung wurde nicht gefunden"})
			},
		},
		{
			name: "NotPublished",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published: false,
					Name:      "foo",
				},
			},
			status: 200,
		},
		{
			name: "MetaNoIndexNotPublished",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published: false,
					Name:      "foo",
				},
			},
			status: 200,
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				assertRobotsIndex(t, doc, "noindex, nofollow")
			},
		},
		{
			name: "MetaIndexPublished",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
				},
			},
			status: 200,
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				assertRobotsIndex(t, doc, "index, follow")
			},
		},
		{
			name:      "MetaDefault",
			tz:        time.UTC,
			uri:       "/event/0",
			theme:     "meta",
			themePath: "testdata/themes/",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "Foo",
				},
			},
			status: 200,
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				assertRobotsIndex(t, doc, "index, follow")
				assertMeta(t, doc, MetaTags{
					Title:       "Foo | Custom Sitename",
					Description: "Custom Meta Description",
					Image:       "/assets/images/logo.png",
				})
			},
		},
		{
			name: "MetaContent",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published:   true,
					Name:        "Foo",
					Description: "Foo bar",
					Image:       "Foobar.png",
				},
			},
			status: 200,
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				assertRobotsIndex(t, doc, "index, follow")
				assertMeta(t, doc, MetaTags{
					Title:       "Foo | Eintopf Stuttgart",
					Description: "Foo bar",
					Image:       "/api/v1/media/Foobar.png",
				})
			},
		},
		{
			name: "Schema.org Only Optional Fields",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published:   true,
					Name:        "Foo",
					Description: "Foo bar",
					Start:       time.Date(2022, 10, 5, 14, 30, 0, 0, time.UTC),
				},
			},
			status: 200,
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				eventMicrodata := microdata.NewItem()
				eventMicrodata.AddType("https://schema.org/Event")
				eventMicrodata.AddString("name", "Foo")
				eventMicrodata.AddString("description", "Foo bar")
				eventMicrodata.AddString("startDate", "2022-10-05T14:30:00Z")
				wantMicrodata := microdata.NewMicrodata()
				wantMicrodata.AddItem(eventMicrodata)
				assertMicroData(tt, doc, wantMicrodata)
			},
		},
		{
			name: "Schema.org All Fields",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published:   true,
					Name:        "Foo",
					Description: "Foo bar",
					Start:       time.Date(2022, 10, 5, 14, 30, 0, 0, time.UTC),
					End:         tptr(time.Date(2022, 10, 5, 20, 30, 0, 0, time.UTC)),
					Location:    "bar",
					Organizers:  []string{"dada"},
				},
			},
			status: 200,
			htmlTestFunc: func(tt *testing.T, doc *goquery.Document) {
				placeMicrodata := microdata.NewItem()
				placeMicrodata.AddType("https://schema.org/Place")
				placeMicrodata.AddString("name", "bar")

				eventMicrodata := microdata.NewItem()
				eventMicrodata.AddType("https://schema.org/Event")
				eventMicrodata.AddString("name", "Foo")
				eventMicrodata.AddString("description", "Foo bar")
				eventMicrodata.AddString("startDate", "2022-10-05T14:30:00Z")
				eventMicrodata.AddString("endDate", "2022-10-05T20:30:00Z")
				eventMicrodata.AddItem("location", placeMicrodata)
				eventMicrodata.AddString("organizer", "dada")

				wantMicrodata := microdata.NewMicrodata()
				wantMicrodata.AddItem(eventMicrodata)

				assertMicroData(tt, doc, wantMicrodata)
			},
		},
		{
			name: "Canceled",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
				},
			},
			transform: func(eventService event.Storer, groupService group.Service, placeService place.Service) error {
				_, err := eventService.Update(context.Background(), &event.Event{
					ID:        "0",
					Published: true,
					Canceled:  true,
					Name:      "foo",
				})
				return err
			},
			status: 200,
			contentTestFunc: func(t *testing.T, content string) {
				if !strings.Contains(content, "Fällt aus") {
					t.Error("should contain 'Fällt aus'")
				}
			},
		},
		{
			name: "NoEndDate",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
					Start:     time.Date(2022, 10, 5, 14, 30, 0, 0, time.UTC),
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertInfoDate(t, doc, "Mi. 05.10.2022 14:30 Uhr")
			},
		},
		{
			name: "WithEndDateSameDay",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
					Start:     time.Date(2022, 10, 5, 14, 30, 0, 0, time.UTC),
					End:       tptr(time.Date(2022, 10, 5, 16, 30, 0, 0, time.UTC)),
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertInfoDate(t, doc, "Mi. 05.10.2022 14:30 Uhr - 16:30 Uhr")
			},
		},
		{
			name: "WithEndDateNotSameDay",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
					Start:     time.Date(2022, 10, 5, 14, 30, 0, 0, time.UTC),
					End:       tptr(time.Date(2022, 10, 7, 16, 30, 0, 0, time.UTC)),
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertInfoDate(t, doc, "Mi. 05.10.2022 14:30 Uhr - Fr. 07.10.2022 16:30 Uhr")
			},
		},
		{
			name: "NoOrganizers",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published:  true,
					Name:       "foo",
					Organizers: []string{},
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertInfoOrganizers(t, doc, "")
			},
		},
		{
			name: "OneOrganizer",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published:  true,
					Name:       "foo",
					Organizers: []string{"dada"},
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertInfoOrganizers(t, doc, "dada")
			},
		},
		{
			name: "MultipleOrganizer",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published:  true,
					Name:       "foo",
					Organizers: []string{"dada", "baba"},
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertInfoOrganizers(t, doc, "dada, baba")
			},
		},
		{
			name: "Involved",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
					Involved:  []event.Involved{{Name: "foo", Description: "descfoo"}, {Name: "bar", Description: "descbar"}},
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				elementsName := doc.Find(`.info-involved .info-data`)
				elementsDescription := doc.Find(`.description`)
				if elementsName.Length() != 2 {
					t.Errorf(`Expected 2 involved, got %d`, elementsName.Length())
				}
				if diff := cmp.Diff("foobar", elementsName.Text()); diff != "" {
					t.Errorf(`Involved content mismatch (-want +got):\n%s`, diff)
				}
				if elementsDescription.Length() != 2 {
					t.Errorf(`Expected 2 involved descriptions, got %d`, elementsDescription.Length())
				}
				if diff := cmp.Diff("descfoodescbar", elementsDescription.Text()); diff != "" {
					t.Errorf(`Involved description content mismatch (-want +got):\n%s`, diff)
				}
			},
		},
		{
			name: "Location",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
					Location:  "bar",
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertInfoLocation(t, doc, "bar", "", "")
			},
		},
		{
			name: "LocationAndLocation2",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
					Location:  "bar",
					Location2: "bar2",
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertInfoLocation(t, doc, "bar", "bar2", "")
			},
		},
		{
			name: "LocationAndLocation2AndAddress",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
					Location:  "bar",
					Location2: "bar2",
					Address:   "bar3",
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertInfoLocation(t, doc, "bar", "bar2", "bar3")
			},
		},
		{
			name: "LocationAndAddress",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
					Location:  "bar",
					Address:   "bar3",
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertInfoLocation(t, doc, "bar", "", "bar3")
			},
		},
		{
			name: "Children",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
				},
				{
					Parent:    "id:0",
					Published: true,
					Name:      "bar",
					Start:     time.Date(2022, 9, 8, 10, 0, 0, 0, time.UTC),
				},
				{
					Parent:    "id:0",
					Published: true,
					Name:      "baz",
					Start:     time.Date(2022, 9, 9, 10, 0, 0, 0, time.UTC),
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertNodes(t, doc, "h2", []string{"Programm 08. Sep / Donnerstag", "Programm 09. Sep / Freitag"})
			},
		},
		{
			name: "WithMap",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
					Location:  "id:0",
				},
			},
			places: []*place.NewPlace{
				{
					Published: true,
					Name:      "bar",
					Lat:       1,
					Lng:       2,
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertMap(t, doc)
			},
		},
		{
			name: "WithMap own location",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
					Location:  "bar",
					Lat:       1,
					Lng:       2,
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertMap(t, doc)
			},
		},
		{
			name: "WithoutMap NoLocation",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertNoMap(t, doc)
			},
		},
		{
			name: "WithoutMap NoPlace",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
					Location:  "bar",
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertNoMap(t, doc)
			},
		},
		{
			name: "WithoutMap NoLatLng",
			tz:   time.UTC,
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published: true,
					Name:      "foo",
					Location:  "id:0",
				},
			},
			places: []*place.NewPlace{
				{
					Published: true,
					Name:      "bar",
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertNoMap(t, doc)
			},
		},
		{
			name: "PastEvents",
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published:  true,
					Name:       "my event",
					Organizers: []string{"id:0"},
				},
				// Included
				{
					Published:  true,
					Name:       "past",
					Organizers: []string{"id:0"},
					Start:      time.Now().Add(-1 * time.Hour * 24),
				},
				{
					Published:  true,
					Name:       "multiday",
					Organizers: []string{"id:0"},
					Start:      time.Now().Add(-1 * time.Hour * 24),
					End:        tptr(time.Now().Add(-1 * time.Hour * 24 * 4)),
				},
				// Not included
				{
					Published:  true,
					Name:       "another group",
					Organizers: []string{"id:1"},
					Start:      time.Now().Add(-1 * time.Hour * 24),
				},
				{
					Published:  true,
					Name:       "future",
					Organizers: []string{"id:0"},
					Start:      time.Now().Add(time.Hour * 24),
				},
				{
					Published:  false,
					Name:       "not published",
					Organizers: []string{"id:0"},
					Start:      time.Now().Add(-1 * time.Hour * 24),
				},
			},
			groups: []*group.NewGroup{
				{
					Published: true,
					Name:      "group1",
				},
				{
					Published: true,
					Name:      "group2",
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertPastEvents(t, doc, []string{"past", "multiday"})
			},
		},
		{
			name: "FutureEvents",
			uri:  "/event/0",
			events: []*event.NewEvent{
				{
					Published:  true,
					Name:       "my event",
					Organizers: []string{"id:0"},
				},
				// Included
				{
					Published:  true,
					Name:       "future",
					Organizers: []string{"id:0"},
					Start:      time.Now().Add(time.Hour * 24 * 2),
				},
				{
					Published:  true,
					Name:       "multiday",
					Organizers: []string{"id:0"},
					Start:      time.Now().Add(time.Hour * 24 * 2),
					End:        tptr(time.Now().Add(time.Hour * 24 * 4)),
				},
				// Not inlcuded
				{
					Published:  true,
					Name:       "another group",
					Organizers: []string{"id:1"},
					Start:      time.Now().Add(-1 * time.Hour * 24),
				},
				{
					Published:  true,
					Name:       "past",
					Organizers: []string{"id:0"},
					Start:      time.Now().Add(-1 * time.Hour * 24),
				},
				{
					Published:  false,
					Name:       "not published",
					Organizers: []string{"id:0"},
					Start:      time.Now().Add(time.Hour * 24 * 2),
				},
			},
			groups: []*group.NewGroup{
				{
					Published: true,
					Name:      "group1",
				},
				{
					Published: true,
					Name:      "group2",
				},
			},
			status: 200,
			htmlTestFunc: func(t *testing.T, doc *goquery.Document) {
				assertFutureEvents(t, doc, []string{"future", "multiday"})
			},
		},
	}

	testRoutes(t, tests)
}

func assertInfoDate(t *testing.T, doc *goquery.Document, date string) {
	t.Helper()
	if diff := cmp.Diff(1, len(doc.Find(".info-date").Nodes)); diff != "" {
		t.Errorf(".info-date mismatch (-want +got):\n%s", diff)
	}
	if diff := cmp.Diff(date, removeWhiteSpace(doc.Find(".info-date .info-data").Text())); diff != "" {
		t.Errorf(".info-date .info-data mismatch (-want +got):\n%s", diff)
	}
}

func assertInfoOrganizers(t *testing.T, doc *goquery.Document, organizers string) {
	t.Helper()
	if organizers == "" {
		if diff := cmp.Diff(0, len(doc.Find(".info-organizers").Nodes)); diff != "" {
			t.Errorf(".info-organizers mismatch (-want +got):\n%s", diff)
		}
	} else {
		if diff := cmp.Diff(1, len(doc.Find(".info-organizers").Nodes)); diff != "" {
			t.Errorf(".info-organizers mismatch (-want +got):\n%s", diff)
		}
		if diff := cmp.Diff(organizers, removeWhiteSpace(doc.Find(".info-organizers .info-data").Text())); diff != "" {
			t.Errorf(".info-organizers .info-data mismatch (-want +got):\n%s", diff)
		}
	}
}

func assertInfoLocation(t *testing.T, doc *goquery.Document, location string, location2 string, address string) {
	t.Helper()
	if location != "" {
		if diff := cmp.Diff(1, len(doc.Find(".info-location").Nodes)); diff != "" {
			t.Errorf(".info-location nodes mismatch (-want +got):\n%s", diff)
		}
		exp := location
		if address != "" {
			exp = exp + ", " + address
		}
		if location2 != "" {
			exp = exp + ", " + location2
		}

		if diff := cmp.Diff(exp, doc.Find(".info-location .info-data").Text()); diff != "" {
			t.Errorf(".info-location .info-data mismatch (-want +got):\n%s", diff)
		}
	}
}

func removeWhiteSpace(s string) string {
	noLineBreaks := strings.ReplaceAll(s, "\n", "")
	noTabs := strings.ReplaceAll(noLineBreaks, "    ", "")
	space := regexp.MustCompile(`\s+`)
	return strings.TrimSpace(space.ReplaceAllString(noTabs, " "))
}

func tptr(t time.Time) *time.Time {
	return &t
}

func TestFormartInterval(t *testing.T) {
	tests := []struct {
		interval revent.Interval
		want     string
	}{
		{
			interval: revent.Interval{
				Type:     revent.IntervalDay,
				Interval: 1,
			},
			want: "Jeden Tag",
		},
		{
			interval: revent.Interval{
				Type:     revent.IntervalDay,
				Interval: 2,
			},
			want: "Jeden zweiten Tag",
		},
		{
			interval: revent.Interval{
				Type:     revent.IntervalWeek,
				Interval: 1,
				WeekDay:  time.Monday,
			},
			want: "Jede Woche Montag",
		},
		{
			interval: revent.Interval{
				Type:     revent.IntervalWeek,
				Interval: 2,
				WeekDay:  time.Monday,
			},
			want: "Jede zweite Woche Montag",
		},
		{
			interval: revent.Interval{
				Type:     revent.IntervalMonth,
				Interval: 1,
				WeekDay:  time.Monday,
			},
			want: "Jeden ersten Montag im Monat",
		},
	}
	for _, tc := range tests {
		if formatInterval(tc.interval) != tc.want {
			t.Errorf("want: %s, got: %s", tc.want, formatInterval(tc.interval))
		}
	}
}

func stringContains(slice []string, item string) bool {
	set := make(map[string]struct{}, len(slice))
	for _, s := range slice {
		set[s] = struct{}{}
	}

	_, ok := set[item]
	return ok
}
