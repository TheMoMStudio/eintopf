//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"net/http"
	"net/url"
)

// BackstagePage renders the backstage page
func (renderer *Renderer) BackstagePage(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "public")
	apiURL, _ := url.JoinPath(renderer.baseURL, "/api/v1")
	err := renderer.renderPage(w, r, "backstage", map[string]any{
		"ApiURL":   apiURL,
		"Timezone": renderer.tz.String(),
	}, nil)
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}
}
