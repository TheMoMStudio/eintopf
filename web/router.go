//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"fmt"
	"io/fs"
	"net/http"
	"strings"
	"time"

	"eintopf.info/internal/plausible"
	"eintopf.info/internal/xerror"
	"eintopf.info/internal/xhttp"
	"eintopf.info/internal/xtime"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

// Routes adds routes to the given router that serve the web frontend.
func Routes(renderer *Renderer, plausibleClient *plausible.Client) func(r chi.Router) {
	todayLastModified := todayLastModified{tz: renderer.tz}
	return func(r chi.Router) {
		r.Group(func(r chi.Router) {
			r.Use(plausible.Middleware(plausibleClient))
			r.With(xhttp.LastModified(todayLastModified, renderer, renderer.eventSearch)).Get("/", renderer.EventListPage)
			r.With(xhttp.LastModified(todayLastModified, renderer, renderer.eventSearch)).Get("/partial/eventlist/", renderer.PartialEventList)
			r.With(xhttp.LastModified(renderer, renderer.eventSearch)).Get("/partial/calendar/", renderer.PartialCalendar)
			r.With(xhttp.LastModified(todayLastModified, renderer, renderer.eventSearch)).Get("/event/{id}", renderer.EventPage)

			r.With(xhttp.LastModified(renderer, renderer.search)).Get("/gruppe/{id}", renderer.GroupPage)
			r.With(xhttp.LastModified(renderer, renderer.search)).Get("/gruppen", renderer.GroupListPage)

			r.With(xhttp.LastModified(renderer, renderer.search)).Get("/ort/{id}", renderer.PlacePage)
			r.With(xhttp.LastModified(renderer, renderer.search)).Get("/orte", renderer.PlaceListPage)

			r.With(xhttp.LastModified(renderer, renderer.search)).Get("/kategorie/{id}", renderer.CategoryPage)
			r.With(xhttp.LastModified(renderer, renderer.search)).Get("/thema/{id}", renderer.TopicPage)
		})

		r.With(xhttp.LastModified(renderer.theme)).Get("/favicon.ico", renderer.asset)
		r.With(middleware.NoCache).Get("/serviceworker.js", renderer.asset)
		r.With(middleware.NoCache).Get("/robots.txt", renderer.asset)
		r.Get("/robots.txt", func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("Content-Type", "text/plain")
			w.Header().Add("Content-Disposition", "inline; filename=robots.txt")

			err := renderer.renderAsset(w, r, "robots.txt", nil, nil)
			if err == fs.ErrNotExist {
				renderer.errorPage(w, r, xerror.NotFoundError{Err: fmt.Errorf("%s not found", r.URL)})
			}
			if err != nil {
				renderer.errorPage(w, r, err)
			}
		})
		r.Get("/sitemap.xml", renderer.renderSitemap)
		r.With(middleware.NoCache).Get("/manifest.json", renderer.asset)
		r.With(xhttp.LastModified(renderer.theme)).Get("/assets/*", renderer.asset)

		r.With(plausible.Middleware(plausibleClient), xhttp.LastModified(renderer)).Get("/*", func(w http.ResponseWriter, r *http.Request) {
			if strings.Contains(r.URL.Path, ".") {
				// Don't allow "." or ".." in custom urls
				renderer.errorPage(w, r, xerror.NotFoundError{Err: fmt.Errorf("%s not found", r.URL)})
				return
			}

			path := strings.TrimPrefix(r.URL.Path, "/")
			err := renderer.renderPage(w, r, path, nil, nil)
			if err == fs.ErrNotExist {
				renderer.errorPage(w, r, xerror.NotFoundError{Err: fmt.Errorf("%s not found", r.URL)})
			} else if err != nil {
				renderer.errorPage(w, r, err)
			}
		})
	}
}

type todayLastModified struct {
	tz *time.Location
}

func (t todayLastModified) LastModified() time.Time {
	return xtime.StartOfDay(xtime.NowInLocation(t.tz))
}
