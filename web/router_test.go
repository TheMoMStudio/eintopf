//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"eintopf.info"
	"eintopf.info/service/auth"
	"eintopf.info/service/event"
	"eintopf.info/service/eventsearch"
	"eintopf.info/service/group"
	"eintopf.info/service/indexo"
	"eintopf.info/service/oqueue"
	"eintopf.info/service/place"
	"eintopf.info/service/revent"
	"eintopf.info/service/search"
	"eintopf.info/service/taxonomy"
	"eintopf.info/test"

	"github.com/PuerkitoBio/goquery"
	"github.com/go-chi/chi/v5"
)

type routeTest struct {
	name            string
	tz              *time.Location
	uri             string
	events          []*event.NewEvent
	revents         []*revent.NewRepeatingEvent
	groups          []*group.NewGroup
	places          []*place.NewPlace
	categories      []*taxonomy.NewCategory
	topics          []*taxonomy.NewTopic
	transform       func(eventService event.Storer, groupService group.Service, placeService place.Service) error
	status          int
	contentTestFunc func(t *testing.T, content string)
	htmlTestFunc    func(t *testing.T, doc *goquery.Document)
	theme           string
	themePath       string
}

func testRoutes(t *testing.T, tests []routeTest) {
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			index, cleanupIndex := test.CreateBleveTestIndex(tc.name)
			t.Cleanup(cleanupIndex)

			tz := time.UTC
			ctx := auth.ContextWithID(context.Background(), "id")

			searchService, err := search.NewService(index, time.Second, 1, 1, tz)
			if err != nil {
				t.Fatalf("search: %s", err)
			}

			eventService := event.NewService(event.NewMemoryStore(), nil)
			groupService := group.NewService(group.NewMemoryStore())
			placeService := place.NewService(place.NewMemoryStore())
			categoryService := taxonomy.NewCategoryMemoryStore()
			topicService := taxonomy.NewTopicMemoryStore()
			staticTheme, err := eintopf.NewTheme(tc.theme, tc.themePath, nil, nil)
			if err != nil {
				t.Fatal(err)
			}

			queue := oqueue.NewQueue()
			indexoServcice := indexo.NewService(queue, searchService, eventService, groupService, placeService)

			reventService := revent.NewService(revent.NewMemoryStore(), eventService)

			for _, event := range tc.events {
				if _, err := eventService.Create(ctx, event); err != nil {
					t.Fatalf("create event: %s", err)
				}
			}
			for _, e := range tc.revents {
				if _, err := reventService.Create(ctx, e); err != nil {
					t.Fatalf("create revents: %s", err)
				}
			}
			for _, group := range tc.groups {
				if _, err := groupService.Create(ctx, group); err != nil {
					t.Fatalf("create group: %s", err)
				}
			}
			for _, place := range tc.places {
				if _, err := placeService.Create(ctx, place); err != nil {
					t.Fatalf("create place: %s", err)
				}
			}
			for _, category := range tc.categories {
				if _, err := categoryService.Create(ctx, category); err != nil {
					t.Fatalf("create category: %s", err)
				}
			}
			for _, topic := range tc.topics {
				if _, err := topicService.Create(ctx, topic); err != nil {
					t.Fatalf("create topic: %s", err)
				}
			}

			if tc.transform != nil {
				err = tc.transform(eventService, groupService, placeService)
				if err != nil {
					t.Fatalf("transform: %s", err)
				}
			}

			eventsearchService := eventsearch.NewService(searchService, eventService, groupService, placeService, reventService, categoryService, topicService)
			eventsearchService.Reindex(context.Background())
			indexoServcice.Reindex(context.Background())

			r := chi.NewRouter()
			ts := httptest.NewServer(r)
			r.Group(Routes(NewRenderer(searchService, eventsearchService, categoryService, topicService, staticTheme, ts.URL, tz), nil))
			defer ts.Close()

			res, err := http.Get(ts.URL + tc.uri)
			if err != nil {
				t.Fatalf("request: %s", err)
			}
			if res.StatusCode != tc.status {
				t.Fatalf("status code: want: %d, got: %d", tc.status, res.StatusCode)
			}
			if tc.contentTestFunc != nil {
				content, err := io.ReadAll(res.Body)
				if err != nil {
					t.Fatalf("body: %s", err)
				}
				tc.contentTestFunc(t, string(content))
			} else if tc.htmlTestFunc != nil {
				doc, err := goquery.NewDocumentFromReader(res.Body)
				if err != nil {
					t.Fatalf("body: %s", err)
				}

				tc.htmlTestFunc(t, doc)
			}
		})
	}
}
