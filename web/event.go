//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"encoding/json"
	"fmt"
	"net/http"
	"text/template"
	"time"

	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/xerror"
	"eintopf.info/internal/xtime"
	"eintopf.info/service/eventsearch"
)

// EventPage renders the even page.
func (renderer *Renderer) EventPage(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	if id == "" {
		renderer.errorPage(w, r, xerror.NotFoundError{Err: fmt.Errorf("event not found: empty id"), Type: "event"})
		return
	}

	result, err := renderer.eventSearch.Search(r.Context(), eventsearch.Options{
		Filters: eventsearch.Filters{
			eventsearch.IDFilter{ID: id},
			// Only search for single day and raw multiday events.
			eventsearch.MultidayRangeFilter{MultidayMax: iptr(0)},
		},
		Aggregations: map[string]eventsearch.Aggregation{},
		Omit:         []string{},
	})
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}
	if len(result.Events) < 1 {
		renderer.errorPage(w, r, xerror.NotFoundError{Err: fmt.Errorf("event not found: id=%s", id), Type: "event"})
		return
	}
	if len(result.Events) > 1 {
		renderer.errorPage(w, r, fmt.Errorf("too many events found: %d", len(result.Events)))
		return
	}

	event := result.Events[0]
	childrenByDay := make(map[time.Time][]*eventsearch.EventDocument)
	if len(event.Children) > 0 {
		childrenByDay = groupEventsByDays(event.Children)
	}

	pastEventsFromOrganizers := map[string][]*eventsearch.EventDocument{}
	for _, o := range event.Organizers {
		if o.Group != nil {
			pastEvents, err := renderer.eventSearch.Search(r.Context(), eventsearch.Options{
				Sort:           "date",
				SortDescending: true,
				PageSize:       5,
				Filters: []eventsearch.Filter{
					eventsearch.ListedFilter{Listed: true},
					eventsearch.NotFilter{Filter: eventsearch.IDFilter{ID: event.ID}},
					eventsearch.MultidayRangeFilter{MultidayMax: iptr(1)},
					eventsearch.GroupIDFilter{GroupID: o.Group.ID},
					eventsearch.StartRangeFilter{StartMax: xtime.NowInLocation(renderer.tz)},
				},
			})
			if err != nil {
				renderer.errorPage(w, r, fmt.Errorf("search past events: groupID=%s: %w", o.Group.ID, err))
				return
			}
			if len(pastEvents.Events) > 0 {
				pastEventsFromOrganizers[o.Group.Name] = pastEvents.Events
			}
		}
	}

	futureEventsFromOrganizers := map[string][]*eventsearch.EventDocument{}
	for _, o := range event.Organizers {
		if o.Group != nil {
			futureEvents, err := renderer.eventSearch.Search(r.Context(), eventsearch.Options{
				Sort:     "date",
				PageSize: 5,
				Filters: []eventsearch.Filter{
					eventsearch.ListedFilter{Listed: true},
					eventsearch.NotFilter{Filter: eventsearch.IDFilter{ID: event.ID}},
					eventsearch.MultidayRangeFilter{MultidayMax: iptr(1)},
					eventsearch.GroupIDFilter{GroupID: o.Group.ID},
					eventsearch.StartRangeFilter{StartMin: xtime.NowInLocation(renderer.tz)},
				},
			})
			if err != nil {
				renderer.errorPage(w, r, fmt.Errorf("search future events: groupID=%s: %w", o.Group.ID, err))
				return
			}
			if len(futureEvents.Events) > 0 {
				futureEventsFromOrganizers[o.Group.Name] = futureEvents.Events
			}
		}
	}

	metaTags := MetaTags{Title: event.Name, Description: event.Description, Image: event.Image, NotPublished: !event.Published}

	err = renderer.renderPage(w, r, "event", map[string]any{
		"Event":        event,
		"Children":     childrenByDay,
		"Options":      eventListOptions{},
		"PastEvents":   pastEventsFromOrganizers,
		"FutureEvents": futureEventsFromOrganizers,
		"Meta":         metaTags,
	}, template.FuncMap{
		"eventShouldRenderMap": func(e *eventsearch.EventDocument) bool {
			if e.Location == nil {
				return false
			}

			if e.Location.Place != nil {
				if e.Location.Place.Lat != 0 && e.Location.Place.Lng != 0 {
					return true
				}
			}
			if e.Location.Lat != 0 && e.Location.Lng != 0 {
				return true
			}
			return false
		},
		"weekdayGermanShort": func (day time.Weekday) string {
			days := map[time.Weekday]string{
				time.Monday:    "Mo.",
				time.Tuesday:   "Di.",
				time.Wednesday: "Mi.",
				time.Thursday:  "Do.",
				time.Friday:    "Fr.",
				time.Saturday:  "Sa.",
				time.Sunday:    "So.",
			}
			return days[day]
		},
	})
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}
}

func iptr(i int) *int {
	return &i
}

func eventToMapMarker(e *eventsearch.EventDocument) string {
	c := []mapMarker{}
	if e.Location == nil {
		return ""
	}

	if e.Location.Place != nil {
		p := e.Location.Place
		c = []mapMarker{{PlaceID: p.ID, Name: p.Name, Lat: p.Lat, Lng: p.Lng}}
	} else {
		l := e.Location
		c = []mapMarker{{Name: l.Name, Lat: l.Lat, Lng: l.Lng}}
	}

	data, err := json.Marshal(c)
	if err != nil {
		return ""
	}
	return string(data)
}

type coordinates struct {
	Lat float64
	Lng float64
}

func getEventCoordinates(e *eventsearch.EventDocument) *coordinates {
	coords := &coordinates{0.0, 0.0}
	if e.Location.Place != nil {
		coords.Lat = e.Location.Place.Lat
		coords.Lng = e.Location.Place.Lng
	} else if (e.Location.Lat != 0) && (e.Location.Lng != 0) {
		coords.Lat = e.Location.Lat
		coords.Lng = e.Location.Lng
	}
	return coords
}
