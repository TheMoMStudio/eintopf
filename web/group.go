//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"fmt"
	"net/http"
	"net/url"

	"eintopf.info/internal/xerror"
	"eintopf.info/internal/xtime"
	"eintopf.info/service/eventsearch"
	"eintopf.info/service/indexo"
	"eintopf.info/service/search"

	"github.com/go-chi/chi/v5"
)

// GroupPage renders a group page.
func (renderer *Renderer) GroupPage(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	if id == "" {
		renderer.errorPage(w, r, xerror.NotFoundError{Err: fmt.Errorf("group not found: empty id"), Type: "group"})
		return
	}
	id, err := url.QueryUnescape(id)
	if err != nil {
		renderer.errorPage(w, r, xerror.NotFoundError{Err: fmt.Errorf("group not found: invalid id=%s", id), Type: "group"})
		return
	}
	result, err := renderer.search.Search(r.Context(), &search.Options{
		Filters: []search.Filter{
			&search.TermsFilter{Field: "type", Terms: []string{"group"}},
			&search.TermsFilter{Field: "id", Terms: []string{id}},
		},
	})
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}
	if len(result.Hits) != 1 {
		renderer.errorPage(w, r, xerror.NotFoundError{Err: fmt.Errorf("group not found: id=%s", id), Type: "group"})
		return
	}

	group := &indexo.GroupDocument{}
	err = result.Hits[0].Unmarshal(group)
	if err != nil {
		renderer.errorPage(w, r, fmt.Errorf("unmarshal group: id=%s: %w", id, err))
		return
	}

	pastEvents, err := renderer.eventSearch.Search(r.Context(), eventsearch.Options{
		Sort:           "date",
		SortDescending: true,
		PageSize:       5,
		Filters: []eventsearch.Filter{
			eventsearch.ListedFilter{Listed: true},
			eventsearch.GroupIDFilter{GroupID: id},
			eventsearch.StartRangeFilter{StartMax: xtime.NowInLocation(renderer.tz)},
			eventsearch.MultidayRangeFilter{MultidayMax: iptr(1)},
		},
	})
	if err != nil {
		renderer.errorPage(w, r, fmt.Errorf("search past events: groupID=%s: %w", id, err))
		return
	}

	futureEvents, err := renderer.eventSearch.Search(r.Context(), eventsearch.Options{
		Sort:     "date",
		PageSize: 5,
		Filters: []eventsearch.Filter{
			eventsearch.ListedFilter{Listed: true},
			eventsearch.GroupIDFilter{GroupID: id},
			eventsearch.StartRangeFilter{StartMin: xtime.NowInLocation(renderer.tz)},
			eventsearch.MultidayRangeFilter{MultidayMax: iptr(1)},
		},
	})
	if err != nil {
		renderer.errorPage(w, r, fmt.Errorf("search past events: groupID=%s: %w", id, err))
		return
	}

	metaTags := MetaTags{Title: group.Name, Description: group.Description, Image: group.Image, NotPublished: !group.Published}

	err = renderer.renderPage(w, r, "group", map[string]any{
		"Group":        group,
		"Options":      eventListOptions{},
		"PastEvents":   pastEvents.Events,
		"FutureEvents": futureEvents.Events,
		"Meta":         metaTags,
	}, nil)
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}
}
