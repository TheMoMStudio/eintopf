//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package web

import (
	"encoding/json"
	"net/http"
	"sort"
	"strings"
	"text/template"

	"eintopf.info/service/indexo"
	"eintopf.info/service/place"
	"eintopf.info/service/search"
)

// PlaceListPage renders the grouplist page.
func (renderer *Renderer) PlaceListPage(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query().Get("query")
	result, err := renderer.search.Search(r.Context(), &search.Options{
		Query: query,
		Sort:  "name",
		Filters: []search.Filter{
			&search.TermsFilter{Field: "type", Terms: []string{"place"}},
			&search.BoolFilter{Field: "listed", Value: true},
		},
	})
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}

	alphabet := defaultAlphabet()
	places := []PlaceDocument{}
	for _, hit := range result.Hits {
		g := PlaceDocument{}
		err := hit.Unmarshal(&g)
		if err != nil {
			renderer.errorPage(w, r, err)
			return
		}
		places = append(places, g)

		for _, letter := range alphabet {
			if []rune(letter.Letter)[0] == g.StartLetterRune() {
				letter.HasItems = true
				break
			}
		}
	}
	sort.Slice(places, func(i, j int) bool {
		return strings.ToLower(places[i].Name) < strings.ToLower(places[j].Name)
	})

	currentLetter := rune('A')

	err = renderer.renderPage(w, r, "placelist", map[string]any{
		"Query":    query,
		"Places":   places,
		"Alphabet": alphabet,
	}, template.FuncMap{
		"isCurrentLetter": func(letter rune) bool {
			if currentLetter <= letter {
				currentLetter = currentLetter + letter - currentLetter + 1
				return true
			}
			return false
		},
	})
	if err != nil {
		renderer.errorPage(w, r, err)
		return
	}
}

type PlaceDocument indexo.PlaceDocument

func (g PlaceDocument) StartLetter() string {
	return strings.ToUpper(string([]rune(g.Name)[0]))
}

func (g PlaceDocument) StartLetterRune() rune {
	return []rune(g.StartLetter())[0]
}

func (p PlaceDocument) MapMarker() mapMarker {
	return mapMarker{PlaceID: p.ID, Name: p.Name, Lat: p.Lat, Lng: p.Lng}
}

type mapMarker struct {
	PlaceID string  `json:"placeId"`
	Name    string  `json:"name"`
	Lat     float64 `json:"lat"`
	Lng     float64 `json:"lng"`
}

func placesToMapMarkers(places []PlaceDocument) string {
	c := []mapMarker{}
	for _, p := range places {
		if p.Lat != 0 && p.Lng != 0 {
			c = append(c, p.MapMarker())
		}
	}
	data, err := json.Marshal(c)
	if err != nil {
		return ""
	}
	return string(data)
}

func placeDocumentToMapMarker(p PlaceDocument) string {
	c := []mapMarker{p.MapMarker()}
	data, err := json.Marshal(c)
	if err != nil {
		return ""
	}
	return string(data)
}

func placeToMapMarker(p *place.Place) string {
	c := []mapMarker{{PlaceID: p.ID, Name: p.Name, Lat: p.Lat, Lng: p.Lng}}
	data, err := json.Marshal(c)
	if err != nil {
		return ""
	}
	return string(data)
}
