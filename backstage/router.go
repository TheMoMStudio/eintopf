//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package backstage

import (
	"embed"
	"io/fs"
	"net/http"
	"strings"

	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/plausible"
	"eintopf.info/web"
)

// buildFS contains the build files for the backstage.
//
//go:embed build
var buildFS embed.FS

// Routes adds routes to the given router that serve the backstage.
// When backstageAssetPath is provided, it serves the assets from the given path
// instead of the embeded filesystem.
func Routes(renderer *web.Renderer, backstageAssetPath string, plausibleClient *plausible.Client) func(r chi.Router) {
	subBuildFS, _ := fs.Sub(buildFS, "build")
	buildFileServer := http.FileServer(http.FS(subBuildFS))
	if backstageAssetPath != "" {
		buildFileServer = http.FileServer(http.Dir("./backstage/build"))
	}

	return func(r chi.Router) {
		r.Get("/backstage*", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if strings.HasSuffix(r.URL.Path, ".js") || strings.HasSuffix(r.URL.Path, ".css") {

				r.URL.Path = strings.TrimPrefix(r.URL.Path, "/backstage")

				buildFileServer.ServeHTTP(w, r)
				return
			}
			renderer.BackstagePage(w, r)
			plausible.TrackPageView(plausibleClient, r)
		}))
	}
}
