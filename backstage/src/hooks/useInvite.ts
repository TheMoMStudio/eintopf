// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { useState } from 'preact/hooks';
import { queryInvite, InviteResponse, InviteError } from '../api/invitation';
import { useAppData } from '../appData';
import useUser from './useUser';

export interface InviteState {
  loading: boolean;
  error?: InviteError;
  data?: InviteResponse;
}

export const useInvite = (): [InviteState, () => Promise<void>] => {
  const { user } = useUser();
  const { apiurl } = useAppData();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<InviteError | undefined>(undefined);
  const [data, setData] = useState<InviteResponse | undefined>(undefined);

  const query = async (): Promise<void> => {
    if (!user) return;

    setLoading(true);
    const response = await queryInvite(apiurl, user.token);
    setLoading(false);
    if (typeof response === 'string') {
      setError(response);
      setData(undefined);
      return;
    }
    setError(undefined);
    setData(response);
  };

  return [{ loading, error, data }, query];
};

export default useInvite;
