// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { useEffect, useState } from 'preact/hooks';
import { querySearch, SearchError, SearchResult, SearchOptions, SearchFilter } from '../api/search';

export const createQueryString = (options: SearchOptions): string => {
  const createFilterString = (filters: SearchFilter[]): string =>
    filters
      .map((filter) => {
        if (filter.type === 'terms') {
          return filter.filter.field + filter.filter.terms.join();
        }
        if (filter.type === 'daterange') {
          let str = filter.filter.field;
          if (filter.filter.min) {
            str += filter.filter.min.toISOString();
          }
          if (filter.filter.max) {
            str += filter.filter.max.toISOString();
          }
          return str;
        }
        return '';
      })
      .join();

  let { query } = options;
  query += options.page.toString();
  query += options.pageSize.toString();
  query += createFilterString(options.filters);
  query += Object.entries(options.aggregations).map(([key, aggregation]) => {
    let str = key + aggregation.field + aggregation.type;
    if (aggregation.filters !== undefined) {
      str += createFilterString(aggregation.filters);
    }
    return str;
  });
  return query;
};

export interface SearchState {
  loading: boolean;
  loadedOnce: boolean;
  error?: SearchError;
  data?: SearchResult;
  options: SearchOptions;
}

export const useSearch = (
  apiurl: string,
  initialOptions: SearchOptions,
): [SearchState, (options: SearchOptions) => void] => {
  const [loading, setLoading] = useState(false);
  const [loadedOnce, setLoadedOnce] = useState(false);
  const [error, setError] = useState<SearchError | undefined>(undefined);
  const [data, setData] = useState<SearchResult | undefined>(undefined);
  const [options, setOptions] = useState<SearchOptions>(initialOptions);
  useEffect(() => {
    if (JSON.stringify(options) !== JSON.stringify(initialOptions)) {
      setOptions(initialOptions);
    }
  }, [options, initialOptions]);

  useEffect(() => {
    const query = async () => {
      if (!options) return;
      setLoading(true);

      const result = await querySearch(apiurl, options);
      if (typeof result === 'string') {
        setError(result);
      } else {
        setData(result);
      }
      setLoading(false);
      setLoadedOnce(true);
    };

    query();
  }, [options, apiurl]);

  return [
    {
      loading,
      loadedOnce,
      error,
      data,
      options,
    },
    setOptions,
  ];
};

export default useSearch;
