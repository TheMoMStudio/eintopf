// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { useEffect, useMemo, useState } from 'preact/hooks';
import { RoleNormal } from '../api/auth';
import { ListError, ListResponse, ListOptions, QueryCrud } from '../api/crud';
import { useAppData } from '../appData';
import useUser from './useUser';

export interface BackstageListState<T> {
  loading: boolean;
  error?: ListError;
  data?: ListResponse<T>;
}

export interface Pagination {
  current: number;
  max: number;
}

export interface BackstageListConfig {
  pageSize?: number;
  initialSorting?: string;
  initialOrder?: 'ASC' | 'DESC';
  baseFilters?: Record<string, any>;
}

export type SelectionState = 'all' | 'partial' | 'none';

export const useBackstageList = <T extends object>(
  queryCrud: QueryCrud<T>,
  config: BackstageListConfig,
  // @ts-ignore
  idField?: keyof T = 'id',
): {
  queryCrud: QueryCrud<T>;
  state: BackstageListState<T>;
  refresh: () => Promise<void>;
  filter: (field: string, value: string) => void;
  sorting: { field?: string; order?: 'ASC' | 'DESC' };
  sort: (field: string) => void;
  pagination: Pagination;
  paginate: (to: 'first' | 'last' | 'next' | 'prev') => void;
  setPageSize: (pageSize: number) => void;
  selection: {
    selected: boolean[];
    toggleSelect: (i: number) => void;
    toggleSelectAll: () => void;
  };
  deleteSelected: () => Promise<void>;
  deactivateSelected: () => Promise<void>;
} => {
  const { user } = useUser();
  const { apiurl } = useAppData();

  const [options, setOptions] = useState<ListOptions>({
    offset: 0,
    limit: config.pageSize,
    sorting: config.initialSorting,
    order: config.initialOrder,
    filters: {
      ...(config.baseFilters || {}),
      // Always set the ownedBy filter to 'self' for normal users.
      ...(user && user.role === RoleNormal ? { ownedBy: ['self'] } : {}),
    },
  });

  const filter = (field: string, value: string | undefined): void => {
    const filters = options.filters || {};
    filters[field] = value;
    console.log('filter', filters);
    setOptions({ ...options, filters });
  };

  // Sorting
  const sort = (field: string): void => {
    if (field === options.sorting) {
      setOptions({
        ...options,
        sorting: options.sorting,
        order: options.order === 'ASC' ? 'DESC' : 'ASC',
      });
    } else {
      setOptions({
        ...options,
        sorting: field,
        order: 'ASC',
      });
    }
  };

  // Pagination
  const [page, setPage] = useState(1);
  const [maxPages, setMaxPages] = useState(1);
  const updatePagination = (total: number): void => {
    const max = Math.ceil(total / (config.pageSize ? config.pageSize : 10));
    setMaxPages(max);
    if (page > max) {
      setPage(max);
    }
    if (page === 0 && max > 0) {
      setPage(1);
    }
  };
  const paginate = (to: 'first' | 'last' | 'next' | 'prev'): void => {
    let newPage = page;
    switch (to) {
      case 'first':
        newPage = 1;
        break;
      case 'last':
        newPage = maxPages;
        break;
      case 'next':
        newPage = page < maxPages ? page + 1 : page;
        break;
      case 'prev':
        newPage = page > 1 ? page - 1 : page;
        break;
      default:
        return;
    }
    setOptions({ ...options, offset: (newPage - 1) * (config.pageSize ? config.pageSize : 10) });
    setPage(newPage);
  };

  const setPageSize = (pageSize: number): void => {
    setOptions({
      ...options,
      limit: pageSize,
    });
  };

  // Selection
  const [selected, setSelected] = useState<boolean[]>([]);
  const [selectionState, setSelectionState] = useState<SelectionState>('none');

  const resetSelection = (count: number): void => {
    setSelected(Array(count).fill(false));
    setSelectionState('none');
  };

  const toggleSelect = (i: number): void => {
    const newSelected = selected.slice();
    newSelected[i] = !newSelected[i];
    setSelected(newSelected);
    setSelectionState('partial');
  };

  const toggleSelectAll = (): void => {
    switch (selectionState) {
      case 'all':
        setSelected(selected.fill(false));
        setSelectionState('none');
        break;
      case 'partial':
      case 'none':
        setSelected(selected.fill(true));
        setSelectionState('all');
        break;
      default:
    }
  };

  // List
  const [state, setState] = useState<BackstageListState<T>>({ loading: false });
  const query = useMemo(
    () => async () => {
      if (!user) return;

      if (state.loading === true) return;

      setState({ ...state, loading: true });
      const response = await queryCrud.list(apiurl, user.token, options);
      if (typeof response === 'string') {
        setState({ loading: false, error: response });
        return;
      }
      setState({ loading: false, error: undefined, data: response });
      updatePagination(response.total);
      resetSelection(response.items.length);
    },
    [user, options, apiurl], // eslint-disable-line react-hooks/exhaustive-deps
  );

  useEffect(() => {
    query();
  }, [options]); // eslint-disable-line react-hooks/exhaustive-deps

  const queryBulkDelete = async (models: T[]): Promise<void> => {
    if (!user) return;

    for (let i = 0; i < models.length; i++) {
      await queryCrud.delete(apiurl, user.token, models[i][idField] as unknown as string);
    }
  };

  const queryBulkDeactivate = async (models: T[]): Promise<void> => {
    if (!user) return;

    for (let i = 0; i < models.length; i++) {
      if (!('id' in models[i])) {
        return;
      }
      const response = await queryCrud.get(
        apiurl,
        user.token,
        models[i][idField] as unknown as string,
      );
      if (typeof response !== 'string') {
        if ('deactivated' in response) {
          // @ts-ignore
          response.deactivated = !response.deactivated;
        }
        await queryCrud.update(apiurl, user.token, response);
      }
    }
  };

  const deleteSelected = async () => {
    if (state.data !== undefined) {
      const models: T[] = selected
        .map((selectedItem, i) => (selectedItem && state.data ? state.data.items[i] : undefined))
        .filter((model) => model !== undefined) as T[];
      await queryBulkDelete(models);
    }
    query();
  };

  const deactivateSelected = async () => {
    if (state.data !== undefined) {
      const models: T[] = selected
        .map((selectedItem, i) => (selectedItem && state.data ? state.data.items[i] : undefined))
        .filter((model) => model !== undefined) as T[];
      await queryBulkDeactivate(models);
    }
    query();
  };

  const queryCrud2 = {
    list: queryCrud.list,
    get: queryCrud.get,
    create: queryCrud.create,
    update: queryCrud.update,
    delete: async (apiurl: string, token: string, id: string) => {
      const deleteResponse = await queryCrud.delete(apiurl, token, id);
      query();

      return deleteResponse;
    },
  };

  return {
    queryCrud: queryCrud2,
    state,
    refresh: query,
    filter,
    sorting: { field: options.sorting, order: options.order },
    sort,
    pagination: { current: page, max: maxPages },
    paginate,
    setPageSize,
    selection: {
      selected,
      toggleSelect,
      toggleSelectAll,
    },
    deleteSelected,
    deactivateSelected,
  };
};

export default useBackstageList;
