// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { route } from 'preact-router';
import { queryLogin, LoginCredentials, LoginError } from '../api/auth';
import { useAppData } from '../appData';
import { createLogoutAction, createLoginAction, User, deleteStore } from '../store/store';
import { useStore } from '../store/useStore';

export const useUser = (): {
  user?: User;
  login: (credentials: LoginCredentials) => Promise<LoginError | undefined>;
  logout: () => void;
} => {
  const { apiurl } = useAppData();
  const [store, dispatch] = useStore();

  const login = async (credentials: LoginCredentials): Promise<LoginError | undefined> => {
    const result = await queryLogin(apiurl, credentials);
    if (typeof result === 'string') {
      return result;
    }
    dispatch(createLoginAction(result));
    return Promise.resolve(undefined);
  };
  const logout = () => {
    dispatch(createLogoutAction());
    deleteStore();
    route('/backstage');
  };

  return { user: store.user, login, logout };
};

export default useUser;
