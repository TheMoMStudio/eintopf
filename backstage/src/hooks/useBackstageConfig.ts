// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import useStore from '../store/useStore';
import { createSetBackstageAction } from '../store/store';

export const useBackstageConfiguration = (): {
  setValue: <T>(key: string, value: T) => void;
  getValue: <T>(key: string, typ: string, defaultValue?: T) => T | undefined;
} => {
  const [store, dispatch] = useStore();

  const setValue = <T>(key: string, value: T) => {
    let backstage: { [key: string]: any } = { ...store.backstage };
    if (store.backstage === undefined) {
      backstage = {};
    }
    backstage[key] = value as any;
    dispatch(createSetBackstageAction(backstage));
  };

  const getValue = <T>(key: string, typ: string, defaultValue?: T): T | undefined => {
    if (store.backstage === undefined) {
      return undefined;
    }
    const value = store.backstage[key];
    if (value === undefined) {
      return defaultValue;
    }
    if (typeof value !== typ) {
      return undefined;
    }
    return value as T;
  };

  return { setValue, getValue };
};

export default useBackstageConfiguration;
