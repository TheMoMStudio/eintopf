// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { Signal, signal } from '@preact/signals';
import { BaseError } from '../api/error';

enum NotificationState {
  'ERROR' = 'error',
  'SUCCESS' = 'success',
  'WARNING' = 'warning',
  'NONE' = 'none',
}

interface Notification {
  state: NotificationState;
  message: string;
  hasClose: boolean;
}

interface NotificationHandler {
  state: Signal<Notification>;
  push: (state: NotificationState, message: string, hasClose: boolean) => void;
}

function createNotificationState() {
  const currentState = signal<Notification>({
    state: NotificationState.NONE,
    message: '',
    hasClose: true,
  });

  let timeout: NodeJS.Timeout;

  const push = (state: NotificationState, message: string, hasClose: boolean) => {
    currentState.value = { state, message, hasClose };

    if (timeout) {
      clearTimeout(timeout);
    }

    timeout = setTimeout(() => {
      currentState.value = { state: NotificationState.NONE, message, hasClose: true };
    }, 5000);
  };

  return { state: currentState, push };
}

const checkError = <T, E extends BaseError>(
  response: T | E,
  notifications: NotificationHandler,
  hasClose: boolean,
  errorMessage?: string,
) => {
  if (typeof response === 'string') {
    notifications.push(NotificationState.ERROR, errorMessage ?? response, hasClose);
    return true;
  }
  return false;
};

export {
  NotificationState,
  Notification,
  createNotificationState,
  checkError,
  NotificationHandler,
};
