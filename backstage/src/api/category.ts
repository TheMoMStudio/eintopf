// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { queryCreate, queryDelete, queryGet, queryList, queryUpdate, ListOptions } from './crud';

export interface Category {
  id: string;
  name: string;
  description: string;
}

export interface Involved {
  name: string;
  description: string;
}

export type NewCategory = Omit<Category, 'id'>;

export const queryCategoryList = (apiurl: string, token: string, options: ListOptions) =>
  queryList<Category>('categories', apiurl, token, options);

export const queryCategoryCreate = (apiurl: string, token: string, category: NewCategory) => {
  return queryCreate<Category>('categories', apiurl, token, category);
};

export const queryCategoryUpdate = (apiurl: string, token: string, category: Category) => {
  return queryUpdate<Category>('categories', apiurl, token, category, category.id);
};

export const queryCategoryGet = (apiurl: string, token: string, id: string) =>
  queryGet<Category>('categories', apiurl, token, id);

export const queryCategoryDelete = (apiurl: string, token: string, id: string) => {
  return queryDelete('categories', apiurl, token, id);
};

export const queryCategoryCrud = {
  list: queryCategoryList,
  get: queryCategoryGet,
  create: queryCategoryCreate,
  update: queryCategoryUpdate,
  delete: queryCategoryDelete,
};
