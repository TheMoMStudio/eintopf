// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { BaseError } from './error';
import request from './request';

export const RoleNormal = 'normal';
export const RoleModerator = 'moderator';
export const RoleAdmin = 'admin';

export type Role = typeof RoleNormal | typeof RoleModerator | typeof RoleAdmin;

export interface LoginCredentials {
  email: string;
  password: string;
}

export interface LoginResponse {
  token: string;
  id: string;
  role: Role;
}

export const InvalidCredentialsError = 'InvalidCredentials';
export const UserDeactivatedError = 'UserDeactivatedError';
export type LoginError = typeof InvalidCredentialsError | typeof UserDeactivatedError | BaseError;

export const queryLogin = async (
  apiurl: string,
  credentials: LoginCredentials,
): Promise<LoginResponse | LoginError> => {
  const response = await request<LoginError>(apiurl, {
    query: '/auth/login',
    method: 'POST',
    body: credentials,
  });
  if (typeof response === 'string') {
    return response as LoginError;
  }
  return response.json();
};
