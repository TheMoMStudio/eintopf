// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import {
  queryCreate,
  queryDelete,
  queryGet,
  queryList,
  queryUpdate,
  ListOptions,
  BaseModel,
} from './crud';
import { BaseError } from './error';
import request from './request';

export interface Invitation extends BaseModel {
  id: string;
  token: string;
  createdAt: string;
  createdBy: string;
  usedBy: string;
}

export const queryInvitationList = (apiurl: string, token: string, options: ListOptions) =>
  queryList<Invitation>('invitations', apiurl, token, options);

export const queryInvitationCreate = (apiurl: string, token: string, invitation: Invitation) =>
  queryCreate<Invitation>('invitations', apiurl, token, invitation);

export const queryInvitationUpdate = (apiurl: string, token: string, invitation: Invitation) =>
  queryUpdate<Invitation>('invitations', apiurl, token, invitation, invitation.id);

export const queryInvitationGet = (apiurl: string, token: string, id: string) =>
  queryGet<Invitation>('invitations', apiurl, token, id);

export const queryInvitationDelete = (apiurl: string, token: string, id: string) =>
  queryDelete('invitations', apiurl, token, id);

export const queryInvitationCrud = {
  list: queryInvitationList,
  get: queryInvitationGet,
  create: queryInvitationCreate,
  update: queryInvitationUpdate,
  delete: queryInvitationDelete,
};

export interface InvitationRequest {
  email: string;
  nickname: string;
  password: string;
}

export const EmailAlreadyExistsError = 'EmailAlreadyExistsError';
export const NicknameAlreadyExistsError = 'NicknameAlreadyExistsError';
export const InvalidInvitationTokenError = 'InvalidInvitationToken';
export const InvitationTokenAlreadyUsedError = 'InvitationTokenAlreadyUsed';
export type InvitationError =
  | typeof EmailAlreadyExistsError
  | typeof NicknameAlreadyExistsError
  | typeof InvalidInvitationTokenError
  | typeof InvitationTokenAlreadyUsedError
  | BaseError;

export const queryInvitation = async (
  apiurl: string,
  token: string,
  invitationRequest: InvitationRequest,
): Promise<undefined | InvitationError> => {
  const response = await request<InvitationError>(apiurl, {
    query: `/invitations/invite/${token}`,
    method: 'POST',
    body: invitationRequest,
  });
  if (typeof response === 'string') {
    return response;
  }
  return undefined;
};

export const NoInvitationYetError = 'NoInvitationYetError';
export const InvitationLimitError = 'InvitationLimitError';
export type InviteError = typeof NoInvitationYetError | typeof InvitationLimitError | BaseError;

export interface InviteResponse {
  token: string;
}

export const queryInvite = async (
  apiurl: string,
  token: string,
): Promise<InviteResponse | InviteError> => {
  const response = await request<InviteError>(apiurl, {
    query: '/invitations/invite',
    method: 'POST',
    errorMapping: {
      'no invitation yet': NoInvitationYetError,
      'invitation limit': InvitationLimitError,
    },
    headers: { Authorization: token },
  });
  if (typeof response === 'string') {
    return response;
  }
  return response.json();
};
