// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { BaseError } from './error';
import request from './request';

export interface SearchOptions {
  query: string;
  sort: string;
  page: number;
  pageSize: number;
  filters: SearchFilter[];
  aggregations: { [key: string]: SearchAggregation };
}

export type SearchFilter = TermsFilter | DateRangeFilter | BoolFilter;

export interface TermsFilter {
  type: 'terms';
  filter: {
    field: string;
    terms: string[];
  };
}

export interface DateRangeFilter {
  type: 'daterange';
  filter: {
    field: string;
    min?: Date;
    max?: Date;
  };
}

export interface BoolFilter {
  type: 'bool';
  filter: {
    field: string;
    value: boolean;
  };
}

export interface SearchAggregation {
  type: AggregationType;
  field: string;
  filters?: SearchFilter[];
}

const TermsAggregation = 'terms';
const DateRangeAggregation = 'daterange';
export { TermsAggregation, DateRangeAggregation };

export type AggregationType = typeof TermsAggregation | typeof DateRangeAggregation;

export interface SearchResult {
  hits: SearchHit[];
  total: number;
  buckets: { [key: string]: SearchBucket };
}

export interface SearchHit {
  type: string;
  raw: string;
}

export type DocumentType = 'event' | 'group' | 'place';

export const parseHit = <T>(hit: SearchHit, docType: DocumentType): T | undefined => {
  if (hit.type !== docType) return undefined;
  return JSON.parse(hit.raw);
};

export const parseHits = <T>(hits: SearchHit[], docType: DocumentType): T[] =>
  hits.map((hit) => parseHit<T>(hit, docType)).filter((doc) => doc !== undefined) as T[];

export type SearchBucket = TermsBucket | DateRangeBucket;

export type TermsBucket = Term[];

export const isTermsBucket = (bucket: SearchBucket): bucket is TermsBucket => Array.isArray(bucket);

export interface Term {
  term: string;
  count: number;
}

export interface DateRangeBucket {
  min: Date;
  max: Date;
}

export const isDateRangeBucket = (bucket: SearchBucket): bucket is DateRangeBucket =>
  'min' in bucket && 'max' in bucket;

export const mergeBuckets = (
  buckets1: { [key: string]: SearchBucket },
  buckets2: { [key: string]: SearchBucket },
): { [key: string]: SearchBucket } => {
  const buckets: { [key: string]: SearchBucket } = {};
  const interator = ([key, bucket]: [string, SearchBucket]): void => {
    if (buckets[key] === undefined) {
      buckets[key] = bucket;
      return;
    }
    const oldBucket = buckets[key];
    if (isTermsBucket(oldBucket) && isTermsBucket(bucket)) {
      const terms: { [term: string]: number } = {};
      oldBucket.forEach((term) => {
        terms[term.term] = term.count;
      });
      bucket.forEach((term) => {
        terms[term.term] = terms[term.term] ? terms[term.term] + term.count : term.count;
      });

      buckets[key] = Object.entries(terms).map(([term, count]) => ({
        term,
        count,
      }));
    }
    if (isDateRangeBucket(oldBucket) && isDateRangeBucket(bucket)) {
      buckets[key] = {
        min: oldBucket.min < bucket.min ? oldBucket.min : bucket.min,
        max: oldBucket.max > bucket.max ? oldBucket.max : bucket.max,
      };
    }
  };

  Object.entries(buckets1).forEach(interator);
  Object.entries(buckets2).forEach(interator);

  return buckets;
};

export const assembleSearchQueryParameters = ({
  query,
  sort,
  page,
  pageSize,
  filters,
  aggregations,
}: SearchOptions): Record<string, string> => ({
  query: encodeURIComponent(query),
  sort,
  page: page.toString(),
  pageSize: pageSize.toString(),
  filters: encodeURIComponent(JSON.stringify(filters)),
  aggregations: encodeURIComponent(JSON.stringify(aggregations)),
});

export type SearchError = BaseError;

export const querySearch = async (
  apiurl: string,
  options: SearchOptions,
): Promise<SearchResult | SearchError> => {
  const response = await request<SearchError>(apiurl, {
    query: '/search',
    queryParamaters: assembleSearchQueryParameters(options),
    method: 'GET',
  });
  if (typeof response === 'string') {
    return response as SearchError;
  }
  return response.json() as Promise<SearchResult>;
};
