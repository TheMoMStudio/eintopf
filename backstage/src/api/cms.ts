// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { BaseError } from './error';
import request, { RequestMethods } from './request';

export enum ThemeFiletype {
  'TEMPLATE' = 'template',
  'CSS' = 'css',
  'JS' = 'js',
  'IMAGE' = 'image',
  'FONT' = 'font',
  'MD' = 'md',
  'META' = 'meta',
  'CONFIG' = 'config',
}

export const themeBaseUrl = '/backstage/cms/theme';
export const fileBaseUrl = `${themeBaseUrl}/file/`;
export const contentBaseUrl = `${themeBaseUrl}/content/`;

export interface Theme {
  name: string;
  active: boolean;
  editable: boolean;
}

export const queryCms = async (
  apiurl: string,
  token: string,
  method: RequestMethods,
  path: string,
  body?: any,
  isForm: boolean = false,
) => {
  const requestParams: {
    query: string;
    method: RequestMethods;
    headers: { Authorization: string };
    body?: any;
  } = {
    query: path,
    method,
    headers: { Authorization: token },
  };

  if (body) requestParams.body = body;

  const response = isForm
    ? await fetch(`${apiurl}${path}`, requestParams)
    : await request<BaseError>(apiurl, requestParams);

  if (typeof response === 'string') {
    return response as BaseError;
  }
  return response.json();
};
