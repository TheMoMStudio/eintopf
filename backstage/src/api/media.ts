// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { BaseError } from './error';
import { ApiError } from './request';

export const mediaUrl = (apiurl: string, image: string): string => `${apiurl}/media/${image}`;

export interface AddFileResponse {
  id: string;
}

export const queryAddFile = async (
  apiurl: string,
  token: string,
  data: any,
): Promise<AddFileResponse | BaseError> => {
  const xhr = new XMLHttpRequest();
  return new Promise((resolve) => {
    // Setup our listener to process compeleted requests
    xhr.onreadystatechange = () => {
      // Only run if the request is complete
      if (xhr.readyState !== 4) return;

      // Process the response
      if (xhr.status >= 200 && xhr.status < 300) {
        const response = JSON.parse(xhr.response);
        resolve(response as AddFileResponse);
      } else {
        const response = JSON.parse(xhr.response) as ApiError;
        resolve(response.error as BaseError);
      }
    };

    xhr.open('POST', `${apiurl}/media`, true);
    xhr.setRequestHeader('Authorization', token);
    xhr.send(data);
  });
};
