// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import {
  queryCreate,
  queryDelete,
  queryGet,
  queryList,
  queryUpdate,
  ListOptions,
  BaseModel,
} from './crud';

export interface Event extends BaseModel {
  deactivated: boolean;
  start: string;
  end?: string;
  parent: string;
  name: string;
  organizers: string[];
  location: string;
  location2: string;
  address: string;
  lat: number;
  lng: number;
  description: string;
  published: boolean;
  ownedBy: string[];
  image: string;
  alt: string;
  tags?: string[];
}

export interface Involved {
  name: string;
  description: string;
}

export type NewEvent = Omit<Event, 'id'>;

export const queryEventList = (apiurl: string, token: string, options: ListOptions) =>
  queryList<Event>('events', apiurl, token, options);

export const queryEventCreate = (apiurl: string, token: string, event: NewEvent) => {
  return queryCreate<Event>('events', apiurl, token, event);
};

export const queryEventUpdate = (apiurl: string, token: string, event: Event) => {
  return queryUpdate<Event>('events', apiurl, token, event, event.id);
};

export const queryEventGet = (apiurl: string, token: string, id: string) =>
  queryGet<Event>('events', apiurl, token, id);

export const queryEventDelete = (apiurl: string, token: string, id: string) => {
  return queryDelete('events', apiurl, token, id);
};

export const queryEventCrud = {
  list: queryEventList,
  get: queryEventGet,
  create: queryEventCreate,
  update: queryEventUpdate,
  delete: queryEventDelete,
};
