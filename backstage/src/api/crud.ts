// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { BaseError } from './error';
import request from './request';

export interface BaseModel {
  id: string;
  deactivated: boolean;
}

export type Model<T extends object> = T;

// List
export interface ListOptions {
  offset?: number;
  limit?: number;
  sorting?: string;
  order?: 'ASC' | 'DESC';
  filters?: Record<string, any>;
}
export interface ListResponse<T> {
  items: T[];
  total: number;
}

export type ListError = BaseError;

export const queryList = async <T extends object>(
  model: string,
  apiurl: string,
  token: string,
  options: ListOptions,
): Promise<ListResponse<T> | ListError> => {
  const response = await request<ListError>(apiurl, {
    query: `/${model}/`,
    queryParamaters: {
      offset: options.offset !== undefined ? options.offset.toString() : undefined,
      limit: options.limit ? options.limit.toString() : undefined,
      sorting: options.sorting ? encodeURIComponent(options.sorting) : undefined,
      order: options.order ? options.order : undefined,
      filters: options.filters ? encodeURIComponent(JSON.stringify(options.filters)) : undefined,
    },
    method: 'GET',
    headers: { Authorization: token },
  });
  if (typeof response === 'string') {
    return response as ListError;
  }

  return {
    items: await response.json(),
    total: parseInt(response.headers.get('x-total-count') || '0', 10),
  };
};

export type QueryList<T extends object> = (
  apiurl: string,
  token: string,
  options: ListOptions,
) => Promise<ListResponse<T> | ListError>;

// Delete
export type DeleteError = BaseError;

export const queryDelete = async (
  model: string,
  apiurl: string,
  token: string,
  id: string,
): Promise<undefined | DeleteError> => {
  const response = await request<DeleteError>(apiurl, {
    query: `/${model}/${id}`,
    method: 'DELETE',
    headers: { Authorization: token },
  });
  if (typeof response === 'string') {
    return response;
  }
  return undefined;
};

export type QueryDelete = (
  apiurl: string,
  token: string,
  id: string,
) => Promise<undefined | DeleteError>;

// Get
export type GetError = BaseError;

export const queryGet = async <T extends object>(
  model: string,
  apiurl: string,
  token: string,
  id: string,
): Promise<T | GetError> => {
  const response = await request<DeleteError>(apiurl, {
    query: `/${model}/${id}`,
    method: 'GET',
    headers: { Authorization: token },
  });
  if (typeof response === 'string') {
    return response;
  }
  return response.json();
};

export type QueryGet<T extends object> = (
  apiurl: string,
  token: string,
  id: string,
) => Promise<T | ListError>;

// Create
export type CreateError = BaseError;

export const queryCreate = async <T extends object>(
  modelName: string,
  apiurl: string,
  token: string,
  model: Omit<T, 'id'>,
): Promise<T | CreateError> => {
  const response = await request<CreateError>(apiurl, {
    query: `/${modelName}`,
    method: 'POST',
    body: model,
    headers: { Authorization: token },
  });
  if (typeof response === 'string') {
    return response;
  }
  return response.json();
};

export type QueryCreate<T extends object> = (
  apiurl: string,
  token: string,
  model: T,
) => Promise<T | ListError>;

// Update
export type UpdateError = BaseError;

export const queryUpdate = async <T extends object>(
  modelName: string,
  apiurl: string,
  token: string,
  model: T,
  id: string,
): Promise<T | UpdateError> => {
  const response = await request<UpdateError>(apiurl, {
    query: `/${modelName}/${id}`,
    method: 'PUT',
    body: model,
    headers: { Authorization: token },
  });
  if (typeof response === 'string') {
    return response;
  }
  return response.json();
};

export type QueryUpdate<T extends object> = (
  apiurl: string,
  token: string,
  model: T,
) => Promise<T | ListError>;

export type QueryCrud<T extends object> = {
  list: QueryList<T>;
  get: QueryGet<T>;
  create: QueryCreate<T>;
  update: QueryUpdate<T>;
  delete: QueryDelete;
};
