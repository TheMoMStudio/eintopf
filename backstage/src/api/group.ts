// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import {
  queryCreate,
  queryDelete,
  queryGet,
  queryList,
  queryUpdate,
  ListOptions,
  BaseModel,
} from './crud';

export interface Group extends BaseModel {
  published: boolean;
  deactivated: boolean;
  name: string;
  link: string;
  email: string;
  description: string;
  image: string;
  alt: string;
  ownedBy: string[];
}

export type NewGroup = Omit<Group, 'id'>;

export const queryGroupList = (apiurl: string, token: string, options: ListOptions) =>
  queryList<Group>('groups', apiurl, token, options);

export const queryGroupCreate = (apiurl: string, token: string, group: NewGroup) => {
  return queryCreate<Group>('groups', apiurl, token, group);
};

export const queryGroupUpdate = (apiurl: string, token: string, group: Group) => {
  return queryUpdate<Group>('groups', apiurl, token, group, group.id);
};

export const queryGroupGet = (apiurl: string, token: string, id: string) =>
  queryGet<Group>('groups', apiurl, token, id);

export const queryGroupDelete = (apiurl: string, token: string, id: string) => {
  return queryDelete('groups', apiurl, token, id);
};

export const queryGroupCrud = {
  list: queryGroupList,
  get: queryGroupGet,
  create: queryGroupCreate,
  update: queryGroupUpdate,
  delete: queryGroupDelete,
};
