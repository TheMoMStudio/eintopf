// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

export const InternalServerError = 'InternalServerError';
export const UnkownError = 'UnkownError';

export type BaseError = typeof InternalServerError | typeof UnkownError;

export const mapError = (err: string): string => {
  switch (err) {
    case 'unauthorized':
      return 'Für diese Aktion bist du nicht Berechtigt';
    case 'invalid credentials':
      return 'E-Mail oder Passwort falsch';
    case 'deactivated':
      return 'Dein Account wurde vom Moderadtionsteam deaktiviert. Wende dich bite an mod@eintof.info um den Grund zu erfahren';
    case 'name already exists':
      return 'Es existiert bereits eine Veranstaltung mit diesem Namen';
    case 'email already exists':
      return 'Die E-Mail Adresse existiert bereits';
    case 'nickname already exists':
      return 'Der Spitzname existiert bereits';
    case 'invalid invitation token':
      return 'Der Einladungstoken ist ungültig';
    case 'invitation token already used':
      return 'Der Einladungstoken wurde bereits benutzt';
    case 'http: request body too large':
      return 'Die Datei ist zu groß';
    case InternalServerError:
      return 'Ups, auf der anderen Seite ist etwas schief gegangen';
    case UnkownError:
      return 'Ups, es ist ein unerwarteter Fehler Aufgetreten';
    default:
      return err;
  }
};
