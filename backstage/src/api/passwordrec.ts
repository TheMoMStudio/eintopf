// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { BaseError } from './error';
import request from './request';

export const InvalidRecoveryTokenError = 'InvalidRecoveryToken';
export const ExpiredRecoveryTokenError = 'ExpiredRecoveryToken';
export type SetPasswordError =
  | typeof InvalidRecoveryTokenError
  | typeof ExpiredRecoveryTokenError
  | BaseError;

export const querySetPassword = async (
  apiurl: string,
  token: string,
  password: string,
): Promise<undefined | SetPasswordError> => {
  const response = await request<SetPasswordError>(apiurl, {
    query: '/passwordrec/setPassword',
    method: 'POST',
    body: { token, password },
    errorMapping: {
      'invalid token': InvalidRecoveryTokenError,
      'expired token': ExpiredRecoveryTokenError,
    },
  });
  if (typeof response === 'string') {
    return response;
  }
  return Promise.resolve(undefined);
};

export type RequestPasswordRecoveryError = BaseError;

export interface PasswordRecoveryRequest {
  email: string;
}

export const queryRequestPasswordRecovery = async (
  apiurl: string,
  data: PasswordRecoveryRequest,
): Promise<undefined | RequestPasswordRecoveryError> => {
  const response = await request<RequestPasswordRecoveryError>(apiurl, {
    query: '/passwordrec/requestRecovery',
    method: 'POST',
    body: data,
  });
  if (typeof response === 'string') {
    return response;
  }
  return Promise.resolve(undefined);
};
