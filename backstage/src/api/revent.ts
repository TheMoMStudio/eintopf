// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { formatRFC3339 } from 'date-fns/formatRFC3339';
import { newDate } from '../utils/date';
import {
  queryCreate,
  queryDelete,
  queryGet,
  queryList,
  queryUpdate,
  ListOptions,
  BaseModel,
} from './crud';
import { Event } from './event';
import request from './request';

export interface RepeatingEvent extends BaseModel {
  published: boolean;
  intervals: Interval[];
  start: string;
  end: string;
  name: string;
  organizers: string[];
  location: string;
  location2: string;
  address: string;
  lat: number;
  lng: number;
  description: string;
  ownedBy: string[];
  image: string;
  alt: string;
  tags?: string[];
}

export type NewRepeatingEvent = Omit<RepeatingEvent, 'id'>;

export interface Interval {
  type: IntervalType;
  interval: number;
  weekDay: number;
  date?: string;
}

export type IntervalType = 'day' | 'week' | 'month';

const MODEL_NAME = 'revents';
export const queryRepeatingEventList = (apiurl: string, token: string, options: ListOptions) =>
  queryList<RepeatingEvent>(MODEL_NAME, apiurl, token, options);

export const queryRepeatingEventCreate = (
  apiurl: string,
  token: string,
  event: NewRepeatingEvent,
) => queryCreate<RepeatingEvent>(MODEL_NAME, apiurl, token, event);

export const queryRepeatingEventUpdate = (
  apiurl: string,
  token: string,
  revent: RepeatingEvent,
) => {
  return queryUpdate<RepeatingEvent>(MODEL_NAME, apiurl, token, revent, revent.id);
};

export const queryRepeatingEventGet = (apiurl: string, token: string, id: string) =>
  queryGet<RepeatingEvent>(MODEL_NAME, apiurl, token, id);

export const queryRepeatingEventDelete = (apiurl: string, token: string, id: string) => {
  return queryDelete(MODEL_NAME, apiurl, token, id);
};

export const queryRepeatingEventCrud = {
  list: queryRepeatingEventList,
  get: queryRepeatingEventGet,
  create: queryRepeatingEventCreate,
  update: queryRepeatingEventUpdate,
  delete: queryRepeatingEventDelete,
};

type GenerateError = string;

export const queryGenerateEvents = async (
  timezone: string,
  apiurl: string,
  token: string,
  id: string,
  start: string,
  end: string,
): Promise<Event[] | GenerateError> => {
  const response = await request<GenerateError>(apiurl, {
    query: `/${MODEL_NAME}/${id}/generate`,
    queryParamaters: {
      start: formatRFC3339(newDate(timezone, start)),
      end: formatRFC3339(newDate(timezone, end)),
    },
    method: 'GET',
    headers: { Authorization: token },
  });
  if (typeof response === 'string') {
    return response as GenerateError;
  }

  return (await response.json()) as Event[];
};
