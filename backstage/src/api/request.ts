// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { formatRFC3339 } from 'date-fns/formatRFC3339';
import { InternalServerError, UnkownError } from './error';

export const buildQuery = (
  query: string,
  queryParamaters?: Record<string, string | string[] | number | boolean | Date | undefined>,
  omitEmptyFields = false,
): string => {
  let q = `${query}`;
  if (queryParamaters !== undefined) {
    const params = new URLSearchParams();
    Object.keys(queryParamaters).forEach((key) => {
      const value = queryParamaters[key];
      if (value === undefined) {
        return;
      }
      if (Array.isArray(value)) {
        if (omitEmptyFields && value.length === 0) {
          return;
        }
        value.forEach((v) => params.append(key, v));
        return;
      }
      if (value instanceof Date) {
        params.append(key, formatRFC3339(value));
        return;
      }
      if (typeof typeof value === 'string') {
        if (omitEmptyFields && value === '') {
          return;
        }
        params.append(key, value as string);
        return;
      }
      if (typeof typeof value === 'boolean') {
        params.append(key, (value as boolean) === true ? 'true' : 'false');
        return;
      }
    });
    if (params.toString() !== '') {
      q += `?${params.toString()}`;
    }
  }
  return q;
};

export interface ApiError {
  error: string;
}

export type RequestMethods = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH';

export interface RequestOptions {
  query: string;
  queryParamaters?: Record<string, string | string[] | number | boolean | Date | undefined>;
  method: RequestMethods;
  headers?: HeadersInit;
  body?: Record<string, any>;
  bodyRaw?: any;
  errorMapping?: Record<string, string>;
}

export const request = async <E extends string>(
  apiurl: string,
  options: RequestOptions,
): Promise<Response | Record<string, any> | E> => {
  try {
    const query = buildQuery(options.query, options.queryParamaters);

    let body = options.bodyRaw;
    if (options.body !== undefined) {
      body = JSON.stringify(options.body);
    }

    const result = await fetch(`${apiurl}${query}`, {
      method: options.method,
      body,
      headers: options.headers,
    });

    switch (result.status) {
      case 200:
        return result;
      case 400:
      case 401:
      case 402:
      case 403:
        const err = ((await result.json()) as ApiError).error;
        if (options.errorMapping && options.errorMapping[err]) {
          return Promise.resolve(options.errorMapping[err]) as unknown as E;
        }
        return err as unknown as E;
      case 500:
        return InternalServerError as unknown as E;
      default:
        return UnkownError as unknown as E;
    }
  } catch (err) {
    console.error(err);
    return UnkownError as unknown as E;
  }
};

export default request;
