// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { queryDelete, queryGet, queryList, queryUpdate, ListOptions, QueryCrud } from './crud';
import { BaseError } from './error';
import request from './request';

export interface Action {
  name: string;
  calls: number;
  lastCall: string;
  lastCalledBy: string;
  error: string;
}

export const queryActionList = (apiurl: string, token: string, options: ListOptions) =>
  queryList<Action>('actions', apiurl, token, options);

export const queryActionUpdate = (apiurl: string, token: string, action: Action) => {
  return queryUpdate<Action>('actions', apiurl, token, action, action.name);
};

export const queryNotificationGet = (apiurl: string, token: string, id: string) =>
  queryGet<Action>('actions', apiurl, token, id);

export const queryActionDelete = (apiurl: string, token: string, id: string) => {
  return queryDelete('actions', apiurl, token, id);
};

export const queryActionRun = async (apiurl: string, token: string, name: string) => {
  const response = await request<BaseError>(apiurl, {
    query: `/actions/run`,
    method: 'POST',
    body: { name },
    headers: { Authorization: token },
  });
  if (typeof response === 'string') {
    return response;
  }
  return;
};

export const queryActionCrud: QueryCrud<Action> = {
  list: queryActionList,
  get: queryNotificationGet,
  create: () => Promise.reject('create not implemented for actions'),
  update: queryActionUpdate,
  delete: queryActionDelete,
};
