// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { Involved } from './event';
import { Group } from './group';
import { Place } from './place';
import { RepeatingEvent } from './revent';

export interface EventDocument {
  id: string;
  published: boolean;
  canceled: boolean;
  name: string;
  organizers: Organizer[];
  involved: Involved[];
  location: Location;
  location2: string;
  start: string;
  end?: string;
  description: string;
  image: string;
  tags?: string[];
  children?: EventDocument[];
  repeatingEvent?: RepeatingEvent;
}

export interface Organizer {
  group?: Group;
  name?: string;
}

export interface Location {
  place?: Place;
  name?: string;
  address: string;
  lat: number;
  lng: number;
}

export interface GroupDocument {
  id: string;
  published: boolean;
  deactivated: boolean;
  name: string;
  link: string;
  email: string;
  description: string;
  image: string;
  ownedBy: string[];
}

export interface PlaceDocument {
  id: string;
  published: boolean;
  deactivated: boolean;
  name: string;
  address: string;
  lat: number;
  lng: number;
  link: string;
  email: string;
  description: string;
  image: string;
  ownedBy: string[];
}
