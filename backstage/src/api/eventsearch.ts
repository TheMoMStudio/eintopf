// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { BaseError } from './error';
import request from './request';
import { RepeatingEvent } from './revent';
import { Group } from './group';
import { Place } from './place';
import { EventSearchFilter } from './eventsearch_filter';
import { EventSearchAggregation } from './eventsearch_aggregation';
import { SearchBucket } from './search';
import { Involved } from './event';

export interface EventSearchOptions {
  query: string;
  sort: string;
  page: number;
  pageSize: number;
  filters: EventSearchFilter[];
  aggregations: { [name: string]: EventSearchAggregation };
  omit: 'description'[];
}

export interface EventDocument {
  listed: boolean;
  id: string;
  canceled: boolean;
  published: boolean;
  name: string;
  organizers: EventOrganizer[];
  involved: Involved[];
  location: EventLocation;
  location2: string;
  description: string;
  start: string;
  end?: string;
  tags: string[];
  image: string;
  children?: EventDocument[];
  repeatingEvent?: RepeatingEvent;
}

export interface EventOrganizer {
  group?: Group;
  name?: string;
}

export interface EventLocation {
  place?: Place;
  name?: string;
  address: string;
  lat: number;
  lng: number;
}

export interface EventSearchResult {
  events: EventDocument[];
  total: number;
  buckets: { [key: string]: SearchBucket };
}

export type SearchError = BaseError;

export const queryEventSearch = async (
  apiurl: string,
  options: EventSearchOptions,
): Promise<EventSearchResult | SearchError> => {
  const response = await request<SearchError>(apiurl, {
    query: '/eventsearch',
    queryParamaters: {
      query: options.query,
      sort: options.sort,
      page: options.page.toString(),
      pageSize: options.pageSize.toString(),
      ...filtersToQueryParams(options.filters),
      ...aggregationsToQueryParams(options.aggregations),
      omit: options.omit,
    },
    method: 'GET',
  });
  if (typeof response === 'string') {
    return response as SearchError;
  }
  if (response instanceof Response) {
    return response.json() as Promise<EventSearchResult>;
  }
  return response as EventSearchResult;
};

export const filtersToQueryParams = (filters: EventSearchFilter[]) => {
  return filters.reduce((obj, item) => ({ ...obj, ...item }), {});
};

export const aggregationsToQueryParams = (aggregations: {
  [name: string]: EventSearchAggregation;
}): Record<string, string | string[] | Date | boolean> => {
  let params: Record<string, string | string[] | Date | boolean> = {};
  const aggregationParam: string[] = [];

  Object.entries(aggregations).forEach(([name, aggregation]) => {
    aggregationParam.push(`${name}-${aggregation.field}-${aggregation.type}`);
    const filters = filtersToQueryParams(aggregation.filters);

    const filters2 = Object.assign(
      {},
      ...Object.keys(filters).map((key) => ({
        [`${name}-${key}`]: filters[key as keyof EventSearchFilter],
      })),
    );
    params = {
      ...params,
      ...filters2,
    };
  });
  params['aggregations'] = aggregationParam;

  return params;
};

export const extractFilter = (
  filters: EventSearchFilter[],
  field: string,
): EventSearchFilter | null => {
  const filter = filters.filter((f) => f === field);
  if (filter.length === 0) return null;
  return filter[0];
};

// applyFiltersToAggregations applies a set of filters to the given
// aggregations. All filters are added to all aggregation, except if they apply
// to the same field.
export const applyFiltersToAggregations = (
  aggregations: { [key: string]: EventSearchAggregation },
  filters: EventSearchFilter[],
): { [key: string]: EventSearchAggregation } => {
  const newAggregations: { [key: string]: EventSearchAggregation } = {};
  Object.entries(aggregations).forEach(([key, aggregation]) => {
    newAggregations[key] = {
      ...aggregation,
      filters: filters.filter(
        (filter) =>
          Object.keys(filter).filter((k) => k.includes(aggregation.field)).length !=
          Object.keys(filter).length,
      ),
    };
  });
  return newAggregations;
};
