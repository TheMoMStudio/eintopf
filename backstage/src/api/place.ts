// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import {
  queryCreate,
  queryDelete,
  queryGet,
  queryList,
  queryUpdate,
  ListOptions,
  BaseModel,
} from './crud';

export interface Place extends BaseModel {
  published: boolean;
  deactivated: boolean;
  name: string;
  address: string;
  lat: number;
  lng: number;
  link: string;
  email: string;
  description: string;
  image: string;
  alt: string;
  ownedBy: string[];
}

export type NewPlace = Omit<Place, 'id'>;

export const queryPlaceList = (apiurl: string, token: string, options: ListOptions) =>
  queryList<Place>('places', apiurl, token, options);

export const queryPlaceCreate = (apiurl: string, token: string, place: NewPlace) => {
  return queryCreate<Place>('places', apiurl, token, place);
};

export const queryPlaceUpdate = (apiurl: string, token: string, place: Place) => {
  return queryUpdate<Place>('places', apiurl, token, place, place.id);
};

export const queryPlaceGet = (apiurl: string, token: string, id: string) =>
  queryGet<Place>('places', apiurl, token, id);

export const queryPlaceDelete = (apiurl: string, token: string, id: string) => {
  return queryDelete('places', apiurl, token, id);
};

export const queryPlaceCrud = {
  list: queryPlaceList,
  get: queryPlaceGet,
  create: queryPlaceCreate,
  update: queryPlaceUpdate,
  delete: queryPlaceDelete,
};
