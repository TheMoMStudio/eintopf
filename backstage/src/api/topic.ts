// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { queryCreate, queryDelete, queryGet, queryList, queryUpdate, ListOptions } from './crud';

export interface Topic {
  id: string;
  name: string;
  description: string;
}

export interface Involved {
  name: string;
  description: string;
}

export type NewTopic = Omit<Topic, 'id'>;

export const queryTopicList = (apiurl: string, token: string, options: ListOptions) =>
  queryList<Topic>('topics', apiurl, token, options);

export const queryTopicCreate = (apiurl: string, token: string, topic: NewTopic) => {
  return queryCreate<Topic>('topics', apiurl, token, topic);
};

export const queryTopicUpdate = (apiurl: string, token: string, topic: Topic) => {
  return queryUpdate<Topic>('topics', apiurl, token, topic, topic.id);
};

export const queryTopicGet = (apiurl: string, token: string, id: string) =>
  queryGet<Topic>('topics', apiurl, token, id);

export const queryTopicDelete = (apiurl: string, token: string, id: string) => {
  return queryDelete('topics', apiurl, token, id);
};

export const queryTopicCrud = {
  list: queryTopicList,
  get: queryTopicGet,
  create: queryTopicCreate,
  update: queryTopicUpdate,
  delete: queryTopicDelete,
};
