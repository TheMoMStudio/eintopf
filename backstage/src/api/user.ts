// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { Role } from './auth';
import {
  queryCreate,
  queryDelete,
  queryGet,
  queryList,
  queryUpdate,
  ListOptions,
  BaseModel,
} from './crud';

export interface User extends BaseModel {
  id: string;
  deactivated: boolean;
  nickname: string;
  email: string;
  role: Role;
}

export const queryUserList = (apiurl: string, token: string, options: ListOptions) =>
  queryList<User>('users', apiurl, token, options);

export const queryUserCreate = (apiurl: string, token: string, user: User) =>
  queryCreate<User>('users', apiurl, token, user);

export const queryUserUpdate = (apiurl: string, token: string, user: User) =>
  queryUpdate<User>('users', apiurl, token, user, user.id);

export const queryUserGet = (apiurl: string, token: string, id: string) =>
  queryGet<User>('users', apiurl, token, id);

export const queryUserDelete = (apiurl: string, token: string, id: string) =>
  queryDelete('users', apiurl, token, id);

export const queryUserCrud = {
  list: queryUserList,
  get: queryUserGet,
  create: queryUserCreate,
  update: queryUserUpdate,
  delete: queryUserDelete,
};
