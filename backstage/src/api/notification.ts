// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import {
  queryDelete,
  queryGet,
  queryList,
  queryUpdate,
  ListOptions,
  QueryCrud,
  queryCreate,
} from './crud';

export interface Notification {
  id: string;
  createdAt: string;
  userId: string;
  subject: string;
  message: string;
  viewed: boolean;
  linkTitle: string;
  linkUrl: string;
}

export type NewNotification = Omit<Notification, 'id'>;

export const queryNotificationList = (apiurl: string, token: string, options: ListOptions) =>
  queryList<Notification>('notifications', apiurl, token, options);

export const queryNotificationUpdate = (
  apiurl: string,
  token: string,
  notification: Notification,
) => {
  return queryUpdate<Notification>('notifications', apiurl, token, notification, notification.id);
};

export const queryNotificationCreate = (
  apiurl: string,
  token: string,
  notification: NewNotification,
) => {
  return queryCreate<Notification>('notifications', apiurl, token, notification);
};

export const queryNotificationGet = (apiurl: string, token: string, id: string) =>
  queryGet<Notification>('notifications', apiurl, token, id);

export const queryNotificationDelete = (apiurl: string, token: string, id: string) => {
  return queryDelete('notifications', apiurl, token, id);
};

export const queryNotificationCrud: QueryCrud<Notification> = {
  list: queryNotificationList,
  get: queryNotificationGet,
  create: () => Promise.reject('create not implemented for notifications'),
  update: queryNotificationUpdate,
  delete: queryNotificationDelete,
};
