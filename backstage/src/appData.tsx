// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, ComponentChildren, FunctionComponent, createContext } from 'preact';
import { useContext } from 'preact/hooks';
import { createNotificationState, NotificationHandler } from './signals/notification';

export interface AppData {
  apiurl: string;
  timezone: string;
  notifications: NotificationHandler;
}

export const AppDataContext = createContext<AppData>({
  apiurl: '/api/v1',
  timezone: 'Europe/London',
  notifications: createNotificationState(),
});

export interface AppDataProviderProps {
  appData: AppData;
  children: ComponentChildren;
}

export const AppDataProvider: FunctionComponent<AppDataProviderProps> = ({ appData, children }) => (
  <AppDataContext.Provider value={appData}>{children}</AppDataContext.Provider>
);

export const useAppData = (): AppData => useContext(AppDataContext);
