// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

export const validateRequired = (value: any): string | undefined => {
  if (!value) return 'Das Feld darf nicht Leer sein!';
  return undefined;
};

export default validateRequired;
