// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { Role } from '../api/auth';

export type NeedsRole = Role | ((role: Role) => boolean) | undefined;
export const checkNeedsRole = (needsRole: NeedsRole, role: Role): boolean => {
  switch (typeof needsRole) {
    case 'string':
      return needsRole !== role;
    case 'function':
      return !needsRole(role);
    default:
      return false;
  }
};
