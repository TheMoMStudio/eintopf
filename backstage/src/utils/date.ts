// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { de } from 'date-fns/locale';
import { formatInTimeZone, getTimezoneOffset, fromZonedTime } from 'date-fns-tz';
import { add } from 'date-fns/add';

export const format = (
  timezone: string,
  date: Date | number,
  formatStr: string,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  options?: {
    weekStartsOn?: 0 | 1 | 2 | 3 | 4 | 5 | 6;
    firstWeekContainsDate?: 1 | 2 | 3 | 4 | 5 | 6 | 7;
    additionalDigits?: 0 | 1 | 2;
    includeSeconds?: boolean;
    addSuffix?: boolean;
    unit?: 'second' | 'minute' | 'hour' | 'day' | 'month' | 'year';
    roundingMethod?: 'floor' | 'ceil' | 'round';
    awareOfUnicodeTokens?: boolean;
  },
): string => formatInTimeZone(date, timezone, formatStr, { locale: de });

/**
 * newDate returns a js Date object in the specified timezone.
 */
export const newDate = (timezone: string, date: Date | string | undefined = new Date()): Date => {
  const wantTzDiff = getTimezoneOffset(timezone, new Date()) / 1000 / 60;
  const gotTzDiff = -1 * new Date().getTimezoneOffset();
  return add(fromZonedTime(date, timezone), { minutes: wantTzDiff - gotTzDiff });
};
