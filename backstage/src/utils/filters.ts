// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { SearchFilter, SearchAggregation } from '../api/search';

export const extractFilter = (filters: SearchFilter[], field: string): SearchFilter | null => {
  const filter = filters.filter((f) => f.filter.field === field);
  if (filter.length === 0) return null;
  return filter[0];
};

// applyFiltersToAggregations applies a set of filters to the given
// aggregations. All filters are added to all aggregation, except if they apply
// to the same field.
export const applyFiltersToAggregations = (
  aggregations: { [key: string]: SearchAggregation },
  filters: SearchFilter[],
): { [key: string]: SearchAggregation } => {
  const newAggregations: { [key: string]: SearchAggregation } = {};
  Object.entries(aggregations).forEach(([key, aggregation]) => {
    newAggregations[key] = {
      ...aggregation,
      filters: filters.filter((filter) => filter.filter.field !== aggregation.field),
    };
  });
  return newAggregations;
};
