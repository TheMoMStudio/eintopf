// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

export type FieldValidator = (value: any | undefined) => string | undefined;

export const composeValidators: (...validators: (FieldValidator | undefined)[]) => FieldValidator =
  (...validators) =>
  (value) =>
    validators.reduce((error: string | undefined, validator: FieldValidator | undefined) => {
      if (error) {
        return error;
      }
      if (validator === undefined) {
        return undefined;
      }
      return validator(value);
    }, undefined);

export default composeValidators;
