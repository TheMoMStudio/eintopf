// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

export const validateLink = (value: string | undefined): string | undefined => {
  if (!value) return undefined;
  if (value.startsWith('http://') || value.startsWith('https://')) {
    return undefined;
  }
  return 'Der Link muss mit https:// oder http:// anfangen';
};

export default validateLink;
