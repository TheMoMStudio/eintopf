// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { newDate } from './date';

describe('newDate', () => {
  it.skip('TIMEZONE=Europe/London', () => {
    expect(newDate('Europe/London').getHours()).toEqual(new Date().getHours());
    expect(newDate('Europe/London', '2021-05-02T20:20:00').getHours()).toEqual(
      new Date('2021-05-02T20:20:00').getHours(),
    );
  });
});
