// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, ComponentChildren } from 'preact';
import { useReducer } from 'preact/hooks';
import { initialStore, reducer } from './store';
import { dispatchConext, storeContext } from './context';

export interface StoreProviderProps {
  children: ComponentChildren;
}

export const StoreProvider: FunctionComponent<StoreProviderProps> = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialStore);

  return (
    <dispatchConext.Provider value={dispatch}>
      <storeContext.Provider value={state}>{children}</storeContext.Provider>
    </dispatchConext.Provider>
  );
};

export default StoreProvider;
