// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { Role } from '../api/auth';

export interface User {
  id: string;
  role: Role;
  token: string;
}

export interface Backstage {
  [key: string]: any;
}

export interface Store {
  user?: User;
  backstage?: Backstage;
}

const STORE_KEY = 'store';

export const loadStore = (): Store => {
  // Check if window object is avaible, for prerender to work
  if (typeof window === 'undefined') {
    return {};
  }
  const loaded = localStorage.getItem(STORE_KEY);
  if (!loaded) return {};

  const parsed = JSON.parse(loaded);
  if (!parsed) return {};
  return parsed;
};

export const saveStore = (store: Store): void => {
  if (Object.keys(store).length > 0) {
    localStorage.setItem(STORE_KEY, JSON.stringify(store));
  }
};

export const deleteStore = (): void => {
  localStorage.removeItem(STORE_KEY);
};

export const initialStore: Store = loadStore();

export type LoginAction = { type: 'login'; user: User };
export type LogoutAction = { type: 'logout' };
export type SetBackstageAction = { type: 'setBackstage'; backstage: Backstage };
export type Action = LoginAction | LogoutAction | SetBackstageAction;

export const createLoginAction = (user: User): LoginAction => ({
  type: 'login',
  user,
});

export const createLogoutAction = (): LogoutAction => ({
  type: 'logout',
});

export const createSetBackstageAction = (backstage: Backstage): SetBackstageAction => ({
  type: 'setBackstage',
  backstage,
});

export const reducer = (store: Store, action: Action): Store => {
  switch (action.type) {
    case 'login':
      return { ...store, ...{ user: action.user } };
    case 'logout':
      return { ...store, ...{ user: undefined, backstage: undefined } };
    case 'setBackstage':
      return { ...store, ...{ backstage: action.backstage } };
    default:
      return store;
  }
};
