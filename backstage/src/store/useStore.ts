// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { useContext, useEffect } from 'preact/hooks';
import { saveStore, Action, Store } from './store';
import { dispatchConext, storeContext } from './context';

export const useStore = (): [Store, (action: Action) => void] => {
  const store = useContext(storeContext);
  const dispatch = useContext(dispatchConext);
  useEffect(() => saveStore(store), [store]);
  return [store, dispatch];
};

export default useStore;
