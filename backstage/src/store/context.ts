// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { createContext } from 'preact';
import { initialStore, Action, Store } from './store';

export const storeContext = createContext<Store>(initialStore);
export const dispatchConext = createContext<(action: Action) => void>(() => 0);
