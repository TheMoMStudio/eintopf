// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import Router, { Route } from 'preact-router';
import BackstageActionsPage from './routes/BackstageActionsPage';
import BackstageCategoryCreatePage from './routes/BackstageCategoryCreatePage';
import BackstageCategoryListPage from './routes/BackstageCategoryListPage';
import BackstageCategoryUpdatePage from './routes/BackstageCategoryUpdatePage';
import BackstageEventCreatePage from './routes/BackstageEventCreatePage';
import BackstageEventListPage from './routes/BackstageEventListPage';
import BackstageEventUpdatePage from './routes/BackstageEventUpdatePage';
import BackstageFAQPage from './routes/BackstageFAQPage';
import BackstageFirstStepsPage from './routes/BackstageFirstSteps';
import BackstageGroupCreatePage from './routes/BackstageGroupCreatePage';
import BackstageGroupListPage from './routes/BackstageGroupListPage';
import BackstageGroupUpdatePage from './routes/BackstageGroupUpdatePage';
import BackstageInvitationPage from './routes/BackstageInvitationPage';
import BackstageInvitePage from './routes/BackstageInvitePage';
import BackstageNotificationCreatePage from './routes/BackstageNotificationCreatePage';
import BackstageNotificationListPage from './routes/BackstageNotificationListPage';
import BackstageNotificationViewPage from './routes/BackstageNotificationViewPage';
import BackstagePage from './routes/BackstagePage';
import BackstagePasswordRecoveryPage from './routes/BackstagePasswordRecoveryPage';
import BackstagePasswordRecoveryRequestPage from './routes/BackstagePasswordRecoveryRequestPage';
import BackstagePlaceCreatePage from './routes/BackstagePlaceCreatePage';
import BackstagePlaceListPage from './routes/BackstagePlaceListPage';
import BackstagePlaceUpdatePage from './routes/BackstagePlaceUpdatePage';
import BackstageRepeatingEventCreatePage from './routes/BackstageRepeatingEventCreatePage';
import BackstageRepeatingEventListPage from './routes/BackstageRepeatingEventListPage';
import BackstageRepeatingEventUpdatePage from './routes/BackstageRepeatingEventUpdatePage';
import BackstageTopicCreatePage from './routes/BackstageTopicCreatePage';
import BackstageTopicUpdatePage from './routes/BackstageTopicUpdatePage';
import BackstageUsagePrinciplesPage from './routes/BackstageUsagePrinciplesPage';
import BackstageUserAccountPage from './routes/BackstageUserAccountPage';
import BackstageUserListPage from './routes/BackstageUserListPage';
import BackstageUserUpdatePage from './routes/BackstageUserUpdatePage';
import StoreProvider from './store/StoreProvider';
import './App.css';
import { AppData, AppDataProvider } from './appData';
import BackstageCmsPage from './routes/BackstageCmsPage';
import BackstageCmsThemePage from './routes/BackstageCmsThemePage';
import { createNotificationState } from './signals/notification';
import BackstageNotifications from './components/BackstageNotifications';

export const App: FunctionComponent<AppData> = ({ apiurl, timezone }) => {
  return (
    <div className="App">
      <link rel="stylesheet" href="/backstage/index.css" />
      <AppDataProvider appData={{ apiurl, timezone, notifications: createNotificationState() }}>
        <BackstageNotifications />
        <StoreProvider>
          <Router>
            <Route path="/backstage" component={BackstagePage} />
            <Route path="/backstage/first-steps" component={BackstageFirstStepsPage} />
            <Route path="/backstage/faq" component={BackstageFAQPage} />
            <Route path="/backstage/usage-principles" component={BackstageUsagePrinciplesPage} />

            <Route path="/backstage/events" component={BackstageEventListPage} />
            <Route path="/backstage/events/new" component={BackstageEventCreatePage} />
            <Route path="/backstage/events/:id" component={BackstageEventUpdatePage} />

            <Route path="/backstage/repeatingevents" component={BackstageRepeatingEventListPage} />
            <Route
              path="/backstage/repeatingevents/new"
              component={BackstageRepeatingEventCreatePage}
            />
            <Route
              path="/backstage/repeatingevents/:id"
              component={BackstageRepeatingEventUpdatePage}
            />
            <Route path="/backstage/groups" component={BackstageGroupListPage} />
            <Route path="/backstage/groups/new" component={BackstageGroupCreatePage} />
            <Route path="/backstage/groups/:id" component={BackstageGroupUpdatePage} />

            <Route path="/backstage/places" component={BackstagePlaceListPage} />
            <Route path="/backstage/places/new" component={BackstagePlaceCreatePage} />
            <Route path="/backstage/places/:id" component={BackstagePlaceUpdatePage} />

            <Route path="/backstage/account" component={BackstageUserAccountPage} />
            <Route path="/backstage/users" component={BackstageUserListPage} />
            <Route path="/backstage/users/:id" component={BackstageUserUpdatePage} />

            <Route path="/backstage/invite" component={BackstageInvitePage} />
            <Route path="/backstage/invitation/:token" component={BackstageInvitationPage} />

            <Route path="/backstage/passwordRecovery" component={BackstagePasswordRecoveryPage} />
            <Route
              path="/backstage/requestPasswordRecovery"
              component={BackstagePasswordRecoveryRequestPage}
            />

            <Route path="/backstage/actions" component={BackstageActionsPage} />

            <Route path="/backstage/notifications" component={BackstageNotificationListPage} />
            <Route path="/backstage/notifications/:id" component={BackstageNotificationViewPage} />
            <Route
              path="/backstage/notifications/new/:userId"
              component={BackstageNotificationCreatePage}
            />

            <Route path="/backstage/cms" component={BackstageCmsPage} />
            <Route path="/backstage/cms/theme" component={BackstageCmsThemePage} />
            <Route path="/backstage/cms/theme/:option" component={BackstageCmsThemePage} />
            <Route
              path="/backstage/cms/theme/:option/:filetype"
              component={BackstageCmsThemePage}
            />
            <Route
              path="/backstage/cms/theme/:option/:filetype/:filePath"
              component={BackstageCmsThemePage}
            />

            <Route path="/backstage/categories" component={BackstageCategoryListPage} />
            <Route path="/backstage/categories/new" component={BackstageCategoryCreatePage} />
            <Route path="/backstage/categories/:id" component={BackstageCategoryUpdatePage} />
            <Route path="/backstage/topics/new" component={BackstageTopicCreatePage} />
            <Route path="/backstage/topics/:id" component={BackstageTopicUpdatePage} />
          </Router>
        </StoreProvider>
      </AppDataProvider>
    </div>
  );
};

export default App;
