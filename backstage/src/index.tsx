// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import register from 'preact-custom-element';
import App from './App';
import './index.css';

register(App as any, 'x-backstage', ['apiurl'], { shadow: true });
