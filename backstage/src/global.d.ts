// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

declare namespace JSX {
  type Element = preact.JSX.Element;
  type HTMLAttributes = preact.JSX.HTMLAttributes;
}

declare module '*.svg' {
  const value: any;
  export = value;
}

declare module '*.png' {
  const value: any;
  export = value;
}

declare module '*.jpg' {
  const value: any;
  export = value;
}

declare module '*.css' {
  const mapping: Record<string, string>;
  export default mapping;
}
