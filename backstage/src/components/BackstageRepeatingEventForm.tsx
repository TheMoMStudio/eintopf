// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import { add } from 'date-fns/add';
import { isBefore } from 'date-fns/isBefore';
import { NewRepeatingEvent } from '../api/revent';
import BackstageFormInputArrayInterval from '../components/BackstageFormInputArrayInterval';
import BackstageFormInputArrayTag from '../components/BackstageFormInputArrayTag';
import BackstageFormInputArrayUser from '../components/BackstageFormInputArrayUser';
import BackstageFormInputDateTime from '../components/BackstageFormInputDateTime';
import BackstageFormInputImage from '../components/BackstageFormInputImage';
import BackstageFormInputReferencePlace from '../components/BackstageFormInputReferencePlace';
import BackstageFormInputText from '../components/BackstageFormInputText';
import BackstageFormInputTextLong from '../components/BackstageFormInputTextLong';
import BackstageFormInputArrayGroup from '../components/BackstageFormInputArrayGroup';
import { newDate } from '../utils/date';
import { useAppData } from '../appData';
import BackstageFormButtonLatLng from './BackstageFormButtonLatLng';
import BackstageFormInputNumber from './BackstageFormInputNumber';
import BackstageFormInputReferenceCategory from './BackstageFormInputReferenceCategory';
import BackstageFormInputReferenceTopic from './BackstageFormInputReferenceTopic';

export const validateRepeatingEvent = <E extends NewRepeatingEvent>(
  event: E,
  timezone: string,
): string | undefined => {
  if (event.end != null && isBefore(newDate(timezone, event.end), newDate(timezone, event.start))) {
    return 'Der Beginn einer Veranstaltung muss vor dem Ende einer Veranstaltung sein.';
  }
  return undefined;
};

export const preSubmitRepeatingEvent = <E extends NewRepeatingEvent>(model: E): E => {
  const event = model;
  if (model.tags) {
    // Remove any empty tags
    event.tags = model.tags.filter((tag) => tag !== '');
  }
  return event;
};

export const BackstageRepeatingEventFormFields: FunctionComponent = () => {
  const { timezone } = useAppData();
  return (
    <Fragment>
      <BackstageFormInputText field="name" label="Titel der einzelnen Veranstaltung" required />
      <BackstageFormInputArrayGroup
        field="organizers"
        label="Veranstalter*in"
        info="Hier kannst du eine Gruppe zuordnen, bei der du als Benutzer*in eingetragen bist. Alternativ dazu kannst du manuell eine Veranstalter*in eintragen."
      />
      <BackstageFormInputReferencePlace
        field="location"
        label="Ort der Veranstaltung"
        info="Hier kannst du einen Ort zuordnen, der schon im EINTOPF angelegt wurde. Alternativ dazu kannst du manuell einen Ort eintragen."
      />
      <BackstageFormInputText
        field="location2"
        label="Ort Zusatz"
        info="Zum Beispiel Raum, Zelt, Hinterhof, ..."
      />
      <BackstageFormInputText
        field="address"
        label="Adresse"
        info="z.B. Hauptstr. 1, Stuttgart"
        hideWhen={{
          field: 'location',
          is: (value) => (value as string).includes('id:') || (value as string) == '',
        }}
      />
      <BackstageFormButtonLatLng
        addressField="address"
        latField="lat"
        lngField="lng"
        info="Damit der Ort auf der Karte angezeigt werden kann müssen die Koordinaten eingetragen werden. Über den Button können diese aus der Adresse generiert werden."
        hideWhen={{
          field: 'location',
          is: (value) => (value as string).includes('id:') || (value as string) == '',
        }}
      />
      <BackstageFormInputNumber
        field="lat"
        label="Breitengrad"
        hideWhen={{
          field: 'location',
          is: (value) => (value as string).includes('id:') || (value as string) == '',
        }}
      />
      <BackstageFormInputNumber
        field="lng"
        label="Längengrad"
        hideWhen={{
          field: 'location',
          is: (value) => (value as string).includes('id:') || (value as string) == '',
        }}
      />
      <BackstageFormInputDateTime
        field="start"
        label="Beginn einer Veranstaltung"
        defaultValue={newDate(timezone).toISOString()}
        disableDateInput
      />
      <BackstageFormInputDateTime
        field="end"
        label="Ende einer Veranstaltung"
        defaultValue={add(newDate(timezone), { hours: 1 }).toISOString()}
        disableDateInput
      />
      <BackstageFormInputArrayInterval
        field="intervals"
        label="Intervalle"
        info="Es können beliebig viele Intervalle konfiguriert werden. Beim generieren der einzelnen Veranstltungen werden für jeden konfigurierte Intervall Veranstltungen generiert."
      />
      <BackstageFormInputImage
        field="image"
        label="Bild"
        previews={[{ name: 'Vorschau', width: 380 }]}
        info="Das Bild wird auf der Veranstaltungsseite in einer Breite von 380px angezeigt. Um Daten zu sparen, sollte das Bild nicht zu groß sein."
      />
      <BackstageFormInputText field="alt" label="Bildbeschreibung" />
      <BackstageFormInputTextLong field="description" label="Beschreibung" />
      <BackstageFormInputReferenceCategory field="category" label="Kategorie" required />
      <BackstageFormInputReferenceTopic field="topic" label="Thema" required />
      <BackstageFormInputArrayTag
        field="tags"
        label="Tags"
        info="Tags werden im Kalender als Filter angezeigt. Achte deshalb bitte darauf, dass du möglichst bestehende Tags verwendest, damit die Liste nicht zu lang und unübersichtlich wird."
      />
      <BackstageFormInputArrayUser
        field="ownedBy"
        label="Benutzer*innen"
        info="Es können beliebig viele Benutzer*innen hinzugefügt werden, die die Veranstaltung bearbeiten können."
      />
    </Fragment>
  );
};
