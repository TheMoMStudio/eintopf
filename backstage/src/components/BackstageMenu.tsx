// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import { RoleAdmin, RoleNormal } from '../api/auth';
import Link from './Link';
import './BackstageMenu.css';
import { User } from '../store/store';
import { queryNotificationList } from '../api/notification';
import { useAppData } from '../appData';

export interface BackstageMenuProps {
  user: User;
  logout: () => void;
}

export const BackstageMenu: FunctionComponent<BackstageMenuProps> = ({ user, logout }) => {
  const { apiurl } = useAppData();
  const [notifications, setNotifications] = useState<number | undefined>();

  useEffect(() => {
    (async () => {
      const resp = await queryNotificationList(apiurl, user.token, {
        filters: { viewed: false, userId: user.id },
      });
      if (typeof resp === 'string') {
        return;
      }
      setNotifications(resp.total);
    })();
  }, [apiurl, user]);

  return (
    <ul className="Backstage-Navigation">
      <li>
        <Link href="/backstage">Übersicht</Link>
        <ul>
          <li>
            <Link href="/backstage/first-steps">Erste Schritte</Link>
          </li>
          <li>
            <Link href="/backstage/usage-principles">Grundsätze zur Benutzung</Link>
          </li>
          <li>
            <Link href="/backstage/faq">Backstage FAQ</Link>
          </li>
        </ul>
      </li>
      <li>
        <Link href="/backstage/events">Veranstaltungen</Link>
        <ul>
          <li>
            <Link href="/backstage/events/new">Veranstaltung erstellen</Link>
          </li>
        </ul>
      </li>
      <li>
        <Link href="/backstage/repeatingevents">Regelmäßige Veranstaltungen</Link>
        <ul>
          <li>
            <Link href="/backstage/repeatingevents/new">Regelmäßige Veranstaltung erstellen</Link>
          </li>
        </ul>
      </li>
      <li>
        <Link href="/backstage/groups">Gruppen</Link>
        <ul>
          <li>
            <Link href="/backstage/groups/new">Gruppe erstellen</Link>
          </li>
        </ul>
      </li>
      <li>
        <Link href="/backstage/places">Orte</Link>
        <ul>
          <li>
            <Link href="/backstage/places/new">Ort erstellen</Link>
          </li>
        </ul>
      </li>
      {user.role !== RoleNormal && (
        <li>
          <Link href="/backstage/categories">Kategorien & Themen</Link>
          <ul>
            <li>
              <Link href="/backstage/categories/new">Kategorie erstellen</Link>
              <li />
              <Link href="/backstage/topics/new">Thema erstellen</Link>
            </li>
          </ul>
        </li>
      )}
      <li>
        <Link href="/backstage/notifications">
          Benachrichtigungen{notifications !== undefined ? ` (${notifications})` : ''}
        </Link>
      </li>
      {user.role !== RoleNormal ? (
        <li>
          <Link href="/backstage/users">Benutzer*innen</Link>
          <ul>
            <li>
              <Link href="/backstage/invite">Benutzer*in einladen</Link>
            </li>
          </ul>
        </li>
      ) : (
        <li>
          <Link href="/backstage/invite">Benutzer*in einladen</Link>
        </li>
      )}
      <li>
        <Link href="/backstage/account">Mein Account</Link>
      </li>
      {user.role === RoleAdmin && (
        <Fragment>
          <li>
            <Link href="/backstage/actions">Actions</Link>
          </li>
          <li>
            <Link href="/backstage/cms">CMS</Link>
          </li>
        </Fragment>
      )}
      <li>
        <Link onClick={logout}>Abmelden</Link>
      </li>
    </ul>
  );
};

export default BackstageMenu;
