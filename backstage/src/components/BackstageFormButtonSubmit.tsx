// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, ComponentChild, FunctionComponent } from 'preact';
import { FormSpy } from 'react-final-form';
import Button from './Button';
import './BackstageFormButtonSubmit.css';

export interface BackstageFormButtonSubmitProps {
  children: ComponentChild;
}

export const BackstageFormButtonSubmit: FunctionComponent<BackstageFormButtonSubmitProps> = ({
  children,
}) => {
  return (
    <div className="BackstageFormButtonSubmit">
      <FormSpy subscription={{ submitting: true }}>
        {/*@ts-ignore */}
        {(props) => (
          <Button type="submit" disabled={props.submitting}>
            {children}
          </Button>
        )}
      </FormSpy>
    </div>
  );
};

export default BackstageFormButtonSubmit;
