// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import './Checkbox.css';

export interface CheckboxProps {
  id?: string;
  label: string;
  onClick: () => void;
  checked: boolean;
}

export const Checkbox: FunctionComponent<CheckboxProps> = ({
  id,
  label: labelStr,
  onClick,
  checked,
}) => (
  <div className="Checkbox">
    <input type="checkbox" onClick={onClick} checked={checked} id={id || labelStr} />
    <label htmlFor={id || labelStr}>{labelStr}</label>
  </div>
);
export default Checkbox;
