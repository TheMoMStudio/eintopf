// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import { NeedsRole } from '../utils/role';
import Input from './Input';

export interface BackstageListFilterTextProps {
  field: string;
  needsRole?: NeedsRole;
  initialState: string;
  onChange?: (value: string) => void;
  label: string;
}

export const isBackstageListFilterText = (props: any): props is BackstageListFilterTextProps =>
  'field' in props && 'initialState' in props && 'label' in props;

export const BackstageListFilterText = (
  props: BackstageListFilterTextProps, // eslint-disable-line @typescript-eslint/no-unused-vars
) => <Fragment />;

export default BackstageListFilterText;

export interface RenderBackstageListFilterTextProps extends BackstageListFilterTextProps {
  filter: (field: string, value: string | string[] | undefined) => void;
}

export const RenderBackstageListFilterText: FunctionComponent<
  RenderBackstageListFilterTextProps
> = ({ field, initialState, onChange, label, filter }) => {
  const [value, setValue] = useState(initialState);
  useEffect(() => {
    if (onChange) {
      onChange(value);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value]);

  return (
    <Input
      onChange={(e) => {
        const v = (e.target as HTMLInputElement).value;
        setValue(v);
        if (v == '') {
          filter(field, undefined);
        } else {
          filter(field, v);
        }
      }}
      value={value}
      placeholder={label}
    />
  );
};
