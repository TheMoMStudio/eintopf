// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import { queryCategoryList, Category } from '../api/category';
import { useAppData } from '../appData';
import useUser from '../hooks/useUser';
import BackstageFormInputSelect from './BackstageFormInputSelect';
import { HideWhenCondition } from './BackstageInput';

export interface BackstageFormInputReferenceCategoryProps {
  field: string;
  label?: string;
  info?: string;
  hideWhen?: HideWhenCondition;
  required: boolean;
}

export const BackstageFormInputReferenceCategory: FunctionComponent<
  BackstageFormInputReferenceCategoryProps
> = ({ field, label, info, hideWhen, required }) => {
  const { user } = useUser();
  const { apiurl } = useAppData();
  const [categoryList, setCategoryList] = useState<Category[]>([]);
  useEffect(() => {
    (async () => {
      if (!user) return [];
      const list = await queryCategoryList(apiurl, user.token, {});
      if (typeof list === 'string') {
        return [];
      }
      return setCategoryList(list.items);
    })();
  }, [apiurl, user]);
  return (
    <BackstageFormInputSelect
      field={field}
      label={label}
      info={info}
      hideWhen={hideWhen}
      required={required}
      options={categoryList.map((c) => ({ value: c.id, label: c.name }))}
    />
  );
};

export default BackstageFormInputReferenceCategory;
