// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import CodeMirror, { EditorView } from '@uiw/react-codemirror';
import { markdown, markdownLanguage } from '@codemirror/lang-markdown';
import { javascript } from '@codemirror/lang-javascript';
import { yaml } from '@codemirror/lang-yaml';
import { less } from '@codemirror/lang-less';
import './BackstageCodeEditor.css';
import Button from '../components/Button';
import { useCallback, useEffect, useState } from 'preact/hooks';
import useUser from '../hooks/useUser';
import { useAppData } from '../appData';
import { checkError, NotificationState } from '../signals/notification';
import { queryCms } from '../api/cms';

export interface BackstageCodeEditorProps {
  fileData: string;
  apiEndpoint: string;
  language?: string;
  height?: number;
}

enum SUPPORTED_LANGUAGES {
  JS = 'js',
  CSS = 'css',
  TEMPLATE = 'template',
  MARKDOWN = 'md',
  YAML = 'yaml',
}

const getExtension = (language: string) => {
  const baseExtensions = [EditorView.lineWrapping];
  switch (language) {
    case SUPPORTED_LANGUAGES.JS:
      baseExtensions.push(javascript());
      break;
    case SUPPORTED_LANGUAGES.CSS:
      baseExtensions.push(less());
      break;
    case SUPPORTED_LANGUAGES.MARKDOWN:
      baseExtensions.push(markdown({ base: markdownLanguage }));
      break;
    case SUPPORTED_LANGUAGES.YAML:
      baseExtensions.push(yaml());
      break;
    default:
      break;
  }
  return baseExtensions;
};

export const BackstageCodeEditor: FunctionComponent<BackstageCodeEditorProps> = ({
  fileData,
  apiEndpoint,
  language = '',
  height = 400,
}) => {
  const { apiurl, notifications } = useAppData();
  const { user } = useUser();
  const [loading, setLoading] = useState<boolean>(false);
  const [editorData, setEditorData] = useState<string>(fileData);
  const onChange = useCallback((val: string) => {
    setEditorData(val);
  }, []);

  const saveData = async (endpoint: string) => {
    setLoading(true);
    if (!user) return;
    const response = await queryCms(apiurl, user.token, 'PUT', endpoint, { content: editorData });
    setLoading(false);
    if (checkError(response, notifications, false, 'Die Datei konnte nicht gespeichert werden!'))
      return;
    notifications.push(NotificationState.SUCCESS, 'Die Datei wurde erfolgreich geupdated.', true);
  };

  useEffect(() => {
    setEditorData(fileData);
  }, [language, fileData]);

  return (
    <div className="Code-Editor">
      <div className="Code">
        <CodeMirror
          value={editorData}
          height={`${height}px`}
          extensions={getExtension(language)}
          onChange={onChange}
        />
      </div>
      {fileData !== editorData && (
        <Button className="Code-Save" loading={loading} onClick={() => saveData(apiEndpoint)}>
          Speichern
        </Button>
      )}
    </div>
  );
};

export default BackstageCodeEditor;
