// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { FunctionComponent, h } from 'preact';
import { useState } from 'preact/hooks';
import { route } from 'preact-router';
import { queryNotificationCrud, Notification } from '../api/notification';
import { useAppData } from '../appData';
import useUser from '../hooks/useUser';
import { format, newDate } from '../utils/date';
import BackstageList from './BackstageList';
import BackstageListField from './BackstageListField';
import BackstageListFilterToggle from './BackstageListFilterToggle';
import './BackstageNotificationList.css';

export interface BackstageNotificationListProps {
  pageSize: number;
  baseFilters?: Record<string, any>;
  onPageSizeChange?: (pageSize: number) => void;
}

export const BackstageNotificationList: FunctionComponent<BackstageNotificationListProps> = ({
  pageSize,
  baseFilters,
  onPageSizeChange,
}) => {
  const { timezone } = useAppData();
  const { user } = useUser();

  const [viewed, setViewed] = useState<boolean>(false);

  return (
    <BackstageList
      title="Benachrichtigungen"
      noItemsInfo="Keine Benachrichtigungen vorhanden"
      queryCrud={queryNotificationCrud}
      bulkDeleteConfirmMessage=""
      config={{
        pageSize,
        initialSorting: 'created_at',
        initialOrder: 'DESC',
        baseFilters: {
          ...baseFilters,
          userId: user!.id,
          viewed,
        },
      }}
      filters={[
        <BackstageListFilterToggle
          key="viewedFilter"
          field="viewed"
          initialState={false}
          onChange={(viewed) => {
            setViewed(viewed);
          }}
          value={(on) => (on ? undefined : false)}
          label="Gelesene Anzeigen"
        />,
      ]}
      onPageSizeChange={onPageSizeChange}
      onClickRow={({ id }) => route(`/backstage/notifications/${id}`)}
    >
      <BackstageListField<Notification>
        field="createdAt"
        name="Datum"
        display="custom"
        render={renderField((createdAt) =>
          format(timezone, newDate(timezone, createdAt), 'dd.MM.yyyy HH:mm'),
        )}
      />
      <BackstageListField<Notification>
        field="subject"
        name="Betreff"
        display="custom"
        render={renderField((v) => v)}
      />
    </BackstageList>
  );
};
export default BackstageNotificationList;

const renderField =
  <T,>(format: (v: T) => string) =>
  (message: T, notification: Notification) => (
    <span className={`notification ${!notification.viewed ? ' unread' : ''}`}>
      {format(message)}
    </span>
  );
