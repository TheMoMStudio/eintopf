// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, ComponentChildren, FunctionComponent, Fragment } from 'preact';
import useUser from '../hooks/useUser';
import Container from './Container';
import Link from './Link';
import Grid from './Grid';
import './PageBackstage.css';
import ContainerLogin from './ContainerLogin';
import BackstageMenu from './BackstageMenu';

export interface PageBackstageProps {
  children: ComponentChildren;
  onSearch?: (query: string) => void;
  mobileTitle?: string;
  showMenu?: boolean;
}

export const PageBackstage: FunctionComponent<PageBackstageProps> = ({ children, showMenu }) => {
  const { user, logout } = useUser();

  return (
    <Fragment>
      {user ? (
        <Grid>
          <Grid cols={2} className="Backstage-Left">
            {children}
          </Grid>
          {showMenu !== false && (
            <Grid cols={1} fixed>
              <Container titleCenter="Backstage">
                <BackstageMenu user={user} logout={logout} />
              </Container>
            </Grid>
          )}
        </Grid>
      ) : (
        <Grid>
          <Grid cols={2}>
            <Container title="Backstage">
              <p>
                Du musst angemeldet sein, um Veranstaltungen erstellen zu können. Auf der{' '}
                <Link highlight href="/about#register">
                  ÜBER EINTOPF
                </Link>{' '}
                Seite erfährst du, wie du an einen Account kommst.
              </p>
              <br />
            </Container>
          </Grid>
          <Grid cols={1}>
            <ContainerLogin />
          </Grid>
        </Grid>
      )}
    </Fragment>
  );
};

export default PageBackstage;
