// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import './Input.css';

export interface InputProps {
  id?: string;
  onChange: (e: any) => void;
  value: string;
  placeholder?: string;
}

export const Input: FunctionComponent<InputProps> = ({ id, onChange, value, placeholder }) => (
  <input
    type="text"
    className="Input"
    onChange={onChange}
    id={id}
    value={value}
    placeholder={placeholder}
  />
);
export default Input;
