// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useField } from 'react-final-form';
import { BackstageFormInputProps } from './BackstageFormInput';
import validateRequired from '../utils/validateRequired';
import BackstageInput, { HideField } from './BackstageInput';

export interface BackstageFormInputTextProps extends BackstageFormInputProps<string> {
  type?: 'email' | 'password' | 'text';
  placeholder?: string;
}

export const BackstageFormInputText: FunctionComponent<BackstageFormInputTextProps> = ({
  field,
  label,
  required,
  info,
  type = 'text',
  defaultValue,
  hideWhen,
  ...rest
}) => {
  const { input, meta } = useField(field, {
    validate: required ? validateRequired : undefined,
    defaultValue,
  });

  const component = (
    <BackstageInput
      className="InputText"
      label={label}
      error={meta.error}
      required={required}
      touched={meta.touched}
      info={info}
      inputId={field}
    >
      <input
        type={type}
        id={field}
        {...rest}
        {...(input as Record<string, unknown>)}
        className={meta.error && meta.touched ? 'error' : ''}
      />
    </BackstageInput>
  );

  if (hideWhen !== undefined) {
    return (
      <HideField field={hideWhen.field} when={hideWhen.is}>
        {component}
      </HideField>
    );
  }

  return component;
};

export default BackstageFormInputText;
