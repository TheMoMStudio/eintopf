// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useState } from 'preact/hooks';
import { useField } from 'react-final-form';
import BackstageError from './BackstageError';
import Button from './Button';
import './BackstageFormButtonLatLng.css';
import { HideField, HideWhenCondition } from './BackstageInput';

export interface BackstageFormButtonLatLngProps {
  addressField: string;
  latField: string;
  lngField: string;
  info?: string;
  hideWhen?: HideWhenCondition;
}

export const BackstageFormButtonLatLng: FunctionComponent<BackstageFormButtonLatLngProps> = ({
  addressField,
  latField,
  lngField,
  info,
  hideWhen,
}) => {
  const [querying, setQuerying] = useState(false);
  const [error, setError] = useState('');
  const { input: addressInput } = useField(addressField);
  const { input: latInput } = useField(latField);
  const { input: lngInput } = useField(lngField);

  const generate = async (): Promise<void> => {
    const address = addressInput.value;
    if (address === '') {
      setError('Die Adresse muss gesetzt sein, um die Koordinaten zu generieren');
      return;
    }

    setQuerying(true);
    try {
      const response = await fetch(
        `https://nominatim.openstreetmap.org/search?q=${address}&format=json`,
      );
      console.log('RESPONSE');
      console.log(response);
      setQuerying(false);
      if (response.status !== 200) {
        setError('Fehler bei der openstreetmaps api Anfrage');
        return;
      }

      const results = await response.json();
      if (results.length === 0) {
        setError('Keine Ergebnisse für die angegebene Adresse');
        return;
      }

      latInput.onChange(
        typeof results[0].lat === 'string' ? parseFloat(results[0].lat) : results[0].lat,
      );
      lngInput.onChange(
        typeof results[0].lon === 'string' ? parseFloat(results[0].lon) : results[0].lon,
      );

      setError('');
    } catch (e) {
      console.log('Fehler bei der openstreetmaps api Anfrage');
      if (e instanceof Error) {
        setError(e.message);
      }
      return;
    }
  };

  const component = (
    <div className="FormButtonLatLng">
      {info && <div className="BackstageInput-Info">{info}</div>}
      <Button
        type="button"
        disabled={querying}
        onClick={generate}
        variant="secondary"
        id="coordinates-button"
      >
        Koordinaten über openstreetmaps.org generieren
      </Button>
      {error && <BackstageError err={error} />}
    </div>
  );

  if (hideWhen !== undefined) {
    return (
      <HideField field={hideWhen.field} when={hideWhen.is}>
        {component}
      </HideField>
    );
  }

  return component;
};

export default BackstageFormButtonLatLng;
