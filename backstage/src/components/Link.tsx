// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, ComponentChildren, FunctionComponent } from 'preact';
import { Link as RouterLink } from 'preact-router/match';
import './Link.css';

export interface LinkProps {
  children: ComponentChildren;
  href?: string;
  target?: string;
  onClick?: () => void;
  highlight?: boolean;
  className?: string;
  variant?: 'Button' | 'Button secondary' | 'Button icon';
}

export const Link: FunctionComponent<LinkProps> = ({
  children,
  href,
  onClick,
  target = '',
  highlight = false,
  className = '',
  variant = '',
}) =>
  href ? (
    <RouterLink
      class={`Link ${highlight ? 'Link-Highlight' : ''} ${className} ${variant}`}
      activeClassName="active"
      // @ts-ignore: This has worked and should absolutely work
      href={href}
      target={target}
    >
      {children}
    </RouterLink>
  ) : (
    <span className={`Link ${highlight ? 'Link-Highlight' : ''} ${variant}`} onClick={onClick}>
      {children}
    </span>
  );

export default Link;
