// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, ComponentChildren } from 'preact';
import './Button.css';

export type ButtonVariant = 'primary' | 'secondary' | 'danger' | 'icon';

export interface ButtonProps {
  children: ComponentChildren;
  className?: string;
  disabled?: boolean;
  loading?: boolean;
  type?: 'button' | 'reset' | 'submit';
  onClick?: () => void;
  variant?: ButtonVariant;
  id?: string;
}

const LoadingSpinner: FunctionComponent = () => (
  <div class="loading-box">
    <div class="lds-ring">
      <div />
      <div />
      <div />
      <div />
    </div>
  </div>
);

export const Button: FunctionComponent<ButtonProps> = ({
  children,
  className = '',
  disabled,
  loading,
  type = 'button',
  onClick,
  variant = 'primary',
  id,
}) => (
  <button
    className={`Button ${className} ${variant} ${loading ? 'loading' : ''}`}
    type={type}
    onClick={onClick}
    disabled={disabled || loading}
    id={id}
  >
    {loading ? <LoadingSpinner /> : children}
  </button>
);

export default Button;
