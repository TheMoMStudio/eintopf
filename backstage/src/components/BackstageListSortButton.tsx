// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useState } from 'preact/hooks';
import './BackstageListSortButton.css';

export interface SortState {
  sorting: string;
  order: 'ASC' | 'DESC';
}

export const useSorting = (
  initialSortState: SortState,
): {
  sorting: string;
  order: 'ASC' | 'DESC';
  sort: (field: string) => void;
} => {
  const [sortState, setSortState] = useState(initialSortState);

  const sort = (field: string): void => {
    if (field === sortState.sorting) {
      setSortState({
        sorting: sortState.sorting,
        order: sortState.order === 'ASC' ? 'DESC' : 'ASC',
      });
    } else {
      setSortState({ sorting: field, order: 'ASC' });
    }
  };

  return { sorting: sortState.sorting, order: sortState.order, sort };
};

export interface SortButtonProps extends SortState {
  label: string;
  field: string;
  sort: (field: string) => void;
}

export const SortButton: FunctionComponent<SortButtonProps> = ({
  label,
  field,
  sort,
  sorting,
  order,
}) => (
  <span onClick={() => sort(field)}>
    {label}
    {sorting === field && <span className={order === 'ASC' ? 'arrow-down' : 'arrow-up'} />}
  </span>
);

export default SortButton;
