// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import { queryTopicList, Topic } from '../api/topic';
import { useAppData } from '../appData';
import useUser from '../hooks/useUser';
import BackstageFormInputSelect from './BackstageFormInputSelect';
import { HideWhenCondition } from './BackstageInput';

export interface BackstageFormInputReferenceTopicProps {
  field: string;
  label?: string;
  info?: string;
  hideWhen?: HideWhenCondition;
  required: boolean;
}

export const BackstageFormInputReferenceTopic: FunctionComponent<
  BackstageFormInputReferenceTopicProps
> = ({ field, label, info, hideWhen, required }) => {
  const { user } = useUser();
  const { apiurl } = useAppData();
  const [topicList, setTopicList] = useState<Topic[]>([]);
  useEffect(() => {
    (async () => {
      if (!user) return [];
      const list = await queryTopicList(apiurl, user.token, {});
      if (typeof list === 'string') {
        return [];
      }
      return setTopicList(list.items);
    })();
  }, [apiurl, user]);
  return (
    <BackstageFormInputSelect
      field={field}
      label={label}
      info={info}
      hideWhen={hideWhen}
      required={required}
      options={topicList.map((c) => ({ value: c.id, label: c.name }))}
    />
  );
};

export default BackstageFormInputReferenceTopic;
