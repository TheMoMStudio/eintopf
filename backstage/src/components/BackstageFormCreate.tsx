// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, toChildArray, ComponentChildren, VNode } from 'preact';
import { route } from 'preact-router';
import { CreateError } from '../api/crud';
import { useAppData } from '../appData';
import useUser from '../hooks/useUser';
import BackstageForm from './BackstageForm';
import BackstageFormButtonSubmit from './BackstageFormButtonSubmit';

export interface BackstageFormCreateProps<
  Model extends object,
  NewModel extends object = Omit<Model, 'id'>,
> {
  children: ComponentChildren;
  queryCreate: (apiurl: string, token: string, model: NewModel) => Promise<Model | CreateError>;
  model: string;
  onCreate?: (values: Model) => void;
  validate?: (values: NewModel) => string | undefined;
}

export const BackstageFormCreate = <
  Model extends object,
  NewModel extends object = Omit<Model, 'id'>,
>({
  children,
  queryCreate,
  model,
  onCreate,
  validate,
}: BackstageFormCreateProps<Model, NewModel>): VNode => {
  const { user } = useUser();
  const { apiurl } = useAppData();

  const onSubmit = async (values: NewModel): Promise<string | undefined> => {
    if (!user) return undefined;

    if (validate) {
      const err = validate(values);
      if (err) {
        return err;
      }
    }

    const response = await queryCreate(apiurl, user.token, values);
    if (typeof response === 'string') {
      return response;
    }

    if (onCreate) {
      onCreate(response);
    } else {
      route(`/backstage/${model}`);
    }
    return undefined;
  };
  return (
    <BackstageForm onSubmit={onSubmit} requiredNotice>
      {toChildArray(children).map((child) => child)}
      <BackstageFormButtonSubmit>Speichern</BackstageFormButtonSubmit>
    </BackstageForm>
  );
};

export default BackstageFormCreate;
