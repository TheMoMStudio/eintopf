// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import { NeedsRole } from '../utils/role';
import Select from './Select';

export interface BackstageListFilterSelectProps {
  field: string;
  needsRole?: NeedsRole;
  initialState: string | undefined;
  options: { value: string; name: string }[];
  onChange?: (value: string | undefined) => void;
  label: string;
}

export const isBackstageListFilterSelect = (props: any): props is BackstageListFilterSelectProps =>
  'field' in props && 'initialState' in props && 'label' in props && 'options' in props;

export const BackstageListFilterSelect = (
  props: BackstageListFilterSelectProps, // eslint-disable-line @typescript-eslint/no-unused-vars
) => <Fragment />;

export default BackstageListFilterSelect;

export interface RenderBackstageListFilterSelectProps extends BackstageListFilterSelectProps {
  filter: (field: string, value: string | string[] | undefined | null) => void;
}

export const RenderBackstageListFilterSelect: FunctionComponent<
  RenderBackstageListFilterSelectProps
> = ({ field, initialState, options, onChange, label, filter }) => {
  const [value, setValue] = useState(initialState);
  useEffect(() => {
    if (onChange) {
      onChange(value || undefined);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value]);

  return (
    <Select
      onChange={(e) => {
        const v = internalToExternal((e.target as HTMLSelectElement).value);
        setValue(v);
        filter(field, v);
      }}
      value={externalToInternal(value)}
      options={[
        { value: 'not set', name: `${label} (alle)` },
        { value: 'empty', name: `ohne ${label}` },
        ...options,
      ]}
    />
  );
};

const externalToInternal = (v: string | undefined): string => {
  if (v === undefined) {
    return 'not set';
  } else if (v === '') {
    return 'empty';
  }
  return v;
};

const internalToExternal = (v: string): string | undefined => {
  if (v === 'not set') {
    return undefined;
  } else if (v === 'empty') {
    return '';
  }
  return v;
};
