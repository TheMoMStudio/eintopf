// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useField } from 'react-final-form';
import { BackstageFormInputProps } from './BackstageFormInput';
import BackstageInput from './BackstageInput';
import './BackstageFormInputCheckbox.css';

export interface BackstageFormInputCheckboxProps extends BackstageFormInputProps<boolean> {
  placeholder?: string;
}

export const BackstageFormInputCheckbox: FunctionComponent<BackstageFormInputCheckboxProps> = ({
  field,
  label,
  info,
  needsRole,
  hideWhen,
  defaultValue,
  ...rest
}) => {
  const { input, meta } = useField(field, { type: 'checkbox', defaultValue });

  return (
    <BackstageInput
      className="InputCheckbox"
      label={label}
      needsRole={needsRole}
      info={info}
      hideWhen={hideWhen}
      inputId={field}
    >
      {meta.error && meta.touched && <span className="error">{meta.error}</span>}
      <input
        type="checkbox"
        id={field}
        {...rest}
        {...(input as Record<string, unknown>)}
        className={meta.error && meta.touched ? 'error' : ''}
      />{' '}
      {label}
    </BackstageInput>
  );
};

export default BackstageFormInputCheckbox;
