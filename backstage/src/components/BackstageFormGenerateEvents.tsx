// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import { add } from 'date-fns/fp/add';
import { differenceInMonths } from 'date-fns/differenceInMonths';
import { queryGenerateEvents } from '../api/revent';
import useUser from '../hooks/useUser';
import BackstageForm from './BackstageForm';
import BackstageFormButtonSubmit from './BackstageFormButtonSubmit';
import BackstageFormInputDateTime from './BackstageFormInputDateTime';
import { newDate } from '../utils/date';
import { useAppData } from '../appData';

export interface BackstageFormGenerateEventsProps {
  repeatingEventID: string;
  onGenerate?: () => void;
}

export const BackstageFormGenerateEvents: FunctionComponent<BackstageFormGenerateEventsProps> = ({
  repeatingEventID,
  onGenerate,
}) => {
  const { user } = useUser();
  const { apiurl, timezone } = useAppData();
  if (!user) {
    return <Fragment />;
  }
  return (
    <BackstageForm<{ start: string; end: string }>
      onSubmit={async ({ start, end }) => {
        if (differenceInMonths(newDate(timezone, end), newDate(timezone, start)) > 6) {
          return Promise.resolve(
            'Es können immer nur Veranstaltungen für die nächsten 6 Monate generiert werden',
          );
        }
        const response = await queryGenerateEvents(
          timezone,
          apiurl,
          user.token,
          repeatingEventID,
          start,
          end,
        );
        if (typeof response === 'string') {
          return response;
        }

        if (onGenerate) {
          onGenerate();
        }
        return undefined;
      }}
    >
      <p>
        Generiere Veranstaltungen für maximal die nächsten 6 Monate. Es werden dabei ganz normale
        Veranstaltungen erstellt, die du bearbeiten und löschen kannst. Die Veranstaltungen werden
        sofort veröffentlicht.
      </p>
      <br />
      <p>
        Es werden nur Veranstaltungen generiert, falls diese für das jeweilige Datum noch nicht
        generiert wurden. Wenn du Veranstaltungen neu generieren willst musst vorher die alten
        Veranstaltungen löschen.
      </p>
      <br />
      <p>
        Bitte achte darauf, dass die generierten Veranstaltungen immer aktuell sind, wenn sich etwas
        an der regelmäßigen Veranstaltung ändert.
      </p>
      <br />
      <p>Gebe den Zeitraum an, für den Veranstaltungen generiert werden sollen.</p>
      <br />
      <BackstageFormInputDateTime
        field="start"
        label="Start des Zeitraums"
        defaultValue={newDate(timezone).toISOString()}
        disableTimeInput
      />
      <BackstageFormInputDateTime
        field="end"
        label="Ende des Zeitraums"
        defaultValue={add({ days: 1 }, newDate(timezone)).toISOString()}
        disableTimeInput
      />
      <BackstageFormButtonSubmit>Veranstaltungen Generieren</BackstageFormButtonSubmit>
    </BackstageForm>
  );
};

export default BackstageFormGenerateEvents;
