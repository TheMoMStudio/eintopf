// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, VNode } from 'preact';
import { useCallback, useEffect, useState } from 'preact/hooks';
import { useField } from 'react-final-form';
import { QueryList } from '../api/crud';
import { useAppData } from '../appData';
import useUser from '../hooks/useUser';
import BackstageInput, { HideWhenCondition } from './BackstageInput';
import BackstageInputAutocomplete from './BackstageInputAutocomplete';

export interface BackstageFormInputReferenceProps<T extends { id: string }> {
  field: string;
  label?: string;
  queryList: QueryList<T>;
  displayField: keyof T;
  mustOwn?: boolean;
  info?: string;
  hideWhen?: HideWhenCondition;
  required?: boolean;
}

export const BackstageFormInputReference = <T extends { id: string }>({
  field,
  label,
  queryList,
  displayField,
  mustOwn = false,
  info,
  hideWhen,
  required,
}: BackstageFormInputReferenceProps<T>): VNode => {
  const { user } = useUser();
  const { apiurl } = useAppData();

  const { input, meta } = useField<string>(field);

  const [models, setModels] = useState<T[]>([]);
  useEffect(() => {
    if (!user) return;

    queryList(apiurl, user.token, {
      offset: 0,
      limit: 10000,
      sorting: displayField as string,
      order: 'ASC',
      filters: {
        deactivated: false,
        ...(mustOwn ? { ownedBy: [user.id] } : {}),
      },
    })
      .then((result) => {
        if (typeof result === 'string') {
          return;
        }
        setModels(result.items);
      })
      .catch(() => {});
  }, [displayField, queryList, user, mustOwn, apiurl]);

  const idFromDisplayField = useCallback(
    (name: string): string => {
      let id = '';
      models.forEach((model) => {
        if ((model[displayField] as unknown as string) === name) {
          id = model.id;
        }
      });
      return id;
    },
    [displayField, models],
  );
  const displayFieldFromID = useCallback(
    (id: string): string => {
      let display = '';
      models.forEach((model) => {
        if (model.id === id) {
          display = model[displayField] as unknown as string;
        }
      });
      return display;
    },
    [displayField, models],
  );

  const modelCompletionProvider = useCallback(
    (text: string): Promise<string[]> => {
      if (!models) {
        return Promise.resolve([]);
      }
      const completions = models
        .map((model) => model[displayField] as unknown as string)
        .filter((display) => display.toLowerCase().includes(text.toLowerCase())) // remove all models, which don't start with the current text
        .filter((display) => display !== '') // remove empty models
        .filter((display) => text.indexOf(idFromDisplayField(display)) === -1); // remove already selected model
      return Promise.resolve(completions);
    },
    [displayField, idFromDisplayField, models],
  );

  const displayValue = (v: string): string => {
    if (!v.startsWith('id:')) {
      return v;
    }
    if (!models) {
      return '';
    }
    return displayFieldFromID(v.replace('id:', ''));
  };

  const onChange = (v: string): void => {
    let newValue = v;
    if (models) {
      const id = idFromDisplayField(v);
      if (id) {
        newValue = `id:${id}`;
      }
    }
    input.onChange(newValue);
  };

  return (
    <BackstageInput
      label={label}
      className="InputReference"
      info={info}
      hideWhen={hideWhen}
      required={required}
      inputId={field}
      error={meta.error}
    >
      {meta.error && meta.touched && <span className="BackstageError">{meta.error}</span>}
      <BackstageInputAutocomplete
        className={meta.error && meta.touched && 'error'}
        value={input.value}
        onChange={onChange}
        completionProvider={modelCompletionProvider}
        displayValue={displayValue}
        inputId={field}
      />
    </BackstageInput>
  );
};

export default BackstageFormInputReference;
