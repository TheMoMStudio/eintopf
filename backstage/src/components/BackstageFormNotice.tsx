// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, Fragment, VNode } from 'preact';
import { useField } from 'react-final-form';
import './BackstageFormNotice.css';
import { NeedsRole, checkNeedsRole } from '../utils/role';
import useUser from '../hooks/useUser';

export interface BackstageFormNoticeProps<T> {
  field: string;
  message: (value: T) => string | false;
  needsRole?: NeedsRole;
}

export const BackstageFormNotice = <T,>({
  field,
  message,
  needsRole,
}: BackstageFormNoticeProps<T>): VNode => {
  const { input } = useField(field);

  const { user } = useUser();
  if (needsRole && user !== undefined && checkNeedsRole(needsRole, user.role)) {
    return <Fragment />;
  }

  const msg = message(input.value);
  if (!msg) {
    return <Fragment />;
  }
  return <div className="Form-Notice">{msg}</div>;
};

export default BackstageFormNotice;
