// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, Fragment, VNode } from 'preact';
import { useContext } from 'preact/hooks';
import { ListContext, ListContextType } from './BackstageList';
import BackstageListItem, { BackstageListItemProps } from './BackstageListItem';
import ButtonConfirm from './ButtonConfirm';
import Button, { ButtonVariant } from './Button';
import './BackstageListAction.css';

export interface BackstageListActionProps<T extends object> extends BackstageListItemProps {
  render: (item: T) => string | JSX.Element;
  action?: (ctx: ListContextType) => void;
  needsConfirm?: string;
  variant?: ButtonVariant;
}

export const BackstageListAction = <T extends object>({
  render,
  action,
  needsConfirm,
  variant,
  ...rest
}: BackstageListActionProps<T>): VNode => {
  const listContext = useContext<ListContextType | undefined>(ListContext);
  if (listContext === undefined) {
    console.error('useContext(ListContext) used outside a ListContext.Provider');
    return <Fragment />;
  }

  const { item } = listContext;

  if (!action) {
    return (
      <BackstageListItem {...rest} className="action">
        {render(item as T)}
      </BackstageListItem>
    );
  }

  return (
    <BackstageListItem {...rest} className="action">
      {needsConfirm && needsConfirm !== '' ? (
        <ButtonConfirm
          onConfirm={() => {
            action(listContext);
          }}
          confirmMessage={needsConfirm}
          variant={variant}
        >
          {render(item as T)}
        </ButtonConfirm>
      ) : (
        <Button
          onClick={() => {
            action(listContext);
          }}
        >
          {render(item as T)}
        </Button>
      )}
    </BackstageListItem>
  );
};

export default BackstageListAction;
