// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useAppData } from '../appData';
import { useEffect, useState } from 'preact/hooks';
import './BackstageThemeFileOption.css';
import { checkError } from '../signals/notification';
import { User } from '../store/store';
import './BackstageThemeConfigOption.css';
import BackstageCodeEditor from './BackstageCodeEditor';
import { queryCms, ThemeFiletype } from '../api/cms';

export interface BackstageThemeConfigOptionProps {
  user: User | undefined;
}

export const BackstageThemeMetaOption: FunctionComponent<BackstageThemeConfigOptionProps> = ({
  user,
}) => {
  const { apiurl, notifications } = useAppData();
  const [fileData, setFileData] = useState<any>();

  useEffect(() => {
    const query = async () => {
      if (!user) return;
      const response = await queryCms(
        apiurl,
        user.token,
        'GET',
        `/cms/files/${ThemeFiletype.CONFIG}/${btoa('theme.yaml')}`,
      );
      if (checkError(response, notifications, false, 'Die Config konnte nicht geöffnet werden!'))
        return;
      setFileData(response.content);
    };
    query();
  }, [user, apiurl, notifications]);

  return (
    <div class="Theme-Config-Option">
      <p className="Description">
        In diesem Teil der Seite kann die Konfiguration des Themes angepasst werden.
        <br />
        Die Optionen die aktuell verfügbar sind lauten:
      </p>
      <div className="Option-Headline">
        <h4>parent</h4>
        <p>
          Mit dieser Option kann eine Vererbung bestimmt werden. Der Wert muss dem Namen des
          Ursprungthemes entsprechen. Dieses Feature sollte genutzt werden, wenn einzelne Dateien
          des Themes überschrieben wurden und nicht das ganze Theme kopiert.
          <br />
          <i>Beispiel: eintopf</i>
        </p>
      </div>
      <div className="Option-Headline">
        <h4>Hinweis</h4>
        <p>Die Konfiguration wird im YAML-Format definiert!</p>
      </div>
      {fileData && (
        <BackstageCodeEditor
          fileData={fileData}
          height={300}
          language="yaml"
          apiEndpoint={`/cms/files/config/${btoa('theme.yaml')}`}
        />
      )}
    </div>
  );
};

export default BackstageThemeMetaOption;
