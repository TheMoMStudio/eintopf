// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, RefObject, Fragment } from 'preact';
import { useAppData } from '../appData';
import { useEffect, useRef, useState } from 'preact/hooks';
import { User } from '../store/store';
import './BackstageFileUpload.css';
import Button from './Button';
import Link from './Link';
import { checkError, NotificationState } from '../signals/notification';
import { fileBaseUrl, queryCms } from '../api/cms';

export interface BackstageFileUploadProps {
  user: User | undefined;
  src: string;
  name: string;
  isTextFile: boolean;
  hideName?: boolean;
  fileType?: string;
  placeholderSrc?: string;
  accept?: string[];
}

export const BackstageFileUpload: FunctionComponent<BackstageFileUploadProps> = ({
  isTextFile,
  user,
  src,
  name,
  hideName = false,
  fileType = 'image',
  placeholderSrc,
  accept,
}) => {
  const formRef = useRef<HTMLFormElement>();
  const inputRef = useRef<HTMLInputElement>();
  const [imageSrc, setImageSrc] = useState<string>(src);
  const [isUploading, setIsUploading] = useState<boolean>(false);

  const { apiurl, notifications } = useAppData();

  useEffect(() => {
    setImageSrc(src);
  }, [src]);

  const swapImage = async (evt: any) => {
    const tgt = evt.target,
      files = tgt.files;
    // FileReader support
    if (FileReader && files && files.length) {
      const fr = new FileReader();

      fr.addEventListener(
        'load',
        () => {
          if (!fr.result) {
            return;
          }
          setImageSrc(fr.result as string);
        },
        false,
      );

      fr.readAsDataURL(files[0]);
    } else {
      notifications.push(
        NotificationState.ERROR,
        'Die Datei konnte nicht eingelesen werden!',
        true,
      );
    }
  };

  const saveData = async (event: Event, fileName: string, typeOfFile: string) => {
    event.preventDefault();
    if (!user) return;
    if (!inputRef.current) return;
    if (!inputRef.current.files) return;
    if (!inputRef.current.files.length) return;
    if (!formRef.current) return;
    setIsUploading(true);
    const data = new FormData(formRef.current);
    data.append('content', '');
    data.append('isBinary', 'true');
    const response = await queryCms(
      apiurl,
      user.token,
      'PUT',
      `/cms/files/${typeOfFile}/${btoa(fileName)}`,
      data,
      true,
    );
    setIsUploading(false);
    if (checkError(response, notifications, false)) return;
    notifications.push(
      NotificationState.SUCCESS,
      `Die Datei ${fileName} wurde erfolgreich hochgeladen!`,
      true,
    );
    inputRef.current.value = '';
  };

  return (
    <div className="Backstage-File-Upload">
      {!isTextFile ? (
        <>
          <label for={`file-upload-${btoa(name)}`} class="File-Upload-Button">
            <img className="File-Upload-Current" src={placeholderSrc ?? imageSrc} />
            <img className="File-Upload-Edit" src="/assets/images/icon_edit.svg" alt="Bearbeiten" />
          </label>
          <form
            method="PUT"
            ref={formRef as RefObject<HTMLFormElement>}
            onSubmit={(event) => saveData(event, name, fileType)}
          >
            <input
              id={`file-upload-${btoa(name)}`}
              ref={inputRef as RefObject<HTMLInputElement>}
              onChange={swapImage}
              type="file"
              name="binary"
              accept={accept
                ?.map((el) => {
                  return `.${el}`;
                })
                .join(',')}
              hidden
            />
            {!hideName && <h3>{name}</h3>}
            {inputRef.current?.value && (
              <Button type="submit" className="Button Save-Upload" loading={isUploading}>
                Speichern
              </Button>
            )}
          </form>
        </>
      ) : (
        <>
          <Link className="File-Upload-Text" href={`${fileBaseUrl}${fileType}/${btoa(name)}`}>
            <div className="File-Upload-Button">
              <img className="File-Upload-Current" src={imageSrc} />
              <img
                className="File-Upload-Edit"
                src="/assets/images/icon_edit.svg"
                alt="Bearbeiten"
              />
            </div>
            <h3>{name}</h3>
          </Link>
        </>
      )}
    </div>
  );
};

export default BackstageFileUpload;
