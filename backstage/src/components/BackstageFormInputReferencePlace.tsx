// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { queryPlaceList, Place } from '../api/place';
import BackstageFormInputReference from './BackstageFormInputReference';

export interface BackstageFormInputReferencePlaceProps {
  field: string;
  label?: string;
  info?: string;
  required?: boolean;
  testid?: string;
}

export const BackstageFormInputReferencePlace: FunctionComponent<
  BackstageFormInputReferencePlaceProps
> = ({ field, label, info, required }) => (
  <BackstageFormInputReference<Place>
    field={field}
    label={label}
    queryList={queryPlaceList}
    displayField="name"
    info={info}
    required={required}
  />
);

export default BackstageFormInputReferencePlace;
