// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, VNode } from 'preact';
import { useAppData } from '../appData';
import BackstageListAction from './BackstageListAction';
import Link from './Link';
import './BackstageActionButtons.css';
import { ListContext, ListContextType } from './BackstageList';
import { useContext } from 'preact/hooks';

export interface BackstageActionButtonsProps {
  context: string;
}

export const BackstageActionButtonEdit = <T extends { id: string }>({
  context,
}: BackstageActionButtonsProps): VNode => {
  return (
    <BackstageListAction<T>
      render={({ id: modelID }) => (
        <Link href={`/backstage/${context}/${modelID}`} variant="Button icon">
          <img src="/assets/images/icon_edit.svg" class="BackstageButton" alt="Bearbeiten" />
        </Link>
      )}
    />
  );
};

export const BackstageActionButtonView = <T extends { id: string }>({
  context,
}: BackstageActionButtonsProps): VNode => {
  return (
    <BackstageListAction<T>
      render={({ id: modelID }) => (
        <Link href={`/${context}/${modelID}`} variant="Button icon">
          <img src="/assets/images/icon_view.svg" class="BackstageButton" alt="Anzeigen" />
        </Link>
      )}
    />
  );
};

export const BackstageActionButtonDelete = <T extends { id: string }>(): VNode => {
  const { apiurl } = useAppData();
  const listContext = useContext(ListContext);
  if (listContext === undefined) {
    console.error('useContext(ListContext) used outside a ListContext.Provider');
  }
  const { queryCrud, token, refresh, item } = listContext as ListContextType;

  return (
    <BackstageListAction<T>
      variant="icon"
      action={() => {
        queryCrud.delete(apiurl, token, item.id as string);
        refresh();
      }}
      render={() => (
        <img src="/assets/images/icon_delete.svg" class="BackstageButton" alt="Löschen" />
      )}
      needsConfirm="Willst du diesen Eintrag wirklich löschen?"
    />
  );
};
