// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, ComponentChildren, FunctionComponent } from 'preact';
import './Grid.css';

export interface GridProps {
  children: ComponentChildren;
  cols?: 1 | 2 | 3;
  fixed?: boolean;
  className?: string;
}

export const Grid: FunctionComponent<GridProps> = ({
  children,
  cols,
  fixed = false,
  className = '',
}) => (
  <div className={`${cols ? `Grid-Cols-${cols}` : 'Grid'} ${className} ${fixed && 'Grid-Fixed'}`}>
    {children}
  </div>
);

export default Grid;
