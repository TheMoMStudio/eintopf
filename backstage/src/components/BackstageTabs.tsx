// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, ComponentChildren, FunctionComponent } from 'preact';
import './BackstageTabs.css';

export interface BackstageTabsProps {
  children: ComponentChildren;
  className?: string;
}

export const BackstageTabs: FunctionComponent<BackstageTabsProps> = ({
  children,
  className = '',
}) => {
  return <nav className={`Backstage-Tabs ${className}`}>{children}</nav>;
};

export default BackstageTabs;
