// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useField } from 'react-final-form';
import { BackstageFormInputProps } from './BackstageFormInput';
import composeValidators from '../utils/composeValidators';
import validateEmail from '../utils/validateEmail';
import validateRequired from '../utils/validateRequired';
import BackstageInput from './BackstageInput';

export interface BackstageFormInputTextProps extends BackstageFormInputProps<string> {
  placeholder?: string;
}

export const BackstageFormInputText: FunctionComponent<BackstageFormInputTextProps> = ({
  field,
  label,
  required,
  ...rest
}) => {
  const { input, meta } = useField(field, {
    validate: composeValidators(validateEmail, required ? validateRequired : undefined),
  });

  return (
    <BackstageInput
      className="InputText"
      label={label}
      error={meta.error}
      required={required}
      touched={meta.touched}
      inputId={field}
    >
      <input
        type="email"
        id={field}
        {...rest}
        {...(input as Record<string, unknown>)}
        className={meta.error && meta.touched ? 'error' : ''}
      />
    </BackstageInput>
  );
};

export default BackstageFormInputText;
