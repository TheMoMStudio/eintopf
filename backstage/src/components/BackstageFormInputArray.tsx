// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, ComponentChild, VNode } from 'preact';
import { useField } from 'react-final-form';
import BackstageInput from './BackstageInput';
import Button from './Button';
import './BackstageFormInputArray.css';

interface BackstageFormInputArrayProps<T> {
  field: string;
  label?: string;
  info?: string | VNode;
  fieldComponent: (props: { value: T; onChange: (value: T) => void }) => ComponentChild;
  newValue: () => T;
  minLength?: number;
  name: string;
}

export const BackstageFormInputArray = <T,>({
  field,
  label,
  info,
  fieldComponent,
  newValue,
  minLength = 0,
  name,
}: BackstageFormInputArrayProps<T>): VNode => {
  const { input, meta } = useField<T[]>(field, { defaultValue: [newValue()] });
  const addTag = (value: T): void => {
    const newArray: T[] = input.value ? [...input.value] : [];
    newArray.push(value);
    input.onChange(newArray);
  };
  const removeValue = (i: number): void => {
    if (input.value === undefined) return;
    const newTags = [...input.value];
    newTags.splice(i, 1);
    input.onChange(newTags);
  };
  const changeValue = (i: number, value: T): void => {
    if (input.value === undefined) return;
    const newTags = [...input.value];
    newTags[i] = value;
    input.onChange(newTags);
  };

  return (
    <BackstageInput className="BackstageInputArray" label={label} info={info} inputId={field}>
      {meta.error && meta.touched && <span className="error">{meta.error}</span>}
      <div className="ArrayItems">
        {input.value &&
          input.value.map((value, i) => (
            <div key={i} className="ArrayItem">
              {fieldComponent({
                value,
                onChange: (v: T) => changeValue(i, v),
              })}
              {i >= minLength && (
                <Button
                  className="ButtonRemoveItem"
                  onClick={() => removeValue(i)}
                  variant="secondary"
                >
                  Entfernen
                </Button>
              )}
            </div>
          ))}
        <Button className="ButtonAddItem" onClick={() => addTag(newValue())} variant="secondary">
          {name} hinzufügen
        </Button>
      </div>
    </BackstageInput>
  );
};

export default BackstageFormInputArray;
