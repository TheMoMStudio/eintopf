// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { queryGroupList, Group } from '../api/group';
import BackstageFormInputReference from './BackstageFormInputReference';
import { HideWhenCondition } from './BackstageInput';

export interface BackstageFormInputReferenceGroupProps {
  field: string;
  label?: string;
  info?: string;
  hideWhen?: HideWhenCondition;
}

export const BackstageFormInputReferenceGroup: FunctionComponent<
  BackstageFormInputReferenceGroupProps
> = ({ field, label, info, hideWhen }) => (
  <BackstageFormInputReference<Group>
    field={field}
    label={label}
    mustOwn
    queryList={queryGroupList}
    displayField="name"
    info={info}
    hideWhen={hideWhen}
  />
);

export default BackstageFormInputReferenceGroup;
