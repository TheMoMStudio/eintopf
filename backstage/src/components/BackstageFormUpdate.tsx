// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, toChildArray, ComponentChildren, VNode } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import { route } from 'preact-router';
import useUser from '../hooks/useUser';
import BackstageForm from './BackstageForm';
import BackstageFormButtonSubmit from './BackstageFormButtonSubmit';
import { useAppData } from '../appData';

export interface BackstageFormUpdateProps<T> {
  children: ComponentChildren;
  id: string;
  queryUpdate: (apiurl: string, token: string, model: T) => Promise<T | string>;
  queryGet: (apiurl: string, token: string, id: string) => Promise<T | string>;
  model: string;
  onUpdate?: (model: T) => void;
  validate?: (values: T) => string | undefined;
}

export const BackstageFormUpdate = <T extends { id: string }>({
  children,
  id,
  queryUpdate,
  queryGet,
  model,
  onUpdate,
  validate,
}: BackstageFormUpdateProps<T>): VNode => {
  const { user } = useUser();
  const { apiurl } = useAppData();

  const [initialValues, setInitialValues] = useState<T | undefined>(undefined);
  useEffect(() => {
    const query = async () => {
      if (!user) return;

      const response = await queryGet(apiurl, user.token, id);
      if (typeof response === 'string') return;
      setInitialValues(response);
    };
    query();
  }, [queryGet, id, user, apiurl]);

  const onSubmit = async (values: T): Promise<string | undefined> => {
    if (!user) return undefined;

    if (validate) {
      const err = validate(values);
      if (err) {
        return err;
      }
    }

    const response = await queryUpdate(apiurl, user.token, values);
    if (typeof response === 'string') {
      return response;
    }

    if (onUpdate) {
      onUpdate(values);
    } else {
      route(`/backstage/${model}`);
    }
    return undefined;
  };

  return (
    <BackstageForm<T> initialValues={initialValues} onSubmit={onSubmit} requiredNotice>
      {toChildArray(children).map((child) => child)}
      <BackstageFormButtonSubmit>Speichern</BackstageFormButtonSubmit>
    </BackstageForm>
  );
};

export default BackstageFormUpdate;
