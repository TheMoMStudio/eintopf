// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useEffect, useState, useMemo } from 'preact/hooks';
import { useField } from 'react-final-form';
import { querySearch, TermsBucket, TermsAggregation } from '../api/search';
import BackstageInput from './BackstageInput';
import BackstageInputAutocomplete from './BackstageInputAutocomplete';
import Button from './Button';
import './BackstageFormInputArray.css';
import { useAppData } from '../appData';

interface BackstageFormInputArrayTagProps {
  field: string;
  label?: string;
  info?: string;
}

export const BackstageFormInputArrayTag: FunctionComponent<BackstageFormInputArrayTagProps> = ({
  field,
  label,
  info,
}) => {
  const { apiurl } = useAppData();
  const { input: tagsInput } = useField<{ name: string }[]>(field);
  const [tags, setTags] = useState<string[]>([]);

  useEffect(() => {
    querySearch(apiurl, {
      query: '',
      sort: '',
      page: 0,
      pageSize: 0,
      filters: [],
      aggregations: { tags: { field: 'tag', type: TermsAggregation } },
    })
      .then((result) => {
        if (typeof result === 'string') {
          return;
        }
        if (!result.buckets || !result.buckets.tags) {
          return;
        }
        setTags(
          (result.buckets.tags as TermsBucket)
            .map((tag) => tag.term.toLowerCase())
            .filter((tag) => tag !== '')
            // Remove duplicate tags.
            .filter((value, index, self) => self.indexOf(value) === index),
        );
      })
      .catch(() => {});
  }, [apiurl]);

  const completionProvider = useMemo(
    () =>
      (text: string): Promise<string[]> => {
        const tagsFromInput = tagsInput.value.map((tag) => tag.name);
        // Remove tags, that are already used.
        const filteredTags = [...tags].filter((tag) => !tagsFromInput.includes(tag));

        if (text === '') return Promise.resolve(filteredTags);
        const newText = text.toLowerCase();

        return Promise.resolve(filteredTags.filter((tag) => tag.startsWith(newText)));
      },
    [tags, tagsInput.value],
  );

  const { input, meta } = useField<string[]>(field);
  const addTag = (value: string): void => {
    const newTags: string[] = input.value ? [...input.value] : [];
    newTags.push(value.toLowerCase());
    input.onChange(newTags);
  };
  const removeTag = (i: number): void => {
    if (input.value === undefined) return;
    const newTags = [...input.value];
    newTags.splice(i, 1);
    input.onChange(newTags);
  };
  const changeTag = (i: number, value: string): void => {
    if (input.value === undefined) return;
    const newTags = [...input.value];
    newTags[i] = value.toLowerCase();
    input.onChange(newTags);
  };
  return (
    <BackstageInput className="BackstageInputArray" label={label} info={info} inputId={field}>
      {meta.error && meta.touched && <span className="error">{meta.error}</span>}
      <div className="ArrayItems">
        {input.value &&
          input.value.map((tag, i) => (
            <div key={tag} className="ArrayItem">
              <BackstageInputAutocomplete
                value={tag}
                onChange={(value) => changeTag(i, value)}
                completionProvider={completionProvider}
                inputId={field}
              />
              <Button className="ButtonRemoveItem" onClick={() => removeTag(i)} variant="secondary">
                Entfernen
              </Button>
            </div>
          ))}
        <Button className="ButtonAddItem" onClick={() => addTag('')} variant="secondary">
          Tag hinzufügen
        </Button>
      </div>
    </BackstageInput>
  );
};

export default BackstageFormInputArrayTag;
