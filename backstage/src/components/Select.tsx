// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import './Select.css';

export interface SelectProps {
  id?: string;
  onChange: (e: any) => void;
  value: string;
  options: { value: string; name: string }[];
}

export const Select: FunctionComponent<SelectProps> = ({ id, onChange, value, options }) => (
  <select className="Select" onChange={onChange} id={id} value={value}>
    {options.map((o) => (
      <option key={o.value} value={o.value}>
        {o.name}
      </option>
    ))}
  </select>
);
export default Select;
