// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useState } from 'preact/hooks';

export interface PaginationState {
  page: number;
  totalPages: number;
  setPage: (page: number) => void;
}

export const usePagination = (pageSize: number): [PaginationState, (total: number) => void] => {
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const setTotal = (total: number): void => setTotalPages(Math.ceil(total / pageSize));

  return [{ page, totalPages, setPage }, setTotal];
};

export const Pagination: FunctionComponent<PaginationState> = ({ page, totalPages, setPage }) => (
  <div className="pagination">
    <button disabled={page <= 5} onClick={() => setPage(page - 5)}>
      &#60;&#60;
    </button>
    <button disabled={page <= 1} onClick={() => setPage(page - 1)}>
      &#60;
    </button>
    <span>
      Seite {page} von {totalPages}
    </span>
    <button disabled={page + 1 > totalPages} onClick={() => setPage(page + 1)}>
      &#62;
    </button>
    <button disabled={page + 5 > totalPages} onClick={() => setPage(page + 5)}>
      &#62;&#62;
    </button>
  </div>
);

export default Pagination;
