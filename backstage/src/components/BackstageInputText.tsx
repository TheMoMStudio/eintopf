// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { BackstageFormInputProps } from './BackstageFormInput';
import BackstageInput from './BackstageInput';

export interface BackstageFormInputTextProps extends BackstageFormInputProps<string> {
  type?: 'email' | 'password' | 'text' | 'textarea';
  value: string;
  onChange: (e: any) => void;
  placeholder?: string;
}

export const BackstageFormInputText: FunctionComponent<
  Omit<BackstageFormInputTextProps, 'field'>
> = ({ label, required, info, type = 'text', ...rest }) => (
  <BackstageInput
    className="InputText"
    label={label}
    required={required}
    info={info}
    inputId="InputText"
  >
    {type === 'textarea' ? <textarea {...rest} /> : <input type={type} {...rest} />}
  </BackstageInput>
);

export default BackstageFormInputText;
