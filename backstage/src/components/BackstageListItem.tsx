// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, ComponentChild, FunctionComponent, Fragment } from 'preact';
import { useContext } from 'preact/hooks';
import { checkNeedsRole, NeedsRole } from '../utils/role';
import useUser from '../hooks/useUser';
import { ListContext, ListContextType } from './BackstageList';

export interface BackstageListItemProps {
  needsRole?: NeedsRole;
  shouldRender?: (model: Record<string, any>) => boolean;
}

export interface BackstageListItemInternalProps extends BackstageListItemProps {
  children: ComponentChild;
  className?: string;
}

export const BackstageListItem: FunctionComponent<BackstageListItemInternalProps> = ({
  children,
  needsRole,
  shouldRender,
  className,
}) => {
  const { user } = useUser();
  const listContext = useContext<ListContextType | undefined>(ListContext);
  if (listContext === undefined) {
    console.error('useContext(ListContext) used outside a ListContext.Provider');
    return <Fragment />;
  }
  const { item } = listContext;

  if (!user) return <Fragment />;

  if (shouldRender && !shouldRender(item)) {
    return <Fragment />;
  }

  if (needsRole && checkNeedsRole(needsRole, user.role)) {
    return <Fragment />;
  }

  return <td className={className}>{children}</td>;
};

export default BackstageListItem;
