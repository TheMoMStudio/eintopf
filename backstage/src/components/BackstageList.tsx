// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, toChildArray, createContext, Fragment, VNode } from 'preact';
import { useRef, useEffect, useState } from 'preact/hooks';
import { RoleNormal, RoleAdmin } from '../api/auth';
import { BaseModel, QueryCrud } from '../api/crud';
import useBackstageList, { BackstageListConfig } from '../hooks/useBackstageList';
import useUser from '../hooks/useUser';
import { format, newDate } from '../utils/date';
import { checkNeedsRole } from '../utils/role';
import { BackstageListActionProps } from './BackstageListAction';
import {
  isBackstageListFieldName,
  isBackstageListField,
  BackstageListFieldProps,
} from './BackstageListField';
import {
  isBackstageListFilterToggle,
  BackstageListFilterToggleProps,
  RenderBackstageListFilterToggle,
} from './BackstageListFilterToggle';
import {
  isBackstageListFilterSelect,
  RenderBackstageListFilterSelect,
} from './BackstageListFilterSelect';
import SortButton from './BackstageListSortButton';
import './BackstageList.css';
import ConfirmDialog from './ConfirmDialog';
import Button from './Button';
import Container from './Container';
import {
  isBackstageListFilterText,
  RenderBackstageListFilterText,
} from './BackstageListFilterText';
import { useAppData } from '../appData';

export interface ListContextType {
  queryCrud: QueryCrud<BaseModel>;
  token: string;
  refresh: () => Promise<void>;
  item: Record<string, unknown>;
}

export const ListContext = createContext<ListContextType | undefined>(undefined);

export type BackstageListFilter = VNode<BackstageListFilterToggleProps>;
export type BackstageListItem<T extends object> = VNode<
  BackstageListActionProps<T> | BackstageListFieldProps<T>
>;

export interface BackstageListProps<T extends object> {
  id?: string;
  idField?: keyof T;
  title: string;
  children: BackstageListItem<T>[] | BackstageListItem<T>;
  queryCrud: QueryCrud<T>;
  config: BackstageListConfig;
  filters?: BackstageListFilter[];
  hideBulkActions?: boolean;
  noItemsInfo?: string;
  onPageSizeChange?: (pageSize: number) => void;
  onClickRow?: (model: T) => void;
  reload?: number;
  bulkDeleteConfirmMessage: string;
}

export const BackstageList = <T extends object>({
  id,
  // @ts-ignore
  idField = 'id',
  title,
  children,
  queryCrud,
  config,
  filters,
  hideBulkActions,
  noItemsInfo,
  onPageSizeChange,
  onClickRow,
  reload,
  bulkDeleteConfirmMessage,
}: BackstageListProps<T>): VNode => {
  const { user } = useUser();
  const { timezone } = useAppData();
  const {
    queryCrud: queryCrud2,
    state,
    refresh,
    filter,
    sorting,
    sort,
    pagination,
    paginate,
    setPageSize,
    selection,
    deleteSelected,
    deactivateSelected,
  } = useBackstageList(queryCrud, config);

  const [lastReload, setLastReload] = useState(0);
  useEffect(() => {
    if (reload === undefined) return;
    if (reload === 0) return;
    if (reload === lastReload) return;

    refresh();
    setLastReload(reload);
  }, [reload, refresh, lastReload]);

  const actionRef = useRef<HTMLSelectElement | null>(null);
  const checkboxRef = useRef<HTMLInputElement | null>(null);

  const selectAction = async (action: () => Promise<void>) => {
    deselectAction();
    await action();
  };

  const deselectAction = () => {
    if (actionRef.current !== null) {
      actionRef.current.value = 'default';
    }
    if (checkboxRef.current !== null) {
      checkboxRef.current.checked = false;
    }
  };

  const [showDeleteConfirm, setShowDeleteConfirm] = useState(false);

  const selectPageSize = (pageSize: number) => {
    setPageSize(pageSize);
    if (onPageSizeChange) {
      onPageSizeChange(pageSize);
    }
  };

  if (!user) {
    return <Fragment />;
  }

  const hasBulkActions = hideBulkActions !== true && user.role !== RoleNormal;
  const hasSelection = selection.selected.find((selected) => selected);

  return (
    <Container
      title={`${title} (${state.data ? state.data.total : '?'})`}
      id={id}
      className="BackstageList"
    >
      <div className="BackstageListTop">
        {(!hasSelection || !hasBulkActions) && (
          <>
            <select className="BackstageListSelect">
              {[10, 25, 50, 100].map((pageSize) => (
                <option
                  key={pageSize}
                  value={pageSize}
                  selected={config.pageSize === pageSize}
                  onClick={() => selectPageSize(pageSize)}
                >
                  {pageSize}
                </option>
              ))}
            </select>
            {filters &&
              (toChildArray(filters) as VNode[]).map((f) => {
                const { props } = f;
                if (isBackstageListFilterToggle(props)) {
                  if (checkNeedsRole(props.needsRole, user.role)) {
                    return undefined;
                  }
                  return (
                    <RenderBackstageListFilterToggle
                      key={props.label}
                      {...props}
                      filter={filter as any}
                    />
                  );
                }
                if (isBackstageListFilterSelect(props)) {
                  return (
                    <RenderBackstageListFilterSelect
                      key={props.label}
                      {...props}
                      filter={filter as any}
                    />
                  );
                }
                if (isBackstageListFilterText(props)) {
                  return (
                    <RenderBackstageListFilterText
                      key={props.label}
                      {...props}
                      filter={filter as any}
                    />
                  );
                }
                return undefined;
              })}
          </>
        )}
        {hasBulkActions && hasSelection && (
          <>
            <span>{selection.selected.filter((selected) => selected).length} Ausgewählt</span>
            {user.role === RoleAdmin && (
              <Button variant="danger" onClick={() => setShowDeleteConfirm(true)}>
                Löschen
              </Button>
            )}
            <Button onClick={() => selectAction(deactivateSelected)}>Deaktivieren</Button>
            {showDeleteConfirm && (
              <ConfirmDialog
                message={bulkDeleteConfirmMessage}
                onAbort={() => {
                  setShowDeleteConfirm(false);
                  deselectAction();
                }}
                onConfirm={() => {
                  setShowDeleteConfirm(false);
                  selectAction(deleteSelected);
                }}
              />
            )}
          </>
        )}
      </div>
      <table>
        <thead>
          <tr>
            {hideBulkActions !== true && user.role !== RoleNormal && (
              <th>
                <input type="checkbox" ref={checkboxRef} onClick={selection.toggleSelectAll} />
              </th>
            )}
            {(toChildArray(children) as VNode<BackstageListFieldProps<T>>[]).map(
              (field) =>
                !checkNeedsRole(field.props.needsRole, user.role) && (
                  <th key={field.props.name}>
                    {isBackstageListFieldName(field.props) &&
                      (field.props.sortable === undefined || field.props.sortable === true ? (
                        <SortButton
                          label={field.props.name}
                          field={field.props.field}
                          sorting={sorting.field || ''}
                          order={sorting.order || 'ASC'}
                          sort={sort}
                        />
                      ) : (
                        field.props.name
                      ))}
                  </th>
                ),
            )}
          </tr>
        </thead>
        <tbody>
          {state.data &&
            state.data.items.map((item, i) => (
              <tr key={item[idField]} onClick={onClickRow ? () => onClickRow(item) : undefined}>
                {hideBulkActions !== true && user.role !== RoleNormal && (
                  <td>
                    <input
                      type="checkbox"
                      checked={selection.selected[i]}
                      onClick={() => selection.toggleSelect(i)}
                    />
                  </td>
                )}
                {(toChildArray(children) as BackstageListItem<T>[]).map(
                  (field) =>
                    !checkNeedsRole(field.props.needsRole, user.role) && (
                      <ListContext.Provider
                        key=""
                        value={{
                          queryCrud: queryCrud2 as unknown as QueryCrud<BaseModel>,
                          token: user.token,
                          refresh,
                          item: item as Record<string, unknown>,
                        }}
                      >
                        {isBackstageListField<T>(field.props) && (
                          <td key={field.props.name}>
                            {field.props.display === 'text' && item[field.props.field]}
                            {field.props.display === 'boolean' &&
                              (item[field.props.field] ? (
                                <span className="Field-Boolean-true">Ja</span>
                              ) : (
                                <span className="Field-Boolean-false">Nein</span>
                              ))}
                            {field.props.display === 'datetime' &&
                              format(
                                timezone,
                                newDate(timezone, item[field.props.field] as unknown as string),
                                'dd.MM.yyyy HH:mm',
                              )}
                            {field.props.display === 'custom' &&
                              field.props.render &&
                              field.props.render(
                                item[field.props.field] as unknown as string,
                                item,
                                refresh,
                              )}
                          </td>
                        )}
                        {field}
                      </ListContext.Provider>
                    ),
                )}
              </tr>
            ))}
        </tbody>
      </table>
      {state.data && state.data.items.length > 0 && (
        <div className="BackstageListPagination">
          <button disabled={pagination.current === 1} onClick={() => paginate('first')}>
            &#60;&#60;
          </button>
          <button disabled={pagination.current === 1} onClick={() => paginate('prev')}>
            &#60;
          </button>
          <span>
            Seite {pagination.current} von {pagination.max}
          </span>
          <button disabled={pagination.current === pagination.max} onClick={() => paginate('next')}>
            &#62;
          </button>
          <button disabled={pagination.current === pagination.max} onClick={() => paginate('last')}>
            &#62;&#62;
          </button>
        </div>
      )}
      {state.data && state.data.items.length === 0 && noItemsInfo && (
        <div className="BackstageListNoItems">{noItemsInfo}</div>
      )}
    </Container>
  );
};

export default BackstageList;
