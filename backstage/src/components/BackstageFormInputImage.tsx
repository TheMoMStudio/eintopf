// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, RefObject } from 'preact';
import { useState, useRef } from 'preact/hooks';
import { useField } from 'react-final-form';
import { mediaUrl, queryAddFile } from '../api/media';
import useUser from '../hooks/useUser';
import { BackstageFormInputProps } from './BackstageFormInput';
import BackstageInput from './BackstageInput';
import BackstageError from './BackstageError';
import './BackstageFormInputImage.css';
import { mapError } from '../api/error';
import { useAppData } from '../appData';

export interface ImagePreview {
  name: string;
  className?: string;
  width?: number;
  height?: number;
}

export interface BackstageFormInputImageProps extends BackstageFormInputProps<string> {
  previews?: ImagePreview[];
}

export const BackstageFormInputImage: FunctionComponent<BackstageFormInputImageProps> = ({
  field,
  label,
  previews = [{ name: 'Vorschau', height: 100, width: 100 }],
}) => {
  const { apiurl } = useAppData();
  const { user } = useUser();
  const { input: mediaInput } = useField(field);
  const inputRef = useRef<HTMLInputElement>();
  const [error, setError] = useState('');
  const upload = async () => {
    if (!user) return;
    if (!inputRef.current) return;
    if (!inputRef.current.files) return;
    if (!inputRef.current.files.length) return;

    const formData = new FormData();
    formData.append('media', inputRef.current.files[0]);

    const response = await queryAddFile(apiurl, user.token, formData);
    if (typeof response === 'string') {
      setError(response);
      return;
    }
    mediaInput.onChange(response.id);
    setError('');
  };

  return (
    <BackstageInput
      className="InputImage"
      label={label}
      info="Es können Bilder mit einer größe von maximal 1Mb verwendet werden."
      inputId={field}
    >
      <input
        ref={inputRef as RefObject<HTMLInputElement>}
        type="file"
        onChange={upload}
        id={field}
      />
      {error && <BackstageError err={mapError(error)} />}
      {mediaInput.value &&
        previews.map((preview) => (
          <div key={preview.name} className="InputImage-Preview">
            <span className="InputImage-Preview-Name">{preview.name}</span>
            <div
              className={
                preview.className ? preview.className : 'InputImage-Preview-Image-Container'
              }
              style={{
                width: preview.width ? `${preview.width}px` : '',
                height: preview.height ? `${preview.height}px` : '',
              }}
            >
              <img
                className="InputImage-Preview-Image"
                src={mediaUrl(apiurl, mediaInput.value)}
                alt={preview.name}
              />
            </div>
          </div>
        ))}
    </BackstageInput>
  );
};

export default BackstageFormInputImage;
