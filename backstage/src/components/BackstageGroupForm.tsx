// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import BackstageFormInputArrayUser from '../components/BackstageFormInputArrayUser';
import BackstageFormInputCheckbox from '../components/BackstageFormInputCheckbox';
import BackstageFormInputImage from '../components/BackstageFormInputImage';
import BackstageFormInputLink from '../components/BackstageFormInputLink';
import BackstageFormInputText from '../components/BackstageFormInputText';
import BackstageFormInputTextLong from '../components/BackstageFormInputTextLong';

export const BackstageGroupFormFields: FunctionComponent = () => (
  <Fragment>
    <BackstageFormInputCheckbox
      field="published"
      label="Veröffentlicht"
      defaultValue={true}
      info="Die Gruppe wird erst in der Liste angezeigt, wenn er veröffentlicht wurde. Nicht veröffentlichte Gruppen können per Url aufgerufen werden. Die Veröffentlichung kann jederzeit rückgängig gemacht werden."
    />
    <BackstageFormInputText field="name" label="Name" required />
    <BackstageFormInputLink field="link" label="Website" />
    <BackstageFormInputText field="email" label="E-Mail" />
    <BackstageFormInputTextLong field="description" label="Beschreibung" />
    <BackstageFormInputImage
      field="image"
      label="Bild"
      previews={[
        {
          name: 'Vorschau Listen-Seite',
          className: 'InputImage-Preview-Image-Container-Listview',
          width: 380,
          height: 160,
        },
        { name: 'Vorschau Detail-Seite', width: 380 },
      ]}
      info="Das Bild wird auf der Gruppenseite mit einer Breite von 380px angezeigt. Auf der Listenseite wird das Bild im Format 380x160px angezeigt. Um Daten zu sparen sollte das Bild nicht zu groß sein."
    />
    <BackstageFormInputText field="alt" label="Bildbeschreibung" />
    <BackstageFormInputArrayUser
      field="ownedBy"
      label="Benutzer*innen"
      info="Es können beliebig viele Benutzer*innen hinzugefügt werden, die die Gruppe Bearbeiten können."
    />
  </Fragment>
);
