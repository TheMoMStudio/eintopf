// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h } from 'preact';
import { render, act } from '@testing-library/preact';
import userEvent from '@testing-library/user-event';
import BackstageFormInputArrayGroup from './BackstageFormInputArrayGroup';
import BackstageForm from './BackstageForm';
import { queryGroupList } from '../api/group';
import { User } from '../store/store';

// Mock for two groups
vi.mock('../api/group', async (importOriginal) => {
  const mod = await importOriginal<typeof import('../api/group')>();
  return {
    ...mod,
    queryGroupList: vi.fn(() => {
      const items = [
        {
          published: true,
          deactivated: false,
          name: 'foo',
          link: '',
          email: '',
          description: 'blabla',
          image: '',
          ownedBy: [''],
        },
        {
          published: true,
          deactivated: false,
          name: 'bar',
          link: '',
          email: '',
          description: 'blabla',
          image: '',
          ownedBy: [''],
        },
        {
          published: true,
          deactivated: false,
          name: 'two words',
          link: '',
          email: '',
          description: 'blabla',
          image: '',
          ownedBy: [''],
        },
      ];
      return { items, total: items.length };
    }),
  };
});
// mock for a user to be able to use the api
vi.mock('../hooks/useUser', async (importOriginal) => {
  const mod = await importOriginal<typeof import('../hooks/useUser')>();
  return {
    ...mod,
    useUser: vi.fn(() => {
      const user = { id: 'foo', role: 'admin', token: 'asfasdg' } as User;
      return { user };
    }),
  };
});

describe('BackstageFormInputArrayGroup', () => {
  beforeEach(() => {
    vi.useFakeTimers();
  });
  afterEach(() => {
    vi.useRealTimers();
    vi.restoreAllMocks();
  });

  it('completion list visible', async () => {
    const { getByTestId } = render(
      <BackstageForm onSubmit={vi.fn() as any} requiredNotice>
        <BackstageFormInputArrayGroup
          field="Fieldtext"
          label="Labeltext"
          info="Infotext"
          hideWhen={{ field: 'parent', is: (value) => (value as string).includes('revent') }}
        />
        ,
      </BackstageForm>,
    );

    // focus on input field
    const input = getByTestId('auto-input');
    await act(() => input.focus());
    await act(() => vi.runAllTimersAsync() as any);
    expect(queryGroupList).toHaveBeenCalledTimes(2);

    // check if completion list is visible
    await vi.waitFor(() => {
      const completions = getByTestId('auto-completions');
      expect(completions).toBeVisible();
      expect(completions.childElementCount).toBe(3);
    });
  });

  it('autocomplete filter working', async () => {
    const { getByTestId } = render(
      <BackstageForm onSubmit={vi.fn() as any} requiredNotice>
        <BackstageFormInputArrayGroup
          field="Fieldtext"
          label="Labeltext"
          info="Infotext"
          hideWhen={{ field: 'parent', is: (value) => (value as string).includes('revent') }}
        />
        ,
      </BackstageForm>,
    );

    // focus on input field
    let input = getByTestId('auto-input');
    await act(() => input.focus());
    await act(() => vi.runAllTimersAsync() as any);
    expect(queryGroupList).toHaveBeenCalledTimes(2);

    // type test word in input and verify it is visible
    userEvent.type(input, 'foo');
    await act(() => vi.runAllTimersAsync() as any);
    input = getByTestId('auto-input');
    expect(input).toHaveValue('foo');

    // check if completion list is filtered
    await vi.waitFor(() => {
      const completions = getByTestId('auto-completions');
      expect(completions).toBeVisible();
      expect(completions.childElementCount).toBe(1);
      expect(completions.childNodes[0].textContent).toBe('foo');
    });
  });

  it('autocomplete filter empty', async () => {
    const { getByTestId, queryByTestId } = render(
      <BackstageForm onSubmit={vi.fn() as any} requiredNotice>
        <BackstageFormInputArrayGroup
          field="Fieldtext"
          label="Labeltext"
          info="Infotext"
          hideWhen={{ field: 'parent', is: (value) => (value as string).includes('revent') }}
        />
        ,
      </BackstageForm>,
    );

    // focus on input field
    let input = getByTestId('auto-input');
    await act(() => input.focus());
    await act(() => vi.runAllTimersAsync() as any);
    expect(queryGroupList).toHaveBeenCalledTimes(2);

    // type test word in input and verify it is visible
    userEvent.type(input, 'nix');
    await act(() => vi.runAllTimersAsync() as any);
    input = getByTestId('auto-input');
    expect(input).toHaveValue('nix');

    // check if completion list is empty
    expect(queryByTestId('auto-completions')).not.toBeInTheDocument();
  });

  it('autocomplete filter capital letters', async () => {
    const { getByTestId } = render(
      <BackstageForm onSubmit={vi.fn() as any} requiredNotice>
        <BackstageFormInputArrayGroup
          field="Fieldtext"
          label="Labeltext"
          info="Infotext"
          hideWhen={{ field: 'parent', is: (value) => (value as string).includes('revent') }}
        />
        ,
      </BackstageForm>,
    );

    // focus on input field
    let input = getByTestId('auto-input');
    await act(() => input.focus());
    await act(() => vi.runAllTimersAsync() as any);
    expect(queryGroupList).toHaveBeenCalledTimes(2);

    // type test word in input and verify it is visible
    userEvent.type(input, 'FOO');
    await act(() => vi.runAllTimersAsync() as any);
    input = getByTestId('auto-input');
    expect(input).toHaveValue('FOO');

    // check if completion list is filtered
    await vi.waitFor(() => {
      const completions = getByTestId('auto-completions');
      expect(completions).toBeVisible();
      expect(completions.childElementCount).toBe(1);
      expect(completions.childNodes[0].textContent).toBe('foo');
    });
  });

  it('autocomplete filter mid position', async () => {
    const { getByTestId } = render(
      <BackstageForm onSubmit={vi.fn() as any} requiredNotice>
        <BackstageFormInputArrayGroup
          field="Fieldtext"
          label="Labeltext"
          info="Infotext"
          hideWhen={{ field: 'parent', is: (value) => (value as string).includes('revent') }}
        />
        ,
      </BackstageForm>,
    );

    // focus on input field
    let input = getByTestId('auto-input');
    await act(() => input.focus());
    await act(() => vi.runAllTimersAsync() as any);
    expect(queryGroupList).toHaveBeenCalledTimes(2);

    // type test word in input and verify it is visible
    userEvent.type(input, 'words');
    await act(() => vi.runAllTimersAsync() as any);
    input = getByTestId('auto-input');
    expect(input).toHaveValue('words');

    // check if completion list is filtered for correct entry
    await vi.waitFor(() => {
      const completions = getByTestId('auto-completions');
      expect(completions).toBeVisible();
      expect(completions.childElementCount).toBe(1);
      expect(completions.childNodes[0].textContent).toBe('two words');
    });
  });
});
