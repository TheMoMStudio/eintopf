// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useCallback, useEffect, useState } from 'preact/hooks';
import { useField } from 'react-final-form';
import { queryGroupList, Group } from '../api/group';
import { useUser } from '../hooks/useUser';
import BackstageInput, { HideWhenCondition } from './BackstageInput';
import BackstageInputAutocomplete from './BackstageInputAutocomplete';
import Button from './Button';
import './BackstageFormInputArray.css';
import { useAppData } from '../appData';

interface BackstageFormInputArrayGroupProps {
  field: string;
  label?: string;
  info?: string;
  hideWhen?: HideWhenCondition;
}

export const BackstageFormInputArrayGroup: FunctionComponent<BackstageFormInputArrayGroupProps> = ({
  field,
  label,
  info,
  hideWhen,
}) => {
  const { user } = useUser();
  const { apiurl } = useAppData();

  const { input, meta } = useField<string[]>(field);

  const addValue = (value: string): void => {
    const values: string[] = input.value ? [...input.value] : [];
    values.push(value);
    input.onChange(values);
  };
  const removeValue = (i: number): void => {
    if (input.value === undefined) return;
    const values = [...input.value];
    values.splice(i, 1);
    input.onChange(values);
  };
  const changeValue = (i: number, value: string): void => {
    if (input.value === undefined) return;
    const values = [...input.value];
    values[i] = value;
    input.onChange(values);
  };

  const [groups, setGroups] = useState<Group[]>([]);
  useEffect(() => {
    if (!user) return;

    const query = async () => {
      const response = await queryGroupList(apiurl, user.token, {
        filters: { ownedBy: [user.id] },
      });
      if (typeof response === 'string') {
        return;
      }
      setGroups(response.items);
    };
    query();
  }, [user, apiurl]);

  const groupCompletionProvider = useCallback(
    (text: string): Promise<string[]> => {
      if (!groups) {
        return Promise.resolve([]);
      }
      const completions = groups
        .map((g) => g.name)
        .filter((groupName) => groupName.toLowerCase().includes(text.toLowerCase()));
      return Promise.resolve(completions);
    },
    [groups],
  );

  const valueFromGroupName = useCallback(
    (groupName: string): string => {
      let id = '';
      groups.forEach((g) => {
        if (g.name === groupName) {
          id = g.id;
        }
      });
      if (id === '') {
        return '';
      }
      return `id:${id}`;
    },
    [groups],
  );
  const groupNameFromValue = useCallback(
    (value: string): string => {
      let groupName = '';
      let id = '';
      if (value.startsWith('id:')) {
        id = value.substring(3);
      } else {
        return '';
      }
      groups.forEach((g) => {
        if (g.id === id) {
          groupName = g.name;
        }
      });
      return groupName;
    },
    [groups],
  );

  const displayValue = (v: string): string => {
    return groups && groupNameFromValue(v) ? groupNameFromValue(v) : v;
  };

  return (
    <BackstageInput
      className="BackstageInputArray"
      label={label}
      info={info}
      hideWhen={hideWhen}
      inputId={field}
    >
      {meta.error && meta.touched && <span className="error">{meta.error}</span>}
      <div className="ArrayItems ArrayItems-FullWidth">
        {input.value &&
          input.value.map((value, i) => (
            <div key={i} className="ArrayItem">
              <BackstageInputAutocomplete
                value={displayValue(value)}
                onChange={(v) =>
                  changeValue(i, groups && valueFromGroupName(v) ? valueFromGroupName(v) : v)
                }
                completionProvider={groupCompletionProvider}
                displayValue={displayValue}
                inputId={field}
              />
              <Button
                className="ButtonRemoveItem"
                onClick={() => removeValue(i)}
                variant="secondary"
              >
                Entfernen
              </Button>
            </div>
          ))}
        {!input.value && (
          <div className="ArrayItem">
            <BackstageInputAutocomplete
              value=""
              onChange={(v) =>
                changeValue(0, groups && valueFromGroupName(v) ? valueFromGroupName(v) : v)
              }
              completionProvider={groupCompletionProvider}
              inputId={field}
            />
          </div>
        )}
        <Button className="ButtonAddItem" onClick={() => addValue('')} variant="secondary">
          Veranstalter*in hinzufügen
        </Button>
      </div>
    </BackstageInput>
  );
};

export default BackstageFormInputArrayGroup;
