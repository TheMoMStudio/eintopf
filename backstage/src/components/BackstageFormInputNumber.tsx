// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useField } from 'react-final-form';
import { BackstageFormInputProps } from './BackstageFormInput';
import BackstageInput, { HideWhenCondition } from './BackstageInput';

export interface BackstageFormInputNumberProps extends BackstageFormInputProps<any> {
  placeholder?: string;
  hideWhen?: HideWhenCondition;
}

export const BackstageFormInputNumber: FunctionComponent<BackstageFormInputNumberProps> = ({
  field,
  label,
  hideWhen,
  ...rest
}) => {
  const { input, meta } = useField(field, { parse: (value) => parseFloat(value) });

  return (
    <BackstageInput className="InputNumber" label={label} hideWhen={hideWhen} inputId={field}>
      {meta.error && meta.touched && <span className="error">{meta.error}</span>}
      <input
        type="number"
        id={field}
        {...rest}
        {...(input as Record<string, unknown>)}
        className={meta.error && meta.touched ? 'error' : ''}
      />
    </BackstageInput>
  );
};

export default BackstageFormInputNumber;
