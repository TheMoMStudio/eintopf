// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useField } from 'react-final-form';
import { Involved } from '../api/event';
import BackstageInput from './BackstageInput';
import BackstageInputText from './BackstageInputText';
import Button from './Button';
import './BackstageFormInputArray.css';

interface BackstageFormInputArrayInvolvedProps {
  field: string;
  label?: string;
  info?: string;
}

export const BackstageFormInputArrayInvolved: FunctionComponent<
  BackstageFormInputArrayInvolvedProps
> = ({ field, label, info }) => {
  const { input, meta } = useField<Involved[]>(field);

  const addInvovled = (): void => {
    const newInvovled: Involved[] = [...input.value];
    newInvovled.push({ name: '', description: '' });
    input.onChange(newInvovled);
  };
  const removeInvolved = (i: number): void => {
    if (input.value === undefined) return;
    const newInvolved = [...input.value];
    newInvolved.splice(i, 1);
    input.onChange(newInvolved);
  };
  const changeInvolvedName = (i: number, value: string): void => {
    if (input.value === undefined) return;
    const newInvolved = [...input.value];
    newInvolved[i].name = value;
    input.onChange(newInvolved);
  };
  const changeInvolvedDescription = (i: number, value: string): void => {
    if (input.value === undefined) return;
    const newInvolved = [...input.value];
    newInvolved[i].description = value;
    input.onChange(newInvolved);
  };

  return (
    <BackstageInput
      className="BackstageInputArray BackstageInputArrayInvolved"
      label={label}
      info={info}
      inputId={field}
    >
      {meta.error && meta.touched && <span className="error">{meta.error}</span>}
      <div className="ArrayItems-FullWidth ArrayItems-Multiline">
        {input.value &&
          input.value.map((invovled, i) => (
            <div key={invovled} className="ArrayItem">
              <BackstageInputText
                label="Name"
                value={invovled.name}
                onChange={(e) => changeInvolvedName(i, (e.target as HTMLInputElement).value)}
              />
              <BackstageInputText
                label="Beschreibung"
                value={invovled.description}
                onChange={(e) => changeInvolvedDescription(i, (e.target as HTMLInputElement).value)}
                type="textarea"
              />
              <Button
                className="ButtonRemoveItem"
                onClick={() => removeInvolved(i)}
                variant="secondary"
              >
                Entfernen
              </Button>
            </div>
          ))}
        <Button className="ButtonAddItem" onClick={() => addInvovled()} variant="secondary">
          Beteiligte*r hinzufügen
        </Button>
      </div>
    </BackstageInput>
  );
};

export default BackstageFormInputArrayInvolved;
