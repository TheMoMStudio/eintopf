// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';

export const BackstageUsagePrinciples: FunctionComponent = () => (
  <Fragment>
    <p>
      Der EINTOPF ist ein Kalender für emanzipatorische Politik und Kultur in Stuttgart und
      Umgebung.
    </p>
    <p>
      Auf dem EINTOPF können Veranstaltungen, Gruppen und Orte veröffentlicht werden. Personen,
      welche Einträge neu erstellen, sollten entweder Organisator*innen der Veranstaltung sein, Teil
      der Gruppe oder des Ortes sein, oder es mit den jeweiligen Personen abgesprochen haben.
    </p>
    <br />
    <p>
      Auf dem EINTOPF können unkommerzielle Veranstaltungen veröffentlicht werden. Dabei müssen
      diese nicht ausschließlich spendenbasiert organisiert sein, aber das Geld sollte nicht der
      Gewinnmaximierung von Privatpersonen und Unternehmen dienen. SInd Veranstaltungen auf einen
      Eintrittspreis/Teilnahmegebühr angewiesen, werden diese nicht grundsätzlich ausgeschlossen.
      Dies sollte jedoch nicht die Regel sein, damit der Fokus auf kostenfreien und damit
      zugänglicheren Veranstaltungen liegt.
    </p>
    <br />
    <p>
      Von der klassischen Politgruppe, über anarchafeministische Lesekreise, Nähbanden, IT-clubs bis
      hin zu unvorstellbaren weiteren Inhalten: jede selbstorganisierte Gruppe ist eingeladen den
      EINTOPF zu nutzen um öffentlich sichtbarer zu werden.
    </p>
    <br />
    <p>
      Auf dem EINTOPF können Orte veröffentlicht werden, die das gemeinschaftliche Zusammenleben in
      Städten hinterfragen und neu gestalten wollen. Auch kleine Kulturbetriebe wie Theater und
      Kinos sind explizit Willkommen, da sie einen wichtigen Ort für Kultur abseits von
      Massenproduktion und -konsum sind.
    </p>
    <br />
    <p>
      Auf dem EINTOPF gibt es keinen Platz für Sexismus, Rassismus, Antisemitismus,
      Trans*-/Homo-/Inter*-/Queerfeindlichkeit und jegliche andere Formen von Diskriminierung.
    </p>
    <br />
    <p>
      Der EINTOPF ist keine Plattform für Parteien und parteipolitische (Wahlkampf)-Veranstaltungen.
    </p>
    <br />
    <p>
      Für den Inhalt der Einträge an sich seid ihr als Veranstalter*innen jeweils selbst
      verantwortlich. Wir als Moderation behalten uns jedoch vor, Termine, Gruppen und Orte die
      nicht den genannten Kriterien entsprechen aus dem EINTOPF zu streichen.
    </p>
  </Fragment>
);
export default BackstageUsagePrinciples;
