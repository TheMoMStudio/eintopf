// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, toChildArray, ComponentChildren, VNode } from 'preact';
import 'preact/compat';
import { Form } from 'react-final-form';
import { FORM_ERROR, SubmissionErrors } from 'final-form';
import { mapError } from '../api/error';
import BackstageError from './BackstageError';
import './BackstageForm.css';

export interface BackstageFormProps<T> {
  children: ComponentChildren;
  initialValues?: T;
  className?: string;
  onSubmit: (values: T) => Promise<string | undefined>;
  requiredNotice?: boolean;
}

export const BackstageForm = <T,>({
  children,
  initialValues,
  onSubmit,
  className,
  requiredNotice,
}: BackstageFormProps<T>): VNode => {
  const onSubmitInternal = async (values: T): Promise<SubmissionErrors | undefined> => {
    const response = await onSubmit(values);
    if (typeof response === 'string') {
      document.getElementById('page')?.scrollTo({ top: 0, behavior: 'smooth' });
      return { [FORM_ERROR]: response };
    }

    return undefined;
  };

  return (
    <div>
      <Form<T>
        initialValues={initialValues}
        onSubmit={onSubmitInternal}
        render={({ handleSubmit, submitError }) =>
          (
            <form className={`BackstageForm ${className}`} onSubmit={handleSubmit} noValidate>
              {requiredNotice && (
                <div className="BackstageForm-InfoRequiredFields">
                  Mit * markierte Felder sind Pflichtfelder.
                </div>
              )}
              {toChildArray(children).map((child) => child)}
              {submitError && <BackstageError err={mapError(submitError)} />}
            </form>
          ) as any
        }
      />
    </div>
  );
};

export default BackstageForm;
