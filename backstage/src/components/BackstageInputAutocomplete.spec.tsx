// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { Fragment, h } from 'preact';
import { render, fireEvent, act } from '@testing-library/preact';
import userEvent from '@testing-library/user-event';
import BackstageInputAutocomplete from './BackstageInputAutocomplete';

describe('BackstageInputAutocomplete', () => {
  beforeEach(() => {
    vi.useFakeTimers();
  });
  afterEach(() => {
    vi.useRealTimers();
  });

  it('can select completion item', async () => {
    const onChange = vi.fn();
    const { getByTestId, getByText, getAllByText, queryByTestId } = render(
      <Fragment>
        <BackstageInputAutocomplete
          value="foo"
          inputId="foo"
          onChange={onChange}
          completionProvider={() => Promise.resolve(['foobar', 'foobaz'])}
        />
        ,
      </Fragment>,
    );

    let input = getByTestId('auto-input');
    await act(() => input.focus());
    await act(() => vi.runAllTimersAsync() as any);

    // check if completion list is visible
    const completions = getByTestId('auto-completions');
    expect(completions).toBeVisible();

    // test if input text is highlighted in suggestions
    const completionValues = getAllByText('foo');
    completionValues.forEach((compl) => {
      expect(compl.classList.toString()).toContain('CompletionValue');
    });

    // use bar instead of foobar, as it is interrupted by span tag
    const bar = getByText('bar');
    fireEvent.mouseDown(bar);
    await act(() => vi.runAllTimersAsync() as any);
    expect(onChange).toHaveBeenCalledWith('foobar');

    // check that selected item is available in input
    input = getByTestId('auto-input');
    expect(input).toHaveValue('foobar');

    // check if completion list is not visible anymore
    expect(queryByTestId('auto-completions')).not.toBeInTheDocument();
  });

  it('can add item by pressing ENTER', async () => {
    const onChange = vi.fn();
    const { getByTestId } = render(
      <Fragment>
        <BackstageInputAutocomplete
          value="foo"
          inputId="foo"
          onChange={onChange}
          completionProvider={() => Promise.resolve(['foobar', 'foobaz'])}
        />
        ,
      </Fragment>,
    );

    const input = getByTestId('auto-input');
    await act(() => input.focus());

    userEvent.type(input, 'bar');
    await act(() => vi.runAllTimersAsync() as any);

    fireEvent.keyDown(input, { keyCode: 13 });
    await act(() => vi.runAllTimersAsync() as any);

    expect(onChange).toHaveBeenCalledWith('foobar');
  });

  it('can select item by clicking outside the element', async () => {
    const onChange = vi.fn();
    const { getByTestId } = render(
      <Fragment>
        <div data-testid="outside" />
        <BackstageInputAutocomplete
          value="foo"
          inputId="foo"
          onChange={onChange}
          completionProvider={() => Promise.resolve([])}
        />
      </Fragment>,
    );

    const input = getByTestId('auto-input');
    fireEvent.focus(input);
    await act(() => input.focus());

    userEvent.type(input, 'bar');
    await act(() => vi.runAllTimersAsync() as any);

    fireEvent.click(getByTestId('outside'));
    await act(() => vi.runAllTimersAsync() as any);

    expect(onChange).toHaveBeenCalledWith('foobar');
  });
});
