// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';

export const BackstageFAQ: FunctionComponent = () => (
  <Fragment>
    <h3>Wie können mehrere Menschen die Veranstaltung, Gruppe oder Ort bearbeiten?</h3>
    <p>
      Über das Feld <i>Benutzer*innen</i> kannst du weiteren Benutzer*innen die Berechtigung geben,
      den Eintrag zu bearbeiten. Über den Menupunkt <i>BENUTZER*IN EINLADEN</i> kannst du weitere
      Menschen einladen.
    </p>
    <br />

    <h3>Meine Veranstaltung wird nicht im Kalender angezeigt. Woran kann das liegen?</h3>
    <p>
      Es gibt zwei mögliche Gründe, warum eine Veranstaltung nicht im Kalender angezeigt wird.
      Entweder das Feld <i>Veröffentlicht</i> wurde nicht ausgewählt. Oder die Veranstaltung wurde
      einer Hauptveranstaltung zugeordnet. Dann erscheint der Eintrag nur als Programmpunkt der
      Hauptveranstaltung. Damit die Veranstaltungen auch alleine im Kalender angezeigt wird, muss
      das Feld <i>Eigenständig im Kalender anzeigen</i> ausgewählt werden.
    </p>
    <br />

    <h3>Kann ich eine Vorschau von einer Veranstaltung sehen?</h3>
    <p>
      Solange die Veranstaltung nicht veröffentlicht wurde, wird sie nicht im Kalender angezeigt. Du
      kannst die Veranstaltung aber trotzdem über den <i>Vorschau</i>-Button ansehen. Die Url der
      Veranstaltung kannst du auch an andere schicken.
    </p>
    <br />

    <h3>Wie lege ich eine Veranstaltung mit mehreren Programmpunkten an?</h3>
    <p>
      Du kannst für eine größere Veranstaltung (Festival, Camp, ...) einzelne Programmpunkte als
      eigene Veranstaltungen eintragen. Lege dazu zuerst die Hauptveranstaltung an. Als nächstes
      können die Teilveranstaltungen angelegt werden. Diese müssen der Hauptveranstaltung über das
      Feld
      <i>Teil einer Hauptveranstaltung</i> zugeordnet werden und im Zeitraum der Hauptveranstaltung
      liegen.
    </p>
    <br />
    <p>
      Teilveranstaltungen werden im Kalender als Programmpunkte der Hauptveranstaltung angezeigt.
      Wenn eine Teilveranstaltungen auch unabhängig von der Hauptveranstaltung angezeigt werden
      soll, musst du das Feld <i>Eigenständig im Kalender anzeigen</i> auswählen.
    </p>
    <br />

    <h3>Meine Gruppe oder Ort wird nicht in der Liste angezeigt. Woran kann das liegen?</h3>
    <p>
      Die Gruppe oder Ort wird erst in der Liste angezeigt, wenn das Feld <i>Veröffentlicht</i>{' '}
      ausgewählt wurde.
    </p>
    <br />

    <h3>Mein Ort wird nicht auf der Karte angezeigt. Woran kann das liegen?</h3>
    <p>
      Der Ort wird nur auf der Karte angezeigt, wenn dafür Koordinaten angegeben wurden. Diese
      können entweder manuell eingetragen oder über den Button{' '}
      <i>Koordinaten über openstreetmaps generieren</i> erstellt werden. Dafür muss zuerst eine
      korrekte Adresse eintragen werden.
    </p>
    <br />
  </Fragment>
);
export default BackstageFAQ;
