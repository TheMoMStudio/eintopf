// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import { useAppData } from '../appData';
import Link from '../components/Link';
import { useEffect, useState } from 'preact/hooks';
import useUser from '../hooks/useUser';
import './BackstageThemeFileOption.css';
import { checkError } from '../signals/notification';
import BackstageTabs from './BackstageTabs';
import BackstageCodeEditor from './BackstageCodeEditor';
import BackstageFileUpload from './BackstageFileUpload';
import { fileBaseUrl, queryCms, ThemeFiletype } from '../api/cms';

const filetypeStaticContent = [
  {
    id: ThemeFiletype.TEMPLATE,
    name: 'Template',
    description: 'In den Templates kann das HTML des Kalenders verändert werden.',
    acceptedFiletypes: ['tpl'],
  },
  {
    id: ThemeFiletype.CSS,
    name: 'CSS',
    description: 'Mit CSS kann das Aussehen des Kalenders verändert werden.',
    acceptedFiletypes: ['css'],
  },
  {
    id: ThemeFiletype.JS,
    name: 'JS',
    description: 'Mit Javascript kurz JS kann das Verhalten des Kalenders verändert werden.',
    acceptedFiletypes: ['js'],
  },
  {
    id: ThemeFiletype.IMAGE,
    name: 'Bilder',
    description: 'Hier kannst du die Bilder des Kalenders anpassen.',
    acceptedFiletypes: ['webp', 'jpg', 'jpeg', 'png', 'svg', 'ico'],
  },
  {
    id: ThemeFiletype.FONT,
    name: 'Schriftarten',
    description: 'Hier kannst du die Schriftarten des Kalenders anpassen.',
    acceptedFiletypes: ['ttf'],
  },
];

const hasBinaryOption = [ThemeFiletype.IMAGE, ThemeFiletype.FONT];

export interface BackstageThemeFileOptionProps {
  filetype: ThemeFiletype;
  filePath: string;
}

export const BackstageThemeFileOption: FunctionComponent<BackstageThemeFileOptionProps> = ({
  filetype = ThemeFiletype.TEMPLATE,
  filePath,
}) => {
  const isValidPath = Object.values(ThemeFiletype).includes(filetype as ThemeFiletype);
  const { apiurl, notifications } = useAppData();
  const { user } = useUser();
  const [fileNames, setFileNames] = useState<string[]>([]);
  const [fileData, setFileData] = useState<string>();
  const [fileBrowser, setFileBrowser] = useState<any>();

  useEffect(() => {
    if (!isValidPath) return;

    const openFile = async (openFilePath: string) => {
      if (!user || !filetype) return;
      const response = await queryCms(
        apiurl,
        user.token,
        'GET',
        `/cms/files/${filetype}/${openFilePath}`,
      );
      if (checkError(response, notifications, false, 'Diese Datei konnte nicht geöffnet werden!'))
        return;
      setFileData(response.content);
    };

    const query = async () => {
      if (!user) return;

      const response = await queryCms(apiurl, user.token, 'GET', `/cms/files/${filetype}`);
      if (checkError(response, notifications, false)) return;
      setFileNames(response);

      if (!filePath) return;
      await openFile(filePath);
    };
    query();
  }, [filetype, filePath, user, apiurl, isValidPath, notifications]);

  useEffect(() => {
    if (!fileNames || !filetype || !user) {
      return;
    }
    const fileType = filetypeStaticContent.find((el) => el.id === filetype);
    setFileBrowser(
      fileNames?.map((file: string, i: number) => {
        if (!hasBinaryOption.includes(filetype)) {
          return (
            <Link
              key={`language-${i}`}
              className="File"
              href={`${fileBaseUrl}${filetype}/${btoa(file)}`}
            >
              {file}
            </Link>
          );
        }

        return filetype === ThemeFiletype.IMAGE ? (
          <BackstageFileUpload
            isTextFile={file.includes('.svg')}
            user={user}
            src={`/assets/images/${file}`}
            name={file}
            accept={fileType?.acceptedFiletypes}
          />
        ) : (
          <BackstageFileUpload
            isTextFile={false}
            user={user}
            src=""
            name={file}
            fileType={filetype}
            placeholderSrc="/assets/images/icon_edit.svg"
            accept={fileType?.acceptedFiletypes}
          />
        );
      }),
    );
  }, [fileNames, filetype, user]);

  if (!isValidPath) {
    return (
      <Fragment>
        <p>Dieser Dateityp existiert nicht! Wähle einen der folgenden Typen:</p>
        <BackstageTabs>
          {filetypeStaticContent.map((staticFiletype, i) => {
            return (
              <Link key={`language-tab-${i}`} href={`${fileBaseUrl}${staticFiletype.id}`}>
                {staticFiletype.name}
              </Link>
            );
          })}
        </BackstageTabs>
      </Fragment>
    );
  }

  return (
    <div class="Theme-Code-Option">
      <p className="Description">
        In diesem Teil der Seite kannst du den Code des Themes anpassen.
        <br />
        Wähle dazu einen Deteityp aus.
      </p>
      <BackstageTabs>
        {filetypeStaticContent.map((staticFiletype, i) => {
          return (
            <Link
              key={`language-tab-${i}`}
              className={`${staticFiletype.id === filetype ? 'active' : ''}`}
              href={`${fileBaseUrl}${staticFiletype.id}`}
            >
              {staticFiletype.name}
            </Link>
          );
        })}
      </BackstageTabs>

      {filetypeStaticContent.map((staticFiletype) => {
        return staticFiletype.id === filetype ? (
          <p className="Language-Info">{staticFiletype.description}</p>
        ) : (
          <Fragment />
        );
      })}
      {fileNames?.length === 0 && <p>Es wurden keine Dateien in dieser Kategorie gefunden!</p>}
      {fileNames?.length > 0 && (
        <div className="Browser">
          <nav className={`Browser-Files ${hasBinaryOption.includes(filetype) && 'upload'}`}>
            {fileBrowser}
          </nav>
        </div>
      )}
      {filePath && (fileData || fileData === '') && (
        <BackstageCodeEditor
          fileData={fileData}
          language={filetype}
          apiEndpoint={`/cms/files/${filetype}/${filePath}`}
        />
      )}
    </div>
  );
};

export default BackstageThemeFileOption;
