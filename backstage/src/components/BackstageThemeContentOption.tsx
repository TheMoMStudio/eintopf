// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useAppData } from '../appData';
import Link from '../components/Link';
import { useEffect, useState } from 'preact/hooks';
import useUser from '../hooks/useUser';
import { checkError } from '../signals/notification';
import BackstageCodeEditor from './BackstageCodeEditor';
import { contentBaseUrl, queryCms, ThemeFiletype } from '../api/cms';

export interface BackstageThemeContentOptionProps {
  filepath: string;
}

export const BackstageThemeContentOption: FunctionComponent<BackstageThemeContentOptionProps> = ({
  filepath,
}) => {
  const { apiurl, notifications } = useAppData();
  const { user } = useUser();
  const [languageData, setLanguageData] = useState<string[]>([]);
  const [fileData, setFileData] = useState<string>();

  useEffect(() => {
    const query = async () => {
      if (!user) return;

      const response = await queryCms(apiurl, user.token, 'GET', `/cms/files/${ThemeFiletype.MD}`);
      if (checkError(response, notifications, false)) return;
      setLanguageData(response);

      if (!filepath) return;
      const fileContentResponse = await queryCms(
        apiurl,
        user.token,
        'GET',
        `/cms/files/${ThemeFiletype.MD}/${filepath}`,
      );
      if (checkError(fileContentResponse, notifications, false)) return;
      setFileData(fileContentResponse.content);
    };
    query();
  }, [filepath, user, apiurl, notifications]);

  return (
    <div class="Theme-Code-Option">
      <p className="Description">
        In diesem Teil der Seite kannst du Texte und Bilder mit Hilfe von Markdown anpassen.
        <br />
        Beispiele für Markdown können in diesem{' '}
        <Link highlight href="https://commonmark.org/help/" target="blank">
          Cheatsheet
        </Link>{' '}
        angesehen werden.
      </p>
      {languageData?.length === 0 && <p>Es wurden keine Dateien in dieser Kategorie gefunden!</p>}
      {languageData?.length > 0 && (
        <div className="Browser">
          <nav className="Browser-Files">
            {languageData?.map((file: string, i: number) => {
              return (
                <Link
                  key={`language-${i}`}
                  className="File"
                  href={`${contentBaseUrl}${btoa(file)}`}
                >
                  {file}
                </Link>
              );
            })}
          </nav>
        </div>
      )}
      {filepath && (fileData || fileData === '') && (
        <BackstageCodeEditor
          fileData={fileData}
          language="md"
          apiEndpoint={`/cms/files/md/${filepath}`}
        />
      )}
    </div>
  );
};

export default BackstageThemeContentOption;
