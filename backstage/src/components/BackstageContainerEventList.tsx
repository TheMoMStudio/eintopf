// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import { Role, RoleAdmin, RoleModerator } from '../api/auth';
import { queryEventCrud, Event } from '../api/event';
import { EventDocument, queryEventSearch } from '../api/eventsearch';
import { queryTopicList, Topic } from '../api/topic';
import { queryCategoryList, Category } from '../api/category';
import { useAppData } from '../appData';
import BackstageList from './BackstageList';
import BackstageListField from './BackstageListField';
import BackstageListFilterSelect from './BackstageListFilterSelect';
import BackstageListFilterText from './BackstageListFilterText';
import BackstageListFilterToggle from './BackstageListFilterToggle';
import useBackstageConfig from '../hooks/useBackstageConfig';
import useUser from '../hooks/useUser';
import {
  BackstageActionButtonDelete,
  BackstageActionButtonEdit,
  BackstageActionButtonView,
} from './BackstageActionButtons';

export interface BackstageContainerEventListProps {
  id?: string;
  title?: string;
  baseFilters?: Record<string, any>;
  reload?: number;
}

export const BackstageContainerEventList: FunctionComponent<BackstageContainerEventListProps> = ({
  id,
  title = 'Veranstaltungen',
  baseFilters,
  reload,
}) => {
  const { getValue, setValue } = useBackstageConfig();

  const initialOwnedByOn: boolean = getValue<boolean>(
    'eventList/ownedByOn',
    'boolean',
    true,
  ) as boolean;
  const initialPageSize: number = getValue<number>('eventList/pageSize', 'number', 25) as number;
  const initialCategory = getValue<string>('eventList/category', 'string', undefined);
  const initialTopic = getValue<string>('eventList/topic', 'string', undefined);
  const initialParent = getValue<string>('eventList/parent', 'string', undefined);
  const initialSearch = getValue<string>('eventList/search', 'string', undefined);
  const { user } = useUser();
  const { apiurl } = useAppData();
  const [ownedByOn, setOwnedByOn] = useState(initialOwnedByOn);

  const [categories, setCategories] = useState<Category[] | undefined>(undefined);
  useEffect(() => {
    if (!user) return;
    queryCategoryList(apiurl, user.token, {}).then((result) => {
      if (typeof result == 'string') {
        console.error(result);
        return;
      }
      setCategories(result.items);
    });
  }, [apiurl, user]);

  const [topics, setTopics] = useState<Topic[] | undefined>(undefined);
  useEffect(() => {
    if (!user) return;
    queryTopicList(apiurl, user.token, {}).then((result) => {
      if (typeof result == 'string') {
        console.error(result);
        return;
      }
      setTopics(result.items);
    });
  }, [apiurl, user]);

  const [eventsWithParent, setEventsWithParent] = useState<EventDocument[] | undefined>(undefined);
  useEffect(() => {
    (async () => {
      if (!user) return;
      const isAdminOrModerator = user.role === RoleAdmin || user.role === RoleModerator;
      const result = await queryEventSearch(apiurl, {
        query: '',
        sort: '',
        page: 0,
        pageSize: 0,
        filters: [
          { children: true },
          {
            ownedBys: (isAdminOrModerator && ownedByOn) || !isAdminOrModerator ? [user.id] : [],
          },
          { multidayMax: 0 },
        ],
        aggregations: {},
        omit: ['description'],
      });
      if (typeof result == 'string') {
        console.error(result);
        return;
      }
      setEventsWithParent(result.events);
    })();
  }, [user, ownedByOn, apiurl]);

  return (
    <BackstageList
      title={title}
      id={id}
      noItemsInfo="Keine Veranstaltungen vorhanden"
      queryCrud={queryEventCrud}
      bulkDeleteConfirmMessage="Willst du wirklich alle ausgewählten Veranstaltungen löschen?"
      config={{
        pageSize: initialPageSize,
        initialSorting: 'start',
        initialOrder: 'DESC',
        baseFilters: {
          parent: initialParent,
          ownedBy: initialOwnedByOn ? ['self'] : undefined,
          likeName: initialSearch,
          category: initialCategory,
          topic: initialTopic,
          ...baseFilters,
        },
      }}
      filters={
        baseFilters
          ? undefined
          : [
              <BackstageListFilterToggle
                key="ownedByFilter"
                field="ownedBy"
                needsRole={(role: Role) => role === RoleAdmin || role === RoleModerator}
                initialState={initialOwnedByOn}
                onChange={(on) => {
                  setValue('eventList/ownedByOn', on);
                  setOwnedByOn(on);
                }}
                value={(on) => (on ? ['self'] : undefined)}
                label="Nur eigene Anzeigen"
              />,
              <BackstageListFilterSelect
                key="parentFilter"
                field="parent"
                initialState={initialParent}
                options={
                  eventsWithParent
                    ? eventsWithParent.map((e) => ({ value: `id:${e.id}`, name: e.name }))
                    : []
                }
                onChange={(parent) => setValue('eventList/parent', parent)}
                label="Unterveranstaltung"
              />,
              <BackstageListFilterSelect
                key="category"
                field="category"
                initialState={initialCategory}
                options={
                  categories ? categories.map((e) => ({ value: `${e.id}`, name: e.name })) : []
                }
                onChange={(category) => setValue('eventList/category', category)}
                label="Kategorie"
              />,
              <BackstageListFilterSelect
                key="topic"
                field="topic"
                initialState={initialTopic}
                options={topics ? topics.map((e) => ({ value: `${e.id}`, name: e.name })) : []}
                onChange={(topic) => setValue('eventList/topic', topic)}
                label="Thema"
              />,
              <BackstageListFilterText
                key="searchFilter"
                field="likeName"
                initialState={initialSearch || ''}
                onChange={(v) => setValue('eventList/search', v)}
                label="Suche"
              />,
            ]
      }
      onPageSizeChange={(pageSize) => setValue('eventList/pageSize', pageSize)}
      reload={reload}
    >
      <BackstageListField<Event> field="name" name="Name" display="text" />
      <BackstageListField<Event> field="start" name="Beginn" display="datetime" />
      <BackstageListField<Event> field="published" name="Veröffentlicht" display="boolean" />
      <BackstageListField
        field="deactivated"
        name="Deaktiviert"
        display="boolean"
        needsRole={(role: Role) => role === RoleAdmin || role === RoleModerator}
      />
      <BackstageActionButtonEdit<Event> context="events" />
      <BackstageActionButtonView<Event> context="event" />
      <BackstageActionButtonDelete<Event> />
    </BackstageList>
  );
};
export default BackstageContainerEventList;
