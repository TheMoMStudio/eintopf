// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useState, useEffect, useContext } from 'preact/hooks';
import { queryUserGet, User } from '../api/user';
import { useAppData } from '../appData';
import { ListContext, ListContextType } from './BackstageList';
import BackstageListItem, { BackstageListItemProps } from './BackstageListItem';

export interface BackstageListFieldReferenceUserProps extends BackstageListItemProps {
  field: string;
  name: string;
}

export const BackstageListFieldReferenceUser: FunctionComponent<
  BackstageListFieldReferenceUserProps
> = ({ field, ...props }) => {
  const { apiurl } = useAppData();
  const listContext = useContext(ListContext);
  if (listContext === undefined) {
    console.error('useContext(ListContext) used outside a ListContext.Provider');
  }

  const { token, item } = listContext as ListContextType;

  const [user, setUser] = useState<User | undefined>(undefined);
  useEffect(() => {
    const query = async () => {
      const userId = item[field];
      if (typeof userId !== 'string') return;
      if (!userId) return;
      const response = await queryUserGet(apiurl, token, userId);
      if (typeof response === 'string') {
        return;
      }
      setUser(response);
    };
    query();
  }, [item, field, token, apiurl]);

  return (
    <BackstageListItem {...props}>
      {user ? user.nickname : (item[field] as string)}
    </BackstageListItem>
  );
};

export default BackstageListFieldReferenceUser;
