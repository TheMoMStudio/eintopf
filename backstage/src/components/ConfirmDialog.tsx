// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import Button from './Button';
import './ConfirmDialog.css';

export interface ConfirmDialogProps {
  message: string;
  onAbort: () => void;
  onConfirm: () => void;
}

export const ConfirmDialog: FunctionComponent<ConfirmDialogProps> = ({
  message,
  onAbort,
  onConfirm,
}) => (
  <div className="Confirm-Dialog">
    <div className="Confirm-Dialog-Message">{message}</div>
    <div className="Confirm-Dialog-Actions">
      <Button onClick={onAbort}>Abbrechen</Button>
      <Button onClick={onConfirm}>Ok</Button>
    </div>
  </div>
);
export default ConfirmDialog;
