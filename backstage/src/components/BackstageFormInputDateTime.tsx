// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import { useRef } from 'preact/hooks';
import { useField } from 'react-final-form';
import { format, newDate } from '../utils/date';
import { BackstageFormInputProps } from './BackstageFormInput';
import BackstageInput from './BackstageInput';
import './BackstageFormInputDateTime.css';
import Button from './Button';
import { useAppData } from '../appData';

export interface BackstageFormInputDateTimeProps extends BackstageFormInputProps<string> {
  optional?: boolean;
  disableDateInput?: boolean;
  disableTimeInput?: boolean;
}

export const BackstageFormInputDateTime: FunctionComponent<BackstageFormInputDateTimeProps> = ({
  field,
  label,
  defaultValue,
  info,
  optional = false,
  disableDateInput = false,
  disableTimeInput = false,
}) => {
  const { timezone } = useAppData();
  if (!defaultValue) {
    defaultValue = newDate(timezone).toISOString();
  }

  const dateRef = useRef<HTMLInputElement | null>(null);
  const timeRef = useRef<HTMLInputElement | null>(null);

  const { input, meta } = useField<string | undefined>(field, {
    defaultValue: optional === true ? undefined : defaultValue,
  });

  const onChange = (): void => {
    if (dateRef.current === null || timeRef.current === null || input.value === undefined) {
      return;
    }

    const date = dateRef.current.value;
    const time = timeRef.current.value;

    input.onChange(
      `${date}T${time}:00${format(timezone, newDate(timezone, input.value), 'XXXXX')}`,
    );
  };

  return (
    <BackstageInput className="InputDateTime" label={label} info={info} inputId={field}>
      {input.value === undefined || input.value === '' ? (
        <Button variant="secondary" onClick={() => input.onChange(defaultValue)}>
          {label} hinzufügen
        </Button>
      ) : (
        <Fragment>
          {meta.error && meta.touched && <span className="error">{meta.error}</span>}
          <div className="InputDateTimeInputWrapper">
            <input
              ref={dateRef}
              type="date"
              value={format(timezone, newDate(timezone, input.value), 'yyyy-MM-dd')}
              onChange={onChange}
              className={disableDateInput ? 'hidden' : undefined}
              id={field}
            />
            <input
              ref={timeRef}
              type="time"
              value={format(timezone, newDate(timezone, input.value), 'HH:mm')}
              onChange={onChange}
              className={disableTimeInput ? 'hidden' : undefined}
              id={field}
            />
          </div>
          {optional && (
            <Button variant="secondary" onClick={() => input.onChange(undefined)}>
              {label} entfernen
            </Button>
          )}
        </Fragment>
      )}
    </BackstageInput>
  );
};

export default BackstageFormInputDateTime;
