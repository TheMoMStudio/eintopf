// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { VNode } from 'preact';
import { NeedsRole } from '../utils/role';

export interface BackstageFormInputProps<T> {
  field: string;
  label?: string;
  autoFocus?: boolean;
  defaultValue?: T;
  needsRole?: NeedsRole;
  hideWhen?: { field: string; is: (value: any) => boolean };
  required?: boolean;
  disabled?: boolean;
  info?: string | VNode;
}
