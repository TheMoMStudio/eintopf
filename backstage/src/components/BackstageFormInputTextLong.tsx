// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useField } from 'react-final-form';
import { BackstageFormInputProps } from './BackstageFormInput';
import BackstageInput from './BackstageInput';
import './BackstageFormInputTextLong.css';

export interface BackstageFormInputTextLongProps extends BackstageFormInputProps<string> {
  placeholder?: string;
}

export const BackstageFormInputTextLong: FunctionComponent<BackstageFormInputTextLongProps> = ({
  field,
  label,
  ...rest
}) => {
  const { input, meta } = useField(field);
  return (
    <BackstageInput className="InputTextLong" label={label} inputId={field}>
      {meta.error && meta.touched && <span className="error">{meta.error}</span>}
      <textarea
        {...rest}
        {...(input as Record<string, unknown>)}
        className={meta.error && meta.touched ? 'error' : ''}
      />
    </BackstageInput>
  );
};

export default BackstageFormInputTextLong;
