// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useEffect, useRef, useState } from 'preact/hooks';
import './BackstageInputAutocomplete.css';
import { Fragment } from 'preact/compat';

interface BackstageInputAutocompleteProps {
  className?: string;
  value: string;
  onChange: (value: string) => void;
  completionProvider: (text: string) => Promise<string[]>;
  displayValue?: (value: string) => string;
  inputId: string;
}

export const BackstageInputAutocomplete: FunctionComponent<BackstageInputAutocompleteProps> = ({
  className,
  value,
  onChange,
  completionProvider,
  displayValue = (v: string) => v,
  inputId,
}) => {
  const [valueInternal, setValueInternal] = useState(value);
  useEffect(() => {
    setValueInternal(value);
  }, [value]);
  const [focused, setFocused] = useState(false);
  const [completions, setCompletions] = useState<string[]>([]);
  useEffect(() => {
    completionProvider(valueInternal)
      .then((comps) => setCompletions(comps))
      .catch(() => {});
  }, [completionProvider, valueInternal, setCompletions]);
  const handleOnChange = (v: string): void => {
    setValueInternal(v);

    completionProvider(v)
      .then((comps) => {
        setCompletions(comps);
      })
      .catch(() => {});
  };

  const wrapperRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    // When clicking outside the element call onChange with the current value.
    const handleWindowClick = (e: MouseEvent) => {
      if (!focused) {
        return;
      }
      if (
        wrapperRef.current !== null &&
        e.target !== null &&
        wrapperRef.current.contains(e.target as Node)
      ) {
        return;
      }
      onChange(valueInternal);
      setFocused(false);
    };
    window.addEventListener('click', handleWindowClick);
    return () => removeEventListener('click', handleWindowClick);
  }, [wrapperRef, focused, valueInternal, onChange]);

  const getStringMatchIndex = (orig: string, pattern: string): number => {
    return orig.toLowerCase().indexOf(pattern.toLowerCase());
  };

  const getHighlightedSuggestion = (suggestion: string, searchPattern: string): h.JSX.Element => {
    const matchIndexStart = getStringMatchIndex(suggestion, searchPattern);
    return (
      <Fragment>
        {suggestion.slice(0, matchIndexStart)}
        <span className="CompletionValue">
          {suggestion.slice(matchIndexStart, matchIndexStart + searchPattern.length)}
        </span>
        {suggestion.slice(matchIndexStart + searchPattern.length, suggestion.length)}
      </Fragment>
    );
  };

  return (
    <div className="BackstageInputAutocomplete" ref={wrapperRef}>
      <input
        type="text"
        id={inputId}
        data-testid="auto-input"
        className={className}
        value={displayValue(valueInternal)}
        onChange={(e) => handleOnChange((e.target as HTMLInputElement).value)}
        onFocus={() => setFocused(true)}
        onClick={(e) => e.stopPropagation()}
        onKeyDown={(e) => {
          if (e.keyCode === 13) {
            // ENTER
            onChange(valueInternal);
          }
        }}
      />
      {focused && completions.length > 0 && (
        <div className="Completions" data-testid="auto-completions">
          {completions.map((completion) => (
            <div
              key={completion}
              className="CompletionItem"
              // use mouse down event instead of click, since it gets called
              // before the window click handler above
              onMouseDown={() => {
                setValueInternal(completion);
                onChange(completion);
                setFocused(false);
              }}
            >
              {getHighlightedSuggestion(completion, valueInternal)}
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default BackstageInputAutocomplete;
