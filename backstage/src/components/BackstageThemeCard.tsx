// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import './BackstageThemeCard.css';
import { Link } from 'preact-router';
import { useRef, useState } from 'preact/hooks';
import Button from './Button';
import { Theme, themeBaseUrl } from '../api/cms';

export interface BackstageThemeCardProps {
  theme: Theme;
  activateTheme: (name: string, dialog: HTMLDialogElement | null) => void;
  deleteTheme: (name: string, dialog: HTMLDialogElement | null) => void;
}

export const BackstageThemeCard: FunctionComponent<BackstageThemeCardProps> = ({
  theme,
  activateTheme,
  deleteTheme,
}) => {
  const dialogRef = useRef<HTMLDialogElement>(null);
  const [dialogContent, setDialogContent] = useState({
    title: '',
    description: '',
    handler: () => {},
  });

  const openDialog = (title: string, description: string, isDelete: boolean) => {
    if (!dialogRef.current) return;
    dialogRef.current.showModal();
    setDialogContent({
      title,
      description,
      handler: isDelete
        ? () => deleteTheme(theme.name, dialogRef.current)
        : () => activateTheme(theme.name, dialogRef.current),
    });
  };

  return (
    <div
      className={`ThemeCard ${theme.active ? 'Active' : ''} ${theme.editable ? '' : 'Disabled'}`}
    >
      <div className={`Container-Title`}>
        <div>{`${theme.name}${theme.active ? '(aktiv)' : ''}`}</div>
        <div className="ThemCardButtonContainer">
          {theme.active && theme.editable ? (
            // @ts-ignore: This has worked and should absolutely work
            <Link href={themeBaseUrl} className="ThemeCardButton Edit" title="Bearbeiten">
              <img src="/assets/images/icon_edit.svg" alt="Bearbeiten" />
            </Link>
          ) : (
            <Fragment>
              {!theme.active && (
                <button
                  onClick={() =>
                    openDialog(
                      `Möchtest du das Theme ${theme.name} aktivieren?`,
                      'Das Aussehen des Kalenders wird dadurch geändert. Die Änderungen am aktuellen Theme bleiben bestehen.',
                      false,
                    )
                  }
                  className="ThemeCardButton Activate"
                  title="Aktivieren"
                >
                  <img src="/assets/images/placeholder.png" alt="Aktivieren" />
                </button>
              )}
              {theme.editable && (
                <button
                  onClick={() =>
                    openDialog(
                      `Möchtest du das Theme ${theme.name} löschen?`,
                      'Dieser Vorgang kann nicht rückgängig gemacht werden!',
                      true,
                    )
                  }
                  className="ThemeCardButton Delete"
                  title="Löschen"
                >
                  <img src="/assets/images/icon_delete.svg" alt="Löschen" />
                </button>
              )}
              <dialog className="ThemeCardDialog" ref={dialogRef}>
                <div class="ThemeCardDialogContainer">
                  <div className={`Container-Title`}>
                    <div>{dialogContent.title}</div>
                  </div>
                  <div className="ThemeCardDialogContent">
                    <p>{dialogContent.description}</p>
                    <div className="ThemeCardDialogInteraction">
                      <Button onClick={dialogContent.handler}>Ja</Button>
                      <Button onClick={() => dialogRef.current?.close()}>Nein</Button>
                    </div>
                  </div>
                </div>
              </dialog>
            </Fragment>
          )}
        </div>
      </div>
    </div>
  );
};

export default BackstageThemeCard;
