// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import { add } from 'date-fns/add';
import { isBefore } from 'date-fns/isBefore';
import { NewEvent } from '../api/event';
import BackstageFormInputArrayTag from '../components/BackstageFormInputArrayTag';
import BackstageFormInputArrayUser from '../components/BackstageFormInputArrayUser';
import BackstageFormInputArrayGroup from '../components/BackstageFormInputArrayGroup';
import BackstageFormInputCheckbox from '../components/BackstageFormInputCheckbox';
import BackstageFormInputDateTime from '../components/BackstageFormInputDateTime';
import BackstageFormInputImage from '../components/BackstageFormInputImage';
import BackstageFormInputReferenceEvent from '../components/BackstageFormInputReferenceEvent';
import BackstageFormInputReferencePlace from '../components/BackstageFormInputReferencePlace';
import BackstageFormInputText from '../components/BackstageFormInputText';
import BackstageFormInputTextLong from '../components/BackstageFormInputTextLong';
import { newDate } from '../utils/date';
import BackstageFormInputArrayInvolved from './BackstageFormInputArrayInvolved';
import { useField } from 'react-final-form';
import { useAppData } from '../appData';
import BackstageFormButtonLatLng from './BackstageFormButtonLatLng';
import BackstageFormInputNumber from './BackstageFormInputNumber';
import BackstageFormInputReferenceCategory from './BackstageFormInputReferenceCategory';
import BackstageFormInputReferenceTopic from './BackstageFormInputReferenceTopic';

export const validateEvent = <E extends NewEvent>(
  timezone: string,
  event: E,
): string | undefined => {
  if (event.end != null && isBefore(newDate(timezone, event.end), newDate(timezone, event.start))) {
    return 'Der Start der Veranstaltung muss vor dem Ende der Veranstaltung sein.';
  }
  return undefined;
};

export const preSubmitEvent = <E extends NewEvent>(model: E): E => {
  const event = model;
  if (model.tags) {
    // Remove any empty tags
    event.tags = model.tags.filter((tag) => tag !== '');
  }
  return event;
};

export interface BackstageEventFormFieldsProps {
  update?: boolean;
}

export const BackstageEventFormFields: FunctionComponent<BackstageEventFormFieldsProps> = ({
  update = false,
}) => {
  const { timezone } = useAppData();
  const [endDateDefaultValue, setEndDateDefaultValue] = useState(
    add(newDate(timezone), { hours: 1 }).toISOString(),
  );
  const { input: startInput } = useField('start');
  useEffect(() => {
    if (startInput.value != '') {
      setEndDateDefaultValue(startInput.value);
    }
  }, [startInput.value]);

  return (
    <Fragment>
      <BackstageFormInputCheckbox
        field="published"
        label="Veröffentlicht"
        defaultValue={true}
        info="Die Veranstaltung wird erst im Kalender angezeigt, wenn sie veröffentlicht wurde. Nicht veröffentlichte Veranstaltungen können per Url aufgerufen werden. Die Veröffentlichung kann jederzeit rückgängig gemacht werden."
      />
      {update && (
        <BackstageFormInputCheckbox
          field="canceled"
          label="Fällt aus"
          info="Wenn die Veranstaltung nicht stattfindet wird dies entsprechend im Kalender angezeigt."
        />
      )}
      <BackstageFormInputText field="name" label="Titel der Veranstaltung" required />
      <BackstageFormInputReferenceEvent
        field="parent"
        label="Teil einer Hauptveranstaltung"
        mustOwn={true}
        info="Hier kann die Veranstaltung mit einer Hauptveranstaltung verknüpft werden. Diese muss vorher angelegt worden sein. Dadurch wird die Veranstaltung als Programmpunkt von der Hauptveranstaltung angezeigt."
      />
      <BackstageFormInputCheckbox
        field="parentListed"
        label="Eigenständig im Kalender anzeigen"
        info="Teilveranstaltungen erscheinen normalerweise nicht extra im Kalender, sondern nur als Programmpunkte der verknüpften Hauptveranstaltung. Wenn die Veranstaltung auch unabhängig von der Hauptveranstaltung angezeigt werden soll, muss dies ausgewält werden."
        hideWhen={{
          field: 'parent',
          is: (value) => !value || (value as string).includes('revent'),
        }}
      />

      <BackstageFormInputArrayGroup
        field="organizers"
        label="Veranstalter*in"
        info="Hier kannst du eine Gruppe zuordnen, bei der du als Benutzer*in eingetragen bist. Es können alle Benutzer*innen die Veranstaltung bearbeiten, die der Gruppe zugeordnet sind. Alternativ dazu kannst du manuell eine Veranstalter*in eintragen."
        hideWhen={{ field: 'parent', is: (value) => (value as string).includes('revent') }}
      />
      <BackstageFormInputArrayInvolved
        field="involved"
        label="Beteiligte (Band, Refernt*in, ...)"
      />
      <BackstageFormInputReferencePlace
        field="location"
        label="Ort der Veranstaltung"
        info="Hier kannst du einen Ort zuordnen, der schon im EINTOPF angelegt wurde. Alternativ dazu kannst du einen eigenen Ort eintragen."
        required
      />
      <BackstageFormInputText
        field="location2"
        label="Ort Zusatz"
        info="Zum Beispiel Raum, Zelt, Hinterhof, ..."
      />
      <BackstageFormInputText
        field="address"
        label="Adresse"
        info="z.B. Hauptstr. 1, Stuttgart"
        hideWhen={{
          field: 'location',
          is: (value) => (value as string).includes('id:') || (value as string) == '',
        }}
      />
      <BackstageFormButtonLatLng
        addressField="address"
        latField="lat"
        lngField="lng"
        info="Damit der Ort auf der Karte angezeigt werden kann müssen die Koordinaten eingetragen werden. Über den Button können diese aus der Adresse generiert werden."
        hideWhen={{
          field: 'location',
          is: (value) => (value as string).includes('id:') || (value as string) == '',
        }}
      />
      <BackstageFormInputNumber
        field="lat"
        label="Breitengrad"
        hideWhen={{
          field: 'location',
          is: (value) => (value as string).includes('id:') || (value as string) == '',
        }}
      />
      <BackstageFormInputNumber
        field="lng"
        label="Längengrad"
        hideWhen={{
          field: 'location',
          is: (value) => (value as string).includes('id:') || (value as string) == '',
        }}
      />
      <BackstageFormInputDateTime
        field="start"
        label="Start"
        defaultValue={newDate(timezone).toISOString()}
      />
      <BackstageFormInputDateTime
        field="end"
        label="Ende"
        optional
        defaultValue={endDateDefaultValue}
      />
      <BackstageFormInputTextLong field="description" label="Beschreibung" />
      <BackstageFormInputImage
        field="image"
        label="Bild"
        previews={[{ name: 'Vorschau', width: 380 }]}
        info="Das Bild wird auf der Veranstaltungsseite in einer Breite von 380px angezeigt. Um Daten zu sparen, sollte das Bild nicht zu groß sein."
      />
      <BackstageFormInputText field="alt" label="Bildbeschreibung" />
      <BackstageFormInputReferenceCategory field="category" label="Kategorie" required />
      <BackstageFormInputReferenceTopic field="topic" label="Thema" required />
      <BackstageFormInputArrayTag
        field="tags"
        label="Tags"
        info="Tags werden im Kalender als Filter angezeigt. Achte deshalb bitte darauf, dass du möglichst bestehende Tags verwendest, damit die Liste nicht zu lang und unübersichtlich wird."
      />
      <BackstageFormInputArrayUser
        field="ownedBy"
        label="Benutzer*innen"
        info="Es können beliebig viele Benutzer*innen hinzugefügt werden, die die Veranstaltung bearbeiten können."
      />
    </Fragment>
  );
};
