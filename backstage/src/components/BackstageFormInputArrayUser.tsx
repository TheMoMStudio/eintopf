// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useCallback, useEffect, useState } from 'preact/hooks';
import { useField } from 'react-final-form';
import { queryUserList, User } from '../api/user';
import useUser from '../hooks/useUser';
import BackstageInput from './BackstageInput';
import BackstageInputAutocomplete from './BackstageInputAutocomplete';
import Button from './Button';
import './BackstageFormInputArray.css';
import { useAppData } from '../appData';

interface BackstageFormInputArrayUserProps {
  field: string;
  label?: string;
  info?: string;
}

export const BackstageFormInputArrayUser: FunctionComponent<BackstageFormInputArrayUserProps> = ({
  field,
  label,
  info,
}) => {
  const { user } = useUser();
  const { apiurl } = useAppData();

  const { input, meta } = useField<string[]>(field);
  useEffect(() => {
    if (!user) return;

    if (!input.value) {
      input.onChange([user.id]);
    }
  }, [user, input]);

  const addValue = (value: string): void => {
    const values: string[] = input.value ? [...input.value] : [];
    values.push(value);
    input.onChange(values);
  };
  const removeValue = (i: number): void => {
    if (input.value === undefined) return;
    const values = [...input.value];
    values.splice(i, 1);
    input.onChange(values);
  };
  const changeValue = (i: number, value: string): void => {
    if (input.value === undefined) return;
    const values = [...input.value];
    values[i] = value;
    input.onChange(values);
  };

  const [users, setUsers] = useState<User[]>([]);
  useEffect(() => {
    if (!user) return;

    const query = async () => {
      const response = await queryUserList(apiurl, user.token, {});
      if (typeof response === 'string') {
        return;
      }
      setUsers(response.items);
    };
    query();
  }, [user, apiurl]);

  const idFromNickname = useCallback(
    (nickname: string): string => {
      let id = '';
      users.forEach((u) => {
        if (u.nickname === nickname) {
          id = u.id;
        }
      });
      return id;
    },
    [users],
  );
  const nicknameFromID = useCallback(
    (id: string): string => {
      let nickname = '';
      users.forEach((u) => {
        if (u.id === id) {
          nickname = u.nickname;
        }
      });
      return nickname;
    },
    [users],
  );

  const userCompletionProvider = useCallback(
    (text: string): Promise<string[]> => {
      if (!users) {
        return Promise.resolve([]);
      }
      const completions = users
        .map((u) => u.nickname)
        .filter((nickname) => nickname.toLowerCase().startsWith(text.toLowerCase()))
        .filter((nickname) => nickname !== '');
      return Promise.resolve(completions);
    },
    [users],
  );

  const displayValue = (v: string): string => {
    return users && nicknameFromID(v) ? nicknameFromID(v) : v;
  };

  return (
    <BackstageInput className="BackstageInputArray" label={label} info={info} inputId={field}>
      {meta.error && meta.touched && <span className="error">{meta.error}</span>}
      <div className="ArrayItems ArrayItems-FullWidth">
        {input.value &&
          input.value.map((value, i) => (
            <div key={value} className="ArrayItem">
              <BackstageInputAutocomplete
                value={displayValue(value)}
                onChange={(v) => changeValue(i, users && idFromNickname(v) ? idFromNickname(v) : v)}
                completionProvider={userCompletionProvider}
                displayValue={displayValue}
                inputId={field}
              />
              <Button
                className="ButtonRemoveItem"
                onClick={() => removeValue(i)}
                variant="secondary"
              >
                Entfernen
              </Button>
            </div>
          ))}
        <Button className="ButtonAddItem" onClick={() => addValue('')} variant="secondary">
          Benutzer*in hinzufügen
        </Button>
      </div>
    </BackstageInput>
  );
};

export default BackstageFormInputArrayUser;
