// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, ComponentChildren, FunctionComponent } from 'preact';
import './Container.css';

export interface ContainerProps {
  id?: string;
  children?: ComponentChildren;
  title?: string | ComponentChildren;
  titleCenter?: string | ComponentChildren;
  titleRight?: string | ComponentChildren;
  className?: string;
  showTitle?: boolean;
  capsTitle?: boolean;
  hideTitleOnMobile?: boolean;
}

export const Container: FunctionComponent<ContainerProps> = ({
  id,
  children,
  title,
  titleCenter,
  titleRight,
  className = '',
  showTitle = true,
  capsTitle = true,
  hideTitleOnMobile = false,
}) => (
  <section className={`Container ${className}`} id={id}>
    {showTitle && (
      <div
        className={`Container-Title ${capsTitle ? 'Container-Title-Caps' : ''} ${
          hideTitleOnMobile ? 'Container-Title-Hide-Mobile' : ''
        }`}
      >
        <div>{title}</div>
        <div>{titleCenter}</div>
        <div>{titleRight}</div>
      </div>
    )}
    {children !== undefined && <div className="Container-Content">{children}</div>}
  </section>
);

export default Container;
