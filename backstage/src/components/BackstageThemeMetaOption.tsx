// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import { User } from '../store/store';
import BackstageFileUpload from './BackstageFileUpload';
import BackstageCodeEditor from './BackstageCodeEditor';
import { useAppData } from '../appData';
import { useEffect, useState } from 'preact/hooks';
import { checkError } from '../signals/notification';
import { queryCms, ThemeFiletype } from '../api/cms';

export interface BackstageThemeMetaOptionProps {
  user: User | undefined;
}

export const BackstageThemeMetaOption: FunctionComponent<BackstageThemeMetaOptionProps> = ({
  user,
}) => {
  const { apiurl, notifications } = useAppData();
  const [fileData, setFileData] = useState<any>();

  useEffect(() => {
    const query = async () => {
      if (!user) return;
      const response = await queryCms(
        apiurl,
        user.token,
        'GET',
        `/cms/files/${ThemeFiletype.META}/${btoa('manifest.json')}`,
      );
      if (checkError(response, notifications, false, 'Die Dateien konnten nicht geöffnet werden!'))
        return;
      setFileData(response.content);
    };
    query();
  }, [user, apiurl, notifications]);

  return (
    <div class="Theme-Meta-Option">
      <p className="Description">
        In diesem Teil der Seite können Suchmaschinen relavante Infos des Themes angepasst werden
        anpassen.
      </p>
      {fileData && (
        <Fragment>
          <h3>Favicon</h3>
          <BackstageFileUpload
            isTextFile={false}
            src="/assets/favicon.ico"
            user={user}
            name="favicon.ico"
            hideName={true}
            fileType="meta"
          />
          <h3>Manifest</h3>
          <BackstageCodeEditor
            fileData={fileData}
            apiEndpoint={`/cms/files/meta/${btoa('manifest.json')}`}
          />
        </Fragment>
      )}
    </div>
  );
};

export default BackstageThemeMetaOption;
