// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import { useAppData } from '../appData';
import { newDate } from '../utils/date';
import { NeedsRole } from '../utils/role';
import Checkbox from './Checkbox';

export interface BackstageListFilterToggleProps {
  field: string;
  needsRole?: NeedsRole;
  initialState: boolean;
  onChange?: (on: boolean) => void;
  value: (on: boolean) => string | string[] | boolean | undefined;
  label: string;
}

export const isBackstageListFilterToggle = (props: any): props is BackstageListFilterToggleProps =>
  'field' in props && 'initialState' in props && 'value' in props && 'label' in props;

export const BackstageListFilterToggle = (
  props: BackstageListFilterToggleProps, // eslint-disable-line @typescript-eslint/no-unused-vars
) => <Fragment />;

export default BackstageListFilterToggle;

export interface RenderBackstageListFilterToggleProps extends BackstageListFilterToggleProps {
  filter: (field: string, value: string | string[] | boolean | undefined) => void;
}

export const RenderBackstageListFilterToggle: FunctionComponent<
  RenderBackstageListFilterToggleProps
> = ({ field, initialState, value, onChange, label, filter }) => {
  const { timezone } = useAppData();
  const [on, setOn] = useState(initialState);
  useEffect(() => {
    if (onChange) {
      onChange(on);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [on]);

  return (
    <Checkbox
      id={newDate(timezone).getTime().toString()}
      label={label}
      checked={on}
      onClick={() => {
        setOn(!on);
        filter(field, value(!on));
      }}
    />
  );
};
