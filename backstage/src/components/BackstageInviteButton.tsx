// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useState, useEffect } from 'preact/hooks';
import { InternalServerError, UnkownError } from '../api/error';
import { NoInvitationYetError, InvitationLimitError, InviteError } from '../api/invitation';
import useInvite from '../hooks/useInvite';
import BackstageError from './BackstageError';
import Button from './Button';
import './BackstageInviteButton.css';

const convertErr = (err: InviteError): string => {
  switch (err) {
    case NoInvitationYetError:
      return 'Einladungen sind erst möglich, nachdem du eine Woche angemeldet bist';
    case InvitationLimitError:
      return 'Du darfst nur einen Einladungslink pro Woche erstellen';
    case InternalServerError:
      return 'Ups, auf der anderen Seite ist etwas schief gegangen';
    case UnkownError:
      return 'Ups, es ist ein unerwarteter Fehler Aufgetreten';
    default:
      return 'Irgendwas ist schief gelaufen, aber nur was?';
  }
};

export const createInvitationLink = (token: string): string =>
  `https://${window.location.host}/backstage/invitation/${token}`;

const copyToClipboard = (text: string): void => {
  navigator.clipboard.writeText(text);
};

export const BackstageInviteButton: FunctionComponent = () => {
  const [state, query] = useInvite();

  const [invitationLink, setInvitationLink] = useState('');
  useEffect(() => {
    if (state.data === undefined || state.data.token === '') return;
    setInvitationLink(createInvitationLink(state.data.token));
  }, [state]);

  return (
    <div>
      <Button onClick={query} disabled={state.loading}>
        Neuen Einladungslink erstellen
      </Button>
      {state.error && <BackstageError err={convertErr(state.error)} />}
      {state.data && (
        <div>
          <span className="InviteLink">{invitationLink}</span>{' '}
          <Button onClick={() => copyToClipboard(invitationLink)}>Kopieren</Button>
        </div>
      )}
    </div>
  );
};

export default BackstageInviteButton;
