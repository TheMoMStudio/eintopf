// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, Fragment } from 'preact';
import { NeedsRole } from '../utils/role';

export interface BackstageListFieldProps<T extends object> {
  field: keyof T;
  name: string;
  display: 'text' | 'boolean' | 'datetime' | 'array' | 'custom' | 'number';
  sortable?: boolean;
  needsRole?: NeedsRole;
  render?: (value: string, values: T, refresh: () => void) => string | JSX.Element;
}

export const isBackstageListFieldName = <T extends object>(
  props: any,
): props is BackstageListFieldProps<T> => 'field' in props && 'name' in props;

export const isBackstageListField = <T extends object>(
  props: any,
): props is BackstageListFieldProps<T> => 'field' in props && 'name' in props && 'display' in props;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const BackstageListField = <T extends object>(props: BackstageListFieldProps<T>) => (
  <Fragment />
);

export default BackstageListField;
