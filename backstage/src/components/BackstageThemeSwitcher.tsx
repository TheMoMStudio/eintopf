// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import './BackstageThemeSwitcher.css';
import { queryCms, Theme } from '../api/cms';
import { User } from '../store/store';
import { useAppData } from '../appData';
import BackstageThemeCard from './BackstageThemeCard';
import { useEffect, useRef, useState } from 'preact/hooks';
import { checkError, NotificationState } from '../signals/notification';
import Button from './Button';
import BackstageFormInputText from './BackstageFormInputText';
import BackstageForm from './BackstageForm';
import BackstageNotifications from './BackstageNotifications';

export interface BackstageThemeSwitcherProps {
  user: User;
}

export const BackstageThemeSwitcher: FunctionComponent<BackstageThemeSwitcherProps> = ({
  user,
}) => {
  const { apiurl, notifications } = useAppData();
  const dialogRef = useRef<HTMLDialogElement>(null);
  const [themes, setThemes] = useState<Theme[]>([]);

  const deleteTheme = async (name: string, dialog: HTMLDialogElement | null) => {
    if (!dialog) return;
    const response = await queryCms(apiurl, user.token, 'DELETE', `/cms/themes/${name}`);
    if (checkError(response, notifications, false)) return;
    let themeTitle;
    setThemes(
      themes.filter((theme) => {
        if (theme.name === name) {
          themeTitle = theme.name;
        }
        return theme.name !== name;
      }),
    );
    dialog.close();
    notifications.push(
      NotificationState.SUCCESS,
      `Das Theme ${themeTitle} wurde erfolgreich gelöscht!`,
      true,
    );
  };

  const activateTheme = async (name: string, dialog: HTMLDialogElement | null) => {
    if (!dialog) return;
    const response = await queryCms(apiurl, user.token, 'PATCH', `/cms/themes`, { name });
    if (checkError(response, notifications, false)) return;
    let themeTitle;
    setThemes(
      themes.map((theme) => {
        theme.active = theme.name === name;
        if (theme.name === name) {
          themeTitle = theme.name;
        }
        return theme;
      }),
    );
    dialog.close();
    notifications.push(
      NotificationState.SUCCESS,
      `Das Theme ${themeTitle} wurde erfolgreich aktiviert!`,
      true,
    );
    document.location.reload();
  };

  const onSubmit = async (values: unknown): Promise<string | undefined> => {
    if (!user || !dialogRef.current) return undefined;
    const themeAddData = values as { name: string };
    const theme: Theme = { name: themeAddData.name, active: false, editable: true };
    const response = await queryCms(apiurl, user.token, 'POST', `/cms/themes`, theme);
    if (checkError(response, notifications, false)) return;
    dialogRef.current.close();
    setThemes([...themes, theme]);
    notifications.push(
      NotificationState.SUCCESS,
      `Das Theme ${theme.name} wurde erfolgreich erstellt!`,
      true,
    );
    return undefined;
  };

  useEffect(() => {
    const query = async () => {
      if (!user) return;
      const response = await queryCms(apiurl, user.token, 'GET', `/cms/themes`);
      if (checkError(response, notifications, false)) return;
      setThemes(response as Theme[]);
    };
    query();
  }, [user, apiurl, notifications]);

  return (
    <div className="ThemeSwitcher">
      <div className="ThemeSwitcherList">
        {themes.map((theme) => (
          <BackstageThemeCard
            key={theme.name}
            theme={theme}
            activateTheme={activateTheme}
            deleteTheme={deleteTheme}
          />
        ))}
        <button onClick={() => dialogRef.current?.showModal()} className="ThemeCard Add">
          +
        </button>
        <dialog className="ThemeCardDialog" ref={dialogRef}>
          <div class="ThemeCardDialogContainer">
            <div className={`Container-Title`}>
              <div>Ein neues Theme erstellen</div>
            </div>
            <BackstageForm className="ThemeCardDialogContent" onSubmit={onSubmit} requiredNotice>
              <BackstageFormInputText field="name" label="Titel des Themes" required />
              <div className="ThemeCardDialogInteraction">
                <Button type="submit">Erstellen</Button>
                <Button onClick={() => dialogRef.current?.close()}>Abbrechen</Button>
              </div>
              <BackstageNotifications />
            </BackstageForm>
          </div>
        </dialog>
      </div>
    </div>
  );
};

export default BackstageThemeSwitcher;
