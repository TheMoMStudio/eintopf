// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useAppData } from '../appData';
import './BackstageNotifications.css';
import { NotificationState } from '../signals/notification';

const BackstageNotifications: FunctionComponent = () => {
  const { notifications } = useAppData();

  const closeNotification = () => {
    notifications.state.value = {
      state: NotificationState.NONE,
      message: notifications.state.value.message,
      hasClose: true,
    };
  };

  return (
    <div className={`Notification ${notifications.state.value.state}`}>
      <span>{notifications.state.value.message}</span>
      {notifications.state.value.hasClose && (
        <button className="Notification-Close" onClick={() => closeNotification()}>
          <span className="Notification-Icon">+</span>
        </button>
      )}
    </div>
  );
};

export default BackstageNotifications;
