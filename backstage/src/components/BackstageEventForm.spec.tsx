// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h } from 'preact';
import { render, act, fireEvent } from '@testing-library/preact';
import { BackstageEventFormFields } from './BackstageEventForm';
import BackstageForm from './BackstageForm';
import userEvent from '@testing-library/user-event';
import { afterAll, afterEach, beforeAll } from 'vitest';
import { setupServer } from 'msw/node';
import { HttpResponse, graphql, http } from 'msw';
import { handlers } from '../api-mock/handlers';

const posts = [
  {
    lat: '12.34',
    lon: '34.56',
  },
];
const graphqlHandlers = [
  graphql.query('ListPosts', () => {
    return HttpResponse.json({
      data: { posts },
    });
  }),
];
const restHandlers = [
  http.get('https://nominatim.openstreetmap.org/search', () => {
    console.log('handler');
    return HttpResponse.json(posts);
  }),
];

const server = setupServer(...restHandlers, ...graphqlHandlers, ...handlers);

describe('BackstageEventFormFields', () => {
  beforeEach(() => {
    vi.useFakeTimers();
  });
  afterEach(() => {
    vi.useRealTimers();
    vi.restoreAllMocks();
    server.resetHandlers();
  });
  // Start server before all tests
  beforeAll(() => server.listen());
  //  Close server after all tests
  afterAll(() => server.close());

  it('address fields visibility', async () => {
    const { getByLabelText, getByRole, queryByLabelText } = render(
      <BackstageForm onSubmit={vi.fn() as any} requiredNotice>
        <BackstageEventFormFields />
      </BackstageForm>,
    );

    // expect address field etc not to be visible
    expect(queryByLabelText('Adresse')).not.toBeInTheDocument();
    //expect(queryByTestId('coordinates-button')).not.toBeInTheDocument();
    expect(queryByLabelText('Breitengrad')).not.toBeInTheDocument();
    expect(queryByLabelText('Längengrad')).not.toBeInTheDocument();

    const input = getByLabelText('Ort der Veranstaltung');
    await act(() => input.focus());

    userEvent.type(input, 'testort');
    await act(() => vi.runAllTimersAsync() as any);

    fireEvent.keyDown(input, { keyCode: 13 });
    await act(() => vi.runAllTimersAsync() as any);

    // expect address field to be visible after using address
    expect(queryByLabelText('Adresse')).toBeInTheDocument();

    const addressInput = getByLabelText('Adresse');
    await act(() => addressInput.focus());
    userEvent.type(addressInput, 'Hauptstr. 1, Stuttgart');

    await act(() => vi.runAllTimersAsync() as any);

    expect(addressInput).toHaveValue('Hauptstr. 1, Stuttgart');

    expect(queryByLabelText('Breitengrad')).toBeInTheDocument();
    expect(queryByLabelText('Längengrad')).toBeInTheDocument();

    const button = getByRole('button', { name: /Koordinaten über openstreetmaps.org generieren/i });
    userEvent.click(button);

    await act(() => vi.runAllTimersAsync() as any);

    expect(queryByLabelText('Breitengrad')).toHaveValue(12.34);
    expect(queryByLabelText('Längengrad')).toHaveValue(34.56);
  });
});
