// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useField } from 'react-final-form';
import { BackstageFormInputProps } from './BackstageFormInput';
import validateLink from '../utils/validateLink';
import BackstageInput from './BackstageInput';

export interface BackstageFormInputLinkProps extends BackstageFormInputProps<string> {
  placeholder?: string;
}

export const BackstageFormInputLink: FunctionComponent<BackstageFormInputLinkProps> = ({
  field,
  label,
  ...rest
}) => {
  const { input, meta } = useField(field, { validate: validateLink });

  return (
    <BackstageInput
      className="InputText"
      label={label}
      error={meta.error}
      touched={meta.touched}
      inputId={field}
    >
      <input
        type="text"
        id={field}
        {...rest}
        {...(input as Record<string, unknown>)}
        className={meta.error && meta.touched ? 'error' : ''}
      />
    </BackstageInput>
  );
};

export default BackstageFormInputLink;
