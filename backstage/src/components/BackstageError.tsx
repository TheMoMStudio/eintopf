// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import './BackstageError.css';

export interface BackstageErrorProps {
  err: string;
}

export const BackstageError: FunctionComponent<BackstageErrorProps> = ({ err }) => (
  <div className="BackstageError">{err}</div>
);

export default BackstageError;
