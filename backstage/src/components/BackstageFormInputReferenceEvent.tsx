// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { queryEventList, Event } from '../api/event';
import BackstageFormInputReference from './BackstageFormInputReference';

export interface BackstageFormInputReferenceEventProps {
  field: string;
  label?: string;
  info?: string;
  mustOwn?: boolean;
}

export const BackstageFormInputReferenceEvent: FunctionComponent<
  BackstageFormInputReferenceEventProps
> = ({ field, label, info, mustOwn = true }) => (
  <BackstageFormInputReference<Event>
    field={field}
    label={label}
    mustOwn={mustOwn}
    queryList={queryEventList}
    displayField="name"
    info={info}
  />
);

export default BackstageFormInputReferenceEvent;
