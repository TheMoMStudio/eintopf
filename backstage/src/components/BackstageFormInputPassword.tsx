// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import { useField } from 'react-final-form';
import validateRequired from '../utils/validateRequired';
import BackstageInput from './BackstageInput';

export interface BackstageFormInputTextProps {
  field: string;
  required?: boolean;
  info?: string;
}

export const BackstageFormInputText: FunctionComponent<BackstageFormInputTextProps> = ({
  field,
  required,
  info,
}) => {
  const { input: passwordInput, meta } = useField(field, {
    validate: required ? validateRequired : undefined,
  });

  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');

  const [password2Error, setPassword2Error] = useState<string | undefined>(undefined);
  useEffect(() => {
    if (password1 === password2) {
      passwordInput.onChange(password1);
      setPassword2Error(undefined);
    }
  }, [passwordInput, password1, password2]);

  const onChangePassword2 = (value: string) => {
    setPassword2(value);
    if (password1 !== value) {
      setPassword2Error('Die Passwörter stimmen nicht überein');
    }
  };

  return (
    <Fragment>
      <BackstageInput
        className="InputText"
        label="Passwort"
        error={password1 === '' && meta.error}
        touched={meta.touched}
        required={required}
        info={info}
        inputId={field}
      >
        <input
          type="password"
          value={password1}
          onChange={(e) => setPassword1((e.target as HTMLInputElement).value)}
          id={field}
        />
      </BackstageInput>
      <BackstageInput
        className="InputText"
        label="Passwort Wiederholen"
        error={password2Error || (password2 === '' && meta.error)}
        touched={meta.touched}
        required={required}
        inputId={field}
      >
        <input
          type="password"
          value={password2}
          onChange={(e) => onChangePassword2((e.target as HTMLInputElement).value)}
          id={field}
        />
      </BackstageInput>
    </Fragment>
  );
};

export default BackstageFormInputText;
