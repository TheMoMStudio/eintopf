// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useEffect } from 'preact/hooks';
import { BackstageFormInputProps } from './BackstageFormInput';
import BackstageFormInputArray from './BackstageFormInputArray';
import { Interval } from '../api/revent';
import { newDate } from '../utils/date';
import './BackstageFormInputArrayInterval.css';
import { useAppData } from '../appData';

export const BackstageFormInputArrayInterval: FunctionComponent<
  BackstageFormInputProps<Interval[]>
> = ({ field, label, info }) => {
  const { timezone } = useAppData();
  return (
    <BackstageFormInputArray<Interval>
      field={field}
      name="Intervall"
      label={label}
      info={info}
      fieldComponent={({ value, onChange }) => (
        <InputInterval interval={value} setInterval={onChange} />
      )}
      newValue={() => ({
        type: 'day',
        interval: 1,
        weekDay: 1,
        date: newDate(timezone).toISOString(),
      })}
      minLength={1}
    />
  );
};
export default BackstageFormInputArrayInterval;

interface InputIntervalProps {
  interval: Interval;
  setInterval: (interval: Interval) => void;
}

const InputInterval: FunctionComponent<InputIntervalProps> = ({ interval, setInterval }) => {
  const { timezone } = useAppData();
  const setIntervalInternal = (partialInterval: Partial<Interval>) =>
    setInterval({
      ...interval,
      ...partialInterval,
    });

  useEffect(() => {
    switch (interval.type) {
      case 'day':
        if (interval.date === undefined) {
          setIntervalInternal({ date: newDate(timezone).toISOString() });
        }
        break;
      case 'week':
      case 'month':
      default:
        setIntervalInternal({ date: undefined });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [interval.type]);

  return (
    <div className="InputInterval">
      <div className="InputSelect InputInterval-Type">
        <select
          value={interval.type}
          onChange={(e: any) => setIntervalInternal({ ...interval, type: e.target.value })}
        >
          <option value="day">Täglich</option>
          <option value="week">Wöchentlich</option>
          <option value="month">Monatlich</option>
        </select>
      </div>
      {interval.type === 'day' && (
        <div>
          Jeden{' '}
          <span className="InputSelect">
            <select
              value={interval.interval}
              onChange={(e: any) => setIntervalInternal({ interval: parseInt(e.target.value, 10) })}
            >
              <option value="1" label=" " />
              <option value="2">zweiten</option>
              <option value="3">dritten</option>
              <option value="4">vierten</option>
              <option value="5">fünften</option>
              <option value="6">sechsten</option>
            </select>
          </span>
        </div>
      )}
      {interval.type === 'week' && (
        <div>
          Jede{' '}
          <span className="InputSelect">
            <select
              value={interval.interval}
              onChange={(e: any) => setIntervalInternal({ interval: parseInt(e.target.value, 10) })}
            >
              <option value="1" label=" " />
              <option value="2">zweite</option>
              <option value="3">dritte</option>
              <option value="4">vierte</option>
            </select>
          </span>{' '}
          Woche{' '}
          <span className="InputSelect">
            <select
              value={interval.weekDay}
              onChange={(e: any) => setIntervalInternal({ weekDay: parseInt(e.target.value, 10) })}
            >
              <option value="1">Montag</option>
              <option value="2">Dienstag</option>
              <option value="3">Mittwoch</option>
              <option value="4">Donnerstag</option>
              <option value="5">Freitag</option>
              <option value="6">Samstag</option>
              <option value="0">Sonntag</option>
            </select>
          </span>
        </div>
      )}
      {interval.type === 'month' && (
        <div>
          Jeden{' '}
          <span className="InputSelect">
            <select
              value={interval.interval}
              onChange={(e: any) => setIntervalInternal({ interval: parseInt(e.target.value, 10) })}
            >
              <option value="1">ersten</option>
              <option value="2">zweiten</option>
              <option value="3">dritten</option>
              <option value="4">vierten</option>
            </select>{' '}
          </span>
          <span className="InputSelect">
            <select
              value={interval.weekDay}
              onChange={(e: any) => setIntervalInternal({ weekDay: parseInt(e.target.value, 10) })}
            >
              <option value="1">Montag</option>
              <option value="2">Dienstag</option>
              <option value="3">Mittwoch</option>
              <option value="4">Donnerstag</option>
              <option value="5">Freitag</option>
              <option value="6">Samstag</option>
              <option value="0">Sonntag</option>
            </select>
          </span>{' '}
          im Monat
        </div>
      )}
    </div>
  );
};
