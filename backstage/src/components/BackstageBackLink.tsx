// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { FunctionComponent, h } from 'preact';
import { Link } from 'preact-router';
import './BackstageBackLink.css';

export interface BackstageBackLinkProps {
  className?: string;
  title?: string;
  href: string;
}

export const BackstageBackLink: FunctionComponent<BackstageBackLinkProps> = ({
  title = '',
  href,
  className = '',
}) => (
  // @ts-ignore: This has worked and should absolutely work
  <Link href={href} title={title} className={`Back-Link ${className}`}>
    <img src="/assets/images/icon_arrow_right.svg" class="BackstageButton" alt={title} />
    <span>Zurück</span>
  </Link>
);

export default BackstageBackLink;
