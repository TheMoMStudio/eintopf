// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import BackstageFormButtonLatLng from '../components/BackstageFormButtonLatLng';
import BackstageFormInputArrayUser from '../components/BackstageFormInputArrayUser';
import BackstageFormInputCheckbox from '../components/BackstageFormInputCheckbox';
import BackstageFormInputImage from '../components/BackstageFormInputImage';
import BackstageFormInputLink from '../components/BackstageFormInputLink';
import BackstageFormInputNumber from '../components/BackstageFormInputNumber';
import BackstageFormInputText from '../components/BackstageFormInputText';
import BackstageFormInputTextLong from '../components/BackstageFormInputTextLong';

export const BackstagePlaceFormFields: FunctionComponent = () => (
  <Fragment>
    <BackstageFormInputCheckbox
      field="published"
      label="Veröffentlicht"
      defaultValue={true}
      info="Der Ort wird erst in der Liste angezeigt, wenn er veröffentlicht wurde. Nicht veröffentlichte Orte können per Url aufgerufen werden. Die Veröffentlichung kann jederzeit rückgängig gemacht werden."
    />
    <BackstageFormInputText field="name" label="Name" required />
    <BackstageFormInputLink field="link" label="Website" />
    <BackstageFormInputText field="email" label="E-Mail" />
    <BackstageFormInputText field="address" label="Adresse" required />
    <BackstageFormButtonLatLng
      addressField="address"
      latField="lat"
      lngField="lng"
      info="Damit der Ort auf der Karte angezeigt werden kann müssen die Koordinaten eingetragen werden. Über den Button können diese aus der Adresse generiert werden."
    />
    <BackstageFormInputNumber field="lat" label="Breitengrad" />
    <BackstageFormInputNumber field="lng" label="Längengrad" />
    <BackstageFormInputTextLong field="description" label="Beschreibung" />
    <BackstageFormInputImage
      field="image"
      label="Bild"
      previews={[
        {
          name: 'Vorschau Listen-Seite',
          className: 'InputImage-Preview-Image-Container-Listview',
          width: 380,
          height: 160,
        },
        { name: 'Vorschau Detail-Seite', width: 380 },
      ]}
      info="Das Bild wird auf der Ortsseite mit einer Breite von 380px angezeigt. Auf der Listenseite wird das Bild im Format 380x160px angezeigt. Um Daten zu sparen sollte das Bild nicht zu groß sein."
    />
    <BackstageFormInputText field="alt" label="Bildbeschreibung" />
    <BackstageFormInputArrayUser
      field="ownedBy"
      label="Benutzer*innen"
      info="Es können beliebig viele Benutzer*innen hinzugefügt werden, die den Ort Bearbeiten können."
    />
  </Fragment>
);
