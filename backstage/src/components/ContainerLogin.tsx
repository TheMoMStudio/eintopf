// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { route } from 'preact-router';
import { LoginCredentials } from '../api/auth';
import useUser from '../hooks/useUser';
import BackstageForm from './BackstageForm';
import BackstageFormInputText from './BackstageFormInputText';
import Button from './Button';
import Container from './Container';
import Link from './Link';
import './ContainerLogin.css';

export const ContainerLogin: FunctionComponent = () => {
  const { user, login } = useUser();

  return (
    <Container title="Backstage Login">
      {user ? (
        <div>Du bist bereits angemeldet</div>
      ) : (
        <BackstageForm<LoginCredentials>
          className="LoginForm"
          onSubmit={async (credentials: LoginCredentials): Promise<string | undefined> => {
            const err = await login(credentials);
            if (typeof err === 'string') {
              return err;
            }

            // Redirect to /backstage, then not on a backstage url already
            if (!window.location.pathname.startsWith('/backstage')) {
              route('/backstage', true);
            }

            return undefined;
          }}
        >
          <BackstageFormInputText field="email" type="email" placeholder="E-Mail" />
          <BackstageFormInputText field="password" type="password" placeholder="Passwort" />
          <Link highlight href="/backstage/requestPasswordRecovery" className="PassRec">
            Passwort vergessen?
          </Link>
          <Button type="submit">Login</Button>
        </BackstageForm>
      )}
    </Container>
  );
};
export default ContainerLogin;
