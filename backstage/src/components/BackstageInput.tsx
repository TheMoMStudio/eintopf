// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, ComponentChild, FunctionComponent, Fragment, ComponentChildren, VNode } from 'preact';
import { useField } from 'react-final-form';
import useUser from '../hooks/useUser';
import { checkNeedsRole, NeedsRole } from '../utils/role';
import BackstageError from './BackstageError';
import './BackstageInput.css';

export interface BackstageInputProps {
  children: ComponentChild;
  className: string;
  label?: string;
  inputId: string;
  needsRole?: NeedsRole;
  error?: string;
  touched?: boolean;
  required?: boolean;
  info?: string | VNode;
  hideWhen?: HideWhenCondition;
}

export type HideWhenCondition = { field: string; is: (value: any) => boolean };

export const BackstageInput: FunctionComponent<BackstageInputProps> = ({
  children,
  className,
  label,
  inputId,
  needsRole,
  hideWhen,
  error,
  touched,
  required = false,
  info,
}) => {
  const { user } = useUser();
  if (needsRole && user !== undefined && checkNeedsRole(needsRole, user.role)) {
    return <Fragment />;
  }

  const component = (
    <div
      className={`BackstageInput ${className} ${error && touched ? 'BackstageInput-Error' : ''}`}
    >
      {label && (
        <div className="BackstageInput-Label">
          <label for={inputId}>{label}</label>
          {required ? '*' : ''}:
        </div>
      )}
      {info && <div className="BackstageInput-Info">{info}</div>}
      <div className="BackstageInput-Container">
        {error && touched && <BackstageError err={error} />}
        {children}
      </div>
    </div>
  );

  if (hideWhen !== undefined) {
    return (
      <HideField field={hideWhen.field} when={hideWhen.is}>
        {component}
      </HideField>
    );
  }

  return component;
};

export default BackstageInput;

interface ConditionProps {
  field: string;
  when: (value: any) => boolean;
  children: ComponentChildren;
}

export const HideField: FunctionComponent<ConditionProps> = ({ field, when, children }) => {
  const { input } = useField(field, { subscription: { value: true } });
  return when(input.value) ? <Fragment /> : <Fragment>{children}</Fragment>;
};
