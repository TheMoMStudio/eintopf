// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, ComponentChildren, FunctionComponent, Fragment } from 'preact';
import { useState } from 'preact/hooks';
import { Button, ButtonVariant } from './Button';
import ConfirmDialog from './ConfirmDialog';

export interface ButtonConfirmProps {
  children: ComponentChildren;
  confirmMessage: string;
  onConfirm: () => void;
  variant?: ButtonVariant;
}

export const ButtonConfirm: FunctionComponent<ButtonConfirmProps> = ({
  children,
  onConfirm,
  confirmMessage,
  variant,
}) => {
  const [dialogOpen, setDialogOpen] = useState(false);

  const abort = () => {
    setDialogOpen(false);
  };

  const confirm = () => {
    setDialogOpen(false);
    onConfirm();
  };

  return (
    <Fragment>
      <Button onClick={() => setDialogOpen(true)} variant={variant}>
        {children}
      </Button>
      {dialogOpen && <ConfirmDialog message={confirmMessage} onAbort={abort} onConfirm={confirm} />}
    </Fragment>
  );
};

export default ButtonConfirm;
