// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import { useField } from 'react-final-form';
import useUser from '../hooks/useUser';
import { checkNeedsRole, NeedsRole } from '../utils/role';
import { BackstageFormInputProps } from './BackstageFormInput';
import BackstageInput from './BackstageInput';
import './BackstageFormInputSelect.css';
import validateRequired from '../utils/validateRequired';

export interface BackstageFormInputSelectOption {
  value: string;
  label: string;
  needsRole?: NeedsRole;
}

export interface BackstageFormInputSelectProps extends BackstageFormInputProps<string> {
  options: BackstageFormInputSelectOption[];
}

export const BackstageFormInputSelect: FunctionComponent<BackstageFormInputSelectProps> = ({
  field,
  label,
  needsRole,
  required,
  options,
  ...rest
}) => {
  const { input, meta } = useField(field, { validate: required ? validateRequired : undefined });

  const { user } = useUser();
  if (!user) return <Fragment />;

  return (
    <BackstageInput
      className="InputSelect"
      label={label}
      needsRole={needsRole}
      inputId={field}
      required={required}
    >
      {meta.error && meta.touched && <span className="BackstageError">{meta.error}</span>}
      <select
        {...rest}
        {...(input as Record<string, unknown>)}
        className={meta.error && meta.touched ? 'error' : ''}
      >
        {options.map((option) =>
          option.needsRole ? (
            !checkNeedsRole(option.needsRole, user.role) && (
              <option value={option.value}>{option.label}</option>
            )
          ) : (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ),
        )}
      </select>
    </BackstageInput>
  );
};

export default BackstageFormInputSelect;
