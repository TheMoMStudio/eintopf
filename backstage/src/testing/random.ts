// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

/**
 * Generates a random integer number with a range of min .. max.
 *
 * @param {number} max
 * @returns {number} the generated number
 */
export const randomIntFromRange = (min: number, max: number): number =>
  Math.floor(Math.random() * Math.floor(max - min + 1) + Math.floor(min));

/**
 * Generates a random integer number with a range of 0 .. max.
 *
 * @param {number} max
 * @returns {number} the generated number
 */
export const randomInt = (max: number): number => randomIntFromRange(0, max);

/**
 * Returns a radom floating point number with a range of min .. max. The part after the floating point is in a range of minDecimals and maxDecimals.
 * @param {number} min
 * @param {number} max
 * @param {number} minDecimals
 * @param {number} maxDecimals
 * @returns {number} the generated number
 */
export const randomFloatFromRange = (
  min: number,
  max: number,
  minDecimals: number,
  maxDecimals: number,
): number =>
  parseFloat(`${randomIntFromRange(min, max)}.${randomIntFromRange(minDecimals, maxDecimals)}`);

/**
 * Returns a radom floating point number with a range of 0 .. max with given maximum of decimals.
 * @param {number} max
 * @param {number} maxDecimals
 * @returns {number} the generated number
 */
export const randomFloat = (max: number, maxDecimals: number): number =>
  randomFloatFromRange(0, max, 0, maxDecimals);

/**
 * Generated a random boolean.
 *
 * @returns {boolean} the generated boolan
 */
export const randomBoolean = (): boolean => randomInt(1) === 1;

/**
 * Generates a random string, with a given length.
 *
 * @param {number} len
 * @returns {string} the generated string
 */
export const randomString = (len: number): string => {
  const CHARACTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  let str = '';
  for (let i = 0; i < len; i++) {
    str += CHARACTERS.charAt(randomInt(CHARACTERS.length));
  }
  return str;
};

export const randomDateString = (): string => new Date().toISOString();
