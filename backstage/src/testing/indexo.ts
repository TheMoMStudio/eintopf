// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { EventDocument } from '../api/indexo';
import { randomBoolean, randomDateString, randomFloat, randomString } from './random';

/**
 * Combines the object with an optional partial object.
 *
 * @param {T} obj
 * @param {Partial<T>} partial
 */
export const combineWithPartial = <T>(obj: T, partial?: Partial<T>): T => {
  if (!partial) return obj;
  return { ...obj, ...partial };
};

export const createEventDocument = (eventDocument?: Partial<EventDocument>): EventDocument =>
  combineWithPartial(
    {
      id: randomString(12),
      published: randomBoolean(),
      canceled: randomBoolean(),
      name: randomString(12),
      organizers: [{ name: randomString(12) }],
      involved: [{ name: randomString(12), description: randomString(200) }],
      location: {
        name: randomString(12),
        address: randomString(12),
        lat: randomFloat(2, 10),
        lng: randomFloat(2, 10),
      },
      location2: randomString(12),
      start: randomDateString(),
      end: randomDateString(),
      description: randomString(240),
      image: '',
      tags: [],
    },
    eventDocument,
  );
