// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { queryCategoryGet, queryCategoryUpdate, Category } from '../api/category';
import BackstageFormUpdate from '../components/BackstageFormUpdate';
import Container from '../components/Container';
import PageBackstage from '../components/PageBackstage';
import BackstageFormInputText from '../components/BackstageFormInputText';
import BackstageFormInputTextLong from '../components/BackstageFormInputTextLong';

export interface BackstageCategoryUpdatePageProps {
  id: string;
}

export const BackstageCategoryUpdatePage: FunctionComponent<BackstageCategoryUpdatePageProps> = ({
  id,
}) => (
  <PageBackstage>
    <Container title="Kategorie Bearbeiten">
      <BackstageFormUpdate<Category>
        id={id}
        queryUpdate={queryCategoryUpdate}
        queryGet={queryCategoryGet}
        model="categories"
      >
        <BackstageFormInputText field="name" label="Name" required />
        <BackstageFormInputText field="headline" label="Überschrift" required />
        <BackstageFormInputTextLong field="description" label="Beschreibung" />
      </BackstageFormUpdate>
    </Container>
  </PageBackstage>
);

export default BackstageCategoryUpdatePage;
