// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { Role, RoleAdmin, RoleModerator, RoleNormal } from '../api/auth';
import { queryEventGet, queryEventUpdate, Event } from '../api/event';
import Container from '../components/Container';
import BackstageFormUpdate from '../components/BackstageFormUpdate';
import BackstageFormInputCheckbox from '../components/BackstageFormInputCheckbox';
import BackstageFormNotice from '../components/BackstageFormNotice';
import PageBackstage from '../components/PageBackstage';
import {
  BackstageEventFormFields,
  preSubmitEvent,
  validateEvent,
} from '../components/BackstageEventForm';
import { useAppData } from '../appData';

const deactivatedMessage =
  'Die Veranstaltung wurde von der Moderation deaktiviert. Für mehr Informationen melde dich bitte bei der Moderation (mod@eintopf.info). Solange die Veranstaltung deaktiviert ist, wird sie nicht auf eintopf.info angezeigt.';

export interface BackstageEventUpdatePageProps {
  id: string;
}

export const BackstageEventUpdatePage: FunctionComponent<BackstageEventUpdatePageProps> = ({
  id,
}) => {
  const { timezone } = useAppData();
  return (
    <PageBackstage>
      <Container title="Veranstaltung Bearbeiten">
        <BackstageFormUpdate<Event>
          id={id}
          queryUpdate={(apiurl: string, token: string, model: Event) => {
            const event = preSubmitEvent(model);
            return queryEventUpdate(apiurl, token, event);
          }}
          queryGet={queryEventGet}
          model="events"
          validate={(values) => validateEvent(timezone, values)}
        >
          <BackstageFormNotice
            field="deactivated"
            message={(deactivated) => (deactivated ? deactivatedMessage : false)}
            needsRole={(role: Role) => role === RoleNormal}
          />
          <BackstageFormInputCheckbox
            field="deactivated"
            label="Deaktiviert"
            needsRole={(role: Role) => role === RoleAdmin || role === RoleModerator}
          />
          <BackstageEventFormFields update />
        </BackstageFormUpdate>
      </Container>
    </PageBackstage>
  );
};

export default BackstageEventUpdatePage;
