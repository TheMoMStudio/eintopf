// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { route } from 'preact-router';
import { queryInvitation, InvitationRequest, InvitationError } from '../api/invitation';
import BackstageForm from '../components/BackstageForm';
import BackstageFormButtonSubmit from '../components/BackstageFormButtonSubmit';
import BackstageFormInputEmail from '../components/BackstageFormInputEmail';
import BackstageFormInputText from '../components/BackstageFormInputText';
import BackstageFormInputPassword from '../components/BackstageFormInputPassword';
import BackstageUsagePrinciples from '../components/BackstageUsagePrinciples';
import Container from '../components/Container';
import Grid from '../components/Grid';
import Link from '../components/Link';
import useUser from '../hooks/useUser';
import { useAppData } from '../appData';

export interface BackstageInvitationPageProps {
  token?: string;
}

export const BackstageInvitationPage: FunctionComponent<BackstageInvitationPageProps> = ({
  token,
}) => {
  const { user, login } = useUser();
  const { apiurl } = useAppData();

  const onSubmit = async (credentials: InvitationRequest): Promise<InvitationError | ''> => {
    if (!token) return '';

    const err = await queryInvitation(apiurl, token, credentials);
    if (typeof err === 'string') {
      return err as InvitationError;
    }

    login(credentials);
    return '';
  };

  // If the automatic login succeeded, redirect to backstage.
  if (user !== undefined) {
    route('/backstage/first-steps');
  }

  return (
    <Grid>
      <Grid cols={2}>
        <Container title="Backstage Einladung">
          <h3>Herzlichen Glückwunsch!</h3>
          <p>
            Du wurdest eingeladen den Inhalt von EINTOPF mit zu gestalten und mit deinen eigenen
            Veranstaltungen zu prägen. Was ist EINTOPF überhaupt? Kurz: Ein alternativer
            Veranstaltungskalender für Stuttgart und Umgebung. Auf der{' '}
            <Link highlight href="/about">
              ÜBER EINTOPF
            </Link>{' '}
            Seite erfährst du mehr.
          </p>
          <br />
          <h3>Grundsätze zur Benutzung</h3>
          <BackstageUsagePrinciples />
          <h3>Datenschutz</h3>
          <p>
            Wir speichern nur die von dir angegebenen Daten. Diese sind nur für dich, die
            Moderator*innen und die Administrator*innen sichtbar. Dein Spitzname kann von allen
            Benutzer*innen eingesehen werden, damit diese dich für die Bearbeitung von Gruppen,
            Orten und Veranstaltungen freischalten können.
            <br />
            Du kannst deinen Account jederzeit wieder löschen. Dabei werden alle persönlichen Daten
            gelöscht. Veranstaltungen, Orte und Gruppen, die du erstellt hast, bleiben aber erhalten
            falls du sie nicht zuvor selbst löscht.
            <br />
            <br />
            Wenn du dich einloggst, wird in deinem Browser ein Token gespeichert, damit du nicht
            jedes Mal dein Passwort erneut eingeben musst. Dieses wird wieder gelöscht, nachdem du
            dich ausgeloggt hast. D.h. falls du dich auf einem Gerät einloggst, welches auch von
            anderen Personen genutzt wird, empfehlen wir dir, dich nach der Nutzung von EINTOPF
            wieder auszuloggen.
          </p>
          <br />
          <br />
          <BackstageForm<InvitationRequest>
            className="BackstageInvitationForm"
            onSubmit={onSubmit}
            requiredNotice
          >
            <BackstageFormInputEmail
              field="email"
              label="E-Mail"
              required
              info="Falls du dein Passwort vergessen hast, kannst du dir über die angegebene E-Mail-Adresse dein Passwort neu setzten. Außerdem läuft die Anmeldung über die E-Mail-Adresse."
            />
            <BackstageFormInputText
              field="nickname"
              label="Spitzname"
              required
              info="Der Spitzname wird verwendet, um in der Backstage andere Benutzer*innen zu referenzieren. Das ermöglicht das gemeinsame Bearbeiten von Veranstaltungen, Orten und Gruppen."
            />
            <BackstageFormInputPassword field="password" required />
            <BackstageFormButtonSubmit>Registrieren</BackstageFormButtonSubmit>
          </BackstageForm>
        </Container>
      </Grid>
    </Grid>
  );
};

export default BackstageInvitationPage;
