// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import { useState } from 'preact/hooks';
import { queryRepeatingEventCreate, RepeatingEvent, NewRepeatingEvent } from '../api/revent';
import { useAppData } from '../appData';
import BackstageFormCreate from '../components/BackstageFormCreate';
import BackstageFormGenerateEvents from '../components/BackstageFormGenerateEvents';
import {
  BackstageRepeatingEventFormFields,
  preSubmitRepeatingEvent,
  validateRepeatingEvent,
} from '../components/BackstageRepeatingEventForm';
import Container from '../components/Container';
import Link from '../components/Link';
import PageBackstage from '../components/PageBackstage';
import BackstageContainerEventList from '../components/BackstageContainerEventList';
import './BackstageRepeatingEventCreatePage.css';

export const BackstageRepeatingEventCreatePage: FunctionComponent = () => {
  const { timezone } = useAppData();
  const [step, setStep] = useState<'create' | 'generate' | 'list'>('create');
  const [id, setID] = useState<string | undefined>(undefined);

  return (
    <PageBackstage>
      {step === 'create' && (
        <>
          <Container title="1. Schritt">
            <p>
              Lege zuerst eine regelmäßige Veranstaltung an. Danach kannst du daraus mehrere
              Veranstaltungen in den angegeben Intervallen generieren.
            </p>
          </Container>
          <Container title="Regelmäßige Veranstaltung Erstellen">
            <BackstageFormCreate<RepeatingEvent>
              queryCreate={(apiurl, token: string, model: NewRepeatingEvent) => {
                const repeatingEvent = preSubmitRepeatingEvent(model);
                return queryRepeatingEventCreate(apiurl, token, repeatingEvent);
              }}
              model="repeatingevents"
              onCreate={({ id }) => {
                setID(id);
                setStep('generate');
              }}
              validate={(values) => validateRepeatingEvent(values, timezone)}
            >
              <BackstageRepeatingEventFormFields />
            </BackstageFormCreate>
          </Container>
        </>
      )}
      {step === 'generate' && id && (
        <>
          <Container title="2. Schritt">
            <p>
              Die regelmäßige Veranstaltung wurde erfolreich erstellt. Als nächstes können daraus
              einzelne Veranstaltungen für den ausgewählten Zeitraum erstellt werden.
            </p>
            <br />
            <div className="RepeatingEventCreate-Buttons">
              <Link variant="Button" href="/backstage/repeatingevents">
                Zur Liste
              </Link>
              <Link variant="Button" href={`/backstage/repeatingevents/${id}`}>
                Regelmäßige Veranstaltung bearbeiten
              </Link>
            </div>
          </Container>
          <Container title="Veranstaltungen generieren">
            <BackstageFormGenerateEvents repeatingEventID={id} onGenerate={() => setStep('list')} />
          </Container>
        </>
      )}
      {step === 'list' && id && (
        <>
          <Container title="3. Schritt">
            <p>
              Die einzelnen Veranstaltungen wurden erfolgreich generiert. Als nächstes können
              einzelne Veranstaltungen bearbeitet oder gelöscht werden. Die einzelnen
              Veranstaltungen werden auf der <i>Bearbeiten</i> Seite der regelmäßigen Veranstaltung
              angezeigt und auch in der normalen <i>Veranstaltungsliste</i>.
            </p>
            <div className="RepeatingEventCreate-Buttons">
              <Link variant="Button" href="/backstage/repeatingevents">
                Zur Liste
              </Link>
              <Link variant="Button" href={`/backstage/repeatingevents/${id}`}>
                Regelmäßige Veranstaltung bearbeiten
              </Link>
            </div>
          </Container>
          <BackstageContainerEventList
            title="Veranstaltungen"
            baseFilters={{ parent: `revent:${id}` }}
          />
        </>
      )}
    </PageBackstage>
  );
};
export default BackstageRepeatingEventCreatePage;
