// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { queryEventCreate, Event, NewEvent } from '../api/event';
import Container from '../components/Container';
import BackstageFormCreate from '../components/BackstageFormCreate';
import PageBackstage from '../components/PageBackstage';
import {
  BackstageEventFormFields,
  preSubmitEvent,
  validateEvent,
} from '../components/BackstageEventForm';
import { useAppData } from '../appData';

export const BackstageEventCreatePage: FunctionComponent = () => {
  const { timezone } = useAppData();
  return (
    <PageBackstage>
      <Container title="Veranstaltung Erstellen">
        <BackstageFormCreate<Event>
          queryCreate={(apiurl: string, token: string, model: NewEvent) => {
            const event = preSubmitEvent(model);
            return queryEventCreate(apiurl, token, event);
          }}
          model="events"
          validate={(values) => validateEvent(timezone, values)}
        >
          <BackstageEventFormFields />
        </BackstageFormCreate>
      </Container>
    </PageBackstage>
  );
};

export default BackstageEventCreatePage;
