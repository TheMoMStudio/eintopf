// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { Role, RoleAdmin, RoleModerator } from '../api/auth';
import { queryGroupCrud, Group } from '../api/group';
import BackstageList from '../components/BackstageList';
import BackstageListField from '../components/BackstageListField';
import BackstageListFilterText from '../components/BackstageListFilterText';
import BackstageListFilterToggle from '../components/BackstageListFilterToggle';
import PageBackstage from '../components/PageBackstage';
import useBackstageConfig from '../hooks/useBackstageConfig';
import {
  BackstageActionButtonDelete,
  BackstageActionButtonEdit,
  BackstageActionButtonView,
} from '../components/BackstageActionButtons';

export const BackstageGroupListPage: FunctionComponent = () => {
  const { getValue, setValue } = useBackstageConfig();

  const initialOwnedByOn: boolean = getValue<boolean>(
    'groupList/ownedByOn',
    'boolean',
    true,
  ) as boolean;
  const initialPageSize: number = getValue<number>('groupList/pageSize', 'number', 25) as number;
  const initialSearch = getValue<string>('groupList/search', 'string', undefined);

  return (
    <PageBackstage>
      <BackstageList
        title="Gruppen"
        noItemsInfo="Keine Gruppen vorhanden"
        bulkDeleteConfirmMessage="Willst du wirklich alle ausgewählten Gruppen löschen?"
        queryCrud={queryGroupCrud}
        config={{
          pageSize: initialPageSize,
          initialSorting: 'name',
          initialOrder: 'DESC',
          baseFilters: {
            ownedBy: initialOwnedByOn ? ['self'] : undefined,
            likeName: initialSearch,
          },
        }}
        filters={[
          <BackstageListFilterToggle
            key="ownedByFilter"
            field="ownedBy"
            needsRole={(role: Role) => role === RoleAdmin || role === RoleModerator}
            initialState={initialOwnedByOn}
            onChange={(on) => setValue('groupList/ownedByOn', on)}
            value={(on) => (on ? ['self'] : undefined)}
            label="Nur eigene Anzeigen"
          />,
          <BackstageListFilterText
            key="searchFilter"
            field="likeName"
            initialState={initialSearch || ''}
            onChange={(v) => setValue('groupList/search', v)}
            label="Suche"
          />,
        ]}
        onPageSizeChange={(pageSize) => setValue('groupList/pageSize', pageSize)}
      >
        <BackstageListField<Group> field="name" name="Name" display="text" />
        <BackstageListField<Group> field="published" name="Veröffentlicht" display="boolean" />
        <BackstageListField
          field="deactivated"
          name="Deaktiviert"
          display="boolean"
          needsRole={(role: Role) => role === RoleAdmin || role === RoleModerator}
        />
        <BackstageActionButtonEdit<Group> context="groups" />
        <BackstageActionButtonView<Group> context="gruppe" />
        <BackstageActionButtonDelete<Group> />
      </BackstageList>
    </PageBackstage>
  );
};

export default BackstageGroupListPage;
