// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useState } from 'preact/hooks';
import { Role, RoleAdmin, RoleModerator, RoleNormal } from '../api/auth';
import { ListResponse } from '../api/crud';
import { queryEventList, Event, queryEventUpdate } from '../api/event';
import { queryRepeatingEventGet, queryRepeatingEventUpdate, RepeatingEvent } from '../api/revent';
import Container from '../components/Container';
import BackstageFormUpdate from '../components/BackstageFormUpdate';
import BackstageFormInputCheckbox from '../components/BackstageFormInputCheckbox';
import BackstageFormNotice from '../components/BackstageFormNotice';
import PageBackstage from '../components/PageBackstage';
import BackstageFormGenerateEvents from '../components/BackstageFormGenerateEvents';
import {
  BackstageRepeatingEventFormFields,
  preSubmitRepeatingEvent,
  validateRepeatingEvent,
} from '../components/BackstageRepeatingEventForm';
import BackstageContainerEventList from '../components/BackstageContainerEventList';
import { newDate } from '../utils/date';
import useUser from '../hooks/useUser';
import { useAppData } from '../appData';
import ConfirmDialog from '../components/ConfirmDialog';

const deactivatedMessage =
  'Die Regelmäßige Veranstaltung wurde von der Moderation deaktiviert. Für mehr Informationen melde dich bitte bei der Moderation (mod@eintopf.info). Solange die Regelmäßige Veranstaltung deaktiviert ist, wird sie nicht auf eintopf.info angezeigt.';

export interface BackstageEventUpdatePageProps {
  id: string;
}

export const BackstageRepeatingEventUpdatePage: FunctionComponent<
  BackstageEventUpdatePageProps
> = ({ id }) => {
  const { apiurl, timezone } = useAppData();
  const { user } = useUser();
  const [reloadEvents, setReloadEvents] = useState(0);

  const [repeatingEvent, setRepeatingEvent] = useState<RepeatingEvent | undefined>(undefined);
  const [updateEvents, setUpdateEvents] = useState<ListResponse<Event> | undefined>(undefined);

  /**
   * Deletes existing events, that were generated previously and regenerates them.
   */
  const regenerateEvents = async () => {
    if (!user) return;
    if (!updateEvents) return;
    if (!repeatingEvent) return;

    for (const event of updateEvents.items) {
      await queryEventUpdate(apiurl, user.token, {
        ...repeatingEvent,
        id: event.id,
        start: event.start,
        end: event.end,
        published: event.published,
        parent: event.parent,
      });
    }

    setReloadEvents(reloadEvents + 1);
  };

  return (
    <PageBackstage>
      <Container title="Regelmäßige Veranstaltung Bearbeiten">
        {updateEvents && (
          <ConfirmDialog
            message={`Sollen die bereits generierten Veranstaltungen (${updateEvents.total}) geupdated werden? Wenn du das Interval geändert hast musst du evtl. selber alle zukünftigen Veranstaltungen löschen und neu generieren.`}
            onAbort={() => {
              setUpdateEvents(undefined);
            }}
            onConfirm={() => {
              regenerateEvents();
              setUpdateEvents(undefined);
            }}
          />
        )}
        <BackstageFormUpdate<RepeatingEvent>
          id={id}
          queryUpdate={(apiUrl, token: string, model: RepeatingEvent) => {
            const revent = preSubmitRepeatingEvent(model);
            return queryRepeatingEventUpdate(apiUrl, token, revent);
          }}
          queryGet={queryRepeatingEventGet}
          model="repeatingevents"
          validate={(values) => validateRepeatingEvent(values, timezone)}
          onUpdate={async (revent: RepeatingEvent) => {
            if (!user) return;

            const events = await queryEventList(apiurl, user.token, {
              filters: {
                parent: `revent:${id}`,
                startAfter: newDate(timezone),
              },
            });
            if (typeof events === 'string') {
              return;
            }
            if (events.total === 0) {
              document.getElementById('generate')?.scrollIntoView({
                behavior: 'smooth',
                block: 'center',
              });
              return;
            }

            setRepeatingEvent(revent);
            setUpdateEvents(events);
          }}
        >
          <BackstageFormNotice
            field="deactivated"
            message={(deactivated) => (deactivated ? deactivatedMessage : false)}
            needsRole={(role: Role) => role === RoleNormal}
          />
          <BackstageFormInputCheckbox
            field="deactivated"
            label="Deaktiviert"
            needsRole={(role: Role) => role === RoleAdmin || role === RoleModerator}
          />
          <BackstageRepeatingEventFormFields />
        </BackstageFormUpdate>
      </Container>
      <Container title="Veranstaltungen generieren" id="generate">
        <BackstageFormGenerateEvents
          repeatingEventID={id}
          onGenerate={() => {
            setReloadEvents(reloadEvents + 1);
            document.getElementById('future-events')?.scrollIntoView({
              behavior: 'smooth',
              block: 'center',
            });
          }}
        />
      </Container>
      <BackstageContainerEventList
        id="future-events"
        title="Zukünftige Veranstaltungen"
        baseFilters={{ parent: `revent:${id}`, startAfter: newDate(timezone) }}
        reload={reloadEvents}
      />
      <BackstageContainerEventList
        title="Vergangene Veranstaltungen"
        baseFilters={{ parent: `revent:${id}`, startBefore: newDate(timezone) }}
        reload={reloadEvents}
      />
    </PageBackstage>
  );
};

export default BackstageRepeatingEventUpdatePage;
