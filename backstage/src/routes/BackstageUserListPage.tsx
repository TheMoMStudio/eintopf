// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { Role, RoleAdmin, RoleModerator } from '../api/auth';
import { queryUserCrud, User } from '../api/user';
import BackstageList from '../components/BackstageList';
import BackstageListField from '../components/BackstageListField';
import BackstageListFilterText from '../components/BackstageListFilterText';
import PageBackstage from '../components/PageBackstage';
import useBackstageConfig from '../hooks/useBackstageConfig';
import {
  BackstageActionButtonDelete,
  BackstageActionButtonEdit,
} from '../components/BackstageActionButtons';
import BackstageListAction from '../components/BackstageListAction';
import Link from '../components/Link';

export const BackstageUserListPage: FunctionComponent = () => {
  const { getValue, setValue } = useBackstageConfig();

  const initialPageSize: number = getValue<number>('userList/pageSize', 'number', 25) as number;
  const initialSearch = getValue<string>('userList/search', 'string', undefined);

  return (
    <PageBackstage>
      <BackstageList
        title="Benutzer*innen"
        noItemsInfo="Keine Benutzer*innen vorhanden"
        bulkDeleteConfirmMessage="Willst du wirklich alle ausgewählten Benutzer*innen löschen?"
        queryCrud={queryUserCrud}
        config={{
          pageSize: initialPageSize,
          initialSorting: 'email',
          initialOrder: 'DESC',
          baseFilters: {
            likeNickname: initialSearch,
          },
        }}
        filters={[
          <BackstageListFilterText
            key="searchFilter"
            field="likeNickname"
            initialState={initialSearch || ''}
            onChange={(v) => setValue('userList/search', v)}
            label="Suche"
          />,
        ]}
        onPageSizeChange={(pageSize) => setValue('userList/pageSize', pageSize)}
      >
        <BackstageListField<User> field="email" name="E-Mail" display="text" />
        <BackstageListField<User> field="nickname" name="Spitzname" display="text" />
        <BackstageListField<User> field="role" name="Rolle" display="text" />
        <BackstageListField<User>
          field="deactivated"
          name="Deaktiviert"
          display="boolean"
          needsRole={(role: Role) => role === RoleAdmin || role === RoleModerator}
        />
        <BackstageActionButtonEdit<User> context="users" />
        <BackstageListAction<User>
          render={({ id }) => (
            <Link href={`/backstage/notifications/new/${id}`} variant="Button icon">
              <img src="/assets/images/icon_send.svg" class="BackstageButton" alt="Nachricht" />
            </Link>
          )}
        />
        <BackstageActionButtonDelete />
      </BackstageList>
    </PageBackstage>
  );
};

export default BackstageUserListPage;
