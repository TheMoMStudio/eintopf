// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { Role, RoleAdmin, RoleModerator, RoleNormal } from '../api/auth';
import { queryGroupGet, queryGroupUpdate, Group } from '../api/group';
import BackstageFormInputCheckbox from '../components/BackstageFormInputCheckbox';
import BackstageFormNotice from '../components/BackstageFormNotice';
import BackstageFormUpdate from '../components/BackstageFormUpdate';
import { BackstageGroupFormFields } from '../components/BackstageGroupForm';
import Container from '../components/Container';
import PageBackstage from '../components/PageBackstage';

const deactivatedMessage =
  'Die Gruppe wurde von der Moderation deaktiviert. Für mehr Informationen, melde dich bitte bei der Moderation (mod@eintopf.info). Solange die Gruppe deaktiviert ist, wird sie nicht auf eintopf.info angezeigt.';

export interface BackstageGroupUpdatePageProps {
  id: string;
}

export const BackstageGroupUpdatePage: FunctionComponent<BackstageGroupUpdatePageProps> = ({
  id,
}) => (
  <PageBackstage>
    <Container title="Gruppe Bearbeiten">
      <BackstageFormUpdate<Group>
        id={id}
        queryUpdate={queryGroupUpdate}
        queryGet={queryGroupGet}
        model="groups"
      >
        <BackstageFormNotice
          field="deactivated"
          message={(deactivated) => (deactivated ? deactivatedMessage : false)}
          needsRole={(role: Role) => role === RoleNormal}
        />
        <BackstageFormInputCheckbox
          field="deactivated"
          label="Deaktiviert"
          needsRole={(role: Role) => role === RoleAdmin || role === RoleModerator}
        />
        <BackstageGroupFormFields />
      </BackstageFormUpdate>
    </Container>
  </PageBackstage>
);

export default BackstageGroupUpdatePage;
