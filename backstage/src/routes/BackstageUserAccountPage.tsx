// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import { Role, RoleAdmin, RoleModerator } from '../api/auth';
import { queryUserDelete, queryUserGet, queryUserUpdate, User } from '../api/user';
import Container from '../components/Container';
import BackstageFormUpdate from '../components/BackstageFormUpdate';
import BackstageFormInputCheckbox from '../components/BackstageFormInputCheckbox';
import BackstageFormInputPassword from '../components/BackstageFormInputPassword';
import BackstageFormInputText from '../components/BackstageFormInputText';
import ButtonConfirm from '../components/ButtonConfirm';
import PageBackstage from '../components/PageBackstage';
import useUser from '../hooks/useUser';
import { useAppData } from '../appData';

export const BackstageUserAccountPage: FunctionComponent = () => {
  const { user, logout } = useUser();
  const { apiurl } = useAppData();

  if (!user) {
    return <Fragment />;
  }

  const deleteAccount = async () => {
    await queryUserDelete(apiurl, user.token, user.id);
    logout();
  };

  return (
    <PageBackstage>
      <Container title="Daten">
        <BackstageFormUpdate<User>
          id={user.id}
          queryUpdate={queryUserUpdate}
          queryGet={queryUserGet}
          model="users"
          onUpdate={() => {}}
        >
          <BackstageFormInputCheckbox
            field="deactivated"
            label="Deaktiviert"
            needsRole={(role: Role) => role === RoleAdmin || role === RoleModerator}
          />
          <BackstageFormInputText field="nickname" label="Spitzname" />
          <BackstageFormInputText field="email" label="E-Mail" />
        </BackstageFormUpdate>
      </Container>
      <Container title="Passwort ändern">
        <BackstageFormUpdate<User>
          id={user.id}
          queryUpdate={queryUserUpdate}
          queryGet={queryUserGet}
          model="users"
          onUpdate={() => {}}
        >
          <BackstageFormInputPassword field="password" required />
        </BackstageFormUpdate>
      </Container>
      <Container title="Account">
        <ButtonConfirm
          confirmMessage="Willst du deinen Account wirklich löschen? Diese Aktion ist unwiederruflich. Dies löscht nur den Benutzeraccount und nicht die damit erstellten Veranstaltungen, Gruppen und Orte."
          onConfirm={deleteAccount}
          variant="danger"
        >
          Account Löschen
        </ButtonConfirm>
      </Container>
    </PageBackstage>
  );
};

export default BackstageUserAccountPage;
