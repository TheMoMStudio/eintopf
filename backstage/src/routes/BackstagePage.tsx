// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import BackstageFAQ from '../components/BackstageFAQ';
import BackstageNotificationList from '../components/BackstageNotificationList';
import BackstageUsagePrinciples from '../components/BackstageUsagePrinciples';
import Container from '../components/Container';
import PageBackstage from '../components/PageBackstage';

export const BackstagePage: FunctionComponent = () => (
  <PageBackstage>
    <Container title="Willkommen zur Backstage">
      <p>
        Hier kannst du Veranstaltungen, Gruppen und Orte eintragen. Bitte trage diese nur ein, wenn
        du mit Veranstalter*in, Teil der Gruppe oder des Ortes bist, oder wenn du es mit den
        jeweiligen Personen abgesprochen hast.
      </p>
    </Container>

    <BackstageNotificationList pageSize={10} baseFilters={{ viewed: false }} />

    <Container title="Grundsätze zur Benutzung">
      <BackstageUsagePrinciples />
    </Container>

    <Container title="Backstage FAQ">
      <BackstageFAQ />
    </Container>
  </PageBackstage>
);

export default BackstagePage;
