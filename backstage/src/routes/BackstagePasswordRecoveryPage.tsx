// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import { useState } from 'preact/hooks';
import { querySetPassword } from '../api/passwordrec';
import { useAppData } from '../appData';
import BackstageForm from '../components/BackstageForm';
import BackstageFormInputPassword from '../components/BackstageFormInputPassword';
import Button from '../components/Button';
import Container from '../components/Container';
import Grid from '../components/Grid';
import Link from '../components/Link';

export const BackstagePasswordRecoveryPage: FunctionComponent = () => {
  const { apiurl } = useAppData();
  const url = new URL(window.location.href);
  const token = url.searchParams.get('token') || '';

  const [success, setSuccess] = useState(false);

  const onSubmit = async ({ password }: { password: string }) => {
    const response = await querySetPassword(apiurl, token, password);
    if (typeof response === 'string') {
      return response;
    }
    setSuccess(true);
    return undefined;
  };

  return (
    <Grid>
      <Grid cols={2}>
        <Container title="Neues Passwort setzten">
          {success ? (
            <Fragment>
              <p>Passwort wurde erfolgreich zurückgesetzt</p>
              <p>
                <Link highlight href="/backstage">
                  Zur Backstage
                </Link>
              </p>
            </Fragment>
          ) : (
            <BackstageForm<{ password: string }> onSubmit={onSubmit}>
              <BackstageFormInputPassword field="password" />
              <Button type="submit">Passwort setzten</Button>
            </BackstageForm>
          )}
        </Container>
      </Grid>
    </Grid>
  );
};

export default BackstagePasswordRecoveryPage;
