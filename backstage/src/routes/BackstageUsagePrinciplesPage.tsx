// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import Container from '../components/Container';
import PageBackstage from '../components/PageBackstage';
import BackstageUsagePrinciples from '../components/BackstageUsagePrinciples';

export const BackstageUsagePrinciplesPage: FunctionComponent = () => (
  <PageBackstage>
    <Container title="Grundsätze zur Benutzung">
      <BackstageUsagePrinciples />
    </Container>
  </PageBackstage>
);

export default BackstageUsagePrinciplesPage;
