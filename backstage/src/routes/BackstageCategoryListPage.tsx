// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { queryCategoryCrud, Category } from '../api/category';
import { queryTopicCrud, Topic } from '../api/topic';
import BackstageList from '../components/BackstageList';
import BackstageListField from '../components/BackstageListField';
import BackstageListFilterText from '../components/BackstageListFilterText';
import PageBackstage from '../components/PageBackstage';
import useBackstageConfig from '../hooks/useBackstageConfig';
import {
  BackstageActionButtonDelete,
  BackstageActionButtonEdit,
  BackstageActionButtonView,
} from '../components/BackstageActionButtons';

export const BackstageCategoryListPage: FunctionComponent = () => {
  const { getValue, setValue } = useBackstageConfig();

  const initialCategoryPageSize: number = getValue<number>(
    'categoryList/pageSize',
    'number',
    25,
  ) as number;
  const initialCategorySearch = getValue<string>('categoryList/search', 'string', undefined);

  const initialTopicPageSize: number = getValue<number>(
    'topicList/pageSize',
    'number',
    25,
  ) as number;
  const initialTopicSearch = getValue<string>('topicList/search', 'string', undefined);

  return (
    <PageBackstage>
      <BackstageList
        title="Kategorien"
        noItemsInfo="Keine Kategorien vorhanden"
        bulkDeleteConfirmMessage="Willst du wirklich alle ausgewählten Kategorie löschen?"
        queryCrud={queryCategoryCrud}
        config={{
          pageSize: initialCategoryPageSize,
          initialSorting: 'name',
          initialOrder: 'DESC',
          baseFilters: {
            likeName: initialCategorySearch,
          },
        }}
        filters={[
          <BackstageListFilterText
            key="searchFilter"
            field="likeName"
            initialState={initialCategorySearch || ''}
            onChange={(v) => setValue('categoryList/search', v)}
            label="Suche"
          />,
        ]}
        onPageSizeChange={(pageSize) => setValue('categoryList/pageSize', pageSize)}
      >
        <BackstageListField<Category> field="name" name="Name" display="text" />
        <BackstageActionButtonEdit<Category> context="categories" />
        <BackstageActionButtonView<Category> context="kategorie" />
        <BackstageActionButtonDelete<Category> />
      </BackstageList>

      <BackstageList
        title="Themen"
        noItemsInfo="Kein Thema vorhanden"
        bulkDeleteConfirmMessage="Willst du wirklich alle ausgewählten Themen löschen?"
        queryCrud={queryTopicCrud}
        config={{
          pageSize: initialTopicPageSize,
          initialSorting: 'name',
          initialOrder: 'DESC',
          baseFilters: {
            likeName: initialTopicSearch,
          },
        }}
        filters={[
          <BackstageListFilterText
            key="searchFilter"
            field="likeName"
            initialState={initialTopicSearch || ''}
            onChange={(v) => setValue('categoryList/search', v)}
            label="Suche"
          />,
        ]}
        onPageSizeChange={(pageSize) => setValue('topicList/pageSize', pageSize)}
      >
        <BackstageListField<Topic> field="name" name="Name" display="text" />
        <BackstageActionButtonEdit<Topic> context="topics" />
        <BackstageActionButtonView<Topic> context="thema" />
        <BackstageActionButtonDelete<Topic> />
      </BackstageList>
    </PageBackstage>
  );
};

export default BackstageCategoryListPage;
