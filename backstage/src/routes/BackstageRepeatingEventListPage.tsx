// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useEffect, useMemo, useState } from 'preact/hooks';
import { Role, RoleAdmin, RoleModerator } from '../api/auth';
import { queryRepeatingEventCrud, RepeatingEvent } from '../api/revent';
import { queryTopicList, Topic } from '../api/topic';
import { queryCategoryList, Category } from '../api/category';
import BackstageList from '../components/BackstageList';
import BackstageListField from '../components/BackstageListField';
import BackstageListFilterToggle from '../components/BackstageListFilterToggle';
import BackstageListFilterSelect from '../components/BackstageListFilterSelect';
import PageBackstage from '../components/PageBackstage';
import useBackstageConfig from '../hooks/useBackstageConfig';
import {
  BackstageActionButtonDelete,
  BackstageActionButtonEdit,
} from '../components/BackstageActionButtons';
import useUser from '../hooks/useUser';
import { useAppData } from '../appData';

export const BackstageRepeatingEventListPage: FunctionComponent = () => {
  const { getValue, setValue } = useBackstageConfig();
  const { user } = useUser();
  const { apiurl } = useAppData();

  const initialOwnedByOn: boolean = useMemo(() => {
    const ownedByOn = getValue<boolean | undefined>('eventList/ownedByOn', 'boolean');
    if (ownedByOn === undefined) {
      return true;
    }
    return ownedByOn;
  }, [getValue]);
  const initialCategory = getValue<string>('eventList/category', 'string', undefined);
  const initialTopic = getValue<string>('eventList/topic', 'string', undefined);

  const [categories, setCategories] = useState<Category[] | undefined>(undefined);
  useEffect(() => {
    if (!user) return;
    queryCategoryList(apiurl, user.token, {}).then((result) => {
      if (typeof result == 'string') {
        console.error(result);
        return;
      }
      setCategories(result.items);
    });
  }, [apiurl, user]);

  const [topics, setTopics] = useState<Topic[] | undefined>(undefined);
  useEffect(() => {
    if (!user) return;
    queryTopicList(apiurl, user.token, {}).then((result) => {
      if (typeof result == 'string') {
        console.error(result);
        return;
      }
      setTopics(result.items);
    });
  }, [apiurl, user]);

  const initialPageSize: number = useMemo(() => {
    const pageSize = getValue<number | undefined>('eventList/pageSize', 'number');
    if (pageSize === undefined) {
      return 25;
    }
    return pageSize;
  }, [getValue]);

  return (
    <PageBackstage>
      <BackstageList
        title="Regelmäßige Veranstaltungen"
        noItemsInfo="Keine Regelmäßigen Veranstaltungen vorhanden"
        bulkDeleteConfirmMessage="Willst du wirklich alle ausgewählten Regelmäßigen Veranstaltungen löschen?"
        queryCrud={queryRepeatingEventCrud}
        config={{
          pageSize: initialPageSize,
          initialSorting: 'start',
          initialOrder: 'DESC',
          baseFilters: {
            ownedBy: initialOwnedByOn ? ['self'] : undefined,
          },
        }}
        filters={[
          <BackstageListFilterToggle
            key="ownedByFilter"
            field="ownedBy"
            needsRole={(role: Role) => role === RoleAdmin || role === RoleModerator}
            initialState={initialOwnedByOn}
            onChange={(on) => setValue('eventList/ownedByOn', on)}
            value={(on) => (on ? ['self'] : undefined)}
            label="Nur eigene Anzeigen"
          />,
          <BackstageListFilterSelect
            key="category"
            field="category"
            initialState={initialCategory}
            options={categories ? categories.map((e) => ({ value: `${e.id}`, name: e.name })) : []}
            onChange={(category) => setValue('eventList/category', category)}
            label="Kategorie"
          />,
          <BackstageListFilterSelect
            key="topic"
            field="topic"
            initialState={initialTopic}
            options={topics ? topics.map((e) => ({ value: `${e.id}`, name: e.name })) : []}
            onChange={(topic) => setValue('eventList/topic', topic)}
            label="Thema"
          />,
        ]}
        onPageSizeChange={(pageSize) => setValue('eventList/pageSize', pageSize)}
      >
        <BackstageListField<RepeatingEvent> field="name" name="Name" display="text" />
        <BackstageListField<RepeatingEvent>
          field="deactivated"
          name="Deaktiviert"
          display="boolean"
          needsRole={(role: Role) => role === RoleAdmin || role === RoleModerator}
        />
        <BackstageActionButtonEdit<RepeatingEvent> context="repeatingevents" />
        <BackstageActionButtonDelete<RepeatingEvent> />
      </BackstageList>
    </PageBackstage>
  );
};

export default BackstageRepeatingEventListPage;
