// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import { route } from 'preact-router';
import { useState } from 'preact/hooks';
import { Event, queryEventCreate } from '../api/event';
import { Group, queryGroupCreate } from '../api/group';
import { Place, queryPlaceCreate } from '../api/place';
import { BackstageEventFormFields } from '../components/BackstageEventForm';
import BackstageFormCreate from '../components/BackstageFormCreate';
import { BackstageGroupFormFields } from '../components/BackstageGroupForm';
import { BackstagePlaceFormFields } from '../components/BackstagePlaceForm';
import Button from '../components/Button';
import Container from '../components/Container';
import PageBackstage from '../components/PageBackstage';
import './BackstageFristSteps.css';

type FirstStepsStep =
  | 'welcome'
  | 'infoGroup'
  | 'createGroup'
  | 'infoPlace'
  | 'createPlace'
  | 'infoEvent'
  | 'createEvent'
  | 'nextSteps';

export const BackstageFirstStepsPage: FunctionComponent = () => {
  const [step, setStep] = useState<FirstStepsStep>('welcome');

  return (
    <PageBackstage showMenu={false}>
      {step === 'welcome' && (
        <Container title="Erste Schritte (2/4) | Willkommen">
          <h3>Willkommen bei EINTOPF Stuttgart</h3>

          <p>
            Herzlichen Glückwunsch, den ersten Schritt hast du geschafft! Du hast einen Account zur
            Verwaltung auf EINTOPF angelegt. Dieser ist <b>nicht</b> öffentlich und dient dazu deine
            Gruppen, Orte und Veranstaltungen zu erstellen und zu bearbeiten.
          </p>
          <p>
            In den nächsten Schritten hast du die Möglichkeit genau diese der Reihe nach anzulegen.
            Falls du gerade keine Zeit hast, kannst du diese Einführung auch später noch durch Klick
            auf "Erste Schritte" im Backstage Menu aufrufen.
          </p>
          <br />
          <Button onClick={() => setStep('infoGroup')}>Weiter</Button>
          <BackstageFirstStepsStepsList step={step} />
        </Container>
      )}
      {step === 'infoGroup' && (
        <Container title="Erste Schritte (2/4) | Gruppen">
          <h3>Erstellen eurer Gruppe</h3>
          <p>
            Neben dem Veranstaltungskalender auf der Startseite bieten wir Gruppen die Möglichkeit
            an, sich dauerhaft auf EINTOPF{' '}
            <a target="_blank" href="/gruppen">
              {' '}
              zu präsentieren
            </a>
            . Dafür hinterlegst du eine Beschreibung eurer Gruppe mit Logo und Link zur Webseite.
            Alle Veranstaltungen die mit der Gruppe verknüpft sind, werden auch dort angezeigt,
            nachdem sie stattgefunden haben. Nur du als Ersteller*in und von dir freigegebene
            User*innen können Veranstaltungen mit eurer Gruppe verknüpfen.
          </p>
          <p>
            Bitte lege nur die Gruppen an bei denen du aktiv bist und mit denen die Präsenz auf
            EINTOPF abgestimmt ist.
          </p>
          <br />
          <div className="first-step-buttons">
            <Button onClick={() => setStep('createGroup')}>Jetzt Gruppe erstellen</Button>
            <Button onClick={() => setStep('infoPlace')}>Keine Gruppe erstellen</Button>
          </div>
          <BackstageFirstStepsStepsList step={step} />
        </Container>
      )}
      {step === 'createGroup' && (
        <Fragment>
          <Container title="Erste Schritte (2/4) | Gruppe Erstellen">
            <BackstageFormCreate<Group>
              queryCreate={queryGroupCreate}
              onCreate={() => setStep('infoPlace')}
              model="groups"
            >
              <BackstageGroupFormFields />
            </BackstageFormCreate>
          </Container>
        </Fragment>
      )}
      {step === 'infoPlace' && (
        <Container title="Erste Schritte (3/4) | Orte">
          <h3>Erstellen eures Ortes</h3>
          <p>
            Auch Orten, wie z.B. Kulturzentren, Gemeinschaftsgärten o.ä. bieten wir die Möglichkeit
            einer dauerhaften{' '}
            <a target="_blank" href="/orte">
              Präsenz
            </a>
            . Dafür hinterlegst du eine Beschreibung eures Ortes mit Logo und Link zur Webseite.
            Alle Veranstaltungen die mit dem Ort verknüpft sind, werden auch dort angezeigt, nachdem
            sie stattgefunden haben. Orte können von allen User*innen auf EINTOPF mit deren
            Veranstaltungen verknüpft werden.
          </p>
          <p>
            Bitte lege nur die Orte an bei denen du Teil der Organisation bist und mit denen die
            Präsenz auf EINTOPF abgestimmt ist. Sollte der Ort den du anlegen willst bereits auf
            EINTOPF vertreten sein, melde dich bitte bei uns via{' '}
            <a href="mailto:mod@eintopf.info">mod@eintopf.info</a>.
          </p>
          <br />
          <div className="first-step-buttons">
            <Button onClick={() => setStep('createPlace')}>Jetzt Ort erstellen</Button>
            <Button onClick={() => setStep('infoEvent')}>Keinen Ort erstellen</Button>
          </div>
          <BackstageFirstStepsStepsList step={step} />
        </Container>
      )}
      {step === 'createPlace' && (
        <Fragment>
          <Container title="Erste Schritte (3/4)| Ort Erstellen">
            <BackstageFormCreate<Place>
              queryCreate={queryPlaceCreate}
              onCreate={() => setStep('infoEvent')}
              model="places"
            >
              <BackstagePlaceFormFields />
            </BackstageFormCreate>
          </Container>
        </Fragment>
      )}
      {step === 'infoEvent' && (
        <Container title="Erste Schritte (4/4) | Veranstaltungen">
          <h3>Veranstaltungen</h3>
          <p>
            Veranstaltungen sind das Herzstück von EINTOPF. Du kannst Veranstaltungen für deine
            Gruppe oder deinen Ort anlegen oder Veranstaltungen für Gruppen und Orte die (noch)
            nicht auf EINTOPF vertreten sind, du aber der Meinung bist, dass die Veranstaltung hier
            her passen. EINTOPF bietet zwei verschiedene Typen von Veranstaltungen an: einmalige und
            regelmäßige. Einmalige Veranstaltungen können einen Abend, einen ganzen Tag, oder
            mehrere Tage dauern.
          </p>
          <br />
          <h3>Regelmäßige Veranstaltungen</h3>
          <p>
            Regelmäßige Veranstaltungen sind wiederkehrend und du kannst einen individuellen
            Rhythmus definieren, z.B. wöchentlich montags, oder jeden zweiten Dienstag im Monat.
            Außerdem legst du einen Zeitraum von bis zu 6 Monaten fest, für welchen du dann per
            Klick in dieser regelmäßigen Veranstaltung die einzelnen Termine generierst. Diesen Typ
            kannst du später im Backstage Menü erstellen.
          </p>
          <br />
          <div className="first-step-buttons">
            <Button onClick={() => setStep('createEvent')}>Veranstaltung erstellen</Button>
            <Button onClick={() => setStep('nextSteps')}>Keine Veranstaltung erstellen</Button>
          </div>
          <BackstageFirstStepsStepsList step={step} />
        </Container>
      )}
      {step === 'createEvent' && (
        <Fragment>
          <Container title="Erste Schritte (4/4) | Veranstaltung erstellen">
            <BackstageFormCreate<Event>
              queryCreate={queryEventCreate}
              onCreate={() => setStep('nextSteps')}
              model="events"
            >
              <BackstageEventFormFields />
            </BackstageFormCreate>
          </Container>
        </Fragment>
      )}
      {step === 'nextSteps' && (
        <Fragment>
          <Container title="Erste Schritte | Erfolgreich abgeschlossen">
            <p>
              Herzlichen Glückwunsch! Du hast die ersten Schritte auf EINTOPF zurückgelegt. Du
              kannst dich jetzt weiter in der <i>Backstage</i>, dem Verwaltungsbereich von EINTOPF
              umsehen und z.B. weitere Veranstaltungen erstellen.
            </p>
            <p>
              Natürlich freuen wir uns auch, wenn du weitere Gruppen oder Orte einlädst,{' '}
              <a target="_blank" href="/backstage/usage-principles">
                die zu EINTOPF passen
              </a>
              . Oder du kannst weitere Mitglieder deiner Gruppe einladen, damit auch diese für euch
              Veranstaltungen erstellen und verwalten können. Dafür kannst du deine erste Einladung
              nach sieben Tagen verschicken und danach eine weitere alle sieben Tage
            </p>
            <p>
              Bei Fragen und Problemen schaue bitte in den{' '}
              <a target="_blank" href="/backstage/faq">
                FAQ
              </a>{' '}
              nach oder kontaktiere uns via <a href="mailto:mod@eintopf.info">mod@eintopf.info</a>.
            </p>
            <p>Das Team von EINTOPF wünscht euch viel Erfolg mit euren Veranstaltungen!</p>
            <br />
            <Button onClick={() => route('/backstage')}>Zur Backstage</Button>
            <BackstageFirstStepsStepsList step={step} />
          </Container>
        </Fragment>
      )}
    </PageBackstage>
  );
};

export interface BackstageFirstStepsProps {
  step: FirstStepsStep;
}

export const BackstageFirstStepsStepsList: FunctionComponent<BackstageFirstStepsProps> = ({
  step,
}) => {
  let checkGroup,
    checkPlace,
    checkEvent = false;
  if (step === 'infoPlace' || step === 'infoEvent' || step === 'nextSteps') {
    checkGroup = true;
  }
  if (step === 'infoEvent' || step === 'nextSteps') {
    checkPlace = true;
  }
  if (step === 'nextSteps') {
    checkEvent = true;
  }

  return (
    <p>
      <br />
      <ol>
        <li>
          Erstellung Account <Checkmark />
        </li>
        <li>Erstellen deiner Gruppe {checkGroup && <Checkmark />}</li>
        <li>Erstellen deines Ortes {checkPlace && <Checkmark />}</li>
        <li>Erstellen deiner ersten Veranstaltung {checkEvent && <Checkmark />}</li>
      </ol>
    </p>
  );
};

const Checkmark = () => <>&#10004;</>;

export default BackstageFirstStepsPage;
