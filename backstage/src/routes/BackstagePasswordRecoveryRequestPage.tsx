// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { useState } from 'preact/hooks';
import { queryRequestPasswordRecovery } from '../api/passwordrec';
import { useAppData } from '../appData';
import BackstageForm from '../components/BackstageForm';
import BackstageFormInputText from '../components/BackstageFormInputText';
import Button from '../components/Button';
import Container from '../components/Container';
import Grid from '../components/Grid';
import './BackstagePasswordRecoveryRequestPage.css';

export const BackstagePasswordRecoveryRequestPage: FunctionComponent = () => {
  const { apiurl } = useAppData();
  const [success, setSuccess] = useState(false);
  const [loading, setLoading] = useState(false);

  const onSubmit = async ({ email }: { email: string }) => {
    setLoading(true);
    const response = await queryRequestPasswordRecovery(apiurl, { email });
    if (typeof response === 'string') {
      return response;
    }
    setLoading(false);
    setSuccess(true);
    return undefined;
  };

  return (
    <Grid>
      <Grid cols={2}>
        <Container title="Neues Passwort setzten">
          <p>
            Du hast dein Passwort vergessen? Hier kannst du einen Link anfordern um dein Passwort
            zurückzusetzen.
          </p>
          <br />
          {success ? (
            <p>
              Du erhälst in wenigen Minuten eine E-Mail mit einem Link, um ein neues Passwort zu
              setzten
            </p>
          ) : (
            <BackstageForm<{ email: string }> onSubmit={onSubmit} className="PasswordRecoveryForm">
              <BackstageFormInputText field="email" placeholder="E-Mail" />
              <Button type="submit" disabled={loading}>
                Link anfordern
              </Button>
            </BackstageForm>
          )}
        </Container>
      </Grid>
    </Grid>
  );
};

export default BackstagePasswordRecoveryRequestPage;
