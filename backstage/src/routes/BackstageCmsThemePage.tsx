// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { Link } from 'preact-router';
import { themeBaseUrl, ThemeFiletype } from '../api/cms';
import PageBackstage from '../components/PageBackstage';
import Container from '../components/Container';
import useUser from '../hooks/useUser';
import BackstageThemeFileOption from '../components/BackstageThemeFileOption';
import BackstageTabs from '../components/BackstageTabs';
import BackstageThemeMetaOption from '../components/BackstageThemeMetaOption';
import BackstageThemeContentOption from '../components/BackstageThemeContentOption';
import BackstageBackLink from '../components/BackstageBackLink';
import BackstageThemeConfigOption from '../components/BackstageThemeConfigOption';
import './BackstageCmsThemePage.css';

enum ThemeOptions {
  META = 'meta',
  FILE = 'file',
  CONTENT = 'content',
  CONFIG = 'config',
}

const tabsData = [
  {
    id: ThemeOptions.CONTENT,
    name: 'Inhalte',
  },
  {
    id: ThemeOptions.FILE,
    name: 'Dateien',
  },
  {
    id: ThemeOptions.META,
    name: 'Metadaten',
  },
  {
    id: ThemeOptions.CONFIG,
    name: 'Config',
  },
];

export interface BackstageCmsThemePageProps {
  option: string;
  filetype: string;
  filePath: string;
}

export const BackstageCmsThemePage: FunctionComponent<BackstageCmsThemePageProps> = ({
  option = ThemeOptions.CONTENT,
  filetype,
  filePath,
}) => {
  const { user } = useUser();

  return (
    <PageBackstage>
      <Container className="Theme-Page" title="CMS - Theme">
        <BackstageBackLink title="Zurück zur Themeübersicht" href="/backstage/cms" />
        <div className="Theme-Page-Description">
          <p>
            Hier kannst du dein Theme bearbeiten und Code und Inhalte anpassen.
            <br /> Wähle dazu eine der folgenden Kategorien:
          </p>
        </div>
        <BackstageTabs>
          {tabsData.map((data, i) => {
            return (
              <Link
                key={`${data}-${i}`}
                className={`${option.toLowerCase() === data.id ? 'active' : ''}`}
                // @ts-ignore: This has worked and should absolutely work
                href={`${themeBaseUrl}/${data.id}`}
              >
                {data.name}
              </Link>
            );
          })}
        </BackstageTabs>
        {option.toLowerCase() === ThemeOptions.CONTENT && (
          <BackstageThemeContentOption filepath={filetype} />
        )}
        {option.toLowerCase() === ThemeOptions.FILE && (
          <BackstageThemeFileOption filetype={filetype as ThemeFiletype} filePath={filePath} />
        )}
        {option.toLowerCase() === ThemeOptions.META && <BackstageThemeMetaOption user={user} />}
        {option.toLowerCase() === ThemeOptions.CONFIG && <BackstageThemeConfigOption user={user} />}
      </Container>
    </PageBackstage>
  );
};

export default BackstageCmsThemePage;
