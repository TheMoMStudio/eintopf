// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import BackstageFAQ from '../components/BackstageFAQ';
import Container from '../components/Container';
import PageBackstage from '../components/PageBackstage';

export const BackstageFAQPage: FunctionComponent = () => (
  <PageBackstage>
    <Container title="Backstage FAQ">
      <BackstageFAQ />
    </Container>
  </PageBackstage>
);

export default BackstageFAQPage;
