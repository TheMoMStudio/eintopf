// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { queryTopicGet, queryTopicUpdate, Topic } from '../api/topic';
import BackstageFormUpdate from '../components/BackstageFormUpdate';
import Container from '../components/Container';
import PageBackstage from '../components/PageBackstage';
import BackstageFormInputText from '../components/BackstageFormInputText';
import BackstageFormInputTextLong from '../components/BackstageFormInputTextLong';

export interface BackstageTopicUpdatePageProps {
  id: string;
}

export const BackstageTopicUpdatePage: FunctionComponent<BackstageTopicUpdatePageProps> = ({
  id,
}) => (
  <PageBackstage>
    <Container title="Thema Bearbeiten">
      <BackstageFormUpdate<Topic>
        id={id}
        queryUpdate={queryTopicUpdate}
        queryGet={queryTopicGet}
        model="categories"
      >
        <BackstageFormInputText field="name" label="Name" required />
        <BackstageFormInputText field="headline" label="Überschrift" required />
        <BackstageFormInputTextLong field="description" label="Beschreibung" />
      </BackstageFormUpdate>
    </Container>
  </PageBackstage>
);

export default BackstageTopicUpdatePage;
