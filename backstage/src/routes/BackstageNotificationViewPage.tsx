// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { FunctionComponent, h } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import { queryNotificationGet, Notification, queryNotificationUpdate } from '../api/notification';
import { format, newDate } from '../utils/date';
import { useAppData } from '../appData';
import Container from '../components/Container';
import Link from '../components/Link';
import PageBackstage from '../components/PageBackstage';
import useUser from '../hooks/useUser';

export interface BackstageNotificationViewPageProps {
  id: string;
}

export const BackstageNotificationViewPage: FunctionComponent<
  BackstageNotificationViewPageProps
> = ({ id }) => {
  const { user } = useUser();
  const { apiurl, timezone } = useAppData();

  const [notification, setValues] = useState<Notification | undefined>(undefined);
  useEffect(() => {
    const query = async () => {
      if (!user) return;

      const response = await queryNotificationGet(apiurl, user.token, id);
      if (typeof response === 'string') return;
      setValues(response);
      if (!response.viewed) {
        queryNotificationUpdate(apiurl, user.token, {
          ...response,
          viewed: true,
        });
      }
    };
    query();
  }, [id, user, apiurl]);

  return (
    <PageBackstage>
      {notification && (
        <Container
          title={notification.subject}
          titleRight={format(
            timezone,
            newDate(timezone, notification.createdAt),
            'dd.MM.yyyy HH:mm',
          )}
        >
          {notification.message}
          {notification.linkTitle !== '' && notification.linkUrl !== '' && (
            <p>
              <br />
              <Link href={notification.linkUrl} highlight={true}>
                {notification.linkTitle}
              </Link>
            </p>
          )}
        </Container>
      )}
    </PageBackstage>
  );
};
export default BackstageNotificationViewPage;
