// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { queryPlaceCreate, Place } from '../api/place';
import Container from '../components/Container';
import BackstageFormCreate from '../components/BackstageFormCreate';
import PageBackstage from '../components/PageBackstage';
import { BackstagePlaceFormFields } from '../components/BackstagePlaceForm';

export const BackstagePlaceCreatePage: FunctionComponent = () => (
  <PageBackstage>
    <Container title="Ort Erstellen">
      <BackstageFormCreate<Place> queryCreate={queryPlaceCreate} model="places">
        <BackstagePlaceFormFields />
      </BackstageFormCreate>
    </Container>
  </PageBackstage>
);
export default BackstagePlaceCreatePage;
