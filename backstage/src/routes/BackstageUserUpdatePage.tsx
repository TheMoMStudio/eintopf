// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { Role, RoleAdmin, RoleModerator, RoleNormal } from '../api/auth';
import { queryUserGet, queryUserUpdate, User } from '../api/user';
import Container from '../components/Container';
import BackstageFormUpdate from '../components/BackstageFormUpdate';
import BackstageFormInputCheckbox from '../components/BackstageFormInputCheckbox';
import BackstageFormInputSelect from '../components/BackstageFormInputSelect';
import BackstageFormInputText from '../components/BackstageFormInputText';
import PageBackstage from '../components/PageBackstage';

export interface BackstageUserUpdatePageProps {
  id: string;
}

export const BackstageUserUpdatePage: FunctionComponent<BackstageUserUpdatePageProps> = ({
  id,
}) => (
  <PageBackstage>
    <Container title="Benutzer*in Bearbeiten">
      <BackstageFormUpdate<User>
        id={id}
        queryUpdate={queryUserUpdate}
        queryGet={queryUserGet}
        model="users"
      >
        <BackstageFormInputCheckbox
          field="deactivated"
          label="Deaktiviert"
          needsRole={(role: Role) => role === RoleAdmin || role === RoleModerator}
        />
        <BackstageFormInputText field="nickname" label="Spitzname" />
        <BackstageFormInputText field="email" label="E-Mail" />
        <BackstageFormInputSelect
          field="role"
          label="Rolle"
          needsRole={(role: Role) => role === RoleAdmin || role === RoleModerator}
          options={[
            { value: RoleNormal, label: 'Normal' },
            { value: RoleModerator, label: 'Moderator*in' },
            { value: RoleAdmin, label: 'Admin', needsRole: RoleAdmin },
          ]}
        />
      </BackstageFormUpdate>
    </Container>
  </PageBackstage>
);

export default BackstageUserUpdatePage;
