// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { Role, RoleAdmin, RoleModerator } from '../api/auth';
import { queryPlaceCrud, Place } from '../api/place';
import BackstageList from '../components/BackstageList';
import BackstageListField from '../components/BackstageListField';
import BackstageListFilterText from '../components/BackstageListFilterText';
import BackstageListFilterToggle from '../components/BackstageListFilterToggle';
import PageBackstage from '../components/PageBackstage';
import useBackstageConfig from '../hooks/useBackstageConfig';
import {
  BackstageActionButtonDelete,
  BackstageActionButtonEdit,
  BackstageActionButtonView,
} from '../components/BackstageActionButtons';

export const BackstagePlaceListPage: FunctionComponent = () => {
  const { getValue, setValue } = useBackstageConfig();

  const initialOwnedByOn: boolean = getValue<boolean>(
    'placeList/ownedByOn',
    'boolean',
    true,
  ) as boolean;
  const initialPageSize: number = getValue<number>('placeList/pageSize', 'number', 25) as number;
  const initialSearch = getValue<string>('placeList/search', 'string', undefined);

  return (
    <PageBackstage>
      <BackstageList
        title="Orte"
        noItemsInfo="Keine Orte vorhanden"
        bulkDeleteConfirmMessage="Willst du wirklich alle ausgewählten Orte löschen?"
        queryCrud={queryPlaceCrud}
        config={{
          pageSize: initialPageSize,
          initialSorting: 'name',
          initialOrder: 'DESC',
          baseFilters: {
            ownedBy: initialOwnedByOn ? ['self'] : undefined,
            likeName: initialSearch,
          },
        }}
        filters={[
          <BackstageListFilterToggle
            key="ownedByFilter"
            field="ownedBy"
            needsRole={(role: Role) => role === RoleAdmin || role === RoleModerator}
            initialState={initialOwnedByOn}
            onChange={(on) => setValue('placeList/ownedByOn', on)}
            value={(on) => (on ? ['self'] : undefined)}
            label="Nur eigene Anzeigen"
          />,
          <BackstageListFilterText
            key="searchFilter"
            field="likeName"
            initialState={initialSearch || ''}
            onChange={(v) => setValue('placeList/search', v)}
            label="Suche"
          />,
        ]}
        onPageSizeChange={(pageSize) => setValue('placeList/pageSize', pageSize)}
      >
        <BackstageListField<Place> field="name" name="Name" display="text" />
        <BackstageListField<Place> field="published" name="Veröffentlicht" display="boolean" />
        <BackstageListField
          field="deactivated"
          name="Deaktiviert"
          display="boolean"
          needsRole={(role: Role) => role === RoleAdmin || role === RoleModerator}
        />
        <BackstageActionButtonEdit<Place> context="places" />
        <BackstageActionButtonView<Place> context="ort" />
        <BackstageActionButtonDelete<Place> />
      </BackstageList>
    </PageBackstage>
  );
};

export default BackstagePlaceListPage;
