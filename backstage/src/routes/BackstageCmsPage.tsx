// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import PageBackstage from '../components/PageBackstage';
import Container from '../components/Container';
import BackstageThemeSwitcher from '../components/BackstageThemeSwitcher';
import useUser from '../hooks/useUser';
import './BackstageCmsPage.css';

export const BackstageCmsPage: FunctionComponent = () => {
  const { user } = useUser();

  return (
    <PageBackstage>
      <Container title="CMS">
        <p className="CmsPage-Description">
          In diesem Teil des Kalenders kannst du Inhalte oder das Aussehen verändern. Erstelle dazu
          ein neues Theme oder bearbeite deine bereits erstellten Themes.
        </p>
        {user && <BackstageThemeSwitcher user={user} />}
      </Container>
    </PageBackstage>
  );
};

export default BackstageCmsPage;
