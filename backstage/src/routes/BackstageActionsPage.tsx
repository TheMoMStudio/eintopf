// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { queryActionCrud, Action, queryActionRun } from '../api/action';
import PageBackstage from '../components/PageBackstage';
import BackstageList from '../components/BackstageList';
import useBackstageConfig from '../hooks/useBackstageConfig';
import BackstageListField from '../components/BackstageListField';
import BackstageListAction from '../components/BackstageListAction';
import { useAppData } from '../appData';

export const BackstageActionsPage: FunctionComponent = () => {
  const { apiurl } = useAppData();
  const { getValue, setValue } = useBackstageConfig();
  const initialPageSize: number = getValue<number>('actionsList/pageSize', 'number', 25) as number;

  return (
    <PageBackstage>
      <BackstageList
        idField="name"
        title="Actions"
        noItemsInfo="Keine Actions vorhanden"
        queryCrud={queryActionCrud}
        bulkDeleteConfirmMessage=""
        config={{
          pageSize: initialPageSize,
          initialSorting: 'name',
          initialOrder: 'DESC',
          baseFilters: {},
        }}
        filters={[]}
        onPageSizeChange={(pageSize) => setValue('actionsList/pageSize', pageSize)}
        hideBulkActions={true}
      >
        <BackstageListField<Action> field="name" name="Name" display="text" />
        <BackstageListField<Action> field="calls" name="Aufrufe" display="text" />
        <BackstageListField<Action> field="lastCall" name="Letzter Aufruf" display="datetime" />
        <BackstageListField<Action> field="error" name="Letzer Fehler" display="text" />
        <BackstageListField<Action> field="lastCalledBy" name="Letzter Aufruf von" display="text" />
        <BackstageListAction<Action>
          action={async ({ refresh, token, item }) => {
            await queryActionRun(apiurl, token, item['name'] as string);
            refresh();
          }}
          render={() => <button>Call</button>}
        />
      </BackstageList>
    </PageBackstage>
  );
};

export default BackstageActionsPage;
