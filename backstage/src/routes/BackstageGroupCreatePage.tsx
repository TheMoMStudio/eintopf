// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { queryGroupCreate, Group } from '../api/group';
import BackstageFormCreate from '../components/BackstageFormCreate';
import { BackstageGroupFormFields } from '../components/BackstageGroupForm';
import Container from '../components/Container';
import PageBackstage from '../components/PageBackstage';

export const BackstageGroupCreatePage: FunctionComponent = () => (
  <PageBackstage>
    <Container title="Gruppe Erstellen">
      <BackstageFormCreate<Group> queryCreate={queryGroupCreate} model="groups">
        <BackstageGroupFormFields />
      </BackstageFormCreate>
    </Container>
  </PageBackstage>
);
export default BackstageGroupCreatePage;
