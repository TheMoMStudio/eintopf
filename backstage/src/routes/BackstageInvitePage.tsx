// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent, Fragment } from 'preact';
import { useMemo } from 'preact/hooks';
import { Role, RoleNormal, RoleModerator, RoleAdmin } from '../api/auth';
import { queryInvitationCrud, Invitation } from '../api/invitation';
import BackstageInviteButton, { createInvitationLink } from '../components/BackstageInviteButton';
import BackstageList from '../components/BackstageList';
import BackstageListField from '../components/BackstageListField';
import BackstageListFieldReferenceUser from '../components/BackstageListFieldReferenceUser';
import BackstageListFilterToggle from '../components/BackstageListFilterToggle';
import Container from '../components/Container';
import Link from '../components/Link';
import PageBackstage from '../components/PageBackstage';
import useUser from '../hooks/useUser';
import useBackstageConfig from '../hooks/useBackstageConfig';
import './BackstageInvitePage.css';
import { BackstageActionButtonDelete } from '../components/BackstageActionButtons';

export const BackstageInvitePage: FunctionComponent = () => {
  const { getValue, setValue } = useBackstageConfig();
  const { user } = useUser();

  const initialOwnedByOn: boolean = useMemo(() => {
    const ownedByOn = getValue<boolean | undefined>('invitePage/ownedByOn', 'boolean');
    if (ownedByOn === undefined) {
      return true;
    }
    return ownedByOn;
  }, [getValue]);

  const initialPageSize: number = useMemo(() => {
    const pageSize = getValue<number | undefined>('invitePage/pageSize', 'number');
    if (pageSize === undefined) {
      return 25;
    }
    return pageSize;
  }, [getValue]);

  if (!user) return <Fragment />;

  return (
    <PageBackstage>
      <Container title="Benutzer*in Einladen">
        <p>
          Dir gefällt der Eintopf und du kennst tolle Menschen, die auch gerne Termine auf ihm
          veröffentlichen würden? Schön! Deshalb kannst du hier Einladungslinks erstellen, die du
          gerne weiterleiten darfst.
        </p>
        <br />
        <p>
          Doch Obacht! Bitte leite die Codes auch nur wirklich den Menschen weiter, die damit
          verantwortungsvoll umgehen, keinen Schindluder treiben und nicht irgendeinen Scheiß
          veröffentlichen. Was wir nicht wollen liest du auf der{' '}
          <Link highlight href="/about">
            ÜBER EINTOPF
          </Link>{' '}
          Seite.
        </p>
        {user.role === RoleNormal && (
          <p>
            <br />
            Einladungslinks können erst eine Woche nach den Registrierung erstellt werden.
            <br />
            Danach kann höchstens ein Einladungslink pro Woche erstellt werden.
            <br />
            Falls du mehrere Menschen gleichzeitig einladen möchtest, melde dich bitte bei der
            Moderation unter mod@eintopf.info
          </p>
        )}
        <br />
        <BackstageInviteButton />
      </Container>
      <BackstageList<Invitation>
        title="Einladungen"
        bulkDeleteConfirmMessage="Willst du wirklich alle ausgewählten Einladungslinks löschen?"
        queryCrud={queryInvitationCrud}
        config={{
          pageSize: initialPageSize,
          initialSorting: 'createdAt',
          initialOrder: 'DESC',
        }}
        filters={[
          <BackstageListFilterToggle
            key="createdByFilter"
            field="createdBy"
            needsRole={(role: Role) => role === RoleAdmin || role === RoleModerator}
            initialState={initialOwnedByOn}
            onChange={(on) => setValue('invitePage/ownedByOn', on)}
            value={(on) => (on ? user.id : undefined)}
            label="Nur eigene Anzeigen"
          />,
        ]}
        onPageSizeChange={(pageSize) => setValue('invitePage/pageSize', pageSize)}
        hideBulkActions
      >
        <BackstageListField<Invitation>
          field="token"
          name="Link"
          display="custom"
          render={(token) => (
            <span className="InviteList-InviteLink">{createInvitationLink(token)}</span>
          )}
        />
        <BackstageListField<Invitation> field="createdAt" name="Erstellt Am" display="datetime" />
        <BackstageListFieldReferenceUser
          field="createdBy"
          name="Erstellt von"
          needsRole={(role: Role) => role === RoleAdmin || role === RoleModerator}
        />
        <BackstageListFieldReferenceUser field="usedBy" name="Benutzt von" />
        <BackstageActionButtonDelete />
      </BackstageList>
    </PageBackstage>
  );
};

export default BackstageInvitePage;
