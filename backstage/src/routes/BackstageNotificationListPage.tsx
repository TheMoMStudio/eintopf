// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import PageBackstage from '../components/PageBackstage';
import useBackstageConfig from '../hooks/useBackstageConfig';
import BackstageNotificationList from '../components/BackstageNotificationList';

export const BackstageNotificationsPage: FunctionComponent = () => {
  const { getValue, setValue } = useBackstageConfig();
  const initialPageSize: number = getValue<number>(
    'notificationsList/pageSize',
    'number',
    25,
  ) as number;

  return (
    <PageBackstage>
      <BackstageNotificationList
        pageSize={initialPageSize}
        baseFilters={{}}
        onPageSizeChange={(pageSize) => setValue('notificationsList/pageSize', pageSize)}
      />
    </PageBackstage>
  );
};

export default BackstageNotificationsPage;
