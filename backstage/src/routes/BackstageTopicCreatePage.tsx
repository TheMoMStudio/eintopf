// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { Topic, queryTopicCreate } from '../api/topic';
import Container from '../components/Container';
import BackstageFormCreate from '../components/BackstageFormCreate';
import PageBackstage from '../components/PageBackstage';
import BackstageFormInputText from '../components/BackstageFormInputText';
import BackstageFormInputTextLong from '../components/BackstageFormInputTextLong';

export const BackstagePlaceCreatePage: FunctionComponent = () => (
  <PageBackstage>
    <Container title="Thema Erstellen">
      <BackstageFormCreate<Topic> queryCreate={queryTopicCreate} model="categories">
        <BackstageFormInputText field="name" label="Name" required />
        <BackstageFormInputText field="headline" label="Überschrift" required />
        <BackstageFormInputTextLong field="description" label="Beschreibung" />
      </BackstageFormCreate>
    </Container>
  </PageBackstage>
);
export default BackstagePlaceCreatePage;
