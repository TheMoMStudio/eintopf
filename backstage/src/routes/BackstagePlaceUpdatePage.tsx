// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { Role, RoleAdmin, RoleModerator, RoleNormal } from '../api/auth';
import { queryPlaceGet, queryPlaceUpdate, Place } from '../api/place';
import BackstageFormInputCheckbox from '../components/BackstageFormInputCheckbox';
import BackstageFormNotice from '../components/BackstageFormNotice';
import BackstageFormUpdate from '../components/BackstageFormUpdate';
import { BackstagePlaceFormFields } from '../components/BackstagePlaceForm';
import Container from '../components/Container';
import PageBackstage from '../components/PageBackstage';

const deactivatedMessage =
  'Der Ort wurde von der Moderation deaktiviert. Für mehr Informationen melde dich bitte bei der Moderation (mod@eintopf.info). Solange der Ort deaktiviert ist, wird er nicht auf eintopf.info angezeigt.';

export interface BackstagePlaceUpdatePageProps {
  id: string;
}

export const BackstagePlaceUpdatePage: FunctionComponent<BackstagePlaceUpdatePageProps> = ({
  id,
}) => (
  <PageBackstage>
    <Container title="Ort Bearbeiten">
      <BackstageFormUpdate<Place>
        id={id}
        queryUpdate={queryPlaceUpdate}
        queryGet={queryPlaceGet}
        model="places"
      >
        <BackstageFormNotice
          field="deactivated"
          message={(deactivated) => (deactivated ? deactivatedMessage : false)}
          needsRole={(role: Role) => role === RoleNormal}
        />
        <BackstageFormInputCheckbox
          field="deactivated"
          label="Deaktiviert"
          needsRole={(role: Role) => role === RoleAdmin || role === RoleModerator}
        />
        <BackstagePlaceFormFields />
      </BackstageFormUpdate>
    </Container>
  </PageBackstage>
);

export default BackstagePlaceUpdatePage;
