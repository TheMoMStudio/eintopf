// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import { NewNotification, Notification, queryNotificationCreate } from '../api/notification';
import BackstageFormCreate from '../components/BackstageFormCreate';
import BackstageFormInputText from '../components/BackstageFormInputText';
import BackstageFormInputTextLong from '../components/BackstageFormInputTextLong';
import Container from '../components/Container';
import PageBackstage from '../components/PageBackstage';
import './BackstageRepeatingEventCreatePage.css';

export interface BackstageNotificationCreatePageProps {
  userId: string;
}
export const BackstageNotificationCreatePage: FunctionComponent<
  BackstageNotificationCreatePageProps
> = ({ userId }) => (
  <PageBackstage>
    <Container title="Nachricht Senden">
      <BackstageFormCreate<Notification, NewNotification>
        queryCreate={(apiurl, token: string, notification: NewNotification) => {
          return queryNotificationCreate(apiurl, token, notification);
        }}
        model="notifications"
      >
        <BackstageFormInputText field="userId" label="User" defaultValue={userId} required />
        <BackstageFormInputText field="subject" label="Betreff" required />
        <BackstageFormInputTextLong field="message" label="Nachricht" required />
      </BackstageFormCreate>
    </Container>
  </PageBackstage>
);
export default BackstageNotificationCreatePage;
