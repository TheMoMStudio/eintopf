// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { h, FunctionComponent } from 'preact';
import PageBackstage from '../components/PageBackstage';
import BackstageContainerEventList from '../components/BackstageContainerEventList';

export const BackstageEventListPage: FunctionComponent = () => (
  <PageBackstage>
    <BackstageContainerEventList />
  </PageBackstage>
);
export default BackstageEventListPage;
