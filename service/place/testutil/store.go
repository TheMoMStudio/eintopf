//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package testutil

import (
	"context"
	"testing"

	"eintopf.info/internal/crud"
	"eintopf.info/service/place"
)

type newStore func() (place.Storer, func(), error)

// TestStore tests if a given place.Service acts as a place store.
func TestStore(t *testing.T, newStore newStore) {
	ctx := context.Background()
	t.Helper()
	t.Run("FindNoMatch", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		place, err := store.FindByID(ctx, "test")
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if place != nil {
			tt.Fatalf("expected to place to be nil. got %v", place)
		}
	})

	t.Run("CreateFindByID", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		place1, _ := store.Create(ctx, &place.NewPlace{Name: "place1"})
		place, err := store.FindByID(ctx, place1.ID)
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if place.ID != place1.ID || place.Name != place1.Name {
			tt.Fatalf("expected to find place1. got %v. want %v", place, place1)
		}
	})

	t.Run("CreateFind", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		_, err = store.Create(ctx, &place.NewPlace{Name: "place1"})
		if err != nil {
			tt.Fatalf("store.Create: %s", err)
		}
		_, err = store.Create(ctx, &place.NewPlace{Name: "place2"})
		if err != nil {
			tt.Fatalf("store.Create: %s", err)
		}
		places, totalPlaces, err := store.Find(ctx, nil)
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(places) != 2 {
			tt.Fatalf("expected to recieve two places. got %d", len(places))
		}
		if totalPlaces != 2 {
			tt.Fatalf("expected to recieve a total of two places. got %d", totalPlaces)
		}
	})

	t.Run("CreateFindFilter", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		place1, _ := store.Create(ctx, &place.NewPlace{Name: "place1"})
		store.Create(ctx, &place.NewPlace{Name: "place2"})
		places, totalPlaces, err := store.Find(ctx, &crud.FindParams[place.FindFilters]{
			Filters: &place.FindFilters{Name: &place1.Name},
		})
		if err != nil {
			tt.Fatalf("expected store.FindAll to succeed. got %s", err.Error())
		}
		if len(places) != 1 {
			tt.Fatalf("expected to recieve one place. got %d", len(places))
		}
		if totalPlaces != 1 {
			tt.Fatalf("expected to recieve a total of one place. got %d", totalPlaces)
		}
		if places[0].ID != place1.ID {
			tt.Fatalf("expected to find place1. got %v. want %v", places[0], place1)
		}
	})

	t.Run("CreateFindSortedASC", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		place1, _ := store.Create(ctx, &place.NewPlace{Name: "c place1"})
		place2, _ := store.Create(ctx, &place.NewPlace{Name: "a place2"})
		place3, _ := store.Create(ctx, &place.NewPlace{Name: "b place3"})
		places, totalPlaces, err := store.Find(ctx, &crud.FindParams[place.FindFilters]{
			Sort:  "name",
			Order: crud.OrderAsc,
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(places) != 3 {
			tt.Fatalf("expected to find three place. got %d", len(places))
		}
		if totalPlaces != 3 {
			tt.Fatalf("expected to recieve a total of three places. got %d", totalPlaces)
		}
		if places[0].ID != place2.ID {
			tt.Fatalf("expected to place[0] to be place2 . got %v", places[0])
		}
		if places[1].ID != place3.ID {
			tt.Fatalf("expected to place[1] to be place3 . got %v", places[1])
		}
		if places[2].ID != place1.ID {
			tt.Fatalf("expected to place[2] to be place1 . got %v", places[2])
		}
	})

	t.Run("CreateFindSortedDESC", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		place1, _ := store.Create(ctx, &place.NewPlace{Name: "c place1"})
		place2, _ := store.Create(ctx, &place.NewPlace{Name: "a place2"})
		place3, _ := store.Create(ctx, &place.NewPlace{Name: "b place3"})
		places, totalPlaces, err := store.Find(ctx, &crud.FindParams[place.FindFilters]{
			Sort:  "name",
			Order: crud.OrderDesc,
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(places) != 3 {
			tt.Fatalf("expected to find three place. got %d", len(places))
		}
		if totalPlaces != 3 {
			tt.Fatalf("expected to recieve a total of three places. got %d", totalPlaces)
		}
		if places[0].ID != place1.ID {
			tt.Fatalf("expected to place[0] to be place1 . got %v", places[0])
		}
		if places[1].ID != place3.ID {
			tt.Fatalf("expected to place[1] to be place3 . got %v", places[1])
		}
		if places[2].ID != place2.ID {
			tt.Fatalf("expected to place[2] to be place2 . got %v", places[2])
		}
	})

	t.Run("CreateFindPaginated", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		store.Create(ctx, &place.NewPlace{Name: "place1"})
		place2, _ := store.Create(ctx, &place.NewPlace{Name: "place2"})
		store.Create(ctx, &place.NewPlace{Name: "place3"})
		places, totalPlaces, err := store.Find(ctx, &crud.FindParams[place.FindFilters]{
			Limit:  1,
			Offset: 1,
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(places) != 1 {
			tt.Fatalf("expected to find one place. got %d", len(places))
		}
		if totalPlaces != 3 {
			tt.Fatalf("expected to recieve a total of three places. got %d", totalPlaces)
		}
		if places[0].ID != place2.ID || places[0].Name != place2.Name {
			tt.Fatalf("expected to find place2. got %v. want %v", places[0], place2)
		}
	})

	t.Run("CreateFindPaginatedAndSorted", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		place1, _ := store.Create(ctx, &place.NewPlace{Name: "c place1"})
		store.Create(ctx, &place.NewPlace{Name: "a place2"})
		store.Create(ctx, &place.NewPlace{Name: "b place3"})
		places, totalPlaces, err := store.Find(ctx, &crud.FindParams[place.FindFilters]{
			Limit:  1,
			Offset: 2,
			Sort:   "name",
			Order:  crud.OrderAsc,
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(places) != 1 {
			tt.Fatalf("expected to find one place. got %d", len(places))
		}
		if totalPlaces != 3 {
			tt.Fatalf("expected to recieve a total of three places. got %d", totalPlaces)
		}
		if places[0].ID != place1.ID {
			tt.Fatalf("expected to place[0] to be place1 . got %v", places[0])
		}
	})

	t.Run("CreateUpdate", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		place1, _ := store.Create(ctx, &place.NewPlace{Name: "place3"})
		place1.Name = "place3.1"

		_, err = store.Update(ctx, place1)
		if err != nil {
			tt.Fatalf("expected store.Update to succeed. got %s", err.Error())
		}

		place, err := store.FindByID(ctx, place1.ID)
		if place.Name != "place3.1" {
			tt.Fatalf("expected place.Name to be 'place3.1. got %s", place.Name)
		}
	})

	t.Run("CreateDelete", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		place1, _ := store.Create(ctx, &place.NewPlace{Name: "place1"})
		err = store.Delete(ctx, place1.ID)
		if err != nil {
			tt.Fatalf("expected store.Delete to succeed. got %s", err.Error())
		}
		place, err := store.FindByID(ctx, place1.ID)
		if place != nil {
			tt.Fatal("expected place1 to be deleted")
		}
	})
}
