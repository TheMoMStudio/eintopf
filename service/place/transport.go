//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package place

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/crud"
	"eintopf.info/internal/xhttp"
	"eintopf.info/service/auth"
)

// Router returns a new http router that handles crud requests on the place
// model, that get forwarded to the given place service.
func Router(service Service, authService auth.Service) func(chi.Router) {
	server := &server{service}
	return func(r chi.Router) {
		r.Options("/", xhttp.CorsHandler)

		// swagger:route GET /places/ place findPlaces
		//
		// Retrieves all places.
		//
		// Parameters:
		//   + name: offset
		//     description: offset in event list
		//     in: query
		//     type: integer
		//     required: false
		//   + name: limit
		//     description: limits the event list
		//     in: query
		//     type: integer
		//     required: false
		//   + name: sort
		//     description: field that gets sorted
		//     in: query
		//     type: string
		//     required: false
		//   + name: order
		//     description: sort order ("ASC" or "DESC")
		//     in: query
		//     type: string
		//     required: false
		//   + name: filters
		//     description: filters get combined with AND logic
		//     in: query
		//     type: object
		//     required: false
		//
		//     Responses:
		//       200: findPlacesResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: false})).
			Get("/", server.find)

		// swagger:route POST /places/ place createPlace
		//
		// Creates a new place with the given data.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: createPlaceResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Post("/", server.create)

		r.Options("/{id}", xhttp.CorsHandler)

		// swagger:route GET /places/{id} place findPlace
		//
		// Finds the place with the given id.
		//
		//     Responses:
		//       200: findPlaceResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.Get("/{id}", server.findByID)

		// swagger:route PUT /places/{id} place updatePlace
		//
		// Updates the place with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: updatePlaceResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Put("/{id}", server.update)

		// swagger:route DELETE /places/{id} place deletePlace
		//
		// Deletes the place with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: deletePlaceResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Delete("/{id}", server.delete)
	}
}

type server struct {
	service Service
}

func (s *server) find(w http.ResponseWriter, r *http.Request) {
	params, err := readFindRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	places, total, err := s.service.Find(r.Context(), params)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeFindResponse(w, &findResponse{Places: places, Total: total})
	if err != nil {
		log.Println(err)
		return
	}
}

// swagger:response findPlacesResponse
type findResponse struct {

	// in:body
	Places []*Place

	// in:header
	Total int `json:"x-total-count"`
}

func readFindRequest(r *http.Request) (*crud.FindParams[FindFilters], error) {
	params := &crud.FindParams[FindFilters]{}
	var err error

	params.Offset, err = xhttp.ReadQueryInt64(r, "offset")
	if err != nil {
		return nil, err
	}
	params.Limit, err = xhttp.ReadQueryInt64(r, "limit")
	if err != nil {
		return nil, err
	}

	params.Sort, err = xhttp.ReadQueryString(r, "sorting")
	if err != nil {
		return nil, err
	}
	order, err := xhttp.ReadQueryString(r, "order")
	if err != nil {
		return nil, err
	}
	params.Order = crud.SortOrder(order)

	filters := &FindFilters{}
	err = xhttp.ReadQueryJson(r, "filters", filters)
	if err != nil {
		return nil, err
	}
	params.Filters = filters

	return params, nil
}

func writeFindResponse(w http.ResponseWriter, resp *findResponse) error {
	data, err := json.Marshal(resp.Places)
	if err != nil {
		return err
	}
	w.Header().Add("Access-Control-Expose-Headers", "X-Total-Count")
	w.Header().Add("X-Total-Count", strconv.Itoa(resp.Total))
	w.Write(data)
	return nil
}

func (s *server) create(w http.ResponseWriter, r *http.Request) {
	req, err := readCreateRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	place, err := s.service.Create(r.Context(), req.Place)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeCreateResponse(w, &createResponse{place})
	if err != nil {
		log.Println(err)
		return
	}
}

// swagger:parameters createPlace
type createRequest struct {

	// in:body
	Place *NewPlace
}

// swagger:response createPlaceResponse
type createResponse struct {

	// in:body
	Place *Place
}

func readCreateRequest(r *http.Request) (*createRequest, error) {
	data, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	place := &NewPlace{}
	err = json.Unmarshal(data, place)
	if err != nil {
		return nil, err
	}
	return &createRequest{place}, nil
}

func writeCreateResponse(w http.ResponseWriter, resp *createResponse) error {
	data, err := json.Marshal(resp.Place)
	if err != nil {
		return err
	}
	w.Write(data)
	return nil
}

func (s *server) findByID(w http.ResponseWriter, r *http.Request) {
	req, err := readFindByIDRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	place, err := s.service.FindByID(r.Context(), req.ID)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeFindByIDResponse(w, &findByIDResponse{place})
	if err != nil {
		log.Println(err)
		return
	}
}

// swagger:parameters findPlace
type findByIDRequest struct {
	// in:query
	ID string `json:"id"`
}

// swagger:response findPlaceResponse
type findByIDResponse struct {

	// in:body
	Place *Place
}

func readFindByIDRequest(r *http.Request) (*findByIDRequest, error) {
	id := chi.URLParam(r, "id")
	return &findByIDRequest{id}, nil
}

func writeFindByIDResponse(w http.ResponseWriter, resp *findByIDResponse) error {
	data, err := json.Marshal(resp.Place)
	if err != nil {
		return err
	}
	w.Write(data)
	return nil
}

func (s *server) update(w http.ResponseWriter, r *http.Request) {
	req, err := readUpdateRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	place, err := s.service.Update(r.Context(), req.Place)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeUpdateResponse(w, &updateResponse{place})
	if err != nil {
		log.Println(err)
		return
	}
}

// swagger:parameters updatePlace
type updateRequest struct {

	// in:body
	Place *Place
}

// swagger:response updatePlaceResponse
type updateResponse struct {

	// in:body
	Place *Place
}

func readUpdateRequest(r *http.Request) (*updateRequest, error) {
	data, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	place := &Place{}
	err = json.Unmarshal(data, place)
	if err != nil {
		return nil, err
	}
	return &updateRequest{place}, nil
}

func writeUpdateResponse(w http.ResponseWriter, resp *updateResponse) error {
	data, err := json.Marshal(resp.Place)
	if err != nil {
		return err
	}
	w.Write(data)
	return nil
}

func (s *server) delete(w http.ResponseWriter, r *http.Request) {
	req, err := readDeleteRequest(r)
	if err != nil {

		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	err = s.service.Delete(r.Context(), req.ID)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeDeleteResponse(w)
	if err != nil {
		log.Println(err)
		return
	}
}

// swagger:parameters deletePlace
type deleteRequest struct {

	// in:query
	ID string `json:"id"`
}

// swagger:response deletePlaceResponse
type deleteResponse struct{}

func readDeleteRequest(r *http.Request) (*deleteRequest, error) {
	id := chi.URLParam(r, "id")
	return &deleteRequest{id}, nil
}

func writeDeleteResponse(w http.ResponseWriter) error {
	w.Write([]byte{})
	return nil
}
