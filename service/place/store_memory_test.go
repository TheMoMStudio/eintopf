//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package place_test

import (
	"testing"

	"eintopf.info/service/place"
	"eintopf.info/service/place/testutil"
)

func TestMemoryStore(t *testing.T) {
	testutil.TestStore(t, func() (place.Storer, func(), error) {
		return place.NewMemoryStore(), func() {}, nil
	})
}
