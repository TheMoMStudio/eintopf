//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package place

import (
	"context"
	"fmt"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
)

type Authorizer struct {
	service Service
}

func NewAuthorizer(service Service) *Authorizer {
	return &Authorizer{service}
}

// Create can only be called by loggedin users.
func (a *Authorizer) Create(ctx context.Context, newPlace *NewPlace) (*Place, error) {
	_, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return nil, auth.ErrUnauthorized
	}
	return a.service.Create(ctx, newPlace)
}

// Update can only be called
// - if the loggedin user is in the OwnedBy field
// - if the loggedin user is a moderator or admin
func (a *Authorizer) Update(ctx context.Context, place *Place) (*Place, error) {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return nil, auth.ErrUnauthorized
	}
	if role == auth.RoleNormal {
		userID, err := auth.UserIDFromContext(ctx)
		if err != nil {
			return nil, auth.ErrUnauthorized
		}
		if !place.IsOwned(userID) {
			return nil, auth.ErrUnauthorized
		}
		oldPlace, err := a.service.FindByID(ctx, place.ID)
		if err != nil {
			return nil, err
		}
		if oldPlace == nil {
			return nil, fmt.Errorf("cant find place with id: %s", place.ID)
		}
		if oldPlace.Deactivated != oldPlace.Deactivated {
			return nil, auth.ErrUnauthorized
		}
	}
	return a.service.Update(ctx, place)
}

// Update can only be called
// - if the loggedin user is in the OwnedBy field
// - if the loggedin user is an admin
func (a *Authorizer) Delete(ctx context.Context, id string) error {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return auth.ErrUnauthorized
	}
	if role == auth.RoleNormal || role == auth.RoleModerator {
		userID, err := auth.UserIDFromContext(ctx)
		if err != nil {
			return auth.ErrUnauthorized
		}
		place, err := a.service.FindByID(ctx, id)
		if err != nil {
			return err
		}
		if place == nil {
			return nil
		}
		if !place.IsOwned(userID) {
			return auth.ErrUnauthorized
		}
	}
	return a.service.Delete(ctx, id)
}

func (a *Authorizer) FindByID(ctx context.Context, id string) (*Place, error) {
	return a.service.FindByID(ctx, id)
}

func (a *Authorizer) Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*Place, int, error) {
	return a.service.Find(ctx, params)
}
