//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package place_test

import (
	"testing"

	"eintopf.info/internal/xhttptest"
	"eintopf.info/service/place"
	"eintopf.info/test"
)

func TestRouter(t *testing.T) {
	tests := []xhttptest.HttpTest{
		{
			Name:       "CORS/",
			URI:        "/",
			Method:     "OPTIONS",
			WantStatus: 200,
			WantHeaders: map[string]string{
				"Access-Control-Allow-Methods": "GET,POST,PUT,PATCH,DELETE,OPTIONS",
				"Access-Control-Allow-Headers": "authorization, origin, content-type, accept, if-modified-since",
				"Allow":                        "HEAD,GET,POST,PUT,PATCH,DELETE,OPTIONS",
			},
		}, {
			Name:       "CORS/foo",
			URI:        "/foo",
			Method:     "OPTIONS",
			WantStatus: 200,
			WantHeaders: map[string]string{
				"Access-Control-Allow-Methods": "GET,POST,PUT,PATCH,DELETE,OPTIONS",
				"Access-Control-Allow-Headers": "authorization, origin, content-type, accept, if-modified-since",
				"Allow":                        "HEAD,GET,POST,PUT,PATCH,DELETE,OPTIONS",
			},
		},
	}

	placeService := place.NewMemoryStore()
	authService := test.NewAuthService()
	xhttptest.TestRouter(t, place.Router(placeService, authService), tests)
}
