//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package place_test

import (
	"context"
	"testing"

	"eintopf.info/service/auth"
	"eintopf.info/service/place"
)

var (
	roleAdmin    = auth.RoleAdmin
	roleModertor = auth.RoleModerator
	roleNormal   = auth.RoleNormal
)

func TestAuthorizerCreate(t *testing.T) {
	for _, test := range []struct {
		name      string
		userID    string
		place     *place.NewPlace
		wantError error
	}{
		{
			name:      "UnauthorizedWithNoUserID",
			userID:    "",
			place:     &place.NewPlace{},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "AuthorizedWithUserID",
			userID:    "foo",
			place:     &place.NewPlace{},
			wantError: nil,
		},
	} {
		t.Run(test.name, func(tt *testing.T) {
			ctx := auth.ContextWithID(context.Background(), test.userID)

			store := place.NewMemoryStore()
			authorizer := place.NewAuthorizer(store)

			_, err := authorizer.Create(ctx, test.place)
			if err != test.wantError {
				tt.Errorf("err != test.wantError: %s != %s", err, test.wantError)
			}

			if test.wantError == nil {
				if g, _ := store.FindByID(context.Background(), "0"); g == nil {
					tt.Errorf("place should have been created")
				}
			}
		})
	}
}

func TestAuthorizerUpdate(t *testing.T) {
	for _, test := range []struct {
		name      string
		userID    string
		role      *auth.Role
		place     *place.Place
		wantError error
	}{
		{
			name:      "UnauthorizedWithNoUserIDAndRole",
			userID:    "",
			role:      nil,
			place:     &place.Place{ID: "0"},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "AuthorizedWithAdminRole",
			userID:    "foo",
			role:      &roleAdmin,
			place:     &place.Place{ID: "0", OwnedBy: []string{"bar"}},
			wantError: nil,
		}, {
			name:      "AuthorizedWithModeratorRole",
			userID:    "foo",
			role:      &roleModertor,
			place:     &place.Place{ID: "0", OwnedBy: []string{"bar"}},
			wantError: nil,
		}, {
			name:      "UnauthorizedWithNormalRoleNotOwned",
			userID:    "foo",
			role:      &roleNormal,
			place:     &place.Place{ID: "0", OwnedBy: []string{"bar"}},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "AuthorizedWithNormalRoleOwned",
			userID:    "bar",
			role:      &roleNormal,
			place:     &place.Place{ID: "0", OwnedBy: []string{"bar"}},
			wantError: nil,
		},
	} {
		t.Run(test.name, func(tt *testing.T) {
			ctx := auth.ContextWithID(context.Background(), test.userID)
			if test.role != nil {
				ctx = auth.ContextWithRole(ctx, *test.role)
			}

			store := place.NewMemoryStore()
			_, err := store.Create(ctx, &place.NewPlace{
				Name:    "foo",
				OwnedBy: []string{"bar"},
			})
			if err != nil {
				t.Fatal(err)
			}
			authorizer := place.NewAuthorizer(store)

			p, err := authorizer.Update(ctx, test.place)
			if err != test.wantError {
				tt.Errorf("err != test.wantError: %s != %s", err, test.wantError)
			}

			if test.wantError == nil {
				if p.Name == "foo" {
					tt.Errorf("name should be updated")
				}
			}
		})
	}
}

func TestAuthorizerDelete(t *testing.T) {
	for _, test := range []struct {
		name      string
		userID    string
		role      *auth.Role
		place     *place.Place
		wantError error
	}{
		{
			name:      "UnauthorizedWithNoUserIDAndRole",
			userID:    "",
			role:      nil,
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "AuthorizedWithAdminRole",
			userID:    "foo",
			role:      &roleAdmin,
			wantError: nil,
		}, {
			name:      "UnauthorizedWithModeratorRoleNotOwned",
			userID:    "foo",
			role:      &roleModertor,
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "AuthorizedWithModeratorRoleOwned",
			userID:    "bar",
			role:      &roleModertor,
			wantError: nil,
		}, {
			name:      "UnauthorizedWithNormalRoleNotOwned",
			userID:    "foo",
			role:      &roleNormal,
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "AuthorizedWithNormalRoleOwned",
			userID:    "bar",
			role:      &roleNormal,
			wantError: nil,
		},
	} {
		t.Run(test.name, func(tt *testing.T) {
			ctx := auth.ContextWithID(context.Background(), test.userID)
			if test.role != nil {
				ctx = auth.ContextWithRole(ctx, *test.role)
			}

			store := place.NewMemoryStore()
			_, err := store.Create(ctx, &place.NewPlace{OwnedBy: []string{"bar"}})
			if err != nil {
				t.Fatal(err)
			}
			authorizer := place.NewAuthorizer(store)

			err = authorizer.Delete(ctx, "0")
			if err != test.wantError {
				tt.Errorf("err != test.wantError: %s != %s", err, test.wantError)
			}

			if test.wantError == nil {
				if p, _ := store.FindByID(context.Background(), "0"); p != nil {
					tt.Errorf("place should have be deleted")
				}
			}
		})
	}
}

func TestAuthorizerFind(t *testing.T) {
	for _, test := range []struct {
		name      string
		userID    string
		role      *auth.Role
		place     *place.Place
		wantError error
	}{
		{
			name:      "AuthorizedAll",
			userID:    "",
			role:      nil,
			wantError: nil,
		},
	} {
		t.Run(test.name, func(tt *testing.T) {
			ctx := auth.ContextWithID(context.Background(), test.userID)
			if test.role != nil {
				ctx = auth.ContextWithRole(ctx, *test.role)
			}

			store := place.NewMemoryStore()
			store.Create(context.Background(), &place.NewPlace{})
			authorizer := place.NewAuthorizer(store)

			_, total, err := authorizer.Find(ctx, nil)
			if err != test.wantError {
				tt.Errorf("err != test.wantError: %s != %s", err, test.wantError)
			}

			if test.wantError == nil && total == 0 {
				tt.Errorf("place should have been found")
			}
		})
	}
}

func TestAuthorizerFindByID(t *testing.T) {
	for _, test := range []struct {
		name      string
		userID    string
		role      *auth.Role
		place     *place.Place
		wantError error
	}{
		{
			name:      "AuthorizedAll",
			userID:    "",
			role:      nil,
			wantError: nil,
		},
	} {
		t.Run(test.name, func(tt *testing.T) {
			ctx := auth.ContextWithID(context.Background(), test.userID)
			if test.role != nil {
				ctx = auth.ContextWithRole(ctx, *test.role)
			}

			store := place.NewMemoryStore()
			store.Create(context.Background(), &place.NewPlace{})
			authorizer := place.NewAuthorizer(store)

			p, err := authorizer.FindByID(ctx, "0")
			if err != test.wantError {
				tt.Errorf("err != test.wantError: %s != %s", err, test.wantError)
			}

			if test.wantError == nil && p == nil {
				tt.Errorf("place should have been found")
			}
		})
	}
}
