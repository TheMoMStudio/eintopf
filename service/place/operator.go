//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package place

import (
	"context"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
	"eintopf.info/service/oqueue"
)

type operator struct {
	service Service
	queue   *oqueue.Queue
}

func NewOperator(service Service, queue *oqueue.Queue) Service {
	return &operator{service: service, queue: queue}
}

type CreateOperation struct {
	Place  *Place
	userID string
}

func (o CreateOperation) UserID() string {
	return o.userID
}

func (i *operator) Create(ctx context.Context, newPlace *NewPlace) (*Place, error) {
	place, err := i.service.Create(ctx, newPlace)
	if err != nil {
		return place, err
	}

	if place != nil {
		userID, err := auth.UserIDFromContext(ctx)
		if err != nil {
			return nil, err
		}
		i.queue.AddOperation(CreateOperation{
			Place:  place,
			userID: userID,
		})
	}

	return place, nil
}

type UpdateOperation struct {
	Place  *Place
	userID string
}

func (o UpdateOperation) UserID() string {
	return o.userID
}

func (i *operator) Update(ctx context.Context, place *Place) (*Place, error) {
	place, err := i.service.Update(ctx, place)
	if err != nil {
		return place, err
	}

	if place != nil {
		userID, err := auth.UserIDFromContext(ctx)
		if err != nil {
			return nil, err
		}
		i.queue.AddOperation(UpdateOperation{
			Place:  place,
			userID: userID,
		})
	}

	return place, nil
}

type DeleteOperation struct {
	ID     string
	userID string
}

func (o DeleteOperation) UserID() string {
	return o.userID
}

func (i *operator) Delete(ctx context.Context, id string) error {
	err := i.service.Delete(ctx, id)
	if err != nil {
		return err
	}

	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return err
	}
	i.queue.AddOperation(DeleteOperation{
		ID:     id,
		userID: userID,
	})

	return nil
}

func (i *operator) FindByID(ctx context.Context, id string) (*Place, error) {
	return i.service.FindByID(ctx, id)
}

func (i *operator) Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*Place, int, error) {
	return i.service.Find(ctx, params)
}
