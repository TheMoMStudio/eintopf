//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package place_test

import (
	"testing"

	"eintopf.info/service/place"
	"eintopf.info/test"
)

func TestURLFromID(t *testing.T) {
	cases := []struct {
		baseURL string
		id      string
		url     string
		err     error
	}{
		{baseURL: "https://my.domain", id: "1234", url: "https://my.domain/place/1234"},
	}
	for _, c := range cases {
		url, err := place.URLFromID(c.baseURL, c.id)
		if !test.EqualError(err, c.err) {
			t.Errorf("%s, %s: want error: %s got: %s", c.baseURL, c.id, c.err, err)
		}
		if url != c.url {
			t.Errorf("%s, %s: want url: %s got: %s", c.baseURL, c.id, c.url, url)
		}
	}
}
