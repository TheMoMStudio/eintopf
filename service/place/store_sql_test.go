//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package place_test

import (
	"fmt"
	"testing"

	"eintopf.info/service/dbmigration"
	"eintopf.info/service/place"
	"eintopf.info/service/place/testutil"
	"eintopf.info/test"
)

func TestSqlStore(t *testing.T) {
	testutil.TestStore(t, func() (place.Storer, func(), error) {
		db, cleanup, err := test.CreateSqliteTestDB(t.Name())
		if err != nil {
			return nil, nil, err
		}

		migrationStore, err := dbmigration.NewSqlStore(db)
		if err != nil {
			return nil, cleanup, fmt.Errorf("failed to create migration store: %s", err)
		}
		migrationService := dbmigration.NewService(migrationStore)

		store, err := place.NewSqlStore(db, migrationService)
		if err != nil {
			return nil, cleanup, err
		}
		return store, cleanup, nil
	})
}
