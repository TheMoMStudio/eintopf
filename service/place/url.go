//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package place

import (
	"fmt"
	"net/url"
	"path"
)

// URLFromID takes a base url an a place id and constructs an url pointing to
// the place on the web frontend.
func URLFromID(baseURL string, id string) (string, error) {
	u, err := url.Parse(baseURL)
	if err != nil {
		return "", fmt.Errorf("baseURL: %s", err)
	}
	u.Path = path.Join(u.Path, "/place/", id)
	return u.String(), nil
}
