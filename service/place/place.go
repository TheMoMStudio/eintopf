//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package place

import (
	"context"
	"fmt"
	"strings"

	"eintopf.info/internal/crud"
	"eintopf.info/internal/xerror"
	"eintopf.info/service/auth"
)

// Place defines a place entity
type NewPlace struct {
	Published   bool     `json:"published"`
	Name        string   `json:"name"`
	Address     string   `json:"address"`
	Lat         float64  `json:"lat"`
	Lng         float64  `json:"lng"`
	Link        string   `json:"link"`
	Email       string   `json:"email"`
	Description string   `json:"description"`
	Image       string   `json:"image"`
	Alt         string   `json:"alt"`
	OwnedBy     []string `json:"ownedBy" db:"owned_by"`
}

// IsOwned returns true if the id is in the OwnedBy field.
func (p *NewPlace) IsOwned(id string) bool {
	owned := false
	for _, owner := range p.OwnedBy {
		if owner == id {
			owned = true
		}
	}
	return owned
}

// Place defines a place entity
// It implements indexo.Coppyable.
type Place struct {
	ID          string   `json:"id" db:"id"`
	Deactivated bool     `json:"deactivated" db:"deactivated"`
	Published   bool     `json:"published" db:"published"`
	Name        string   `json:"name" db:"name"`
	Description string   `json:"description" db:"description"`
	Email       string   `json:"email" db:"email"`
	Image       string   `json:"image" db:"image"`
	Alt         string   `json:"alt" db:"alt"`
	Link        string   `json:"link" db:"link"`
	Address     string   `json:"address" db:"address"`
	Lat         float64  `json:"lat" db:"lat"`
	Lng         float64  `json:"lng" db:"lng"`
	OwnedBy     []string `json:"ownedBy" db:"owned_by"`
}

func (p Place) Identifier() string { return p.ID }

func PlaceFromNewPlace(newPlace *NewPlace, id string) *Place {
	return &Place{
		ID:          id,
		Deactivated: false,
		Name:        newPlace.Name,
		Address:     newPlace.Address,
		Lat:         newPlace.Lat,
		Lng:         newPlace.Lng,
		Link:        newPlace.Link,
		Email:       newPlace.Email,
		Description: newPlace.Description,
		Published:   newPlace.Published,
		Image:       newPlace.Image,
		Alt:         newPlace.Alt,
		OwnedBy:     newPlace.OwnedBy,
	}
}

// Indexable indicates wether a place should be added to the search index.
func (p *Place) Indexable() bool {
	return !p.Deactivated
}

// Listable indicates wether a place should be shown on the list page.
func (p *Place) Listable() bool {
	return p.Published
}

// IsOwned returns true if the id is in the OwnedBy field.
func (p *Place) IsOwned(id string) bool {
	owned := false
	for _, owner := range p.OwnedBy {
		if owner == id {
			owned = true
		}
	}
	return owned
}

func (p *Place) Coordinates() (float64, float64) {
	return p.Lat, p.Lng
}

// Service defines the crud service to manage places.
type Service interface {
	Storer
}

// SortOrder defines the order of sorting.
type SortOrder string

// Possible values for SortOrder.
const (
	OrderAsc  = SortOrder("ASC")
	OrderDesc = SortOrder("DESC")
)

// FindFilters defines the possible filters for the find method.
type FindFilters struct {
	ID          *string  `json:"id"`
	NotID       *string  `json:"notID"`
	Deactivated *bool    `json:"deactivated"`
	Published   *bool    `json:"published"`
	Name        *string  `json:"name"`
	LikeName    *string  `json:"likeName"`
	Description *string  `json:"description"`
	OwnedBy     []string `json:"ownedBy"`
}

// Storer defines a service for CRUD operations on the event model.
type Storer interface {
	Create(ctx context.Context, newPlace *NewPlace) (*Place, error)
	Update(ctx context.Context, place *Place) (*Place, error)
	Delete(ctx context.Context, id string) error
	FindByID(ctx context.Context, id string) (*Place, error)
	Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*Place, int, error)
}

// NewService returns a new place service.
func NewService(store Storer) Service {
	return &service{store: store}
}

type service struct {
	store Storer
}

var ErrNameAlreadyExists = xerror.BadInputError{Err: fmt.Errorf("name already exists")}

// Create makes sure the loggedin user is in the owned field.
func (s *service) Create(ctx context.Context, newPlace *NewPlace) (*Place, error) {
	if s.placeNameExists(ctx, newPlace.Name, "") {
		return nil, ErrNameAlreadyExists
	}
	id, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return nil, err
	}
	if !newPlace.IsOwned(id) {
		newPlace.OwnedBy = append(newPlace.OwnedBy, id)
	}
	newPlace.Name = strings.TrimSpace(newPlace.Name)
	return s.store.Create(ctx, newPlace)
}

func (s *service) Update(ctx context.Context, place *Place) (*Place, error) {
	if s.placeNameExists(ctx, place.Name, place.ID) {
		return nil, ErrNameAlreadyExists
	}
	return s.store.Update(ctx, place)
}

func (s *service) Delete(ctx context.Context, id string) error {
	return s.store.Delete(ctx, id)
}

func (s *service) FindByID(ctx context.Context, id string) (*Place, error) {
	return s.store.FindByID(ctx, id)
}

// Find adds a special filter value "self" for the OwnedBy field. If it is set
// the value gets replaced with the id of the loggedin user.
func (s *service) Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*Place, int, error) {
	if params != nil && params.Filters != nil && params.Filters.OwnedBy != nil {
		if len(params.Filters.OwnedBy) == 1 && params.Filters.OwnedBy[0] == "self" {
			id, err := auth.UserIDFromContext(ctx)
			if err == nil {
				params.Filters.OwnedBy = []string{id}
			}
		}
	}
	return s.store.Find(ctx, params)
}

func (s *service) placeNameExists(ctx context.Context, name string, id string) bool {
	existingPlaces, _, err := s.store.Find(auth.ContextWithRole(ctx, auth.RoleInternal), &crud.FindParams[FindFilters]{
		Filters: &FindFilters{Name: &name, NotID: &id},
	})
	if err != nil || len(existingPlaces) > 0 {
		return true
	}
	return false
}
