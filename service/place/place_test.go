//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package place_test

import (
	"context"
	"fmt"
	"testing"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
	"eintopf.info/service/place"
)

func TestPlaceIndexable(t *testing.T) {
	for i, test := range []struct {
		place     *place.Place
		indexable bool
	}{
		{
			place:     &place.Place{Deactivated: true, Published: true},
			indexable: false,
		}, {
			place:     &place.Place{Deactivated: false, Published: false},
			indexable: true,
		}, {
			place:     &place.Place{Deactivated: false, Published: true},
			indexable: true,
		},
	} {
		t.Run(fmt.Sprintf("%d", i), func(tt *testing.T) {
			if test.place.Indexable() != test.indexable {
				tt.Errorf("%d: Deactivated: %t Published: %t -> Indexable %t", i, test.place.Deactivated, test.place.Published, test.place.Indexable())
			}
		})
	}
}

func TestPlaceListable(t *testing.T) {
	for i, test := range []struct {
		place    *place.Place
		listable bool
	}{
		{
			place:    &place.Place{Deactivated: true, Published: true},
			listable: true,
		}, {
			place:    &place.Place{Deactivated: false, Published: false},
			listable: false,
		}, {
			place:    &place.Place{Deactivated: false, Published: true},
			listable: true,
		},
	} {
		t.Run(fmt.Sprintf("%d", i), func(tt *testing.T) {
			if test.place.Listable() != test.listable {
				tt.Errorf("%d: Deactivated: %t Published: %t -> Listable %t", i, test.place.Deactivated, test.place.Published, test.place.Listable())
			}
		})
	}
}

func TestPlaceIsOwned(t *testing.T) {
	for _, test := range []struct {
		name  string
		place *place.Place
		id    string
		owned bool
	}{
		{
			name:  "Owned",
			place: &place.Place{OwnedBy: []string{"foo", "bar"}},
			id:    "foo",
			owned: true,
		}, {
			name:  "NotOwned",
			place: &place.Place{OwnedBy: []string{"bar"}},
			id:    "foo",
			owned: false,
		},
	} {
		t.Run(test.name, func(tt *testing.T) {
			if test.place.IsOwned(test.id) != test.owned {
				tt.Errorf("test.place.IsOwned(%s) != test.owned: %t != %t", test.id, test.place.IsOwned(test.id), test.owned)
			}
		})
	}
}

func TestNewPlaceIsOwned(t *testing.T) {
	for _, test := range []struct {
		name  string
		place *place.NewPlace
		id    string
		owned bool
	}{
		{
			name:  "Owned",
			place: &place.NewPlace{OwnedBy: []string{"foo", "bar"}},
			id:    "foo",
			owned: true,
		}, {
			name:  "NotOwned",
			place: &place.NewPlace{OwnedBy: []string{"bar"}},
			id:    "foo",
			owned: false,
		},
	} {
		t.Run(test.name, func(tt *testing.T) {
			if test.place.IsOwned(test.id) != test.owned {
				tt.Errorf("test.place.IsOwned(%s) != test.owned: %t != %t", test.id, test.place.IsOwned(test.id), test.owned)
			}
		})
	}
}

func TestServiceCreateAddsOwnedBy(t *testing.T) {
	store := place.NewMemoryStore()
	service := place.NewService(store)

	ctx := auth.ContextWithID(context.Background(), "bar")
	_, err := service.Create(ctx, &place.NewPlace{OwnedBy: []string{"foo"}})
	if err != nil {
		t.Fatal(err)
	}

	p, err := store.FindByID(context.Background(), "0")
	if err != nil {
		t.Fatal(err)
	}

	if len(p.OwnedBy) != 2 || p.OwnedBy[0] != "foo" || p.OwnedBy[1] != "bar" {
		t.Fatalf("owned by should include 'bar', got %v", p.OwnedBy)
	}
}

func TestServiceCreateWhiteSpace(t *testing.T) {
	store := place.NewMemoryStore()
	service := place.NewService(store)

	ctx := auth.ContextWithID(context.Background(), "bar")
	p, _ := service.Create(ctx, &place.NewPlace{Name: " foo "})
	if p.Name != "foo" {
		t.Fatalf("should remove white space from name: '%s'", p.Name)
	}
}

func TestServiceFindFilterOwnedBySelf(t *testing.T) {
	store := place.NewMemoryStore()
	_, err := store.Create(context.Background(), &place.NewPlace{
		OwnedBy: []string{"bar"},
	})
	if err != nil {
		t.Fatal(err)
	}
	service := place.NewService(store)

	places, _, err := service.Find(context.Background(), &crud.FindParams[place.FindFilters]{
		Filters: &place.FindFilters{OwnedBy: []string{"self"}},
	})
	if err != nil {
		t.Fatal(err)
	}
	if len(places) != 0 {
		t.Fatalf("expected 0 events, got %d", len(places))
	}

	ctx := auth.ContextWithID(context.Background(), "bar")
	places, _, err = service.Find(ctx, &crud.FindParams[place.FindFilters]{
		Filters: &place.FindFilters{OwnedBy: []string{"self"}},
	})
	if err != nil {
		t.Fatal(err)
	}
	if len(places) != 1 {
		t.Fatalf("expected 1 event, got %d", len(places))
	}
}
