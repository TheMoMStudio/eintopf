// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package cms

import (
	"context"
	"html/template"
	"io"
	"io/fs"
	"time"

	"eintopf.info"
)

type Service interface {
	ListThemes(ctx context.Context) ([]Theme, error)
	CreateTheme(ctx context.Context, themeName string) (Theme, error)
	ActivateTheme(ctx context.Context, themeName string) (Theme, error)
	RemoveTheme(ctx context.Context, themeName string) error

	ListFiles(ctx context.Context, filetype string) ([]string, error)
	FileContent(ctx context.Context, filetype string, filePathDecoded string) (File, error)
	UpdateFile(ctx context.Context, filetype string, filePathDecoded string, file *File) (File, error)
}

func NewService(themePath string, theme string) (*service, error) {
	newTheme, err := eintopf.NewTheme(theme, themePath, nil, nil)
	if err != nil {
		return nil, err
	}

	return &service{
		themePath:   themePath,
		activeTheme: newTheme,
		lastUpdated: time.Now(),
		data:        nil,
		funcs:       nil,
	}, nil
}

type service struct {
	// themePath is the directory where all themes are stored.
	themePath   string
	activeTheme *eintopf.StaticTheme
	data        map[string]any
	funcs       template.FuncMap
	lastUpdated time.Time
}

// LastModified implements the LastModifiedCache interface.
func (s *service) LastModified() time.Time {
	return s.lastUpdated
}

// AssetFS implements part of the Theme interface
func (s *service) AssetFS() fs.FS {
	return s.activeTheme.AssetFS()
}

// FS implements part of the Theme interface
func (s *service) FS() fs.FS {
	return s.activeTheme.FS()
}

// Render implements part of the Theme interface
func (s *service) Render(ctx context.Context, w io.Writer, templates []string, data map[string]any, funcs template.FuncMap, includeHelper bool) error {
	return s.activeTheme.Render(ctx, w, templates, data, funcs, includeHelper)
}
