// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package cms

// This package is responsible for theming the calendar.
// You can activate, create and delete Themes and edit the files of it.

import (
	"errors"
	"io"
	"net/http"
	"strings"

	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/xhttp"
	"eintopf.info/service/auth"
)

type router struct {
	s Service
}

// Router returns a new http router that handles crud action requests for a given
// action service.
func Router(authService auth.Service, service Service) func(chi.Router) {
	router := &router{service}
	return func(r chi.Router) {
		r.Options("/", xhttp.CorsHandler)

		// swagger:route GET /cms/themes cms listThemes
		//
		// List all available themes.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: createActionResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
			Get("/themes", router.getThemes)

		// swagger:route POST /cms/themes cms createTheme
		//
		// Create a new theme based on the embedded theme.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: createActionResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
			Post("/themes", router.postTheme)

		// swagger:route PATCH /cms/themes cms activateTheme
		//
		// Activate a theme that will be rendered for users.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: createActionResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
			Patch("/themes", router.patchTheme)

		// swagger:route GET /cms/themes/{name} cms deleteTheme
		//
		// Remove a theme and all files of it.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: createActionResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
			Delete("/themes/{name}", router.deleteTheme)

		// swagger:route GET /cms/files/{filetype} cms getFiles
		//
		// Get all filenames of a filetype.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: createActionResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
			Get("/files/{filetype}", router.getFiles)

		// swagger:route GET /cms/files/{filetype}/{filePath} cms getFileData
		//
		// Get the content of a file.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: createActionResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
			Get("/files/{filetype}/{filePath}", router.getFileData)

		// swagger:route PUT /cms/files/{filetype}/{filePath} cms putFileData
		//
		// Replace the content of a file.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: createActionResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
			Put("/files/{filetype}/{filePath}", router.putFileData)
	}
}

func (router *router) getThemes(w http.ResponseWriter, r *http.Request) {
	themes, err := router.s.ListThemes(r.Context())
	if err == auth.ErrUnauthorized {
		xhttp.WriteUnauthorized(w, err)
		return
	}
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}

	xhttp.WriteJSON(r.Context(), w, themes)
}

func (router *router) postTheme(w http.ResponseWriter, r *http.Request) {
	theme, err := xhttp.ReadJSON[ThemeRequest](r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}

	newTheme, err := router.s.CreateTheme(r.Context(), theme.Name)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}

	xhttp.WriteJSON(r.Context(), w, newTheme)
}

func (router *router) patchTheme(w http.ResponseWriter, r *http.Request) {
	theme, err := xhttp.ReadJSON[ThemeRequest](r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}

	newTheme, err := router.s.ActivateTheme(r.Context(), theme.Name)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}

	xhttp.WriteJSON(r.Context(), w, newTheme)
}

func (router *router) deleteTheme(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name")
	if name == "" {
		xhttp.WriteBadRequest(r.Context(), w, errors.New("empty id"))
		return
	}

	err := router.s.RemoveTheme(r.Context(), name)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}

	xhttp.WriteJSON(r.Context(), w, struct{}{})
}

func (router *router) getFiles(w http.ResponseWriter, r *http.Request) {
	filetype := chi.URLParam(r, "filetype")
	if checkFiletype(filetype) {
		xhttp.WriteBadRequest(r.Context(), w, errors.New("Empty or invalid filetype only "+strings.Join(filetypes[:], ", ")+" are allowed!"))
		return
	}

	names, err := router.s.ListFiles(r.Context(), filetype)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}

	xhttp.WriteJSON(r.Context(), w, names)
}

func (router *router) getFileData(w http.ResponseWriter, r *http.Request) {
	filetype := chi.URLParam(r, "filetype")
	filePathEncoded := chi.URLParam(r, "filePath")
	if checkFiletype(filetype) || filePathEncoded == "" {
		xhttp.WriteBadRequest(r.Context(), w, errors.New("filetype invalid or missing file path"))
		return
	}
	filePathDecoded, err := decodeFilePath(filePathEncoded)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, errors.New("could not decode file path"))
		return
	}

	file, err := router.s.FileContent(r.Context(), filetype, filePathDecoded)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}

	xhttp.WriteJSON(r.Context(), w, file)
}

const maxFileSize = 1 * 1024 * 1024 // 1Mb

func (router *router) putFileData(w http.ResponseWriter, r *http.Request) {
	filetype := chi.URLParam(r, "filetype")
	filePathEncoded := chi.URLParam(r, "filePath")
	if checkFiletype(filetype) || filePathEncoded == "" {
		xhttp.WriteBadRequest(r.Context(), w, errors.New("filetype invalid or filepath not given"))
		return
	}

	file := &File{}

	if strings.Contains(r.Header.Get("Content-Type"), "multipart/form-data") {
		r.Body = http.MaxBytesReader(w, r.Body, maxFileSize)

		// Parse our multipart form, 10 << 20 specifies a maximum
		// upload of 10 MB files.
		err := r.ParseMultipartForm(maxFileSize)
		if err != nil {
			xhttp.WriteBadRequest(r.Context(), w, err)
			return
		}
		// FormFile returns the first file for the given key `media`
		// it also returns the FileHeader so we can get the Filename,
		// the Header and the size of the file
		image, _, err := r.FormFile("binary")
		if err != nil {
			xhttp.WriteInternalError(r.Context(), w, err)
			return
		}
		defer image.Close()

		fileBytes, err := io.ReadAll(image)
		if err != nil {
			xhttp.WriteBadRequest(r.Context(), w, errors.New("could not read uploaded file data"))
			return
		}
		file.Binary = fileBytes
		file.IsBinary = true
	} else {
		fileData, err := xhttp.ReadJSON[File](r)
		if err != nil {
			xhttp.WriteBadRequest(r.Context(), w, err)
			return
		}
		file = fileData
		file.IsBinary = false
	}

	filePathDecoded, err := decodeFilePath(filePathEncoded)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, errors.New("could not decode file path"))
		return
	}

	newFile, err := router.s.UpdateFile(r.Context(), filetype, filePathDecoded, file)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}

	xhttp.WriteJSON(r.Context(), w, newFile)
}

type ThemeRequest struct {
	// in:body
	Name string
}

// swagger:response listThemes
type ListResponse struct {
	// in:body
	Themes []Theme
}

// swagger:response createTheme
type CreateResponse struct {
	// in:body
	Theme Theme
}

// swagger:response activateTheme
type ActivateResponse struct {
	// in:body
	Theme Theme
}

// swagger:response deleteTheme
type DeleteResponse struct {
	// in:body
	Deleted bool
}

// swagger:response getFiles
type GetFileResponse struct {
	// in:body
	Filenames []string
}

// swagger:response getFileData
type GetFileDataResponse struct {
	// in:body
	File File
}

// swagger:response putFileData
type PutFileDataResponse struct {
	// in:body
	File File
}
