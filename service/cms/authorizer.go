// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package cms

import (
	"context"

	"eintopf.info/service/auth"
)

type authorizer struct {
	service Service
}

func NewAuthorizer(service Service) Service {
	return &authorizer{service}
}

func (a *authorizer) ListThemes(ctx context.Context) ([]Theme, error) {
	err := authorize(ctx)
	if err != nil {
		return nil, auth.ErrUnauthorized
	}
	return a.service.ListThemes(ctx)
}

func (a *authorizer) CreateTheme(ctx context.Context, themeName string) (Theme, error) {
	err := authorize(ctx)
	if err != nil {
		return Theme{}, auth.ErrUnauthorized
	}
	return a.service.CreateTheme(ctx, themeName)
}

func (a *authorizer) ActivateTheme(ctx context.Context, themeName string) (Theme, error) {
	err := authorize(ctx)
	if err != nil {
		return Theme{}, auth.ErrUnauthorized
	}
	return a.service.ActivateTheme(ctx, themeName)
}

func (a *authorizer) RemoveTheme(ctx context.Context, themeName string) error {
	err := authorize(ctx)
	if err != nil {
		return auth.ErrUnauthorized
	}
	return a.service.RemoveTheme(ctx, themeName)
}

func (a *authorizer) ListFiles(ctx context.Context, filetype string) ([]string, error) {
	err := authorize(ctx)
	if err != nil {
		return nil, auth.ErrUnauthorized
	}
	return a.service.ListFiles(ctx, filetype)
}

func (a *authorizer) FileContent(ctx context.Context, filetype string, filePathDecoded string) (File, error) {
	err := authorize(ctx)
	if err != nil {
		return File{}, auth.ErrUnauthorized
	}
	return a.service.FileContent(ctx, filetype, filePathDecoded)
}

func (a *authorizer) UpdateFile(ctx context.Context, filetype string, filePathDecoded string, file *File) (File, error) {
	err := authorize(ctx)
	if err != nil {
		return File{}, auth.ErrUnauthorized
	}
	return a.service.UpdateFile(ctx, filetype, filePathDecoded, file)
}

// Update is allowed by
//   - an admin
func authorize(ctx context.Context) error {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return auth.ErrUnauthorized
	}
	if role != auth.RoleAdmin {
		return auth.ErrUnauthorized
	}
	return nil
}
