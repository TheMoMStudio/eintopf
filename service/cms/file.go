// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package cms

import (
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path"
	"slices"
	"strings"
)

type File struct {
	IsBinary bool   `json:"isBinary"`
	Binary   []byte `json:"binary"`
	Content  string `json:"content"`
}

var filetypes = []string{"css", "js", "template", "md", "config", "image", "font", "meta"}

func (service service) ListFiles(ctx context.Context, filetype string) ([]string, error) {
	if service.activeTheme.IsBundeled() {
		return []string{}, nil
	}

	files := []string{}
	codeDir := folderFromFileType(filetype)

	var fileError error
	fs.WalkDir(service.activeTheme.FS(), codeDir,
		func(path string, info fs.DirEntry, err error) error {
			if err != nil {
				fileError = err
				return err
			}

			languageFilePathSplit := strings.Split(path, codeDir+"/")

			if len(languageFilePathSplit) == 2 && !info.IsDir() && !strings.Contains(languageFilePathSplit[1], ".license") {
				files = append(files, languageFilePathSplit[1])
			}

			return nil
		})

	if fileError != nil {
		return []string{}, fileError
	}

	return files, nil
}

func folderFromFileType(filetype string) string {
	switch filetype {
	case "template":
		return "templates"
	case "md":
		return "content"
	case "config":
		return "."
	case "image":
		return path.Join("assets", "images")
	case "font":
		return path.Join("assets", "fonts")
	case "meta":
		return "assets"
	default:
		return path.Join("assets", filetype)
	}
}

func checkFiletype(filetype string) bool {
	return filetype == "" || !slices.Contains(filetypes, filetype)
}

func decodeFilePath(filePathEncoded string) (string, error) {
	filePathDecoded, err := base64.StdEncoding.DecodeString(filePathEncoded)
	if err != nil {
		return "", err
	}
	return string(filePathDecoded), nil
}

func (service service) FileContent(ctx context.Context, filetype string, filePathDecoded string) (File, error) {
	if service.activeTheme.IsBundeled() {
		return File{}, errors.New("can not edit a bundeled theme")
	}

	codeDir := folderFromFileType(filetype)

	file, err := fs.ReadFile(service.activeTheme.FS(), path.Join(codeDir, filePathDecoded))
	if err != nil {
		return File{}, fmt.Errorf("could not decode file path(%s): %s", path.Join(codeDir, filePathDecoded), err)
	}
	fileDataContent := string(file)
	return File{IsBinary: false, Content: fileDataContent}, nil
}

func (service service) UpdateFile(ctx context.Context, filetype string, filePathDecoded string, file *File) (File, error) {
	if service.activeTheme.IsBundeled() {
		return File{}, errors.New("can not edit a bundeled theme")
	}

	codeDir := folderFromFileType(filetype)
	codePath := path.Join(codeDir, filePathDecoded)

	if _, err := fs.Stat(service.activeTheme.FS(), codePath); errors.Is(err, os.ErrNotExist) {
		return File{}, errors.New("file not found")
	}

	byteData := []byte{}
	if file.IsBinary {
		byteData = file.Binary
	} else {
		byteData = []byte(file.Content)
	}

	err := os.WriteFile(path.Join(service.themePath, service.activeTheme.Name(), codePath), byteData, 0o644)
	if err != nil {
		return File{}, err
	}

	return File{IsBinary: file.IsBinary, Binary: file.Binary, Content: file.Content}, nil
}
