// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package cms

import (
	"context"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path"
	"regexp"
	"strings"
	"time"

	"eintopf.info"
	"gopkg.in/yaml.v3"
)

type Theme struct {
	Name     string              `json:"name"`
	Active   bool                `json:"active"`
	Editable bool                `json:"editable"`
	Config   eintopf.ThemeConfig `json:"config"`
}

func (service *service) ListThemes(ctx context.Context) ([]Theme, error) {
	themes := []Theme{}

	dirEntries, err := eintopf.BundeledThemes.ReadDir("themes")
	if err != nil {
		return []Theme{}, fmt.Errorf("list bundeled theme directories: %s", err)
	}
	for _, entry := range dirEntries {
		if !entry.IsDir() {
			continue
		}
		themes = append(themes, Theme{
			Name:     entry.Name(),
			Active:   entry.Name() == service.activeTheme.Name(),
			Editable: false,
		})
	}

	if service.themePath != "" {
		dirEntries, err = os.ReadDir(service.themePath)
		if err != nil {
			return []Theme{}, fmt.Errorf("list theme directories: %s", err)
		}
		for _, entry := range dirEntries {
			if !entry.IsDir() {
				continue
			}
			// Check for duplicate themes.
			duplicateThemeName := false
			for _, theme := range themes {
				if theme.Name == entry.Name() {
					duplicateThemeName = true
					break
				}
			}
			if !duplicateThemeName {
				themes = append(themes, Theme{
					Name:     entry.Name(),
					Active:   entry.Name() == service.activeTheme.Name(),
					Editable: true,
				})
			}
		}
	}
	return themes, nil
}

func (service *service) isThemeUniq(name string) (bool, error) {
	themes, err := service.ListThemes(context.Background())
	if err != nil {
		return false, fmt.Errorf("isThemeUniq: could not get themes: %s", err)
	}
	themeNameLower := strings.ToLower(name)
	for _, theme := range themes {
		if themeNameLower == strings.ToLower(theme.Name) {
			return false, nil
		}
	}
	return true, nil
}

func isThemeNameAllowed(name string) bool {
	validName := regexp.MustCompile(`^[A-Za-z0-9_-]+$`)
	return validName.MatchString(name)
}

func (service *service) copyDir(themeFS fs.FS, origin string, dest string) error {
	var err error
	var files []fs.DirEntry

	files, err = fs.ReadDir(themeFS, origin)
	if err != nil {
		return errors.New("copyDir: could not read origin theme path")
	}

	if _, err := fs.Stat(themeFS, dest); os.IsNotExist(err) {
		os.MkdirAll(path.Join(service.themePath, dest), os.ModePerm)
	}

	for _, file := range files {
		if file.IsDir() {
			service.copyDir(themeFS, path.Join(origin, file.Name()), path.Join(dest, file.Name()))
			continue
		}
		var fileContent []byte
		fileContent, err = fs.ReadFile(themeFS, path.Join(origin, file.Name()))
		if err != nil {
			return errors.New("copyDir: could not read file to copy")
		}

		filename := path.Join(service.themePath, dest, file.Name())

		if err := os.WriteFile(filename, fileContent, 0o666); err != nil {
			return errors.New("copyDir: could not write copy of file")
		}

	}
	return nil
}

func (service *service) CreateTheme(ctx context.Context, themeName string) (Theme, error) {
	if !isThemeNameAllowed(themeName) {
		return Theme{}, errors.New("theme name is not allowed, use A-Z, a-z, 0-9, - or _")
	}
	isUniq, err := service.isThemeUniq(themeName)
	if err != nil {
		return Theme{}, err
	}
	if !isUniq {
		return Theme{}, errors.New("theme already exists!")
	}
	f, _ := fs.Sub(eintopf.BundeledThemes, "themes")
	err = service.copyDir(f, eintopf.DefaultTheme, themeName)
	if err != nil {
		return Theme{}, err
	}

	config := eintopf.ThemeConfig{
		Parent: eintopf.DefaultTheme,
	}

	configCOntent, err := yaml.Marshal(config)
	if err != nil {
		return Theme{}, err
	}
	err = os.WriteFile(path.Join(service.themePath, themeName, "theme.yaml"), configCOntent, os.ModePerm)
	if err != nil {
		return Theme{}, err
	}

	return Theme{Name: themeName, Active: false, Editable: true}, nil
}

func (service *service) ActivateTheme(ctx context.Context, themeName string) (Theme, error) {
	theme, err := eintopf.NewTheme(themeName, service.themePath, nil, nil)
	if err != nil {
		return Theme{}, err
	}
	service.activeTheme = theme
	service.lastUpdated = time.Now()

	return Theme{Name: themeName, Editable: true, Active: true}, nil
}

func (service *service) RemoveTheme(ctx context.Context, themeName string) error {
	themes, err := service.ListThemes(ctx)
	if err != nil {
		return err
	}

	theme := Theme{}
	for _, t := range themes {
		if t.Name == themeName {
			theme = t
			break
		}
	}
	if theme.Name == "" {
		return errors.New("theme does not exist")
	}

	if service.activeTheme.Name() == themeName {
		return errors.New("you can not delete the active theme")
	}

	if !theme.Editable {
		return errors.New("you can not delete the embedded theme")
	}

	return os.RemoveAll(path.Join(service.themePath, themeName))
}
