// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package action_test

import (
	"context"
	"fmt"
	"testing"

	"eintopf.info/service/action"
	"eintopf.info/service/auth"
)

func TestAction(t *testing.T) {
	store := action.NewMemoryStore()
	service := action.NewService(store, nil)

	t.Run("Sucessull Call", func(t *testing.T) {
		i := 0
		service.Register("foo", func(ctx context.Context) error {
			i++
			return nil
		}, "")

		err := service.Call(auth.ContextWithID(context.Background(), "1312"), "foo")
		if err != nil {
			t.Error(err)
		}
		if i != 1 {
			t.Errorf("i should be 1, got %d", i)
		}

		a, err := service.Store().FindByID(context.Background(), "foo")
		if err != nil {
			t.Fatal(err)
		}
		if a.Calls != 1 {
			t.Errorf("Calls should be 1, got %d", a.Calls)
		}
		if a.Error != "" {
			t.Errorf("Error should be empty, got '%s'", a.Error)
		}
		if a.LastCalledBy != "id:1312" {
			t.Errorf("LastCalledBy should be '1312', got %s", a.LastCalledBy)
		}
	})

	t.Run("Error Call", func(t *testing.T) {
		service.Register("bar", func(ctx context.Context) error {
			return fmt.Errorf("oh no")
		}, "")

		err := service.Call(auth.ContextWithID(context.Background(), "1312"), "bar")
		if err == nil {
			t.Error("should return an error")
		}

		a, err := service.Store().FindByID(context.Background(), "bar")
		if err != nil {
			t.Fatal(err)
		}
		if a.Calls != 1 {
			t.Errorf("Calls should be 1, got %d", a.Calls)
		}
		if a.Error != "oh no" {
			t.Errorf("Error should be 'oh no', got '%s'", a.Error)
		}
		if a.LastCalledBy != "id:1312" {
			t.Errorf("LastCalledBy should be '1312', got %s", a.LastCalledBy)
		}
	})
}
