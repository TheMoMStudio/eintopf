// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package action_test

import (
	"context"
	"testing"

	"eintopf.info/service/action"
	"eintopf.info/service/auth"
)

var (
	normalCtx    = auth.ContextWithRole(context.Background(), auth.RoleNormal)
	moderatorCtx = auth.ContextWithRole(context.Background(), auth.RoleModerator)
	adminCtx     = auth.ContextWithRole(context.Background(), auth.RoleAdmin)
	internalCtx  = auth.ContextWithRole(context.Background(), auth.RoleInternal)
)

func TestAuthorizerCreate(t *testing.T) {
	store := action.NewMemoryStore()
	authorizer := action.NewAuthorizer(store)

	t.Run("Background context -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.Create(context.Background(), &action.Action{})
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Normal -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.Create(normalCtx, &action.Action{})
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Moderator -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.Create(moderatorCtx, &action.Action{})
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Admin -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.Create(adminCtx, &action.Action{})
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Internal -> Authorized", func(t *testing.T) {
		_, err := authorizer.Create(internalCtx, &action.Action{})
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})
}

func TestAuthorizerUpdate(t *testing.T) {
	store := action.NewMemoryStore()
	_, err := store.Create(context.Background(), &action.Action{
		Name: "foo",
	})
	if err != nil {
		t.Fatal(err)
	}
	authorizer := action.NewAuthorizer(store)

	t.Run("Background -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.Update(context.Background(), &action.Action{Name: "foo"})
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Normal user -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.Update(normalCtx, &action.Action{Name: "foo"})
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Moderator -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.Update(auth.ContextWithID(moderatorCtx, "1312"), &action.Action{Name: "foo"})
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Admin -> Authorized", func(t *testing.T) {
		_, err := authorizer.Update(adminCtx, &action.Action{Name: "foo"})
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})

	t.Run("Internal -> Authorized", func(t *testing.T) {
		_, err := authorizer.Update(internalCtx, &action.Action{Name: "foo"})
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})
}

func TestAuthorizerDelete(t *testing.T) {
	store := action.NewMemoryStore()
	_, err := store.Create(context.Background(), &action.Action{
		Name: "foo",
	})
	if err != nil {
		t.Fatal(err)
	}
	authorizer := action.NewAuthorizer(store)

	t.Run("Background -> Unauthorized", func(t *testing.T) {
		err := authorizer.Delete(context.Background(), "foo")
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Normal user -> Unauthorized", func(t *testing.T) {
		err := authorizer.Delete(normalCtx, "foo")
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Moderator -> Unauthorized", func(t *testing.T) {
		err := authorizer.Delete(moderatorCtx, "foo")
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Admin -> Authorized", func(t *testing.T) {
		err := authorizer.Delete(adminCtx, "foo")
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		_, err = store.Create(context.Background(), &action.Action{
			Name: "foo",
		})
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Internal -> Authorized", func(t *testing.T) {
		err := authorizer.Delete(internalCtx, "foo")
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})
}

func TestAuthorizerFindByID(t *testing.T) {
	store := action.NewMemoryStore()
	_, err := store.Create(context.Background(), &action.Action{
		Name: "foo",
	})
	if err != nil {
		t.Fatal(err)
	}
	authorizer := action.NewAuthorizer(store)

	t.Run("Background -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.FindByID(context.Background(), "foo")
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Normal user -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.FindByID(auth.ContextWithID(normalCtx, "foo"), "foo")
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Moderator -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.FindByID(auth.ContextWithID(moderatorCtx, "foo"), "foo")
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Admin -> Authorized", func(t *testing.T) {
		_, err := authorizer.FindByID(adminCtx, "foo")
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})

	t.Run("Internal -> Authorized", func(t *testing.T) {
		_, err := authorizer.FindByID(internalCtx, "foo")
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})
}

func TestAuthorizerFind(t *testing.T) {
	store := action.NewMemoryStore()
	_, err := store.Create(context.Background(), &action.Action{
		Name: "foo",
	})
	if err != nil {
		t.Fatal(err)
	}
	authorizer := action.NewAuthorizer(store)

	t.Run("Background -> Unauthorized", func(t *testing.T) {
		_, _, err := authorizer.Find(context.Background(), nil)
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Normal user -> Unauthorized", func(t *testing.T) {
		_, _, err := authorizer.Find(auth.ContextWithID(normalCtx, "foo"), nil)
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Moderator -> Unauthorized", func(t *testing.T) {
		_, _, err := authorizer.Find(auth.ContextWithID(moderatorCtx, "foo"), nil)
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Admin -> Authorized", func(t *testing.T) {
		_, total, err := authorizer.Find(adminCtx, nil)
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		if total != 1 {
			t.Errorf("total should be 1, got %d", total)
		}
	})

	t.Run("Internal -> Authorized", func(t *testing.T) {
		_, total, err := authorizer.Find(internalCtx, nil)
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		if total != 1 {
			t.Errorf("total should be 1, got %d", total)
		}
	})
}
