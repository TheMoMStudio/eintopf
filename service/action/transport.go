// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package action

import (
	"net/http"

	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/crud"
	"eintopf.info/internal/xhttp"
	"eintopf.info/service/auth"
)

// Router returns a new http router that handles crud action requests for a given
// action service.
func Router(service *Service, authService auth.Service) func(chi.Router) {
	handler := crud.NewHandler[Action, Action, Filters](service.Store())
	return func(r chi.Router) {
		r.Options("/", xhttp.CorsHandler)

		// swagger:route GET /actions/ action findActions
		//
		// Retrieves all actions.
		//
		// Parameters:
		//   + name: offset
		//     description: offset in action list
		//     in: query
		//     type: integer
		//     required: false
		//   + name: limit
		//     description: limits the action list
		//     in: query
		//     type: integer
		//     required: false
		//   + name: sort
		//     description: field that gets sorted
		//     in: query
		//     type: string
		//     required: false
		//   + name: order
		//     description: sort order ("ASC" or "DESC")
		//     in: query
		//     type: string
		//     required: false
		//   + name: filters
		//     description: filters get combined with AND logic
		//     in: query
		//     type: object
		//     required: false
		//
		//     Responses:
		//       200: findActionsResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: false})).
			Get("/", handler.Find)

		// swagger:route POST /actions/ action createAction
		//
		// Creates a new action with the given data.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: createActionResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
			Post("/", handler.Create)

		r.Options("/{id}", xhttp.CorsHandler)

		// swagger:route GET /actions/{id} action findAction
		//
		// Finds the action with the given id.
		//
		//     Responses:
		//       200: findActionResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
			Get("/{id}", handler.FindByID)

		// swagger:route PUT /actions/{id} action updateAction
		//
		// Updates the action with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: updateActionResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
			Put("/{id}", handler.Update)

		// swagger:route DELETE /actions/{id} action deleteAction
		//
		// Deletes the action with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: deleteActionResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
			Delete("/{id}", handler.Delete)

		// swagger:route POST /actions/run action runAction
		//
		// Runs the action with the given name.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: runActionResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
			Post("/run", func(w http.ResponseWriter, r *http.Request) {
				req, err := xhttp.ReadJSON[RunRequest](r)
				if err != nil {
					xhttp.WriteBadRequest(r.Context(), w, err)
				}
				err = service.Call(r.Context(), req.Name)
				if err != nil {
					xhttp.WriteError(r.Context(), w, err)
				}

				xhttp.WriteJSON(r.Context(), w, RunResponse{})
			})
	}
}

// swagger:response findActionsResponse
type FindResponse struct {
	// in:body
	Actions []*Action

	// in:header
	TotalActions int `json:"x-total-count"`
}

// swagger:parameters createAction
type CreateRequest struct {
	// in:body
	Action *Action
}

// swagger:response createActionResponse
type CreateResponse struct {
	// in:body
	Action *Action
}

// swagger:parameters findAction
type FindByIDRequest struct {
	// in:query
	ID string `json:"id"`
}

// swagger:response findActionResponse
type FindByIDResponse struct {
	// in:body
	Action *Action
}

// swagger:parameters updateAction
type UpdateRequest struct {
	// in:body
	Action *Action
}

// swagger:response updateActionResponse
type UpdateResponse struct {
	// in:body
	Action *Action
}

// swagger:parameters deleteAction
type DeleteRequest struct {
	// in:query
	ID string `json:"id"`
}

// swagger:response deleteActionResponse
type DeleteResponse struct{}

// swagger:parameters deleteAction
type RunRequest struct {
	// in:body
	Name string `json:"name"`
}

// swagger:response runActionResponse
type RunResponse struct{}
