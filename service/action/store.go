// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package action

import (
	"context"

	"eintopf.info/internal/crud"
	"eintopf.info/service/dbmigration"
	"github.com/jmoiron/sqlx"
)

func actionFromAction(action *Action, id string) *Action { return action }
func newID(action *Action) string                        { return action.Name }

// NewMemoryStore returns a new in memory store.
func NewMemoryStore() *crud.MemoryStore[Action, Action, Filters] {
	return crud.NewMemoryStore[Action, Action, Filters](actionFromAction, newID)
}

// NewSqlStore returns a new sql store for notifications.
func NewSqlStore(db *sqlx.DB, migrationService dbmigration.Service) (*SqlStore, error) {
	store := &SqlStore{
		db,
		migrationService,
		crud.NewSqlStore(db, table, actionFromAction, newID),
	}
	if err := store.runMigrations(context.Background()); err != nil {
		return nil, err
	}
	return store, nil
}

type SqlStore struct {
	db               *sqlx.DB
	migrationService dbmigration.Service

	*crud.SqlStore[Action, Action, Filters]
}

func (s *SqlStore) runMigrations(ctx context.Context) error {
	return s.migrationService.RunMigrations(ctx, []dbmigration.Migration{
		dbmigration.NewMigration("createActionTable", s.createActionTable, nil),
	})
}

func (s *SqlStore) createActionTable(ctx context.Context) error {
	_, err := s.db.ExecContext(ctx, `
        CREATE TABLE IF NOT EXISTS actions (
            name VARCHAR(64) NOT NULL PRIMARY KEY UNIQUE,
            calls INT NOT NULL DEFAULT 0,
            last_call DATETIME,
            last_called_by VARCHAR(64),
            error TEXT
        );
    `)
	return err
}

var table = crud.SqlTable[Filters]{
	Table:          "actions",
	IDField:        "name",
	Fields:         []string{"name", "calls", "last_call", "last_called_by", "error"},
	SortableFields: []string{"name", "calls", "last_call", "error"},
}
