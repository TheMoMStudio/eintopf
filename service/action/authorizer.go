// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package action

import (
	"context"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
)

type Authorizer struct {
	store Storer
}

// NewAuthorizer wraps the given store with authorization methods.
func NewAuthorizer(store Storer) *Authorizer {
	return &Authorizer{store: store}
}

// Create is allowed by
//   - internally
func (a *Authorizer) Create(ctx context.Context, action *Action) (*Action, error) {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return nil, auth.ErrUnauthorized
	}
	if role != auth.RoleInternal {
		return nil, auth.ErrUnauthorized
	}
	return a.store.Create(ctx, action)
}

// Update is allowed by
//   - an admin
//   - internally
func (a *Authorizer) Update(ctx context.Context, action *Action) (*Action, error) {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return nil, auth.ErrUnauthorized
	}
	if role != auth.RoleInternal && role != auth.RoleAdmin {
		return nil, auth.ErrUnauthorized
	}
	return a.store.Update(ctx, action)
}

// Delete is allowed by
//   - the user owning the action
//   - an admin
//   - internally
func (a *Authorizer) Delete(ctx context.Context, id string) error {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return auth.ErrUnauthorized
	}
	if role != auth.RoleInternal && role != auth.RoleAdmin {
		return auth.ErrUnauthorized
	}
	return a.store.Delete(ctx, id)
}

// FindByID is allowed by
//   - an admin
//   - internally
func (a *Authorizer) FindByID(ctx context.Context, id string) (*Action, error) {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return nil, auth.ErrUnauthorized
	}
	if role != auth.RoleInternal && role != auth.RoleAdmin {
		return nil, auth.ErrUnauthorized
	}
	return a.store.FindByID(ctx, id)
}

// Find is allowed by
//   - an admin
//   - internally
func (a *Authorizer) Find(ctx context.Context, params *crud.FindParams[Filters]) ([]*Action, int, error) {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return nil, 0, auth.ErrUnauthorized
	}
	if role != auth.RoleInternal && role != auth.RoleAdmin {
		return nil, 0, auth.ErrUnauthorized
	}
	return a.store.Find(ctx, params)
}
