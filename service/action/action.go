// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package action

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/go-co-op/gocron/v2"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
)

type Action struct {
	Name         string    `json:"name"`
	Calls        int       `json:"calls"`
	LastCall     time.Time `json:"lastCall" db:"last_call"`
	LastCalledBy string    `json:"lastCalledBy" db:"last_called_by"`
	Error        string    `json:"error"`
}

func (a Action) Identifier() string { return a.Name }

type Function func(ctx context.Context) error

type (
	Filters struct{}
	Storer  crud.Storer[Action, Action, Filters]
)

type Service struct {
	actions map[string]Function

	store    Storer
	sheduler gocron.Scheduler
}

func NewService(store Storer, sheduler gocron.Scheduler) *Service {
	return &Service{
		actions:  map[string]Function{},
		store:    store,
		sheduler: sheduler,
	}
}

func (s *Service) Register(name string, fn Function, cronExpression string) error {
	s.actions[name] = fn
	ctx := auth.ContextWithID(auth.ContextWithRole(context.Background(), auth.RoleInternal), "internal")

	if cronExpression != "" {
		_, err := s.sheduler.NewJob(gocron.CronJob(cronExpression, false), gocron.NewTask(func() {
			err := s.Call(ctx, name)
			if err != nil {
				log.Printf("cronjob: %s", err)
			}
		}))
		if err != nil {
			return err
		}
	}

	action, err := s.store.FindByID(ctx, name)
	if err != nil {
		return err
	}
	if action != nil {
		// Prevent the action from getting registered multiple times.
		return nil
	}

	_, err = s.store.Create(ctx, &Action{Name: name})
	if err != nil {
		return err
	}

	return nil
}

func (s *Service) Call(ctx context.Context, name string) error {
	fn, ok := s.actions[name]
	if !ok {
		return fmt.Errorf("action was not registered: %s", name)
	}
	callError := fn(ctx)
	if callError != nil {
		log.Printf("action(%s) error: %s", name, callError)
	}

	action, err := s.store.FindByID(ctx, name)
	if err != nil {
		return fmt.Errorf("action not found: %s", err)
	}
	action.Calls += 1
	action.LastCall = time.Now()
	calledBy, err := auth.UserIDFromContext(ctx)
	if err == nil {
		if calledBy == "internal" {
			action.LastCalledBy = "internal"
		} else {
			action.LastCalledBy = fmt.Sprintf("id:%s", calledBy)
		}
	}
	if callError != nil {
		action.Error = callError.Error()
	} else {
		action.Error = ""
	}
	_, err = s.store.Update(ctx, action)
	if err != nil {
		return fmt.Errorf("action update: %s", err)
	}
	return callError
}

func (s *Service) Store() Storer {
	return s.store
}
