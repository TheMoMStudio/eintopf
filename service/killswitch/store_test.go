//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package killswitch_test

import (
	"context"
	"os"
	"testing"
	"time"

	"eintopf.info/service/killswitch"
	"eintopf.info/service/killswitch/testutil"
)

func TestMemoryStore(t *testing.T) {
	store := killswitch.NewMemoryStore()
	testutil.TestStore(t, store)
}

func TestFileStore(t *testing.T) {
	store := killswitch.NewFileStore("test_killswitch", 5*time.Second)
	defer store.Stop()
	defer os.Remove("test_killswitch")
	testutil.TestStore(t, store)
}

func TestFileStoreExternal(t *testing.T) {
	store := killswitch.NewFileStore("test_killswitch_external", 10*time.Millisecond)
	defer store.Stop()
	defer os.Remove("test_killswitch_external")

	err := store.SetState(context.Background(), false)
	if err != nil {
		t.Errorf("store.SetState(ctx, false): %s", err)
	}
	if _, err := os.Stat("test_killswitch_external"); err != nil {
		t.Errorf("file should exist: %s", err)
	}
	err = os.Remove("test_killswitch_external")
	if err != nil {
		t.Errorf("os.Remove('test_killswitch_external'): %s", err)
	}
	time.Sleep(100 * time.Millisecond)
	on, err := store.GetState(context.Background())
	if err != nil {
		t.Errorf("store.GetState(ctx): %s", err)
	}
	if on != true {
		t.Error("state: on != true, after delting file")
	}

	err = store.SetState(context.Background(), true)
	if err != nil {
		t.Errorf("store.SetState(ctx, true): %s", err)
	}
	if _, err := os.Stat("test_killswitch_external"); err == nil {
		t.Errorf("file should not exist: %s", err)
	}
	err = os.WriteFile("test_killswitch_external", []byte{}, 0644)
	if err != nil {
		t.Errorf("os.WriteFile('test_killswitch_external', []byte{}, 0644): %s", err)
	}
	time.Sleep(100 * time.Millisecond)
	on, err = store.GetState(context.Background())
	if err != nil {
		t.Errorf("store.GetState(ctx): %s", err)
	}
	if on != false {
		t.Error("state: on != false, after creating file")
	}
}
