//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package killswitch

import (
	"context"
	"fmt"
	"log"
	"os"
	"sync"
	"time"
)

func NewMemoryStore() *MemoryStore {
	return &MemoryStore{on: true, rwm: &sync.RWMutex{}}
}

type MemoryStore struct {
	on  bool
	rwm *sync.RWMutex
}

func (m *MemoryStore) SetState(ctx context.Context, on bool) error {
	m.rwm.Lock()
	m.on = on
	m.rwm.Unlock()
	return nil
}

func (m *MemoryStore) GetState(ctx context.Context) (bool, error) {
	m.rwm.RLock()
	on := m.on
	m.rwm.RUnlock()
	return on, nil
}

func NewFileStore(filePath string, watcherRate time.Duration) *FileStore {
	store := &FileStore{
		filePath:    filePath,
		watcherRate: watcherRate,

		memoryStore: NewMemoryStore(),
		stop:        make(chan bool),
	}

	go store.watchForFileChanges()

	return store
}

type FileStore struct {
	filePath    string
	watcherRate time.Duration
	memoryStore *MemoryStore
	stop        chan bool
}

func (f *FileStore) SetState(ctx context.Context, on bool) error {
	f.memoryStore.SetState(ctx, on)
	if on {
		if _, err := os.Stat(f.filePath); err == nil {
			if err := os.Remove(f.filePath); err != nil {
				return fmt.Errorf("remove killswitch file: %s", err)
			}
		}
	} else {
		if err := os.WriteFile(f.filePath, []byte{}, 0644); err != nil {
			return fmt.Errorf("write killswitch file: %s", err)
		}
	}
	return nil
}

func (f *FileStore) GetState(ctx context.Context) (bool, error) {
	return f.memoryStore.GetState(ctx)
}

func (f *FileStore) Stop() {
	close(f.stop)
}

func (f *FileStore) watchForFileChanges() {
	ticker := time.NewTicker(f.watcherRate)
	defer func() { ticker.Stop() }()

	for {
		select {
		case <-f.stop:
			return
		case <-ticker.C:
			if _, err := os.Stat(f.filePath); err == nil {
				f.memoryStore.SetState(context.Background(), false)
			} else if os.IsNotExist(err) {
				f.memoryStore.SetState(context.Background(), true)
			} else {
				log.Printf("killswitch: fileWatcher: %s\n", err)
			}
		}
	}
}
