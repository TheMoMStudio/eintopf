//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package killswitch_test

import (
	"context"
	"fmt"
	"net/http"
	"testing"

	"eintopf.info/internal/xhttptest"
	"eintopf.info/service/killswitch"
)

func TestRouter(t *testing.T) {
	tests := []xhttptest.HttpTest{
		{
			Name:       "TurnOffInvalidHash",
			URI:        "/turnoff/invalid",
			WantStatus: http.StatusBadRequest,
		}, {
			Name:       "TurnOffValidHash",
			URI:        "/turnoff/foo",
			WantStatus: http.StatusOK,
		}, {
			Name:       "TurnOnInvalidHash",
			URI:        "/turnon/invalid",
			WantStatus: http.StatusBadRequest,
		}, {
			Name:       "TurnOnValidHash",
			URI:        "/turnon/foo",
			WantStatus: http.StatusOK,
		},
	}

	killswitchService := killswitch.NewService(killswitch.NewMemoryStore(), []string{"foo"})

	xhttptest.TestRouter(t, killswitch.Router(killswitchService), tests)
}

func testHandler() http.HandlerFunc {
	fn := func(w http.ResponseWriter, req *http.Request) {
		w.WriteHeader(http.StatusOK)
	}
	return http.HandlerFunc(fn)
}

func TestMiddleware(t *testing.T) {
	store := killswitch.NewMemoryStore()
	killswitchService := killswitch.NewService(store, nil)

	onFunc := func(on bool) func() error {
		return func() error {
			err := store.SetState(context.Background(), on)
			if err != nil {
				return fmt.Errorf("store.SetState(ctx, %t): %s", on, err)
			}
			return nil
		}
	}

	tests := []xhttptest.HttpTest{
		{
			Name:       "On200",
			PreTest:    onFunc(true),
			URI:        "/foo",
			WantStatus: http.StatusOK,
		}, {
			Name:       "Off503",
			PreTest:    onFunc(false),
			URI:        "/foo",
			WantStatus: http.StatusServiceUnavailable,
		}, {
			Name:       "OffTurnOn200",
			PreTest:    onFunc(false),
			URI:        "/turnon",
			WantStatus: http.StatusOK,
		},
	}

	xhttptest.TestMiddleware(t, killswitch.Middleware(killswitchService), tests)
}
