//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package testutil

import (
	"context"
	"testing"

	"eintopf.info/service/killswitch"
)

func TestKillSwitchService(t *testing.T, store killswitch.Storer) {
	service := killswitch.NewService(store, []string{"valid"})

	err := service.TurnOff(context.Background(), "invalid")
	if err != killswitch.ErrInvalidHash {
		t.Errorf("service.TurnOff(ctx, 'invalid'): hash should be invalid: error: %s", err)
	}
	err = service.TurnOff(context.Background(), "valid")
	if err != nil {
		t.Errorf("service.TurnOff(ctx, 'valid'): %s", err)
	}

	on, err := service.On(context.Background())
	if err != nil {
		t.Errorf("service.On(ctx): %s", err)
	}
	if on == false {
		t.Errorf("service state should be off after succesful turn off")
	}

	err = service.TurnOn(context.Background(), "invalid")
	if err != killswitch.ErrInvalidHash {
		t.Errorf("service.TurnOn(ctx, 'invalid'): hash should be invalid: error: %s", err)
	}
	err = service.TurnOn(context.Background(), "valid")
	if err != nil {
		t.Errorf("service.TurnOn(ctx, 'valid'): %s", err)
	}

	on, err = service.On(context.Background())
	if err != nil {
		t.Errorf("service.On(ctx): %s", err)
	}
	if on == true {
		t.Errorf("service state should be on after succesful turn on")
	}
}
