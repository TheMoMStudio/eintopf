//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package testutil

import (
	"context"
	"testing"

	"eintopf.info/service/killswitch"
)

func TestStore(t *testing.T, store killswitch.Storer) {
	on, err := store.GetState(context.Background())
	if err != nil {
		t.Errorf("store.GetState(ctx): %s", err)
	}
	if on == false {
		t.Error("initial value should be on")
	}

	err = store.SetState(context.Background(), false)
	if err != nil {
		t.Errorf("store.SetState(ctx, false): %s", err)
	}
	on, err = store.GetState(context.Background())
	if err != nil {
		t.Errorf("store.GetState(ctx): %s", err)
	}
	if on != false {
		t.Error("state should be off")
	}

	err = store.SetState(context.Background(), true)
	if err != nil {
		t.Errorf("store.SetState(ctx, true): %s", err)
	}
	on, err = store.GetState(context.Background())
	if err != nil {
		t.Errorf("store.GetState(ctx): %s", err)
	}
	if on != true {
		t.Error("state should be on")
	}
}
