//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package killswitch_test

import (
	"testing"

	"eintopf.info/service/killswitch"
	"eintopf.info/service/killswitch/testutil"
)

func TestKillSwitchService(t *testing.T) {
	store := killswitch.NewMemoryStore()
	testutil.TestKillSwitchService(t, store)
}
