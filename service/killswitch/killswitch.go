//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package killswitch

import (
	"context"
	"fmt"

	"eintopf.info/internal/xerror"
)

// Service defines an interface for interacting with a killswitch service.
type Service interface {
	// TurnOff tries to set the the state to off.
	// Returns an error if the hash is invalid.
	TurnOff(ctx context.Context, hash string) error
	// TurnOn tries to set the the state to on.
	// Returns an error if the hash is invalid.
	TurnOn(ctx context.Context, hash string) error
	// On returns wether the current state is set to on.
	On(ctx context.Context) (bool, error)
}

// Storer provides methods to store or retrive the killswitch state.
// The state can either be on or off.
type Storer interface {
	// SetState sets the state of the killswitch.
	SetState(ctx context.Context, on bool) error
	// GetState retrieves the state of the killswitch.
	GetState(ctx context.Context) (bool, error)
}

// NewService returns a new killswitch service. It takes a list of hashes, that
// can be used to turn the state on or off.
func NewService(storer Storer, hashes []string) Service {
	s := &service{
		hashes: make(map[string]struct{}, len(hashes)),

		storer: storer,
	}

	for _, hash := range hashes {
		s.hashes[hash] = struct{}{}
	}

	return s
}

type service struct {
	hashes map[string]struct{}

	storer Storer
}

// ErrInvalidHash indicates an invalid hash was provided.
var ErrInvalidHash = xerror.BadInputError{Err: fmt.Errorf("invalid hash")}

func (s *service) TurnOn(ctx context.Context, hash string) error {
	if _, ok := s.hashes[hash]; !ok {
		return ErrInvalidHash
	}
	return s.storer.SetState(ctx, false)
}

func (s *service) TurnOff(ctx context.Context, hash string) error {
	if _, ok := s.hashes[hash]; !ok {
		return ErrInvalidHash
	}
	return s.storer.SetState(ctx, true)
}

func (s *service) On(ctx context.Context) (bool, error) {
	return s.storer.GetState(ctx)
}
