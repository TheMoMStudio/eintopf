//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package killswitch

import (
	"net/http"
	"strings"

	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/xhttp"
)

// Router returns a new killswitch router.
func Router(service Service) func(chi.Router) {
	server := &server{service}
	return func(r chi.Router) {
		r.Options("/turnon", xhttp.CorsHandler)

		// swagger:route GET /killswitch/turnon/{hash} killswitch kill
		//
		// Sets the internal state to on, provided a valid hash was provided.
		//
		//     Responses:
		//       200: loginSuccess
		//       400: badRequest
		//       500: internalError
		r.Get("/turnon/{hash}", server.turnOn)

		r.Options("/turnoff", xhttp.CorsHandler)

		// swagger:route GET /killswitch/turnoff/{hash} killswitch resurrect
		//
		// Sets the internal state to off, provided the hash is valid
		// When the state is off, the server will respond with a 503 status
		// code.
		//
		//
		//     Responses:
		//       200: loginSuccess
		//       400: badRequest
		//       500: internalError
		//       503: serviceUnavailable
		r.Get("/turnoff/{hash}", server.turnOff)
	}
}

type server struct {
	service Service
}

func (s *server) turnOn(w http.ResponseWriter, r *http.Request) {
	hash := chi.URLParam(r, "hash")
	err := s.service.TurnOn(r.Context(), hash)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	w.Write([]byte{})
}

func (s *server) turnOff(w http.ResponseWriter, r *http.Request) {
	hash := chi.URLParam(r, "hash")
	err := s.service.TurnOff(r.Context(), hash)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	w.Write([]byte{})
}

// Middleware returns a new killswitch middleware.
// It checks wether the state is on. If not it writes a 503 response.
func Middleware(service Service) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			on, err := service.On(r.Context())
			if err != nil {
				xhttp.WriteInternalError(r.Context(), w, err)
			}
			if !on && !strings.Contains(r.RequestURI, "turnon") {
				// Cancel the request when the state is off and the
				// turnon endpoint wasn't called.
				xhttp.WriteServiceUnavailable(w)
				return
			}
			next.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}
