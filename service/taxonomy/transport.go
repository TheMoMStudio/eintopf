// Copyright (C) 2024 The Eintopf authors
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package taxonomy

import (
	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/crud"
	"eintopf.info/internal/xhttp"
	"eintopf.info/service/auth"
)

// swagger:response findCategorysResponse
type findResponse struct {
	// in:body
	Categorys []*Category

	// in:header
	TotalCategorys int `json:"x-total-count"`
}

// swagger:parameters createCategory
type createRequest struct {
	// in:body
	Category *NewCategory
}

// swagger:response createCategoryResponse
type createResponse struct {
	// in:body
	Category *Category
}

// swagger:parameters findCategory
type findByIDRequest struct {
	// in:query
	ID string `json:"id"`
}

// swagger:response findCategoryResponse
type findByIDResponse struct {
	// in:body
	Category *Category
}

type FindResponse[Model any] struct {
	// in:body
	Model *Model
}

// swagger:parameters updateCategory
type updateRequest struct {
	// in:body
	Category *Category
}

// swagger:response updateCategoryResponse
type updateResponse struct {
	// in:body
	Category *Category
}

// swagger:parameters deleteCategory
type deleteRequest struct {
	// in:query
	ID string `json:"id"`
}

// swagger:response deleteCategoryResponse
type deleteResponse struct{}

func CategoryRouter(storer CategoryStorer, authService auth.Service) func(chi.Router) {
	handler := crud.NewHandler(storer)
	return func(r chi.Router) {
		r.Options("/", xhttp.CorsHandler)

		// swagger:route GET /categories/ category findCategories
		//
		// Retrieves all categorys.
		//
		// Parameters:
		//   + name: offset
		//     description: offset in category list
		//     in: query
		//     type: integer
		//     required: false
		//   + name: limit
		//     description: limits the category list
		//     in: query
		//     type: integer
		//     required: false
		//   + name: sort
		//     description: field that gets sorted
		//     in: query
		//     type: string
		//     required: false
		//   + name: order
		//     description: sort order ("ASC" or "DESC")
		//     in: query
		//     type: string
		//     required: false
		//   + name: filters
		//     description: filters get combined with AND logic
		//     in: query
		//     type: object
		//     required: false
		//
		//     Responses:
		//       200: findCategorysResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       500: internalError
		//       503: serviceUnavailable
		r.Get("/", handler.Find)

		// swagger:route POST /categories/ category createCategory
		//
		// Creates a new category.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: createCategoryResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
			Post("/", handler.Create)

		r.Options("/{id}", xhttp.CorsHandler)

		// swagger:route GET /categories/{id} category findCategory
		//
		// Finds the category with the given id.
		//
		//     Responses:
		//       200: findCategoryResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.Get("/{id}", handler.FindByID)

		// swagger:route PUT /categories/{id} category updateCategory
		//
		// Updates the category with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: updateCategoryResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
			Put("/{id}", handler.Update)

		// swagger:route DELETE /categories/{id} category deleteCategory
		//
		// Deletes the category with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: deleteCategoryResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
			Delete("/{id}", handler.Delete)
	}
}

// swagger:response findTopicsResponse
type findTopicResponse struct {
	// in:body
	Topics []*Topic

	// in:header
	TotalTopics int `json:"x-total-count"`
}

// swagger:parameters createTopic
type createTopicRequest struct {
	// in:body
	Topic *NewTopic
}

// swagger:response createTopicResponse
type createTopicResponse struct {
	// in:body
	Topic *Topic
}

// swagger:parameters findTopic
type findTopicByIDRequest struct {
	// in:query
	ID string `json:"id"`
}

// swagger:response findTopicResponse
type findTopicByIDResponse struct {
	// in:body
	Topic *Topic
}

type FindTopicResponse[Model any] struct {
	// in:body
	Model *Model
}

// swagger:parameters updateTopic
type updateTopicRequest struct {
	// in:body
	Topic *Topic
}

// swagger:response updateTopicResponse
type updateTopicResponse struct {
	// in:body
	Topic *Topic
}

// swagger:parameters deleteTopic
type deleteTopicRequest struct {
	// in:query
	ID string `json:"id"`
}

// swagger:response deleteTopicResponse
type deleteTopicResponse struct{}

func TopicRouter(storer TopicStorer, authService auth.Service) func(chi.Router) {
	handler := crud.NewHandler(storer)
	return func(r chi.Router) {
		r.Options("/", xhttp.CorsHandler)

		// swagger:route GET /topics/ topic findTopics
		//
		// Retrieves all topics.
		//
		// Parameters:
		//   + name: offset
		//     description: offset in topic list
		//     in: query
		//     type: integer
		//     required: false
		//   + name: limit
		//     description: limits the topic list
		//     in: query
		//     type: integer
		//     required: false
		//   + name: sort
		//     description: field that gets sorted
		//     in: query
		//     type: string
		//     required: false
		//   + name: order
		//     description: sort order ("ASC" or "DESC")
		//     in: query
		//     type: string
		//     required: false
		//   + name: filters
		//     description: filters get combined with AND logic
		//     in: query
		//     type: object
		//     required: false
		//
		//     Responses:
		//       200: findTopicsResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       500: internalError
		//       503: serviceUnavailable
		r.Get("/", handler.Find)

		// swagger:route POST /topics/ topic createTopic
		//
		// Creates a new topic.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: createTopicResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
			Post("/", handler.Create)

		r.Options("/{id}", xhttp.CorsHandler)

		// swagger:route GET /topics/{id} topic findTopic
		//
		// Finds the topic with the given id.
		//
		//     Responses:
		//       200: findTopicResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.Get("/{id}", handler.FindByID)

		// swagger:route PUT /topics/{id} topic updateTopic
		//
		// Updates the topic with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: updateTopicResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
			Put("/{id}", handler.Update)

		// swagger:route DELETE /topics/{id} topic deleteTopic
		//
		// Deletes the topic with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: deleteTopicResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
			Delete("/{id}", handler.Delete)
	}
}
