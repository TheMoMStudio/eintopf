// Copyright (C) 2024 The Eintopf authors
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package taxonomy

import (
	"context"

	"eintopf.info/internal/crud"
	"eintopf.info/service/dbmigration"
	"github.com/jmoiron/sqlx"
)

func NewCategoryMemoryStore() *crud.MemoryStore[NewCategory, Category, Filters] {
	return crud.NewMemoryStore[NewCategory, Category, Filters](CategoryFromNewCategory, nil)
}

func NewCategorySqlStore(db *sqlx.DB, migrationService dbmigration.Service) (*CategorySqlStore, error) {
	store := &CategorySqlStore{
		db,
		migrationService,
		crud.NewSqlStore(db, categoryTable, CategoryFromNewCategory, nil),
	}
	if err := store.runMigrations(context.Background()); err != nil {
		return nil, err
	}
	return store, nil
}

type CategorySqlStore struct {
	db               *sqlx.DB
	migrationService dbmigration.Service

	*crud.SqlStore[NewCategory, Category, Filters]
}

func (s *CategorySqlStore) runMigrations(ctx context.Context) error {
	return s.migrationService.RunMigrations(ctx, []dbmigration.Migration{
		dbmigration.NewMigration("createCategoryTable", s.createCategoryTable, nil),
	})
}

func (s *CategorySqlStore) createCategoryTable(ctx context.Context) error {
	_, err := s.db.ExecContext(ctx, `
        CREATE TABLE IF NOT EXISTS category (
            id VARCHAR(32) NOT NULL PRIMARY KEY UNIQUE,
            name VARCHAR(32) NOT NULL,
            headline VARCHAR(64) NOT NULL,
            description TEXT NOT NULL
        );
    `)
	return err
}

var categoryTable = crud.SqlTable[Filters]{
	IDField:        "id",
	Table:          "category",
	Fields:         []string{"id", "name", "headline", "description"},
	SortableFields: []string{"id", "name"},
}

func NewTopicMemoryStore() *crud.MemoryStore[NewTopic, Topic, Filters] {
	return crud.NewMemoryStore[NewTopic, Topic, Filters](TopicFromNewTopic, nil)
}

func NewTopicSqlStore(db *sqlx.DB, migrationService dbmigration.Service) (*TopicSqlStore, error) {
	store := &TopicSqlStore{
		db,
		migrationService,
		crud.NewSqlStore(db, topicTable, TopicFromNewTopic, nil),
	}
	if err := store.runMigrations(context.Background()); err != nil {
		return nil, err
	}
	return store, nil
}

type TopicSqlStore struct {
	db               *sqlx.DB
	migrationService dbmigration.Service

	*crud.SqlStore[NewTopic, Topic, Filters]
}

func (s *TopicSqlStore) runMigrations(ctx context.Context) error {
	return s.migrationService.RunMigrations(ctx, []dbmigration.Migration{
		dbmigration.NewMigration("createTopicTable", s.createTopicTable, nil),
	})
}

func (s *TopicSqlStore) createTopicTable(ctx context.Context) error {
	_, err := s.db.ExecContext(ctx, `
        CREATE TABLE IF NOT EXISTS topic (
            id VARCHAR(32) NOT NULL PRIMARY KEY UNIQUE,
            name VARCHAR(32) NOT NULL,
            headline VARCHAR(64) NOT NULL,
            description TEXT NOT NULL
        );
    `)
	return err
}

var topicTable = crud.SqlTable[Filters]{
	IDField:        "id",
	Table:          "topic",
	Fields:         []string{"id", "name", "headline", "description"},
	SortableFields: []string{"id", "name"},
}
