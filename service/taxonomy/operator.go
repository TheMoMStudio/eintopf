// Copyright (C) 2024 The Eintopf authors
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package taxonomy

import (
	"context"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
	"eintopf.info/service/oqueue"
)

type CategoryOperator struct {
	storer CategoryStorer
	queue  oqueue.Service
}

func NewCategoryOperator(storer CategoryStorer, queue oqueue.Service) *CategoryOperator {
	return &CategoryOperator{storer: storer, queue: queue}
}

type CategoryCreateOperation struct {
	Category *Category
	userID   string
}

func (o CategoryCreateOperation) UserID() string {
	return o.userID
}

func (i *CategoryOperator) Create(ctx context.Context, newCategory *NewCategory) (*Category, error) {
	category, err := i.storer.Create(ctx, newCategory)
	if err != nil {
		return category, err
	}
	if category == nil {
		return nil, nil
	}

	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return nil, err
	}
	i.queue.AddOperation(CategoryCreateOperation{
		Category: category,
		userID:   userID,
	})

	return category, nil
}

type CategoryUpdateOperation struct {
	Category *Category
	userID   string
}

func (o CategoryUpdateOperation) UserID() string {
	return o.userID
}

func (i *CategoryOperator) Update(ctx context.Context, category *Category) (*Category, error) {
	category, err := i.storer.Update(ctx, category)
	if err != nil {
		return category, err
	}
	if category == nil {
		return nil, nil
	}

	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return nil, err
	}
	i.queue.AddOperation(CategoryUpdateOperation{
		Category: category,
		userID:   userID,
	})

	return category, nil
}

type CategoryDeleteOperation struct {
	ID     string
	userID string
}

func (o CategoryDeleteOperation) UserID() string {
	return o.userID
}

func (i *CategoryOperator) Delete(ctx context.Context, id string) error {
	err := i.storer.Delete(ctx, id)
	if err != nil {
		return err
	}

	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return err
	}
	i.queue.AddOperation(CategoryDeleteOperation{
		ID:     id,
		userID: userID,
	})

	return nil
}

func (i *CategoryOperator) FindByID(ctx context.Context, id string) (*Category, error) {
	return i.storer.FindByID(ctx, id)
}

func (i *CategoryOperator) Find(ctx context.Context, params *crud.FindParams[Filters]) ([]*Category, int, error) {
	return i.storer.Find(ctx, params)
}

type TopicOperator struct {
	storer TopicStorer
	queue  oqueue.Service
}

func NewTopicOperator(storer TopicStorer, queue oqueue.Service) *TopicOperator {
	return &TopicOperator{storer: storer, queue: queue}
}

type TopicCreateOperation struct {
	Topic  *Topic
	userID string
}

func (o TopicCreateOperation) UserID() string {
	return o.userID
}

func (i *TopicOperator) Create(ctx context.Context, newTopic *NewTopic) (*Topic, error) {
	category, err := i.storer.Create(ctx, newTopic)
	if err != nil {
		return category, err
	}
	if category == nil {
		return nil, nil
	}

	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return nil, err
	}
	i.queue.AddOperation(TopicCreateOperation{
		Topic:  category,
		userID: userID,
	})

	return category, nil
}

type TopicUpdateOperation struct {
	Topic  *Topic
	userID string
}

func (o TopicUpdateOperation) UserID() string {
	return o.userID
}

func (i *TopicOperator) Update(ctx context.Context, category *Topic) (*Topic, error) {
	category, err := i.storer.Update(ctx, category)
	if err != nil {
		return category, err
	}
	if category == nil {
		return nil, nil
	}

	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return nil, err
	}
	i.queue.AddOperation(TopicUpdateOperation{
		Topic:  category,
		userID: userID,
	})

	return category, nil
}

type TopicDeleteOperation struct {
	ID     string
	userID string
}

func (o TopicDeleteOperation) UserID() string {
	return o.userID
}

func (i *TopicOperator) Delete(ctx context.Context, id string) error {
	err := i.storer.Delete(ctx, id)
	if err != nil {
		return err
	}

	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return err
	}
	i.queue.AddOperation(CategoryDeleteOperation{
		ID:     id,
		userID: userID,
	})

	return nil
}

func (i *TopicOperator) FindByID(ctx context.Context, id string) (*Topic, error) {
	return i.storer.FindByID(ctx, id)
}

func (i *TopicOperator) Find(ctx context.Context, params *crud.FindParams[Filters]) ([]*Topic, int, error) {
	return i.storer.Find(ctx, params)
}
