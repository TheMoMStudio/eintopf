// Copyright (C) 2024 The Eintopf authors
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package taxonomy

import (
	"context"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
)

type CategoryAuthorizer struct {
	store CategoryStorer
}

// NewAuthorizer wraps the given store with authorization methods.
func NewCategoryAuthorizer(store CategoryStorer) *CategoryAuthorizer {
	return &CategoryAuthorizer{store: store}
}

// Create is allowed by
//   - an admin
//   - a moderator
//   - internally
func (a *CategoryAuthorizer) Create(ctx context.Context, category *NewCategory) (*Category, error) {
	if err := notNormalUser(ctx); err != nil {
		return nil, err
	}
	return a.store.Create(ctx, category)
}

// Update is allowed by
//   - an admin
//   - a moderator
//   - internally
func (a *CategoryAuthorizer) Update(ctx context.Context, category *Category) (*Category, error) {
	if err := notNormalUser(ctx); err != nil {
		return nil, err
	}
	return a.store.Update(ctx, category)
}

// Delete is allowed by
//   - an admin
//   - a moderator
//   - internally
func (a *CategoryAuthorizer) Delete(ctx context.Context, id string) error {
	if err := notNormalUser(ctx); err != nil {
		return err
	}
	return a.store.Delete(ctx, id)
}

// FindByID is allowed by all.
func (a *CategoryAuthorizer) FindByID(ctx context.Context, id string) (*Category, error) {
	return a.store.FindByID(ctx, id)
}

// Find is allowed by all
func (a *CategoryAuthorizer) Find(ctx context.Context, params *crud.FindParams[Filters]) ([]*Category, int, error) {
	return a.store.Find(ctx, params)
}

type TopicAuthorizer struct {
	store TopicStorer
}

func NewTopicAuthorizer(store TopicStorer) *TopicAuthorizer {
	return &TopicAuthorizer{store: store}
}

// Create is allowed by
//   - an admin
//   - a moderator
//   - internally
func (a *TopicAuthorizer) Create(ctx context.Context, topic *NewTopic) (*Topic, error) {
	if err := notNormalUser(ctx); err != nil {
		return nil, err
	}
	return a.store.Create(ctx, topic)
}

// Update is allowed by
//   - an admin
//   - a moderator
//   - internally
func (a *TopicAuthorizer) Update(ctx context.Context, topic *Topic) (*Topic, error) {
	if err := notNormalUser(ctx); err != nil {
		return nil, err
	}
	return a.store.Update(ctx, topic)
}

// Delete is allowed by
//   - an admin
//   - a moderator
//   - internally
func (a *TopicAuthorizer) Delete(ctx context.Context, id string) error {
	if err := notNormalUser(ctx); err != nil {
		return err
	}
	return a.store.Delete(ctx, id)
}

// FindByID is allowed by all.
func (a *TopicAuthorizer) FindByID(ctx context.Context, id string) (*Topic, error) {
	return a.store.FindByID(ctx, id)
}

// Find is allowed by all
func (a *TopicAuthorizer) Find(ctx context.Context, params *crud.FindParams[Filters]) ([]*Topic, int, error) {
	return a.store.Find(ctx, params)
}

func notNormalUser(ctx context.Context) error {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return auth.ErrUnauthorized
	}
	if role == auth.RoleNormal {
		return auth.ErrUnauthorized
	}
	return nil
}
