// Copyright (C) 2024 The Eintopf authors
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package taxonomy

import (
	"context"

	"eintopf.info/internal/crud"
)

type Category struct {
	ID          string `json:"id" db:"id"`
	Name        string `json:"name" db:"name"`
	Headline    string `json:"headline" db:"headline"`
	Description string `json:"description" db:"description"`
}

func (c Category) Identifier() string {
	return c.ID
}

type NewCategory struct {
	Name        string `json:"name" db:"name"`
	Headline    string `json:"headline" db:"headline"`
	Description string `json:"description" db:"description"`
}

func CategoryFromNewCategory(c *NewCategory, id string) *Category {
	return &Category{
		ID:          id,
		Name:        c.Name,
		Headline:    c.Headline,
		Description: c.Description,
	}
}

type Topic struct {
	ID          string `json:"id" db:"id"`
	Name        string `json:"name" db:"name"`
	Headline    string `json:"headline" db:"headline"`
	Description string `json:"description" db:"description"`
}

func (t Topic) Identifier() string {
	return t.ID
}

type NewTopic struct {
	Name        string `json:"name" db:"name"`
	Headline    string `json:"headline" db:"headline"`
	Description string `json:"description" db:"description"`
}

func TopicFromNewTopic(t *NewTopic, id string) *Topic {
	return &Topic{
		ID:          id,
		Name:        t.Name,
		Headline:    t.Headline,
		Description: t.Description,
	}
}

type Filters struct {
	ID       *string `json:"id" db:"id"`
	Name     *string `json:"name" db:"name"`
	LikeName *string `json:"likeName" db:"name"`
}

type (
	CategoryStorer = crud.Storer[NewCategory, Category, Filters]
	TopicStorer    = crud.Storer[NewTopic, Topic, Filters]
)

// CategoryService defines a service to manage categories.
type CategoryService interface {
	CategoryStorer
}

type categoryservice struct {
	store CategoryStorer
}

type TopicService interface {
	TopicStorer
}

type topicservice struct {
	store TopicStorer
}

func NewCategoryService(store CategoryStorer) CategoryService {
	return &categoryservice{store}
}

func (s *categoryservice) Create(ctx context.Context, category *NewCategory) (*Category, error) {

	return s.store.Create(ctx, category)
}

func (s *categoryservice) Update(ctx context.Context, category *Category) (*Category, error) {
	return s.store.Update(ctx, category)
}

func (s *categoryservice) Delete(ctx context.Context, id string) error {
	return s.store.Delete(ctx, id)
}

func (s *categoryservice) FindByID(ctx context.Context, id string) (*Category, error) {
	return s.store.FindByID(ctx, id)
}

func (s *categoryservice) Find(ctx context.Context, params *crud.FindParams[Filters]) ([]*Category, int, error) {
	return s.store.Find(ctx, params)
}

func NewTopicService(store TopicStorer) TopicService {
	return &topicservice{store}
}

func (s *topicservice) Create(ctx context.Context, topic *NewTopic) (*Topic, error) {

	return s.store.Create(ctx, topic)
}

func (s *topicservice) Update(ctx context.Context, topic *Topic) (*Topic, error) {
	return s.store.Update(ctx, topic)
}

func (s *topicservice) Delete(ctx context.Context, id string) error {
	return s.store.Delete(ctx, id)
}

func (s *topicservice) FindByID(ctx context.Context, id string) (*Topic, error) {
	return s.store.FindByID(ctx, id)
}

func (s *topicservice) Find(ctx context.Context, params *crud.FindParams[Filters]) ([]*Topic, int, error) {
	return s.store.Find(ctx, params)
}
