//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package passwordrec

import (
	"bytes"
	"context"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"net/url"
	"strconv"
	"time"

	"eintopf.info"
	"eintopf.info/internal/crud"
	"eintopf.info/internal/xerror"
	"eintopf.info/service/auth"
	"eintopf.info/service/user"
)

// PasswordRecovery is the password recovery model.
type PasswordRecovery struct {
	ID          string    `json:"id" db:"id"`
	UserID      string    `json:"user_id" db:"user_id"`
	Email       string    `json:"email" db:"email"`
	Token       string    `json:"token" db:"token"`
	RequestedAt time.Time `json:"requested_at" db:"requested_at"`
}

func (p PasswordRecovery) Identifier() string { return p.ID }

// InvitationFromNewInvitation converts a NewInvitation to an Invitation with a
// given id.
func PasswordRecoveryFromNewPasswordRecovery(newPasswordRecvoery *NewPasswordRecovery, id string) *PasswordRecovery {
	return &PasswordRecovery{
		ID:          id,
		UserID:      newPasswordRecvoery.UserID,
		Email:       newPasswordRecvoery.Email,
		Token:       newPasswordRecvoery.Token,
		RequestedAt: newPasswordRecvoery.RequestedAt,
	}
}

// NewPasswordRecovery is the model used to create new password recoveries.
type NewPasswordRecovery struct {
	UserID      string    `json:"user_id"`
	Email       string    `json:"email"`
	Token       string    `json:"token"`
	RequestedAt time.Time `json:"requested_at"`
}

// Storer defines an interface for accessing the PasswordRecovery model.
type Storer crud.Storer[NewPasswordRecovery, PasswordRecovery, FindFilters]

// FindFilters defines the possible filters for the find method.
type FindFilters struct {
	ID          *string
	UserID      *string
	Email       *string
	Token       *string
	RequestedAt *time.Time
}

type Service struct {
	mailService Mailer
	userService user.Storer
	store       Storer
	theme       eintopf.Theme

	baseURL string
}

// Config  is used to configure the recover email.
type Config struct {
	RecoveryURL string
	Subject     string
}

type Mailer interface {
	Send(email, subject, body string) error
}

// NewService returns a new recovery service.
func NewService(mailService Mailer, userService user.Storer, theme eintopf.Theme, store Storer, baseURL string) *Service {
	return &Service{mailService: mailService, userService: userService, store: store, theme: theme, baseURL: baseURL}
}

func (s *Service) RequestRecovery(ctx context.Context, email string) error {
	users, _, err := s.userService.Find(auth.ContextWithRole(ctx, auth.RoleInternal), &crud.FindParams[user.FindFilters]{
		Filters: &user.FindFilters{Email: &email},
	})
	if err != nil {
		return nil
	}
	if len(users) == 0 {
		return nil
	}

	passwordRecovery := &NewPasswordRecovery{
		UserID:      users[0].ID,
		Email:       email,
		RequestedAt: time.Now(),
	}
	h := sha256.New()
	h.Write([]byte(email + strconv.FormatInt(passwordRecovery.RequestedAt.Unix(), 10)))

	sum := h.Sum(nil)
	token := make([]byte, hex.EncodedLen(len(sum)))
	hex.Encode(token, sum)

	passwordRecovery.Token = string(token)

	_, err = s.store.Create(ctx, passwordRecovery)
	if err != nil {
		return err
	}

	return s.sendRecoveryEmail(ctx, email, string(token))
}

func (s *Service) sendRecoveryEmail(ctx context.Context, email, token string) error {
	b := new(bytes.Buffer)

	u, err := url.JoinPath(s.baseURL, "backstage", "passwordRecovery")
	if err != nil {
		return err
	}
	err = s.theme.Render(ctx, b, []string{"mail/passwordrecovery"}, map[string]any{
		"RecoveryURL": fmt.Sprintf("%s?token=%s", u, token),
	}, nil, false)
	if err != nil {
		return err
	}

	return s.mailService.Send(email, "Passwort Zurücksetzten", b.String())
}

var ErrInvalidRecoveryToken = xerror.BadInputError{Err: fmt.Errorf("invalid recovery token")}
var ErrExpiredRecoveryToken = xerror.BadInputError{Err: fmt.Errorf("expired recovery token")}

func (s *Service) SetPassword(ctx context.Context, token string, password string) error {
	recoveries, _, err := s.store.Find(ctx, &crud.FindParams[FindFilters]{Filters: &FindFilters{Token: &token}})
	if err != nil {
		return err
	}
	if len(recoveries) == 0 {
		return ErrInvalidRecoveryToken
	}
	recovery := recoveries[0]

	if time.Now().Sub(recovery.RequestedAt)/time.Hour > 1 {
		return ErrExpiredRecoveryToken
	}

	u, err := s.userService.FindByID(auth.ContextWithRole(ctx, auth.RoleInternal), recovery.UserID)
	if err != nil {
		return err
	}
	u.Password = password

	_, err = s.userService.Update(auth.ContextWithRole(ctx, auth.RoleInternal), u)
	if err != nil {
		return err
	}
	return s.store.Delete(ctx, recovery.ID)
}
