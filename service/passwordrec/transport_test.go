// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package passwordrec_test

import (
	"context"
	"encoding/json"
	"testing"

	"eintopf.info"
	"eintopf.info/internal/xhttptest"
	"eintopf.info/service/passwordrec"
	"eintopf.info/service/user"
	"eintopf.info/test"
)

func TestTransport(t *testing.T) {
	emailService := test.NewMailer()
	userService := user.NewMemoryStore()
	staticTheme, err := eintopf.NewTheme("foo", "testdata/themes", nil, nil)
	if err != nil {
		t.Fatal(err)
	}
	service := passwordrec.NewService(emailService, userService, staticTheme, passwordrec.NewMemoryStore(), "http://localhost")

	_, err = userService.Create(context.Background(), &user.NewUser{
		Email: "foo@example.com",
	})
	if err != nil {
		t.Fatal(err)
	}

	tests := []xhttptest.HttpTest{
		{
			Name:       "RequestWithExistingEmail",
			URI:        "/requestRecovery",
			Method:     "POST",
			Body:       mustMarshal(&passwordrec.PasswordRecovery{Email: "foo@example.com"}),
			WantStatus: 200,
		},
		{
			Name:       "RequestWithNonExistingEmailReturns200",
			URI:        "/requestRecovery",
			Method:     "POST",
			Body:       mustMarshal(&passwordrec.PasswordRecovery{Email: "bar@example.com"}),
			WantStatus: 200,
		},
	}

	xhttptest.TestRouter(t, passwordrec.Router(service), tests)
}

func mustMarshal(v interface{}) string {
	data, err := json.Marshal(v)
	if err != nil {
		panic("mustMarshal failed")
	}
	return string(data)
}
