//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package passwordrec

import (
	"encoding/json"
	"io"
	"net/http"

	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/xhttp"
)

// Router returns a new passwordrec router.
func Router(service *Service) func(chi.Router) {
	server := &server{service}
	return func(r chi.Router) {
		r.Options("/requestRecovery", xhttp.CorsHandler)

		// swagger:route POST /passwordrec/requestRecovery passwordrec recoveryRequest
		//
		// Requests a password recovery.
		//
		//     Responses:
		//       200: emptyResponse
		//       500: internalError
		//       503: serviceUnavailable
		r.Post("/requestRecovery", server.requestRecovery)

		r.Options("/setPassword", xhttp.CorsHandler)

		// swagger:route POST /passwordrec/setPassword passwordrec setPassword
		//
		// Sets the new password, using a recovery token.
		//
		//
		//     Responses:
		//       200: emptyResponse
		//       400: badRequest
		//       500: internalError
		//       503: serviceUnavailable
		r.Post("/setPassword", server.setPassword)
	}
}

type server struct {
	service *Service
}

func (s *server) requestRecovery(w http.ResponseWriter, r *http.Request) {
	req, err := readRecoveryRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	err = s.service.RequestRecovery(r.Context(), req.RecoveryRequest.Email)
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, err)
		return
	}
	w.Write([]byte{})
}

// swagger:parameters recoveryRequest
type recoveryRequest struct {
	// in:body
	RecoveryRequest *RecoveryRequest
}

type RecoveryRequest struct {
	Email string `json:"email"`
}

func readRecoveryRequest(r *http.Request) (*recoveryRequest, error) {
	data, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	req := &RecoveryRequest{}
	err = json.Unmarshal(data, req)
	if err != nil {
		return nil, err
	}
	return &recoveryRequest{RecoveryRequest: req}, nil
}

func (s *server) setPassword(w http.ResponseWriter, r *http.Request) {
	req, err := readSetPasswordRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	err = s.service.SetPassword(r.Context(), req.SetPasswordRequest.Token, req.SetPasswordRequest.Password)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	w.Write([]byte{})
}

// swagger:parameters setPassword
type setPasswordRequest struct {
	// in:body
	SetPasswordRequest *SetPasswordRequest
}

type SetPasswordRequest struct {
	Token    string `json:"token"`
	Password string `json:"password"`
}

func readSetPasswordRequest(r *http.Request) (*setPasswordRequest, error) {
	data, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	req := &SetPasswordRequest{}
	err = json.Unmarshal(data, req)
	if err != nil {
		return nil, err
	}
	return &setPasswordRequest{SetPasswordRequest: req}, nil
}
