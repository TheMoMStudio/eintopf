//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package passwordrec

import (
	"context"
	"database/sql"
	"fmt"
	"strings"

	"eintopf.info/internal/crud"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

// NewSqlStore returns a new sql db passwordrec store.
func NewSqlStore(db *sqlx.DB) *SqlStore {
	return &SqlStore{db: db}
}

type SqlStore struct {
	db *sqlx.DB
}

func (s *SqlStore) RunMigrations(ctx context.Context) error {
	_, err := s.db.ExecContext(ctx, `
        CREATE TABLE IF NOT EXISTS passwordrecs (
            id varchar(32) NOT NULL PRIMARY KEY UNIQUE,
            user_id varchar(32),
            email VARCHAR(128),
            token VARCHAR(128),
            requested_at timestamp
        );
    `)
	return err
}

func (s *SqlStore) Create(ctx context.Context, newPasswordRecovery *NewPasswordRecovery) (*PasswordRecovery, error) {
	passwordrec := PasswordRecoveryFromNewPasswordRecovery(newPasswordRecovery, uuid.New().String())
	_, err := s.db.NamedExecContext(ctx, `
        INSERT INTO passwordrecs (
            id,
            user_id,
            email,
            token,
            requested_at
        ) VALUES (
            :id,
            :user_id,
            :email,
            :token,
            :requested_at
        )
    `, passwordrec)
	if err != nil {
		return nil, err
	}

	return passwordrec, nil
}

func (s *SqlStore) Update(ctx context.Context, passwordrec *PasswordRecovery) (*PasswordRecovery, error) {
	_, err := s.db.NamedExecContext(ctx, `
        UPDATE
            passwordrecs
        SET
            token=:published,
            created_at=:created_at,
            created_by=:created_by,
            used_by=:used_by
        WHERE
            id=:id
    `, passwordrec)
	if err != nil {
		return nil, err
	}

	return passwordrec, err
}

func (s *SqlStore) Delete(ctx context.Context, id string) error {
	_, err := s.db.ExecContext(ctx, "DELETE FROM passwordrecs WHERE id = $1", id)
	if err != nil {
		return err
	}

	return nil
}

func (s *SqlStore) FindByID(ctx context.Context, id string) (*PasswordRecovery, error) {
	passwordrec := &PasswordRecovery{}
	err := s.db.GetContext(ctx, passwordrec, `
        SELECT *
        FROM passwordrecs
        WHERE passwordrecs.id = $1
    `, id)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	return passwordrec, nil
}

var sortableFields = map[string]string{
	"id":          "id",
	"userID":      "user_id",
	"email":       "email",
	"requestedAt": "requested_at",
}

func (s *SqlStore) Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*PasswordRecovery, int, error) {
	query := `SELECT passwordrecs.* FROM passwordrecs`
	whereStatements := []string{}
	sqlParams := make(map[string]interface{})
	if params != nil {
		if params.Filters != nil {
			if params.Filters.ID != nil {
				whereStatements = append(whereStatements, "passwordrecs.id=:id")
				sqlParams["id"] = params.Filters.ID
			}
			if params.Filters.Token != nil {
				whereStatements = append(whereStatements, "passwordrecs.token=:token")
				sqlParams["token"] = params.Filters.Token
			}
			if params.Filters.UserID != nil {
				whereStatements = append(whereStatements, "passwordrecs.user_id=:user_id")
				sqlParams["user_id"] = params.Filters.UserID
			}
			if params.Filters.Email != nil {
				whereStatements = append(whereStatements, "passwordrecs.email=:email")
				sqlParams["email"] = params.Filters.Email
			}
			if params.Filters.Token != nil {
				whereStatements = append(whereStatements, "passwordrecs.token=:token")
				sqlParams["token"] = params.Filters.Token
			}
			if params.Filters.RequestedAt != nil {
				whereStatements = append(whereStatements, "passwordrecs.requested_at=:requested_at")
				sqlParams["requested_at"] = params.Filters.RequestedAt
			}
			if len(whereStatements) > 0 {
				query += " WHERE " + strings.Join(whereStatements, " AND ")
			}
		}

		if params.Sort != "" {
			sort, ok := sortableFields[params.Sort]
			if !ok {
				return nil, 0, fmt.Errorf("find passwordrecs: invalid sort field: %s", params.Sort)
			}
			order := "ASC"
			if params.Order == "DESC" {
				order = "DESC"
			}
			query += fmt.Sprintf(" ORDER BY %s %s", sort, order)
		}
		if params.Limit > 0 {
			query += fmt.Sprintf(" LIMIT %d", params.Limit)
		}
		if params.Offset > 0 {
			query += fmt.Sprintf(" OFFSET %d", params.Offset)
		}
	}
	rows, err := s.db.NamedQueryContext(ctx, query, sqlParams)
	if err != nil && err != sql.ErrNoRows {
		return nil, 0, fmt.Errorf("find passwordrecs: %s", params.Sort)
	}
	defer rows.Close()
	passwordrecs := make([]*PasswordRecovery, 0)
	for rows.Next() {
		passwordrec := PasswordRecovery{}
		rows.StructScan(&passwordrec)

		passwordrecs = append(passwordrecs, &passwordrec)
	}

	totalQuery := `SELECT COUNT(*) as total FROM passwordrecs`
	if len(whereStatements) > 0 {
		totalQuery += " WHERE " + strings.Join(whereStatements, " AND ")
	}
	totalPasswordRecoverys := struct {
		Total int `db:"total"`
	}{}
	rows, err = s.db.NamedQueryContext(ctx, totalQuery, sqlParams)
	if err != nil && err != sql.ErrNoRows {
		return nil, 0, fmt.Errorf("find passwordrecs: total: %s", params.Sort)
	}
	defer rows.Close()
	if rows.Next() {
		rows.StructScan(&totalPasswordRecoverys)
	}

	return passwordrecs, totalPasswordRecoverys.Total, nil
}
