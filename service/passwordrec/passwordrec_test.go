// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package passwordrec_test

import (
	"context"
	"strings"
	"testing"

	"eintopf.info"
	"eintopf.info/service/passwordrec"
	"eintopf.info/service/user"
	"eintopf.info/test"
)

func TestPasswordRecovery(t *testing.T) {
	emailService := test.NewMailer()
	userService := user.NewMemoryStore()
	staticTheme, err := eintopf.NewTheme("foo", "testdata/themes", nil, nil)
	if err != nil {
		t.Fatal(err)
	}
	service := passwordrec.NewService(emailService, userService, staticTheme, passwordrec.NewMemoryStore(), "http://localhost")

	u, err := userService.Create(context.Background(), &user.NewUser{
		Email: "foo@example.com",
	})
	if err != nil {
		t.Fatal(err)
	}

	err = service.RequestRecovery(context.Background(), "foo@example.com")
	if err != nil {
		t.Error(err)
	}

	if len(emailService.Mails) != 1 {
		t.Errorf("expected one mail, got %d", len(emailService.Mails))
	}

	if emailService.Mails[0].Subject != "Passwort Zurücksetzten" {
		t.Errorf("expected mail subject to be '%s', got %s", "Passwort Zurücksetzten", emailService.Mails[0].Subject)
	}
	splitted := strings.Split(emailService.Mails[0].Body, "=")
	if len(splitted) != 2 {
		t.Fatalf("recovery url is not correct: %s", emailService.Mails[0].Body)
	}

	wantURL := "http://localhost/backstage/passwordRecovery?token"
	if splitted[0] != wantURL {
		t.Errorf("expected url to be %s, got %s", wantURL, splitted[0])
	}

	pw := "my password"
	err = service.SetPassword(context.Background(), strings.TrimSpace(splitted[1]), pw)
	if err != nil {
		t.Error(err)
	}

	u, err = userService.FindByID(context.Background(), u.ID)
	if err != nil {
		t.Fatal(err)
	}
	if u.Password != pw {
		t.Errorf("expected password to be '%s', go '%s'", pw, u.Password)
	}
}
