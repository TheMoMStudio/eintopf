//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package rss_test

import (
	"context"
	"os"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"

	"eintopf.info/service/auth"
	"eintopf.info/service/event"
	"eintopf.info/service/eventsearch"
	"eintopf.info/service/group"
	"eintopf.info/service/oqueue"
	"eintopf.info/service/place"
	"eintopf.info/service/revent"
	"eintopf.info/service/rss"
	"eintopf.info/service/search"
	"eintopf.info/service/taxonomy"
)

func TestFeed(t *testing.T) {
	testFeed(t, testcase{
		events: []*event.NewEvent{
			{
				Published: true,
				Name:      "event0",
				Start:     time.Now().Add(time.Hour * 2),
			}, { // should be in rss feed, before event1
				Published: true,
				Name:      "event1",
				Start:     time.Now().Add(time.Hour),
			}, { // should not be in rss feed, not published
				Published: false,
				Name:      "event2",
				Start:     time.Now().Add(time.Hour * 2),
			}, { // should be in rss feed
				Published: true,
				Name:      "event3",
				Start:     time.Now().Add(time.Hour * 3),
			}, { // should not be in rss feed, start is before the current date
				Published: true,
				Name:      "event5",
				Start:     time.Now().Add(-1 * time.Hour * 3),
			},
		},
		getFeed: func(rssService rss.Service) (*rss.Feed, error) {
			return rssService.Feed(context.Background(), eventsearch.Options{}, &rss.Channel{Title: "events"})
		},
		wantFeed: &rss.Feed{
			Version: "2.0",
			Channel: &rss.Channel{
				Title: "events",
				Items: []rss.Item{
					{
						Title: "event1",
						Link:  "https://my.domain/event/1",
						Guid:  "1",
					},
					{
						Title: "event0",
						Link:  "https://my.domain/event/0",
						Guid:  "0",
					},
					{
						Title: "event3",
						Link:  "https://my.domain/event/3",
						Guid:  "3",
					},
				},
			},
		},
	})
}
func TestGroupFeed(t *testing.T) {
	testFeed(t, testcase{
		groups: []*group.NewGroup{
			{
				Name:        "some group",
				Description: "some description",
			},
			{
				Name:        "other group",
				Description: "some description",
			},
		},
		events: []*event.NewEvent{
			{
				Published:  true,
				Name:       "event0",
				Organizers: []string{"id:0"},
				Start:      time.Now().Add(time.Hour * 2),
			}, { // should be in rss feed, before event1
				Published:  true,
				Name:       "event1",
				Organizers: []string{"id:0"},
				Start:      time.Now().Add(time.Hour),
			}, { // should not be in rss feed, not published
				Published:  false,
				Name:       "event2",
				Organizers: []string{"id:0"},
				Start:      time.Now().Add(time.Hour * 2),
			}, { // should be in rss feed
				Published:  true,
				Name:       "event3",
				Organizers: []string{"id:0"},
				Start:      time.Now().Add(time.Hour * 3),
			}, { // should not be in rss feed, is from another location
				Published:  true,
				Name:       "event4",
				Organizers: []string{"id:1"},
				Start:      time.Now().Add(time.Hour * 3),
			}, { // should not be in rss feed, start is before the current date
				Published:  true,
				Name:       "event5",
				Organizers: []string{"id:0"},
				Start:      time.Now().Add(-1 * time.Hour * 3),
			},
		},
		getFeed: func(rssService rss.Service) (*rss.Feed, error) {
			return rssService.GroupFeed(context.Background(), "0")
		},
		wantFeed: &rss.Feed{
			Version: "2.0",
			Channel: &rss.Channel{
				Title:       "some group",
				Link:        "https://my.domain/group/0/rss",
				Description: "some description",
				Items: []rss.Item{
					{
						Title: "event1",
						Link:  "https://my.domain/event/1",
						Guid:  "1",
					},
					{
						Title: "event0",
						Link:  "https://my.domain/event/0",
						Guid:  "0",
					},
					{
						Title: "event3",
						Link:  "https://my.domain/event/3",
						Guid:  "3",
					},
				},
			},
		},
	})
}

func TestPlaceFeed(t *testing.T) {
	testFeed(t, testcase{
		dateMin: time.Date(2022, 0o4, 20, 0, 0, 0, 0, time.UTC),
		places: []*place.NewPlace{
			{
				Name:        "some place",
				Description: "some description",
			},
		},
		events: []*event.NewEvent{
			{
				Published: true,
				Name:      "event0",
				Location:  "id:0",
				Start:     time.Now().Add(time.Hour * 2),
			}, { // should be in rss feed, before event1
				Published: true,
				Name:      "event1",
				Location:  "id:0",
				Start:     time.Now().Add(time.Hour),
			}, { // should not be in rss feed, not published
				Published: false,
				Name:      "event2",
				Location:  "id:0",
				Start:     time.Now().Add(time.Hour * 2),
			}, { // should be in rss feed
				Published: true,
				Name:      "event3",
				Location:  "id:0",
				Start:     time.Now().Add(time.Hour * 3),
			}, { // should not be in rss feed, is from another location
				Published: true,
				Name:      "event4",
				Location:  "some Organizer",
				Start:     time.Now().Add(time.Hour * 3),
			}, { // should not be in rss feed, start is before the current date
				Published: true,
				Name:      "event5",
				Location:  "id:0",
				Start:     time.Now().Add(-1 * time.Hour * 3),
			},
		},
		getFeed: func(rssService rss.Service) (*rss.Feed, error) {
			return rssService.PlaceFeed(context.Background(), "0")
		},
		wantFeed: &rss.Feed{
			Version: "2.0",
			Channel: &rss.Channel{
				Title:       "some place",
				Link:        "https://my.domain/place/0/rss",
				Description: "some description",
				Items: []rss.Item{
					{
						Title: "event1",
						Link:  "https://my.domain/event/1",
						Guid:  "1",
					},
					{
						Title: "event0",
						Link:  "https://my.domain/event/0",
						Guid:  "0",
					},
					{
						Title: "event3",
						Link:  "https://my.domain/event/3",
						Guid:  "3",
					},
				},
			},
		},
	})
}

type testcase struct {
	dateMin  time.Time
	events   []*event.NewEvent
	places   []*place.NewPlace
	groups   []*group.NewGroup
	getFeed  func(rssService rss.Service) (*rss.Feed, error)
	wantFeed *rss.Feed
}

func testFeed(t *testing.T, tc testcase) {
	userCtx := auth.ContextWithRole(auth.ContextWithID(context.Background(), "user1"), auth.RoleInternal)

	queue := oqueue.NewQueue()

	eventService := event.NewService(event.NewOperator(event.NewMemoryStore(), queue), nil)
	reventService := revent.NewService(revent.NewMemoryStore(), eventService)
	groupService := group.NewService(group.NewMemoryStore())
	placeService := place.NewService(place.NewMemoryStore())
	categoryService := taxonomy.NewCategoryMemoryStore()
	topicService := taxonomy.NewTopicMemoryStore()

	searchService, err := search.NewService("TestPlaceFeed.bleve", time.Second, 1, 1, time.UTC)
	if err != nil {
		t.Errorf("search.NewService: %s", err)
	}
	defer searchService.Stop()
	defer cleanupIndex("TestPlaceFeed.bleve")

	eventsearchService := eventsearch.NewService(searchService, eventService, groupService, placeService, reventService, categoryService, topicService)
	queue.AddSubscriber(eventsearchService.ConsumeOperation, 1)

	for _, g := range tc.groups {
		_, err := groupService.Create(userCtx, g)
		if err != nil {
			t.Fatal(err)
		}
	}
	for _, p := range tc.places {
		_, err := placeService.Create(userCtx, p)
		if err != nil {
			t.Fatal(err)
		}
	}
	for _, e := range tc.events {
		e.OwnedBy = append(e.OwnedBy, "user1")
		_, err := eventService.Create(userCtx, e)
		if err != nil {
			t.Fatal(err)
		}
	}

	// Closing the queue waits for all index operations to be processed
	queue.Close()

	rssService := rss.NewService(eventsearchService, groupService, placeService, "https://my.domain")
	feed, err := tc.getFeed(rssService)

	if diff := cmp.Diff(tc.wantFeed, feed); diff != "" {
		t.Errorf("rss feed mismatch (-want +got):\n%s", diff)
	}
}

func cleanupIndex(name string) error {
	return os.RemoveAll(name)
}
