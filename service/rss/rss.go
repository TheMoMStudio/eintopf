//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package rss

import (
	"context"
	"encoding/xml"
	"fmt"
	"time"

	"eintopf.info/service/event"
	"eintopf.info/service/eventsearch"
	"eintopf.info/service/group"
	"eintopf.info/service/place"
)

type Service interface {
	Feed(ctx context.Context, opts eventsearch.Options, channel *Channel) (*Feed, error)
	GroupFeed(ctx context.Context, groupID string) (*Feed, error)
	PlaceFeed(ctx context.Context, placeID string) (*Feed, error)
}

type Feed struct {
	XMLName xml.Name `xml:"rss"`
	Version string   `xml:"version,attr"`
	Channel *Channel `xml:"channel"`
}

type Channel struct {
	XMLName     xml.Name `xml:"channel"`
	Title       string   `xml:"title"`
	Link        string   `xml:"link"`
	Description string   `xml:"description"`
	Items       []Item   `xml:"item"`
}

type Item struct {
	XMLName     xml.Name `xml:"item"`
	Title       string   `xml:"title"`
	Link        string   `xml:"link"`
	Description string   `xml:"description"`
	Guid        string   `xml:"guid,omitempty"`
}

type ServiceImpl struct {
	eventsearchService eventsearch.Service
	groupService       group.Service
	placeService       place.Service

	baseURL string
}

func NewService(eventsearchService eventsearch.Service, groupService group.Service, placeService place.Service, baseURL string) *ServiceImpl {
	return &ServiceImpl{
		eventsearchService: eventsearchService,
		groupService:       groupService,
		placeService:       placeService,

		baseURL: baseURL,
	}
}

func (s *ServiceImpl) Feed(ctx context.Context, opts eventsearch.Options, channel *Channel) (*Feed, error) {
	if opts.Sort == "" {
		opts.Sort = "start"
	}
	if opts.Filters == nil {
		opts.Filters = []eventsearch.Filter{eventsearch.ListedFilter{Listed: true}}
	} else {
		foundListedFilter := false
		for _, f := range opts.Filters {
			if _, ok := f.(eventsearch.ListedFilter); ok {
				foundListedFilter = true
				break
			}
		}
		if !foundListedFilter {
			opts.Filters = append(opts.Filters, eventsearch.ListedFilter{Listed: true})
		}
	}
	opts.Filters = append(opts.Filters, eventsearch.DateRangeFilter{DateMin: time.Now()})
	result, err := s.eventsearchService.Search(ctx, opts)
	if err != nil {
		return nil, err
	}
	return s.feedFromEvents(result.Events, channel)
}

func (s *ServiceImpl) feedFromEvents(events []*eventsearch.EventDocument, channel *Channel) (*Feed, error) {
	feed := &Feed{
		Version: "2.0",
		Channel: channel,
	}

	for _, e := range events {
		eventURL, err := event.URLFromID(s.baseURL, e.ID)
		if err != nil {
			return nil, fmt.Errorf("invalid event url: %s", err)
		}
		feed.Channel.Items = append(feed.Channel.Items, Item{
			Title:       e.Name,
			Link:        eventURL,
			Description: e.Description,
			Guid:        e.ID,
		})
	}

	return feed, nil
}

func (s *ServiceImpl) GroupFeed(ctx context.Context, groupID string) (*Feed, error) {
	groupURL, err := group.URLFromID(s.baseURL, groupID)
	if err != nil {
		return nil, fmt.Errorf("invalid group url: %s", err)
	}
	g, err := s.groupService.FindByID(ctx, groupID)
	if err != nil {
		return nil, err
	}
	return s.Feed(
		ctx,
		eventsearch.Options{Filters: []eventsearch.Filter{eventsearch.GroupIDFilter{GroupID: groupID}}},
		&Channel{Title: g.Name, Link: groupURL + "/rss", Description: g.Description},
	)
}

func (s *ServiceImpl) PlaceFeed(ctx context.Context, placeID string) (*Feed, error) {
	placeURL, err := place.URLFromID(s.baseURL, placeID)
	if err != nil {
		return nil, fmt.Errorf("invalid group url: %s", err)
	}
	p, err := s.placeService.FindByID(ctx, placeID)
	if err != nil {
		return nil, err
	}
	return s.Feed(
		ctx,
		eventsearch.Options{Filters: []eventsearch.Filter{eventsearch.PlaceIDFilter{PlaceID: placeID}}},
		&Channel{Title: p.Name, Link: placeURL + "/rss", Description: p.Description},
	)
}
