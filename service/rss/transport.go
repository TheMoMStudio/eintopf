//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package rss

import (
	"context"
	"encoding/xml"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/plausible"
	"eintopf.info/internal/xhttp"
	"eintopf.info/service/eventsearch"
)

func Routes(service Service, plausibleClient *plausible.Client) func(r chi.Router) {
	router := router{service: service}
	return func(r chi.Router) {
		r.Use(plausible.MiddlewareWithName(plausibleClient, "rss"))
		r.Get("/rss", router.getEventsRSSFeed)
		r.Get("/groups/{id}/rss", router.getGroupRSSFeed)
		r.Get("/places/{id}/rss", router.getPlaceRSSFeed)
	}
}

type router struct {
	service Service
}

func (router router) getEventsRSSFeed(w http.ResponseWriter, r *http.Request) {
	opts, err := eventsearch.OptionsFromRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	feed, err := router.service.Feed(r.Context(), opts, &Channel{Title: "events"})
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeFeed(r.Context(), w, feed)
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, err)
		return
	}
}

func (router router) getGroupRSSFeed(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	if id == "" {
		xhttp.WriteBadRequest(r.Context(), w, fmt.Errorf("empty id"))
		return
	}
	feed, err := router.service.GroupFeed(r.Context(), id)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeFeed(r.Context(), w, feed)
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, err)
		return
	}
}

func (router router) getPlaceRSSFeed(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	if id == "" {
		xhttp.WriteBadRequest(r.Context(), w, fmt.Errorf("empty id"))
		return
	}
	feed, err := router.service.PlaceFeed(r.Context(), id)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeFeed(r.Context(), w, feed)
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, err)
		return
	}
}

func writeFeed(ctx context.Context, w http.ResponseWriter, feed *Feed) error {
	feedXML, err := xml.Marshal(feed)
	if err != nil {
		return err
	}
	w.Header().Add("Content-Type", "application/rss+xml")
	w.Header().Add("Content-Disposition", "inline; filename=eintopf.rss")
	_, err = w.Write(feedXML)
	if err != nil {
		return err
	}
	return nil
}
