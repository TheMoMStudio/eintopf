// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package eventsearch_test

import (
	"net/http/httptest"
	"net/url"
	"testing"
	"time"

	"eintopf.info/service/eventsearch"
	"github.com/google/go-cmp/cmp"
)

func TestOptionsFromRequest(t *testing.T) {
	tests := []struct {
		url     string
		options eventsearch.Options
	}{
		{
			url: "/?sort=start",
			options: eventsearch.Options{
				Sort:         "start",
				Filters:      eventsearch.Filters{},
				Aggregations: eventsearch.Aggregations{},
			},
		},
		{
			url: "/?sort=start&groupIDs=12c22b9b-173a-4488-94df-3a474bc310e9",
			options: eventsearch.Options{
				Sort: "start",
				Filters: eventsearch.Filters{
					eventsearch.GroupIDsFilter{GroupIDs: []string{"12c22b9b-173a-4488-94df-3a474bc310e9"}},
				},
				Aggregations: eventsearch.Aggregations{},
			},
		},
		{
			url: "/?sort=start&dateMin=2023-03-06T09:55:05Z",
			options: eventsearch.Options{
				Sort:         "start",
				Filters:      eventsearch.Filters{eventsearch.DateRangeFilter{DateMin: time.Date(2023, 3, 6, 9, 55, 5, 0, time.UTC)}},
				Aggregations: eventsearch.Aggregations{},
			},
		},
		{
			url: "/?sort=start&dateMin=" + url.QueryEscape("2023-03-06T09:55:05Z"),
			options: eventsearch.Options{
				Sort:         "start",
				Filters:      eventsearch.Filters{eventsearch.DateRangeFilter{DateMin: time.Date(2023, 3, 6, 9, 55, 5, 0, time.UTC)}},
				Aggregations: eventsearch.Aggregations{},
			},
		},
	}

	for _, test := range tests {
		r := httptest.NewRequest("GET", test.url, nil)
		got, err := eventsearch.OptionsFromRequest(r)
		if err != nil {
			t.Fatal(err)
		}
		if diff := cmp.Diff(test.options, got); diff != "" {
			t.Errorf("options mismatch (-want +got):\n%s", diff)
		}
	}
}
