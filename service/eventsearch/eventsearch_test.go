// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package eventsearch_test

import (
	"context"
	"testing"
	"time"

	"eintopf.info/service/auth"
	"eintopf.info/service/event"
	"eintopf.info/service/eventsearch"
	"eintopf.info/service/group"
	"eintopf.info/service/oqueue"
	"eintopf.info/service/place"
	"eintopf.info/service/revent"
	"eintopf.info/service/search"
	"eintopf.info/service/taxonomy"
	"eintopf.info/test"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
)

func TestAggregation(t *testing.T) {
	tests := []eventsearchTest{
		{
			name: "InvolvedAggregation",
			index: []*event.Event{
				{
					ID:   "1",
					Name: "foo",
					Involved: []event.Involved{
						{Name: "involved1", Description: "foo bar foo"},
						{Name: "involved2", Description: "bar foo bar"},
					},
				},
				{
					ID:   "2",
					Name: "bar",
					Involved: []event.Involved{
						{Name: "involved3", Description: "foo bar foo"},
						{Name: "involved4", Description: "bar foo bar"},
					},
				},
			},
			options: eventsearch.Options{
				Filters: []eventsearch.Filter{},
				Aggregations: map[string]eventsearch.Aggregation{
					"involved": eventsearch.InvolvedObjectsAggregation{},
				},
			},
			wantBuckets: map[string]search.Bucket{
				"involved": eventsearch.InvolvedBucket{
					{Name: "involved1", Description: "foo bar foo"},
					{Name: "involved2", Description: "bar foo bar"},
					{Name: "involved3", Description: "foo bar foo"},
					{Name: "involved4", Description: "bar foo bar"},
				},
			},
		},
	}
	testEventSearch(t, tests)
}

func TestFilter(t *testing.T) {
	tests := []eventsearchTest{
		{
			name: "FilterRepeating",
			revents: []*revent.NewRepeatingEvent{
				{
				Name:        "foo",
				Organizers:  []string{"a"},
				Description: "e",
				Intervals: []revent.Interval{
					{
						Type:     revent.IntervalDay,
						Interval: 1,
					},
				},
				Start:   time.Date(2021, 8, 1, 16, 0, 0, 0, time.UTC),
				End:     time.Date(2021, 8, 1, 18, 0, 0, 0, time.UTC),
				},
			},
			index: []*event.Event{
				{
					ID:         "1",
					Name:       "bar",
					Published:  true,
					Parent:  revent.ID("0"),
				},
				{
					ID:         "2",
					Name:       "foo",
					Published:  true,
				},
			},
			options: eventsearch.Options{
				Filters: []eventsearch.Filter{
					eventsearch.RepeatingFilter{Repeating: true},
				},
			},
			wantEvents: []*eventsearch.EventDocument{
				{
					ID:         "1",
					Listed:     true,
					Published:  true,
					Name:       "bar",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   -1,
					RepeatingEvent: &revent.RepeatingEvent{
						ID: "0",
						Name:        "foo",
						Organizers:  []string{"a"},
						Description: "e",
						Intervals: []revent.Interval{
							{
								Type:     revent.IntervalDay,
								Interval: 1,
							},
						},
						Start:   time.Date(2021, 8, 1, 16, 0, 0, 0, time.UTC),
						End:     time.Date(2021, 8, 1, 18, 0, 0, 0, time.UTC),
						OwnedBy:  []string{"my_id"},
                 	},
				},
			},
		},
		{
			name: "GroupFilter",
			groups: []*group.NewGroup{
				{Name: "foo"},
			},
			index: []*event.Event{
				{
					ID:         "1",
					Name:       "bar",
					Published:  true,
					Organizers: []string{"id:0"},
				},
			},
			options: eventsearch.Options{
				Filters: []eventsearch.Filter{
					eventsearch.GroupIDsFilter{GroupIDs: []string{"0"}},
				},
			},
			wantEvents: []*eventsearch.EventDocument{
				{
					ID:         "1",
					Listed:     true,
					Published:  true,
					Name:       "bar",
					Organizers: []eventsearch.Organizer{{Group: &group.Group{ID: "0", Name: "foo", OwnedBy: []string{"my_id"}}}},
					MultiDay:   -1,
				},
			},
		},
		{
			name: "EmptyIDsFilter",
			index: []*event.Event{
				{
					ID:   "1",
					Name: "bar",
				},
			},
			options: eventsearch.Options{
				Filters: []eventsearch.Filter{
					eventsearch.IDsFilter{IDs: []string{}},
				},
			},
			wantEvents: []*eventsearch.EventDocument{
				{
					ID:         "1",
					Name:       "bar",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   -1,
				},
			},
		},
		{
			name: "MultiDay -2 to -1",
			index: []*event.Event{
				{
					ID:    "1",
					Name:  "foo",
					Start: time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:   tptr(time.Date(2023, 3, 28, 12, 0, 0, 0, time.UTC)),
				},
			},
			options: eventsearch.Options{
				Sort: "day",
				Filters: []eventsearch.Filter{
					eventsearch.MultidayRangeFilter{MultidayMin: intptr(-2), MultidayMax: intptr(-1)},
				},
			},
			wantEvents: []*eventsearch.EventDocument{
				{
					ID:         "1",
					Name:       "foo",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   -2,
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 28, 12, 0, 0, 0, time.UTC)),
				},
			},
		},
		{
			name: "MultiDay < 0",
			index: []*event.Event{
				{
					ID:    "1",
					Name:  "foo",
					Start: time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:   tptr(time.Date(2023, 3, 28, 12, 0, 0, 0, time.UTC)),
				},
				{
					ID:        "2",
					Name:      "bar",
					Published: true,
				},
			},
			options: eventsearch.Options{
				Sort: "day",
				Filters: []eventsearch.Filter{
					eventsearch.MultidayRangeFilter{MultidayMax: intptr(0)},
				},
			},
			wantEvents: []*eventsearch.EventDocument{
				{
					ID:         "1",
					Name:       "foo",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   -2,
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 28, 12, 0, 0, 0, time.UTC)),
				},
				{
					ID:         "2",
					Listed:     true,
					Published:  true,
					Name:       "bar",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   -1,
				},
			},
		},
		{
			name: "MultiDay > 1",
			index: []*event.Event{
				{
					ID:    "1",
					Name:  "foo",
					Start: time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:   tptr(time.Date(2023, 3, 28, 12, 0, 0, 0, time.UTC)),
				},
			},
			options: eventsearch.Options{
				Sort: "day",
				Filters: []eventsearch.Filter{
					eventsearch.MultidayRangeFilter{MultidayMin: intptr(1)},
				},
			},
			wantEvents: []*eventsearch.EventDocument{
				{
					ID:         "1",
					Name:       "foo",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   1,
					Date:       time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 28, 12, 0, 0, 0, time.UTC)),
				},
				{
					ID:         "1",
					Name:       "foo",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   2,
					Date:       time.Date(2023, 3, 26, 12, 0, 0, 0, time.UTC),
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 28, 12, 0, 0, 0, time.UTC)),
				},
				{
					ID:         "1",
					Name:       "foo",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   3,
					Date:       time.Date(2023, 3, 27, 12, 0, 0, 0, time.UTC),
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 28, 12, 0, 0, 0, time.UTC)),
				},
				{
					ID:         "1",
					Name:       "foo",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   4,
					Date:       time.Date(2023, 3, 28, 12, 0, 0, 0, time.UTC),
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 28, 12, 0, 0, 0, time.UTC)),
				},
			},
		},
	}
	testEventSearch(t, tests)
}

func TestMultiDayEvents(t *testing.T) {
	tests := []eventsearchTest{
		{
			name: "MultiDay",
			index: []*event.Event{
				{
					ID:    "1",
					Name:  "foo",
					Start: time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:   tptr(time.Date(2023, 3, 28, 12, 0, 0, 0, time.UTC)),
				},
			},
			options: eventsearch.Options{
				Sort: "day",
			},
			wantEvents: []*eventsearch.EventDocument{
				{
					ID:         "1",
					Name:       "foo",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   1,
					Date:       time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 28, 12, 0, 0, 0, time.UTC)),
				},
				{
					ID:         "1",
					Name:       "foo",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   2,
					Date:       time.Date(2023, 3, 26, 12, 0, 0, 0, time.UTC),
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 28, 12, 0, 0, 0, time.UTC)),
				},
				{
					ID:         "1",
					Name:       "foo",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   3,
					Date:       time.Date(2023, 3, 27, 12, 0, 0, 0, time.UTC),
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 28, 12, 0, 0, 0, time.UTC)),
				},
				{
					ID:         "1",
					Name:       "foo",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   4,
					Date:       time.Date(2023, 3, 28, 12, 0, 0, 0, time.UTC),
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 28, 12, 0, 0, 0, time.UTC)),
				},
				{
					ID:         "1",
					Name:       "foo",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   -2,
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 28, 12, 0, 0, 0, time.UTC)),
				},
			},
		},
		{
			name: "MultiDayWithSubEvent",
			events: []*event.NewEvent{
				{
					Published:    true,
					Parent:       "id:abcd",
					ParentListed: false,
					Name:         "bar",
					Start:        time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
				},
			},
			index: []*event.Event{
				{
					ID:    "abcd",
					Name:  "foo",
					Start: time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:   tptr(time.Date(2023, 3, 26, 12, 0, 0, 0, time.UTC)),
				},
			},
			options: eventsearch.Options{
				Sort: "day",
			},
			wantEvents: []*eventsearch.EventDocument{
				{
					ID:         "abcd",
					Name:       "foo",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   1,
					Date:       time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 26, 12, 0, 0, 0, time.UTC)),
					Children: []*eventsearch.EventDocument{
						{
							ID:         "0",
							Published:  true,
							Name:       "bar",
							Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
							Date:       time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
							MultiDay:   -1,
							Organizers: []eventsearch.Organizer{},
						},
					},
				},
				{
					ID:         "abcd",
					Name:       "foo",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   2,
					Date:       time.Date(2023, 3, 26, 12, 0, 0, 0, time.UTC),
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 26, 12, 0, 0, 0, time.UTC)),
				},
				{
					ID:         "abcd",
					Name:       "foo",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   -2,
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 26, 12, 0, 0, 0, time.UTC)),
					Children: []*eventsearch.EventDocument{
						{
							ID:         "0",
							Published:  true,
							Name:       "bar",
							Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
							Date:       time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
							MultiDay:   -1,
							Organizers: []eventsearch.Organizer{},
						},
					},
				},
			},
		},
	}
	testEventSearch(t, tests)
}

func TestReindex(t *testing.T) {
	tests := []eventsearchTest{
		{
			name: "RemoveAndAdd",
			events: []*event.NewEvent{
				{
					Name: "bar",
				},
			},
			index: []*event.Event{
				{
					ID:   "10",
					Name: "foo",
				},
			},
			reindex: true,
			options: eventsearch.Options{},
			wantEvents: []*eventsearch.EventDocument{
				{
					ID:         "0",
					Name:       "bar",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   -1,
				},
			},
		},
		{
			name: "RemoveMultiDay",
			events: []*event.NewEvent{
				{
					Name: "bar",
				},
			},
			index: []*event.Event{
				{
					ID:    "1",
					Name:  "foo",
					Start: time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:   tptr(time.Date(2023, 3, 28, 12, 0, 0, 0, time.UTC)),
				},
			},
			reindex: true,
			options: eventsearch.Options{},
			wantEvents: []*eventsearch.EventDocument{
				{
					ID:         "0",
					Name:       "bar",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   -1,
				},
			},
		},
	}
	testEventSearch(t, tests)
}

func TestOperations(t *testing.T) {
	tests := []eventsearchTest{
		{
			name: "Update",
			events: []*event.NewEvent{
				{
					Name: "bar",
				},
			},
			operations: []oqueue.Operation{
				event.UpdateOperation{
					Event: &event.Event{
						ID:   "0",
						Name: "bar updated",
					},
				},
			},
			options: eventsearch.Options{},
			wantEvents: []*eventsearch.EventDocument{
				{
					ID:         "0",
					Name:       "bar updated",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   -1,
				},
			},
		},
		{
			name: "Delete",
			events: []*event.NewEvent{
				{
					Name: "bar",
				},
			},
			operations: []oqueue.Operation{
				event.DeleteOperation{
					ID: "0",
				},
			},
			options:    eventsearch.Options{},
			wantEvents: []*eventsearch.EventDocument{},
		},
		{
			name: "DeleteMultiDay",
			index: []*event.Event{
				{
					ID:    "0",
					Name:  "bar",
					Start: time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:   tptr(time.Date(2023, 3, 28, 12, 0, 0, 0, time.UTC)),
				},
			},
			operations: []oqueue.Operation{
				event.DeleteOperation{
					ID: "0",
				},
			},
			options:    eventsearch.Options{},
			wantEvents: []*eventsearch.EventDocument{},
		},
		{
			name: "UpdateMultiDay",
			index: []*event.Event{
				{
					ID:    "0",
					Name:  "bar",
					Start: time.Date(2023, 3, 24, 12, 0, 0, 0, time.UTC),
					End:   tptr(time.Date(2023, 3, 30, 12, 0, 0, 0, time.UTC)),
				},
			},
			operations: []oqueue.Operation{
				event.UpdateOperation{
					Event: &event.Event{
						ID:    "0",
						Name:  "bar",
						Start: time.Date(2023, 3, 26, 12, 0, 0, 0, time.UTC),
						End:   tptr(time.Date(2023, 3, 27, 12, 0, 0, 0, time.UTC)),
					},
				},
			},
			options: eventsearch.Options{},
			wantEvents: []*eventsearch.EventDocument{
				{
					ID:         "0",
					Name:       "bar",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   -2,
					Start:      time.Date(2023, 3, 26, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 27, 12, 0, 0, 0, time.UTC)),
				},
				{
					ID:         "0",
					Name:       "bar",
					Organizers: []eventsearch.Organizer{},
					Date:       time.Date(2023, 3, 27, 12, 0, 0, 0, time.UTC),
					MultiDay:   2,
					Start:      time.Date(2023, 3, 26, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 27, 12, 0, 0, 0, time.UTC)),
				},
				{
					ID:         "0",
					Name:       "bar",
					Organizers: []eventsearch.Organizer{},
					Date:       time.Date(2023, 3, 26, 12, 0, 0, 0, time.UTC),
					MultiDay:   1,
					Start:      time.Date(2023, 3, 26, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 27, 12, 0, 0, 0, time.UTC)),
				},
			},
		},
	}
	testEventSearch(t, tests)
}

type eventsearchTest struct {
	name string
	tz   *time.Location

	events []*event.NewEvent
	revents []*revent.NewRepeatingEvent
	groups []*group.NewGroup
	places []*place.NewPlace

	// index, reindex and operations are applied in this order
	index      []*event.Event
	reindex    bool
	operations []oqueue.Operation

	options eventsearch.Options

	wantEvents  []*eventsearch.EventDocument
	wantBuckets map[string]search.Bucket
}

func testEventSearch(t *testing.T, tests []eventsearchTest) {
	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			index, cleanupIndex := test.CreateBleveTestIndex(tc.name)
			t.Cleanup(cleanupIndex)

			tz := time.UTC
			ctx := auth.ContextWithID(context.Background(), "my_id")

			searchService, err := search.NewService(index, time.Second, 1, 1, tz)
			if err != nil {
				t.Fatalf("search: %s", err)
			}

			eventService := event.NewService(event.NewMemoryStore(), nil)
			groupService := group.NewService(group.NewMemoryStore())
			placeService := place.NewService(place.NewMemoryStore())
			categoryService := taxonomy.NewCategoryMemoryStore()
			topicService := taxonomy.NewTopicMemoryStore()

			queue := oqueue.NewQueue()

			reventService := revent.NewService(revent.NewMemoryStore(),eventService)

			for _, e := range tc.events {
				if _, err := eventService.Create(ctx, e); err != nil {
					tt.Fatalf("create events: %s", err)
				}
			}

			for _, e := range tc.revents {
				if _, err := reventService.Create(ctx, e); err != nil {
					tt.Fatalf("create revents: %s", err)
				}
			}

			for _, g := range tc.groups {
				if _, err := groupService.Create(ctx, g); err != nil {
					tt.Fatalf("create group: %s", err)
				}
			}
			for _, p := range tc.places {
				if _, err := placeService.Create(ctx, p); err != nil {
					tt.Fatalf("create place: %s", err)
				}
			}

			eventsearchService := eventsearch.NewService(searchService, eventService, groupService, placeService, reventService, categoryService, topicService)
			queue.AddSubscriber(eventsearchService.ConsumeOperation, 1)

			for _, e := range tc.index {
				err = eventsearchService.Index(e)
				if err != nil {
					tt.Fatalf("Index: %s", err)
				}
			}

			if tc.reindex {
				eventsearchService.Reindex(context.Background())
			}

			for _, op := range tc.operations {
				err = queue.AddOperation(op)
				if err != nil {
					tt.Errorf("AddOperation: %s", err)
				}
			}
			// Wait for all operations to be consumed
			queue.Close()

			result, err := eventsearchService.Search(context.Background(), tc.options)
			if err != nil {
				tt.Fatalf("Search: %s", err)
			}

			if tc.wantEvents != nil {
				if diff := cmp.Diff(tc.wantEvents, result.Events, cmpopts.SortSlices(func(a, b *eventsearch.EventDocument) bool {
					return a.MultiDay < b.MultiDay
				}), cmpopts.IgnoreUnexported(eventsearch.EventDocument{})); diff != "" {
					tt.Errorf("result mismatch (-want +got):\n%s", diff)
				}
			}

			if tc.wantBuckets != nil {
				if diff := cmp.Diff(tc.wantBuckets, result.Buckets, cmpopts.IgnoreUnexported()); diff != "" {
					tt.Errorf("buckets mismatch (-want +got):\n%s", diff)
				}
			}
		})
	}
}

func intptr(i int) *int {
	return &i
}
