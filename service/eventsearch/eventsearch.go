//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package eventsearch

import (
	"context"
	"fmt"
	"log"
	"sync"
	"time"

	"eintopf.info/internal/crud"
	"eintopf.info/service/event"
	"eintopf.info/service/group"
	"eintopf.info/service/oqueue"
	"eintopf.info/service/place"
	"eintopf.info/service/revent"
	"eintopf.info/service/search"
	"eintopf.info/service/taxonomy"
)

type Service interface {
	// Search makes a search in the search database for events. The result can
	// be modified using Options.
	Search(ctx context.Context, opts Options) (*Result, error)
	// Index takes one or many event and adds them to the search index.
	Index(e ...*event.Event) error
	// LastModified returns the time the index was last modified.
	LastModified() time.Time
	// Reindex makes sure, that the state of the search index is the same as in the
	// event database.
	Reindex(ctx context.Context) error
}

// Options is a set of options, that can be used to modify a search result.
// swagger:parameters eventsearch eventsearch
type Options struct {
	// Query is the search query used for a text search.
	Query string `json:"query"`

	// Sort is the field, that should be sorted by.
	// When left empty, the default sorting is used.
	Sort string `json:"sort"`

	// SortDescending defines the sort order.
	SortDescending bool `json:"sortAscending"`

	// Page is the current page.
	Page int `json:"page"`

	// PageSize defines the number of events returned per page.
	//
	// PageSize is infinite when set to 0.
	PageSize int `json:"pageSize"`

	// Filters is a list of filters, that reduce the search result. All filters
	// are combined with AND logic in addition with the search query.
	Filters Filters `json:"filters"`

	// Aggregations is a map of aggregations. Each map key corresponds to a
	// bucket of aggregated items with the same ke.
	Aggregations Aggregations `json:"aggregations"`

	// Omit takes a list of fields, tha will be removed from all response
	// objects recursively.
	Omit []string `json:"omit"`
}

// Filter is an event filter, that can reduce an event search.
//
//go:generate go run ../../scripts/gen/go/searchfilter/main.go -modelPath eventdocument.yaml -path filter.go -packageName eventsearch
type Filter interface {
	// SearchFilter converts the the filter to a search.Filter.
	SearchFilter() search.Filter
}

// Filters is a list of filters. They can be converted to a list of search.Filter.
type Filters []Filter

// SearchFilters converts the list of filters to a list of search filters.
func (s Filters) SearchFilters() []search.Filter {
	searchFilters := make([]search.Filter, 0, len(s))
	for _, f := range s {
		searchFilters = append(searchFilters, f.SearchFilter())
	}
	return searchFilters
}

// Aggregation is an event aggregation.
//
//go:generate go run ../../scripts/gen/go/searchaggregation/main.go -modelPath eventdocument.yaml -path aggregation.go -packageName eventsearch
type Aggregation interface {
	// SearchAggregation converts the aggregation to a search.Aggregation.
	SearchAggregation() search.Aggregation
}

// Aggregations maps a name to a aggregation. This allows multiple
// aggregations on the same field with different filters.
type Aggregations map[string]Aggregation

func (s Aggregations) SearchAggregations() map[string]search.Aggregation {
	searchAggregations := make(map[string]search.Aggregation, len(s))
	for k, v := range s {
		searchAggregations[k] = v.SearchAggregation()
	}
	return searchAggregations
}

// Result contains an event search result.
// swagger:response eventsearchResult
type Result struct {
	// Events are the events found for the current pagination.
	Events []*EventDocument `json:"events"`

	// Total is the total number of events in the search result.
	// It is independet of the current pagination.
	Total uint64 `json:"total"`

	// Buckets is a set of aggregation buckets.
	// The map key corresponds to aggregation name.
	Buckets map[string]search.Bucket `json:"buckets"`
}

// ServiceImpl is an implementation of Service.
type ServiceImpl struct {
	searchService         search.Service
	eventService          event.Storer
	groupService          group.Service
	placeService          place.Service
	repeatingEventService revent.Service
	categoryService       taxonomy.CategoryStorer
	topicService          taxonomy.TopicStorer

	lastModified  time.Time
	lastModifiedM *sync.Mutex
}

// NewService returns a new ServiceImpl.
func NewService(
	searchService search.Service,
	eventService event.Storer,
	groupService group.Service,
	placeService place.Service,
	repeatingEventService revent.Service,
	categoryService taxonomy.CategoryStorer,
	topicService taxonomy.TopicStorer,
) *ServiceImpl {
	s := &ServiceImpl{
		searchService:         searchService,
		eventService:          eventService,
		groupService:          groupService,
		placeService:          placeService,
		repeatingEventService: repeatingEventService,
		categoryService:       categoryService,
		topicService:          topicService,

		lastModified:  time.Now(),
		lastModifiedM: &sync.Mutex{},
	}

	return s
}

func (s ServiceImpl) Search(ctx context.Context, opts Options) (*Result, error) {
	searchOpts := &search.Options{
		Query:          opts.Query,
		Sort:           opts.Sort,
		SortDescending: opts.SortDescending,
		Page:           opts.Page,
		PageSize:       opts.PageSize,
		Filters: append(
			opts.Filters.SearchFilters(),
			// Always filter for type event.
			&search.TermsFilter{Field: "type", Terms: []string{"event"}},
		),
		Aggregations: opts.Aggregations.SearchAggregations(),
	}

	rawResult, err := s.searchService.Search(ctx, searchOpts)
	if err != nil {
		return nil, err
	}
	result := &Result{
		Total:   rawResult.Total,
		Events:  []*EventDocument{},
		Buckets: rawResult.Buckets,
	}
	result.Buckets = convertSearchBuckets(result.Buckets, opts.Aggregations)
	for _, hit := range rawResult.Hits {
		if hit.Type != "event" {
			log.Println("eventsearch: hit is not of type event")
			continue
		}
		e := &EventDocument{}
		e.indexID = hit.ID
		err := hit.Unmarshal(e)
		if err != nil {
			return nil, err
		}
		for _, omit := range opts.Omit {
			switch omit {
			case "description":
				e.Description = ""
				for _, c := range e.Children {
					c.Description = ""
				}
				for _, o := range e.Organizers {
					if o.Group != nil {
						o.Group.Description = ""
					}
				}
				if e.Location != nil && e.Location.Place != nil {
					e.Location.Place.Description = ""
				}
				if e.RepeatingEvent != nil {
					e.RepeatingEvent.Description = ""
				}
			}
		}
		result.Events = append(result.Events, e)
	}

	return result, nil
}

func (s *ServiceImpl) LastModified() time.Time {
	s.lastModifiedM.Lock()
	defer s.lastModifiedM.Unlock()

	return s.lastModified
}

func (s *ServiceImpl) updateLastModified() {
	s.lastModifiedM.Lock()
	defer s.lastModifiedM.Unlock()

	s.lastModified = time.Now()
}

// ConsumeOperation consumes an event operations of the type create, update and
// delete. Depending on wether the model is searchable, it performs an index or
// a delete operation on the search index.
func (s *ServiceImpl) ConsumeOperation(op oqueue.Operation) error {
	ctx := context.Background()

	switch op := op.(type) {
	case event.CreateOperation:
		if op.Event.Indexable() {
			defer s.updateLastModified()
			return s.Index(op.Event)
		}
	case event.UpdateOperation:
		defer s.updateLastModified()
		// Always remove the event from the index before indexing it again. This
		// ensures, that when making the period for a multiday event smaller all
		// events which are outside the new period are deleted.
		err := s.deleteEvent(context.Background(), op.Event.ID)
		if err != nil {
			return err
		}
		if op.Event.Indexable() {
			return s.Index(op.Event)
		}
	case event.DeleteOperation:
		defer s.updateLastModified()
		return s.deleteEvent(context.Background(), op.ID)

	case taxonomy.CategoryCreateOperation:
		s.updateEventsWithCategory(ctx, op.Category.ID)
	case taxonomy.CategoryUpdateOperation:
		s.updateEventsWithCategory(ctx, op.Category.ID)
	case taxonomy.CategoryDeleteOperation:
		s.updateEventsWithCategory(ctx, op.ID)

	case taxonomy.TopicUpdateOperation:
		s.updateEventsWithTopic(ctx, op.Topic.ID)
	case taxonomy.TopicCreateOperation:
		s.updateEventsWithTopic(ctx, op.Topic.ID)
	case taxonomy.TopicDeleteOperation:
		s.updateEventsWithTopic(ctx, op.ID)
	}

	return nil
}

func (s *ServiceImpl) updateEventsWithCategory(ctx context.Context, categoryID string) error {
	events, _, err := s.eventService.Find(context.Background(), &crud.FindParams[event.FindFilters]{
		Limit: 0,
		Filters: &event.FindFilters{
			Category: &categoryID,
		},
	})
	if err != nil {
		return err
	}
	return s.updateEvents(ctx, events)
}

func (s *ServiceImpl) updateEventsWithTopic(ctx context.Context, topicID string) error {
	events, _, err := s.eventService.Find(context.Background(), &crud.FindParams[event.FindFilters]{
		Limit: 0,
		Filters: &event.FindFilters{
			Topic: &topicID,
		},
	})
	if err != nil {
		return err
	}
	return s.updateEvents(ctx, events)
}

func (s *ServiceImpl) updateEvents(ctx context.Context, events []*event.Event) error {
	defer s.updateLastModified()
	for _, event := range events {
		if event.Indexable() {
			return s.Index(event)
		}
	}
	return nil
}

func (s *ServiceImpl) deleteEvent(ctx context.Context, id string) error {
	// Collect all events with the given id, to properly delete multiday
	// events.
	result, err := s.Search(context.Background(), Options{
		Filters: Filters{IDFilter{ID: id}},
	})
	if err != nil {
		return fmt.Errorf("delete: find existing events: %s", err)
	}
	for _, e := range result.Events {
		err = s.searchService.Delete("event", e.indexID)
		if err != nil {
			return fmt.Errorf("delete: %s", err)
		}
	}
	return nil
}

// Reindex makes sure, that the state of the search index is the same as in the
// event database.
func (s *ServiceImpl) Reindex(ctx context.Context) error {
	// Collect all event ids, that should be in the search index.
	events, _, err := s.eventService.Find(ctx, nil)
	if err != nil {
		return err
	}
	indexableIDs := []string{}
	indexableEvents := []*event.Event{}
	for _, event := range events {
		if event.Indexable() {
			indexableEvents = append(indexableEvents, event)
			indexableIDs = append(indexableIDs, event.ID)
		}
	}
	s.Index(indexableEvents...)

	// Collect all events, that should not be in the search index and remove them.
	result, err := s.Search(ctx, Options{
		Filters: []Filter{
			&NotFilter{&IDsFilter{IDs: indexableIDs}},
		},
	})
	if err != nil {
		return err
	}
	if result.Total > 0 {
		log.Printf("Reindex(events): deleting %d events from index\n", len(result.Events))
		for _, e := range result.Events {
			s.searchService.Delete("event", e.indexID)
		}
	}

	s.lastModifiedM.Lock()
	s.lastModified = time.Now()
	s.lastModifiedM.Unlock()

	return nil
}
