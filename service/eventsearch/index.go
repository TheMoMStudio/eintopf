//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package eventsearch

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"eintopf.info/internal/crud"
	"eintopf.info/internal/xtime"
	"eintopf.info/service/event"
	"eintopf.info/service/group"
	"eintopf.info/service/place"
	"eintopf.info/service/revent"
	"eintopf.info/service/search"
	"eintopf.info/service/taxonomy"
)

func (s *ServiceImpl) Index(events ...*event.Event) error {
	docs := []search.Indexable{}
	for _, e := range events {
		docs = append(docs, toIndexables(s.EventDocumentsFromEvent(e))...)
	}

	return s.searchService.Index(docs...)
}

func toIndexables(events []*EventDocument) []search.Indexable {
	docs := []search.Indexable{}
	for _, e := range events {
		docs = append(docs, e)
	}
	return docs
}

func (s *ServiceImpl) EventDocumentsFromEvent(e *event.Event) []*EventDocument {
	// assumed single day events without end dates
	if e.End == nil {
		return []*EventDocument{s.newEventDocument(e, e.ID, e.Start, -1, true)}
	}

	// single day events with end date on same day or until 6 am of next day
	startDay := xtime.StartOfDay(e.Start)
	endDay := xtime.StartOfDay(*e.End)
	if startDay.Compare(endDay) == 0 || (endDay.Sub(startDay).Hours() == 24 && e.End.Hour() < 7) {
		return []*EventDocument{s.newEventDocument(e, e.ID, e.Start, -1, true)}
	}

	// multi day events handling: create one raw event and a multi day event for each day
	docs := []*EventDocument{}
	docs = append(docs, s.newEventDocument(e, e.ID, time.Time{}, -2, true))
	i := 1
	for d := e.Start; !(xtime.StartOfDay(d).After(xtime.StartOfDay(*e.End))); d = d.Add(time.Hour * 24) {
		docs = append(docs, s.newEventDocument(e, fmt.Sprintf("%s_%d", e.ID, i), d, i, true))
		i++
	}
	return docs
}

func (s *ServiceImpl) newEventDocument(e *event.Event, indexID string, date time.Time, multiDay int, includeChildren bool) *EventDocument {
	doc := &EventDocument{
		indexID:     indexID,
		ID:          e.ID,
		Name:        e.Name,
		Location2:   e.Location2,
		Description: e.Description,
		Involved:    e.Involved,
		Date:        date,
		MultiDay:    multiDay,
		Start:       e.Start,
		End:         e.End,
		Tags:        e.Tags,
		Image:       e.Image,
		Alt:         e.Alt,
		Published:   e.Published,
		Canceled:    e.Canceled,
		Listed:      e.Listable(),
		ownedBy:     e.OwnedBy,
	}

	if includeChildren {
		parentFilter := fmt.Sprintf("id:%s", e.ID)
		deactivatedFilter := false
		publishedFilter := true
		childEvents, _, err := s.eventService.Find(context.Background(), &crud.FindParams[event.FindFilters]{
			Filters: &event.FindFilters{
				Parent:      &parentFilter,
				Deactivated: &deactivatedFilter,
				Published:   &publishedFilter,
			},
			Sort:  "start",
			Order: crud.OrderAsc,
		})
		if err == nil && len(childEvents) > 0 {
			doc.Children = []*EventDocument{}
			for _, childEvent := range childEvents {
				if multiDay < 0 || (childEvent.Start.After(xtime.StartOfDay(date)) && childEvent.Start.Before(xtime.EndOfDay(date))) {
					doc.Children = append(doc.Children, s.newEventDocument(childEvent, childEvent.ID, childEvent.Start, -1, false))
				}
			}
		}
	}

	doc.Organizers = []Organizer{}
	for _, organizer := range e.Organizers {
		if strings.HasPrefix(organizer, "id:") {
			groupID := strings.TrimPrefix(organizer, "id:")
			group, err := s.groupService.FindByID(context.Background(), groupID)
			if err == nil && group != nil {
				doc.Organizers = append(doc.Organizers, Organizer{Group: group})
			}
			doc.ownedBy = append(doc.ownedBy, group.OwnedBy...)
		} else {
			doc.Organizers = append(doc.Organizers, Organizer{Name: organizer})
		}
	}

	if strings.HasPrefix(e.Location, "id:") {
		placeID := strings.TrimPrefix(e.Location, "id:")
		place, err := s.placeService.FindByID(context.Background(), placeID)
		if err == nil && place != nil {
			doc.Location = &Location{Place: place}
		}
	} else if e.Location != "" {
		doc.Location = &Location{Name: e.Location, Lat: e.Lat, Lng: e.Lng, Address: e.Address}
	}

	if repeatingEventID, ok := revent.ParseID(e.Parent); ok {
		repeatingEvent, err := s.repeatingEventService.FindByID(context.Background(), repeatingEventID)
		if err == nil && repeatingEvent != nil {
			doc.RepeatingEvent = repeatingEvent
		}
	}

	if e.Category != "" {
		category, err := s.categoryService.FindByID(context.Background(), e.Category)
		if err == nil {
			doc.Category = category
		}
	}
	if e.Topic != "" {
		topic, err := s.topicService.FindByID(context.Background(), e.Topic)
		if err == nil {
			doc.Topic = topic
		}
	}

	return doc
}

type EventDocument struct {
	// indexID is the ID with an optional prefix for multiday events:
	//   $id_$day
	// This is needed, to ensure a unique ID for multiday events, since seach
	// day gets its own entry.
	indexID     string
	ID          string           `json:"id"`
	Canceled    bool             `json:"canceled"`
	Listed      bool             `json:"listed"`
	Published   bool             `json:"published"`
	Name        string           `json:"name"`
	Organizers  []Organizer      `json:"organizers"`
	Involved    []event.Involved `json:"involved"`
	Location    *Location        `json:"location"`
	Location2   string           `json:"location2"`
	Description string           `json:"description"`
	Date        time.Time        `json:"day"`
	// MultiDay tracks the day of a multiday event
	//   -2 for the raw multi day event
	//   -1 for single day event
	//   n for the nth day of a multiday event
	MultiDay       int                    `json:"multiday"`
	Start          time.Time              `json:"start"`
	End            *time.Time             `json:"end"`
	Category       *taxonomy.Category     `json:"category"`
	Topic          *taxonomy.Topic        `json:"topic"`
	Tags           []string               `json:"tags"`
	Image          string                 `json:"image"`
	Alt            string                 `json:"alt"`
	Children       []*EventDocument       `json:"children,omitempty"`
	RepeatingEvent *revent.RepeatingEvent `json:"repeatingEvent,omitempty"`
	ownedBy        []string
}

// Organizer can either be a group.Group or a string
type Organizer struct {
	Group *group.Group `json:"group,omitempty"`
	Name  string       `json:"name,omitempty"`
}

// Location can either be a place.Place or a string
type Location struct {
	Place   *place.Place `json:"place,omitempty"`
	Name    string       `json:"name,omitempty"`
	Address string       `json:"address,omitempty"`
	Lat     float64      `json:"lat,omitempty"`
	Lng     float64      `json:"lng,omitempty"`
}

// Identifier returns the event id.
func (e *EventDocument) Identifier() string {
	return e.indexID
}

// Type returns "event" as the event type.
func (e *EventDocument) Type() string {
	return "event"
}

// QueryText consists of the following fields:
//   - Name
//   - Description
//   - Organizers (Name or Group.Name)
//   - Location (Name or Place.Name)
//   - Tags
//
// The query text is used when making a text search.
func (e *EventDocument) QueryText() string {
	fields := []string{
		e.Name,
		e.Description,
	}
	for _, organizer := range e.Organizers {
		if organizer.Group != nil {
			fields = append(fields, organizer.Group.Name)
		} else {
			fields = append(fields, organizer.Name)
		}
	}
	if e.Location != nil {
		if e.Location.Place != nil {
			fields = append(fields, e.Location.Place.Name)
		} else if e.Location.Name != "" {
			fields = append(fields, e.Location.Name)
		}
	}
	for _, tag := range e.Tags {
		fields = append(fields, tag)
	}

	if e.Category != nil {
		fields = append(fields, e.Category.Name)
	}
	if e.Topic != nil {
		fields = append(fields, e.Topic.Name)
	}
	return strings.Join(fields, " ")
}

const layoutISO = "2006-01-02"

// SearchFields returns a map of fields to be index for searching.
func (e *EventDocument) SearchFields() map[string]interface{} {
	tags := []string{}
	for _, tag := range e.Tags {
		tags = append(tags, tag)
	}
	organizers := []string{}
	groupIDs := []string{}
	for _, organizer := range e.Organizers {
		if organizer.Group != nil {
			organizers = append(organizers, organizer.Group.Name)
			groupIDs = append(groupIDs, organizer.Group.ID)
		} else {
			organizers = append(organizers, organizer.Name)
		}
	}
	location := ""
	placeID := ""
	if e.Location != nil {
		location = e.Location.Name
		if e.Location.Place != nil {
			location = e.Location.Place.Name
			placeID = e.Location.Place.ID
		}
	}
	involved := []string{}
	for _, i := range e.Involved {
		data, err := json.Marshal(i)
		if err == nil {
			involved = append(involved, string(data))
		}
	}

	category := ""
	if e.Category != nil {
		category = e.Category.Name
	}
	topic := ""
	if e.Topic != nil {
		topic = e.Topic.Name
	}

	repeating := false
	if e.RepeatingEvent != nil {
		repeating = true
	}

	return map[string]interface{}{
		"is":         append(tags, e.Type()),
		"id":         e.ID,
		"organizers": organizers,
		"groupID":    groupIDs,
		"location":   location,
		"location2":  e.Location2,
		"placeID":    placeID,
		"category":   category,
		"topic":      topic,
		"tag":        tags,
		"name":       e.Name,
		"start":      e.Start,
		"end":        e.End,
		"date":       e.Date,
		"multiday":   e.MultiDay,
		"day":        time.Date(e.Date.Year(), e.Date.Month(), e.Date.Day(), 0, 0, 0, 0, time.UTC),
		"listed":     e.Listed,
		"published":  e.Published,
		"involved":   involved,
		"ownedBy":    e.ownedBy,
		"children":   len(e.Children) > 0,
		"repeating":  repeating,
	}
}

func tptr(t time.Time) *time.Time {
	return &t
}

func sameDay(t1, t2 time.Time) bool {
	return t1.Year() == t2.Year() && t1.Month() == t2.Month() && t1.Day() == t2.Day()
}
