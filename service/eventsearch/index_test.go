//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package eventsearch_test

import (
	"context"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"

	"eintopf.info/service/auth"
	"eintopf.info/service/event"
	"eintopf.info/service/eventsearch"
	"eintopf.info/service/group"
	"eintopf.info/service/place"
	"eintopf.info/service/revent"
	"eintopf.info/service/search"
	"eintopf.info/service/taxonomy"
	"eintopf.info/test"
)

func TestEventDocumentsFromEvent(t *testing.T) {
	tests := []struct {
		name string

		events []*event.NewEvent
		groups []*group.NewGroup
		places []*place.NewPlace

		event    *event.Event
		wantDocs []*eventsearch.EventDocument
	}{
		{
			name: "basics",
			event: &event.Event{
				Name:  "foo",
				Start: time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
			},
			wantDocs: []*eventsearch.EventDocument{
				{
					Name:       "foo",
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					Date:       time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					MultiDay:   -1,
					Organizers: []eventsearch.Organizer{},
				},
			},
		},
		{
			name: "basics with end date",
			event: &event.Event{
				Name:  "foo",
				Start: time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
				End:   tptr(time.Date(2023, 3, 25, 18, 0, 0, 0, time.UTC)),
			},
			wantDocs: []*eventsearch.EventDocument{
				{
					Name:       "foo",
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 25, 18, 0, 0, 0, time.UTC)),
					Date:       time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					MultiDay:   -1,
					Organizers: []eventsearch.Organizer{},
				},
			},
		},
		{
			name: "night event",
			event: &event.Event{
				Name:  "foo",
				Start: time.Date(2023, 3, 25, 23, 0, 0, 0, time.UTC),
				End:   tptr(time.Date(2023, 3, 26, 6, 0, 0, 0, time.UTC)),
			},
			wantDocs: []*eventsearch.EventDocument{
				{
					Name:       "foo",
					Start:      time.Date(2023, 3, 25, 23, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 26, 6, 0, 0, 0, time.UTC)),
					Date:       time.Date(2023, 3, 25, 23, 0, 0, 0, time.UTC),
					MultiDay:   -1,
					Organizers: []eventsearch.Organizer{},
				},
			},
		},
		{
			name: "multi day",
			event: &event.Event{
				Name:  "foo",
				Start: time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
				End:   tptr(time.Date(2023, 3, 26, 12, 0, 0, 0, time.UTC)),
			},
			wantDocs: []*eventsearch.EventDocument{
				{
					Name:       "foo",
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 26, 12, 0, 0, 0, time.UTC)),
					MultiDay:   -2,
					Organizers: []eventsearch.Organizer{},
				},
				{
					Name:       "foo",
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 26, 12, 0, 0, 0, time.UTC)),
					Date:       time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					MultiDay:   1,
					Organizers: []eventsearch.Organizer{},
				},
				{
					Name:       "foo",
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 26, 12, 0, 0, 0, time.UTC)),
					Date:       time.Date(2023, 3, 26, 12, 0, 0, 0, time.UTC),
					MultiDay:   2,
					Organizers: []eventsearch.Organizer{},
				},
			},
		},
		{
			name: "multi day different times",
			event: &event.Event{
				Name:  "foo",
				Start: time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
				End:   tptr(time.Date(2023, 3, 26, 11, 0, 0, 0, time.UTC)),
			},
			wantDocs: []*eventsearch.EventDocument{
				{
					Name:       "foo",
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 26, 11, 0, 0, 0, time.UTC)),
					MultiDay:   -2,
					Organizers: []eventsearch.Organizer{},
				},
				{
					Name:       "foo",
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 26, 11, 0, 0, 0, time.UTC)),
					Date:       time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					MultiDay:   1,
					Organizers: []eventsearch.Organizer{},
				},
				{
					Name:       "foo",
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 26, 11, 0, 0, 0, time.UTC)),
					Date:       time.Date(2023, 3, 26, 12, 0, 0, 0, time.UTC),
					MultiDay:   2,
					Organizers: []eventsearch.Organizer{},
				},
			},
		},
		{
			name: "sub event",
			events: []*event.NewEvent{
				{
					Name:      "bar",
					Published: true,
					Parent:    "id:abcd",
					Start:     time.Date(2023, 3, 25, 16, 0, 0, 0, time.UTC),
				},
			},
			event: &event.Event{
				ID:    "abcd",
				Name:  "foo",
				Start: time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
			},
			wantDocs: []*eventsearch.EventDocument{
				{
					ID:         "abcd",
					Name:       "foo",
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					Date:       time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					MultiDay:   -1,
					Organizers: []eventsearch.Organizer{},
					Children: []*eventsearch.EventDocument{
						{
							ID:         "0",
							Published:  true,
							Name:       "bar",
							Start:      time.Date(2023, 3, 25, 16, 0, 0, 0, time.UTC),
							Date:       time.Date(2023, 3, 25, 16, 0, 0, 0, time.UTC),
							MultiDay:   -1,
							Organizers: []eventsearch.Organizer{},
						},
					},
				},
			},
		},
		{
			name: "multiday sub event",
			events: []*event.NewEvent{
				{
					Name:      "bar",
					Published: true,
					Parent:    "id:abcd",
					Start:     time.Date(2023, 3, 26, 16, 0, 0, 0, time.UTC),
				},
				{
					Name:      "bar2",
					Published: true,
					Parent:    "id:abcd",
					Start:     time.Date(2023, 3, 27, 11, 0, 0, 0, time.UTC),
				},
			},
			event: &event.Event{
				ID:    "abcd",
				Name:  "foo",
				Start: time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
				End:   tptr(time.Date(2023, 3, 27, 12, 0, 0, 0, time.UTC)),
			},
			wantDocs: []*eventsearch.EventDocument{
				{
					ID:         "abcd",
					Name:       "foo",
					Organizers: []eventsearch.Organizer{},
					MultiDay:   -2,
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 27, 12, 0, 0, 0, time.UTC)),
					Children: []*eventsearch.EventDocument{
						{
							ID:         "0",
							Published:  true,
							Name:       "bar",
							Start:      time.Date(2023, 3, 26, 16, 0, 0, 0, time.UTC),
							Date:       time.Date(2023, 3, 26, 16, 0, 0, 0, time.UTC),
							MultiDay:   -1,
							Organizers: []eventsearch.Organizer{},
						},
						{
							ID:         "1",
							Published:  true,
							Name:       "bar2",
							Start:      time.Date(2023, 3, 27, 11, 0, 0, 0, time.UTC),
							Date:       time.Date(2023, 3, 27, 11, 0, 0, 0, time.UTC),
							MultiDay:   -1,
							Organizers: []eventsearch.Organizer{},
						},
					},
				},
				{
					ID:         "abcd",
					Name:       "foo",
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 27, 12, 0, 0, 0, time.UTC)),
					Date:       time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					MultiDay:   1,
					Organizers: []eventsearch.Organizer{},
					Children:   []*eventsearch.EventDocument{},
				},
				{
					ID:         "abcd",
					Name:       "foo",
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 27, 12, 0, 0, 0, time.UTC)),
					Date:       time.Date(2023, 3, 26, 12, 0, 0, 0, time.UTC),
					MultiDay:   2,
					Organizers: []eventsearch.Organizer{},
					Children: []*eventsearch.EventDocument{
						{
							ID:         "0",
							Published:  true,
							Name:       "bar",
							Start:      time.Date(2023, 3, 26, 16, 0, 0, 0, time.UTC),
							Date:       time.Date(2023, 3, 26, 16, 0, 0, 0, time.UTC),
							MultiDay:   -1,
							Organizers: []eventsearch.Organizer{},
						},
					},
				},
				{
					ID:         "abcd",
					Name:       "foo",
					Start:      time.Date(2023, 3, 25, 12, 0, 0, 0, time.UTC),
					End:        tptr(time.Date(2023, 3, 27, 12, 0, 0, 0, time.UTC)),
					Date:       time.Date(2023, 3, 27, 12, 0, 0, 0, time.UTC),
					MultiDay:   3,
					Organizers: []eventsearch.Organizer{},
					Children: []*eventsearch.EventDocument{
						{
							ID:         "1",
							Published:  true,
							Name:       "bar2",
							Start:      time.Date(2023, 3, 27, 11, 0, 0, 0, time.UTC),
							Date:       time.Date(2023, 3, 27, 11, 0, 0, 0, time.UTC),
							MultiDay:   -1,
							Organizers: []eventsearch.Organizer{},
						},
					},
				},
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			index, cleanupIndex := test.CreateBleveTestIndex(tc.name)
			t.Cleanup(cleanupIndex)
			searchService, err := search.NewService(index, time.Second, 1, 1, time.UTC)
			if err != nil {
				t.Fatalf("search.NewService: %s", err)
			}

			eventService := event.NewService(event.NewMemoryStore(), nil)
			groupService := group.NewService(group.NewMemoryStore())
			placeService := place.NewService(place.NewMemoryStore())
			reventService := revent.NewService(revent.NewMemoryStore(), eventService)
			categoryService := taxonomy.NewCategoryMemoryStore()
			topicService := taxonomy.NewTopicMemoryStore()

			ctx := auth.ContextWithID(context.Background(), "my_id")
			for _, e := range tc.events {
				if _, err := eventService.Create(ctx, e); err != nil {
					t.Fatalf("create events: %s", err)
				}
			}
			for _, g := range tc.groups {
				if _, err := groupService.Create(ctx, g); err != nil {
					t.Fatalf("create group: %s", err)
				}
			}
			for _, p := range tc.places {
				if _, err := placeService.Create(ctx, p); err != nil {
					t.Fatalf("create place: %s", err)
				}
			}

			eventsearchService := eventsearch.NewService(searchService, eventService, groupService, placeService, reventService, categoryService, topicService)
			docs := eventsearchService.EventDocumentsFromEvent(tc.event)

			if diff := cmp.Diff(tc.wantDocs, docs, cmpopts.IgnoreUnexported(eventsearch.EventDocument{})); diff != "" {
				t.Errorf("docs mismatch (-want +got):\n%s", diff)
			}
		})
	}
}

func TestEventDocumentSearchFields(t *testing.T) {
	doc := &eventsearch.EventDocument{
		ID:   "1",
		Name: "foo",
		Organizers: []eventsearch.Organizer{
			{Name: "oar"},
			{Group: &group.Group{ID: "aaa", Name: "foo"}},
		},
		Involved: []event.Involved{
			{Name: "foo", Description: "foo bar foo"},
		},
		Location:    &eventsearch.Location{Name: "lar", Address: "street 1", Lat: 12.34, Lng: 56.67},
		Description: "one two three",
		Date:        time.Date(2021, 4, 4, 10, 0, 0, 0, time.UTC),
		Start:       time.Date(2021, 4, 4, 10, 0, 0, 0, time.UTC),
		End:         tptr(time.Date(2021, 4, 7, 11, 0, 0, 0, time.UTC)),
		Tags:        []string{"a", "b"},
		Category:    &taxonomy.Category{ID: "1", Name: "mycategory"},
		Topic:       &taxonomy.Topic{ID: "1", Name: "mytopic"},
		Published:   true,
		Listed:      false,
	}
	searchFields := doc.SearchFields()
	want := map[string]interface{}{
		"is":         []string{"a", "b", "event"},
		"id":         "1",
		"organizers": []string{"oar", "foo"},
		"groupID":    []string{"aaa"},
		"involved":   []string{`{"name":"foo","description":"foo bar foo"}`},
		"location":   "lar",
		"location2":  "",
		"placeID":    "",
		"tag":        []string{"a", "b"},
		"name":       "foo",
		"start":      time.Date(2021, 4, 4, 10, 0, 0, 0, time.UTC),
		"end":        tptr(time.Date(2021, 4, 7, 11, 0, 0, 0, time.UTC)),
		"date":       time.Date(2021, 4, 4, 10, 0, 0, 0, time.UTC),
		"day":        time.Date(2021, 4, 4, 0, 0, 0, 0, time.UTC),
		"multiday":   0,
		"listed":     false,
		"published":  true,
		"ownedBy":    []string(nil),
		"category":   "mycategory",
		"topic":      "mytopic",
		"children":   false,
		"repeating":  false,
	}

	if diff := cmp.Diff(want, searchFields); diff != "" {
		t.Errorf("searchFields mismatch (-want +got):\n%s", diff)
	}
}

func tptr(t time.Time) *time.Time {
	return &t
}
