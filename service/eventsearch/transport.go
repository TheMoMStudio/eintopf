//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package eventsearch

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/xhttp"
)

// Router returns an http router for the search service.
func Router(service Service) func(chi.Router) {
	server := &server{service: service}
	return func(r chi.Router) {
		r.Options("/", xhttp.CorsHandler)

		// swagger:route GET /eventsearch eventsearch eventsearch
		//
		// Perfomes an event search request.
		//
		//     Responses:
		//       200: eventsearchResult
		//       304:
		//       400: badRequest
		//       500: internalError
		//       503: serviceUnavailable
		r.Get("/", server.search)
	}
}

type server struct {
	service Service
}

func (s *server) search(w http.ResponseWriter, r *http.Request) {
	if xhttp.RequestIsNotModified(r, s.service.LastModified()) {
		xhttp.WriteNotModified(w, s.service.LastModified())
		return
	}

	options, err := OptionsFromRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}

	result, err := s.service.Search(r.Context(), options)
	if err != nil {
		xhttp.WriteError(r.Context(), w, fmt.Errorf("failed to search: %s", err))
		return
	}
	data, err := json.Marshal(result)
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, err)
		return
	}

	xhttp.WriteWithLastModified(w, data, s.service.LastModified())
}

// OptionsFromRequest parses Options from a http.Request
func OptionsFromRequest(r *http.Request) (Options, error) {
	query, err := xhttp.ReadQueryString(r, "query")
	if err != nil {
		return Options{}, fmt.Errorf("query: %s", err)
	}
	sort, err := xhttp.ReadQueryString(r, "sort")
	if err != nil {
		return Options{}, fmt.Errorf("sort: %s", err)
	}
	page, err := xhttp.ReadQueryInt(r, "page")
	if err != nil {
		return Options{}, fmt.Errorf("page: %s", err)
	}
	pageSize, err := xhttp.ReadQueryInt(r, "pageSize")
	if err != nil {
		return Options{}, fmt.Errorf("page: %s", err)
	}
	filters, err := filtersFromQuery(r.URL.Query(), "")
	if err != nil {
		return Options{}, err
	}
	aggregations, err := aggregationsFromQuery(r.URL.Query())
	if err != nil {
		return Options{}, err
	}
	omit, err := xhttp.ReadQueryStringArray(r, "omit")
	if err != nil {
		return Options{}, fmt.Errorf("omit: %s", err)
	}

	return Options{
		Query:        query,
		Sort:         sort,
		Page:         page,
		PageSize:     pageSize,
		Filters:      filters,
		Aggregations: aggregations,
		Omit:         omit,
	}, nil
}
