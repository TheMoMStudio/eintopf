// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package osmproxy

import (
	"fmt"
	"io"
	"net/http"

	"eintopf.info/internal/xhttp"
	"github.com/go-chi/chi/v5"
)

func Router() func(chi.Router) {
	return func(r chi.Router) {
		r.Get("/tile/{z}/{x}/{y}.png", func(w http.ResponseWriter, r *http.Request) {
			osmURL := fmt.Sprintf(
				"https://tile.openstreetmap.de/%s/%s/%s.png",
				chi.URLParam(r, "z"),
				chi.URLParam(r, "x"),
				chi.URLParam(r, "y"),
			)

			r2, err := http.NewRequest("GET", osmURL, nil)
			if err != nil {
				xhttp.WriteInternalError(r.Context(), w, err)
				return
			}
			r2.Header = r.Header
			r2.Header.Del("User-Agent")
			c := &http.Client{}

			response, err := c.Do(r2)
			if err != nil {
				xhttp.WriteInternalError(r.Context(), w, err)
				return
			}
			data, err := io.ReadAll(response.Body)
			if err != nil {
				xhttp.WriteInternalError(r.Context(), w, err)
				return
			}
			for k, vs := range response.Header {
				for _, v := range vs {
					w.Header().Add(k, v)
				}
			}
			w.Write(data)
		})
	}
}
