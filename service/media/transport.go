//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package media

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/xhttp"
	"eintopf.info/service/auth"
)

// Router returns a new http router that handles crud event requests for a given
// event service.
func Router(service Service, authService auth.Service, mediaPath, prefix string) func(chi.Router) {
	server := &server{service, mediaPath, prefix}
	return func(r chi.Router) {
		r.Options("/", xhttp.CorsHandler)
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: false})).
			Post("/", server.AddFile)

		r.Options("/{id}", xhttp.CorsHandler)
		r.Get("/{id}", server.GetFile)
	}
}

type server struct {
	service   Service
	mediaPath string
	prefix    string
}

func (s *server) GetFile(w http.ResponseWriter, r *http.Request) {
	dir := http.Dir(s.mediaPath)

	http.StripPrefix(s.prefix+"/v1/media", http.FileServer(dir)).ServeHTTP(w, r)
}

type AddFileResponse struct {
	ID string `json:"id"`
}

const maxFileSize = 1 * 1024 * 1024 // 1Mb

func (s *server) AddFile(w http.ResponseWriter, r *http.Request) {
	r.Body = http.MaxBytesReader(w, r.Body, maxFileSize)

	// Parse our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	err := r.ParseMultipartForm(maxFileSize)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	// FormFile returns the first file for the given key `media`
	// it also returns the FileHeader so we can get the Filename,
	// the Header and the size of the file
	file, _, err := r.FormFile("media")
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, err)
		return
	}
	defer file.Close()

	id, err := s.service.AddFile(r.Context(), file)
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, err)
		return
	}
	data, err := json.Marshal(&AddFileResponse{id})
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, err)
		return
	}
	w.Write(data)
}
