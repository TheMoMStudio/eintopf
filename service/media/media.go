//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package media

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

type Service interface {
	AddFile(ctx context.Context, file io.Reader) (id string, err error)
	GetFile(ctx context.Context, name string) (file io.Writer, err error)
}

type service struct {
	path string
}

func NewService(path string) Service {
	return &service{
		path: path,
	}
}

func (s *service) AddFile(ctx context.Context, file io.Reader) (string, error) {
	os.MkdirAll(s.path, 0666)
	fileBytes, err := io.ReadAll(file)
	if err != nil {
		return "", err
	}
	hasher := sha256.New()
	_, err = hasher.Write(fileBytes)
	if err != nil {
		return "", err
	}
	id := hex.EncodeToString(hasher.Sum(nil))
	path := filepath.Join(s.path, id)
	err = os.WriteFile(path, fileBytes, 0666)
	if err != nil {
		return "", fmt.Errorf("writing media file to %s: %s", path, err)
	}

	return id, nil
}

func (s *service) GetFile(ctx context.Context, id string) (io.Writer, error) {
	f, err := os.Open(filepath.Join(s.path, id))
	if err != nil {
		return nil, err
	}
	return f, nil
}
