//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package media

import (
	"context"
	"io"

	"eintopf.info/service/auth"
)

type authorizer struct {
	service Service
}

func NewAuthorizer(service Service) Service {
	return &authorizer{service}
}

func (a *authorizer) AddFile(ctx context.Context, file io.Reader) (string, error) {
	_, err := auth.RoleFromContext(ctx)
	if err != nil {
		return "", auth.ErrUnauthorized
	}
	return a.service.AddFile(ctx, file)
}

func (a *authorizer) GetFile(ctx context.Context, id string) (io.Writer, error) {
	return a.GetFile(ctx, id)
}
