// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package email

import (
	"crypto/tls"
	"fmt"
	"net"
	"net/mail"
	"net/smtp"

	"golang.org/x/exp/slices"
)

// Config  is used to configure the email service.
type Config struct {
	SMTPHost     string
	SMTPPassword string
	SMTPUser     string
	SMTPSecure   string
	Mail         string
	AllowList    []string
}

type Service struct {
	config Config
}

func NewService(config Config) *Service {
	return &Service{config: config}
}

var ErrDisallowedEmail = fmt.Errorf("email is not in allow list")

func (s *Service) Send(email, subject, body string) error {
	if len(s.config.AllowList) > 0 && !slices.Contains(s.config.AllowList, email) {
		return ErrDisallowedEmail
	}
	from := mail.Address{Address: s.config.Mail}
	to := mail.Address{Address: email}

	// Setup headers
	headers := map[string]string{
		"From":         from.String(),
		"To":           to.String(),
		"MIME-Version": "1.0",
		"Content-Type": "text/text; charset=utf-8",
		"Subject":      subject,
	}

	// Setup message
	message := ""
	for k, v := range headers {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	message += "\r\n" + body

	host, _, err := net.SplitHostPort(s.config.SMTPHost)
	if err != nil {
		return fmt.Errorf("split host: %s", err)
	}
	// TLS
	insecureSkipVerify := false
	if s.config.SMTPSecure == "SSLInsecure" {
		insecureSkipVerify = true
	}
	tlsconfig := &tls.Config{
		InsecureSkipVerify: insecureSkipVerify,
		ServerName:         host,
	}
	var c *smtp.Client
	if s.config.SMTPSecure == "SSL" || s.config.SMTPSecure == "SSLInsecure" {
		conn, err := tls.Dial("tcp", s.config.SMTPHost, tlsconfig)
		if err != nil {
			return fmt.Errorf("dial tls: %s", err)
		}
		c, err = smtp.NewClient(conn, host)
		if err != nil {
			return fmt.Errorf("new client: %s", err)
		}
	} else if s.config.SMTPSecure == "StartTLS" {
		c, err = smtp.Dial(s.config.SMTPHost)
		if err != nil {
			return fmt.Errorf("smtp dial: %s", err)
		}
		if err := c.StartTLS(tlsconfig); err != nil {
			return fmt.Errorf("smtp starttls: %s", err)
		}
	} else {
		return fmt.Errorf("invalid smpt secure method: '%s'", s.config.SMTPSecure)
	}

	// Auth
	if s.config.SMTPPassword != "" {
		auth := smtp.PlainAuth("", s.config.SMTPUser, s.config.SMTPPassword, host)
		if err := c.Auth(auth); err != nil {
			return fmt.Errorf("smtp auth: %s", err)
		}
	}

	// To && From
	if err := c.Mail(from.Address); err != nil {
		return fmt.Errorf("smtp mail: %s", err)
	}
	if err := c.Rcpt(to.Address); err != nil {
		return fmt.Errorf("smtp rcpt: %s", err)
	}

	// Data
	w, err := c.Data()
	if err != nil {
		return fmt.Errorf("smtp data: %s", err)
	}
	if _, err := w.Write([]byte(message)); err != nil {
		return fmt.Errorf("smtp write: %s", err)
	}
	if err := w.Close(); err != nil {
		return fmt.Errorf("smtp close: %s", err)
	}

	return c.Quit()
}
