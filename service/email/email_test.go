// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package email_test

import (
	"testing"

	"eintopf.info/service/email"
	"eintopf.info/test"
)

func TestEmailSend(t *testing.T) {
	pool := test.InitDockerPool(t)
	smtpHost, client := test.RunMailhogWithSSL(t, pool)

	emailService := email.NewService(email.Config{
		SMTPHost:     smtpHost,
		SMTPUser:     "foo",
		SMTPPassword: "test",
		SMTPSecure:   "SSLInsecure",
		Mail:         "eintopf@example.com",
	})

	err := emailService.Send("foo@example.com", "test message", "a test message")
	if err != nil {
		t.Error(err)
	}

	client.WantLastEmailToBe(t, "eintopf@example.com", "foo@example.com", "test message", "a test message")
}

func TestEmailAllowList(t *testing.T) {
	pool := test.InitDockerPool(t)
	smtpHost, client := test.RunMailhogWithSSL(t, pool)

	emailService := email.NewService(email.Config{
		SMTPHost:     smtpHost,
		SMTPPassword: "",
		SMTPSecure:   "SSLInsecure",
		Mail:         "eintopf@example.com",
		AllowList:    []string{"foo@example.com"},
	})

	err := emailService.Send("bar@example.com", "test message", "a test message")
	if err == nil || err.Error() != email.ErrDisallowedEmail.Error() {
		t.Error("email should be disallowed")
	}
	err = emailService.Send("foo@example.com", "test message", "a test message")
	if err != nil {
		t.Error(err)
	}
	client.WantLastEmailToBe(t, "eintopf@example.com", "foo@example.com", "test message", "a test message")
}
