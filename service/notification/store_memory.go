// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package notification

import (
	"eintopf.info/internal/crud"
)

// NewMemoryStore returns a new in memory store.
func NewMemoryStore() *crud.MemoryStore[NewNotification, Notification, Filters] {
	return crud.NewMemoryStore[NewNotification, Notification, Filters](NotifictaionFromNewNotification, nil)
}

// NewMemoryStore returns a new in memory store.
func NewSettingsMemoryStore() *crud.MemoryStore[NewSettings, Settings, SettingsFilters] {
	return crud.NewMemoryStore[NewSettings, Settings, SettingsFilters](SettingsFromNewSettings, nil)
}
