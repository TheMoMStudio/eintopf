// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package notification

import (
	"context"
	"fmt"
	"time"

	"eintopf.info/internal/crud"
	"eintopf.info/service/oqueue"
	"eintopf.info/service/user"
)

type Notification struct {
	ID        string    `json:"id"`
	CreatedAt time.Time `json:"createdAt" db:"created_at"`
	UserID    string    `json:"userId" db:"user_id"`
	Subject   string    `json:"subject"`
	Message   string    `json:"message"`
	LinkTitle string    `json:"linkTitle" db:"link_title"`
	LinkURL   string    `json:"linkUrl" db:"link_url"`
	Viewed    bool      `json:"viewed"`
}

type Link struct {
	Title string `json:"title"`
	URL   string `json:"url"`
}

func (n Notification) Identifier() string {
	return n.ID
}

type NewNotification struct {
	UserID    string `json:"userId"`
	Subject   string `json:"subject"`
	Message   string `json:"message"`
	LinkTitle string `json:"linkTitle"`
	LinkURL   string `json:"linkUrl"`
}

func NotifictaionFromNewNotification(n *NewNotification, id string) *Notification {
	return &Notification{
		ID:        id,
		CreatedAt: time.Now(),
		UserID:    n.UserID,
		Subject:   n.Subject,
		Message:   n.Message,
		LinkTitle: n.LinkTitle,
		LinkURL:   n.LinkURL,
		Viewed:    false,
	}
}

// FindFilters defines for notifications.
type Filters struct {
	ID      *string `json:"id,omitempty" db:"id"`
	UserID  *string `json:"userId,omitempty" db:"user_id"`
	Subject *string `json:"subject,omitempty" db:"subject"`
	Message *string `json:"message,omitempty" db:"message"`
	Viewed  *bool   `json:"viewed,omitempty" db:"viewed"`
}

type Storer = crud.Storer[NewNotification, Notification, Filters]

// TODO: ensure this is deleted, when the user gets deleted.
type Settings struct {
	UserID string `json:"userId" db:"user_id"`
	Email  bool   `json:"email" db:"email"`
}

func (n Settings) Identifier() string {
	return n.UserID
}

func SettingsFromNewSettings(n *NewSettings, id string) *Settings {
	return &Settings{
		UserID: n.UserID,
		Email:  n.Email,
	}
}

type NewSettings struct {
	UserID string `json:"userId" db:"user_id"`
	Email  bool   `json:"email" db:"email"`
}

type SettingsFilters struct {
	UserID *string `json:"userId,omitempty" db:"user_id"`
	Email  *bool   `json:"email,omitempty" db:"email"`
}

type SettingsStorer = crud.Storer[NewSettings, Settings, SettingsFilters]

type Mailer interface {
	Send(email, subject, body string) error
}

type Service struct {
	mailer        Mailer
	store         Storer
	settingsStore SettingsStorer
	user          user.Storer
}

func NewService(mailer Mailer, store Storer, settingsStore SettingsStorer, user user.Storer) *Service {
	return &Service{
		mailer:        mailer,
		store:         store,
		settingsStore: settingsStore,
		user:          user,
	}
}

type Notifier interface {
	Notify(ctx context.Context, userID string, subject string, message string, link Link) error
}

func (s *Service) ConsumeOperation(op oqueue.Operation) error {
	switch op := op.(type) {
	case user.DeleteOperation:
		_ = s.settingsStore.Delete(context.Background(), op.ID)
	}
	return nil
}

func (s *Service) Notify(ctx context.Context, userID string, subject string, message string, link Link) error {
	_, err := s.store.Create(ctx, &NewNotification{UserID: userID, Subject: subject, Message: message, LinkTitle: link.Title, LinkURL: link.URL})
	if err != nil {
		return fmt.Errorf("create notification: %s", err)
	}
	settings, err := s.settingsStore.FindByID(ctx, userID)
	if err != nil {
		return fmt.Errorf("find settings: %s", err)
	}
	if settings == nil {
		// Settings does not exist yet.
		return nil
	}
	// TODO: enable email
	// if settings.Email {
	// 	user, err := s.user.FindByID(ctx, userID)
	// 	if err != nil {
	// 		return fmt.Errorf("find user: %s", err)
	// 	}
	// 	// TODO: use email template
	// 	err = s.mailer.Send(user.Email, subject, message)
	// 	if err != nil {
	// 		return fmt.Errorf("send mail: %s", err)
	// 	}
	// }
	return nil
}

func (s *Service) Store() Storer {
	return s.store
}

func (s *Service) SettingsStore() SettingsStorer {
	return s.settingsStore
}
