// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package notification_test

import (
	"context"
	"testing"

	"eintopf.info/service/notification"
	"eintopf.info/service/user"
	"eintopf.info/test"
	"github.com/google/go-cmp/cmp"
)

func TestNotify(t *testing.T) {
	mailer := test.NewMailer()
	notificationStore := notification.NewMemoryStore()
	settingsStore := notification.NewSettingsMemoryStore()
	userStore := user.NewMemoryStore()
	service := notification.NewService(mailer, notificationStore, settingsStore, userStore)

	user, err := userStore.Create(context.Background(), &user.NewUser{
		Email: "foo@example.com",
	})
	if err != nil {
		t.Fatal(err)
	}

	// Set email to false
	_, err = settingsStore.Create(context.Background(), &notification.NewSettings{
		UserID: user.ID,
		Email:  false,
	})
	if err != nil {
		t.Fatal(err)
	}

	err = service.Notify(context.Background(), user.ID, "hello", "a notification", notification.Link{Title: "foo", URL: "/foo"})
	if err != nil {
		t.Error(err)
	}
	if diff := cmp.Diff([]test.Mail{}, mailer.Mails); diff != "" {
		t.Errorf("mails mismatch (-want +got):\n%s", diff)
	}

	// TODO: enable when email setup is enabled
	// // Set email to true
	// _, err = settingsStore.Update(context.Background(), &notification.Settings{
	// 	UserID: user.ID,
	// 	Email:  true,
	// })
	// if err != nil {
	// 	t.Fatal(err)
	// }
	//
	// err = service.Notify(context.Background(), user.ID, "hello", "a notification", notification.Link{Title: "foo", URL: "/foo"})
	// if err != nil {
	// 	t.Error(err)
	// }
	// if diff := cmp.Diff([]test.Mail{
	// 	{Email: "foo@example.com", Subject: "hello", Body: "a notification"},
	// }, mailer.Mails); diff != "" {
	// 	t.Errorf("mails mismatch (-want +got):\n%s", diff)
	// }
}
