// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package notification

import (
	"context"
	"fmt"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
)

type Authorizer struct {
	store Storer
}

// NewAuthorizer wraps the given store with authorization methods.
func NewAuthorizer(store Storer) *Authorizer {
	return &Authorizer{store: store}
}

// Create is allowed by
//   - an admin
//   - a moderator
//   - internally
func (a *Authorizer) Create(ctx context.Context, notification *NewNotification) (*Notification, error) {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return nil, auth.ErrUnauthorized
	}
	if role == auth.RoleNormal {
		return nil, auth.ErrUnauthorized
	}
	return a.store.Create(ctx, notification)
}

// Update is allowed by
//   - the user owning the notification
//   - an admin
//   - internally
func (a *Authorizer) Update(ctx context.Context, notification *Notification) (*Notification, error) {
	if err := a.adminOrOwned(ctx, notification.ID); err != nil {
		return nil, err
	}

	return a.store.Update(ctx, notification)
}

// Delete is allowed by
//   - the user owning the notification
//   - an admin
//   - internally
func (a *Authorizer) Delete(ctx context.Context, id string) error {
	if err := a.adminOrOwned(ctx, id); err != nil {
		return err
	}

	return a.store.Delete(ctx, id)
}

// FindByID is allowed by
//   - the user owning the notification
//   - an admin
//   - internally
func (a *Authorizer) FindByID(ctx context.Context, id string) (*Notification, error) {
	if err := a.adminOrOwned(ctx, id); err != nil {
		return nil, err
	}

	return a.store.FindByID(ctx, id)
}

// Find is allowed by
//   - normal and moderator for notifications that are owned
//   - an admin
//   - internally
func (a *Authorizer) Find(ctx context.Context, params *crud.FindParams[Filters]) ([]*Notification, int, error) {
	notifications, total, err := a.store.Find(ctx, params)
	if err != nil {
		return nil, 0, err
	}

	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return nil, 0, auth.ErrUnauthorized
	}
	if role == auth.RoleAdmin || role == auth.RoleInternal {
		return notifications, total, nil
	}

	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return nil, 0, auth.ErrUnauthorized
	}

	filtered := []*Notification{}
	for _, n := range notifications {
		if n.UserID == userID {
			filtered = append(filtered, n)
		} else {
			total--
		}
	}
	return filtered, total, nil
}

func (a *Authorizer) adminOrOwned(ctx context.Context, id string) error {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return auth.ErrUnauthorized
	}
	if role == auth.RoleNormal || role == auth.RoleModerator {
		userID, err := auth.UserIDFromContext(ctx)
		if err != nil {
			return auth.ErrUnauthorized
		}

		n, err := a.store.FindByID(auth.ContextWithRole(ctx, auth.RoleInternal), id)
		if err != nil {
			return err
		}
		if n == nil {
			return fmt.Errorf("could not find existing notification")
		}

		if n.UserID != userID {
			return auth.ErrUnauthorized
		}
	}
	return nil
}

type SettingsAuthorizer struct {
	store SettingsStorer
}

// NewSettingsAuthorizer wraps the given store with authorization methods.
func NewSettingsAuthorizer(store SettingsStorer) *SettingsAuthorizer {
	return &SettingsAuthorizer{store: store}
}

// Create is allowed by
//   - the user owning the settings
//   - an admin
//   - internally
func (a *SettingsAuthorizer) Create(ctx context.Context, settings *NewSettings) (*Settings, error) {
	if err := a.adminOrOwned(ctx, settings.UserID); err != nil {
		return nil, err
	}
	return a.store.Create(ctx, settings)
}

// Update is allowed by
//   - the user owning the settings
//   - an admin
//   - internally
func (a *SettingsAuthorizer) Update(ctx context.Context, settings *Settings) (*Settings, error) {
	if err := a.adminOrOwned(ctx, settings.UserID); err != nil {
		return nil, err
	}

	return a.store.Update(ctx, settings)
}

// Delete is allowed by
//   - the user owning the settings
//   - an admin
//   - internally
func (a *SettingsAuthorizer) Delete(ctx context.Context, id string) error {
	if err := a.adminOrOwned(ctx, id); err != nil {
		return err
	}

	return a.store.Delete(ctx, id)
}

// FindByID is allowed by
//   - the user owning the settings
//   - an admin
//   - internally
func (a *SettingsAuthorizer) FindByID(ctx context.Context, id string) (*Settings, error) {
	if err := a.adminOrOwned(ctx, id); err != nil {
		return nil, err
	}

	return a.store.FindByID(ctx, id)
}

// Find is allowed by
//   - normal and moderator can only retrieve their own settings
//   - an admin
//   - internally
func (a *SettingsAuthorizer) Find(ctx context.Context, params *crud.FindParams[SettingsFilters]) ([]*Settings, int, error) {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return nil, 0, auth.ErrUnauthorized
	}
	if role == auth.RoleNormal || role == auth.RoleModerator {
		if params == nil {
			params = &crud.FindParams[SettingsFilters]{}
		}
		if params.Filters == nil {
			params.Filters = &SettingsFilters{}
		}
		id, err := auth.UserIDFromContext(ctx)
		if err != nil {
			return nil, 0, auth.ErrUnauthorized
		}
		params.Filters.UserID = &id
	}

	return a.store.Find(ctx, params)
}

func (a *SettingsAuthorizer) adminOrOwned(ctx context.Context, id string) error {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return auth.ErrUnauthorized
	}
	if role == auth.RoleNormal || role == auth.RoleModerator {
		userID, err := auth.UserIDFromContext(ctx)
		if err != nil {
			return auth.ErrUnauthorized
		}

		if id != userID {
			return auth.ErrUnauthorized
		}
	}
	return nil
}
