// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package notification_test

import (
	"context"
	"testing"

	"eintopf.info/service/auth"
	"eintopf.info/service/notification"
)

var (
	normalCtx    = auth.ContextWithRole(context.Background(), auth.RoleNormal)
	moderatorCtx = auth.ContextWithRole(context.Background(), auth.RoleModerator)
	adminCtx     = auth.ContextWithRole(context.Background(), auth.RoleAdmin)
	internalCtx  = auth.ContextWithRole(context.Background(), auth.RoleInternal)
)

func TestAuthorizerCreate(t *testing.T) {
	store := notification.NewMemoryStore()
	authorizer := notification.NewAuthorizer(store)

	t.Run("Background context -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.Create(context.Background(), &notification.NewNotification{})
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Normal -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.Create(normalCtx, &notification.NewNotification{})
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Moderator -> Authorized", func(t *testing.T) {
		_, err := authorizer.Create(moderatorCtx, &notification.NewNotification{})
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})

	t.Run("Admin -> Authorized", func(t *testing.T) {
		_, err := authorizer.Create(adminCtx, &notification.NewNotification{})
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})

	t.Run("Internal -> Authorized", func(t *testing.T) {
		_, err := authorizer.Create(internalCtx, &notification.NewNotification{})
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})
}

func TestAuthorizerUpdate(t *testing.T) {
	store := notification.NewMemoryStore()
	_, err := store.Create(context.Background(), &notification.NewNotification{
		UserID:  "1312",
		Message: "all cats are beautifull",
	})
	if err != nil {
		t.Fatal(err)
	}
	authorizer := notification.NewAuthorizer(store)

	t.Run("Background -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.Update(context.Background(), &notification.Notification{})
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Different Normal user -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.Update(auth.ContextWithID(normalCtx, "foo"), &notification.Notification{
			ID:     "0",
			UserID: "1312",
			Viewed: true,
		})
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Same Normal user -> Authorized", func(t *testing.T) {
		_, err := authorizer.Update(auth.ContextWithID(normalCtx, "1312"), &notification.Notification{
			ID:     "0",
			UserID: "1312",
			Viewed: true,
		})
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})

	t.Run("Different Moderator -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.Update(auth.ContextWithID(moderatorCtx, "foo"), &notification.Notification{
			ID:     "0",
			UserID: "1312",
			Viewed: true,
		})
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Same Moderator -> Authorized", func(t *testing.T) {
		_, err := authorizer.Update(auth.ContextWithID(moderatorCtx, "1312"), &notification.Notification{
			ID:     "0",
			UserID: "1312",
			Viewed: true,
		})
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})

	t.Run("Admin -> Authorized", func(t *testing.T) {
		_, err := authorizer.Update(adminCtx, &notification.Notification{ID: "0"})
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})

	t.Run("Internal -> Authorized", func(t *testing.T) {
		_, err := authorizer.Update(internalCtx, &notification.Notification{ID: "0"})
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})
}

func TestAuthorizerDelete(t *testing.T) {
	store := notification.NewMemoryStore()
	_, err := store.Create(context.Background(), &notification.NewNotification{
		UserID:  "1312",
		Message: "all cats are beautifull",
	})
	if err != nil {
		t.Fatal(err)
	}
	authorizer := notification.NewAuthorizer(store)

	t.Run("Background -> Unauthorized", func(t *testing.T) {
		err := authorizer.Delete(context.Background(), "0")
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Different Normal user -> Unauthorized", func(t *testing.T) {
		err := authorizer.Delete(auth.ContextWithID(normalCtx, "foo"), "0")
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Same Normal user -> Authorized", func(t *testing.T) {
		err := authorizer.Delete(auth.ContextWithID(normalCtx, "1312"), "0")
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		_, err = store.Create(context.Background(), &notification.NewNotification{
			UserID:  "1312",
			Message: "all cats are beautifull",
		})
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Different Moderator -> Unauthorized", func(t *testing.T) {
		err := authorizer.Delete(auth.ContextWithID(moderatorCtx, "foo"), "0")
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Same Moderator -> Authorized", func(t *testing.T) {
		err := authorizer.Delete(auth.ContextWithID(moderatorCtx, "1312"), "0")
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		_, err = store.Create(context.Background(), &notification.NewNotification{
			UserID:  "1312",
			Message: "all cats are beautifull",
		})
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Admin -> Authorized", func(t *testing.T) {
		err := authorizer.Delete(adminCtx, "0")
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		_, err = store.Create(context.Background(), &notification.NewNotification{
			UserID:  "1312",
			Message: "all cats are beautifull",
		})
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Internal -> Authorized", func(t *testing.T) {
		err := authorizer.Delete(internalCtx, "0")
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})
}

func TestAuthorizerFindByID(t *testing.T) {
	store := notification.NewMemoryStore()
	_, err := store.Create(context.Background(), &notification.NewNotification{
		UserID:  "1312",
		Message: "all cats are beautifull",
	})
	if err != nil {
		t.Fatal(err)
	}
	authorizer := notification.NewAuthorizer(store)

	t.Run("Background -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.FindByID(context.Background(), "0")
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Different Normal user -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.FindByID(auth.ContextWithID(normalCtx, "foo"), "0")
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Same Normal user -> Authorized", func(t *testing.T) {
		_, err := authorizer.FindByID(auth.ContextWithID(normalCtx, "1312"), "0")
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})

	t.Run("Different Moderator -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.FindByID(auth.ContextWithID(moderatorCtx, "foo"), "0")
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Same Moderator -> Authorized", func(t *testing.T) {
		_, err := authorizer.FindByID(auth.ContextWithID(moderatorCtx, "1312"), "0")
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})

	t.Run("Admin -> Authorized", func(t *testing.T) {
		_, err := authorizer.FindByID(adminCtx, "0")
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})

	t.Run("Internal -> Authorized", func(t *testing.T) {
		_, err := authorizer.FindByID(internalCtx, "0")
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})
}

func TestAuthorizerFind(t *testing.T) {
	store := notification.NewMemoryStore()
	_, err := store.Create(context.Background(), &notification.NewNotification{
		UserID:  "1312",
		Message: "all cats are beautifull",
	})
	if err != nil {
		t.Fatal(err)
	}
	_, err = store.Create(context.Background(), &notification.NewNotification{
		UserID:  "0",
		Message: "foo",
	})
	if err != nil {
		t.Fatal(err)
	}
	authorizer := notification.NewAuthorizer(store)

	t.Run("Background -> Unauthorized", func(t *testing.T) {
		_, _, err := authorizer.Find(context.Background(), nil)
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Different Normal user -> No Result", func(t *testing.T) {
		_, total, err := authorizer.Find(auth.ContextWithID(normalCtx, "foo"), nil)
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		if total != 0 {
			t.Errorf("wrong total: %d", total)
		}
	})

	t.Run("Same Normal user -> Authorized", func(t *testing.T) {
		_, total, err := authorizer.Find(auth.ContextWithID(normalCtx, "1312"), nil)
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		if total != 1 {
			t.Errorf("wrong total: %d", total)
		}
	})

	t.Run("Different Moderator -> Unauthorized", func(t *testing.T) {
		_, total, err := authorizer.Find(auth.ContextWithID(moderatorCtx, "foo"), nil)
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		if total != 0 {
			t.Errorf("wrong total: %d", total)
		}
	})

	t.Run("Same Moderator -> Authorized", func(t *testing.T) {
		_, total, err := authorizer.Find(auth.ContextWithID(moderatorCtx, "1312"), nil)
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		if total != 1 {
			t.Errorf("wrong total: %d", total)
		}
	})

	t.Run("Admin -> Authorized", func(t *testing.T) {
		_, total, err := authorizer.Find(adminCtx, nil)
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		if total != 2 {
			t.Errorf("wrong total: %d", total)
		}
	})

	t.Run("Internal -> Authorized", func(t *testing.T) {
		_, total, err := authorizer.Find(internalCtx, nil)
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		if total != 2 {
			t.Errorf("wrong total: %d", total)
		}
	})
}

func TestSettingsAuthorizerCreate(t *testing.T) {
	store := notification.NewSettingsMemoryStore()
	authorizer := notification.NewSettingsAuthorizer(store)

	t.Run("Background context -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.Create(context.Background(), &notification.NewSettings{})
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Normal -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.Create(auth.ContextWithID(normalCtx, "1312"), &notification.NewSettings{UserID: "foo"})
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Normal (self)-> Authorized", func(t *testing.T) {
		_, err := authorizer.Create(auth.ContextWithID(normalCtx, "1312"), &notification.NewSettings{UserID: "1312"})
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})

	t.Run("Moderator -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.Create(auth.ContextWithID(moderatorCtx, "1312"), &notification.NewSettings{UserID: "foo"})
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Moderator (self)-> Authorized", func(t *testing.T) {
		_, err := authorizer.Create(auth.ContextWithID(moderatorCtx, "1312"), &notification.NewSettings{UserID: "1312"})
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})

	t.Run("Admin -> Authorized", func(t *testing.T) {
		_, err := authorizer.Create(adminCtx, &notification.NewSettings{UserID: "foo"})
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})

	t.Run("Internal -> Authorized", func(t *testing.T) {
		_, err := authorizer.Create(internalCtx, &notification.NewSettings{UserID: "foo"})
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})
}

func TestSettingsAuthorizerUpdate(t *testing.T) {
	store := notification.NewSettingsMemoryStore()
	_, err := store.Create(context.Background(), &notification.NewSettings{
		UserID: "1312",
		Email:  false,
	})
	if err != nil {
		t.Fatal(err)
	}
	authorizer := notification.NewSettingsAuthorizer(store)

	t.Run("Background -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.Update(context.Background(), &notification.Settings{})
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Different Normal user -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.Update(auth.ContextWithID(normalCtx, "foo"), &notification.Settings{
			UserID: "1312",
			Email:  true,
		})
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Same Normal user -> Authorized", func(t *testing.T) {
		_, err := authorizer.Update(auth.ContextWithID(normalCtx, "1312"), &notification.Settings{
			UserID: "1312",
			Email:  true,
		})
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})

	t.Run("Different Moderator -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.Update(auth.ContextWithID(moderatorCtx, "foo"), &notification.Settings{
			UserID: "1312",
			Email:  true,
		})
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Same Moderator -> Authorized", func(t *testing.T) {
		_, err := authorizer.Update(auth.ContextWithID(moderatorCtx, "1312"), &notification.Settings{
			UserID: "1312",
			Email:  true,
		})
		if err != nil {
			t.Errorf("should not return unauthorized error: %s", err)
		}
	})

	t.Run("Admin -> Authorized", func(t *testing.T) {
		_, err := authorizer.Update(adminCtx, &notification.Settings{UserID: "1312", Email: true})
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})

	t.Run("Internal -> Authorized", func(t *testing.T) {
		_, err := authorizer.Update(internalCtx, &notification.Settings{UserID: "1312", Email: true})
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})
}

func TestSettingsAuthorizerDelete(t *testing.T) {
	store := notification.NewSettingsMemoryStore()
	_, err := store.Create(context.Background(), &notification.NewSettings{
		UserID: "1312",
	})
	if err != nil {
		t.Fatal(err)
	}
	authorizer := notification.NewSettingsAuthorizer(store)

	t.Run("Background -> Unauthorized", func(t *testing.T) {
		err := authorizer.Delete(context.Background(), "1312")
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Different Normal user -> Unauthorized", func(t *testing.T) {
		err := authorizer.Delete(auth.ContextWithID(normalCtx, "foo"), "1312")
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Same Normal user -> Authorized", func(t *testing.T) {
		err := authorizer.Delete(auth.ContextWithID(normalCtx, "1312"), "1312")
		if err != nil {
			t.Errorf("should not return unauthorized error: %s", err)
		}
		_, err = store.Create(context.Background(), &notification.NewSettings{
			UserID: "1312",
		})
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Different Moderator -> Unauthorized", func(t *testing.T) {
		err := authorizer.Delete(auth.ContextWithID(moderatorCtx, "foo"), "1312")
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Same Moderator -> Authorized", func(t *testing.T) {
		err := authorizer.Delete(auth.ContextWithID(moderatorCtx, "1312"), "1312")
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		_, err = store.Create(context.Background(), &notification.NewSettings{
			UserID: "1312",
		})
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Admin -> Authorized", func(t *testing.T) {
		err := authorizer.Delete(adminCtx, "1312")
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		_, err = store.Create(context.Background(), &notification.NewSettings{
			UserID: "1312",
		})
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Internal -> Authorized", func(t *testing.T) {
		err := authorizer.Delete(internalCtx, "1312")
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})
}

func TestSettingsAuthorizerFindByID(t *testing.T) {
	store := notification.NewSettingsMemoryStore()
	_, err := store.Create(context.Background(), &notification.NewSettings{
		UserID: "1312",
	})
	if err != nil {
		t.Fatal(err)
	}
	authorizer := notification.NewSettingsAuthorizer(store)

	t.Run("Background -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.FindByID(context.Background(), "1312")
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Different Normal user -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.FindByID(auth.ContextWithID(normalCtx, "foo"), "1312")
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Same Normal user -> Authorized", func(t *testing.T) {
		_, err := authorizer.FindByID(auth.ContextWithID(normalCtx, "1312"), "1312")
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})

	t.Run("Different Moderator -> Unauthorized", func(t *testing.T) {
		_, err := authorizer.FindByID(auth.ContextWithID(moderatorCtx, "foo"), "1312")
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Same Moderator -> Authorized", func(t *testing.T) {
		_, err := authorizer.FindByID(auth.ContextWithID(moderatorCtx, "1312"), "1312")
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})

	t.Run("Admin -> Authorized", func(t *testing.T) {
		_, err := authorizer.FindByID(adminCtx, "1312")
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})

	t.Run("Internal -> Authorized", func(t *testing.T) {
		_, err := authorizer.FindByID(internalCtx, "1312")
		if err != nil {
			t.Error("should not return unauthorized error")
		}
	})
}

func TestSettingsAuthorizerFind(t *testing.T) {
	store := notification.NewSettingsMemoryStore()
	_, err := store.Create(context.Background(), &notification.NewSettings{
		UserID: "1312",
	})
	if err != nil {
		t.Fatal(err)
	}
	_, err = store.Create(context.Background(), &notification.NewSettings{
		UserID: "0",
	})
	if err != nil {
		t.Fatal(err)
	}
	authorizer := notification.NewSettingsAuthorizer(store)

	t.Run("Background -> Unauthorized", func(t *testing.T) {
		_, _, err := authorizer.Find(context.Background(), nil)
		if err == nil {
			t.Error("should return unauthorized error")
		}
	})

	t.Run("Different Normal user -> No Result", func(t *testing.T) {
		_, total, err := authorizer.Find(auth.ContextWithID(normalCtx, "foo"), nil)
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		if total != 0 {
			t.Errorf("wrong total: %d", total)
		}
	})

	t.Run("Same Normal user -> Authorized", func(t *testing.T) {
		_, total, err := authorizer.Find(auth.ContextWithID(normalCtx, "1312"), nil)
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		if total != 1 {
			t.Errorf("wrong total: %d", total)
		}
	})

	t.Run("Different Moderator -> Unauthorized", func(t *testing.T) {
		_, total, err := authorizer.Find(auth.ContextWithID(moderatorCtx, "foo"), nil)
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		if total != 0 {
			t.Errorf("wrong total: %d", total)
		}
	})

	t.Run("Same Moderator -> Authorized", func(t *testing.T) {
		_, total, err := authorizer.Find(auth.ContextWithID(moderatorCtx, "1312"), nil)
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		if total != 1 {
			t.Errorf("wrong total: %d", total)
		}
	})

	t.Run("Admin -> Authorized", func(t *testing.T) {
		_, total, err := authorizer.Find(adminCtx, nil)
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		if total != 2 {
			t.Errorf("wrong total: %d", total)
		}
	})

	t.Run("Internal -> Authorized", func(t *testing.T) {
		_, total, err := authorizer.Find(internalCtx, nil)
		if err != nil {
			t.Error("should not return unauthorized error")
		}
		if total != 2 {
			t.Errorf("wrong total: %d", total)
		}
	})
}
