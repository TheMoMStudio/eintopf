// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package notification

import (
	"context"

	"eintopf.info/internal/crud"
	"eintopf.info/service/dbmigration"
	"github.com/jmoiron/sqlx"
)

// NewSqlStore returns a new sql store for notifications.
func NewSqlStore(db *sqlx.DB, migrationService dbmigration.Service) (*SqlStore, error) {
	store := &SqlStore{
		db,
		migrationService,
		crud.NewSqlStore(db, notificationTable, NotifictaionFromNewNotification, nil),
	}
	if err := store.runMigrations(context.Background()); err != nil {
		return nil, err
	}
	return store, nil
}

type SqlStore struct {
	db               *sqlx.DB
	migrationService dbmigration.Service

	*crud.SqlStore[NewNotification, Notification, Filters]
}

func (s *SqlStore) runMigrations(ctx context.Context) error {
	return s.migrationService.RunMigrations(ctx, []dbmigration.Migration{
		dbmigration.NewMigration("createNotificationTable", s.createNotificationTable, nil),
		dbmigration.NewMigration("addNotificationLinkFields", s.addotificationLinkFields, nil),
	})
}

func (s *SqlStore) createNotificationTable(ctx context.Context) error {
	_, err := s.db.ExecContext(ctx, `
        CREATE TABLE IF NOT EXISTS notifications (
            id VARCHAR(32) NOT NULL PRIMARY KEY UNIQUE,
            created_at TIMESTAMP,
            user_id VARCHAR(32) NOT NULL,
            subject TEXT NOT NULL,
            message TEXT NOT NULL,
            viewed BOOLEAN DEFAULT FALSE
        );
    `)
	return err
}
func (s *SqlStore) addotificationLinkFields(ctx context.Context) error {
	_, err := s.db.ExecContext(ctx, `
		ALTER TABLE notifications ADD COLUMN link_title TEXT NOT NULL;
		ALTER TABLE notifications ADD COLUMN link_url TEXT NOT NULL;
		UPDATE notifications SET link_title="", link_url="";
    `)
	return err
}

var notificationTable = crud.SqlTable[Filters]{
	IDField:        "id",
	Table:          "notifications",
	Fields:         []string{"id", "created_at", "user_id", "subject", "message", "link_title", "link_url", "viewed"},
	SortableFields: []string{"id", "created_at", "user_id", "subject", "viewed"},
}

// NewSqlStore returns a new sql db event store.
func NewSettingsSqlStore(db *sqlx.DB, migrationService dbmigration.Service) (*SettingsSqlStore, error) {
	store := &SettingsSqlStore{
		db,
		migrationService,
		crud.NewSqlStore(db, settingsTable, SettingsFromNewSettings, nil),
	}
	if err := store.runMigrations(context.Background()); err != nil {
		return nil, err
	}
	return store, nil
}

type SettingsSqlStore struct {
	db               *sqlx.DB
	migrationService dbmigration.Service

	*crud.SqlStore[NewSettings, Settings, SettingsFilters]
}

func (s *SettingsSqlStore) runMigrations(ctx context.Context) error {
	return s.migrationService.RunMigrations(ctx, []dbmigration.Migration{
		dbmigration.NewMigration("createNotificationSettingsTable", s.createSettingsTable, nil),
	})
}

func (s *SettingsSqlStore) createSettingsTable(ctx context.Context) error {
	_, err := s.db.ExecContext(ctx, `
        CREATE TABLE IF NOT EXISTS notification_settings (
            user_id VARCHAR(32) NOT NULL PRIMARY KEY UNIQUE,
            email BOOLEAN DEFAULT FALSE
        );
    `)
	return err
}

var settingsTable = crud.SqlTable[SettingsFilters]{
	IDField:        "user_id",
	Table:          "notification_settings",
	Fields:         []string{"user_id", "email"},
	SortableFields: []string{"user_id", "email"},
}
