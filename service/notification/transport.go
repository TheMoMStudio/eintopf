// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package notification

import (
	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/crud"
	"eintopf.info/internal/xhttp"
	"eintopf.info/service/auth"
)

// Router returns a new http router that handles crud requests for
// notifications and notification settings.
func Router(storer Storer, settingsStorer SettingsStorer, authService auth.Service) func(r chi.Router) {
	return func(r chi.Router) {
		notificationRoutes(r, storer, authService)
		r.Route("/settings", func(r chi.Router) {
			settingsRoutes(r, settingsStorer, authService)
		})
	}
}

// swagger:response findNotificationsResponse
type findResponse struct {
	// in:body
	Notifications []*Notification

	// in:header
	TotalNotifications int `json:"x-total-count"`
}

// swagger:parameters createNotification
type createRequest struct {
	// in:body
	Notification *NewNotification
}

// swagger:response createNotificationResponse
type createResponse struct {
	// in:body
	Notification *Notification
}

// swagger:parameters findNotification
type findByIDRequest struct {
	// in:query
	ID string `json:"id"`
}

// swagger:response findNotificationResponse
type findByIDResponse struct {
	// in:body
	Notification *Notification
}

type FindResponse[Model any] struct {
	// in:body
	Model *Model
}

// swagger:parameters updateNotification
type updateRequest struct {
	// in:body
	Notification *Notification
}

// swagger:response updateNotificationResponse
type updateResponse struct {
	// in:body
	Notification *Notification
}

// swagger:parameters deleteNotification
type deleteRequest struct {
	// in:query
	ID string `json:"id"`
}

// swagger:response deleteNotificationResponse
type deleteResponse struct{}

func notificationRoutes(r chi.Router, storer Storer, authService auth.Service) {
	handler := crud.NewHandler(storer)

	r.Options("/", xhttp.CorsHandler)

	// swagger:route GET /notifications/ notification findNotifications
	//
	// Retrieves all notifications.
	//
	// Parameters:
	//   + name: offset
	//     description: offset in notification list
	//     in: query
	//     type: integer
	//     required: false
	//   + name: limit
	//     description: limits the notification list
	//     in: query
	//     type: integer
	//     required: false
	//   + name: sort
	//     description: field that gets sorted
	//     in: query
	//     type: string
	//     required: false
	//   + name: order
	//     description: sort order ("ASC" or "DESC")
	//     in: query
	//     type: string
	//     required: false
	//   + name: filters
	//     description: filters get combined with AND logic
	//     in: query
	//     type: object
	//     required: false
	//
	//     Responses:
	//       200: findNotificationsResponse
	//       400: badRequest
	//       401: unauthorizedError
	//       500: internalError
	//       500: internalError
	//       503: serviceUnavailable
	r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
		Get("/", handler.Find)

	// swagger:route POST /notifications/ notification createNotifcation
	//
	// Creates a new notification.
	//     Security:
	//       bearer: []
	//
	//     Responses:
	//       200: createNotificationResponse
	//       400: badRequest
	//       401: unauthorizedError
	//       500: internalError
	//       503: serviceUnavailable
	r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
		Post("/", handler.Create)

	r.Options("/{id}", xhttp.CorsHandler)

	// swagger:route GET /notifications/{id} notification findNotification
	//
	// Finds the notification with the given id.
	//
	//     Responses:
	//       200: findNotificationResponse
	//       400: badRequest
	//       401: unauthorizedError
	//       500: internalError
	//       503: serviceUnavailable
	r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
		Get("/{id}", handler.FindByID)

	// swagger:route PUT /notifications/{id} notification updateNotification
	//
	// Updates the notification with the given id.
	//     Security:
	//       bearer: []
	//
	//     Responses:
	//       200: updateNotificationResponse
	//       400: badRequest
	//       401: unauthorizedError
	//       500: internalError
	//       503: serviceUnavailable
	r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
		Put("/{id}", handler.Update)

	// swagger:route DELETE /notifications/{id} notification deleteNotifications
	//
	// Deletes the notification with the given id.
	//     Security:
	//       bearer: []
	//
	//     Responses:
	//       200: deleteNotificationResponse
	//       400: badRequest
	//       401: unauthorizedError
	//       500: internalError
	//       503: serviceUnavailable
	r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
		Delete("/{id}", handler.Delete)
}

// swagger:response findNotificationSettingsResponse
type findSettingsResponse struct {
	// in:body
	Settings []*Settings

	// in:header
	TotalSettings int `json:"x-total-count"`
}

// swagger:parameters createNotificationSettings
type createSettingsRequest struct {
	// in:body
	Settings *NewSettings
}

// swagger:response createNotificationSettingsResponse
type createSettingsResponse struct {
	// in:body
	Settings *Settings
}

// swagger:parameters findSettingsNotification
type findSettingsByIDRequest struct {
	// in:query
	ID string `json:"id"`
}

// swagger:response findNotificationSettingsResponse
type findSettingsByIDResponse struct {
	// in:body
	Settings *Settings
}

// swagger:parameters updateNotificationSettings
type updateSettingsRequest struct {
	// in:body
	Settings *Settings
}

// swagger:response updateNotificationSettingsResponse
type updateSettingsResponse struct {
	// in:body
	Settings *Settings
}

// swagger:parameters deleteNotificationSettings
type deleteSettingsRequest struct {
	// in:query
	ID string `json:"id"`
}

// swagger:response deleteNotificationSettingsResponse
type deleteSettingsResponse struct{}

func settingsRoutes(r chi.Router, storer SettingsStorer, authService auth.Service) {
	handler := crud.NewHandler(storer)
	r.Options("/", xhttp.CorsHandler)

	// swagger:route GET /notifications/settings/ notificationSetting findNotificationSettings
	//
	// Retrieves all notification settings.
	//
	// Parameters:
	//   + name: offset
	//     description: offset in the list
	//     in: query
	//     type: integer
	//     required: false
	//   + name: limit
	//     description: limits the list
	//     in: query
	//     type: integer
	//     required: false
	//   + name: sort
	//     description: field that gets sorted
	//     in: query
	//     type: string
	//     required: false
	//   + name: order
	//     description: sort order ("ASC" or "DESC")
	//     in: query
	//     type: string
	//     required: false
	//   + name: filters
	//     description: filters get combined with AND logic
	//     in: query
	//     type: object
	//     required: false
	//
	//     Responses:
	//       200: findNotificationSettingsResponse
	//       400: badRequest
	//       401: unauthorizedError
	//       500: internalError
	//       500: internalError
	//       503: serviceUnavailable
	r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: false})).
		Get("/", handler.Find)

	// swagger:route POST /notifications/settings/ notificationSetting createNotifcationSettings
	//
	// Creates a new notification settings.
	//     Security:
	//       bearer: []
	//
	//     Responses:
	//       200: createNotificationSettingsResponse
	//       400: badRequest
	//       401: unauthorizedError
	//       500: internalError
	//       503: serviceUnavailable
	r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
		Post("/", handler.Create)

	r.Options("/{id}", xhttp.CorsHandler)

	// swagger:route GET /notifications/settings/{id} notificationSettings findNotificationSetting
	//
	// Finds the notification setting with the given id.
	//
	//     Responses:
	//       200: findNotificationResponse
	//       400: badRequest
	//       401: unauthorizedError
	//       500: internalError
	//       503: serviceUnavailable
	r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
		Get("/{id}", handler.FindByID)

	// swagger:route PUT /notifications/settings/{id} notificationSettings updateNotificationSettings
	//
	// Updates the notifcation settings with the given id.
	//     Security:
	//       bearer: []
	//
	//     Responses:
	//       200: updateNotificationSettingsResponse
	//       400: badRequest
	//       401: unauthorizedError
	//       500: internalError
	//       503: serviceUnavailable
	r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
		Put("/{id}", handler.Update)

	// swagger:route DELETE /notifications/settings/{id} notificationSetting deleteNotificationSettings
	//
	// Deletes the notification settings with the given id.
	//     Security:
	//       bearer: []
	//
	//     Responses:
	//       200: deleteNotificationSettingsResponse
	//       400: badRequest
	//       401: unauthorizedError
	//       500: internalError
	//       503: serviceUnavailable
	r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
		Delete("/{id}", handler.Delete)
}
