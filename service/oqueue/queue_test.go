//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package oqueue_test

import (
	"sync"
	"testing"
	"time"

	"eintopf.info/service/oqueue"
)

type testOperation struct{}

func (t testOperation) UserID() string { return "" }

func TestOqueueWaitsOnClose(t *testing.T) {
	queue := oqueue.NewQueue()

	counter := 0
	m := sync.Mutex{}
	queue.AddSubscriber(func(op oqueue.Operation) error {
		time.Sleep(time.Millisecond * 100)
		m.Lock()
		counter++
		m.Unlock()
		return nil
	}, 2)

	queue.AddOperation(testOperation{})
	queue.AddOperation(testOperation{})
	queue.AddOperation(testOperation{})

	queue.Close()
	if counter != 3 {
		t.Errorf("all operations should be consumed and processed. only got %d", counter)
	}
}

func TestMultipleSubscribers(t *testing.T) {
	queue := oqueue.NewQueue()

	counter := 0
	m := sync.Mutex{}
	queue.AddSubscriber(func(op oqueue.Operation) error {
		m.Lock()
		counter++
		m.Unlock()
		return nil
	}, 2)
	queue.AddSubscriber(func(op oqueue.Operation) error {
		m.Lock()
		counter++
		m.Unlock()
		return nil
	}, 2)

	queue.AddOperation(testOperation{})
	queue.AddOperation(testOperation{})
	queue.AddOperation(testOperation{})

	queue.Close()
	if counter != 6 {
		t.Errorf("all operations should be consumed and processed twice. only got %d", counter)
	}
}
