//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package oqueue

import (
	"fmt"
	"log"
	"sync"
	"time"
)

// Service defines an interface for handling an operator queue. It has methods
// to add and consume operations.
// All methods should be save for concurrent use.
type Service interface {
	// AddOperation adds an operation to the queue.
	// It might return an error, if the queue is closed.
	AddOperation(op Operation) error

	// AddSubscriber adds an operation consumer. It gets notified for every
	// operation landing in the queue.
	// If the consumer returns an error, the error gets logged.
	// The amount of worker defines how many operations are to be consumed
	// concurrently.
	AddSubscriber(consume OperationConsumer, worker int)

	// Close closes the queue. When closed the queue no longer accepts
	// operations.
	// It waits for all operations to be processed before returning.
	Close()
}

// OperationConsumer is a function, that consumes an operation. The consumer
// might return an error.
type OperationConsumer func(op Operation) error

// Queue implements an operation queue.
type Queue struct {
	subscriber []*subscriber
	closed     bool
	wgAll      sync.WaitGroup
}

// Operation is an operation, that can be added to the oqueue.
type Operation interface {
	UserID() string
}

type subscriber struct {
	queue chan Operation
}

// NewQueue returns a new queue.
func NewQueue() *Queue {
	return &Queue{
		subscriber: make([]*subscriber, 0),
		closed:     false,
		wgAll:      sync.WaitGroup{},
	}
}

var ErrClosed = fmt.Errorf("queue was closed")

// AddOperation adds an operation to the queue.
// Returns ErrClosed if the queue was closed.
func (q *Queue) AddOperation(op Operation) error {
	if q.closed {
		return ErrClosed
	}
	for _, subscriber := range q.subscriber {
		subscriber.queue <- op
	}
	return nil
}

func (q *Queue) AddSubscriber(consume OperationConsumer, worker int) {
	subscriber := &subscriber{queue: make(chan Operation)}
	q.subscriber = append(q.subscriber, subscriber)
	for i := 0; i < worker; i++ {
		q.wgAll.Add(1)
		go func() {
			for op := range subscriber.queue {
				if err := consume(op); err != nil {
					log.Printf("oqueue: subscriber consume(%v): %s", op, err)
				}
			}
			q.wgAll.Done()
		}()
	}
}

// Close closes the the queue and waits for all elements in the queue to be
// processed.
func (q *Queue) Close() {
	q.closed = true

	// Wait for all subscriber queues to be empty before returning.
Outer:
	for {
		time.Sleep(time.Millisecond * 100)
		for _, subscriber := range q.subscriber {
			if len(subscriber.queue) > 1 {
				continue Outer
			}
			close(subscriber.queue)
		}
		break
	}
	q.wgAll.Wait()
}
