//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package revent

import (
	"context"
	"fmt"
	"time"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
	"eintopf.info/service/event"
)

// authorizer wraps a Storer. It checks the authorization before propagating
// the call to the wrapped storer.
type authorizer struct {
	service Service
}

// NewAuthorizer returns a new role manager limiting access to the provided
// store.
func NewAuthorizer(service Service) Service {
	return &authorizer{service}
}

// Create can be called by all users.
func (r *authorizer) Create(ctx context.Context, newRepeatingEvent *NewRepeatingEvent) (*RepeatingEvent, error) {
	_, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return nil, auth.ErrUnauthorized
	}
	return r.service.Create(ctx, newRepeatingEvent)
}

// Update can be called by an admin user or the repeatingEvent author.
func (r *authorizer) Update(ctx context.Context, repeatingEvent *RepeatingEvent) (*RepeatingEvent, error) {
	if err := r.adminOrOwned(ctx, repeatingEvent.ID); err != nil {
		return nil, err
	}
	oldRepeatingEvent, err := r.service.FindByID(ctx, repeatingEvent.ID)
	if err != nil {
		return nil, err
	}
	if oldRepeatingEvent == nil {
		return nil, fmt.Errorf("cant find repeatingEvent with id: %s", repeatingEvent.ID)
	}
	if oldRepeatingEvent.Deactivated != repeatingEvent.Deactivated {
		role, err := auth.RoleFromContext(ctx)
		if err != nil {
			return nil, auth.ErrUnauthorized
		}
		if role == auth.RoleNormal {
			return nil, auth.ErrUnauthorized
		}
	}
	return r.service.Update(ctx, repeatingEvent)
}

// Delete can be called by an admin user or the repeatingEvent author.
func (r *authorizer) Delete(ctx context.Context, id string) error {
	if err := r.adminOrOwned(ctx, id); err != nil {
		return err
	}
	return r.service.Delete(ctx, id)
}

// FindByID can be called by everyone.
func (r *authorizer) FindByID(ctx context.Context, id string) (*RepeatingEvent, error) {
	return r.service.FindByID(ctx, id)
}

// Find can be called by everyone.
func (r *authorizer) Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*RepeatingEvent, int, error) {
	return r.service.Find(ctx, params)
}

func (a *authorizer) GenerateEvents(ctx context.Context, id string, start, end time.Time) ([]*event.Event, error) {
	if err := a.adminOrOwned(ctx, id); err != nil {
		return nil, err
	}
	return a.service.GenerateEvents(ctx, id, start, end)
}

func (r *authorizer) adminOrOwned(ctx context.Context, repeatingEventID string) error {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return auth.ErrUnauthorized
	}
	if role == auth.RoleAdmin || role == auth.RoleInternal {
		return nil
	}

	repeatingEvent, err := r.FindByID(ctx, repeatingEventID)
	if err != nil {
		return err
	}
	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return auth.ErrUnauthorized
	}
	if !repeatingEvent.IsOwned(userID) {
		return auth.ErrUnauthorized
	}

	return nil
}
