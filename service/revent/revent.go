//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

// package revent implement repeating events with a very flexible interval type.
//
// Example intervals are:
//   - every day
//   - every second day
//   - every monday
//   - every second tuesday
//   - every first monday of a month
//   - every second tuesday of a month
//   - every first monday of every second month
//
// This gives three categories of intervals:
//   - DayInterval:   every [1..7] day, starting at [date]
//   - WeekInterval:  every [1..] [weekday] of week
//   - MonthInterval: every [1..4] [weekday] a month
package revent

import (
	"context"
	"fmt"
	"strings"
	"time"

	"eintopf.info/internal/crud"
	"eintopf.info/internal/xerror"
	"eintopf.info/service/auth"
	"eintopf.info/service/event"
)

type NewRepeatingEvent struct {
	Name        string     `json:"name"`
	Organizers  []string   `json:"organizers"`
	Location    string     `json:"location"`
	Location2   string     `json:"location2"`
	Address     string     `json:"address"`
	Lat         float64    `json:"lat"`
	Lng         float64    `json:"lng"`
	Description string     `json:"description"`
	Intervals   []Interval `json:"intervals"`
	Start       time.Time  `json:"start"`
	End         time.Time  `json:"end"`
	Tags        []string   `json:"tags"`
	Image       string     `json:"image"`
	Alt         string     `json:"alt"`
	OwnedBy     []string   `json:"ownedBy"`
	Category    string     `json:"category"`
	Topic       string     `json:"topic"`
}

// IsOwned returns true if the id is in the OwnedBy field.
func (r *NewRepeatingEvent) IsOwned(id string) bool {
	owned := false
	for _, owner := range r.OwnedBy {
		if owner == id {
			owned = true
		}
	}
	return owned
}

func RepeatingEventFromNewRepeatingEvent(newRepeatingEvent *NewRepeatingEvent, id string) *RepeatingEvent {
	return &RepeatingEvent{
		ID:          id,
		Deactivated: false,
		Name:        newRepeatingEvent.Name,
		Organizers:  newRepeatingEvent.Organizers,
		Location:    newRepeatingEvent.Location,
		Location2:   newRepeatingEvent.Location2,
		Address:     newRepeatingEvent.Address,
		Lat:         newRepeatingEvent.Lat,
		Lng:         newRepeatingEvent.Lng,
		Description: newRepeatingEvent.Description,
		Intervals:   newRepeatingEvent.Intervals,
		Start:       newRepeatingEvent.Start,
		End:         newRepeatingEvent.End,
		Image:       newRepeatingEvent.Image,
		Alt:         newRepeatingEvent.Alt,
		Tags:        newRepeatingEvent.Tags,
		OwnedBy:     newRepeatingEvent.OwnedBy,
		Category:    newRepeatingEvent.Category,
		Topic:       newRepeatingEvent.Topic,
	}
}

type RepeatingEvent struct {
	ID          string     `json:"id" db:"id"`
	Deactivated bool       `json:"deactivated" db:"deactivated"`
	Name        string     `json:"name" db:"name"`
	Organizers  []string   `json:"organizers" db:"organizers"`
	Location    string     `json:"location" db:"location"`
	Location2   string     `json:"location2" db:"location2"`
	Address     string     `json:"address" db:"address"`
	Lat         float64    `json:"lat" db:"lat"`
	Lng         float64    `json:"lng" db:"lng"`
	Description string     `json:"description" db:"description"`
	Intervals   []Interval `json:"intervals" db:"intervals"`
	Start       time.Time  `json:"start" db:"start"`
	End         time.Time  `json:"end" db:"end"`
	Tags        []string   `json:"tags" db:"tags"`
	Image       string     `json:"image" db:"image"`
	Alt         string     `json:"alt" db:"alt"`
	OwnedBy     []string   `json:"ownedBy" db:"ownedBy"`
	Category    string     `json:"category"`
	Topic       string     `json:"topic"`
}

func (r RepeatingEvent) Identifier() string { return r.ID }

// IsOwned returns true if the id is in the OwnedBy field.
func (r *RepeatingEvent) IsOwned(id string) bool {
	owned := false
	for _, owner := range r.OwnedBy {
		if owner == id {
			owned = true
		}
	}
	return owned
}

// IntervalType defines the type of the interval (day, week or month)
type IntervalType string

const (
	IntervalDay   = IntervalType("day")
	IntervalWeek  = IntervalType("week")
	IntervalMonth = IntervalType("month")
)

type Interval struct {
	// Type changes the meaning od Interval an WeekDay.
	Type IntervalType `json:"type" db:"type"`
	// Interval used for day, week and month
	Interval int `json:"interval" db:"interval"`
	// WeekDay is a week day, with a range of 0 (sunday) to 7 (saturday).
	WeekDay time.Weekday `json:"weekDay" db:"weekDay"`
}

// GenerateDates generates a list of dates, in a given date range using the
// defined interval.
func (i *Interval) GenerateDates(start time.Time, end time.Time) ([]time.Time, error) {
	if start.After(end) {
		return nil, fmt.Errorf("start must be before end: start %s end %s", start, end)
	}
	dates := []time.Time{}

	switch i.Type {
	case IntervalDay:
		current := start
		for {
			if current.After(end) {
				return dates, nil
			}

			dates = append(dates, current)
			current = current.AddDate(0, 0, i.Interval)
		}
	case IntervalWeek:
		current := i.findWeekStart(start)
		for {
			if current.After(end) {
				return dates, nil
			}

			dates = append(dates, current)
			current = current.AddDate(0, 0, i.Interval*7)
		}
	case IntervalMonth:
		current := time.Date(start.Year(), start.Month(), 1, start.Hour(), start.Minute(), start.Second(), 0, start.Location())
		current = i.findMonthStart(current)
		for {
			if current.After(end) {
				return dates, nil
			}

			// It is possible, that the first date is before the start date.
			if current.After(start) {
				dates = append(dates, current)
			}

			current = time.Date(current.Year(), current.Month()+1, 1, current.Hour(), current.Minute(), current.Second(), 0, current.Location())
			current = i.findMonthStart(current)
		}
	default:
		return nil, fmt.Errorf("invalid interval type: %s", i.Type)
	}
}

func normalizeDate(date time.Time) time.Time {
	return time.Date(date.Year(), date.Month(), date.Day(), 0, 0, 0, 0, date.Location())
}

func (i *Interval) findWeekStart(date time.Time) time.Time {
	for {
		if i.isWeekStart(date) {
			return date
		}
		date = date.AddDate(0, 0, 1)
	}
}

func (i *Interval) isWeekStart(date time.Time) bool {
	return date.Weekday() == i.WeekDay
}

func (i *Interval) findMonthStart(date time.Time) time.Time {
	// // for {
	// // 	if date.Day() == 0 {
	// // 		return date
	// // 	}
	// // 	date = date.AddDate(0, 0, 1)
	// // }
	// s := time.Date(date.Year(), date.Month(), date.Day(), date.Hour(), date.Minute(), date.Second(), 0, date.Location())
	weekStart := i.findWeekStart(date)
	return weekStart.AddDate(0, 0, 7*(i.Interval-1))
}

// ID returns a repeating event id in the form of:
//
//	revent:<id>
func ID(id string) string {
	return fmt.Sprintf("revent:%s", id)
}

// ParseID parses a repeating event id. The second return value indicates if the
// given id is a valid repeating event id.
func ParseID(id string) (string, bool) {
	if !strings.HasPrefix(id, "revent:") {
		return "", false
	}
	return strings.TrimPrefix(id, "revent:"), true
}

type Service interface {
	Storer

	GenerateEvents(ctx context.Context, id string, start, end time.Time) ([]*event.Event, error)
}

// Storer defines a service for CRUD operations on the event model.
type Storer interface {
	Create(ctx context.Context, newEvent *NewRepeatingEvent) (*RepeatingEvent, error)
	Update(ctx context.Context, event *RepeatingEvent) (*RepeatingEvent, error)
	Delete(ctx context.Context, id string) error
	FindByID(ctx context.Context, id string) (*RepeatingEvent, error)
	Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*RepeatingEvent, int, error)
}

// SortOrder defines the order of sorting.
type SortOrder string

// Possible values for SortOrder.
const (
	OrderAsc  = SortOrder("ASC")
	OrderDesc = SortOrder("DESC")
)

// FindFilters defines the possible filters for the find method.
type FindFilters struct {
	ID          *string    `json:"id,omitempty"`
	Deactivated *bool      `json:"deactivated,omitempty"`
	Published   *bool      `json:"published,omitempty"`
	Parent      *string    `json:"parent,omitempty"`
	Name        *string    `json:"name,omitempty"`
	Organizers  []string   `json:"organizers,omitempty"`
	Location    *string    `json:"location,omitempty"`
	Location2   *string    `json:"location2,omitempty"`
	Address     *string    `json:"address,omitempty"`
	Lat         *float64   `json:"lat,omitempty"`
	Lng         *float64   `json:"lng,omitempty"`
	Description *string    `json:"description,omitempty"`
	Start       *time.Time `json:"start,omitempty"`
	End         *time.Time `json:"end,omitempty"`
	Tags        []string   `json:"tags,omitempty"`
	OwnedBy     []string   `json:"ownedBy,omitempty"`
	Category    *string    `json:"category"`
	Topic       *string    `json:"topic"`
}

// NewService returns a new event service.
func NewService(store Storer, eventService event.Storer) Service {
	return &service{store: store, eventService: eventService}
}

type service struct {
	store        Storer
	eventService event.Storer
}

func (s *service) Create(ctx context.Context, newRepeatingEvent *NewRepeatingEvent) (*RepeatingEvent, error) {
	id, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return nil, err
	}
	if !newRepeatingEvent.IsOwned(id) {
		newRepeatingEvent.OwnedBy = append(newRepeatingEvent.OwnedBy, id)
	}
	if newRepeatingEvent.Name == "" {
		return nil, xerror.BadInputError{Err: fmt.Errorf("empty name")}
	}
	if len(newRepeatingEvent.Intervals) == 0 {
		return nil, xerror.BadInputError{Err: fmt.Errorf("no intervals")}
	}
	return s.store.Create(ctx, newRepeatingEvent)
}

func (s *service) Update(ctx context.Context, repeatingEvent *RepeatingEvent) (*RepeatingEvent, error) {
	if repeatingEvent.Name == "" {
		return nil, xerror.BadInputError{Err: fmt.Errorf("empty name")}
	}
	if len(repeatingEvent.Intervals) == 0 {
		return nil, xerror.BadInputError{Err: fmt.Errorf("no intervals")}
	}
	return s.store.Update(ctx, repeatingEvent)
}

func (s *service) Delete(ctx context.Context, id string) error {
	return s.store.Delete(ctx, id)
}

func (s *service) FindByID(ctx context.Context, id string) (*RepeatingEvent, error) {
	return s.store.FindByID(ctx, id)
}

func (s *service) Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*RepeatingEvent, int, error) {
	if params != nil && params.Filters != nil && params.Filters.OwnedBy != nil {
		if len(params.Filters.OwnedBy) == 1 && params.Filters.OwnedBy[0] == "self" {
			id, err := auth.UserIDFromContext(ctx)
			if err == nil {
				params.Filters.OwnedBy = []string{id}
			}
		}
	}
	return s.store.Find(ctx, params)
}

func (s *service) GenerateEvents(ctx context.Context, id string, start, end time.Time) ([]*event.Event, error) {
	repeatingEvent, err := s.FindByID(ctx, id)
	if err != nil {
		return nil, err
	}

	parentID := ID(repeatingEvent.ID)
	existingEvents, _, err := s.eventService.Find(ctx, &crud.FindParams[event.FindFilters]{
		Filters: &event.FindFilters{
			Parent: &parentID,
		},
	})
	if err != nil {
		return nil, err
	}

	newEvents, err := GenerateEvents(repeatingEvent, existingEvents, start, end)
	if err != nil {
		return nil, err
	}

	events := []*event.Event{}
	for _, newEvent := range newEvents {
		e, err := s.eventService.Create(ctx, newEvent)
		if err != nil {
			return nil, err
		}
		events = append(events, e)
	}

	return events, nil
}

func GenerateEvents(repeatingEvent *RepeatingEvent, existingEvents []*event.Event, start, end time.Time) ([]*event.NewEvent, error) {
	if len(repeatingEvent.Intervals) == 0 {
		return nil, fmt.Errorf("repeating event has no intervals")
	}

	newEvents := []*event.NewEvent{}
	for _, interval := range repeatingEvent.Intervals {
		dates, err := interval.GenerateDates(start, end)
		if err != nil {
			return nil, xerror.BadInputError{Err: err}
		}
		for _, date := range dates {
			eventExists := false
			for _, existingEvent := range existingEvents {
				y1, m1, d1 := date.Date()
				y2, m2, d2 := existingEvent.Start.Date()
				if y1 == y2 && m1 == m2 && d1 == d2 {
					eventExists = true
					break
				}
			}
			// Skip events, that exist already, to prevent generating them twice.
			// In order to regenerate the event, the existing one needs to be
			// deleted first.
			if eventExists {
				continue
			}

			ownedBy := make([]string, len(repeatingEvent.OwnedBy))
			copy(ownedBy, repeatingEvent.OwnedBy)

			tags := make([]string, len(repeatingEvent.Tags))
			copy(tags, repeatingEvent.Tags)

			startEvent := time.Date(date.Year(), date.Month(), date.Day(), repeatingEvent.Start.Hour(), repeatingEvent.Start.Minute(), 0, 0, date.Location())
			endEvent := startEvent.Add(repeatingEvent.End.Sub(repeatingEvent.Start))

			newEvents = append(newEvents, &event.NewEvent{
				Published:    true,
				Parent:       ID(repeatingEvent.ID),
				ParentListed: false,
				Name:         repeatingEvent.Name,
				Organizers:   repeatingEvent.Organizers,
				Location:     repeatingEvent.Location,
				Location2:    repeatingEvent.Location2,
				Address:      repeatingEvent.Address,
				Lat:          repeatingEvent.Lat,
				Lng:          repeatingEvent.Lng,
				Description:  repeatingEvent.Description,
				Start:        startEvent,
				End:          &endEvent,
				Image:        repeatingEvent.Image,
				Tags:         tags,
				OwnedBy:      ownedBy,
				Category:     repeatingEvent.Category,
				Topic:        repeatingEvent.Topic,
			})
		}
	}

	return newEvents, nil
}
