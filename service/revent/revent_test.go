//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package revent_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"

	"eintopf.info/service/auth"
	"eintopf.info/service/event"
	"eintopf.info/service/revent"
	"eintopf.info/test"
)

func tptr(t time.Time) *time.Time {
	return &t
}

func TestIntervalGenerateDates(t *testing.T) {
	tests := []struct {
		name      string
		interval  *revent.Interval
		start     time.Time
		end       time.Time
		wantDates []time.Time
	}{
		{
			name: "daily",
			interval: &revent.Interval{
				Type:     revent.IntervalDay,
				Interval: 1,
			},
			start: time.Date(2021, 8, 10, 16, 0, 0, 0, time.UTC),
			end:   time.Date(2021, 8, 15, 16, 0, 0, 0, time.UTC),
			wantDates: []time.Time{
				time.Date(2021, 8, 10, 16, 0, 0, 0, time.UTC),
				time.Date(2021, 8, 11, 16, 0, 0, 0, time.UTC),
				time.Date(2021, 8, 12, 16, 0, 0, 0, time.UTC),
				time.Date(2021, 8, 13, 16, 0, 0, 0, time.UTC),
				time.Date(2021, 8, 14, 16, 0, 0, 0, time.UTC),
				time.Date(2021, 8, 15, 16, 0, 0, 0, time.UTC),
			},
		},
		{
			name: "every second day",
			interval: &revent.Interval{
				Type:     revent.IntervalDay,
				Interval: 2,
			},
			start: time.Date(2021, 8, 10, 16, 0, 0, 0, time.UTC),
			end:   time.Date(2021, 8, 15, 16, 0, 0, 0, time.UTC),
			wantDates: []time.Time{
				time.Date(2021, 8, 10, 16, 0, 0, 0, time.UTC),
				time.Date(2021, 8, 12, 16, 0, 0, 0, time.UTC),
				time.Date(2021, 8, 14, 16, 0, 0, 0, time.UTC),
			},
		},
		{
			name: "every monday",
			interval: &revent.Interval{
				Type:     revent.IntervalWeek,
				Interval: 1,
				WeekDay:  time.Monday,
			},
			start: time.Date(2021, 8, 1, 16, 0, 0, 0, time.UTC),
			end:   time.Date(2021, 9, 1, 16, 0, 0, 0, time.UTC),
			wantDates: []time.Time{
				time.Date(2021, 8, 2, 16, 0, 0, 0, time.UTC),
				time.Date(2021, 8, 9, 16, 0, 0, 0, time.UTC),
				time.Date(2021, 8, 16, 16, 0, 0, 0, time.UTC),
				time.Date(2021, 8, 23, 16, 0, 0, 0, time.UTC),
				time.Date(2021, 8, 30, 16, 0, 0, 0, time.UTC),
			},
		},
		{
			name: "every second thursday",
			interval: &revent.Interval{
				Type:     revent.IntervalWeek,
				Interval: 2,
				WeekDay:  time.Thursday,
			},
			start: time.Date(2021, 8, 1, 16, 0, 0, 0, time.UTC),
			end:   time.Date(2021, 9, 1, 16, 0, 0, 0, time.UTC),
			wantDates: []time.Time{
				time.Date(2021, 8, 5, 16, 0, 0, 0, time.UTC),
				time.Date(2021, 8, 19, 16, 0, 0, 0, time.UTC),
			},
		},
		{
			name: "every first friday per month",
			interval: &revent.Interval{
				Type:     revent.IntervalMonth,
				Interval: 1,
				WeekDay:  time.Friday,
			},
			start: time.Date(2021, 8, 1, 18, 30, 0, 0, time.UTC),
			end:   time.Date(2021, 11, 1, 18, 30, 0, 0, time.UTC),
			wantDates: []time.Time{
				time.Date(2021, 8, 6, 18, 30, 0, 0, time.UTC),
				time.Date(2021, 9, 3, 18, 30, 0, 0, time.UTC),
				time.Date(2021, 10, 1, 18, 30, 0, 0, time.UTC),
			},
		},
		{
			name: "every second sunday per month",
			interval: &revent.Interval{
				Type:     revent.IntervalMonth,
				Interval: 2,
				WeekDay:  time.Sunday,
			},
			start: time.Date(2021, 7, 20, 18, 30, 0, 0, time.UTC),
			end:   time.Date(2021, 11, 1, 18, 30, 0, 0, time.UTC),
			wantDates: []time.Time{
				time.Date(2021, 8, 8, 18, 30, 0, 0, time.UTC),
				time.Date(2021, 9, 12, 18, 30, 0, 0, time.UTC),
				time.Date(2021, 10, 10, 18, 30, 0, 0, time.UTC),
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(tt *testing.T) {
			dates, err := test.interval.GenerateDates(test.start, test.end)
			if err != nil {
				tt.Error(err)
			}
			if diff := cmp.Diff(test.wantDates, dates); diff != "" {
				tt.Errorf("dates mismatch (-want +got):\n%s", diff)
			}
		})
	}
}

func TestServiceGenerateEvents(t *testing.T) {
	tests := []struct {
		name              string
		newRepeatingEvent *revent.NewRepeatingEvent
		wantErr           error
		wantFirstEvent    *event.Event
		wantEventsCount   int
	}{
		{
			name: "it works",
			newRepeatingEvent: &revent.NewRepeatingEvent{
				Name:        "foo",
				Organizers:  []string{"a", "b"},
				Location:    "c",
				Location2:   "d",
				Description: "e",
				Intervals: []revent.Interval{
					{
						Type:     revent.IntervalDay,
						Interval: 1,
					},
				},
				Start:   time.Date(2021, 8, 1, 16, 0, 0, 0, time.UTC),
				End:     time.Date(2021, 8, 1, 18, 0, 0, 0, time.UTC),
				Tags:    []string{"x", "y", "z"},
				Image:   "i",
				OwnedBy: []string{"user1", "user2"},
			},
			wantEventsCount: 31,
			wantFirstEvent: &event.Event{
				Published:   true,
				Name:        "foo",
				Organizers:  []string{"a", "b"},
				Location:    "c",
				Location2:   "d",
				Description: "e",
				Start:       time.Date(2021, 8, 1, 16, 0, 0, 0, time.UTC),
				End:         tptr(time.Date(2021, 8, 1, 18, 0, 0, 0, time.UTC)),
				Tags:        []string{"x", "y", "z"},
				Image:       "i",
				OwnedBy:     []string{"user1", "user2", "1"},
			},
		},
	}

	for _, testCase := range tests {
		t.Run(testCase.name, func(tt *testing.T) {
			reventStore := revent.NewMemoryStore()
			eventStore := event.NewMemoryStore()

			service := revent.NewService(reventStore, event.NewService(eventStore, nil))

			ctx := auth.ContextWithID(context.Background(), "1")

			repeatingEvent, err := service.Create(ctx, testCase.newRepeatingEvent)
			if err != nil {
				tt.Fatal(err)
			}

			events, err := service.GenerateEvents(ctx, repeatingEvent.ID,
				time.Date(2021, 8, 1, 0, 0, 0, 0, time.UTC),
				time.Date(2021, 8, 31, 0, 0, 0, 0, time.UTC),
			)
			if !test.EqualError(testCase.wantErr, err) {
				tt.Errorf("err: want != got: %s, %s", testCase.wantErr, err)
			}
			if len(events) != testCase.wantEventsCount {
				tt.Errorf("events len: want != got: %d, %d", testCase.wantEventsCount, len(events))
			}
			if len(events) > 0 && testCase.wantFirstEvent != nil {
				testCase.wantFirstEvent.Parent = revent.ID(repeatingEvent.ID)
				if diff := cmp.Diff(testCase.wantFirstEvent, events[0], cmpopts.IgnoreFields(event.Event{}, "ID")); diff != "" {
					tt.Errorf("first event mismatch mismatch (-want +got):\n%s", diff)
				}
			}
		})
	}
}

func TestGenerateEvents(t *testing.T) {
	tests := []struct {
		name               string
		repeatingEvent     *revent.RepeatingEvent
		existingEvents     []*event.Event
		wantErr            error
		wantFirstNewEvent  *event.NewEvent
		wantNewEventsCount int
	}{
		{
			name: "no intervals error",
			repeatingEvent: &revent.RepeatingEvent{
				ID:          "1",
				Name:        "foo",
				Organizers:  []string{"a"},
				Location:    "c",
				Location2:   "d",
				Description: "e",
				Intervals:   []revent.Interval{},
				Start:       time.Date(2021, 8, 1, 16, 0, 0, 0, time.UTC),
				End:         time.Date(2021, 8, 1, 18, 0, 0, 0, time.UTC),
				Tags:        []string{"x", "y", "z"},
				Image:       "i",
				OwnedBy:     []string{"user1", "user2"},
			},
			wantErr: fmt.Errorf("repeating event has no intervals"),
		},
		{
			name: "generate with IntervalDay",
			repeatingEvent: &revent.RepeatingEvent{
				ID:          "2",
				Name:        "foo",
				Organizers:  []string{"a"},
				Location:    "c",
				Location2:   "d",
				Description: "e",
				Intervals: []revent.Interval{
					{
						Type:     revent.IntervalDay,
						Interval: 1,
					},
				},
				Start:   time.Date(2021, 8, 1, 16, 0, 0, 0, time.UTC),
				End:     time.Date(2021, 8, 1, 18, 0, 0, 0, time.UTC),
				Tags:    []string{"x", "y", "z"},
				Image:   "i",
				OwnedBy: []string{"user1", "user2"},
			},
			wantNewEventsCount: 31,
			wantFirstNewEvent: &event.NewEvent{
				Published:   true,
				Name:        "foo",
				Organizers:  []string{"a"},
				Location:    "c",
				Location2:   "d",
				Description: "e",
				Start:       time.Date(2021, 8, 1, 16, 0, 0, 0, time.UTC),
				End:         tptr(time.Date(2021, 8, 1, 18, 0, 0, 0, time.UTC)),
				Tags:        []string{"x", "y", "z"},
				Image:       "i",
				OwnedBy:     []string{"user1", "user2"},
			},
		},
		{
			name: "has existingEvents",
			repeatingEvent: &revent.RepeatingEvent{
				ID:          "2",
				Name:        "foo",
				Organizers:  []string{"a", "b"},
				Location:    "c",
				Location2:   "d",
				Description: "e",
				Intervals: []revent.Interval{
					{
						Type:     revent.IntervalDay,
						Interval: 1,
					},
				},
				Start:   time.Date(2021, 8, 1, 16, 0, 0, 0, time.UTC),
				End:     time.Date(2021, 8, 1, 18, 0, 0, 0, time.UTC),
				Tags:    []string{"x", "y", "z"},
				Image:   "i",
				OwnedBy: []string{"user1", "user2"},
			},
			existingEvents: []*event.Event{
				{
					Published:   true,
					Name:        "foo",
					Organizers:  []string{"a", "b"},
					Location:    "c",
					Location2:   "d",
					Description: "e",
					Start:       time.Date(2021, 8, 1, 16, 0, 0, 0, time.UTC),
					End:         tptr(time.Date(2021, 8, 1, 18, 0, 0, 0, time.UTC)),
					Tags:        []string{"x", "y", "z"},
					Image:       "i",
					OwnedBy:     []string{"user1", "user2"},
				},
			},
			wantNewEventsCount: 30,
			wantFirstNewEvent: &event.NewEvent{
				Published:   true,
				Name:        "foo",
				Organizers:  []string{"a", "b"},
				Location:    "c",
				Location2:   "d",
				Description: "e",
				Start:       time.Date(2021, 8, 2, 16, 0, 0, 0, time.UTC),
				End:         tptr(time.Date(2021, 8, 2, 18, 0, 0, 0, time.UTC)),
				Tags:        []string{"x", "y", "z"},
				Image:       "i",
				OwnedBy:     []string{"user1", "user2"},
			},
		},
	}

	for _, testCase := range tests {
		t.Run(testCase.name, func(tt *testing.T) {
			events, err := revent.GenerateEvents(
				testCase.repeatingEvent,
				testCase.existingEvents,
				time.Date(2021, 8, 1, 0, 0, 0, 0, time.UTC),
				time.Date(2021, 8, 31, 0, 0, 0, 0, time.UTC),
			)
			if !test.EqualError(testCase.wantErr, err) {
				tt.Errorf("err: want != got: %s, %s", testCase.wantErr, err)
			}
			if len(events) != testCase.wantNewEventsCount {
				tt.Errorf("new events len: want != got: %d, %d", testCase.wantNewEventsCount, len(events))
			}
			if len(events) > 0 && testCase.wantFirstNewEvent != nil {
				testCase.wantFirstNewEvent.Parent = revent.ID(testCase.repeatingEvent.ID)
				if diff := cmp.Diff(testCase.wantFirstNewEvent, events[0], cmpopts.IgnoreFields(event.Event{}, "ID")); diff != "" {
					tt.Errorf("first event mismatch mismatch (-want +got):\n%s", diff)
				}
			}
		})
	}
}
