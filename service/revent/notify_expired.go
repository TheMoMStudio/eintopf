// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package revent

import (
	"context"
	"fmt"
	"time"

	"eintopf.info/internal/crud"
	"eintopf.info/service/dbmigration"
	"eintopf.info/service/event"
	"eintopf.info/service/notification"
	"eintopf.info/service/oqueue"
	"github.com/jmoiron/sqlx"
)

type ExpiredState struct {
	RepeatingEventID string `db:"repeating_event_id"`
}

func (e ExpiredState) Identifier() string { return e.RepeatingEventID }

type ExpiredStateFilter struct{}

type ExpiredStorer = crud.Storer[ExpiredState, ExpiredState, ExpiredStateFilter]

func newExpiredState(state *ExpiredState, id string) *ExpiredState { return state }

func NewExpiredMemoryStore() *crud.MemoryStore[ExpiredState, ExpiredState, ExpiredStateFilter] {
	return crud.NewMemoryStore[ExpiredState, ExpiredState, ExpiredStateFilter](newExpiredState, nil)
}

func NewExpiredStateSqlStore(db *sqlx.DB, migrationService dbmigration.Service) (*ExpiredStateSqlStore, error) {
	store := &ExpiredStateSqlStore{
		db,
		migrationService,
		crud.NewSqlStore(db, table, newExpiredState, nil),
	}
	if err := store.runMigrations(context.Background()); err != nil {
		return nil, err
	}
	return store, nil
}

type ExpiredStateSqlStore struct {
	db               *sqlx.DB
	migrationService dbmigration.Service

	*crud.SqlStore[ExpiredState, ExpiredState, ExpiredStateFilter]
}

func (s *ExpiredStateSqlStore) runMigrations(ctx context.Context) error {
	return s.migrationService.RunMigrations(ctx, []dbmigration.Migration{
		dbmigration.NewMigration("createActionTable3", s.createActionTable, nil),
	})
}

func (s *ExpiredStateSqlStore) createActionTable(ctx context.Context) error {
	_, err := s.db.ExecContext(ctx, `
        CREATE TABLE IF NOT EXISTS repeating_event_expired_notification (
            repeating_event_id VARCHAR(64) NOT NULL PRIMARY KEY UNIQUE
        );
    `)
	return err
}

var table = crud.SqlTable[ExpiredStateFilter]{
	IDField: "repeating_event_id",
	Table:   "repeating_event_expired_notification",
	Fields:  []string{"repeating_event_id"},
}

type ExpiredNotifier struct {
	expiredStore ExpiredStorer
	reventStore  Storer
	eventStore   event.Storer
	notifier     notification.Notifier
}

func NewExpiredNotifier(expiredStore ExpiredStorer, reventStore Storer, eventStore event.Storer, notifier notification.Notifier) *ExpiredNotifier {
	return &ExpiredNotifier{
		expiredStore: expiredStore,
		reventStore:  reventStore,
		eventStore:   eventStore,
		notifier:     notifier,
	}
}

func (e *ExpiredNotifier) ConsumeOperation(op oqueue.Operation) error {
	switch op := op.(type) {
	case DeleteOperation:
		_ = e.expiredStore.Delete(context.Background(), op.ID)
	case event.CreateOperation:
		repeatingEventID, ok := ParseID(op.Event.Parent)
		if !ok {
			return nil
		}
		_ = e.expiredStore.Delete(context.Background(), repeatingEventID)
	}
	return nil
}

func (e *ExpiredNotifier) NotifyAction(ctx context.Context) error {
	deactivatedFilter := false
	revents, _, err := e.reventStore.Find(ctx, &crud.FindParams[FindFilters]{
		Filters: &FindFilters{
			Deactivated: &deactivatedFilter,
		},
	})
	if err != nil {
		return err
	}

	for _, repeatingEvent := range revents {
		publishedFilter := true
		parentFilter := ID(repeatingEvent.ID)
		events, _, err := e.eventStore.Find(ctx, &crud.FindParams[event.FindFilters]{
			Sort:  "start",
			Order: crud.OrderDesc,
			Limit: 1,
			Filters: &event.FindFilters{
				Deactivated: &deactivatedFilter,
				Published:   &publishedFilter,
				Parent:      &parentFilter,
			},
		})
		if err != nil {
			return err
		}
		if len(events) != 1 {
			// No events were generated for this repeating event
			continue
		}

		// Check if the last event is less than one day from now.
		if events[0].Start.After(time.Now().AddDate(0, 0, 1)) {
			continue
		}

		state, err := e.expiredStore.FindByID(ctx, repeatingEvent.ID)
		if err != nil {
			return err
		}
		if state != nil {
			// The notification was already send for this repeating event.
			continue
		}

		for _, userID := range repeatingEvent.OwnedBy {
			// TODO: Check if user wants this notification
			err := e.notifier.Notify(
				ctx,
				userID,
				"Regelmäßige Veranstaltung abgelaufen",
				fmt.Sprintf("Für die regelmäßige Veranstaltung '%s' wurden keine neuen Veranstaltungen mehr erstellt. Die letzte Veranstaltung war am %s", repeatingEvent.Name, events[0].Start.Format("02.01.2006")),
				notification.Link{Title: "Zur Regelmäßigen Veranstaltung", URL: fmt.Sprintf("/backstage/repeatingevents/%s#generate", repeatingEvent.ID)},
			)
			if err != nil {
				return err
			}
		}

		_, err = e.expiredStore.Create(ctx, &ExpiredState{RepeatingEventID: repeatingEvent.ID})
		if err != nil {
			return err
		}
	}

	return nil
}
