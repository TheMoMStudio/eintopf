// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package revent_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"eintopf.info/service/event"
	"eintopf.info/service/notification"
	"eintopf.info/service/revent"
	"eintopf.info/test"
	"github.com/google/go-cmp/cmp"
)

func TestNotifyAction(t *testing.T) {
	now := time.Now()
	tests := []struct {
		name          string
		events        []event.NewEvent
		revents       []revent.NewRepeatingEvent
		notifications []test.Notification
	}{
		{
			name: "no notification",
			revents: []revent.NewRepeatingEvent{
				{
					Name: "MyRepeatingEvent",
					Intervals: []revent.Interval{
						{
							Type:     revent.IntervalDay,
							Interval: 1,
						},
					},
				},
			},
			events: []event.NewEvent{
				{
					Name:   "MyRepeatingEvent",
					Parent: "revent:0",
					Start:  time.Now().AddDate(0, 0, 20),
				},
			},
			notifications: []test.Notification{},
		},
		{
			name: "notification",
			revents: []revent.NewRepeatingEvent{
				{
					Name: "MyRepeatingEvent",
					Intervals: []revent.Interval{
						{
							Type:     revent.IntervalDay,
							Interval: 1,
						},
					},
					OwnedBy: []string{"007"},
				},
			},
			events: []event.NewEvent{
				{
					Published: true,
					Name:      "MyRepeatingEvent",
					Parent:    "revent:0",
					Start:     now.AddDate(0, 0, -14),
				},
			},
			notifications: []test.Notification{
				{
					UserID:  "007",
					Subject: "Regelmäßige Veranstaltung abgelaufen",
					Message: fmt.Sprintf("Für die regelmäßige Veranstaltung 'MyRepeatingEvent' wurden keine neuen Veranstaltungen mehr erstellt. Die letzte Veranstaltung war am %s", now.AddDate(0, 0, -14).Format("02.01.2006")),
					Link:    notification.Link{Title: "Zur Regelmäßigen Veranstaltung", URL: "/backstage/repeatingevents/0#generate"},
				},
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			expiredStore := revent.NewExpiredMemoryStore()
			reventStore := revent.NewMemoryStore()
			for _, e := range tc.revents {
				if _, err := reventStore.Create(context.Background(), &e); err != nil {
					t.Fatalf("create event: %s", err)
				}
			}
			eventStore := event.NewMemoryStore()
			for _, e := range tc.events {
				if _, err := eventStore.Create(context.Background(), &e); err != nil {
					t.Fatalf("create event: %s", err)
				}
			}
			notifier := test.NewNotifier()
			expiredNotifier := revent.NewExpiredNotifier(expiredStore, reventStore, eventStore, notifier)

			err := expiredNotifier.NotifyAction(context.Background())
			if err != nil {
				t.Error(err)
			}
			// Run NotifyAction twice to check that it is idempotent
			err = expiredNotifier.NotifyAction(context.Background())
			if err != nil {
				t.Error(err)
			}

			if diff := cmp.Diff(tc.notifications, notifier.Notifications); diff != "" {
				t.Errorf("notifications mismatch (-want +got):\n%s", diff)
			}
		})
	}
}
