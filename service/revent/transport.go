//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package revent

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/crud"
	"eintopf.info/internal/xerror"
	"eintopf.info/internal/xhttp"
	"eintopf.info/service/auth"
	"eintopf.info/service/event"
)

// Router returns a new http router that handles crud RepeatingEvent requests for a given
// RepeatingEvent service.
func Router(service Service, authService auth.Service) func(chi.Router) {
	router := &router{service}
	return func(r chi.Router) {
		r.Options("/", xhttp.CorsHandler)

		// swagger:route GET /revents/ RepeatingEvents findRepeatingEvents
		//
		// Retrieves all RepeatingEvents.
		//
		// Parameters:
		//   + name: offset
		//     description: offset in event list
		//     in: query
		//     type: integer
		//     required: false
		//   + name: limit
		//     description: limits the event list
		//     in: query
		//     type: integer
		//     required: false
		//   + name: sort
		//     description: field that gets sorted
		//     in: query
		//     type: string
		//     required: false
		//   + name: order
		//     description: sort order ("ASC" or "DESC")
		//     in: query
		//     type: string
		//     required: false
		//   + name: filters
		//     description: filters get combined with AND logic
		//     in: query
		//     type: object
		//     required: false
		//
		//     Responses:
		//       200: findRepeatingEventsResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: false})).
			Get("/", router.find)

		// swagger:route POST /revents/ RepeatingEvent createRepeatingEvent
		//
		// Creates a new RepeatingEvent with the given data.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: createRepeatingEventsResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Post("/", router.create)

		r.Options("/{id}", xhttp.CorsHandler)

		// swagger:route GET /revents/{id} RepeatingEvent findRepeatingEvent
		//
		// Finds the RepeatingEvent with the given id.
		//
		//     Responses:
		//       200: findRepeatingEventResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.Get("/{id}", router.findByID)

		// swagger:route PUT /revents/{id} RepeatingEvent updateRepeatingEvent
		//
		// Updates the RepeatingEvent with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: updateRepeatingEventResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Put("/{id}", router.update)

		// swagger:route DELETE /revents/{id} RepeatingEvent deleteRepeatingEvent
		//
		// Deltes the RepeatingEvent with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: deleteRepeatingEventResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Delete("/{id}", router.delete)

		r.Options("/{id}/generate", xhttp.CorsHandler)

		// swagger:route GET /revents/{id}/generate RepeatingEvent generateRepeatingEvent
		//
		// Generates Events from the repeating event
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: generateRepeatingEventResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: true})).
			Get("/{id}/generate", router.generate)
	}
}

type router struct {
	s Service
}

func (router *router) find(w http.ResponseWriter, r *http.Request) {
	params, err := readFindRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, fmt.Errorf("failed to read findAll request: %s", err))
		return
	}
	RepeatingEvents, totalRepeatingEvents, err := router.s.Find(r.Context(), params)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeFindResponse(w, &findResponse{RepeatingEvents: RepeatingEvents, TotalRepeatingEvents: totalRepeatingEvents})
	if err != nil {
		log.Println(fmt.Errorf("failed to write findAll response: %s", err))
		return
	}
}

// swagger:response findRepeatingEventsResponse
type findResponse struct {

	// in:body
	RepeatingEvents []*RepeatingEvent

	// in:header
	TotalRepeatingEvents int `json:"x-total-count"`
}

func readFindRequest(r *http.Request) (*crud.FindParams[FindFilters], error) {
	params := &crud.FindParams[FindFilters]{}
	var err error

	params.Offset, err = xhttp.ReadQueryInt64(r, "offset")
	if err != nil {
		return nil, err
	}
	params.Limit, err = xhttp.ReadQueryInt64(r, "limit")
	if err != nil {
		return nil, err
	}

	params.Sort, err = xhttp.ReadQueryString(r, "sorting")
	if err != nil {
		return nil, err
	}
	order, err := xhttp.ReadQueryString(r, "order")
	if err != nil {
		return nil, err
	}
	params.Order = crud.SortOrder(order)

	filters := &FindFilters{}
	err = xhttp.ReadQueryJson(r, "filters", filters)
	if err != nil {
		return nil, err
	}
	params.Filters = filters

	return params, nil
}

func writeFindResponse(w http.ResponseWriter, resp *findResponse) error {
	data, err := json.Marshal(resp.RepeatingEvents)
	if err != nil {
		return err
	}
	w.Header().Add("Access-Control-Expose-Headers", "X-Total-Count")
	w.Header().Add("X-Total-Count", strconv.Itoa(resp.TotalRepeatingEvents))
	w.Write(data)
	return nil
}

func (router *router) create(w http.ResponseWriter, r *http.Request) {
	req, err := readCreateRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, fmt.Errorf("failed to read create request: %s", err))
		return
	}
	RepeatingEvent, err := router.s.Create(r.Context(), req.RepeatingEvent)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeCreateResponse(w, &createResponse{RepeatingEvent})
	if err != nil {
		log.Println(fmt.Errorf("failed to write create response: %s", err))
		return
	}
}

// swagger:parameters createRepeatingEvent
type createRequest struct {

	// in:body
	RepeatingEvent *NewRepeatingEvent
}

// swagger:response createRepeatingEventsResponse
type createResponse struct {

	// in:body
	RepeatingEvent *RepeatingEvent
}

func readCreateRequest(r *http.Request) (*createRequest, error) {
	data, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	RepeatingEvent, err := decodeNewRepeatingEvent(data)
	if err != nil {
		return nil, err
	}
	return &createRequest{RepeatingEvent}, nil
}

func writeCreateResponse(w http.ResponseWriter, resp *createResponse) error {
	data, err := encodeRepeatingEvent(resp.RepeatingEvent)
	if err != nil {
		return err
	}
	w.Write(data)
	return nil
}

func (router *router) findByID(w http.ResponseWriter, r *http.Request) {
	req, err := readFindByIDRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, fmt.Errorf("failed to read find request: %s", err))
		return
	}
	repeatingEvent, err := router.s.FindByID(r.Context(), req.ID)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeFindByIDResponse(w, &findByIDResponse{repeatingEvent})
	if err != nil {
		log.Println(fmt.Errorf("failed to write find response: %s", err))
		return
	}
}

// swagger:parameters findRepeatingEvent
type findByIDRequest struct {
	// in:query
	ID string `json:"id"`
}

// swagger:response findRepeatingEventResponse
type findByIDResponse struct {

	// in:body
	RepeatingEvent *RepeatingEvent
}

func readFindByIDRequest(r *http.Request) (*findByIDRequest, error) {
	id := chi.URLParam(r, "id")
	return &findByIDRequest{id}, nil
}

func writeFindByIDResponse(w http.ResponseWriter, resp *findByIDResponse) error {
	data, err := encodeRepeatingEvent(resp.RepeatingEvent)
	if err != nil {
		return err
	}
	w.Write(data)
	return nil
}

func (router *router) update(w http.ResponseWriter, r *http.Request) {
	req, err := readUpdateRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, fmt.Errorf("failed to read update request: %s", err))
		return
	}
	RepeatingEvent, err := router.s.Update(r.Context(), req.RepeatingEvent)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeUpdateResponse(w, &updateResponse{RepeatingEvent})
	if err != nil {
		log.Println(fmt.Errorf("failed to write update response: %s", err))
		return
	}
}

// swagger:parameters updateRepeatingEvent
type updateRequest struct {

	// in:body
	RepeatingEvent *RepeatingEvent
}

// swagger:response updateRepeatingEventResponse
type updateResponse struct {

	// in:body
	RepeatingEvent *RepeatingEvent
}

func readUpdateRequest(r *http.Request) (*updateRequest, error) {
	data, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	RepeatingEvent, err := decodeRepeatingEvent(data)
	if err != nil {
		return nil, err
	}
	return &updateRequest{RepeatingEvent}, nil
}

func writeUpdateResponse(w http.ResponseWriter, resp *updateResponse) error {
	data, err := encodeRepeatingEvent(resp.RepeatingEvent)
	if err != nil {
		return err
	}
	w.Write(data)
	return nil
}

func (router *router) delete(w http.ResponseWriter, r *http.Request) {
	req, err := readDeleteRequest(r)
	if err != nil {

		xhttp.WriteBadRequest(r.Context(), w, fmt.Errorf("failed to read delete request: %s", err))
		return
	}
	err = router.s.Delete(r.Context(), req.ID)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeDeleteResponse(w)
	if err != nil {
		log.Println(fmt.Errorf("failed to write delete response: %s", err))
		return
	}
}

// swagger:parameters deleteRepeatingEvent
type deleteRequest struct {

	// in:query
	ID string `json:"id"`
}

// swagger:response deleteRepeatingEventResponse
type deleteResponse struct{}

func readDeleteRequest(r *http.Request) (*deleteRequest, error) {
	id := chi.URLParam(r, "id")
	return &deleteRequest{id}, nil
}

func writeDeleteResponse(w http.ResponseWriter) error {
	w.Write([]byte{})
	return nil
}

func (router *router) generate(w http.ResponseWriter, r *http.Request) {
	req, err := readGenerateRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, fmt.Errorf("failed to generate events: %s", err))
		return
	}
	events, err := router.s.GenerateEvents(r.Context(), req.ID, req.Start, req.End)
	if xerror.IsBadInputError(err) {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, fmt.Errorf("failed to generate events: %s", err))
		return
	}
	err = writeGenerateResponse(w, &generateResponse{events})
	if err != nil {
		log.Println(fmt.Errorf("failed to write find response: %s", err))
		return
	}
}

// swagger:parameters generateRepeatingEvent
type generateRequest struct {
	// in:query
	ID string `json:"id"`

	// in:query
	Start time.Time `json:"start"`

	// in:query
	End time.Time `json:"end"`
}

// swagger:response generateRepeatingEventResponse
type generateResponse struct {

	// in:body
	Events []*event.Event
}

func readGenerateRequest(r *http.Request) (*generateRequest, error) {
	id := chi.URLParam(r, "id")
	startRaw := r.URL.Query().Get("start")
	start, err := time.Parse(time.RFC3339, startRaw)
	if err != nil {
		return nil, fmt.Errorf("parse start time: %s", err)
	}
	endRaw := r.URL.Query().Get("end")
	end, err := time.Parse(time.RFC3339, endRaw)
	if err != nil {
		return nil, fmt.Errorf("parse end time: %s", err)
	}
	return &generateRequest{ID: id, Start: start, End: end}, nil
}

func writeGenerateResponse(w http.ResponseWriter, resp *generateResponse) error {
	data, err := json.Marshal(resp.Events)
	if err != nil {
		return err
	}
	w.Write(data)
	return nil
}

func decodeRepeatingEvent(data []byte) (*RepeatingEvent, error) {
	RepeatingEvent := &RepeatingEvent{}
	err := json.Unmarshal(data, RepeatingEvent)
	return RepeatingEvent, err
}

func encodeRepeatingEvent(RepeatingEvent *RepeatingEvent) ([]byte, error) {
	return json.Marshal(RepeatingEvent)
}

func encodeRepeatingEvents(RepeatingEvents []RepeatingEvent) ([]byte, error) {
	return json.Marshal(RepeatingEvents)
}

func decodeNewRepeatingEvent(data []byte) (*NewRepeatingEvent, error) {
	RepeatingEvent := &NewRepeatingEvent{}
	err := json.Unmarshal(data, RepeatingEvent)
	return RepeatingEvent, err
}
