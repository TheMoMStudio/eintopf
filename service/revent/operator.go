//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package revent

import (
	"context"
	"time"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
	"eintopf.info/service/event"
	"eintopf.info/service/oqueue"
)

type operator struct {
	service Service
	queue   oqueue.Service
}

func NewOperator(service Service, queue oqueue.Service) Service {
	return &operator{service: service, queue: queue}
}

type CreateOperation struct {
	RepeatingEvent *RepeatingEvent
	userID         string
}

func NewCreateOperation(repeatingEvent *RepeatingEvent, userID string) CreateOperation {
	return CreateOperation{repeatingEvent, userID}
}

func (o CreateOperation) UserID() string {
	return o.userID
}

func (i *operator) Create(ctx context.Context, newRepeatingEvent *NewRepeatingEvent) (*RepeatingEvent, error) {
	repeatingEvent, err := i.service.Create(ctx, newRepeatingEvent)
	if err != nil {
		return repeatingEvent, err
	}
	if repeatingEvent == nil {
		return nil, nil
	}

	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return nil, err
	}
	i.queue.AddOperation(CreateOperation{
		RepeatingEvent: repeatingEvent,
		userID:         userID,
	})

	return repeatingEvent, nil
}

type UpdateOperation struct {
	RepeatingEvent *RepeatingEvent
	userID         string
}

func NewUpdateOperation(repeatingEvent *RepeatingEvent, userID string) UpdateOperation {
	return UpdateOperation{repeatingEvent, userID}
}

func (o UpdateOperation) UserID() string {
	return o.userID
}

func (i *operator) Update(ctx context.Context, repeatingEvent *RepeatingEvent) (*RepeatingEvent, error) {
	repeatingEvent, err := i.service.Update(ctx, repeatingEvent)
	if err != nil {
		return repeatingEvent, err
	}
	if repeatingEvent == nil {
		return nil, nil
	}

	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return nil, err
	}
	i.queue.AddOperation(UpdateOperation{
		RepeatingEvent: repeatingEvent,
		userID:         userID,
	})

	return repeatingEvent, nil
}

type DeleteOperation struct {
	ID     string
	userID string
}

func NewDeleteOperation(id string, userID string) DeleteOperation {
	return DeleteOperation{id, userID}
}

func (o DeleteOperation) UserID() string {
	return o.userID
}

func (i *operator) Delete(ctx context.Context, id string) error {
	err := i.service.Delete(ctx, id)
	if err != nil {
		return err
	}

	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return err
	}
	i.queue.AddOperation(DeleteOperation{
		ID:     id,
		userID: userID,
	})

	return nil
}

func (i *operator) FindByID(ctx context.Context, id string) (*RepeatingEvent, error) {
	return i.service.FindByID(ctx, id)
}

func (i *operator) Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*RepeatingEvent, int, error) {
	return i.service.Find(ctx, params)
}

func (o *operator) GenerateEvents(ctx context.Context, id string, start, end time.Time) ([]*event.Event, error) {
	return o.service.GenerateEvents(ctx, id, start, end)
}
