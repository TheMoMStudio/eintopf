//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package revent

import "eintopf.info/internal/crud"

// NewMemoryStore returns a new in memory store.
func NewMemoryStore() *crud.MemoryStore[NewRepeatingEvent, RepeatingEvent, FindFilters] {
	return crud.NewMemoryStore[NewRepeatingEvent, RepeatingEvent, FindFilters](RepeatingEventFromNewRepeatingEvent, nil)
}
