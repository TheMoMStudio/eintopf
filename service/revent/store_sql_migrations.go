//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package revent

import (
	"context"

	"eintopf.info/internal/crud"
	"eintopf.info/service/dbmigration"
)

func (s *SqlStore) runMigrations(ctx context.Context, migrationService dbmigration.Service) error {
	return migrationService.RunMigrations(ctx, []dbmigration.Migration{
		dbmigration.NewMigration("createRepeatingEventsTable", s.createRepeatingEventsTable, nil),
		dbmigration.NewMigration("createRepeatingEventsOrganizerTable", s.createRepeatingEventsOrganizerTable, nil),
		dbmigration.NewMigration("createRepeatingEventsIntervalsTable", s.createRepeatingEventsIntervalsTable, nil),
		dbmigration.NewMigration("createRepeatingEventsTagsTable", s.createRepeatingEventsTagsTable, nil),
		dbmigration.NewMigration("createRepeatingEventsOwnedByTable", s.createRepeatingEventsOwnedByTable, nil),
		dbmigration.NewMigration("removeDateColumnFromIntervalsTable", s.removeDateColumnFromIntervalsTable, nil),
		dbmigration.NewMigration("addRepeatingEventCoordinates", s.addRepeatingEventCoordinates, nil),
		dbmigration.NewMigration("addRepeatingEventAddress", s.addRepeatingEventAddress, nil),
		dbmigration.NewMigration("fixRepeatingEventAddress", s.fixRepeatingEventAddress, nil),
		dbmigration.NewMigration("removeEmptyTags", s.removeEmptyTags, nil),
		dbmigration.NewMigration("addRepeatingEventCategoryAndTopic", s.addRepeatingEventCategoryAndTopic, nil),
		dbmigration.NewMigration("addReventAlt", s.addReventAlt, nil),
	})
}

func (s *SqlStore) addRepeatingEventCategoryAndTopic(ctx context.Context) error {
	_, err := s.db.ExecContext(ctx, `
		ALTER TABLE repeatingEvents ADD COLUMN category VARCHAR(64);
		ALTER TABLE repeatingEvents ADD COLUMN topic VARCHAR(64);
		UPDATE repeatingEvents SET category='', topic='';
    `)
	return err
}

func (s *SqlStore) addRepeatingEventAddress(ctx context.Context) error {
	_, err := s.db.ExecContext(ctx, `
		ALTER TABLE repeatingEvents ADD COLUMN address varchar(64);
    `)
	return err
}

func (s *SqlStore) fixRepeatingEventAddress(ctx context.Context) error {
	_, err := s.db.ExecContext(ctx, `
		UPDATE repeatingEvents SET address='' WHERE address IS NULL;
    `)
	return err
}

func (s *SqlStore) addRepeatingEventCoordinates(ctx context.Context) error {
	_, err := s.db.ExecContext(ctx, `
		ALTER TABLE repeatingEvents ADD COLUMN lat FLOAT(24);
		ALTER TABLE repeatingEvents ADD COLUMN lng FLOAT(24);
		UPDATE repeatingEvents SET lat=0, lng=0;
    `)
	return err
}

func (s *SqlStore) createRepeatingEventsTable(ctx context.Context) error {
	_, err := s.db.ExecContext(ctx, `
        CREATE TABLE IF NOT EXISTS repeatingEvents (
            id varchar(32) NOT NULL PRIMARY KEY UNIQUE,
            deactivated boolean DEFAULT FALSE,
            name varchar(64) NOT NULL,
            location varchar(64),
            location2 varchar(64),
            description varchar(512),
            start TIMESTAMP,
            end TIMESTAMP,
            image varchar(128)
        );
    `)
	return err
}

func (s *SqlStore) createRepeatingEventsOrganizerTable(ctx context.Context) error {
	_, err := s.db.ExecContext(ctx, `
        CREATE TABLE IF NOT EXISTS repeatingEvents_organizer (
            ID INTEGER PRIMARY KEY AUTOINCREMENT,
            repeatingEvent_id varchar(32),
            organizer varchar(64)
        );
    `)
	return err
}

func (s *SqlStore) createRepeatingEventsIntervalsTable(ctx context.Context) error {
	_, err := s.db.ExecContext(ctx, `
        CREATE TABLE IF NOT EXISTS repeatingEvents_interval (
            repeatingEvent_id varchar(32),
            type varchar(32),
            interval integer,
            weekDay integer
        );
    `)
	return err
}

func (s *SqlStore) createRepeatingEventsTagsTable(ctx context.Context) error {
	_, err := s.db.ExecContext(ctx, `
        CREATE TABLE IF NOT EXISTS repeatingEvents_tags (
            ID INTEGER PRIMARY KEY AUTOINCREMENT,
            repeatingEvent_id varchar(32),
            tag varchar(64)
        );
    `)
	return err
}

func (s *SqlStore) createRepeatingEventsOwnedByTable(ctx context.Context) error {
	_, err := s.db.ExecContext(ctx, `
        CREATE TABLE IF NOT EXISTS repeatingEvents_owned_by (
            ID INTEGER PRIMARY KEY AUTOINCREMENT,
            repeatingEvent_id varchar(32),
            user_id varchar(32)
        );
    `)
	return err
}

func (s *SqlStore) removeEmptyTags(ctx context.Context) error {
	revents, _, err := s.Find(context.Background(), &crud.FindParams[FindFilters]{})
	if err != nil {
		return err
	}
	for _, e := range revents {
		needsUpdate := false
		for i, tag := range e.Tags {
			if tag == "" {
				needsUpdate = true
				e.Tags = append(e.Tags[:i], e.Tags[i+1:]...)
			}
		}
		if needsUpdate {
			_, err := s.Update(context.Background(), e)
			if err != nil {
				return err
			}
		}
	}
	return err
}

func (s *SqlStore) removeDateColumnFromIntervalsTable(ctx context.Context) error {
	s.db.Exec(`
        ALTER TABLE repeatingEvents_interval
        DROP COLUMN date;
    `)
	return nil
}

func (s *SqlStore) addReventAlt(ctx context.Context) error {
	_, err := s.db.ExecContext(ctx, `
		ALTER TABLE repeatingEvents ADD COLUMN alt varchar(512) DEFAULT '';
    `)
	return err
}
