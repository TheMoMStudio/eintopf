//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package revent

import (
	"context"
	"database/sql"
	"fmt"
	"strings"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"

	"eintopf.info/internal/crud"
	"eintopf.info/service/dbmigration"
)

// NewSqlStore returns a new sql db RepeatingEvent store.
func NewSqlStore(db *sqlx.DB, migrationService dbmigration.Service) (*SqlStore, error) {
	store := &SqlStore{db: db}
	if err := store.runMigrations(context.Background(), migrationService); err != nil {
		return nil, err
	}
	return store, nil
}

type SqlStore struct {
	db *sqlx.DB
}

func (s *SqlStore) Create(ctx context.Context, newRepeatingEvent *NewRepeatingEvent) (*RepeatingEvent, error) {
	repeatingEvent := RepeatingEventFromNewRepeatingEvent(newRepeatingEvent, uuid.New().String())
	err := s.insertRepeatingEvent(ctx, repeatingEvent)
	if err != nil {
		return nil, err
	}

	err = s.insertOrganizersForRepeatingEvent(ctx, repeatingEvent)
	if err != nil {
		return nil, err
	}
	err = s.insertIntervalsForRepeatingEvent(ctx, repeatingEvent)
	if err != nil {
		return nil, err
	}
	err = s.insertTagsForRepeatingEvent(ctx, repeatingEvent)
	if err != nil {
		return nil, err
	}
	err = s.insertOwnedByForRepeatingEvent(ctx, repeatingEvent)
	if err != nil {
		return nil, err
	}

	return repeatingEvent, nil
}

func (s *SqlStore) insertRepeatingEvent(ctx context.Context, repeatingEvent *RepeatingEvent) error {
	_, err := s.db.NamedExecContext(ctx, `
        INSERT INTO repeatingEvents (
            id,
            deactivated,
            name,
            location,
            location2,
			address,
			lat,
			lng,
            description,
            start,
            end,
            image,
	    alt,
            category,
            topic
        ) VALUES (
            :id,
            :deactivated,
            :name,
            :location,
            :location2,
			:address,
			:lat,
			:lng,
            :description,
            :start,
            :end,
            :image,
	    :alt,
            :category,
            :topic
        )
    `, repeatingEvent)
	return err
}

func (s *SqlStore) insertOrganizersForRepeatingEvent(ctx context.Context, repeatingEvent *RepeatingEvent) error {
	for _, organizer := range repeatingEvent.Organizers {
		_, err := s.db.ExecContext(ctx, `
            INSERT INTO repeatingEvents_organizer (
                repeatingEvent_id,
                organizer
            ) VALUES (
                $1,
                $2
            )
        `, repeatingEvent.ID, organizer)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *SqlStore) insertTagsForRepeatingEvent(ctx context.Context, repeatingEvent *RepeatingEvent) error {
	for _, tag := range repeatingEvent.Tags {
		_, err := s.db.ExecContext(ctx, `
            INSERT INTO repeatingEvents_tags (
                repeatingEvent_id,
                tag
            ) VALUES (
                $1,
                $2
            )
        `, repeatingEvent.ID, tag)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *SqlStore) insertOwnedByForRepeatingEvent(ctx context.Context, repeatingEvent *RepeatingEvent) error {
	for _, ownedBy := range repeatingEvent.OwnedBy {
		_, err := s.db.ExecContext(ctx, `
            INSERT INTO repeatingEvents_owned_by (
                repeatingEvent_id,
                user_id
            ) VALUES (
                $1,
                $2
            )
        `, repeatingEvent.ID, ownedBy)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *SqlStore) insertIntervalsForRepeatingEvent(ctx context.Context, repeatingEvent *RepeatingEvent) error {
	for _, interval := range repeatingEvent.Intervals {
		_, err := s.db.ExecContext(ctx, `
            INSERT INTO repeatingEvents_interval (
                repeatingEvent_id,
                type,
                interval,
                weekDay
            ) VALUES (
                $1,
                $2,
                $3,
                $4
            )
            `, repeatingEvent.ID, interval.Type, interval.Interval, interval.WeekDay)
		if err != nil {
			return fmt.Errorf("insert intervals: %s", err)
		}
	}
	return nil
}

func (s *SqlStore) Update(ctx context.Context, repeatingEvent *RepeatingEvent) (*RepeatingEvent, error) {
	_, err := s.db.NamedExecContext(ctx, `
        UPDATE
            repeatingEvents
        SET
            deactivated=:deactivated,
            name=:name,
            location=:location,
            location2=:location2,
			address=:address,
			lat=:lat,
			lng=:lng,
            description=:description,
            start=:start,
            end=:end,
            image=:image,
	    alt=:alt,
            category=:category,
            topic=:topic
        WHERE
            id=:id
    `, repeatingEvent)
	if err != nil {
		return nil, err
	}

	err = s.deleteOrganizersForRepeatingEvent(ctx, repeatingEvent.ID)
	if err != nil {
		return nil, err
	}
	err = s.insertOrganizersForRepeatingEvent(ctx, repeatingEvent)
	if err != nil {
		return nil, err
	}

	err = s.deleteIntervalsForRepeatingEvent(ctx, repeatingEvent.ID)
	if err != nil {
		return nil, err
	}
	err = s.insertIntervalsForRepeatingEvent(ctx, repeatingEvent)
	if err != nil {
		return nil, err
	}

	err = s.deleteTagsForRepeatingEvent(ctx, repeatingEvent.ID)
	if err != nil {
		return nil, err
	}
	err = s.insertTagsForRepeatingEvent(ctx, repeatingEvent)
	if err != nil {
		return nil, err
	}

	err = s.deleteOwnedByForRepeatingEvent(ctx, repeatingEvent.ID)
	if err != nil {
		return nil, err
	}
	err = s.insertOwnedByForRepeatingEvent(ctx, repeatingEvent)
	if err != nil {
		return nil, err
	}

	return repeatingEvent, err
}

func (s *SqlStore) Delete(ctx context.Context, id string) error {
	_, err := s.db.ExecContext(ctx, "DELETE FROM repeatingEvents WHERE id = $1", id)
	if err != nil {
		return err
	}

	err = s.deleteOrganizersForRepeatingEvent(ctx, id)
	if err != nil {
		return err
	}
	err = s.deleteIntervalsForRepeatingEvent(ctx, id)
	if err != nil {
		return err
	}
	err = s.deleteTagsForRepeatingEvent(ctx, id)
	if err != nil {
		return err
	}
	err = s.deleteOwnedByForRepeatingEvent(ctx, id)
	if err != nil {
		return err
	}

	return nil
}

func (s *SqlStore) deleteOrganizersForRepeatingEvent(ctx context.Context, id string) error {
	_, err := s.db.ExecContext(ctx, "DELETE FROM repeatingEvents_organizer WHERE repeatingEvent_id = $1", id)
	if err != nil {
		return err
	}
	return nil
}

func (s *SqlStore) deleteIntervalsForRepeatingEvent(ctx context.Context, id string) error {
	_, err := s.db.ExecContext(ctx, "DELETE FROM repeatingEvents_interval WHERE repeatingEvent_id = $1", id)
	if err != nil {
		return err
	}
	return nil
}

func (s *SqlStore) deleteTagsForRepeatingEvent(ctx context.Context, id string) error {
	_, err := s.db.ExecContext(ctx, "DELETE FROM repeatingEvents_tags WHERE repeatingEvent_id = $1", id)
	if err != nil {
		return err
	}
	return nil
}

func (s *SqlStore) deleteOwnedByForRepeatingEvent(ctx context.Context, id string) error {
	_, err := s.db.ExecContext(ctx, "DELETE FROM repeatingEvents_owned_by WHERE repeatingEvent_id = $1", id)
	if err != nil {
		return err
	}
	return nil
}

func (s *SqlStore) FindByID(ctx context.Context, id string) (*RepeatingEvent, error) {
	repeatingEvent := &RepeatingEvent{}
	err := s.db.GetContext(ctx, repeatingEvent, `
        SELECT
            id,
            deactivated,
            name,
            location,
            location2,
			address,
			lat,
			lng,
            description,
            start,
            end,
            image,
	    alt,
            category,
            topic
        FROM
            repeatingEvents
        WHERE
            repeatingEvents.id = $1
    `, id)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	s.findOrganizersForRepeatingEvent(ctx, repeatingEvent)
	if err != nil {
		return nil, err
	}
	s.findIntervalsForRepeatingEvent(ctx, repeatingEvent)
	if err != nil {
		return nil, err
	}
	s.findTagsForRepeatingEvent(ctx, repeatingEvent)
	if err != nil {
		return nil, err
	}
	s.findOwnedByForRepeatingEvent(ctx, repeatingEvent)
	if err != nil {
		return nil, err
	}

	return repeatingEvent, nil
}

func (s *SqlStore) findOrganizersForRepeatingEvent(ctx context.Context, repeatingEvent *RepeatingEvent) error {
	organizers := []string{}
	err := s.db.SelectContext(ctx, &organizers, `
        SELECT organizer
        FROM repeatingEvents_organizer
        WHERE repeatingEvent_id = $1
    `, repeatingEvent.ID)
	if err == sql.ErrNoRows {
		return nil
	}
	if err != nil {
		return err
	}
	repeatingEvent.Organizers = organizers
	return nil
}

func (s *SqlStore) findIntervalsForRepeatingEvent(ctx context.Context, repeatingEvent *RepeatingEvent) error {
	intervals := []Interval{}
	err := s.db.SelectContext(ctx, &intervals, `
        SELECT type, interval, weekDay
        FROM repeatingEvents_interval
        WHERE repeatingEvent_id = $1
    `, repeatingEvent.ID)
	if err == sql.ErrNoRows {
		return nil
	}
	if err != nil {
		return err
	}
	repeatingEvent.Intervals = intervals
	return nil
}

func (s *SqlStore) findTagsForRepeatingEvent(ctx context.Context, repeatingEvent *RepeatingEvent) error {
	tags := []string{}
	err := s.db.SelectContext(ctx, &tags, `
        SELECT tag
        FROM repeatingEvents_tags
        WHERE repeatingEvent_id = $1
    `, repeatingEvent.ID)
	if err == sql.ErrNoRows {
		return nil
	}
	if err != nil {
		return err
	}
	repeatingEvent.Tags = tags
	return nil
}

func (s *SqlStore) findOwnedByForRepeatingEvent(ctx context.Context, RepeatingEvent *RepeatingEvent) error {
	ownedBy := []string{}
	err := s.db.SelectContext(ctx, &ownedBy, `
        SELECT user_id
        FROM repeatingEvents_owned_by
        WHERE repeatingEvent_id = $1
    `, RepeatingEvent.ID)
	if err == sql.ErrNoRows {
		return nil
	}
	if err != nil {
		return err
	}
	RepeatingEvent.OwnedBy = ownedBy
	return nil
}

var sortableFields = map[string]string{
	"id":          "id",
	"deactivated": "deactivated",
	"name":        "name",
	"location":    "location",
	"start":       "start",
	"end":         "end",
}

func (s *SqlStore) Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*RepeatingEvent, int, error) {
	query := `
        SELECT
            repeatingEvents.id,
            repeatingEvents.deactivated,
            repeatingEvents.name,
            repeatingEvents.location,
            repeatingEvents.location2,
			repeatingEvents.address,
			repeatingEvents.lat,
			repeatingEvents.lng,
            repeatingEvents.description,
            repeatingEvents.start,
            repeatingEvents.end,
            repeatingEvents.image,
            repeatingEvents.category,
            repeatingEvents.topic
        FROM
            repeatingEvents
    `
	joinQuery := ""
	whereStatements := []string{}
	sqlParams := make(map[string]interface{})
	if params != nil {
		if params.Filters != nil {
			if params.Filters.ID != nil {
				whereStatements = append(whereStatements, "repeatingEvents.id=:id")
				sqlParams["id"] = params.Filters.ID
			}
			if params.Filters.Deactivated != nil {
				whereStatements = append(whereStatements, "repeatingEvents.deactivated=:deactivated")
				sqlParams["deactivated"] = params.Filters.Deactivated
			}
			if params.Filters.Name != nil {
				whereStatements = append(whereStatements, "repeatingEvents.name=:name")
				sqlParams["name"] = params.Filters.Name
			}
			if params.Filters.Organizers != nil {
				joinQuery += " JOIN repeatingEvents_organizer ON repeatingEvents.id = repeatingEvents_organizer.repeatingEvent_id"
				organizerStatements := make([]string, len(params.Filters.Organizers))
				for i, organizer := range params.Filters.Organizers {
					organizerRef := fmt.Sprintf("tag%d", i)
					organizerStatements[i] = fmt.Sprintf("repeatingEvents_organizer.tag=:%s", organizerRef)
					sqlParams[organizerRef] = organizer
				}
				whereStatements = append(whereStatements, fmt.Sprintf("(%s)", strings.Join(organizerStatements, " OR ")))
			}
			if params.Filters.Location != nil {
				whereStatements = append(whereStatements, "repeatingEvents.location=:location")
				sqlParams["Location"] = params.Filters.Location
			}
			if params.Filters.Location2 != nil {
				whereStatements = append(whereStatements, "repeatingEvents.location2=:location2")
				sqlParams["Location2"] = params.Filters.Location2
			}
			if params.Filters.Category != nil {
				whereStatements = append(whereStatements, "repeatingEvents.category=:category")
				sqlParams["category"] = params.Filters.Category
			}
			if params.Filters.Topic != nil {
				whereStatements = append(whereStatements, "repeatingEvents.topic=:topic")
				sqlParams["topic"] = params.Filters.Topic
			}
			if params.Filters.Address != nil {
				whereStatements = append(whereStatements, "repeatingEvents.address=:address")
				sqlParams["Address"] = params.Filters.Address
			}
			if params.Filters.Lat != nil {
				whereStatements = append(whereStatements, "repeatingEvents.lat=:lat")
				sqlParams["Lat"] = params.Filters.Lat
			}
			if params.Filters.Lng != nil {
				whereStatements = append(whereStatements, "repeatingEvents.lng=:lng")
				sqlParams["Lng"] = params.Filters.Lat
			}
			if params.Filters.Description != nil {
				whereStatements = append(whereStatements, "repeatingEvents.description=:description")
				sqlParams["Description"] = params.Filters.Description
			}
			if params.Filters.Start != nil {
				whereStatements = append(whereStatements, "repeatingEvents.start=:start")
				sqlParams["start"] = params.Filters.Start
			}
			if params.Filters.End != nil {
				whereStatements = append(whereStatements, "repeatingEvents.end=:end")
				sqlParams["end"] = params.Filters.End
			}
			if params.Filters.Tags != nil {
				joinQuery += " JOIN repeatingEvents_tags ON repeatingEvents.id = repeatingEvents_tags.repeatingEvent_id"
				tagStatemtes := make([]string, len(params.Filters.Tags))
				for i, tag := range params.Filters.Tags {
					tagRef := fmt.Sprintf("tag%d", i)
					tagStatemtes[i] = fmt.Sprintf("repeatingEvents_tags.tag=:%s", tagRef)
					sqlParams[tagRef] = tag
				}
				whereStatements = append(whereStatements, fmt.Sprintf("(%s)", strings.Join(tagStatemtes, " OR ")))
			}
			if params.Filters.OwnedBy != nil {
				joinQuery += " JOIN repeatingEvents_owned_by ON repeatingEvents.id = repeatingEvents_owned_by.repeatingEvent_id"
				ownedByStatements := make([]string, len(params.Filters.OwnedBy))
				for i, ownedBy := range params.Filters.OwnedBy {
					ownedByRef := fmt.Sprintf("ownedBy%d", i)
					ownedByStatements[i] = fmt.Sprintf("repeatingEvents_owned_by.user_id=:%s", ownedByRef)
					sqlParams[ownedByRef] = ownedBy
				}
				whereStatements = append(whereStatements, fmt.Sprintf("(%s)", strings.Join(ownedByStatements, " OR ")))
			}

			if joinQuery != "" {
				query += " " + joinQuery
			}
			if len(whereStatements) > 0 {
				query += " WHERE " + strings.Join(whereStatements, " AND ")
			}
			if joinQuery != "" {
				query += " GROUP BY repeatingEvents.id"
			}
		}

		if params.Sort != "" {
			sort, ok := sortableFields[params.Sort]
			if !ok {
				return nil, 0, fmt.Errorf("find repeatingEvents: invalid sort field: %s", params.Sort)
			}
			order := "ASC"
			if params.Order == "DESC" {
				order = "DESC"
			}
			query += fmt.Sprintf(" ORDER BY %s %s", sort, order)
		}
		if params.Limit > 0 {
			query += fmt.Sprintf(" LIMIT %d", params.Limit)
		}
		if params.Offset > 0 {
			query += fmt.Sprintf(" OFFSET %d", params.Offset)
		}
	}
	rows, err := s.db.NamedQueryContext(ctx, query, sqlParams)
	if err != nil && err != sql.ErrNoRows {
		return nil, 0, fmt.Errorf("find repeatingEvents: %s", err)
	}
	defer rows.Close()
	repeatingEvents := make([]*RepeatingEvent, 0)
	for rows.Next() {
		repeatingEvent := RepeatingEvent{}
		rows.StructScan(&repeatingEvent)

		repeatingEvents = append(repeatingEvents, &repeatingEvent)
	}

	for i, repeatingEvent := range repeatingEvents {
		err = s.findOrganizersForRepeatingEvent(ctx, repeatingEvent)
		if err != nil {
			return nil, 0, err
		}
		err = s.findIntervalsForRepeatingEvent(ctx, repeatingEvent)
		if err != nil {
			return nil, 0, err
		}
		err := s.findTagsForRepeatingEvent(ctx, repeatingEvent)
		if err != nil {
			return nil, 0, err
		}
		err = s.findOwnedByForRepeatingEvent(ctx, repeatingEvent)
		if err != nil {
			return nil, 0, err
		}
		repeatingEvents[i] = repeatingEvent
	}

	totalQuery := `
        SELECT COUNT(*) as total
        FROM (
            SELECT repeatingEvents.id
            FROM repeatingEvents
    `
	if len(whereStatements) > 0 {
		if joinQuery != "" {
			totalQuery += " " + joinQuery
		}
		totalQuery += " WHERE " + strings.Join(whereStatements, " AND ")
		if joinQuery != "" {
			totalQuery += " GROUP BY repeatingEvents.id"
		}
	}
	totalQuery += ")"

	totalRepeatingEvents := struct {
		Total int `db:"total"`
	}{}
	rows2, err := s.db.NamedQueryContext(ctx, totalQuery, sqlParams)
	if err != nil && err != sql.ErrNoRows {
		return nil, 0, fmt.Errorf("find repeatingEvents: total: %s", err)
	}
	defer rows2.Close()
	if rows2.Next() {
		rows2.StructScan(&totalRepeatingEvents)
	}

	return repeatingEvents, totalRepeatingEvents.Total, nil
}
