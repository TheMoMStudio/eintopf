//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package event

import (
	"context"
	"fmt"
	"strings"
	"time"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
)

// NewEvent defines the data for a new event.
type NewEvent struct {
	Published    bool       `json:"published"`
	Parent       string     `json:"parent"`
	ParentListed bool       `json:"parentListed"`
	Name         string     `json:"name"`
	Organizers   []string   `json:"organizers"`
	Involved     []Involved `json:"involved"`
	Location     string     `json:"location"`
	Location2    string     `json:"location2"`
	Address      string     `json:"address"`
	Lat          float64    `json:"lat"`
	Lng          float64    `json:"lng"`
	Description  string     `json:"description"`
	Start        time.Time  `json:"start"`
	End          *time.Time `json:"end"`
	Tags         []string   `json:"tags"`
	Category     string     `json:"category"`
	Topic        string     `json:"topic"`
	Image        string     `json:"image"`
	OwnedBy      []string   `json:"ownedBy"`
}

// Involved defines an entity, that is involved with an event.
type Involved struct {
	Name        string `json:"name" db:"name"`
	Description string `json:"description" db:"description"`
}

// IsOwned returns true if the id is in the OwnedBy field.
func (e *NewEvent) IsOwned(id string) bool {
	owned := false
	for _, owner := range e.OwnedBy {
		if owner == id {
			owned = true
		}
	}
	return owned
}

// Event defines a calendar event.
// It implements indexo.Coppyable.
type Event struct {
	ID           string     `json:"id" db:"id"`
	Deactivated  bool       `json:"deactivated" db:"deactivated"`
	Published    bool       `json:"published" db:"published"`
	Canceled     bool       `json:"canceled" db:"canceled"`
	Parent       string     `json:"parent" db:"parent"`
	ParentListed bool       `json:"parentListed" db:"parentListed"`
	Name         string     `json:"name" db:"name"`
	Organizers   []string   `json:"organizers" db:"organizers"`
	Involved     []Involved `json:"involved" db:"involved"`
	Location     string     `json:"location" db:"location"`
	Location2    string     `json:"location2" db:"location2"`
	Address      string     `json:"address" db:"address"`
	Lat          float64    `json:"lat" db:"lat"`
	Lng          float64    `json:"lng" db:"lng"`
	Description  string     `json:"description" db:"description"`
	Start        time.Time  `json:"start" db:"start"`
	End          *time.Time `json:"end" db:"end"`
	Tags         []string   `json:"tags" db:"tags"`
	Category     string     `json:"category" db:"category"`
	Topic        string     `json:"topic" db:"topic"`
	Image        string     `json:"image" db:"image"`
	Alt          string     `json:"alt" db:"alt"`
	OwnedBy      []string   `json:"ownedBy" db:"ownedBy"`
}

func (e Event) Identifier() string {
	return e.ID
}

func EventFromNewEvent(newEvent *NewEvent, id string) *Event {
	return &Event{
		ID:           id,
		Deactivated:  false,
		Published:    newEvent.Published,
		Parent:       newEvent.Parent,
		ParentListed: newEvent.ParentListed,
		Name:         newEvent.Name,
		Organizers:   newEvent.Organizers,
		Involved:     newEvent.Involved,
		Location:     newEvent.Location,
		Location2:    newEvent.Location2,
		Address:      newEvent.Address,
		Lat:          newEvent.Lat,
		Lng:          newEvent.Lng,
		Description:  newEvent.Description,
		Image:        newEvent.Image,
		Start:        newEvent.Start,
		End:          newEvent.End,
		Tags:         newEvent.Tags,
		Category:     newEvent.Category,
		Topic:        newEvent.Topic,
		OwnedBy:      newEvent.OwnedBy,
	}
}

// IsOwned returns true if the id is in the OwnedBy field.
func (e *Event) IsOwned(id string) bool {
	owned := false
	for _, owner := range e.OwnedBy {
		if owner == id {
			owned = true
		}
	}
	return owned
}

// Indexable indicates wether the event should be indexed in the search.
// All events are indexable if they are not deactivated.
func (e *Event) Indexable() bool {
	if e.Deactivated {
		return false
	}
	return true
}

// Listable indicates wether the event should be shown on list pages.
func (e *Event) Listable() bool {
	if !e.Published {
		return false
	}
	if e.Parent != "" {
		if strings.HasPrefix(e.Parent, "revent") {
			return true
		}
		if !e.ParentListed {
			return false
		}
	}
	return true
}

// FindFilters defines the possible filters for the find method.
type FindFilters struct {
	ID                  *string    `json:"id,omitempty"`
	Deactivated         *bool      `json:"deactivated,omitempty"`
	Published           *bool      `json:"published,omitempty"`
	Canceled            *bool      `json:"canceled,omitempty"`
	Parent              *string    `json:"parent,omitempty"`
	Name                *string    `json:"name,omitempty"`
	LikeName            *string    `json:"likeName,omitempty"`
	Organizers          []string   `json:"organizers,omitempty"`
	Location            *string    `json:"location,omitempty"`
	Location2           *string    `json:"location2,omitempty"`
	Address             *string    `json:"address,omitempty"`
	Lat                 *float64   `json:"lat,omitempty"`
	Lng                 *float64   `json:"lng,omitempty"`
	Description         *string    `json:"description,omitempty"`
	Start               *time.Time `json:"start,omitempty"`
	StartBefore         *time.Time `json:"startBefore,omitempty"`
	StartAfter          *time.Time `json:"startAfter,omitempty"`
	End                 *time.Time `json:"end,omitempty"`
	EndBefore           *time.Time `json:"endBefore,omitempty"`
	EndAfter            *time.Time `json:"endAfter,omitempty"`
	Tags                []string   `json:"tags,omitempty"`
	Category            *string    `json:"category,omitempty"`
	Topic               *string    `json:"topic,omitempty"`
	OwnedBy             []string   `json:"ownedBy,omitempty"`
	OwnedByOrOrganizers []string
}

// Storer defines a service for CRUD operations on the event model.
type Storer = crud.Storer[NewEvent, Event, FindFilters]

// NewService returns a new event service.
func NewService(store Storer, groupOwnerService groupOwnerService) *Service {
	return &Service{store: store, groupOwnerService: groupOwnerService}
}

type Service struct {
	store             Storer
	groupOwnerService groupOwnerService
}

type groupOwnerService interface {
	// GroupIDsByOwners returns all group id for any of the owners.
	GroupIDsByOwners(ctx context.Context, owners []string) ([]string, error)
}

func (s *Service) Create(ctx context.Context, newEvent *NewEvent) (*Event, error) {
	id, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return nil, err
	}
	if !newEvent.IsOwned(id) {
		newEvent.OwnedBy = append(newEvent.OwnedBy, id)
	}
	newEvent.Name = strings.TrimSpace(newEvent.Name)
	return s.store.Create(ctx, newEvent)
}

func (s *Service) Update(ctx context.Context, event *Event) (*Event, error) {
	return s.store.Update(ctx, event)
}

func (s *Service) Delete(ctx context.Context, id string) error {
	return s.store.Delete(ctx, id)
}

func (s *Service) FindByID(ctx context.Context, id string) (*Event, error) {
	return s.store.FindByID(ctx, id)
}

func (s *Service) Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*Event, int, error) {
	if params != nil && params.Filters != nil && params.Filters.OwnedBy != nil {
		if len(params.Filters.OwnedBy) == 1 && params.Filters.OwnedBy[0] == "self" {
			id, err := auth.UserIDFromContext(ctx)
			if err == nil {
				params.Filters.OwnedBy = []string{id}
			}
		}

		ownedGroupIDs, err := s.groupOwnerService.GroupIDsByOwners(ctx, params.Filters.OwnedBy)
		if err != nil {
			return nil, 0, err
		}
		if len(ownedGroupIDs) > 0 {
			for i := range ownedGroupIDs {
				ownedGroupIDs[i] = fmt.Sprintf("id:%s", ownedGroupIDs[i])
			}
			params.Filters.OwnedByOrOrganizers = ownedGroupIDs
		}
	}

	events, total, err := s.store.Find(ctx, params)
	if err != nil {
		return nil, 0, err
	}
	return events, total, nil
}

func containsOwnedBy(ownedBy, filter []string) bool {
	for _, o := range ownedBy {
		for _, f := range filter {
			if o == f {
				return true
			}
		}
	}
	return false
}
