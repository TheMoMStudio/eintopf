//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package event_test

import (
	"fmt"
	"testing"

	"eintopf.info/service/dbmigration"
	"eintopf.info/service/event"
	"eintopf.info/service/event/testutil"
	"eintopf.info/test"
)

func TestSqlStore(t *testing.T) {
	testutil.TestStore(t, func() (event.Storer, func(), error) {
		db, cleanup, err := test.CreateSqliteTestDB(t.Name())
		if err != nil {
			return nil, cleanup, err
		}

		migrationStore, err := dbmigration.NewSqlStore(db)
		if err != nil {
			return nil, cleanup, fmt.Errorf("failed to create migration store: %s", err)
		}
		migrationService := dbmigration.NewService(migrationStore)

		store, err := event.NewSqlStore(db, migrationService)
		if err != nil {
			return nil, cleanup, err
		}
		return store, cleanup, nil
	})
}
