//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package event

import (
	"context"
	"strings"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
	"eintopf.info/service/oqueue"
)

type Operator struct {
	storer Storer
	queue  oqueue.Service
}

func NewOperator(storer Storer, queue oqueue.Service) *Operator {
	return &Operator{storer: storer, queue: queue}
}

type CreateOperation struct {
	Event  *Event
	userID string
}

func NewCreateOperation(event *Event, userID string) CreateOperation {
	return CreateOperation{event, userID}
}

func (o CreateOperation) UserID() string {
	return o.userID
}

func (i *Operator) Create(ctx context.Context, newEvent *NewEvent) (*Event, error) {
	event, err := i.storer.Create(ctx, newEvent)
	if err != nil {
		return event, err
	}
	if event == nil {
		return nil, nil
	}

	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return nil, err
	}
	i.queue.AddOperation(CreateOperation{
		Event:  event,
		userID: userID,
	})

	if event.Parent != "" {
		parentID := strings.TrimPrefix(event.Parent, "id:")
		parentEvent, err := i.storer.FindByID(auth.ContextWithRole(ctx, auth.RoleInternal), parentID)
		if err == nil && parentEvent != nil {
			i.queue.AddOperation(UpdateOperation{
				Event:  parentEvent,
				userID: userID,
			})
		}
	}

	return event, nil
}

type UpdateOperation struct {
	Event  *Event
	userID string
}

func NewUpdateOperation(event *Event, userID string) UpdateOperation {
	return UpdateOperation{event, userID}
}

func (o UpdateOperation) UserID() string {
	return o.userID
}

func (i *Operator) Update(ctx context.Context, event *Event) (*Event, error) {
	event, err := i.storer.Update(ctx, event)
	if err != nil {
		return event, err
	}
	if event == nil {
		return nil, nil
	}

	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return nil, err
	}
	i.queue.AddOperation(UpdateOperation{
		Event:  event,
		userID: userID,
	})

	if event.Parent != "" {
		parentID := strings.TrimPrefix(event.Parent, "id:")
		parentEvent, err := i.storer.FindByID(auth.ContextWithRole(ctx, auth.RoleInternal), parentID)
		if err == nil && parentEvent != nil {
			i.queue.AddOperation(UpdateOperation{
				Event:  parentEvent,
				userID: userID,
			})
		}
	}

	return event, nil
}

type DeleteOperation struct {
	ID     string
	userID string
}

func NewDeleteOperation(id string, userID string) DeleteOperation {
	return DeleteOperation{id, userID}
}

func (o DeleteOperation) UserID() string {
	return o.userID
}

func (i *Operator) Delete(ctx context.Context, id string) error {
	event, err := i.storer.FindByID(ctx, id)
	if err != nil {
		return nil
	}

	err = i.storer.Delete(ctx, id)
	if err != nil {
		return err
	}

	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return err
	}
	i.queue.AddOperation(DeleteOperation{
		ID:     id,
		userID: userID,
	})

	if event != nil && event.Parent != "" {
		parentID := strings.TrimPrefix(event.Parent, "id:")
		parentEvent, err := i.storer.FindByID(auth.ContextWithRole(ctx, auth.RoleInternal), parentID)
		if err == nil && parentEvent != nil {
			i.queue.AddOperation(UpdateOperation{
				Event:  parentEvent,
				userID: userID,
			})
		}
	}

	return nil
}

func (i *Operator) FindByID(ctx context.Context, id string) (*Event, error) {
	return i.storer.FindByID(ctx, id)
}

func (i *Operator) Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*Event, int, error) {
	return i.storer.Find(ctx, params)
}
