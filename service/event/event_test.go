//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package event_test

import (
	"context"
	"testing"

	"github.com/google/go-cmp/cmp"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
	"eintopf.info/service/dbmigration"
	"eintopf.info/service/event"
	"eintopf.info/test"
)

func TestEventIndexable(t *testing.T) {
	tests := []struct {
		event     *event.Event
		indexable bool
	}{
		{event: &event.Event{Deactivated: true, Published: true}, indexable: false},
		{event: &event.Event{Published: false}, indexable: true},
		{event: &event.Event{Published: true}, indexable: true},
		{event: &event.Event{Published: true, Parent: "foo"}, indexable: true},
		{event: &event.Event{Published: true, Parent: "foo", ParentListed: true}, indexable: true},
	}

	for i, test := range tests {
		if test.event.Indexable() != test.indexable {
			t.Errorf(
				"%d: Deactivated: %t, Published: %t, Parent: %s, ParentListed: %t -> Indexable %t",
				i,
				test.event.Deactivated,
				test.event.Published,
				test.event.Parent,
				test.event.ParentListed,
				test.indexable,
			)
		}
	}
}

func TestEventListable(t *testing.T) {
	tests := []struct {
		event    *event.Event
		listable bool
	}{
		{event: &event.Event{Deactivated: true, Published: true}, listable: true},
		{event: &event.Event{Published: false}, listable: false},
		{event: &event.Event{Published: true}, listable: true},
		{event: &event.Event{Published: true, Parent: "foo", ParentListed: false}, listable: false},
		{event: &event.Event{Published: true, Parent: "foo", ParentListed: true}, listable: true},
		{event: &event.Event{Published: true, Parent: "revent:foo"}, listable: true},
	}

	for i, test := range tests {
		if test.event.Listable() != test.listable {
			t.Errorf(
				"%d: Deactivated: %t, Published: %t, Parent: %s, ParentListed: %t -> Listable %t",
				i,
				test.event.Deactivated,
				test.event.Published,
				test.event.Parent,
				test.event.ParentListed,
				test.listable,
			)
		}
	}
}

func TestEventServiceCreateOwnedBy(t *testing.T) {
	store := event.NewMemoryStore()
	eventService := event.NewService(store, groupOwnerService{})

	// It adds the OwnedBy field.
	e, err := eventService.Create(auth.ContextWithID(context.Background(), "foo"), &event.NewEvent{})
	if err != nil {
		t.Fatalf("expected eventService.Create to succeed. got %s", err.Error())
	}
	if e.OwnedBy[0] != "foo" {
		t.Fatal("OwnedBy should be set")
	}
}

func TestEventServiceWhiteSpace(t *testing.T) {
	store := event.NewMemoryStore()
	eventService := event.NewService(store, groupOwnerService{})

	// It adds the OwnedBy field.
	e, err := eventService.Create(auth.ContextWithID(context.Background(), "foo"), &event.NewEvent{Name: " foo "})
	if err != nil {
		t.Fatalf("expected eventService.Create to succeed. got %s", err.Error())
	}
	if e.Name != "foo" {
		t.Fatalf("should remove white space from name: '%s'", e.Name)
	}
}

func TestEventServiceFindOwnedBySelf(t *testing.T) {
	db, cleanup, _ := test.CreateSqliteTestDB("TestEventServiceFindOwnedBySelf")
	t.Cleanup(cleanup)
	migration, _ := dbmigration.NewSqlStore(db)

	store, _ := event.NewSqlStore(db, dbmigration.NewService(migration))
	e1, err := store.Create(context.Background(), &event.NewEvent{OwnedBy: []string{"1"}, Tags: []string{}, Involved: []event.Involved{}, Organizers: []string{}})
	if err != nil {
		t.Fatal(err)
	}

	eventService := event.NewService(store, groupOwnerService{})

	ctx := auth.ContextWithID(context.Background(), "1")
	events, total, err := eventService.Find(ctx, &crud.FindParams[event.FindFilters]{Filters: &event.FindFilters{OwnedBy: []string{"self"}}})
	if err != nil {
		t.Fatal(err)
	}
	if total != 1 {
		t.Errorf("total mismatch: want: %d, got: %d", 1, total)
	}
	if diff := cmp.Diff([]*event.Event{e1}, events); diff != "" {
		t.Errorf("events mismatch (-want +got):\n%s", diff)
	}
}

func TestEventServiceFindOwnedByGroup(t *testing.T) {
	db, cleanup, _ := test.CreateSqliteTestDB("TestEventServiceFindOwnedByGroup")
	t.Cleanup(cleanup)
	migration, _ := dbmigration.NewSqlStore(db)

	store, _ := event.NewSqlStore(db, dbmigration.NewService(migration))
	_, err := store.Create(context.Background(), &event.NewEvent{OwnedBy: []string{"1"}})
	if err != nil {
		t.Fatal(err)
	}
	e2, err := store.Create(context.Background(), &event.NewEvent{
		Name:       "a",
		OwnedBy:    []string{"1"},
		Organizers: []string{"id:3", "id:2"},
		Tags:       []string{},
		Involved:   []event.Involved{},
	})
	if err != nil {
		t.Fatal(err)
	}
	e3, err := store.Create(context.Background(), &event.NewEvent{
		Name:       "b",
		OwnedBy:    []string{"1"},
		Organizers: []string{"id:2"},
		Tags:       []string{},
		Involved:   []event.Involved{},
	})
	if err != nil {
		t.Fatal(err)
	}

	eventService := event.NewService(store, groupOwnerService{})

	events, total, err := eventService.Find(context.Background(), &crud.FindParams[event.FindFilters]{
		Sort:    "name",
		Filters: &event.FindFilters{OwnedBy: []string{"2"}},
	})
	if err != nil {
		t.Fatal(err)
	}
	if total != 2 {
		t.Errorf("total mismatch: want: %d, got: %d", 2, total)
	}
	if diff := cmp.Diff([]*event.Event{e2, e3}, events); diff != "" {
		t.Errorf("events mismatch (-want +got):\n%s", diff)
	}
}

type groupOwnerService struct{}

// GroupIDsByOwners returns the following:
//
//	1 -> 1, 2
//	2 -> 3, 4
//	3 -> 4
func (g groupOwnerService) GroupIDsByOwners(ctx context.Context, ownedBy []string) ([]string, error) {
	for _, o := range ownedBy {
		switch o {
		case "1":
			return []string{"1", "2"}, nil
		case "2":
			return []string{"2", "3"}, nil
		case "3":
			return []string{"4"}, nil
		}
	}
	return []string{}, nil
}
