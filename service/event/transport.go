//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package event

import (
	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/crud"
	"eintopf.info/internal/xhttp"
	"eintopf.info/service/auth"
)

// Router returns a new http router that handles crud event requests for a given
// event service.
func Router(storer Storer, authService auth.Service) func(chi.Router) {
	handler := crud.NewHandler(storer)
	return func(r chi.Router) {
		r.Options("/", xhttp.CorsHandler)

		// swagger:route GET /events/ event findEvents
		//
		// Retrieves all events.
		//
		// Parameters:
		//   + name: offset
		//     description: offset in event list
		//     in: query
		//     type: integer
		//     required: false
		//   + name: limit
		//     description: limits the event list
		//     in: query
		//     type: integer
		//     required: false
		//   + name: sort
		//     description: field that gets sorted
		//     in: query
		//     type: string
		//     required: false
		//   + name: order
		//     description: sort order ("ASC" or "DESC")
		//     in: query
		//     type: string
		//     required: false
		//   + name: filters
		//     description: filters get combined with AND logic
		//     in: query
		//     type: object
		//     required: false
		//
		//     Responses:
		//       200: findEventsResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: false})).
			Get("/", handler.Find)

		// swagger:route POST /events/ event createEvent
		//
		// Creates a new event with the given data.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: createEventResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Post("/", handler.Create)

		r.Options("/{id}", xhttp.CorsHandler)

		// swagger:route GET /events/{id} event findEvent
		//
		// Finds the event with the given id.
		//
		//     Responses:
		//       200: findEventResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.Get("/{id}", handler.FindByID)

		// swagger:route PUT /events/{id} event updateEvent
		//
		// Updates the event with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: updateEventResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Put("/{id}", handler.Update)

		// swagger:route DELETE /events/{id} event deleteEvent
		//
		// Deletes the event with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: deleteEventResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Delete("/{id}", handler.Delete)
	}
}

// swagger:response findEventsResponse
type findResponse struct {
	// in:body
	Events []*Event

	// in:header
	TotalEvents int `json:"x-total-count"`
}

// swagger:parameters createEvent
type createRequest struct {
	// in:body
	Event *NewEvent
}

// swagger:response createEventResponse
type createResponse struct {
	// in:body
	Event *Event
}

// swagger:parameters findEvent
type findByIDRequest struct {
	// in:query
	ID string `json:"id"`
}

// swagger:response findEventResponse
type findByIDResponse struct {
	// in:body
	Event *Event
}

type FindResponse[Model any] struct {
	// in:body
	Model *Model
}

// swagger:parameters updateEvent
type updateRequest struct {
	// in:body
	Event *Event
}

// swagger:response updateEventResponse
type updateResponse struct {
	// in:body
	Event *Event
}

// swagger:parameters deleteEvent
type deleteRequest struct {
	// in:query
	ID string `json:"id"`
}

// swagger:response deleteEventResponse
type deleteResponse struct{}
