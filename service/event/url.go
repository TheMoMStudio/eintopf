//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package event

import (
	"fmt"
	"net/url"
	"path"
)

// URLFromID takes a base url an a event id and constructs an url pointing to
// the event on the web frontend.
func URLFromID(baseURL string, id string) (string, error) {
	u, err := url.Parse(baseURL)
	if err != nil {
		return "", fmt.Errorf("baseURL: %s", err)
	}
	u.Path = path.Join(u.Path, "/event/", id)
	return u.String(), nil
}
