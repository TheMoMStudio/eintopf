//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package testutil

import (
	"context"
	"reflect"
	"testing"

	"eintopf.info/internal/crud"
	"eintopf.info/service/event"
)

type newStore func() (event.Storer, func(), error)

// TestStore tests if a given event.Service acts as a event store.
func TestStore(t *testing.T, newStore newStore) {
	ctx := context.Background()
	t.Helper()

	t.Run("Create", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event, err := store.Create(ctx, &event.NewEvent{Name: "event1"})
		if err != nil {
			tt.Fatalf("expected store.Create to succeed. got %s", err.Error())
		}
		if event == nil {
			tt.Fatalf("expected event not to be nil")
		}
	})

	t.Run("CreateWithTags", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event, err := store.Create(ctx, &event.NewEvent{Name: "event1", Tags: []string{"foo", "bar"}})
		if err != nil {
			tt.Fatalf("expected store.Create to succeed. got %s", err.Error())
		}
		if event == nil {
			tt.Fatalf("expected event not to be nil")
		}
	})

	t.Run("CreateWithOwnedBy", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event, err := store.Create(ctx, &event.NewEvent{Name: "event1", OwnedBy: []string{"foo", "bar"}})
		if err != nil {
			tt.Fatalf("expected store.Create to succeed. got %s", err.Error())
		}
		if event == nil {
			tt.Fatalf("expected event not to be nil")
		}
	})

	t.Run("FindByIDNoMatch", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event, err := store.FindByID(ctx, "test")
		if err != nil {
			tt.Fatalf("expected store.FindByID to succeed. got %s", err.Error())
		}
		if event != nil {
			tt.Fatalf("expected to event to be nil. got %v", event)
		}
	})

	t.Run("FindByIDWithMatch", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event1, _ := store.Create(ctx, &event.NewEvent{Name: "event1"})
		event, err := store.FindByID(ctx, event1.ID)
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if event.ID != event1.ID || event.Name != event1.Name {
			tt.Fatalf("expected to find event1. got %v. want %v", event, event1)
		}
	})

	t.Run("FindByIDWithTags", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event1, _ := store.Create(ctx, &event.NewEvent{
			Name: "event1",
			Tags: []string{"a", "b"},
		})

		event, err := store.FindByID(ctx, event1.ID)
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if event.ID != event1.ID || event.Name != event1.Name || !reflect.DeepEqual(event.Tags, event1.Tags) {
			tt.Fatalf("expected to find event1. got %v. want %v", event, event1)
		}
	})

	t.Run("FindByIDWithOwnedBy", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event1, _ := store.Create(ctx, &event.NewEvent{
			Name:    "event1",
			OwnedBy: []string{"a", "b"},
		})

		event, err := store.FindByID(ctx, event1.ID)
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if event.ID != event1.ID || event.Name != event1.Name || !reflect.DeepEqual(event.OwnedBy, event1.OwnedBy) {
			tt.Fatalf("expected to find event1. got %v. want %v", event, event1)
		}
	})

	t.Run("Find", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		store.Create(ctx, &event.NewEvent{Name: "event1"})
		store.Create(ctx, &event.NewEvent{Name: "event2"})
		events, totalEvents, err := store.Find(ctx, nil)
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(events) != 2 {
			tt.Fatalf("expected to recieve two events. got %d", len(events))
		}
		if totalEvents != 2 {
			tt.Fatalf("expected to recieve a total of two events. got %d", totalEvents)
		}
	})

	t.Run("FindTags", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event0, _ := store.Create(ctx, &event.NewEvent{Name: "event1", Tags: []string{"foo"}})
		event1, _ := store.Create(ctx, &event.NewEvent{Name: "event2", Tags: []string{"bar", "foo"}})
		events, _, err := store.Find(ctx, nil)
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if !reflect.DeepEqual(events[0].Tags, event0.Tags) {
			tt.Fatalf("event0: tags are not the same: got %v, want %v", events[0].Tags, event0.Tags)
		}
		if !reflect.DeepEqual(events[1].Tags, event1.Tags) {
			tt.Fatalf("event0: tags are not the same: got %v, want %v", events[1].Tags, event1.Tags)
		}
	})

	t.Run("FindOwnedBy", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event0, _ := store.Create(ctx, &event.NewEvent{Name: "event1", OwnedBy: []string{"foo"}})
		event1, _ := store.Create(ctx, &event.NewEvent{Name: "event2", OwnedBy: []string{"bar", "foo"}})
		events, _, err := store.Find(ctx, nil)
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if !reflect.DeepEqual(events[0].OwnedBy, event0.OwnedBy) {
			tt.Fatalf("event0: ownedBy are not the same: got %v, want %v", events[0].OwnedBy, event0.OwnedBy)
		}
		if !reflect.DeepEqual(events[1].OwnedBy, event1.OwnedBy) {
			tt.Fatalf("event0: ownedBy are not the same: got %v, want %v", events[1].OwnedBy, event1.OwnedBy)
		}
	})

	t.Run("FindFilter", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event1, _ := store.Create(ctx, &event.NewEvent{Name: "event1"})
		store.Create(ctx, &event.NewEvent{Name: "event2"})
		events, totalEvents, err := store.Find(ctx, &crud.FindParams[event.FindFilters]{
			Filters: &event.FindFilters{Name: &event1.Name},
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(events) != 1 {
			tt.Fatalf("expected to recieve one event. got %d", len(events))
		}
		if totalEvents != 1 {
			tt.Fatalf("expected to recieve a total of one event. got %d", totalEvents)
		}
		if events[0].ID != event1.ID {
			tt.Fatalf("expected to find event1. got %v. want %v", events[0], event1)
		}
	})

	t.Run("FindFilterTags", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event1, _ := store.Create(ctx, &event.NewEvent{Name: "event1", Tags: []string{"foo"}})
		store.Create(ctx, &event.NewEvent{Name: "event2"})
		events, totalEvents, err := store.Find(ctx, &crud.FindParams[event.FindFilters]{
			Filters: &event.FindFilters{Tags: []string{"foo"}},
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(events) != 1 {
			tt.Fatalf("expected to recieve one event. got %d", len(events))
		}
		if totalEvents != 1 {
			tt.Fatalf("expected to recieve a total of one event. got %d", totalEvents)
		}
		if events[0].ID != event1.ID {
			tt.Fatalf("expected to find event1. got %v. want %v", events[0], event1)
		}
	})

	t.Run("FindFilterOwnedBy", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event1, _ := store.Create(ctx, &event.NewEvent{Name: "event1", OwnedBy: []string{"foo"}})
		store.Create(ctx, &event.NewEvent{Name: "event2"})
		events, totalEvents, err := store.Find(ctx, &crud.FindParams[event.FindFilters]{
			Filters: &event.FindFilters{OwnedBy: []string{"foo"}},
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(events) != 1 {
			tt.Fatalf("expected to recieve one event. got %d", len(events))
		}
		if totalEvents != 1 {
			tt.Fatalf("expected to recieve a total of one event. got %d", totalEvents)
		}
		if events[0].ID != event1.ID {
			tt.Fatalf("expected to find event1. got %v. want %v", events[0], event1)
		}
	})

	t.Run("FindFilterLikeName", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event1, _ := store.Create(ctx, &event.NewEvent{Name: "event1"})
		store.Create(ctx, &event.NewEvent{Name: "event2"})
		events, totalEvents, err := store.Find(ctx, &crud.FindParams[event.FindFilters]{
			Filters: &event.FindFilters{LikeName: strptr("1")},
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(events) != 1 {
			tt.Fatalf("expected to recieve one event. got %d", len(events))
		}
		if totalEvents != 1 {
			tt.Fatalf("expected to recieve a total of one event. got %d", totalEvents)
		}
		if events[0].ID != event1.ID {
			tt.Fatalf("expected to find event1. got %v. want %v", events[0], event1)
		}
	})

	t.Run("CreateFindSortedASC", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event1, _ := store.Create(ctx, &event.NewEvent{Name: "c event1"})
		event2, _ := store.Create(ctx, &event.NewEvent{Name: "a event2"})
		event3, _ := store.Create(ctx, &event.NewEvent{Name: "b event3"})
		events, totalEvents, err := store.Find(ctx, &crud.FindParams[event.FindFilters]{
			Sort:  "name",
			Order: crud.OrderAsc,
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(events) != 3 {
			tt.Fatalf("expected to find three event. got %d", len(events))
		}
		if totalEvents != 3 {
			tt.Fatalf("expected to recieve a total of three events. got %d", totalEvents)
		}
		if events[0].ID != event2.ID {
			tt.Fatalf("expected to event[0] to be event2 . got %v", events[0])
		}
		if events[1].ID != event3.ID {
			tt.Fatalf("expected to event[1] to be event3 . got %v", events[1])
		}
		if events[2].ID != event1.ID {
			tt.Fatalf("expected to event[2] to be event1 . got %v", events[2])
		}
	})

	t.Run("CreateFindSortedDESC", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event1, _ := store.Create(ctx, &event.NewEvent{Name: "c event1"})
		event2, _ := store.Create(ctx, &event.NewEvent{Name: "a event2"})
		event3, _ := store.Create(ctx, &event.NewEvent{Name: "b event3"})
		events, totalEvents, err := store.Find(ctx, &crud.FindParams[event.FindFilters]{
			Sort:  "name",
			Order: crud.OrderDesc,
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(events) != 3 {
			tt.Fatalf("expected to find three event. got %d", len(events))
		}
		if totalEvents != 3 {
			tt.Fatalf("expected to recieve a total of three events. got %d", totalEvents)
		}
		if events[0].ID != event1.ID {
			tt.Fatalf("expected to event[0] to be event1 . got %v", events[0])
		}
		if events[1].ID != event3.ID {
			tt.Fatalf("expected to event[1] to be event3 . got %v", events[1])
		}
		if events[2].ID != event2.ID {
			tt.Fatalf("expected to event[2] to be event2 . got %v", events[2])
		}
	})

	t.Run("CreateFindPaginated", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		store.Create(ctx, &event.NewEvent{Name: "event1"})
		event2, _ := store.Create(ctx, &event.NewEvent{Name: "event2"})
		store.Create(ctx, &event.NewEvent{Name: "event3"})
		events, totalEvents, err := store.Find(ctx, &crud.FindParams[event.FindFilters]{
			Limit:  1,
			Offset: 1,
			Sort:   "name",
			Order:  "ASC",
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(events) != 1 {
			tt.Fatalf("expected to find one event. got %d", len(events))
		}
		if totalEvents != 3 {
			tt.Fatalf("expected to recieve a total of three events. got %d", totalEvents)
		}
		if events[0].ID != event2.ID || events[0].Name != event2.Name {
			tt.Fatalf("expected to find event2. got %v. want %v", events[0], event2)
		}
	})

	t.Run("CreateFindPaginatedAndSorted", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event1, _ := store.Create(ctx, &event.NewEvent{Name: "c event1"})
		store.Create(ctx, &event.NewEvent{Name: "a event2"})
		store.Create(ctx, &event.NewEvent{Name: "b event3"})
		events, totalEvents, err := store.Find(ctx, &crud.FindParams[event.FindFilters]{
			Limit:  1,
			Offset: 2,
			Sort:   "name",
			Order:  crud.OrderAsc,
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(events) != 1 {
			tt.Fatalf("expected to find one event. got %d", len(events))
		}
		if totalEvents != 3 {
			tt.Fatalf("expected to recieve a total of three events. got %d", totalEvents)
		}
		if events[0].ID != event1.ID {
			tt.Fatalf("expected to event[0] to be event1 . got %v", events[0])
		}
	})

	t.Run("Update", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event1, _ := store.Create(ctx, &event.NewEvent{Name: "event3"})
		event1.Name = "event3.1"

		_, err = store.Update(ctx, event1)
		if err != nil {
			tt.Fatalf("expected store.Update to succeed. got %s", err.Error())
		}

		event, err := store.FindByID(ctx, event1.ID)
		if event.Name != "event3.1" {
			tt.Fatalf("expected event.Name to be 'event3.1. got %s", event.Name)
		}
	})

	t.Run("UpdateTags", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event1, _ := store.Create(ctx, &event.NewEvent{Name: "event3", Tags: []string{"foo", "bar"}})
		event1.Tags = []string{"foz", "baz"}

		_, err = store.Update(ctx, event1)
		if err != nil {
			tt.Fatalf("expected store.Update to succeed. got %s", err.Error())
		}

		event, err := store.FindByID(ctx, event1.ID)
		if !reflect.DeepEqual(event.Tags, []string{"foz", "baz"}) {
			tt.Fatalf("expected event.Tags to be ['foz', 'baz']. got %v", event.Tags)
		}
	})

	t.Run("UpdateOwnedBy", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event1, _ := store.Create(ctx, &event.NewEvent{Name: "event3", OwnedBy: []string{"foo", "bar"}})
		event1.OwnedBy = []string{"foz", "baz"}

		_, err = store.Update(ctx, event1)
		if err != nil {
			tt.Fatalf("expected store.Update to succeed. got %s", err.Error())
		}

		event, err := store.FindByID(ctx, event1.ID)
		if !reflect.DeepEqual(event.OwnedBy, []string{"foz", "baz"}) {
			tt.Fatalf("expected event.OwnedBy to be ['foz', 'baz']. got %v", event.OwnedBy)
		}
	})

	t.Run("Delete", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		event1, _ := store.Create(ctx, &event.NewEvent{Name: "event1"})
		err = store.Delete(ctx, event1.ID)
		if err != nil {
			tt.Fatalf("expected store.Delete to succeed. got %s", err.Error())
		}
		event, err := store.FindByID(ctx, event1.ID)
		if event != nil {
			tt.Fatal("expected event1 to be deleted")
		}
	})
}

func strptr(str string) *string { return &str }
