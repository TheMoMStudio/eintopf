//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package event_test

import (
	"context"
	"testing"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
	"eintopf.info/service/event"
)

func TestAuthorizer(t *testing.T) {
	store := event.NewMemoryStore()
	e1, _ := store.Create(context.Background(), &event.NewEvent{OwnedBy: []string{"self"}})
	e2, _ := store.Create(context.Background(), &event.NewEvent{OwnedBy: []string{"else"}})
	e3, _ := store.Create(context.Background(), &event.NewEvent{Organizers: []string{"foo", "id:1"}})
	e4, _ := store.Create(context.Background(), &event.NewEvent{Parent: "id:2"})
	roleManager := event.NewAuthorizer(store, groupFakeAuthorizer{})

	for _, test := range []struct {
		description string
		role        string
		fn          func(context.Context) error
		want        error
	}{
		// Create
		{
			"CreateUnauthorized",
			"none",
			func(ctx context.Context) error {
				_, err := roleManager.Create(ctx, &event.NewEvent{})
				return err
			},
			auth.ErrUnauthorized,
		}, {
			"CreateUserAuthorized",
			"user",
			func(ctx context.Context) error {
				_, err := roleManager.Create(ctx, &event.NewEvent{})
				return err
			},
			nil,
		}, {
			"CreateAdminAuthorized",
			"admin",
			func(ctx context.Context) error {
				_, err := roleManager.Create(ctx, &event.NewEvent{})
				return err
			},
			nil,
		},
		// Update
		{
			"UpdateUnauthorized",
			"none",
			func(ctx context.Context) error {
				_, err := roleManager.Update(ctx, e1)
				return err
			},
			auth.ErrUnauthorized,
		}, {
			"UpdateUserUnauthorized",
			"user",
			func(ctx context.Context) error {
				_, err := roleManager.Update(ctx, e2)
				return err
			},
			auth.ErrUnauthorized,
		}, {
			"UpdateAdminAuthorized",
			"user",
			func(ctx context.Context) error {
				_, err := roleManager.Update(ctx, e1)
				return err
			},
			nil,
		}, {
			"UpdateAdminAuthorized",
			"admin",
			func(ctx context.Context) error {
				_, err := roleManager.Update(ctx, e2)
				return err
			},
			nil,
		}, {
			"UpdateOwnedGroupAuthorized",
			"user1",
			func(ctx context.Context) error {
				_, err := roleManager.Update(ctx, e3)
				return err
			},
			nil,
		}, {
			"UpdateNotOwnedGroupUnauthorized",
			"user2",
			func(ctx context.Context) error {
				_, err := roleManager.Update(ctx, e3)
				return err
			},
			auth.ErrUnauthorized,
		}, {
			"UpdateOwnedParent",
			"user1",
			func(ctx context.Context) error {
				_, err := roleManager.Update(ctx, e4)
				return err
			},
			nil,
		},
		// Delete
		{
			"DeleteUnauthorized",
			"none",
			func(ctx context.Context) error {
				e3, _ := store.Create(context.Background(), &event.NewEvent{OwnedBy: []string{"self"}})
				return roleManager.Delete(ctx, e3.ID)
			},
			auth.ErrUnauthorized,
		}, {
			"DeleteUserUnauthorized",
			"user",
			func(ctx context.Context) error {
				e3, _ := store.Create(context.Background(), &event.NewEvent{OwnedBy: []string{"else"}})
				return roleManager.Delete(ctx, e3.ID)
			},
			auth.ErrUnauthorized,
		}, {
			"DeleteAdminAuthorized",
			"user",
			func(ctx context.Context) error {
				e3, _ := store.Create(context.Background(), &event.NewEvent{OwnedBy: []string{"self"}})
				return roleManager.Delete(ctx, e3.ID)
			},
			nil,
		}, {
			"DeleteAdminAuthorized",
			"admin",
			func(ctx context.Context) error {
				e3, _ := store.Create(context.Background(), &event.NewEvent{OwnedBy: []string{"self"}})
				return roleManager.Delete(ctx, e3.ID)
			},
			nil,
		},
		// FindByID
		{
			"FindByIDAuthorized",
			"none",
			func(ctx context.Context) error {
				_, err := roleManager.FindByID(ctx, e1.ID)
				return err
			},
			nil,
		},
		// Find
		{
			"FindAuthorized",
			"none",
			func(ctx context.Context) error {
				_, _, err := roleManager.Find(ctx, &crud.FindParams[event.FindFilters]{})
				return err
			},
			nil,
		},
	} {

		t.Run(test.description, func(tt *testing.T) {
			ctx := context.Background()
			switch test.role {
			case "none":
			case "admin":
				ctx = auth.ContextWithID(ctx, "self")
				ctx = auth.ContextWithRole(ctx, auth.RoleAdmin)
			case "user":
				ctx = auth.ContextWithID(ctx, "self")
				ctx = auth.ContextWithRole(ctx, auth.RoleNormal)
			case "user1":
				ctx = auth.ContextWithID(ctx, "1")
				ctx = auth.ContextWithRole(ctx, auth.RoleNormal)
			case "user2":
				ctx = auth.ContextWithID(ctx, "2")
				ctx = auth.ContextWithRole(ctx, auth.RoleNormal)
			}
			err := test.fn(ctx)
			if test.want != err {
				tt.Fatalf("expected err to be %v, got %v", test.want, err)
			}
		})
	}
}

type groupFakeAuthorizer struct{}

func (g groupFakeAuthorizer) IsOwned(ctx context.Context, groupID string, userID string) bool {
	if groupID == "1" && userID == "1" {
		return true
	}
	return false
}
