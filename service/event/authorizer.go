//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package event

import (
	"context"
	"fmt"
	"strings"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
)

// Authorizer wraps a Storer. It checks the authorization before propagating
// the call to the wrapped storer.
type Authorizer struct {
	store           Storer
	groupAuthorizer authorizer
}

// NewAuthorizer returns a new authorizer limiting access to the provided
// store.
func NewAuthorizer(store Storer, groupAuthorizer authorizer) Storer {
	return &Authorizer{
		store:           store,
		groupAuthorizer: groupAuthorizer,
	}
}

type authorizer interface {
	IsOwned(ctx context.Context, id string, userID string) bool
}

// Create can be called by all users.
func (a *Authorizer) Create(ctx context.Context, newEvent *NewEvent) (*Event, error) {
	_, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return nil, auth.ErrUnauthorized
	}
	return a.store.Create(ctx, newEvent)
}

// Update can be called by an admin user or the event author.
func (a *Authorizer) Update(ctx context.Context, event *Event) (*Event, error) {
	if err := a.adminOrOwned(ctx, event.ID); err != nil {
		return nil, err
	}
	oldEvent, err := a.store.FindByID(ctx, event.ID)
	if err != nil {
		return nil, err
	}
	if oldEvent == nil {
		return nil, fmt.Errorf("cant find event with id: %s", event.ID)
	}
	if oldEvent.Deactivated != event.Deactivated {
		role, err := auth.RoleFromContext(ctx)
		if err != nil {
			return nil, auth.ErrUnauthorized
		}
		if role == auth.RoleNormal {
			return nil, auth.ErrUnauthorized
		}
	}
	return a.store.Update(ctx, event)
}

// Delete can be called by an admin user or the event author.
func (a *Authorizer) Delete(ctx context.Context, id string) error {
	if err := a.adminOrOwned(ctx, id); err != nil {
		return err
	}
	return a.store.Delete(ctx, id)
}

// FindByID can be called by everyone.
func (a *Authorizer) FindByID(ctx context.Context, id string) (*Event, error) {
	return a.store.FindByID(ctx, id)
}

// Find can be called by everyone.
func (a *Authorizer) Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*Event, int, error) {
	return a.store.Find(ctx, params)
}

func (a *Authorizer) adminOrOwned(ctx context.Context, eventID string) error {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return auth.ErrUnauthorized
	}
	if role == auth.RoleAdmin || role == auth.RoleInternal {
		return nil
	}

	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return auth.ErrUnauthorized
	}
	event, err := a.FindByID(ctx, eventID)
	if err != nil {
		return err
	}
	if a.ownesEvent(ctx, userID, event) {
		return nil
	}
	// Check if the user ownes the parent event.
	if event.Parent != "" {
		parentID := strings.TrimPrefix(event.Parent, "id:")
		parentEvent, err := a.FindByID(ctx, parentID)
		if err != nil {
			return err
		}

		if a.ownesEvent(ctx, userID, parentEvent) {
			return nil
		}
	}

	return auth.ErrUnauthorized
}

func (a *Authorizer) ownesEvent(ctx context.Context, userID string, event *Event) bool {
	if event.IsOwned(userID) {
		return true
	}
	// When an organizer is a Group, check if the user ownes it.
	// They then get permissions for this event.
	for _, organizer := range event.Organizers {
		if strings.Contains(organizer, "id:") {
			groupID := strings.TrimPrefix(organizer, "id:")
			if a.groupAuthorizer.IsOwned(ctx, groupID, userID) {
				return true
			}
		}
	}
	return false
}
