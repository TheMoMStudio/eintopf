//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package event_test

import (
	"testing"

	"eintopf.info/service/event"
	"eintopf.info/service/event/testutil"
)

func TestMemoryStore(t *testing.T) {
	testutil.TestStore(t, func() (event.Storer, func(), error) {
		return event.NewMemoryStore(), func() {}, nil
	})
}
