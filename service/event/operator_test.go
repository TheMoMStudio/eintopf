//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package event

import (
	"context"
	"testing"

	"eintopf.info/service/auth"
	"eintopf.info/test"
)

func TestOperatorCreate(t *testing.T) {
	ctx := auth.ContextWithID(context.Background(), "foo")
	queueService := test.NewTestQueue()
	store := NewMemoryStore()
	operator := NewOperator(store, queueService)

	operator.Create(ctx, &NewEvent{Name: "foo"})
	queueService.WasCalled(t, CreateOperation{Event: &Event{ID: "0", Name: "foo"}, userID: "foo"})

	operator.Create(ctx, &NewEvent{Name: "bar", Parent: "id:0"})
	queueService.WasCalled(t, CreateOperation{Event: &Event{ID: "1", Name: "bar", Parent: "id:0"}, userID: "foo"})
	queueService.WasCalled(t, UpdateOperation{Event: &Event{ID: "0", Name: "foo"}, userID: "foo"})
}

func TestOperatorUpdate(t *testing.T) {
	ctx := auth.ContextWithID(context.Background(), "foo")
	queueService := test.NewTestQueue()
	store := NewMemoryStore()
	operator := NewOperator(store, queueService)

	store.Create(ctx, &NewEvent{Name: "foo"})
	operator.Update(ctx, &Event{ID: "0", Name: "bar"})
	queueService.WasCalled(t, UpdateOperation{Event: &Event{ID: "0", Name: "bar"}, userID: "foo"})

	store.Create(ctx, &NewEvent{Name: "parent"})
	operator.Update(ctx, &Event{ID: "0", Name: "bar", Parent: "id:1"})
	queueService.WasCalled(t, UpdateOperation{Event: &Event{ID: "0", Name: "bar", Parent: "id:1"}, userID: "foo"})
	queueService.WasCalled(t, UpdateOperation{Event: &Event{ID: "1", Name: "parent"}, userID: "foo"})
}

func TestOperatorDelete(t *testing.T) {
	ctx := auth.ContextWithID(context.Background(), "foo")
	queueService := test.NewTestQueue()
	store := NewMemoryStore()
	operator := NewOperator(store, queueService)

	store.Create(ctx, &NewEvent{Name: "foo"})
	operator.Delete(ctx, "0")
	queueService.WasCalled(t, DeleteOperation{ID: "0", userID: "foo"})

	store.Create(ctx, &NewEvent{Name: "parent"})
	store.Create(ctx, &NewEvent{Name: "foo", Parent: "id:0"})
	operator.Delete(ctx, "1")
	queueService.WasCalled(t, DeleteOperation{ID: "1", userID: "foo"})
	queueService.WasCalled(t, UpdateOperation{Event: &Event{ID: "0", Name: "parent"}, userID: "foo"})
}
