//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package search

import (
	"fmt"
	"time"

	"github.com/blevesearch/bleve/v2"
	"github.com/blevesearch/bleve/v2/search/query"
)

type Filter interface {
	filterQuery() query.Query
	CacheKey() string
}

// TermsFilter can be used to filter documents containing one of the provided
// terms in the field. If the field contains mutiple terms, only one must match.
type TermsFilter struct {
	Field string   `json:"field"`
	Terms []string `json:"terms"`
}

// CacheKey returns the same string for all identical filters.
func (t *TermsFilter) CacheKey() string {
	cacheKey := t.Field
	for _, term := range t.Terms {
		cacheKey += term
	}
	return cacheKey
}

func (t *TermsFilter) filterQuery() query.Query {
	if len(t.Terms) == 0 {
		return nil
	}
	query := bleve.NewBooleanQuery()
	for _, term := range t.Terms {
		// Note: we use a match phrase query instead of a term query, since the
		// term query is exact and won't match if one of the words/elements
		// match.
		matchQuery := bleve.NewMatchPhraseQuery(term)
		matchQuery.SetField(fieldKey(t.Field))
		query.AddShould(matchQuery)
	}
	return query
}

type NumericRangeFilter struct {
	Field string   `json:"field"`
	Min   *float64 `json:"min"`
	Max   *float64 `json:"max"`
}

// CacheKey returns the same string for all identical filters.
func (n *NumericRangeFilter) CacheKey() string {
	return fmt.Sprintf("%s-%d-%d", n.Field, n.Min, n.Max)
}

func (n *NumericRangeFilter) filterQuery() query.Query {
	query := bleve.NewNumericRangeQuery(n.Min, n.Max)
	query.SetField(fieldKey(n.Field))
	return query
}

type DateRangeFilter struct {
	Field string    `json:"field"`
	Min   time.Time `json:"min"`
	Max   time.Time `json:"max"`
}

// CacheKey returns the same string for all identical filters.
func (d *DateRangeFilter) CacheKey() string {
	return d.Field + d.Min.String() + d.Max.String()
}

func (d *DateRangeFilter) filterQuery() query.Query {
	query := bleve.NewDateRangeQuery(d.Min, d.Max)
	query.SetField(fieldKey(d.Field))
	return query
}

type BoolFilter struct {
	Field string `json:"field"`
	Value bool   `json:"value"`
}

func (b *BoolFilter) CacheKey() string {
	return fmt.Sprintf("%s%t", b.Field, b.Value)
}

func (b *BoolFilter) filterQuery() query.Query {
	query := bleve.NewBoolFieldQuery(b.Value)
	query.SetField(fieldKey(b.Field))
	return query
}

type NotFilter struct {
	Filter Filter
}

func (n *NotFilter) CacheKey() string {
	return fmt.Sprintf("not-%s", n.Filter.CacheKey())
}

func (n *NotFilter) filterQuery() query.Query {
	query := bleve.NewBooleanQuery()
	query.MustNot = n.Filter.filterQuery()

	return query
}
