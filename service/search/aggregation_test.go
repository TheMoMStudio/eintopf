//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package search

import (
	"reflect"
	"testing"
	"time"
)

type aggregatorTestcase struct {
	name       string
	values     []interface{}
	wantError  string
	wantBucket Bucket
}

func TestTermsAggregator(t *testing.T) {
	testcases := []aggregatorTestcase{
		{
			name:      "InvalidType",
			values:    []interface{}{2},
			wantError: "invalid type: int",
		}, {
			name:       "TypeString",
			values:     []interface{}{"a"},
			wantBucket: TermsBucket{Term{Term: "a", Count: 1}},
		}, {
			name:       "Type[]string",
			values:     []interface{}{[]string{"a"}},
			wantBucket: TermsBucket{Term{Term: "a", Count: 1}},
		}, {
			name:       "Type[]interface{}",
			values:     []interface{}{[]interface{}{"a"}},
			wantBucket: TermsBucket{Term{Term: "a", Count: 1}},
		}, {
			name:   "IncreasesCount",
			values: []interface{}{"a", "a", "b"},
			wantBucket: TermsBucket{
				Term{Term: "a", Count: 2},
				Term{Term: "b", Count: 1},
			},
		},
	}
	testAggregator(t, testcases, func() aggregator {
		return &termsAggregator{make(map[string]int)}
	})
}

func TestDateAggregator(t *testing.T) {
	time1 := time.Date(2019, 1, 12, 20, 0, 0, 0, time.UTC)
	time2 := time.Date(2019, 2, 12, 20, 0, 0, 0, time.UTC)
	time3 := time.Date(2019, 7, 12, 20, 0, 0, 0, time.UTC)
	testcases := []aggregatorTestcase{
		{
			name:      "InvalidType",
			values:    []interface{}{2},
			wantError: "invalid type: int",
		}, {
			name:      "InvalidDate",
			values:    []interface{}{"a"},
			wantError: `invalid date format: parsing time "a" as "2006-01-02T15:04:05Z07:00": cannot parse "a" as "2006"`,
		}, {
			name: "2Dates",
			values: []interface{}{
				time1.Format(DateLayout),
				time2.Format(DateLayout),
			},
			wantBucket: DateRangeBucket{
				Min: time1,
				Max: time2,
			},
		}, {
			name: "3Dates",
			values: []interface{}{
				time2.Format(DateLayout),
				time3.Format(DateLayout),
				time1.Format(DateLayout),
			},
			wantBucket: DateRangeBucket{
				Min: time1,
				Max: time3,
			},
		},
	}

	testAggregator(t, testcases, func() aggregator {
		return &dateRangeAggregator{
			min: time.Date(9999, 0, 0, 0, 0, 0, 0, time.UTC),
			max: time.Date(0, 0, 0, 0, 0, 0, 0, time.UTC),
		}
	})
}

func testAggregator(t *testing.T, testcases []aggregatorTestcase, newAggregator func() aggregator) {
	t.Helper()

	for _, tc := range testcases {
		t.Run(tc.name, func(tt *testing.T) {
			aggregator := newAggregator()
			for _, v := range tc.values {
				err := aggregator.aggregate(v)
				if tc.wantError != "" {
					if err.Error() != tc.wantError {
						tt.Fatalf("aggregator.aggregate(%v): error doesn't match:\nwant: %s\ngot:  %s", v, tc.wantError, err.Error())
					}
					return
				}
				if err != nil {
					tt.Fatalf("aggregator.aggregate(%v): failed unexpectedly: %s", v, err.Error())
				}
			}

			bucket := aggregator.bucket()
			if !reflect.DeepEqual(bucket, tc.wantBucket) {
				tt.Fatalf("bucket doesn't match:\nwant: %v\ngot:  %v", tc.wantBucket, bucket)
			}
		})
	}
}

func TestAggregationCacheKey(t *testing.T) {
	for _, tc := range []struct {
		a1        Aggregation
		a2        Aggregation
		wantEqual bool
	}{
		{
			a1: Aggregation{
				Type:  TermsAggregation,
				Field: "foo",
				Filters: []Filter{
					&TermsFilter{Field: "bar", Terms: []string{"a", "b"}},
					&DateRangeFilter{Field: "baz", Min: time.Unix(1000000, 0), Max: time.Unix(1000, 0)},
				},
			},
			a2: Aggregation{
				Type:  TermsAggregation,
				Field: "foo",
				Filters: []Filter{
					&TermsFilter{Field: "bar", Terms: []string{"a", "b"}},
					&DateRangeFilter{Field: "baz", Min: time.Unix(1000000, 0), Max: time.Unix(1000, 0)},
				},
			},
			wantEqual: true,
		}, {
			a1:        Aggregation{Type: TermsAggregation},
			a2:        Aggregation{Type: DateRangeAggregation},
			wantEqual: false,
		}, {
			a1:        Aggregation{Field: "foo"},
			a2:        Aggregation{Field: "bar"},
			wantEqual: false,
		}, {
			a1:        Aggregation{Filters: []Filter{&TermsFilter{Field: "foo"}}},
			a2:        Aggregation{Filters: []Filter{&TermsFilter{Field: "bar"}}},
			wantEqual: false,
		},
	} {
		c1 := tc.a1.CacheKey()
		c2 := tc.a2.CacheKey()

		if tc.wantEqual && c1 != c2 {
			t.Errorf("cache keys are unequal: %v == %v: %s != %s", tc.a1, tc.a2, c1, c2)
		}
		if !tc.wantEqual && c1 == c2 {
			t.Errorf("cache keys are equal: %v != %v: %s == %s", tc.a1, tc.a2, c1, c2)
		}
	}
}
