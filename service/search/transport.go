//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package search

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/xhttp"
)

// Router returns an http router for the search service.
func Router(service Service) func(chi.Router) {
	server := &server{service}
	return func(r chi.Router) {
		r.Options("/", xhttp.CorsHandler)

		// swagger:route GET /search search search
		//
		// Perfomes a search.
		//
		//     Responses:
		//       200: searchResponse
		//       400: badRequest
		//       500: internalError
		//       503: serviceUnavailable
		r.With(xhttp.LastModified(service)).Get("/", server.search)
	}
}

// swagger:response searchResponse
type searchResponse struct {
	// in:body
	Result Result
}

type server struct {
	service Service
}

func (s *server) search(w http.ResponseWriter, r *http.Request) {
	req, err := readSearchRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, fmt.Errorf("failed to read search request: %s", err))
		return
	}

	result, err := s.service.Search(r.Context(), req)
	if err != nil {
		xhttp.WriteError(r.Context(), w, fmt.Errorf("failed to search: %s", err))
		return
	}

	data, err := json.Marshal(result)
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, err)
	}
	w.Write(data)
}

func readSearchRequest(r *http.Request) (*Options, error) {
	var err error

	opts := &Options{}

	opts.Query, err = xhttp.ReadQueryString(r, "query")
	if err != nil {
		return nil, err
	}

	opts.Sort, err = xhttp.ReadQueryString(r, "sort")
	if err != nil {
		return nil, err
	}

	opts.Page, err = xhttp.ReadQueryInt(r, "page")
	if err != nil {
		return nil, err
	}

	opts.PageSize, err = xhttp.ReadQueryInt(r, "pageSize")
	if err != nil {
		return nil, err
	}

	var filters []filterRequest
	err = xhttp.ReadQueryJson(r, "filters", &filters)
	if err != nil {
		return nil, err
	}
	opts.Filters, err = filtersFromFilterRequests(filters)

	var aggregations map[string]aggregationRequest
	err = xhttp.ReadQueryJson(r, "aggregations", &aggregations)
	if err != nil {
		return nil, err
	}
	opts.Aggregations, err = aggregationsFromAggregationRequests(aggregations)

	return opts, nil
}

type filterRequest struct {
	Type   string          `json:"type"`
	Filter json.RawMessage `json:"filter"`
}

func filtersFromFilterRequests(filterRequests []filterRequest) ([]Filter, error) {
	var filters []Filter
	for _, filterRequest := range filterRequests {
		switch filterRequest.Type {
		case "terms":
			var filter TermsFilter
			err := json.Unmarshal(filterRequest.Filter, &filter)
			if err != nil {
				return nil, err
			}
			filters = append(filters, &filter)
		case "daterange":
			var filter DateRangeFilter
			err := json.Unmarshal(filterRequest.Filter, &filter)
			if err != nil {
				return nil, err
			}
			filters = append(filters, &filter)
		case "bool":
			var filter BoolFilter
			err := json.Unmarshal(filterRequest.Filter, &filter)
			if err != nil {
				return nil, err
			}
			filters = append(filters, &filter)
		}
	}
	return filters, nil
}

type aggregationRequest struct {
	Type    string          `json:"type"`
	Field   string          `json:"field"`
	Filters []filterRequest `json:"filters"`
}

func aggregationsFromAggregationRequests(aggregationRequests map[string]aggregationRequest) (map[string]Aggregation, error) {
	aggregations := make(map[string]Aggregation, len(aggregationRequests))
	for key, aggregationRequest := range aggregationRequests {
		filters, err := filtersFromFilterRequests(aggregationRequest.Filters)
		if err != nil {
			return nil, err
		}
		switch aggregationRequest.Type {
		case "terms":
			aggregations[key] = Aggregation{
				Type:    TermsAggregation,
				Field:   aggregationRequest.Field,
				Filters: filters,
			}
		case "daterange":
			aggregations[key] = Aggregation{
				Type:    DateRangeAggregation,
				Field:   aggregationRequest.Field,
				Filters: filters,
			}
		}
	}
	return aggregations, nil
}
