//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package invitation

import (
	"context"

	"eintopf.info/service/auth"
	"eintopf.info/service/user"
)

type authorizer struct {
	service Service
}

func NewAuthorizer(service Service) Service {
	return &authorizer{service}
}

func (a *authorizer) Create(ctx context.Context, invitation *NewInvitation) (*Invitation, error) {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return nil, auth.ErrUnauthorized
	}
	if role != auth.RoleAdmin && role != auth.RoleInternal {
		return nil, auth.ErrUnauthorized
	}
	return a.service.Create(ctx, invitation)
}
func (a *authorizer) Update(ctx context.Context, invitation *Invitation) (*Invitation, error) {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return nil, auth.ErrUnauthorized
	}
	if role != auth.RoleAdmin && role != auth.RoleInternal {
		return nil, auth.ErrUnauthorized
	}
	return a.service.Update(ctx, invitation)
}
func (a *authorizer) Delete(ctx context.Context, id string) error {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return auth.ErrUnauthorized
	}
	if role != auth.RoleAdmin && role != auth.RoleInternal {
		return auth.ErrUnauthorized
	}
	return a.service.Delete(ctx, id)
}
func (a *authorizer) FindByID(ctx context.Context, id string) (*Invitation, error) {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return nil, auth.ErrUnauthorized
	}
	if role != auth.RoleAdmin && role != auth.RoleInternal {
		userID, err := auth.UserIDFromContext(ctx)
		if err != nil {
			return nil, auth.ErrUnauthorized
		}
		if userID != id {
			return nil, auth.ErrUnauthorized
		}
	}
	return a.service.FindByID(ctx, id)
}
func (a *authorizer) Find(ctx context.Context, params *FindParams) ([]Invitation, int, error) {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return nil, 0, auth.ErrUnauthorized
	}
	invitations, total, err := a.service.Find(ctx, params)
	if err != nil {
		return nil, 0, err
	}
	if role != auth.RoleAdmin && role != auth.RoleModerator && role != auth.RoleInternal {
		userID, err := auth.UserIDFromContext(ctx)
		if err != nil {
			return nil, 0, auth.ErrUnauthorized
		}
		filteredInvitations := []Invitation{}
		for _, invitation := range invitations {
			if invitation.ID == userID {
				filteredInvitations = append(filteredInvitations, invitation)
			}
		}
		invitations = filteredInvitations
	}

	return invitations, total, nil
}

func (a *authorizer) Invite(ctx context.Context) (token string, err error) {
	_, err = auth.UserIDFromContext(ctx)
	if err != nil {
		return "", auth.ErrUnauthorized
	}
	return a.service.Invite(ctx)
}
func (a *authorizer) UseInvite(ctx context.Context, token string, user *user.NewUser) error {
	return a.service.UseInvite(ctx, token, user)
}
