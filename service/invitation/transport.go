//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package invitation

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/xhttp"
	"eintopf.info/service/auth"
	"eintopf.info/service/user"
)

// Router returns a new http router that handles requests for a given Service.
func Router(service Service, authService auth.Service) func(chi.Router) {
	server := &server{service}
	return func(r chi.Router) {
		r.Options("/", xhttp.CorsHandler)

		// swagger:route GET /invitations/ invitation findInvitations
		//
		// Retrieves all invitations.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: findInvitationsResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: false})).
			Get("/", server.find)

		// swagger:route POST /invitations/ invitation createInvitation
		//
		// Creates a new invitation with the given data.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: createInvitationResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Post("/", server.create)

		r.Options("/{id}", xhttp.CorsHandler)

		// swagger:route GET /invitations/{id} invitation findInvitation
		//
		// Finds the invitation with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: findInvitationResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.Get("/{id}", server.findByID)

		// swagger:route PUT /invitations/{id} invitation updateInvitation
		//
		// Updates the invitation with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: updateInvitationResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Put("/{id}", server.update)

		// swagger:route DELETE /invitations/{id} invitation deleteInvitation
		//
		// Deletes the invitation with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: deleteInvitationResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Delete("/{id}", server.delete)

		r.Options("/invite", xhttp.CorsHandler)

		// swagger:route POST /invitations/invite invitation invite
		//
		// Creates a new invite.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: inviteResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Post("/invite", server.invite)

		r.Options("/invite/{token}", xhttp.CorsHandler)

		// swagger:route POST /invitations/invite/{token} invitation use
		//
		// Uses an invite
		//
		//     Responses:
		//       200: useResponse
		//       400: badRequest
		//       500: internalError
		//       503: serviceUnavailable
		r.Post("/invite/{token}", server.use)
	}
}

type server struct {
	service Service
}

func (s *server) find(w http.ResponseWriter, r *http.Request) {
	params, err := readFindRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	invitations, total, err := s.service.Find(r.Context(), params)
	if err == auth.ErrUnauthorized {
		xhttp.WriteUnauthorized(w, err)
		return
	}
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, err)
		return
	}
	err = writeFindResponse(w, &findResponse{Invitations: invitations, Total: total})
	if err != nil {
		log.Println(err)
		return
	}
}

// swagger:response findInvitationsResponse
type findResponse struct {

	// in:body
	Invitations []Invitation

	// in:header
	Total int `json:"x-total-count"`
}

func readFindRequest(r *http.Request) (*FindParams, error) {
	params := &FindParams{}
	var err error

	params.Offset, err = xhttp.ReadQueryInt64(r, "offset")
	if err != nil {
		return nil, err
	}
	params.Limit, err = xhttp.ReadQueryInt64(r, "limit")
	if err != nil {
		return nil, err
	}

	params.Sort, err = xhttp.ReadQueryString(r, "sorting")
	if err != nil {
		return nil, err
	}
	order, err := xhttp.ReadQueryString(r, "order")
	if err != nil {
		return nil, err
	}
	params.Order = SortOrder(order)

	filters := &FindFilters{}
	err = xhttp.ReadQueryJson(r, "filters", filters)
	if err != nil {
		return nil, err
	}
	params.Filters = filters

	return params, nil
}

func writeFindResponse(w http.ResponseWriter, resp *findResponse) error {
	data, err := json.Marshal(resp.Invitations)
	if err != nil {
		return err
	}
	w.Header().Add("Access-Control-Expose-Headers", "X-Total-Count")
	w.Header().Add("X-Total-Count", strconv.Itoa(resp.Total))
	w.Write(data)
	return nil
}

func (s *server) create(w http.ResponseWriter, r *http.Request) {
	req, err := readCreateRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	invitation, err := s.service.Create(r.Context(), req.Invitation)
	if err == auth.ErrUnauthorized {
		xhttp.WriteUnauthorized(w, err)
		return
	}
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, err)
		return
	}
	err = writeCreateResponse(w, &createResponse{invitation})
	if err != nil {
		log.Println(err)
		return
	}
}

// swagger:parameters createInvitation
type createRequest struct {

	// in:body
	Invitation *NewInvitation
}

// swagger:response createInvitationResponse
type createResponse struct {

	// in:body
	Invitation *Invitation
}

func readCreateRequest(r *http.Request) (*createRequest, error) {
	data, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	invitation := &NewInvitation{}
	err = json.Unmarshal(data, invitation)
	if err != nil {
		return nil, err
	}
	return &createRequest{invitation}, nil
}

func writeCreateResponse(w http.ResponseWriter, resp *createResponse) error {
	data, err := json.Marshal(resp.Invitation)
	if err != nil {
		return err
	}
	w.Write(data)
	return nil
}

func (s *server) findByID(w http.ResponseWriter, r *http.Request) {
	req, err := readFindByIDRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	invitation, err := s.service.FindByID(r.Context(), req.ID)
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, err)
		return
	}
	err = writeFindByIDResponse(w, &findByIDResponse{invitation})
	if err != nil {
		log.Println(err)
		return
	}
}

// swagger:parameters findInvitation
type findByIDRequest struct {
	// in:query
	ID string `json:"id"`
}

// swagger:response findInvitationResponse
type findByIDResponse struct {

	// in:body
	Invitation *Invitation
}

func readFindByIDRequest(r *http.Request) (*findByIDRequest, error) {
	id := chi.URLParam(r, "id")
	return &findByIDRequest{id}, nil
}

func writeFindByIDResponse(w http.ResponseWriter, resp *findByIDResponse) error {
	data, err := json.Marshal(resp.Invitation)
	if err != nil {
		return err
	}
	w.Write(data)
	return nil
}

func (s *server) update(w http.ResponseWriter, r *http.Request) {
	req, err := readUpdateRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	invitation, err := s.service.Update(r.Context(), req.Invitation)
	if err == auth.ErrUnauthorized {
		xhttp.WriteUnauthorized(w, err)
		return
	}
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, err)
		return
	}
	err = writeUpdateResponse(w, &updateResponse{invitation})
	if err != nil {
		log.Println(err)
		return
	}
}

// swagger:parameters updateInvitation
type updateRequest struct {

	// in:body
	Invitation *Invitation
}

// swagger:response updateInvitationResponse
type updateResponse struct {

	// in:body
	Invitation *Invitation
}

func readUpdateRequest(r *http.Request) (*updateRequest, error) {
	data, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	invitation := &Invitation{}
	err = json.Unmarshal(data, invitation)
	if err != nil {
		return nil, err
	}
	return &updateRequest{invitation}, nil
}

func writeUpdateResponse(w http.ResponseWriter, resp *updateResponse) error {
	data, err := json.Marshal(resp.Invitation)
	if err != nil {
		return err
	}
	w.Write(data)
	return nil
}

func (s *server) delete(w http.ResponseWriter, r *http.Request) {
	req, err := readDeleteRequest(r)
	if err != nil {

		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	err = s.service.Delete(r.Context(), req.ID)
	if err == auth.ErrUnauthorized {
		xhttp.WriteUnauthorized(w, err)
		return
	}
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, err)
		return
	}
	w.Write([]byte{})
}

// swagger:parameters deleteInvitation
type deleteRequest struct {

	// in:query
	ID string `json:"id"`
}

// swagger:response deleteInvitationResponse
type deleteResponse struct{}

func readDeleteRequest(r *http.Request) (*deleteRequest, error) {
	id := chi.URLParam(r, "id")
	return &deleteRequest{id}, nil
}

func writeDeleteResponse(w http.ResponseWriter) error {
	w.Write([]byte{})
	return nil
}

func (s *server) invite(w http.ResponseWriter, r *http.Request) {
	token, err := s.service.Invite(r.Context())
	if err == auth.ErrUnauthorized {
		xhttp.WriteUnauthorized(w, err)
		return
	}
	if err == ErrInvitationLimit || err == ErrNoInvitationYet {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, err)
		return
	}
	err = writeInviteResponse(w, &inviteResponse{&InviteResponse{token}})
	if err != nil {
		log.Println(err)
		return
	}
}

// swagger:parameters invite
type inviteRequest struct {
}

// swagger:response inviteResponse
type inviteResponse struct {
	// in:body
	Body *InviteResponse
}

type InviteResponse struct {
	Token string `json:"token"`
}

func writeInviteResponse(w http.ResponseWriter, resp *inviteResponse) error {
	data, err := json.Marshal(resp.Body)
	if err != nil {
		return err
	}
	w.Write(data)
	return nil
}

func (s *server) use(w http.ResponseWriter, r *http.Request) {
	req, err := readUseRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	err = s.service.UseInvite(r.Context(), req.Token, req.User)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	w.Write([]byte(""))
}

// swagger:parameters use
type useRequest struct {
	// in:query
	Token string `json:"token"`

	// in:body
	User *user.NewUser
}

// swagger:response useResponse
type useResponse struct{}

func readUseRequest(r *http.Request) (*useRequest, error) {
	token := chi.URLParam(r, "token")

	data, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	user := &user.NewUser{}
	err = json.Unmarshal(data, user)
	if err != nil {
		return nil, err
	}
	return &useRequest{Token: token, User: user}, nil
}
