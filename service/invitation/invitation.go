//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package invitation

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"strconv"
	"time"

	"eintopf.info/internal/xerror"
	"eintopf.info/service/auth"
	"eintopf.info/service/user"
)

// Service defines an invitation services, that can handle crud operations on
// the Invitation model
type Service interface {
	Storer
	Invite(ctx context.Context) (token string, err error)
	UseInvite(ctx context.Context, token string, user *user.NewUser) error
}

// Invitation is an invitation model.
type Invitation struct {
	ID        string    `json:"id" db:"id"`
	Token     string    `json:"token" db:"token"`
	CreatedAt time.Time `json:"createdAt" db:"created_at"`
	CreatedBy string    `json:"createdBy" db:"created_by"`
	UsedBy    string    `json:"usedBy" db:"used_by"`
}

// NewInvitation is the model used to create new invitations by the storer.
type NewInvitation struct {
	Token     string    `json:"token"`
	CreatedAt time.Time `json:"createdAt"`
	CreatedBy string    `json:"createdBy"`
}

// InvitationFromNewInvitation converts a NewInvitation to an Invitation with a
// given id.
func InvitationFromNewInvitation(newInvitation *NewInvitation, id string) *Invitation {
	return &Invitation{
		ID:        id,
		Token:     newInvitation.Token,
		CreatedAt: newInvitation.CreatedAt,
		CreatedBy: newInvitation.CreatedBy,
	}
}

// Storer defines an interface for crud operations on the invitation model.
type Storer interface {
	Create(ctx context.Context, invitation *NewInvitation) (*Invitation, error)
	Update(ctx context.Context, invitation *Invitation) (*Invitation, error)
	Delete(ctx context.Context, id string) error
	FindByID(ctx context.Context, id string) (*Invitation, error)
	Find(ctx context.Context, params *FindParams) ([]Invitation, int, error)
}

// SortOrder defines the order of sorting.
type SortOrder string

// Possible values for SortOrder.
const (
	OrderAsc  = SortOrder("ASC")
	OrderDesc = SortOrder("DESC")
)

// FindParams defines parameters used by the Find method.
type FindParams struct {
	Offset int64
	Limit  int64

	Sort  string
	Order SortOrder

	Filters *FindFilters
}

// FindFilters defines the possible filters for the find method.
type FindFilters struct {
	ID        *string
	Token     *string
	CreatedAt *time.Time
	CreatedBy *string
	UsedBy    *string
}

type service struct {
	store       Storer
	userService user.Service
}

func NewService(store Storer, userService user.Service) Service {
	return &service{store: store, userService: userService}
}

func (s *service) Create(ctx context.Context, invitation *NewInvitation) (*Invitation, error) {
	return s.store.Create(ctx, invitation)
}
func (s *service) Update(ctx context.Context, invitation *Invitation) (*Invitation, error) {

	return s.store.Update(ctx, invitation)
}
func (s *service) Delete(ctx context.Context, id string) error {
	invitation, err := s.store.FindByID(ctx, id)
	if err != nil {
		return err
	}
	if invitation.UsedBy != "" {
		return fmt.Errorf("can not delete used invitation")
	}
	return s.store.Delete(ctx, id)
}
func (s *service) FindByID(ctx context.Context, id string) (*Invitation, error) {

	return s.store.FindByID(ctx, id)
}
func (s *service) Find(ctx context.Context, params *FindParams) ([]Invitation, int, error) {

	return s.store.Find(ctx, params)
}

var ErrNoInvitationYet = fmt.Errorf("no invitation yet")
var ErrInvitationLimit = fmt.Errorf("invitation limit")

func (s *service) Invite(ctx context.Context) (token string, err error) {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return "", err
	}
	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return "", fmt.Errorf("invalid inviter")
	}

	if role == auth.RoleNormal {
		user, err := s.userService.FindByID(ctx, userID)
		if err != nil {
			return "", err
		}
		if time.Now().Sub(user.CreatedAt) < time.Hour*24*7 {
			return "", ErrNoInvitationYet
		}

		invitations, _, err := s.store.Find(ctx, &FindParams{
			Filters: &FindFilters{CreatedBy: &userID},
		})
		if err != nil {
			return "", err
		}

		lastInvitationAt := time.Date(2000, 0, 0, 0, 0, 0, 0, time.UTC)
		for _, invitation := range invitations {
			if invitation.CreatedAt.After(lastInvitationAt) {
				lastInvitationAt = invitation.CreatedAt
			}
		}
		if time.Now().Sub(lastInvitationAt) < time.Hour*24*7 {
			return "", ErrInvitationLimit
		}
	}

	h := sha256.New()
	h.Write([]byte(userID + strconv.FormatInt(time.Now().Unix(), 10)))
	sum := h.Sum(nil)
	dst := make([]byte, hex.EncodedLen(len(sum)))
	hex.Encode(dst, sum)

	invitation := &NewInvitation{
		Token:     string(dst),
		CreatedAt: time.Now(),
		CreatedBy: userID,
	}
	_, err = s.store.Create(auth.ContextWithRole(ctx, auth.RoleInternal), invitation)
	if err != nil {
		return "", err
	}

	return invitation.Token, nil
}

var ErrInvalidInvitationToken = xerror.BadInputError{Err: fmt.Errorf("invalid invitation token")}
var ErrInvitationTokenAlreadyUsed = xerror.BadInputError{Err: fmt.Errorf("invitation token already used")}

func (s *service) UseInvite(ctx context.Context, token string, user *user.NewUser) error {
	invitations, _, err := s.store.Find(auth.ContextWithRole(ctx, auth.RoleInternal), &FindParams{
		Filters: &FindFilters{Token: &token},
	})
	if err != nil || len(invitations) != 1 {
		return ErrInvalidInvitationToken
	}
	invitation := invitations[0]
	if invitation.UsedBy != "" {
		return ErrInvitationTokenAlreadyUsed
	}
	user.Role = auth.RoleNormal

	newUser, err := s.userService.Create(auth.ContextWithRole(ctx, auth.RoleInternal), user)
	if err != nil {
		return err
	}

	invitation.UsedBy = newUser.ID

	_, err = s.store.Update(auth.ContextWithRole(ctx, auth.RoleInternal), &invitation)
	if err != nil {
		return err
	}
	return nil
}
