//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package group_test

import (
	"fmt"
	"testing"

	"eintopf.info/service/dbmigration"
	"eintopf.info/service/group"
	"eintopf.info/service/group/testutil"
	"eintopf.info/test"
)

func TestSqlStore(t *testing.T) {
	testutil.TestStore(t, func() (group.Storer, func(), error) {
		db, cleanup, err := test.CreateSqliteTestDB(t.Name())
		if err != nil {
			return nil, nil, err
		}

		migrationStore, err := dbmigration.NewSqlStore(db)
		if err != nil {
			return nil, cleanup, fmt.Errorf("failed to create migration store: %s", err)
		}
		migrationService := dbmigration.NewService(migrationStore)
		store, err := group.NewSqlStore(db, migrationService)
		if err != nil {
			return nil, cleanup, fmt.Errorf("failed to run migrations: %s", err)
		}
		return store, cleanup, nil
	})
}
