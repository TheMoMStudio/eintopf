//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package group

import "eintopf.info/internal/crud"

// NewMemoryStore returns a new in memory store.
func NewMemoryStore() *crud.MemoryStore[NewGroup, Group, FindFilters] {
	return crud.NewMemoryStore[NewGroup, Group, FindFilters](GroupFromNewGroup, nil)
}
