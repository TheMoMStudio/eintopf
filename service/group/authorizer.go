//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package group

import (
	"context"
	"fmt"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
)

type Authorizer struct {
	service Service
}

func NewAuthorizer(service Service) *Authorizer {
	return &Authorizer{service}
}

// Create can only be called by loggedin users.
func (a *Authorizer) Create(ctx context.Context, newGroup *NewGroup) (*Group, error) {
	_, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return nil, auth.ErrUnauthorized
	}
	return a.service.Create(ctx, newGroup)
}

// Update can only be called
// - if the loggedin user is in the OwnedBy field
// - if the loggedin user is a moderator or admin
func (a *Authorizer) Update(ctx context.Context, group *Group) (*Group, error) {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return nil, auth.ErrUnauthorized
	}
	if role == auth.RoleNormal {
		userID, err := auth.UserIDFromContext(ctx)
		if err != nil {
			return nil, auth.ErrUnauthorized
		}
		if !group.IsOwned(userID) {
			return nil, auth.ErrUnauthorized
		}
		oldGroup, err := a.service.FindByID(ctx, group.ID)
		if err != nil {
			return nil, err
		}
		if oldGroup == nil {
			return nil, fmt.Errorf("cant find group with id: %s", group.ID)
		}
		if oldGroup.Deactivated != group.Deactivated {
			return nil, auth.ErrUnauthorized
		}
	}
	return a.service.Update(ctx, group)
}

// Update can only be called
// - if the loggedin user is in the OwnedBy field
// - if the loggedin user is an admin
func (a *Authorizer) Delete(ctx context.Context, id string) error {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return auth.ErrUnauthorized
	}
	if role == auth.RoleNormal || role == auth.RoleModerator {
		userID, err := auth.UserIDFromContext(ctx)
		if err != nil {
			return auth.ErrUnauthorized
		}
		group, err := a.service.FindByID(ctx, id)
		if err != nil {
			return err
		}
		if group == nil {
			return nil
		}
		if !group.IsOwned(userID) {
			return auth.ErrUnauthorized
		}
	}
	return a.service.Delete(ctx, id)
}

func (a *Authorizer) FindByID(ctx context.Context, id string) (*Group, error) {
	return a.service.FindByID(ctx, id)
}

func (a *Authorizer) Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*Group, int, error) {
	return a.service.Find(ctx, params)
}

func (a *Authorizer) IsOwned(ctx context.Context, groupID string, userID string) bool {
	group, err := a.service.FindByID(ctx, groupID)
	if err != nil {
		return false
	}
	return group.IsOwned(userID)
}

func (a *Authorizer) Owners(ctx context.Context, groupID string) ([]string, error) {
	group, err := a.service.FindByID(ctx, groupID)
	if err != nil {
		return nil, err
	}
	return group.OwnedBy, nil
}
