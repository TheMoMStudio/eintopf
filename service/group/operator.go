//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package group

import (
	"context"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
	"eintopf.info/service/oqueue"
)

type operator struct {
	service Service
	queue   *oqueue.Queue
}

func NewOperator(service Service, queue *oqueue.Queue) Service {
	return &operator{service: service, queue: queue}
}

type CreateOperation struct {
	Group  *Group
	userID string
}

func (o CreateOperation) UserID() string {
	return o.userID
}

func (i *operator) Create(ctx context.Context, newGroup *NewGroup) (*Group, error) {
	group, err := i.service.Create(ctx, newGroup)
	if err != nil {
		return group, err
	}

	if group != nil {
		userID, err := auth.UserIDFromContext(ctx)
		if err != nil {
			return nil, err
		}
		i.queue.AddOperation(CreateOperation{
			Group:  group,
			userID: userID,
		})
	}

	return group, nil
}

type UpdateOperation struct {
	Group  *Group
	userID string
}

func (o UpdateOperation) UserID() string {
	return o.userID
}

func (i *operator) Update(ctx context.Context, group *Group) (*Group, error) {
	group, err := i.service.Update(ctx, group)
	if err != nil {
		return group, err
	}

	if group != nil {
		userID, err := auth.UserIDFromContext(ctx)
		if err != nil {
			return nil, err
		}
		i.queue.AddOperation(UpdateOperation{
			Group:  group,
			userID: userID,
		})
	}

	return group, nil
}

type DeleteOperation struct {
	ID     string
	userID string
}

func (o DeleteOperation) UserID() string {
	return o.userID
}

func (i *operator) Delete(ctx context.Context, id string) error {
	err := i.service.Delete(ctx, id)
	if err != nil {
		return err
	}

	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return err
	}
	i.queue.AddOperation(DeleteOperation{
		ID:     id,
		userID: userID,
	})

	return nil
}

func (i *operator) FindByID(ctx context.Context, id string) (*Group, error) {
	return i.service.FindByID(ctx, id)
}

func (i *operator) Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*Group, int, error) {
	return i.service.Find(ctx, params)
}
