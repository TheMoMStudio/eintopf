//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package group_test

import (
	"context"
	"fmt"
	"testing"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
	"eintopf.info/service/group"
)

func TestGroupIndexable(t *testing.T) {
	for i, test := range []struct {
		group     *group.Group
		indexable bool
	}{
		{
			group:     &group.Group{Deactivated: true, Published: true},
			indexable: false,
		}, {
			group:     &group.Group{Deactivated: false, Published: true},
			indexable: true,
		}, {
			group:     &group.Group{Deactivated: false, Published: false},
			indexable: true,
		},
	} {
		t.Run(fmt.Sprintf("%d", i), func(tt *testing.T) {
			if test.group.Indexable() != test.indexable {
				tt.Errorf("%d: Deactivated: %t Published: %t -> Indexable %t", i, test.group.Deactivated, test.group.Published, test.group.Indexable())
			}
		})
	}
}

func TestGroupListable(t *testing.T) {
	for i, test := range []struct {
		group    *group.Group
		listable bool
	}{
		{
			group:    &group.Group{Deactivated: true, Published: true},
			listable: true,
		}, {
			group:    &group.Group{Deactivated: false, Published: true},
			listable: true,
		}, {
			group:    &group.Group{Deactivated: false, Published: false},
			listable: false,
		},
	} {
		t.Run(fmt.Sprintf("%d", i), func(tt *testing.T) {
			if test.group.Listable() != test.listable {
				tt.Errorf("%d: Deactivated: %t Published: %t -> Listable %t", i, test.group.Deactivated, test.group.Published, test.group.Listable())
			}
		})
	}
}

func TestGroupIsOwned(t *testing.T) {
	for _, test := range []struct {
		name  string
		group *group.Group
		id    string
		owned bool
	}{
		{
			name:  "Owned",
			group: &group.Group{OwnedBy: []string{"foo", "bar"}},
			id:    "foo",
			owned: true,
		}, {
			name:  "NotOwned",
			group: &group.Group{OwnedBy: []string{"bar"}},
			id:    "foo",
			owned: false,
		},
	} {
		t.Run(test.name, func(tt *testing.T) {
			if test.group.IsOwned(test.id) != test.owned {
				tt.Errorf("test.group.IsOwned(%s) != test.owned: %t != %t", test.id, test.group.IsOwned(test.id), test.owned)
			}
		})
	}
}

func TestNewGroupIsOwned(t *testing.T) {
	for _, test := range []struct {
		name  string
		group *group.NewGroup
		id    string
		owned bool
	}{
		{
			name:  "Owned",
			group: &group.NewGroup{OwnedBy: []string{"foo", "bar"}},
			id:    "foo",
			owned: true,
		}, {
			name:  "NotOwned",
			group: &group.NewGroup{OwnedBy: []string{"bar"}},
			id:    "foo",
			owned: false,
		},
	} {
		t.Run(test.name, func(tt *testing.T) {
			if test.group.IsOwned(test.id) != test.owned {
				tt.Errorf("test.group.IsOwned(%s) != test.owned: %t != %t", test.id, test.group.IsOwned(test.id), test.owned)
			}
		})
	}
}

func TestServiceCreateAddsOwnedBy(t *testing.T) {
	store := group.NewMemoryStore()
	service := group.NewService(store)

	ctx := auth.ContextWithID(context.Background(), "bar")
	_, err := service.Create(ctx, &group.NewGroup{OwnedBy: []string{"foo"}})
	if err != nil {
		t.Fatal(err)
	}

	g, err := store.FindByID(context.Background(), "0")
	if err != nil {
		t.Fatal(err)
	}

	if len(g.OwnedBy) != 2 || g.OwnedBy[0] != "foo" || g.OwnedBy[1] != "bar" {
		t.Fatalf("owned by should include 'bar', got %v", g.OwnedBy)
	}
}

func TestServiceCreateWhiteSpace(t *testing.T) {
	store := group.NewMemoryStore()
	service := group.NewService(store)

	ctx := auth.ContextWithID(context.Background(), "bar")
	g, _ := service.Create(ctx, &group.NewGroup{Name: " foo "})

	if g.Name != "foo" {
		t.Fatalf("should remove white space from name: '%s'", g.Name)
	}
}

func TestServiceFindFilterOwnedBySelf(t *testing.T) {
	store := group.NewMemoryStore()
	_, err := store.Create(context.Background(), &group.NewGroup{
		OwnedBy: []string{"bar"},
	})
	if err != nil {
		t.Fatal(err)
	}
	service := group.NewService(store)

	groups, _, err := service.Find(context.Background(), &crud.FindParams[group.FindFilters]{
		Filters: &group.FindFilters{OwnedBy: []string{"self"}},
	})
	if err != nil {
		t.Fatal(err)
	}
	if len(groups) != 0 {
		t.Fatalf("expected 0 events, got %d", len(groups))
	}

	ctx := auth.ContextWithID(context.Background(), "bar")
	groups, _, err = service.Find(ctx, &crud.FindParams[group.FindFilters]{
		Filters: &group.FindFilters{OwnedBy: []string{"self"}},
	})
	if err != nil {
		t.Fatal(err)
	}
	if len(groups) != 1 {
		t.Fatalf("expected 1 event, got %d", len(groups))
	}
}
