//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package group

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/crud"
	"eintopf.info/internal/xhttp"
	"eintopf.info/service/auth"
)

// Router returns a new http router that handles crud requests on the group
// model, that get forwarded to the given group service.
func Router(service Service, authService auth.Service) func(chi.Router) {
	server := &server{service}
	return func(r chi.Router) {
		r.Options("/", xhttp.CorsHandler)

		// swagger:route GET /groups/ group findGroups
		//
		// Retrieves all groups.
		//
		// Parameters:
		//   + name: offset
		//     description: offset in event list
		//     in: query
		//     type: integer
		//     required: false
		//   + name: limit
		//     description: limits the event list
		//     in: query
		//     type: integer
		//     required: false
		//   + name: sort
		//     description: field that gets sorted
		//     in: query
		//     type: string
		//     required: false
		//   + name: order
		//     description: sort order ("ASC" or "DESC")
		//     in: query
		//     type: string
		//     required: false
		//   + name: filters
		//     description: filters get combined with AND logic
		//     in: query
		//     type: object
		//     required: false
		//
		//     Responses:
		//       200: findGroupsResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.MiddlewareWithOpts(authService, auth.MiddlewareOpts{Validate: false})).
			Get("/", server.find)

		// swagger:route POST /groups/ group createGroup
		//
		// Creates a new group with the given data.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: createGroupResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Post("/", server.create)

		r.Options("/{id}", xhttp.CorsHandler)

		// swagger:route GET /groups/{id} group findGroup
		//
		// Finds the group with the given id.
		//
		//     Responses:
		//       200: findGroupResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.Get("/{id}", server.findByID)

		// swagger:route PUT /groups/{id} group updateGroup
		//
		// Updates the group with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: updateGroupResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Put("/{id}", server.update)

		// swagger:route DELETE /groups/{id} group deleteGroup
		//
		// Deletes the group with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: deleteGroupResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Delete("/{id}", server.delete)
	}
}

type server struct {
	service Service
}

func (s *server) find(w http.ResponseWriter, r *http.Request) {
	params, err := readFindRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	groups, total, err := s.service.Find(r.Context(), params)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeFindResponse(w, &findResponse{Groups: groups, Total: total})
	if err != nil {
		log.Println(err)
		return
	}
}

// swagger:response findGroupsResponse
type findResponse struct {

	// in:body
	Groups []*Group

	// in:header
	Total int `json:"x-total-count"`
}

func readFindRequest(r *http.Request) (*crud.FindParams[FindFilters], error) {
	params := &crud.FindParams[FindFilters]{}
	var err error

	params.Offset, err = xhttp.ReadQueryInt64(r, "offset")
	if err != nil {
		return nil, err
	}
	params.Limit, err = xhttp.ReadQueryInt64(r, "limit")
	if err != nil {
		return nil, err
	}

	params.Sort, err = xhttp.ReadQueryString(r, "sorting")
	if err != nil {
		return nil, err
	}
	order, err := xhttp.ReadQueryString(r, "order")
	if err != nil {
		return nil, err
	}
	params.Order = crud.SortOrder(order)

	filters := &FindFilters{}
	err = xhttp.ReadQueryJson(r, "filters", filters)
	if err != nil {
		return nil, err
	}
	params.Filters = filters

	return params, nil
}

func writeFindResponse(w http.ResponseWriter, resp *findResponse) error {
	data, err := json.Marshal(resp.Groups)
	if err != nil {
		return err
	}
	w.Header().Add("Access-Control-Expose-Headers", "X-Total-Count")
	w.Header().Add("X-Total-Count", strconv.Itoa(resp.Total))
	w.Write(data)
	return nil
}

func (s *server) create(w http.ResponseWriter, r *http.Request) {
	req, err := readCreateRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	group, err := s.service.Create(r.Context(), req.Group)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeCreateResponse(w, &createResponse{group})
	if err != nil {
		log.Println(err)
		return
	}
}

// swagger:parameters createGroup
type createRequest struct {

	// in:body
	Group *NewGroup
}

// swagger:response createGroupResponse
type createResponse struct {

	// in:body
	Group *Group
}

func readCreateRequest(r *http.Request) (*createRequest, error) {
	data, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	group := &NewGroup{}
	err = json.Unmarshal(data, group)
	if err != nil {
		return nil, err
	}
	return &createRequest{group}, nil
}

func writeCreateResponse(w http.ResponseWriter, resp *createResponse) error {
	data, err := json.Marshal(resp.Group)
	if err != nil {
		return err
	}
	w.Write(data)
	return nil
}

func (s *server) findByID(w http.ResponseWriter, r *http.Request) {
	req, err := readFindByIDRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	group, err := s.service.FindByID(r.Context(), req.ID)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeFindByIDResponse(w, &findByIDResponse{group})
	if err != nil {
		log.Println(err)
		return
	}
}

// swagger:parameters findGroup
type findByIDRequest struct {
	// in:query
	ID string `json:"id"`
}

// swagger:response findGroupResponse
type findByIDResponse struct {

	// in:body
	Group *Group
}

func readFindByIDRequest(r *http.Request) (*findByIDRequest, error) {
	id := chi.URLParam(r, "id")
	return &findByIDRequest{id}, nil
}

func writeFindByIDResponse(w http.ResponseWriter, resp *findByIDResponse) error {
	data, err := json.Marshal(resp.Group)
	if err != nil {
		return err
	}
	w.Write(data)
	return nil
}

func (s *server) update(w http.ResponseWriter, r *http.Request) {
	req, err := readUpdateRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	group, err := s.service.Update(r.Context(), req.Group)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeUpdateResponse(w, &updateResponse{group})
	if err != nil {
		log.Println(err)
		return
	}
}

// swagger:parameters updateGroup
type updateRequest struct {

	// in:body
	Group *Group
}

// swagger:response updateGroupResponse
type updateResponse struct {

	// in:body
	Group *Group
}

func readUpdateRequest(r *http.Request) (*updateRequest, error) {
	data, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	group := &Group{}
	err = json.Unmarshal(data, group)
	if err != nil {
		return nil, err
	}
	return &updateRequest{group}, nil
}

func writeUpdateResponse(w http.ResponseWriter, resp *updateResponse) error {
	data, err := json.Marshal(resp.Group)
	if err != nil {
		return err
	}
	w.Write(data)
	return nil
}

func (s *server) delete(w http.ResponseWriter, r *http.Request) {
	req, err := readDeleteRequest(r)
	if err != nil {

		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	err = s.service.Delete(r.Context(), req.ID)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeDeleteResponse(w)
	if err != nil {
		log.Println(err)
		return
	}
}

// swagger:parameters deleteGroup
type deleteRequest struct {

	// in:query
	ID string `json:"id"`
}

// swagger:response deleteGroupResponse
type deleteResponse struct{}

func readDeleteRequest(r *http.Request) (*deleteRequest, error) {
	id := chi.URLParam(r, "id")
	return &deleteRequest{id}, nil
}

func writeDeleteResponse(w http.ResponseWriter) error {
	w.Write([]byte{})
	return nil
}
