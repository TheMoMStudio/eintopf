//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package testutil

import (
	"context"
	"testing"

	"eintopf.info/internal/crud"
	"eintopf.info/service/group"
)

type newStore func() (group.Storer, func(), error)

// TestStore tests if a given group.Service acts as a group store.
func TestStore(t *testing.T, newStore newStore) {
	ctx := context.Background()
	t.Helper()
	t.Run("FindNoMatch", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		group, err := store.FindByID(ctx, "test")
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if group != nil {
			tt.Fatalf("expected to group to be nil. got %v", group)
		}
	})

	t.Run("CreateFindByID", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		group1, _ := store.Create(ctx, &group.NewGroup{Name: "group1"})
		group, err := store.FindByID(ctx, group1.ID)
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if group.ID != group1.ID || group.Name != group1.Name {
			tt.Fatalf("expected to find group1. got %v. want %v", group, group1)
		}
	})

	t.Run("CreateFind", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		store.Create(ctx, &group.NewGroup{Name: "group1"})
		store.Create(ctx, &group.NewGroup{Name: "group2"})
		groups, totalGroups, err := store.Find(ctx, nil)
		if err != nil {
			tt.Fatalf("expected store.FindAll to succeed. got %s", err.Error())
		}
		if len(groups) != 2 {
			tt.Fatalf("expected to recieve two groups. got %d", len(groups))
		}
		if totalGroups != 2 {
			tt.Fatalf("expected to recieve a total of two groups. got %d", totalGroups)
		}
	})

	t.Run("CreateFindFilter", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		group1, _ := store.Create(ctx, &group.NewGroup{Name: "group1"})
		group2, _ := store.Create(ctx, &group.NewGroup{Name: "group2"})
		groups, totalGroups, err := store.Find(ctx, &crud.FindParams[group.FindFilters]{
			Filters: &group.FindFilters{Name: &group1.Name},
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(groups) != 1 {
			tt.Fatalf("expected to recieve one group. got %d", len(groups))
		}
		if totalGroups != 1 {
			tt.Fatalf("expected to recieve a total of one group. got %d", totalGroups)
		}
		if groups[0].ID != group1.ID {
			tt.Fatalf("expected to find group1. got %v. want %v", groups[0], group1)
		}

		groups, totalGroups, err = store.Find(ctx, &crud.FindParams[group.FindFilters]{
			Filters: &group.FindFilters{NotID: &group1.ID},
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(groups) != 1 {
			tt.Fatalf("expected to recieve one group. got %d", len(groups))
		}
		if totalGroups != 1 {
			tt.Fatalf("expected to recieve a total of one group. got %d", totalGroups)
		}
		if groups[0].ID != group2.ID {
			tt.Fatalf("expected to find group1. got %v. want %v", groups[0], group2)
		}
	})

	t.Run("CreateFindSortedASC", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		group1, _ := store.Create(ctx, &group.NewGroup{Name: "c group1"})
		group2, _ := store.Create(ctx, &group.NewGroup{Name: "a group2"})
		group3, _ := store.Create(ctx, &group.NewGroup{Name: "b group3"})
		groups, totalGroups, err := store.Find(ctx, &crud.FindParams[group.FindFilters]{
			Sort:  "name",
			Order: crud.OrderAsc,
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(groups) != 3 {
			tt.Fatalf("expected to find three group. got %d", len(groups))
		}
		if totalGroups != 3 {
			tt.Fatalf("expected to recieve a total of three groups. got %d", totalGroups)
		}
		if groups[0].ID != group2.ID {
			tt.Fatalf("expected to group[0] to be group2 . got %v", groups[0])
		}
		if groups[1].ID != group3.ID {
			tt.Fatalf("expected to group[1] to be group3 . got %v", groups[1])
		}
		if groups[2].ID != group1.ID {
			tt.Fatalf("expected to group[2] to be group1 . got %v", groups[2])
		}
	})

	t.Run("CreateFindSortedDESC", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		group1, _ := store.Create(ctx, &group.NewGroup{Name: "c group1"})
		group2, _ := store.Create(ctx, &group.NewGroup{Name: "a group2"})
		group3, _ := store.Create(ctx, &group.NewGroup{Name: "b group3"})
		groups, totalGroups, err := store.Find(ctx, &crud.FindParams[group.FindFilters]{
			Sort:  "name",
			Order: crud.OrderDesc,
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(groups) != 3 {
			tt.Fatalf("expected to find three group. got %d", len(groups))
		}
		if totalGroups != 3 {
			tt.Fatalf("expected to recieve a total of three groups. got %d", totalGroups)
		}
		if groups[0].ID != group1.ID {
			tt.Fatalf("expected to group[0] to be group1 . got %v", groups[0])
		}
		if groups[1].ID != group3.ID {
			tt.Fatalf("expected to group[1] to be group3 . got %v", groups[1])
		}
		if groups[2].ID != group2.ID {
			tt.Fatalf("expected to group[2] to be group2 . got %v", groups[2])
		}
	})

	t.Run("CreateFindPaginated", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		store.Create(ctx, &group.NewGroup{Name: "group1"})
		group2, _ := store.Create(ctx, &group.NewGroup{Name: "group2"})
		store.Create(ctx, &group.NewGroup{Name: "group3"})
		groups, totalGroups, err := store.Find(ctx, &crud.FindParams[group.FindFilters]{
			Limit:  1,
			Offset: 1,
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(groups) != 1 {
			tt.Fatalf("expected to find one group. got %d", len(groups))
		}
		if totalGroups != 3 {
			tt.Fatalf("expected to recieve a total of three groups. got %d", totalGroups)
		}
		if groups[0].ID != group2.ID || groups[0].Name != group2.Name {
			tt.Fatalf("expected to find group2. got %v. want %v", groups[0], group2)
		}
	})

	t.Run("CreateFindPaginatedAndSorted", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		group1, _ := store.Create(ctx, &group.NewGroup{Name: "c group1"})
		store.Create(ctx, &group.NewGroup{Name: "a group2"})
		store.Create(ctx, &group.NewGroup{Name: "b group3"})
		groups, totalGroups, err := store.Find(ctx, &crud.FindParams[group.FindFilters]{
			Limit:  1,
			Offset: 2,
			Sort:   "name",
			Order:  crud.OrderAsc,
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(groups) != 1 {
			tt.Fatalf("expected to find one group. got %d", len(groups))
		}
		if totalGroups != 3 {
			tt.Fatalf("expected to recieve a total of three groups. got %d", totalGroups)
		}
		if groups[0].ID != group1.ID {
			tt.Fatalf("expected to group[0] to be group1 . got %v", groups[0])
		}
	})

	t.Run("CreateUpdate", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		group1, _ := store.Create(ctx, &group.NewGroup{Name: "group3"})
		group1.Name = "group3.1"

		_, err = store.Update(ctx, group1)
		if err != nil {
			tt.Fatalf("expected store.Update to succeed. got %s", err.Error())
		}

		group, err := store.FindByID(ctx, group1.ID)
		if group.Name != "group3.1" {
			tt.Fatalf("expected group.Name to be 'group3.1. got %s", group.Name)
		}
	})

	t.Run("CreateDelete", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		group1, _ := store.Create(ctx, &group.NewGroup{Name: "group1"})
		err = store.Delete(ctx, group1.ID)
		if err != nil {
			tt.Fatalf("expected store.Delete to succeed. got %s", err.Error())
		}
		group, err := store.FindByID(ctx, group1.ID)
		if group != nil {
			tt.Fatal("expected group1 to be deleted")
		}
	})
}
