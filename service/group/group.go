//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package group

import (
	"context"
	"fmt"
	"strings"

	"eintopf.info/internal/crud"
	"eintopf.info/internal/xerror"
	"eintopf.info/service/auth"
)

// Group defines a group entity
type NewGroup struct {
	Published   bool     `json:"published"`
	Name        string   `json:"name"`
	Link        string   `json:"link"`
	Email       string   `json:"email"`
	Description string   `json:"description"`
	Image       string   `json:"image"`
	Alt         string   `json:"alt"`
	OwnedBy     []string `json:"ownedBy" db:"owned_by"`
}

// IsOwned returns true if the id is in the OwnedBy field.
func (g *NewGroup) IsOwned(id string) bool {
	owned := false
	for _, owner := range g.OwnedBy {
		if owner == id {
			owned = true
		}
	}
	return owned
}

// Group defines a group entity
// It implements indexo.Coppyable.
type Group struct {
	ID          string   `json:"id" db:"id"`
	Deactivated bool     `json:"deactivated" db:"deactivated"`
	Published   bool     `json:"published" db:"published"`
	Name        string   `json:"name" db:"name"`
	Link        string   `json:"link" db:"link"`
	Email       string   `json:"email" db:"email"`
	Description string   `json:"description" db:"description"`
	Image       string   `json:"image" db:"image"`
	Alt         string   `json:"alt" db:"alt"`
	OwnedBy     []string `json:"ownedBy" db:"owned_by"`
}

func (g Group) Identifier() string { return g.ID }

func GroupFromNewGroup(newGroup *NewGroup, id string) *Group {
	return &Group{
		ID:          id,
		Deactivated: false,
		Name:        newGroup.Name,
		Link:        newGroup.Link,
		Email:       newGroup.Email,
		Description: newGroup.Description,
		OwnedBy:     newGroup.OwnedBy,
		Image:       newGroup.Image,
		Alt:         newGroup.Alt,
		Published:   newGroup.Published,
	}
}

// Indexable indicates wether the group should be added to the search index.
func (g *Group) Indexable() bool {
	return !g.Deactivated
}

// Listed indicates wether the group should be shown on the list page.
func (g *Group) Listable() bool {
	return g.Published
}

// IsOwned returns true if the id is in the OwnedBy field.
func (g *Group) IsOwned(id string) bool {
	owned := false
	for _, owner := range g.OwnedBy {
		if owner == id {
			owned = true
		}
	}
	return owned
}

// Service defines the crud service to manage groups.
type Service interface {
	Storer
}

// SortOrder defines the order of sorting.
type SortOrder string

// Possible values for SortOrder.
const (
	OrderAsc  = SortOrder("ASC")
	OrderDesc = SortOrder("DESC")
)

// FindFilters defines the possible filters for the find method.
type FindFilters struct {
	ID          *string  `json:"id"`
	NotID       *string  `json:"not_id"`
	Deactivated *bool    `json:"deactivated"`
	Published   *bool    `json:"published"`
	Name        *string  `json:"name"`
	LikeName    *string  `json:"likeName"`
	Link        *string  `json:"link"`
	Email       *string  `json:"email"`
	Description *string  `json:"description"`
	OwnedBy     []string `json:"ownedBy"`
}

// Storer defines a service for CRUD operations on the event model.
type Storer interface {
	Create(ctx context.Context, newGroup *NewGroup) (*Group, error)
	Update(ctx context.Context, group *Group) (*Group, error)
	Delete(ctx context.Context, id string) error
	FindByID(ctx context.Context, id string) (*Group, error)
	Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*Group, int, error)
}

// NewService returns a new group service.
func NewService(store Storer) *service {
	return &service{store: store}
}

type service struct {
	store Storer
}

var ErrNameAlreadyExists = xerror.BadInputError{Err: fmt.Errorf("name already exists")}

// Create makes sure the loggedin user is in the owned field.
func (s *service) Create(ctx context.Context, newGroup *NewGroup) (*Group, error) {
	if s.groupNameExists(ctx, newGroup.Name, "") {
		return nil, ErrNameAlreadyExists
	}
	id, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return nil, err
	}
	if !newGroup.IsOwned(id) {
		newGroup.OwnedBy = append(newGroup.OwnedBy, id)
	}
	newGroup.Name = strings.TrimSpace(newGroup.Name)
	return s.store.Create(ctx, newGroup)
}

func (s *service) Update(ctx context.Context, group *Group) (*Group, error) {
	if s.groupNameExists(ctx, group.Name, group.ID) {
		return nil, ErrNameAlreadyExists
	}
	return s.store.Update(ctx, group)
}

func (s *service) Delete(ctx context.Context, id string) error {
	return s.store.Delete(ctx, id)
}

func (s *service) FindByID(ctx context.Context, id string) (*Group, error) {
	return s.store.FindByID(ctx, id)
}

// Find adds a special filter value "self" for the OwnedBy field. If it is set
// the value gets replaced with the id of the loggedin user.
func (s *service) Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*Group, int, error) {
	if params != nil && params.Filters != nil && params.Filters.OwnedBy != nil {
		if len(params.Filters.OwnedBy) == 1 && params.Filters.OwnedBy[0] == "self" {
			id, err := auth.UserIDFromContext(ctx)
			if err == nil {
				params.Filters.OwnedBy = []string{id}
			}
		}
	}
	return s.store.Find(ctx, params)
}

func (s *service) groupNameExists(ctx context.Context, name string, id string) bool {
	existingGroups, _, err := s.store.Find(auth.ContextWithRole(ctx, auth.RoleInternal), &crud.FindParams[FindFilters]{
		Filters: &FindFilters{Name: &name, NotID: &id},
	})
	if err != nil || len(existingGroups) > 0 {
		return true
	}
	return false
}

func (s *service) GroupIDsByOwners(ctx context.Context, ownedBy []string) ([]string, error) {

	groups, _, err := s.store.Find(auth.ContextWithRole(ctx, auth.RoleInternal), &crud.FindParams[FindFilters]{
		Filters: &FindFilters{OwnedBy: ownedBy},
	})
	if err != nil {
		return nil, err
	}
	ids := []string{}
	for _, g := range groups {
		ids = append(ids, g.ID)
	}
	return ids, nil
}
