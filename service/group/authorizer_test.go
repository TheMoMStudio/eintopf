//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package group_test

import (
	"context"
	"testing"

	"eintopf.info/service/auth"
	"eintopf.info/service/group"
)

var (
	roleAdmin    = auth.RoleAdmin
	roleModertor = auth.RoleModerator
	roleNormal   = auth.RoleNormal
)

func TestAuthorizerCreate(t *testing.T) {
	for _, test := range []struct {
		name      string
		userID    string
		group     *group.NewGroup
		wantError error
	}{
		{
			name:      "UnauthorizedWithNoUserID",
			userID:    "",
			group:     &group.NewGroup{},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "AuthorizedWithUserID",
			userID:    "foo",
			group:     &group.NewGroup{},
			wantError: nil,
		},
	} {
		t.Run(test.name, func(tt *testing.T) {
			ctx := auth.ContextWithID(context.Background(), test.userID)

			store := group.NewMemoryStore()
			authorizer := group.NewAuthorizer(store)

			_, err := authorizer.Create(ctx, test.group)
			if err != test.wantError {
				tt.Errorf("err != test.wantError: %s != %s", err, test.wantError)
			}

			if test.wantError == nil {
				if g, _ := store.FindByID(context.Background(), "0"); g == nil {
					tt.Errorf("group should have been created")
				}
			}
		})
	}
}

func TestAuthorizerUpdate(t *testing.T) {
	for _, test := range []struct {
		name      string
		userID    string
		role      *auth.Role
		group     *group.NewGroup
		wantError error
	}{
		{
			name:      "UnauthorizedWithNoUserIDAndRole",
			userID:    "",
			role:      nil,
			group:     &group.NewGroup{},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "AuthorizedWithAdminRole",
			userID:    "foo",
			role:      &roleAdmin,
			group:     &group.NewGroup{OwnedBy: []string{"bar"}},
			wantError: nil,
		}, {
			name:      "AuthorizedWithModeratorRole",
			userID:    "foo",
			role:      &roleModertor,
			group:     &group.NewGroup{OwnedBy: []string{"bar"}},
			wantError: nil,
		}, {
			name:      "UnauthorizedWithNormalRoleNotOwned",
			userID:    "foo",
			role:      &roleNormal,
			group:     &group.NewGroup{OwnedBy: []string{"bar"}},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "AuthorizedWithNormalRoleOwned",
			userID:    "bar",
			role:      &roleNormal,
			group:     &group.NewGroup{OwnedBy: []string{"bar"}},
			wantError: nil,
		},
	} {
		t.Run(test.name, func(tt *testing.T) {
			ctx := auth.ContextWithID(context.Background(), test.userID)
			if test.role != nil {
				ctx = auth.ContextWithRole(ctx, *test.role)
			}

			store := group.NewMemoryStore()
			g, err := store.Create(context.Background(), test.group)
			if err != nil {
				t.Fatal(err)
			}
			authorizer := group.NewAuthorizer(store)

			g.Name = "updated"
			g2, err := authorizer.Update(ctx, g)
			if err != test.wantError {
				tt.Errorf("err != test.wantError: %s != %s", err, test.wantError)
			}
			if test.wantError == nil {
				if g2.Name != "updated" {
					tt.Errorf("name should be updated")
				}
			}
		})
	}
}

func TestAuthorizerDelete(t *testing.T) {
	for _, test := range []struct {
		name      string
		userID    string
		role      *auth.Role
		wantError error
	}{
		{
			name:      "UnauthorizedWithNoUserIDAndRole",
			userID:    "",
			role:      nil,
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "AuthorizedWithAdminRole",
			userID:    "foo",
			role:      &roleAdmin,
			wantError: nil,
		}, {
			name:      "UnauthorizedWithModeratorRoleNotOwned",
			userID:    "foo",
			role:      &roleModertor,
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "AuthorizedWithModeratorRoleOwned",
			userID:    "bar",
			role:      &roleModertor,
			wantError: nil,
		}, {
			name:      "UnauthorizedWithNormalRoleNotOwned",
			userID:    "foo",
			role:      &roleNormal,
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "AuthorizedWithNormalRoleOwned",
			userID:    "bar",
			role:      &roleNormal,
			wantError: nil,
		},
	} {
		t.Run(test.name, func(tt *testing.T) {
			ctx := auth.ContextWithID(context.Background(), test.userID)
			if test.role != nil {
				ctx = auth.ContextWithRole(ctx, *test.role)
			}

			store := group.NewMemoryStore()
			_, err := store.Create(context.Background(), &group.NewGroup{OwnedBy: []string{"bar"}})
			authorizer := group.NewAuthorizer(store)

			err = authorizer.Delete(ctx, "0")
			if err != test.wantError {
				tt.Errorf("err != test.wantError: %s != %s", err, test.wantError)
			}
			if test.wantError == nil {
				if g, _ := store.FindByID(context.Background(), "0"); g != nil {
					tt.Errorf("group should have be deleted")
				}
			}
		})
	}
}

func TestAuthorizerFind(t *testing.T) {
	for _, test := range []struct {
		name      string
		userID    string
		role      *auth.Role
		group     *group.Group
		wantError error
	}{
		{
			name:      "AuthorizedAll",
			userID:    "",
			role:      nil,
			wantError: nil,
		},
	} {
		t.Run(test.name, func(tt *testing.T) {
			ctx := auth.ContextWithID(context.Background(), test.userID)
			if test.role != nil {
				ctx = auth.ContextWithRole(ctx, *test.role)
			}

			store := group.NewMemoryStore()
			store.Create(context.Background(), &group.NewGroup{})
			authorizer := group.NewAuthorizer(store)

			_, total, err := authorizer.Find(ctx, nil)
			if err != test.wantError {
				tt.Errorf("err != test.wantError: %s != %s", err, test.wantError)
			}

			if test.wantError == nil && total == 0 {
				tt.Errorf("group should have been found")
			}
		})
	}
}

func TestAuthorizerFindByID(t *testing.T) {
	for _, test := range []struct {
		name      string
		userID    string
		role      *auth.Role
		group     *group.Group
		wantError error
	}{
		{
			name:      "AuthorizedAll",
			userID:    "",
			role:      nil,
			wantError: nil,
		},
	} {
		t.Run(test.name, func(tt *testing.T) {
			ctx := auth.ContextWithID(context.Background(), test.userID)
			if test.role != nil {
				ctx = auth.ContextWithRole(ctx, *test.role)
			}

			store := group.NewMemoryStore()
			store.Create(context.Background(), &group.NewGroup{})
			authorizer := group.NewAuthorizer(store)

			g, err := authorizer.FindByID(ctx, "0")
			if err != test.wantError {
				tt.Errorf("err != test.wantError: %s != %s", err, test.wantError)
			}

			if test.wantError == nil && g == nil {
				tt.Errorf("group should have been found")
			}
		})
	}
}
