//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package group_test

import (
	"testing"

	"eintopf.info/service/group"
	"eintopf.info/service/group/testutil"
)

func TestMemoryStore(t *testing.T) {
	testutil.TestStore(t, func() (group.Storer, func(), error) {
		return group.NewMemoryStore(), func() {}, nil
	})
}
