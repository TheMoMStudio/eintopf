// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package osm

import (
	"context"
	"errors"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	"eintopf.info/internal/xhttp"
	"github.com/go-chi/chi/v5"
)

type Tiler interface {
	GetTile(ctx context.Context, z, x, y int) ([]byte, error)
}

type FileCache struct {
	cacheDir string
}

func (f FileCache) GetTile(ctx context.Context, z, x, y int) ([]byte, error) {
	p := path.Join(f.cacheDir, strconv.Itoa(z), strconv.Itoa(x), strconv.Itoa(y))
	if _, err := os.Stat(p); errors.Is(err, os.ErrNotExist) {
		return nil, err
	}
	tile, err := os.ReadFile(p)
	if err != nil {
		return nil, err
	}
	return tile, nil
}

func (f FileCache) SetTile(ctx context.Context, z, x, y int, tile []byte) error {
	dir := path.Join(f.cacheDir, strconv.Itoa(z), strconv.Itoa(x))
	err := os.MkdirAll(dir, os.ModePerm)
	if err != nil {
		return err
	}
	p := path.Join(f.cacheDir, strconv.Itoa(z), strconv.Itoa(x), strconv.Itoa(y))
	return os.WriteFile(p, tile, os.ModePerm)
}

func (f FileCache) Clear() error {
	return os.Remove(f.cacheDir)
}

type Client struct {
	serverURL string
}

func (c Client) GetTile(ctx context.Context, z, x, y int) ([]byte, error) {
	u := c.serverURL
	u = strings.Replace(u, "{z}", strconv.Itoa(z), 1)
	u = strings.Replace(u, "{x}", strconv.Itoa(x), 1)
	u = strings.Replace(u, "{y}", strconv.Itoa(y), 1)

	r, err := http.NewRequest("GET", u, nil)
	if err != nil {
		return nil, err
	}
	r.Header.Set("User-Agent", "eintopf")
	hc := &http.Client{}

	response, err := hc.Do(r)
	if err != nil {
		return nil, err
	}
	data, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	return data, nil
}

type Service struct {
	cache        FileCache
	client       Client
	lastModified time.Time
}

func NewService(cacheDir string, tileServer string) *Service {
	return &Service{
		cache:        FileCache{cacheDir: cacheDir},
		client:       Client{serverURL: tileServer},
		lastModified: time.Now(),
	}
}

func (s *Service) GetTile(ctx context.Context, z, x, y int) ([]byte, error) {
	tile, err := s.cache.GetTile(ctx, z, x, y)
	if errors.Is(err, os.ErrNotExist) {
		log.Printf("osm cache miss: %d/%d/%d", z, x, y)
		var err2 error
		tile, err2 = s.client.GetTile(ctx, z, x, y)
		if err2 != nil {
			return nil, err2
		}
		err2 = s.cache.SetTile(ctx, z, x, y, tile)
		if err2 != nil {
			return nil, err2
		}
	} else if err != nil {
		return nil, err
	}
	return tile, nil
}

func (s *Service) ClearCache(ctx context.Context) error {
	err := s.cache.Clear()
	if err != nil {
		return err
	}
	s.lastModified = time.Now()
	return nil
}

func (s *Service) LastModified() time.Time {
	return s.lastModified
}

func Router(service *Service) func(chi.Router) {
	return func(r chi.Router) {
		r.With(xhttp.LastModified(service)).Get("/tile/{z}/{x}/{y}.png", func(w http.ResponseWriter, r *http.Request) {
			z, err := xhttp.ReadParamInt(r, "z")
			if err != nil {
				xhttp.WriteBadRequest(r.Context(), w, err)
				return
			}
			x, err := xhttp.ReadParamInt(r, "x")
			if err != nil {
				xhttp.WriteBadRequest(r.Context(), w, err)
				return
			}

			y, err := xhttp.ReadParamInt(r, "y")
			if err != nil {
				xhttp.WriteBadRequest(r.Context(), w, err)
				return
			}

			tile, err := service.GetTile(r.Context(), z, x, y)
			if err != nil {
				xhttp.WriteInternalError(r.Context(), w, err)
				return
			}
			w.Header().Add("Content-Type", "image/png")
			w.Write(tile)

		})
	}
}
