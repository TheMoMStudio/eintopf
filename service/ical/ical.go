//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package ical

import (
	"context"
	"fmt"
	"path"
	"strings"
	"time"

	ics "github.com/arran4/golang-ical"

	"eintopf.info/service/event"
	"eintopf.info/service/eventsearch"
)

type Service interface {
	Feed(ctx context.Context, opts eventsearch.Options) (string, error)
	GroupFeed(ctx context.Context, groupID string) (string, error)
	PlaceFeed(ctx context.Context, placeID string) (string, error)
	EventIcal(ctx context.Context, eventID string) (string, error)
}

type ServiceImpl struct {
	eventSearchService eventsearch.Service

	baseURL string
}

func NewService(eventSearchService eventsearch.Service, baseURL string) *ServiceImpl {
	return &ServiceImpl{
		eventSearchService: eventSearchService,

		baseURL: baseURL,
	}
}

func (s *ServiceImpl) Feed(ctx context.Context, opts eventsearch.Options) (string, error) {
	if opts.Sort == "" {
		opts.Sort = "start"
	}
	if opts.Filters == nil {
		opts.Filters = []eventsearch.Filter{eventsearch.ListedFilter{Listed: true}}
	} else {
		foundListedFilter := false
		for _, f := range opts.Filters {
			if _, ok := f.(eventsearch.ListedFilter); ok {
				foundListedFilter = true
				break
			}
		}
		if !foundListedFilter {
			opts.Filters = append(opts.Filters, eventsearch.ListedFilter{Listed: true})
		}
	}
	// Do not include every day of a multiday event.
	opts.Filters = append(opts.Filters, eventsearch.MultidayRangeFilter{
		MultidayMin: intptr(-1),
		MultidayMax: intptr(2),
	})
	result, err := s.eventSearchService.Search(ctx, opts)
	if err != nil {
		return "", err
	}

	return s.icsFromEventDocuments(ctx, result.Events)
}

func (s *ServiceImpl) GroupFeed(ctx context.Context, groupID string) (string, error) {
	opts := eventsearch.Options{
		Filters: []eventsearch.Filter{
			eventsearch.GroupIDFilter{GroupID: groupID},
			eventsearch.DateRangeFilter{DateMin: time.Now()},
		},
	}
	return s.Feed(ctx, opts)
}

func (s *ServiceImpl) PlaceFeed(ctx context.Context, placeID string) (string, error) {
	opts := eventsearch.Options{
		Filters: []eventsearch.Filter{
			eventsearch.PlaceIDFilter{PlaceID: placeID},
			eventsearch.DateRangeFilter{DateMin: time.Now()},
		},
	}
	return s.Feed(ctx, opts)
}

func (s *ServiceImpl) EventIcal(ctx context.Context, eventID string) (string, error) {
	opts := eventsearch.Options{
		Filters: []eventsearch.Filter{eventsearch.IDFilter{ID: eventID}},
	}
	return s.Feed(ctx, opts)
}

func (s *ServiceImpl) icsFromEventDocuments(ctx context.Context, events []*eventsearch.EventDocument) (string, error) {
	cal := ics.NewCalendar()
	cal.SetProductId("eintopf.info")
	cal.SetMethod(ics.MethodRequest)

	for _, e := range events {
		calEvent := cal.AddEvent(e.ID)
		calEvent.SetURL(path.Join(s.baseURL, "event", e.ID))
		calEvent.SetSummary(e.Name)
		calEvent.SetStartAt(e.Start)
		eventURL, err := event.URLFromID(s.baseURL, e.ID)
		if err != nil {
			return "", fmt.Errorf("event url: %s", err)
		}
		calEvent.SetURL(eventURL)
		if e.End != nil {
			calEvent.SetEndAt(*e.End)
		}

		// add category and topic to ical categories
		var categories []string

		if e.Category != nil {
			categories = append(categories, e.Category.Name)
		}

		if e.Topic != nil {
			categories = append(categories, e.Topic.Name)
		}

		if len(categories) > 0 {
			calEvent.AddProperty(ics.ComponentPropertyCategories, strings.Join(categories, ","))
		}

		calEvent.SetDescription(e.Description)
		if e.Location != nil {
			if e.Location.Name != "" {
				calEvent.SetLocation(e.Location.Name)
			}
			if e.Location.Place != nil {
				place := e.Location.Place
				if place.Name != "" && place.Address != "" {
					calEvent.SetLocation(fmt.Sprintf("%s, %s", place.Name, place.Address))
				} else if place.Address != "" {
					calEvent.SetLocation(place.Address)
				} else if place.Name != "" {
					calEvent.SetLocation(place.Name)
				}

				if place.Lat != 0 && place.Lng != 0 {
					calEvent.SetGeo(place.Lat, place.Lng)
				}
			} else {
				loc := ""
				if e.Location.Name != "" {
					loc = e.Location.Name
				}
				if e.Location.Address != "" {
					loc = loc + ", " + e.Location.Address
				}
				if e.Location2 != "" {
					loc = fmt.Sprintf("%s, %s", loc, e.Location2)
				}
				if loc != "" {
					calEvent.SetLocation(loc)
				}

				if e.Location.Lat != 0 && e.Location.Lng != 0 {
					calEvent.SetGeo(e.Location.Lat, e.Location.Lng)
				}
			}
		}

		organizers := []string{}
		for _, organizer := range e.Organizers {
			if organizer.Name != "" {
				organizers = append(organizers, organizer.Name)
			}
			if organizer.Group != nil {
				organizers = append(organizers, organizer.Group.Name)
			}
		}
		if len(organizers) > 0 {
			calEvent.SetOrganizer(strings.Join(organizers, ","))
		}
	}

	return cal.Serialize(), nil
}

func intptr(i int) *int { return &i }
