//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package ical

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/plausible"
	"eintopf.info/internal/xhttp"
	"eintopf.info/service/eventsearch"
)

func Routes(service Service, plausibleClient *plausible.Client) func(r chi.Router) {
	router := router{service: service}
	return func(r chi.Router) {
		r.Use(plausible.MiddlewareWithName(plausibleClient, "ical"))
		r.Get("/ical", router.getEventsIcalFeed)
		r.Get("/events/{id}/ical", router.getEventIcal)
		r.Get("/groups/{id}/ical", router.getGroupIcalFeed)
		r.Get("/places/{id}/ical", router.getPlaceIcalFeed)
	}
}

type router struct {
	service Service
}

func (router router) getEventsIcalFeed(w http.ResponseWriter, r *http.Request) {
	opts, err := eventsearch.OptionsFromRequest(r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	feed, err := router.service.Feed(r.Context(), opts)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeIcal(r.Context(), w, "eintopf", feed)
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, err)
		return
	}
}

func (router router) getEventIcal(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	if id == "" {
		xhttp.WriteBadRequest(r.Context(), w, fmt.Errorf("empty id"))
		return
	}
	ical, err := router.service.EventIcal(r.Context(), id)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeIcal(r.Context(), w, id, ical)
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, err)
		return
	}
}

func (router router) getGroupIcalFeed(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	if id == "" {
		xhttp.WriteBadRequest(r.Context(), w, fmt.Errorf("empty id"))
		return
	}
	ical, err := router.service.GroupFeed(r.Context(), id)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeIcal(r.Context(), w, id, ical)
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, err)
		return
	}
}

func (router router) getPlaceIcalFeed(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	if id == "" {
		xhttp.WriteBadRequest(r.Context(), w, fmt.Errorf("empty id"))
		return
	}
	ical, err := router.service.PlaceFeed(r.Context(), id)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	err = writeIcal(r.Context(), w, id, ical)
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, err)
		return
	}
}

func writeIcal(ctx context.Context, w http.ResponseWriter, filename, ical string) error {
	w.Header().Add("Content-type", "text/calendar; charset=utf-8")
	w.Header().Add("Content-Disposition", fmt.Sprintf("inline; filename=%s.ics", filename))
	_, err := w.Write([]byte(ical))
	if err != nil {
		return err
	}
	return nil
}
