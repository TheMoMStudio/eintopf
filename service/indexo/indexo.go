//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

// Package indexo provides an operator with an index and a delete operation,
// that operates on a search index
package indexo

import (
	"context"

	"eintopf.info/service/event"
	"eintopf.info/service/group"
	"eintopf.info/service/oqueue"
	"eintopf.info/service/place"
	"eintopf.info/service/search"
)

// Service defines an operation indexer service.
type Service interface {
	// Reindex takes all models, that are searchable and indexes them into the
	// search index.
	Reindex(ctx context.Context) error
}

type service struct {
	queue oqueue.Service

	searchService search.Service
	eventService  event.Storer
	groupService  group.Service
	placeService  place.Service
}

// NewService returns a new index operator service.
func NewService(
	queue oqueue.Service,
	searchService search.Service,
	eventService event.Storer,
	groupService group.Service,
	placeService place.Service,
) Service {
	s := &service{
		queue:         queue,
		searchService: searchService,
		eventService:  eventService,
		groupService:  groupService,
		placeService:  placeService,
	}

	queue.AddSubscriber(s.consumeOperation, 1)

	return s
}

// consumeOperation consumes an event, group and place operations of the type
// create, update and delete. Depending on wether the model is searchable, it
// performs an index or a delete operation on the search index.
func (s *service) consumeOperation(op oqueue.Operation) error {
	switch op := op.(type) {
	case group.CreateOperation:
		if op.Group.Indexable() {
			return s.indexGroup(op.Group)
		}
	case group.UpdateOperation:
		if op.Group.Indexable() {
			return s.indexGroup(op.Group)
		} else {
			return s.searchService.Delete("group", op.Group.ID)
		}
	case group.DeleteOperation:
		return s.searchService.Delete("group", op.ID)
	case place.CreateOperation:
		if op.Place.Indexable() {
			return s.indexPlace(op.Place)
		}
	case place.UpdateOperation:
		if op.Place.Indexable() {
			return s.indexPlace(op.Place)
		} else {
			return s.searchService.Delete("place", op.Place.ID)
		}
	case place.DeleteOperation:
		return s.searchService.Delete("place", op.ID)
	}
	return nil
}

func (s *service) Reindex(ctx context.Context) error {
	groups, _, err := s.groupService.Find(ctx, nil)
	if err != nil {
		return err
	}
	docs := []search.Indexable{}
	for _, group := range groups {
		if group.Indexable() {
			docs = append(docs, groupDocumentFromGroup(group))
		}
	}

	places, _, err := s.placeService.Find(ctx, nil)
	if err != nil {
		return err
	}
	for _, place := range places {
		if place.Indexable() {
			docs = append(docs, placeDocumentFromPlace(place))
		}
	}
	return s.searchService.Index(docs...)
}
