//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package indexo

import (
	"strings"

	"eintopf.info/service/group"
)

func (s *service) indexGroup(group *group.Group) error {
	return s.searchService.Index(groupDocumentFromGroup(group))
}

func groupDocumentFromGroup(group *group.Group) *GroupDocument {
	return &GroupDocument{
		ID:          group.ID,
		Published:   group.Published,
		Name:        group.Name,
		Link:        group.Link,
		Email:       group.Email,
		Image:       group.Image,
		Alt:         group.Alt,
		Description: group.Description,
		Listed:      group.Listable(),
	}
}

type GroupDocument struct {
	ID          string `json:"id"`
	Published   bool   `json:"published"`
	Name        string `json:"name"`
	Link        string `json:"link"`
	Email       string `json:"email"`
	Image       string `json:"image"`
	Alt         string `json:"alt"`
	Description string `json:"description"`
	Listed      bool   `json:"listed"`
}

// Identifier returns an id which should uniquely identify the object for
// its type.
func (g *GroupDocument) Identifier() string {
	return g.ID
}

// Type returns the type of the object.
func (g *GroupDocument) Type() string {
	return "group"
}

// QueryText returns the string to index for a text search.
func (g *GroupDocument) QueryText() string {
	var b strings.Builder
	b.WriteString(g.Name)
	b.WriteString(" ")
	b.WriteString(g.Description)
	return b.String()
}

// SearchFields returns a map of fields to be index for searching.
func (g *GroupDocument) SearchFields() map[string]interface{} {
	return map[string]interface{}{
		"is":        g.Type(),
		"name":      g.Name,
		"id":        g.ID,
		"listed":    g.Listed,
		"published": g.Published,
	}
}
