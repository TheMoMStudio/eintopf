//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package indexo

import (
	"strings"

	"eintopf.info/service/place"
)

func (s *service) indexPlace(place *place.Place) error {
	return s.searchService.Index(placeDocumentFromPlace(place))
}

func placeDocumentFromPlace(place *place.Place) *PlaceDocument {
	return &PlaceDocument{
		ID:          place.ID,
		Published:   place.Published,
		Name:        place.Name,
		Link:        place.Link,
		Email:       place.Email,
		Address:     place.Address,
		Lat:         place.Lat,
		Lng:         place.Lng,
		Image:       place.Image,
		Alt:         place.Alt,
		Description: place.Description,
		Listed:      place.Listable(),
	}
}

type PlaceDocument struct {
	ID          string   `json:"id"`
	Published   bool     `json:"published"`
	Name        string   `json:"name"`
	Address     string   `json:"address"`
	Lat         float64  `json:"lat"`
	Lng         float64  `json:"lng"`
	Link        string   `json:"link"`
	Email       string   `json:"email"`
	Description string   `json:"description"`
	Image       string   `json:"image"`
	Alt         string   `json:"alt"`
	OwnedBy     []string `json:"ownedBy"`
	Listed      bool     `json:"listed"`
}

// Identifier returns an id which should uniquely identify the object for
// its type.
func (p *PlaceDocument) Identifier() string {
	return p.ID
}

// Type returns the type of the object.
func (p *PlaceDocument) Type() string {
	return "place"
}

// QueryText returns the string to index for a text search.
func (p *PlaceDocument) QueryText() string {
	var b strings.Builder
	b.WriteString(p.Name)
	b.WriteString(" ")
	b.WriteString(p.Description)
	return b.String()
}

// SearchFields returns a map of fields to be index for searching.
func (p *PlaceDocument) SearchFields() map[string]interface{} {
	return map[string]interface{}{
		"is":        p.Type(),
		"name":      p.Name,
		"id":        p.ID,
		"listed":    p.Listed,
		"published": p.Published,
	}
}
