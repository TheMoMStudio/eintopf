//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

//go:generate go run github.com/go-swagger/go-swagger/cmd/swagger generate spec -o ./../../api/swagger.json

// EINTOPF Api
//
// Api for EINTOPF Calendar
//
//	Schemes: http, https
//	BasePath: /api/v1
//	Version: 0.0.1
//
//	Security:
//	- bearer
//
//	SecurityDefinitions:
//	bearer:
//	     type: apiKey
//	     name: Authorization
//	     in: header
//
//	Consumes:
//	- application/json
//
//	Produces:
//	- application/json
//
// swagger:meta
package api

import (
	"net/http"

	"github.com/go-chi/chi/v5"

	apispec "eintopf.info/api"
	"eintopf.info/internal/xhttp"
	"eintopf.info/service/action"
	"eintopf.info/service/auth"
	"eintopf.info/service/cms"
	"eintopf.info/service/event"
	"eintopf.info/service/eventsearch"
	"eintopf.info/service/group"
	"eintopf.info/service/invitation"
	"eintopf.info/service/killswitch"
	"eintopf.info/service/media"
	"eintopf.info/service/notification"
	"eintopf.info/service/osm"
	"eintopf.info/service/passwordrec"
	"eintopf.info/service/place"
	"eintopf.info/service/revent"
	"eintopf.info/service/search"
	"eintopf.info/service/taxonomy"
	"eintopf.info/service/user"
)

var apiFS http.FileSystem = http.Dir("api")

// Routes returns the http router for the api.
func Routes(
	actionService *action.Service,
	authService auth.Service,
	categoryStore taxonomy.CategoryStorer,
	eventService event.Storer,
	eventsearchService eventsearch.Service,
	groupService group.Service,
	invitationService invitation.Service,
	killswitchService killswitch.Service,
	mediaService media.Service,
	notificationSettingsStore notification.SettingsStorer,
	notificationStore notification.Storer,
	passwordrecService *passwordrec.Service,
	placeService place.Service,
	reventService revent.Service,
	searchService search.Service,
	topicStore taxonomy.TopicStorer,
	userService user.Service,
	osmTiler *osm.Service,
	mediaPath string,
	cmsService cms.Service,
) func(r chi.Router) {
	return func(r chi.Router) {
		r.Route("/api/v1", func(r chi.Router) {
			r.Use(xhttp.CorsMiddleware)
			r.Use(killswitch.Middleware(killswitchService))
			r.Route("/actions", action.Router(actionService, authService))
			r.Route("/cms", cms.Router(authService, cmsService))
			r.Route("/auth", auth.Router(authService))
			r.Route("/events", event.Router(eventService, authService))
			r.Route("/eventsearch", eventsearch.Router(eventsearchService))
			r.Route("/categories", taxonomy.CategoryRouter(categoryStore, authService))
			r.Route("/groups", group.Router(groupService, authService))
			r.Route("/invitations", invitation.Router(invitationService, authService))
			r.Route("/killswitch", killswitch.Router(killswitchService))
			r.Route("/media", media.Router(mediaService, authService, mediaPath, "/api"))
			r.Route("/notifications", notification.Router(notificationStore, notificationSettingsStore, authService))
			r.Route("/osm", osm.Router(osmTiler))
			r.Route("/passwordrec", passwordrec.Router(passwordrecService))
			r.Route("/places", place.Router(placeService, authService))
			r.Route("/revents", revent.Router(reventService, authService))
			r.Route("/search", search.Router(searchService))
			r.Route("/topics", taxonomy.TopicRouter(topicStore, authService))
			r.Route("/users", user.Router(userService, authService))

			apispec.AddRoutes(r, "/api/v1")
		})
	}
}
