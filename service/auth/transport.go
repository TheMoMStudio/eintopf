//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package auth

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/xhttp"
)

// Router returns a new auth router.
func Router(service Service) func(chi.Router) {
	router := &router{service}
	return func(r chi.Router) {
		r.Options("/login", xhttp.CorsHandler)

		// swagger:route POST /auth/login auth login
		//
		// Tries to log in the given user.
		//
		// When authentication was succesful returns a new JWT token.
		//
		//     Responses:
		//       200: loginSuccess
		//       400: badRequest
		//       500: internalError
		//       503: serviceUnavailable
		r.Post("/login", router.login)
	}
}

type router struct {
	s Service
}

func (router *router) login(w http.ResponseWriter, r *http.Request) {
	req, err := readLoginRequest(r)
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, fmt.Errorf("failed to read login request: %s", err))
		return
	}
	token, err := router.s.Login(r.Context(), req.Credentials.Email, req.Credentials.Password)
	if err == ErrInvalidCredentials {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	if err == ErrDeactivated {
		xhttp.WriteForbidden(w, err)
		return
	}
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, fmt.Errorf("failed to login: %s", err))
		return
	}

	id, err := router.s.Authenticate(token)
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, fmt.Errorf("failed to login: %s", err))
		return
	}
	role, err := router.s.Authorize(r.Context(), id)
	if err != nil {
		xhttp.WriteInternalError(r.Context(), w, fmt.Errorf("failed to login: %s", err))
		return
	}
	err = writeLoginResponse(w, &loginResponse{LoginResponsePayload{
		Token: token,
		ID:    id,
		Role:  role,
	}})
	if err != nil {
		log.Println(fmt.Errorf("failed to write login response: %s", err))
		return
	}
}

// swagger:parameters login
type loginRequest struct {

	// in: body
	Credentials Credentials
}

// Credentials define the login credentials.
type Credentials struct {

	// required:true
	Email string `json:"email"`

	// required:true
	Password string `json:"password"`
}

// swagger:response loginSuccess
type loginResponse struct {
	// in:body
	Body LoginResponsePayload
}

// LoginResponsePayload describes the payload of a succesful login xhttp.
type LoginResponsePayload struct {
	Token string `json:"token"`
	ID    string `json:"id"`
	Role  Role   `json:"role"`
}

func readLoginRequest(r *http.Request) (*loginRequest, error) {
	data, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	req := &loginRequest{}
	err = json.Unmarshal(data, &req.Credentials)
	if err != nil {
		return nil, err
	}
	return req, nil
}

func writeLoginResponse(w http.ResponseWriter, resp *loginResponse) error {
	data, err := json.Marshal(resp.Body)
	if err != nil {
		return err
	}

	w.Write(data)
	return nil
}

// Middleware returns a new validating middleware. Calls MiddlewareWithOpts.
func Middleware(service Service) func(next http.Handler) http.Handler {
	return MiddlewareWithOpts(service, MiddlewareOpts{Validate: true})
}

// MiddlewareOpts are options used by the authorization middleware.
type MiddlewareOpts struct {
	Validate bool
}

// MiddlewareWithOpts returns a new middleware, that adds user information from
// the "Authorization" header to the request context. If opts.Validate is set to
// true, it validates the user authorization and aborts for invalid
// authorization headers.
func MiddlewareWithOpts(service Service, opts MiddlewareOpts) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			token := r.Header.Get("Authorization")
			if token == "" {
				if opts.Validate {
					xhttp.WriteUnauthorized(w, fmt.Errorf("missing authorization header"))
				} else {
					next.ServeHTTP(w, r.WithContext(r.Context()))
				}
				return
			}
			id, err := service.Authenticate(token)
			if err != nil {
				if opts.Validate {
					xhttp.WriteInternalError(r.Context(), w, fmt.Errorf("failed to authenticate: %s", err))
				} else {
					next.ServeHTTP(w, r.WithContext(r.Context()))
				}
				return
			}
			if id == "" {
				if opts.Validate {
					xhttp.WriteUnauthorized(w, fmt.Errorf("invalid authentication token"))
				} else {
					next.ServeHTTP(w, r.WithContext(r.Context()))
				}
				return
			}
			role, err := service.Authorize(r.Context(), id)
			if err != nil {
				if opts.Validate {
					xhttp.WriteInternalError(r.Context(), w, fmt.Errorf("authorization error"))
				} else {
					next.ServeHTTP(w, r.WithContext(r.Context()))
				}
				return
			}
			ctx := r.Context()
			ctx = ContextWithID(ctx, id)
			ctx = ContextWithRole(ctx, role)
			next.ServeHTTP(w, r.WithContext(ctx))
		}
		return http.HandlerFunc(fn)
	}
}
