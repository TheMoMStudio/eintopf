//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package auth

import (
	"context"
	"crypto/rsa"
	"errors"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v5"

	"eintopf.info/internal/xerror"
)

// Service defines an authentication service.
type Service interface {
	// Login takes an email and a password. When valid returns a new JWT.
	Login(ctx context.Context, email, password string) (token string, err error)

	// Authenticate takes a JWT and checks wether it's valid.
	// If it is valid. It returns the stored id.
	Authenticate(token string) (id string, err error)

	// Authorize authorizes the given user identifier by returning its
	// authorization role.
	Authorize(ctx context.Context, id string) (role Role, err error)
}

// Authenticator determines wether a set of credentials are valid.
type Authenticator interface {
	// Validate takes an email and password and checks if they are valid.
	// If it is valid it returns a unique identifier corresponding to that
	// identifier.
	// It might return an error, when something unexpected happens during
	// validation.
	Validate(ctx context.Context, email, password string) (id string, err error)
}

// Role defines an authorization role.
type Role string

// Authorization roles.
const (
	RoleInternal  Role = "internal"
	RoleAdmin     Role = "admin"
	RoleModerator Role = "moderator"
	RoleNormal    Role = "normal"
)

// Authorizer checks the authorization for a given identifier.
type Authorizer interface {
	// Role returns the role for the given identifier.
	Role(ctx context.Context, id string) (role Role, err error)
}

// NewService returns a new authentication service.
func NewService(authenticator Authenticator, authorizer Authorizer, privateKey, publicKey []byte, tz *time.Location) (Service, error) {
	signKey, verifyKey, err := ParseAuthKeys(privateKey, publicKey)
	if err != nil {
		return nil, err
	}

	return &service{authenticator, authorizer, verifyKey, signKey, tz}, nil
}

type service struct {
	authenticator Authenticator
	authorizer    Authorizer

	verifyKey *rsa.PublicKey
	signKey   *rsa.PrivateKey
	tz        *time.Location
}

// ErrInvalidCredentials is the error, that gets returned by the login method, when
// the provided credentials where invalid.
var ErrInvalidCredentials = fmt.Errorf("invalid credentials")

// ErrDeactivated indicates, that the user is deactivated.
var ErrDeactivated = fmt.Errorf("deactivated")

func (s *service) Login(ctx context.Context, email, password string) (string, error) {
	id, err := s.authenticator.Validate(ctx, email, password)
	if err != nil {
		return "", err
	}
	if id == "" {
		return "", ErrInvalidCredentials
	}

	// Create a new token object, specifying signing method and the claims
	// you would like it to contain.
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, jwt.MapClaims{
		"nbf": time.Date(2015, 10, 10, 12, 0, 0, 0, s.tz).Unix(),
		"id":  id,
	})

	// Sign and get the complete encoded token as a string using the secret
	tokenString, err := token.SignedString(s.signKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func (s *service) Authenticate(tokenStr string) (string, error) {
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		// since we only use the one private key to sign the tokens,
		// we also only use its public counter part to verify
		return s.verifyKey, nil
	})

	switch {
	case token == nil:
		return "", err
	case token.Valid:
		claims, ok := token.Claims.(jwt.MapClaims)
		if !ok {
			return "", nil
		}
		id, ok := claims["id"].(string)
		if !ok {
			return "", nil
		}
		return id, nil
	case errors.Is(err, jwt.ErrTokenMalformed):
		return "", err
	case errors.Is(err, jwt.ErrTokenSignatureInvalid):
		return "", err
	case errors.Is(err, jwt.ErrTokenExpired) || errors.Is(err, jwt.ErrTokenNotValidYet):
		return "", err
	default:
		return "", err
	}
}

// ErrUnauthorized indicates an unauthorized action.
var ErrUnauthorized = xerror.AuthError{Err: fmt.Errorf("unauthorized")}

func (s *service) Authorize(ctx context.Context, id string) (Role, error) {
	return s.authorizer.Role(ctx, id)
}
