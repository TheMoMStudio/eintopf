//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package auth_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"eintopf.info/service/auth"
)

var privateKey = []byte(`-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQCGGYYvHAZvjOVC6NtJNu42nwb1tvl1bhmgpwxg5gpnce0cuGK8
N4kky86sOpuUDmRCCoXCyR8l5wEE+XOluf0NOIEg8cA0w1+je60bVztDCK/IAlI9
szfdV7s0sFVgvb+SqoR6ADxmdPKnsIpqQfAYOSzwjMm/yinReyUO0mDXNQIDAQAB
AoGAFiMyjqZevZ2R32jXgo+p5aR9HqU1K1igrG/HUFvJjeg1z4705cN5RXTLyozm
UBvMG0bnbA+Nr9EhzvNrTwmUJ5H/7cXHEs0wOVO6y86bYO8XtP6Dfj2CvB704Zff
mcjfROuGolt4PUODlJnZh71ZK4FXnBXfugfcSzj7Og8K1+ECQQD0+p6FEiICLM9l
HrvZ85yFgZuRlzLo3AHuNIzpzhx4rRd1aijWhIGdlyg4hm0LJ0oCWK9YEJ0ce0yE
W2vbmg69AkEAjCHtaGCK543eMWLSdDBy5O/Ye2oTi5uKWm32ezsnZBF3/oZibw7J
kJBtYXx2Y+8MAeTzNE09GFe5lPtNZUHN2QJBAOAyECrGWntVGQR46P/g06jW4VGP
Zxb2aYnfa+p5J1NFTYe1/OO9ZoWblUKNu3OOpEubb/UPV0l+iZtDs2TJC50CQD/Y
WVUb301+aoRvtNjxFffOewBHpR4PQKrQvOMKYXkLKHOTgJd+0kEGPH+U+E+xovPd
/xyOME698TS6hlmi8IkCQCfRNndp4oBTtxoZVETxWw/6zZzoRduR7Ut2Yo3VSckf
rf7FI8Mzm5QV0eQOdwmHWKn1RRlJv+bOXBFttsrpgJo=
-----END RSA PRIVATE KEY-----`)

var publicKey = []byte(`-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCGGYYvHAZvjOVC6NtJNu42nwb1
tvl1bhmgpwxg5gpnce0cuGK8N4kky86sOpuUDmRCCoXCyR8l5wEE+XOluf0NOIEg
8cA0w1+je60bVztDCK/IAlI9szfdV7s0sFVgvb+SqoR6ADxmdPKnsIpqQfAYOSzw
jMm/yinReyUO0mDXNQIDAQAB
-----END PUBLIC KEY-----`)

type mockedAuthenticator struct{}

func (m *mockedAuthenticator) Validate(ctx context.Context, email, password string) (string, error) {
	if email == "admin@example.com" && password == "password" {
		return "id", nil
	}

	if email == "error" || password == "error" {
		return "", fmt.Errorf("error")
	}

	return "", nil
}

type mockedAuthorizer struct{}

func (m *mockedAuthorizer) Role(ctx context.Context, id string) (auth.Role, error) {
	switch id {
	case "admin":
		return auth.RoleAdmin, nil
	case "normal":
		return auth.RoleNormal, nil
	case "error":
		return auth.RoleNormal, fmt.Errorf("error")
	default:
		return "", nil
	}
}

func TestAuthService(t *testing.T) {
	authService, err := auth.NewService(&mockedAuthenticator{}, &mockedAuthorizer{}, privateKey, publicKey, time.UTC)
	if err != nil {
		t.Fatalf("expected auth.NewService to succeed. got %s", err)
	}

	t.Run("invalid private/public keys", func(tt *testing.T) {
		_, err := auth.NewService(&mockedAuthenticator{}, &mockedAuthorizer{}, []byte(""), []byte(""), time.UTC)
		if err == nil {
			tt.Fatal("expected auth.NewService to return an error")
		}
	})

	t.Run("login/authentication with invalid credentials", func(tt *testing.T) {
		token, err := authService.Login(context.Background(), "foo", "bar")
		if err != auth.ErrInvalidCredentials {
			tt.Fatal("expected Login to return an err")
		}
		if token != "" {
			tt.Fatal("expected token to be empty")
		}
		id, err := authService.Authenticate(token)
		if err == nil {
			tt.Fatalf("expected Authenticate to return an error. got %s", err)
		}
		if id != "" {
			tt.Fatal("expected token to not be authenticated")
		}
	})

	t.Run("login with valid credentials", func(tt *testing.T) {
		token, err := authService.Login(context.Background(), "admin@example.com", "password")
		if err != nil {
			tt.Fatalf("expected Login not return an error. got %s", err)
		}
		if token == "" {
			tt.Fatal("expected token to not be empty")
		}

		id, err := authService.Authenticate(token)
		if err != nil {
			tt.Fatalf("expected Authenticate not return an error. got %s", err)
		}
		if id == "" {
			tt.Fatal("expected token to be authenticated")
		}
	})

	t.Run("Authorize calls authorizer", func(tt *testing.T) {
		role, err := authService.Authorize(context.Background(), "admin")
		if err != nil {
			tt.Fatalf("expected Authorize not return an error. got %s", err)
		}
		if role != auth.RoleAdmin {
			tt.Fatalf("expected role to be 'admin'. got %s", role)
		}
	})
}
