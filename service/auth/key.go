//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package auth

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"

	"github.com/golang-jwt/jwt/v5"
)

// ParseAuthKeys parses a PEM encoded private and public rsa key pair.
func ParseAuthKeys(privateKey, publicKey []byte) (*rsa.PrivateKey, *rsa.PublicKey, error) {
	priv, err := jwt.ParseRSAPrivateKeyFromPEM(privateKey)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to parse private key: %s", err)
	}

	pub, err := jwt.ParseRSAPublicKeyFromPEM(publicKey)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to parse public key: %s", err)
	}

	return priv, pub, nil
}

// CreateAuthKeys creates a PEM encoded private and public rsa key pair.
func CreateAuthKeys() ([]byte, []byte, error) {
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, nil, err
	}
	privateKeyBytes, err := x509.MarshalPKCS8PrivateKey(privateKey)
	if err != nil {
		return nil, nil, err
	}
	privateKeyBlock := pem.EncodeToMemory(
		&pem.Block{
			Type:  "PRIVATE KEY",
			Bytes: privateKeyBytes,
		},
	)

	publicKeyBytes, err := x509.MarshalPKIXPublicKey(&privateKey.PublicKey)
	if err != nil {
		return nil, nil, err
	}
	publicKeyBlock := pem.EncodeToMemory(
		&pem.Block{
			Type:  "PUBLIC KEY",
			Bytes: publicKeyBytes,
		},
	)
	return privateKeyBlock, publicKeyBlock, nil
}
