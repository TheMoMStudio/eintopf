//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package auth_test

import (
	"testing"

	"eintopf.info/service/auth"
)

func TestGenerateParse(t *testing.T) {
	privateKey, publicKey, err := auth.CreateAuthKeys()
	if err != nil {
		t.Fatal(err)
	}
	_, _, err = auth.ParseAuthKeys(privateKey, publicKey)
	if err != nil {
		t.Fatal(err)
	}
}
