//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package auth_test

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"eintopf.info/internal/xhttptest"
	"eintopf.info/service/auth"
)

type authService struct{}

func (a *authService) Login(ctx context.Context, email, password string) (string, error) {
	if email == "admin@example.com" && password == "password" {
		return "valid", nil
	}
	return "", auth.ErrInvalidCredentials
}

func (a *authService) Authenticate(token string) (string, error) {
	switch token {
	case "valid":
		return "id", nil
	case "errorAuthorization":
		return "error", nil
	case "error":
		return "", fmt.Errorf("error")
	default:
		return "", nil
	}
}

func (a *authService) Authorize(ctx context.Context, id string) (auth.Role, error) {
	switch id {
	case "id":
		return auth.RoleAdmin, nil
	case "error":
		return auth.RoleNormal, fmt.Errorf("error")
	default:
		return "", nil
	}
}

func getTestHandler() http.HandlerFunc {
	fn := func(w http.ResponseWriter, req *http.Request) {
		w.WriteHeader(http.StatusOK)
	}
	return http.HandlerFunc(fn)
}

func mustMarshal(v interface{}) string {
	data, err := json.Marshal(v)
	if err != nil {
		panic("mustMarshal failed")
	}
	return string(data)
}

func TestRouter(t *testing.T) {
	tests := []xhttptest.HttpTest{
		{
			Name:       "LoginValidCredentials",
			URI:        "/login",
			Method:     "POST",
			Body:       mustMarshal(&auth.Credentials{Email: "admin@example.com", Password: "password"}),
			WantBody:   `{"token":"valid","id":"id","role":"admin"}`,
			WantStatus: 200,
		}, {
			Name:       "LoginInvalidCredentials",
			URI:        "/login",
			Method:     "POST",
			Body:       mustMarshal(&auth.Credentials{Email: "u", Password: "p"}),
			WantBody:   `{"error":"invalid credentials"}`,
			WantStatus: 400,
		}, {
			Name:       "OptionsHandler",
			URI:        "/login",
			Method:     "OPTIONS",
			WantStatus: 200,
		},
	}

	xhttptest.TestRouter(t, auth.Router(&authService{}), tests)
}

func TestMiddleware(t *testing.T) {
	tests := []xhttptest.HttpTest{
		{
			Name:       "valid authentication header",
			Method:     "GET",
			Headers:    map[string]string{"Authorization": "valid"},
			WantBody:   "",
			WantStatus: 200,
		}, {
			Name:       "missing authentication header",
			Method:     "GET",
			WantBody:   `{"error":"missing authorization header"}`,
			WantStatus: 401,
		}, {
			Name:       "invalid authentication header",
			Method:     "GET",
			Headers:    map[string]string{"Authorization": "invalid"},
			WantBody:   `{"error":"invalid authentication token"}`,
			WantStatus: 401,
		}, {
			Name:       "error during authentication",
			Method:     "GET",
			Headers:    map[string]string{"Authorization": "error"},
			WantBody:   `{"error":"internal error"}`,
			WantStatus: 500,
		}, {
			Name:       "error during authorization",
			Method:     "GET",
			Headers:    map[string]string{"Authorization": "errorAuthorization"},
			WantBody:   `{"error":"internal error"}`,
			WantStatus: 500,
		},
	}

	xhttptest.TestMiddleware(t, auth.Middleware(&authService{}), tests)
}

func testMiddlewareAddsInformationToContext(t *testing.T, middleware func(next http.Handler) http.Handler) {
	testHandlerCalled := false
	getTestHandler := func() http.HandlerFunc {
		fn := func(w http.ResponseWriter, req *http.Request) {
			testHandlerCalled = true
			id, err := auth.UserIDFromContext(req.Context())
			if err != nil && id != "id" {
				t.Fatalf("expected id in context to be 'id'. got %s", id)
			}
			role, err := auth.RoleFromContext(req.Context())
			if err != nil && role != auth.RoleAdmin {
				t.Fatalf("expected role in context to be 'admin'. got %s", role)
			}
		}
		return http.HandlerFunc(fn)
	}
	ts := httptest.NewServer(middleware(getTestHandler()))
	defer ts.Close()

	client := &http.Client{}

	req, err := http.NewRequest("GET", ts.URL, nil)
	if err != nil {
		t.Fatalf("expected no error from http.NewRequest. got %s", err)
	}
	req.Header.Set("Authorization", "valid")
	_, err = client.Do(req)
	if err != nil {
		t.Fatalf("expected no error from client.Do. got %s", err)
	}

	if !testHandlerCalled {
		t.Fatal("expected nextHandler to be called")
	}
}
func TestMiddlewareAddsInformationToContext(t *testing.T) {
	testMiddlewareAddsInformationToContext(t, auth.Middleware(&authService{}))
}

func TestMiddlewareNoValidationAddsInformationToContext(t *testing.T) {
	testMiddlewareAddsInformationToContext(t, auth.MiddlewareWithOpts(&authService{}, auth.MiddlewareOpts{Validate: false}))
}
