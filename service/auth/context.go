//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package auth

import (
	"context"
	"fmt"
)

type ctxKey string

const (
	ctxKeyID   = ctxKey("id")
	ctxKeyRole = ctxKey("role")
)

// ContextWithID adds the given id to the context.
func ContextWithID(ctx context.Context, id string) context.Context {
	return context.WithValue(ctx, ctxKeyID, id)
}

// ContextWithRole adds the given role to the context.
func ContextWithRole(ctx context.Context, role Role) context.Context {
	return context.WithValue(ctx, ctxKeyRole, role)
}

// ErrUserIDFromContext is the error, that gets returned by
// UserIDFromRequest, when the retrieval fails.
var ErrUserIDFromContext = fmt.Errorf("failed to retrieve user id from context")

// UserIDFromContext retrieves the user id from the given context.
// Returns an error on failure.
func UserIDFromContext(ctx context.Context) (string, error) {
	userID, ok := ctx.Value(ctxKeyID).(string)
	if !ok || userID == "" {
		return "", ErrUserIDFromContext
	}
	return userID, nil
}

// ErrRoleFromContext is the error, that gets returned by
// RoleFromRequest, when the retrieval fails.
var ErrRoleFromContext = fmt.Errorf("failed to retrieve authorization role from context")

// RoleFromContext retrieves the authorization role from the given context.
// Returns an error on failure.
func RoleFromContext(ctx context.Context) (Role, error) {
	role, ok := ctx.Value(ctxKeyRole).(Role)
	if !ok || role == "" {
		return "", ErrRoleFromContext
	}
	return role, nil
}
