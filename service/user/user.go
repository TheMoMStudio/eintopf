//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package user

import (
	"context"
	"fmt"
	"time"

	"golang.org/x/crypto/bcrypt"

	"eintopf.info/internal/crud"
	"eintopf.info/internal/xerror"
	"eintopf.info/service/auth"
)

type user interface {
	getID() string
	getEmail() string
	getNickname() string
	getPassword() string
}

// NewUser defines the data for a new user.
type NewUser struct {
	Email    string    `json:"email"`
	Nickname string    `json:"nickname"`
	Password string    `json:"password"`
	Role     auth.Role `json:"role"`
}

func (n *NewUser) getID() string {
	return ""
}

func (n *NewUser) getEmail() string {
	return n.Email
}

func (n *NewUser) getNickname() string {
	return n.Nickname
}

func (n *NewUser) getPassword() string {
	return n.Password
}

// User defines the user model by embedding NewUser along with additional
// fields, that can't be set by the user during creation.
type User struct {
	ID          string    `json:"id" db:"id"`
	Deactivated bool      `json:"deactivated" db:"deactivated"`
	CreatedAt   time.Time `json:"createdAt" db:"created_at"`
	Email       string    `json:"email" db:"email"`
	Nickname    string    `json:"nickname" db:"nickname"`
	Password    string    `json:"password" db:"password"`
	Role        auth.Role `json:"role" db:"role"`
}

// UserFromNewUser converts a NewUser to a User.
func UserFromNewUser(newUser *NewUser, id string) *User {
	return &User{
		ID:          id,
		Deactivated: false,
		CreatedAt:   time.Now(),
		Email:       newUser.Email,
		Nickname:    newUser.Nickname,
		Password:    newUser.Password,
		Role:        newUser.Role,
	}
}

func (u User) Identifier() string {
	return u.ID
}

func (u *User) getID() string {
	return u.ID
}

func (u *User) getEmail() string {
	return u.Email
}

func (u *User) getNickname() string {
	return u.Nickname
}

func (u *User) getPassword() string {
	return u.Password
}

// SortOrder defines the order of sorting.
type SortOrder string

// Possible values for SortOrder.
const (
	OrderAsc  = SortOrder("ASC")
	OrderDesc = SortOrder("DESC")
)

// FindParams defines parameters used by the Find method.
type FindParams struct {
	Offset int64
	Limit  int64

	Sort  string
	Order SortOrder

	Filters *FindFilters
}

// FindFilters defines the possible filters for the find method.
type FindFilters struct {
	ID           *string    `db:"id"`
	NotID        *string    `db:"id"`
	Deactivated  *bool      `db:"deactivated"`
	Email        *string    `db:"email"`
	Nickname     *string    `db:"nickname"`
	LikeNickname *string    `db:"nickname"`
	Password     *string    `db:"password"`
	Role         *auth.Role `db:"role"`
}

// Storer defines a service for CRUD operations on the user model.
type Storer = crud.Storer[NewUser, User, FindFilters]

// Service defines a service to manage users.
type Service interface {
	Storer
	auth.Authenticator
	auth.Authorizer
}

type service struct {
	store Storer
}

// NewService returns a new user service.
func NewService(store Storer) Service {
	return &service{store}
}

func (s *service) Create(ctx context.Context, user *NewUser) (*User, error) {
	if err := s.checkUser(ctx, user); err != nil {
		return nil, err
	}

	hashedPW, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	user.Password = string(hashedPW)
	return s.store.Create(ctx, user)
}

func (s *service) Update(ctx context.Context, user *User) (*User, error) {
	if user.Password == "" {
		// It should be able to update the user without providing a password. In
		// this case get the password from the database.
		oldUser, err := s.store.FindByID(auth.ContextWithRole(ctx, auth.RoleInternal), user.ID)
		if err != nil {
			return nil, err
		}
		if oldUser != nil {
			user.Password = oldUser.Password
		}
	} else {
		// Make sure the password gets hashed, when the user is updating the
		// password.
		hashedPW, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
		if err != nil {
			return nil, err
		}
		user.Password = string(hashedPW)
	}
	if err := s.checkUser(ctx, user); err != nil {
		return nil, err
	}
	return s.store.Update(ctx, user)
}
func (s *service) Delete(ctx context.Context, id string) error {
	return s.store.Delete(ctx, id)
}
func (s *service) FindByID(ctx context.Context, id string) (*User, error) {
	return s.store.FindByID(ctx, id)
}
func (s *service) Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*User, int, error) {
	return s.store.Find(ctx, params)
}

func (s *service) Validate(ctx context.Context, email, password string) (string, error) {
	users, _, err := s.store.Find(auth.ContextWithRole(ctx, auth.RoleInternal), &crud.FindParams[FindFilters]{
		Filters: &FindFilters{Email: &email},
	})

	if err != nil {
		return "", err
	}
	if len(users) != 1 {
		return "", err
	}
	user := users[0]
	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)); err != nil {
		return "", nil
	}
	if user.Deactivated {
		return "", auth.ErrDeactivated
	}
	return user.ID, nil
}

func (s *service) Role(ctx context.Context, id string) (auth.Role, error) {
	user, err := s.FindByID(auth.ContextWithRole(ctx, auth.RoleInternal), id)
	if err != nil {
		return auth.RoleNormal, err
	}
	if user == nil {
		return auth.RoleNormal, fmt.Errorf("user not found")
	}
	return user.Role, nil
}

var ErrEmailAlreadyExists = xerror.BadInputError{Err: fmt.Errorf("email already exists")}
var ErrNicknameAlreadyExists = xerror.BadInputError{Err: fmt.Errorf("nickname already exists")}

func (s *service) checkUser(ctx context.Context, u user) error {
	if u.getEmail() == "" {
		return xerror.BadInputError{Err: fmt.Errorf("empty email")}
	}
	if u.getPassword() == "" {
		return xerror.BadInputError{Err: fmt.Errorf("empty password")}
	}
	if u.getNickname() == "" {
		return xerror.BadInputError{Err: fmt.Errorf("empty nickname")}
	}

	id := u.getID()
	email := u.getEmail()
	existingUsers, _, err := s.store.Find(auth.ContextWithRole(ctx, auth.RoleInternal), &crud.FindParams[FindFilters]{
		Filters: &FindFilters{Email: &email, NotID: &id},
	})
	if err != nil {
		return err
	}
	if len(existingUsers) > 0 {
		return ErrEmailAlreadyExists
	}

	nickname := u.getNickname()
	existingUsers, _, err = s.store.Find(auth.ContextWithRole(ctx, auth.RoleInternal), &crud.FindParams[FindFilters]{
		Filters: &FindFilters{Nickname: &nickname, NotID: &id},
	})
	if err != nil || len(existingUsers) > 0 {
		return ErrNicknameAlreadyExists
	}
	return nil
}
