//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package user

import (
	"context"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
)

// CreateAdminAccount creates a new admin account, if none exists with the given
// email address.
func CreateAdminAccount(s Service, email, password string) error {
	ctx := auth.ContextWithRole(context.Background(), auth.RoleAdmin)
	users, _, err := s.Find(ctx, &crud.FindParams[FindFilters]{
		Filters: &FindFilters{Email: &email},
	})
	if err != nil {
		return err
	}
	if len(users) > 0 {
		return nil
	}

	_, err = s.Create(ctx, &NewUser{
		Email:    email,
		Nickname: email,
		Password: password,
		Role:     auth.RoleAdmin,
	})
	return err
}
