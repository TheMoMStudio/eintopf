//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package user

import (
	"github.com/go-chi/chi/v5"

	"eintopf.info/internal/crud"
	"eintopf.info/internal/xhttp"
	"eintopf.info/service/auth"
)

// Router returns a new http router that handles crud user requests for a given
// user service.
func Router(storer Storer, authService auth.Service) func(chi.Router) {
	handler := crud.NewHandler[NewUser, User, FindFilters](storer)
	return func(r chi.Router) {
		r.Options("/", xhttp.CorsHandler)

		// swagger:route GET /users/ users findUsers
		//
		// Retrieves all users.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: findUsersResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Get("/", handler.Find)

		// swagger:route POST /users/ user createUser
		//
		// Creates a new new user.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: createUserResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Post("/", handler.Create)

		r.Options("/{id}", xhttp.CorsHandler)

		// swagger:route GET /users/{id} user findUser
		//
		// Finds the user with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: findUserResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Get("/{id}", handler.FindByID)

		// swagger:route PUT /users/{id} user updateUser
		//
		// Updates the user with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: updateUserResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable

		r.With(auth.Middleware(authService)).
			Put("/{id}", handler.Update)

		// swagger:route DELETE /users/{id} user deleteUser
		//
		// Deletes the user with the given id.
		//     Security:
		//       bearer: []
		//
		//     Responses:
		//       200: deleteUserResponse
		//       400: badRequest
		//       401: unauthorizedError
		//       500: internalError
		//       503: serviceUnavailable
		r.With(auth.Middleware(authService)).
			Delete("/{id}", handler.Delete)
	}
}

type router struct {
	s Service
}

// swagger:parameters findUsers
type findRequest struct {
	// in:query
	Start int64 `json:"_start"`
	// in:query
	End int64 `json:"_end"`
	// in:query
	Sort string `json:"_sort"`
	// in:query
	Order SortOrder `json:"_order"`

	// in:query
	FilterID *string `json:"id"`
	// in:query
	FilterDeactivated *bool `json:"deactivated"`
	// in:query
	FilterEmail *string `json:"email"`
	// in:query
	FilterNickname *string `json:"nickname"`
	// in:query
	FilterRole *auth.Role `json:"role"`
}

// swagger:response findUsersResponse
type findResponse struct {

	// in:body
	Users []User

	// in:header
	TotalUsers int `json:"x-total-count"`
}

// swagger:parameters createUser
type createRequest struct {

	// in:body
	NewUser *NewUser
}

// swagger:response createUserResponse
type createResponse struct {

	// in:body
	User *User
}

// swagger:parameters updateUser
type updateRequest struct {

	// in:body
	User *User
}

//swagger:response updateUserResponse
type updateResponse struct {

	// in:body
	User *User
}

// swagger:parameters deleteUser
type deleteRequest struct {

	// in:query
	ID string `json:"id"`
}

// swagger:response deleteUserResponse
type deleteResponse struct{}

// swagger:parameters findUser
type findByIDRequest struct {

	// in:query
	ID string `json:"id"`
}

// swagger:response findUserResponse
type findByIDResponse struct {

	// in:body
	User *User
}
