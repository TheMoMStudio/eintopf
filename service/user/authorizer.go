//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package user

import (
	"context"
	"fmt"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
)

type authorizer struct {
	store Storer
}

func NewAuthorizer(store Storer) Storer {
	return &authorizer{store}
}

// Create can only be called
// - if the logged in user role is moderator, admin or internal
// AND
// - if the role of the loggedin user is less than the new role
// Only returns the password field, when the internal role is given.
func (a *authorizer) Create(ctx context.Context, newUser *NewUser) (*User, error) {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return nil, auth.ErrUnauthorized
	}
	if role == auth.RoleNormal {
		return nil, auth.ErrUnauthorized
	}
	err = checkRole(newUser.Role, role)
	if err != nil {
		return nil, err
	}
	user, err := a.store.Create(ctx, newUser)
	if err != nil {
		return nil, err
	}
	if role != auth.RoleInternal {
		user.Password = ""
	}
	return user, nil
}

// Update can only be called
// - if the loggedin role is internal
// - if the loggedin role is admin
// - if the loggedin user updates itself
// Only returns the password field, when the internal role is given.
func (a *authorizer) Update(ctx context.Context, user *User) (*User, error) {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return nil, auth.ErrUnauthorized
	}
	if role != auth.RoleInternal {
		id, err := auth.UserIDFromContext(ctx)
		if err != nil {
			return nil, auth.ErrUnauthorized
		}
		err = checkRole(user.Role, role)
		if err != nil {
			return nil, err
		}
		if role == auth.RoleNormal {
			if id != user.ID {
				// Only the internal and admin role are allowed to modify users
				// other than themself.
				return nil, auth.ErrUnauthorized
			}
		}
		if role == auth.RoleModerator {
			oldUser, err := a.store.FindByID(ctx, user.ID)
			if err != nil {
				return nil, err
			}
			if oldUser == nil {
				return nil, fmt.Errorf("cant find user with id: %s", user.ID)
			}
			switch oldUser.Role {
			case auth.RoleNormal:
				if oldUser.ID != user.ID ||
					oldUser.Email != user.Email ||
					oldUser.Nickname != user.Nickname {
					// Moderators can only change the deactivated and role fields.
					return nil, auth.ErrUnauthorized
				}
			case auth.RoleModerator:
				// A moderator can still update themself.
				if id != user.ID {
					return nil, auth.ErrUnauthorized
				}
			case auth.RoleAdmin:
				return nil, auth.ErrUnauthorized
			}
		}
	}

	oldUser, err := a.store.FindByID(ctx, user.ID)
	if err != nil {
		return nil, err
	}
	if oldUser == nil {
		return nil, fmt.Errorf("cant find user with id: %s", user.ID)
	}
	if oldUser.Deactivated != user.Deactivated && role == auth.RoleNormal {
		return nil, auth.ErrUnauthorized
	}

	user, err = a.store.Update(ctx, user)
	if err != nil {
		return nil, err
	}
	if role != auth.RoleInternal {
		user.Password = ""
	}
	return user, nil
}

// Delete can only be called
// - if the loggedin user is an admin or has the internal role
// - if the loogedin user is deleting itself
func (a *authorizer) Delete(ctx context.Context, id string) error {
	if err := adminOrSelf(ctx, id); err != nil {
		return err
	}
	return a.store.Delete(ctx, id)
}

// FindByID can only be called
// - if the loggedin user is an admin or has the internal role
// - if the loogedin user is requesting itself
// Only returns the password field, when the internal role is given.
func (a *authorizer) FindByID(ctx context.Context, id string) (*User, error) {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return nil, auth.ErrUnauthorized
	}
	if role != auth.RoleInternal && role != auth.RoleAdmin && role != auth.RoleModerator {
		userID, err := auth.UserIDFromContext(ctx)
		if err != nil {
			return nil, auth.ErrUnauthorized
		}
		if id != userID {
			return nil, auth.ErrUnauthorized
		}
	}
	user, err := a.store.FindByID(ctx, id)
	if err != nil {
		return nil, err
	}
	if user == nil {
		return nil, nil
	}
	if role != auth.RoleInternal {
		user.Password = ""
	}
	return user, nil
}

// Find can only be called
// - if the loggedin user has the moderator, admin or internal role
// Only the id and nickname field get returned
// - if the user is a normal user
// - if the user is a moderator user for all admin users
// Only returns the password field, when the internal role is given.
func (a *authorizer) Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*User, int, error) {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return nil, 0, auth.ErrUnauthorized
	}
	users, total, err := a.store.Find(ctx, params)
	if err != nil {
		return nil, 0, err
	}
	users2 := make([]*User, 0)

	for _, user := range users {
		switch role {
		case auth.RoleInternal:
			users2 = append(users2, user)
		case auth.RoleAdmin:
			user.Password = ""
			users2 = append(users2, user)
		case auth.RoleModerator:
			user.Password = ""
			users2 = append(users2, user)
		case auth.RoleNormal:
			users2 = append(users2, &User{
				ID: user.ID, Nickname: user.Nickname,
			})
		}
	}
	return users2, total, nil
}

// checkRole checks that a given role is valid for a given author role.
func checkRole(role auth.Role, author auth.Role) error {
	switch role {
	case auth.RoleInternal:
		// A user with the internal role should never exist.
		return auth.ErrUnauthorized
	case auth.RoleAdmin:
		if author == auth.RoleNormal || author == auth.RoleModerator {
			return auth.ErrUnauthorized
		}
	case auth.RoleModerator:
		if author == auth.RoleNormal {
			return auth.ErrUnauthorized
		}
	}
	return nil
}

// adminOrSelf checks if the loggedin user
// - has an internal role
// - has an admin role
// - updates itself
// Returns auth.ErrUnauthorized otherwise.
func adminOrSelf(ctx context.Context, userID string) error {
	role, err := auth.RoleFromContext(ctx)
	if err != nil {
		return auth.ErrUnauthorized
	}
	if role == auth.RoleInternal || role == auth.RoleAdmin {
		return nil
	}
	id, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return auth.ErrUnauthorized
	}
	if id == userID {
		return nil
	}
	return auth.ErrUnauthorized
}
