//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package user_test

import (
	"context"
	"testing"

	"eintopf.info/service/auth"
	"eintopf.info/service/user"
)

func TestUserServiceCreate(t *testing.T) {
	ctx := context.Background()

	store := user.NewMemoryStore()
	userService := user.NewService(store)

	existingUser, err := store.Create(ctx, &user.NewUser{Email: "existing", Nickname: "existing"})
	if err != nil {
		t.Fatalf("userService.Create() failed: %s", err)
	}

	// Check for empty email
	_, err = userService.Create(ctx, &user.NewUser{Email: "", Nickname: "foo", Password: "foo"})
	if err == nil || err.Error() != "empty email" {
		t.Errorf("userService.UseInvite should fail with empty email: %s", err)
	}

	// Check for empty nickname
	_, err = userService.Create(ctx, &user.NewUser{Email: "foo", Nickname: "", Password: "foo"})
	if err == nil || err.Error() != "empty nickname" {
		t.Errorf("userService.UseInvite should fail with empty nickname: %s", err)
	}

	// Check for empty password
	_, err = userService.Create(ctx, &user.NewUser{Email: "foo", Nickname: "foo", Password: ""})
	if err == nil || err.Error() != "empty password" {
		t.Errorf("userService.UseInvite should fail with empty password: %s", err)
	}

	// Check for existing email
	_, err = userService.Create(ctx, &user.NewUser{Email: existingUser.Email, Nickname: "blub", Password: "123"})
	if err == nil || err.Error() != "email already exists" {
		t.Errorf("userService.UseInvite should fail with existing email: %s", err)
	}

	// Check for existing nickname
	_, err = userService.Create(ctx, &user.NewUser{Email: "foo", Nickname: existingUser.Nickname, Password: "123"})
	if err == nil || err.Error() != "nickname already exists" {
		t.Errorf("userService.UseInvite should fail with existing nickname: %s", err)
	}

	_, err = userService.Create(ctx, &user.NewUser{Email: "foo", Nickname: "foo", Password: "123"})
	if err != nil {
		t.Errorf("userService.Create failed: %s", err)
	}
}

func TestUserServicePassword(t *testing.T) {
	ctx := context.Background()

	store := user.NewMemoryStore()
	userService := user.NewService(store)

	// It creates a hashed password
	u, err := userService.Create(ctx, &user.NewUser{Email: "foo", Nickname: "foo", Password: "123"})
	if err != nil {
		t.Errorf("userService.Create failed: %s", err.Error())
	}
	if u.Password == "123" {
		t.Errorf("password should be hashed: got %s", u.Password)
	}

	// Validate uses hashed password
	id, err := userService.Validate(ctx, "foo", "123")
	if err != nil {
		t.Errorf("userService.Validate failed: %s", err.Error())
	}
	if id == "" {
		t.Errorf("user is invalid")
	}
}

func TestUpdatePassword(t *testing.T) {
	store := user.NewMemoryStore()
	service := user.NewService(user.NewAuthorizer(store))

	internalCtx := auth.ContextWithRole(context.Background(), auth.RoleInternal)
	u, err := service.Create(internalCtx, &user.NewUser{
		Email:    "email",
		Nickname: "nickname",
		Password: "foo",
	})
	if err != nil {
		t.Fatalf("service.Create failed: %s", err)
	}
	id, err := service.Validate(internalCtx, "email", "foo")
	if err != nil {
		t.Errorf("service.Validate: %s", err)
	}
	if id == "" {
		t.Error("password should be 'foo'")
	}

	ctx := auth.ContextWithID(auth.ContextWithRole(context.Background(), auth.RoleNormal), u.ID)
	u = &user.User{ID: u.ID, Email: u.Email, Nickname: u.Nickname, Password: "bar"}
	u, err = service.Update(ctx, u)
	if err != nil {
		t.Fatalf("service.Update failed: %s", err)
	}
	id, err = service.Validate(context.Background(), "email", "bar")
	if err != nil {
		t.Errorf("service.Validate: %s", err)
	}
	if id == "" {
		t.Error("password should be 'bar' after updating")
	}
}

func TestUpdatePasswordDoesntReset(t *testing.T) {
	store := user.NewMemoryStore()
	service := user.NewService(user.NewAuthorizer(store))

	internalCtx := auth.ContextWithRole(context.Background(), auth.RoleInternal)
	u, err := service.Create(internalCtx, &user.NewUser{
		Email:    "email",
		Nickname: "nickname",
		Password: "foo",
	})
	if err != nil {
		t.Fatalf("service.Create failed: %s", err)
	}
	id, err := service.Validate(context.Background(), "email", "foo")
	if err != nil {
		t.Errorf("service.Validate: %s", err)
	}
	if id == "" {
		t.Error("password should be 'foo'")
	}

	ctx := auth.ContextWithID(auth.ContextWithRole(context.Background(), auth.RoleNormal), u.ID)
	_, err = service.Update(ctx, &user.User{
		ID:       u.ID,
		Email:    u.Email,
		Nickname: u.Nickname,
		Password: "",
	})
	if err != nil {
		t.Fatalf("service.Update failed: %s", err)
	}
	id, err = service.Validate(context.Background(), "email", "foo")
	if err != nil {
		t.Errorf("service.Validate: %s", err)
	}
	if id == "" {
		t.Error("password should be 'foo' after updating")
	}
}
