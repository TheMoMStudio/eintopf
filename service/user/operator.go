// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package user

import (
	"context"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
	"eintopf.info/service/oqueue"
)

type Operator struct {
	storer Storer
	queue  oqueue.Service
}

func NewOperator(storer Storer, queue oqueue.Service) *Operator {
	return &Operator{storer: storer, queue: queue}
}

type CreateOperation struct {
	User   *User
	userID string
}

func NewCreateOperation(user *User, userID string) CreateOperation {
	return CreateOperation{user, userID}
}

func (o CreateOperation) UserID() string {
	return o.userID
}

func (i *Operator) Create(ctx context.Context, newUser *NewUser) (*User, error) {
	user, err := i.storer.Create(ctx, newUser)
	if err != nil {
		return user, err
	}
	if user == nil {
		return nil, nil
	}

	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return nil, err
	}
	i.queue.AddOperation(CreateOperation{
		User:   user,
		userID: userID,
	})

	return user, nil
}

type UpdateOperation struct {
	User   *User
	userID string
}

func NewUpdateOperation(user *User, userID string) UpdateOperation {
	return UpdateOperation{user, userID}
}

func (o UpdateOperation) UserID() string {
	return o.userID
}

func (i *Operator) Update(ctx context.Context, user *User) (*User, error) {
	user, err := i.storer.Update(ctx, user)
	if err != nil {
		return user, err
	}
	if user == nil {
		return nil, nil
	}

	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return nil, err
	}
	i.queue.AddOperation(UpdateOperation{
		User:   user,
		userID: userID,
	})

	return user, nil
}

type DeleteOperation struct {
	ID     string
	userID string
}

func NewDeleteOperation(id string, userID string) DeleteOperation {
	return DeleteOperation{id, userID}
}

func (o DeleteOperation) UserID() string {
	return o.userID
}

func (i *Operator) Delete(ctx context.Context, id string) error {
	err := i.storer.Delete(ctx, id)
	if err != nil {
		return err
	}

	userID, err := auth.UserIDFromContext(ctx)
	if err != nil {
		return err
	}
	i.queue.AddOperation(DeleteOperation{
		ID:     id,
		userID: userID,
	})

	return nil
}

func (i *Operator) FindByID(ctx context.Context, id string) (*User, error) {
	return i.storer.FindByID(ctx, id)
}

func (i *Operator) Find(ctx context.Context, params *crud.FindParams[FindFilters]) ([]*User, int, error) {
	return i.storer.Find(ctx, params)
}
