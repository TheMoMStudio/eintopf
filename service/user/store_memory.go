//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package user

import (
	"eintopf.info/internal/crud"
)

// NewMemoryStore returns a new in memory store.
func NewMemoryStore() *crud.MemoryStore[NewUser, User, FindFilters] {
	return crud.NewMemoryStore[NewUser, User, FindFilters](UserFromNewUser, nil)
}
