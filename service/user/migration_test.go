//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package user_test

import (
	"context"
	"testing"

	"eintopf.info/internal/crud"
	"eintopf.info/service/user"
)

func TestCreateAdminAccount(t *testing.T) {
	email := "foo@ar.de"
	service := user.NewService(user.NewMemoryStore())
	err := user.CreateAdminAccount(service, email, "bar")
	if err != nil {
		t.Errorf("user.CreateAdminAccount(service, foo@bar.de, bar): %s", err)
	}
	users, _, err := service.Find(context.Background(), &crud.FindParams[user.FindFilters]{
		Filters: &user.FindFilters{Email: &email}},
	)
	if err != nil {
		t.Errorf("service.Find: %s", err)
	}
	if len(users) != 1 && users[0].Email != email {
		t.Errorf("user was not created")
	}
}
