//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package user

import (
	"context"

	"github.com/jmoiron/sqlx"

	"eintopf.info/internal/crud"
	"eintopf.info/service/dbmigration"
)

// NewSqlStore returns a new sql db user store.
func NewSqlStore(db *sqlx.DB, migrationService dbmigration.Service) (*SqlStore, error) {
	store := &SqlStore{
		db,
		crud.NewSqlStore(db, table, UserFromNewUser, nil),
	}
	err := store.runMigrations(migrationService)
	if err != nil {
		return nil, err
	}
	return store, nil
}

type SqlStore struct {
	db *sqlx.DB

	*crud.SqlStore[NewUser, User, FindFilters]
}

func (s *SqlStore) runMigrations(migrationService dbmigration.Service) error {
	return migrationService.RunMigrations(context.Background(), []dbmigration.Migration{
		dbmigration.NewMigration("createUserTable", s.createUserTable, nil),
	})
}

func (s *SqlStore) createUserTable(ctx context.Context) error {
	_, err := s.db.ExecContext(ctx, `
        CREATE TABLE IF NOT EXISTS users (
            id varchar(32) NOT NULL PRIMARY KEY UNIQUE,
            deactivated boolean DEFAULT FALSE,
            created_at timestamp,
            email VARCHAR(128),
            nickname VARCHAR(128),
            password VARCHAR(128),
            role VARCHAR(32)
        );
    `)
	return err
}

var table = crud.SqlTable[FindFilters]{
	Table:          "users",
	IDField:        "id",
	Fields:         []string{"id", "deactivated", "created_at", "email", "nickname", "password", "role"},
	SortableFields: []string{"id", "deactivated", "created_at", "email", "nickname", "role"},
}
