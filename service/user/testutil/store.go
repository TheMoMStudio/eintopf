//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package testutil

import (
	"context"
	"testing"

	"eintopf.info/internal/crud"
	"eintopf.info/service/user"
)

type newStore func() (user.Storer, func(), error)

// TestStore tests if a given user.Storer acts as a user store.
func TestStore(t *testing.T, newStore newStore) {
	ctx := context.Background()
	t.Helper()
	t.Run("FindyByIDNoMatch", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		user, err := store.FindByID(ctx, "test")
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if user != nil {
			tt.Fatalf("expected to user to be nil. got %v", user)
		}
	})

	t.Run("CreateFindByID", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		user1, _ := store.Create(ctx, &user.NewUser{Email: "user1"})
		user, err := store.FindByID(ctx, user1.ID)
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if user.ID != user1.ID || user.Email != user1.Email {
			tt.Fatalf("expected to find user1. got %v. want %v", user, user1)
		}
	})

	t.Run("CreateFind", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		store.Create(ctx, &user.NewUser{Email: "user1"})
		store.Create(ctx, &user.NewUser{Email: "user2"})
		users, totalUsers, err := store.Find(ctx, nil)
		if err != nil {
			tt.Fatalf("expected store.FindAll to succeed. got %s", err.Error())
		}
		if len(users) != 2 {
			tt.Fatalf("expected to recieve two users. got %d", len(users))
		}
		if totalUsers != 2 {
			tt.Fatalf("expected to recieve a total of two users. got %d", totalUsers)
		}
	})

	t.Run("CreateFindFilter", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		user1, _ := store.Create(ctx, &user.NewUser{Email: "user1"})
		store.Create(ctx, &user.NewUser{Email: "user2"})
		users, totalUsers, err := store.Find(ctx, &crud.FindParams[user.FindFilters]{
			Filters: &user.FindFilters{Email: &user1.Email},
		})
		if err != nil {
			tt.Fatalf("expected store.FindAll to succeed. got %s", err.Error())
		}
		if len(users) != 1 {
			tt.Fatalf("expected to recieve one user. got %d", len(users))
		}
		if totalUsers != 1 {
			tt.Fatalf("expected to recieve a total of one user. got %d", totalUsers)
		}
		if users[0].ID != user1.ID {
			tt.Fatalf("expected to find user1. got %v. want %v", users[0], user1)
		}
	})

	t.Run("CreateFindSortedASC", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		user1, _ := store.Create(ctx, &user.NewUser{Email: "c user1"})
		user2, _ := store.Create(ctx, &user.NewUser{Email: "a user2"})
		user3, _ := store.Create(ctx, &user.NewUser{Email: "b user3"})
		users, totalUsers, err := store.Find(ctx, &crud.FindParams[user.FindFilters]{
			Sort:  "email",
			Order: crud.OrderAsc,
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(users) != 3 {
			tt.Fatalf("expected to find three user. got %d", len(users))
		}
		if totalUsers != 3 {
			tt.Fatalf("expected to recieve a total of three users. got %d", totalUsers)
		}
		if users[0].ID != user2.ID {
			tt.Fatalf("expected to user[0] to be user2 . got %v", users[0])
		}
		if users[1].ID != user3.ID {
			tt.Fatalf("expected to user[1] to be user3 . got %v", users[1])
		}
		if users[2].ID != user1.ID {
			tt.Fatalf("expected to user[2] to be user1 . got %v", users[2])
		}
	})

	t.Run("CreateFindSortedDESC", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		user1, _ := store.Create(ctx, &user.NewUser{Email: "c user1"})
		user2, _ := store.Create(ctx, &user.NewUser{Email: "a user2"})
		user3, _ := store.Create(ctx, &user.NewUser{Email: "b user3"})
		users, totalUsers, err := store.Find(ctx, &crud.FindParams[user.FindFilters]{
			Sort:  "email",
			Order: crud.OrderDesc,
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(users) != 3 {
			tt.Fatalf("expected to find three user. got %d", len(users))
		}
		if totalUsers != 3 {
			tt.Fatalf("expected to recieve a total of three users. got %d", totalUsers)
		}
		if users[0].ID != user1.ID {
			tt.Fatalf("expected to user[0] to be user1 . got %v", users[0])
		}
		if users[1].ID != user3.ID {
			tt.Fatalf("expected to user[1] to be user3 . got %v", users[1])
		}
		if users[2].ID != user2.ID {
			tt.Fatalf("expected to user[2] to be user2 . got %v", users[2])
		}
	})

	t.Run("CreateFindPaginated", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		store.Create(ctx, &user.NewUser{Email: "user1"})
		user2, _ := store.Create(ctx, &user.NewUser{Email: "user2"})
		store.Create(ctx, &user.NewUser{Email: "user3"})
		users, totalUsers, err := store.Find(ctx, &crud.FindParams[user.FindFilters]{
			Limit:  1,
			Offset: 1,
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(users) != 1 {
			tt.Fatalf("expected to find one user. got %d", len(users))
		}
		if totalUsers != 3 {
			tt.Fatalf("expected to recieve a total of three users. got %d", totalUsers)
		}
		if users[0].ID != user2.ID || users[0].Email != user2.Email {
			tt.Fatalf("expected to find user2. got %v. want %v", users[0], user2)
		}
	})

	t.Run("CreateFindPaginatedAndSorted", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		user1, _ := store.Create(ctx, &user.NewUser{Email: "c user1"})
		store.Create(ctx, &user.NewUser{Email: "a user2"})
		store.Create(ctx, &user.NewUser{Email: "b user3"})
		users, totalUsers, err := store.Find(ctx, &crud.FindParams[user.FindFilters]{
			Limit:  1,
			Offset: 2,
			Sort:   "email",
			Order:  crud.OrderAsc,
		})
		if err != nil {
			tt.Fatalf("expected store.Find to succeed. got %s", err.Error())
		}
		if len(users) != 1 {
			tt.Fatalf("expected to find one user. got %d", len(users))
		}
		if totalUsers != 3 {
			tt.Fatalf("expected to recieve a total of three users. got %d", totalUsers)
		}
		if users[0].ID != user1.ID {
			tt.Fatalf("expected to user[0] to be user1 . got %v", users[0])
		}
	})

	t.Run("CreateUpdate", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		user1, _ := store.Create(ctx, &user.NewUser{Email: "user3", Password: "nosecret"})
		user1.Email = "user3.1"
		user1.Password = "mysecret"

		_, err = store.Update(ctx, user1)
		if err != nil {
			tt.Fatalf("expected store.Update to succeed. got %s", err.Error())
		}

		user, err := store.FindByID(ctx, user1.ID)
		if user.Email != "user3.1" {
			tt.Fatalf("expected user.Email to be 'user3.1' got %s", user.Email)
		}
		if user.Password != "mysecret" {
			tt.Fatalf("expected user.Password to be 'mysecret' got %s", user.Email)
		}
	})

	t.Run("CreateDelete", func(tt *testing.T) {
		store, cleanup, err := newStore()
		if err != nil {
			tt.Fatalf("Failed to create a new store: %s", err)
		}
		defer cleanup()

		user1, _ := store.Create(ctx, &user.NewUser{Email: "user1"})
		err = store.Delete(ctx, user1.ID)
		if err != nil {
			tt.Fatalf("expected store.Delete to succeed. got %s", err.Error())
		}
		user, err := store.FindByID(ctx, user1.ID)
		if user != nil {
			tt.Fatal("expected user1 to be deleted")
		}
	})
}
