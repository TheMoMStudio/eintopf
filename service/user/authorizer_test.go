//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package user_test

import (
	"context"
	"log"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"

	"eintopf.info/service/auth"
	"eintopf.info/service/user"
)

var (
	roleAdmin    = auth.RoleAdmin
	roleModertor = auth.RoleModerator
	roleNormal   = auth.RoleNormal
	roleInternal = auth.RoleInternal
)

func TestAuthorizerCreate(t *testing.T) {
	for _, test := range []struct {
		name      string
		userID    string
		role      *auth.Role
		user      *user.NewUser
		wantError error
	}{
		{
			name:      "UnauthorizedWithNoUserIDAndRole",
			userID:    "",
			role:      nil,
			user:      &user.NewUser{},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Authorized:Internal->Admin",
			userID:    "foo",
			role:      &roleInternal,
			user:      &user.NewUser{Role: auth.RoleAdmin, Password: "foo"},
			wantError: nil,
		}, {
			name:      "Unauthorized:Admin->Internal",
			userID:    "foo",
			role:      &roleAdmin,
			user:      &user.NewUser{Role: auth.RoleInternal, Password: "foo"},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Authorized:Admin->Admin",
			userID:    "foo",
			role:      &roleAdmin,
			user:      &user.NewUser{Role: auth.RoleAdmin, Password: "foo"},
			wantError: nil,
		}, {
			name:      "Unauthorized:Moderator->Internal",
			userID:    "foo",
			role:      &roleModertor,
			user:      &user.NewUser{Role: auth.RoleInternal, Password: "foo"},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Unauthorized:Moderator->Admin",
			userID:    "foo",
			role:      &roleModertor,
			user:      &user.NewUser{Role: auth.RoleAdmin, Password: "foo"},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Authorized:Moderator->Moderator",
			userID:    "foo",
			role:      &roleModertor,
			user:      &user.NewUser{Role: auth.RoleModerator, Password: "foo"},
			wantError: nil,
		}, {
			name:      "Unauthorized:Normal->Internal",
			userID:    "foo",
			role:      &roleNormal,
			user:      &user.NewUser{Role: auth.RoleInternal, Password: "foo"},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Unauthorized:Normal->Admin",
			userID:    "foo",
			role:      &roleNormal,
			user:      &user.NewUser{Role: auth.RoleAdmin, Password: "foo"},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Unauthorized:Normal->Moderator",
			userID:    "foo",
			role:      &roleNormal,
			user:      &user.NewUser{Role: auth.RoleModerator, Password: "foo"},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Unauthorized:Normal->Normal",
			userID:    "foo",
			role:      &roleNormal,
			user:      &user.NewUser{Role: auth.RoleNormal, Password: "foo"},
			wantError: auth.ErrUnauthorized,
		},
	} {
		t.Run(test.name, func(tt *testing.T) {
			ctx := auth.ContextWithID(context.Background(), test.userID)
			if test.role != nil {
				ctx = auth.ContextWithRole(ctx, *test.role)
			}

			store := user.NewMemoryStore()

			u := &user.User{Role: test.user.Role, Password: test.user.Password}
			authorizer := user.NewAuthorizer(store)

			u, err := authorizer.Create(ctx, test.user)
			if err != test.wantError {
				tt.Errorf("err != test.wantError: %s != %s", err, test.wantError)
			}

			if test.wantError == nil {
				if *test.role == auth.RoleInternal {
					if u.Password != "foo" {
						tt.Error("Password should not be empty for internal role")
					}
				} else {
					if u.Password != "" {
						tt.Error("Password should be empty for non internal role")
					}
				}

				u, err := store.FindByID(context.Background(), "0")
				if err != nil {
					t.Error(err)
				}
				if u == nil {
					t.Error("user should have been created")
				}
			}
		})
	}
}

func TestAuthorizerUpdate(t *testing.T) {
	for _, test := range []struct {
		name      string
		userID    string
		role      *auth.Role
		user      *user.User
		wantError error
	}{
		{
			name:      "UnauthorizedWithNoUserIDAndRole",
			userID:    "",
			role:      nil,
			user:      &user.User{},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Authorized:Internal->Admin",
			role:      &roleInternal,
			user:      &user.User{ID: "0", Role: auth.RoleAdmin, Password: "foo"},
			wantError: nil,
		}, {
			name:      "Unauthorized:Admin->Internal",
			userID:    "0",
			role:      &roleAdmin,
			user:      &user.User{Role: auth.RoleInternal, Password: "foo"},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Authorized:Admin->Admin",
			userID:    "1",
			role:      &roleAdmin,
			user:      &user.User{ID: "0", Role: auth.RoleAdmin, Password: "foo"},
			wantError: nil,
		}, {
			name:      "Unauthorized:Moderator->Internal",
			userID:    "1",
			role:      &roleModertor,
			user:      &user.User{ID: "0", Role: auth.RoleInternal, Password: "foo"},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Unauthorized:Moderator->Admin",
			userID:    "1",
			role:      &roleModertor,
			user:      &user.User{ID: "0", Role: auth.RoleAdmin, Password: "foo"},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Unauthorized:Moderator->Moderator",
			userID:    "1",
			role:      &roleModertor,
			user:      &user.User{ID: "0", Role: auth.RoleModerator},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Authorized:Moderator(self)->Moderator",
			userID:    "0",
			role:      &roleModertor,
			user:      &user.User{ID: "0", Role: auth.RoleModerator, Password: "foo"},
			wantError: nil,
		}, {
			name:      "Authorized:Moderator->Normal (deactivated, role)",
			userID:    "0",
			role:      &roleModertor,
			user:      &user.User{ID: "0", Role: auth.RoleModerator, Nickname: "foo", Deactivated: true},
			wantError: nil,
		}, {
			name:      "Unauthorized:Moderator->Normal (nickname, ..)",
			userID:    "1",
			role:      &roleModertor,
			user:      &user.User{ID: "0", Role: auth.RoleNormal, Nickname: "changed", Deactivated: false},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Unauthorized:Normal->Internal",
			userID:    "1",
			role:      &roleNormal,
			user:      &user.User{Role: auth.RoleInternal, Password: "foo"},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Unauthorized:Normal->Admin",
			userID:    "1",
			role:      &roleNormal,
			user:      &user.User{Role: auth.RoleAdmin, Password: "foo"},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Unauthorized:Normal->Moderator",
			userID:    "1",
			role:      &roleNormal,
			user:      &user.User{Role: auth.RoleModerator, Password: "foo"},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Unauthorized:Normal->Normal",
			userID:    "1",
			role:      &roleNormal,
			user:      &user.User{Role: auth.RoleNormal, Password: "foo"},
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Authorized:Normal(self->Normal",
			userID:    "0",
			role:      &roleNormal,
			user:      &user.User{ID: "0", Role: auth.RoleNormal, Password: "foo"},
			wantError: nil,
		},
	} {
		t.Run(test.name, func(tt *testing.T) {
			ctx := auth.ContextWithID(context.Background(), test.userID)
			if test.role != nil {
				ctx = auth.ContextWithRole(ctx, *test.role)
			}

			store := user.NewMemoryStore()

			// ID: 0 test user that gets updated
			_, err := store.Create(context.Background(), &user.NewUser{Role: test.user.Role})
			if err != nil {
				t.Fatal(err)
			}
			// ID: 1 doer
			if test.role != nil {
				_, err = store.Create(context.Background(), &user.NewUser{Role: *test.role})
				if err != nil {
					t.Fatal(err)
				}
			}

			authorizer := user.NewAuthorizer(store)

			u, err := authorizer.Update(ctx, test.user)
			if err != test.wantError {
				tt.Errorf("err != test.wantError: %s != %s", err, test.wantError)
			}

			if test.wantError == nil {
				if *test.role == auth.RoleInternal {
					if u.Password != "foo" {
						tt.Error("Password should not be empty for internal role")
					}
				} else {
					if u.Password != "" {
						tt.Error("Password should be empty for non internal role")
					}
				}

				u, err := store.FindByID(context.Background(), "0")
				if err != nil {
					t.Error(err)
				}
				if u == nil {
					t.Error("user should have been created")
				}
			}
		})
	}
}

func TestAuthorizerDelete(t *testing.T) {
	for _, test := range []struct {
		name      string
		userID    string
		role      *auth.Role
		wantError error
	}{
		{
			name:      "UnauthorizedWithNoUserIDAndRole",
			userID:    "",
			role:      nil,
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Authorized:Internal",
			userID:    "1",
			role:      &roleInternal,
			wantError: nil,
		}, {
			name:      "Authorized:Admin",
			userID:    "1",
			role:      &roleAdmin,
			wantError: nil,
		}, {
			name:      "Unauthorized:Moderator",
			userID:    "1",
			role:      &roleModertor,
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Authorized:Moderator(self)",
			userID:    "0",
			role:      &roleModertor,
			wantError: nil,
		}, {
			name:      "Unauthorized:Normal",
			userID:    "1",
			role:      &roleNormal,
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Authorized:Normal(self)",
			userID:    "0",
			role:      &roleNormal,
			wantError: nil,
		},
	} {
		t.Run(test.name, func(t *testing.T) {
			ctx := auth.ContextWithID(context.Background(), test.userID)
			if test.role != nil {
				ctx = auth.ContextWithRole(ctx, *test.role)
			}

			store := user.NewMemoryStore()
			// ID: 0 test user that gets deleted
			_, err := store.Create(context.Background(), &user.NewUser{})
			if err != nil {
				t.Fatal(err)
			}
			// ID: 1 doer
			if test.role != nil {
				_, err = store.Create(context.Background(), &user.NewUser{Role: *test.role})
				if err != nil {
					t.Fatal(err)
				}
			}

			authorizer := user.NewAuthorizer(store)

			err = authorizer.Delete(ctx, "0")
			if err != test.wantError {
				t.Errorf("err != test.wantError: %s != %s", err, test.wantError)
			}

			if test.wantError == nil {
				u, _ := store.FindByID(context.Background(), "0")
				if u != nil {
					t.Fatal("user should not exist anymore")
				}
			}
		})
	}
}

func TestAuthorizerFindByID(t *testing.T) {
	for _, test := range []struct {
		name      string
		userID    string
		role      *auth.Role
		wantError error
	}{
		{
			name:      "UnauthorizedWithNoUserIDAndRole",
			userID:    "",
			role:      nil,
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Authorized:Internal",
			userID:    "0",
			role:      &roleInternal,
			wantError: nil,
		}, {
			name:      "Authorized:Admin",
			userID:    "0",
			role:      &roleAdmin,
			wantError: nil,
		}, {
			name:      "Unauthorized:Moderator",
			userID:    "1",
			role:      &roleModertor,
			wantError: nil,
		}, {
			name:      "Unauthorized:Normal",
			userID:    "1",
			role:      &roleNormal,
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Authorized:Normal(self)",
			userID:    "0",
			role:      &roleNormal,
			wantError: nil,
		},
	} {
		t.Run(test.name, func(tt *testing.T) {
			ctx := auth.ContextWithID(context.Background(), test.userID)
			if test.role != nil {
				ctx = auth.ContextWithRole(ctx, *test.role)
			}

			memoryStore := user.NewMemoryStore()
			role := auth.RoleNormal
			if test.role != nil {
				role = *test.role
			}
			_, err := memoryStore.Create(context.Background(), &user.NewUser{
				Role:     role,
				Password: "foo",
			})
			if err != nil {
				log.Fatal(err)
			}
			authorizer := user.NewAuthorizer(memoryStore)

			u, err := authorizer.FindByID(ctx, "0")
			if err != test.wantError {
				tt.Errorf("err != test.wantError: %s != %s", err, test.wantError)
				return
			}

			if test.wantError == nil {
				if u == nil {
					tt.Errorf("u should not be nil")
				} else {
					if *test.role == auth.RoleInternal {
						if u.Password != "foo" {
							tt.Error("Password should not be empty for internal role")
						}
					} else {
						if u.Password != "" {
							tt.Error("Password should be empty for non internal role")
						}
					}
				}
			}
		})
	}
}

func TestAuthorizerFind(t *testing.T) {
	user1 := &user.User{
		ID:       "0",
		Email:    "secret",
		Nickname: "bli",
		Password: "foo",
		Role:     auth.RoleAdmin,
	}
	user2 := &user.User{
		ID:       "1",
		Email:    "secret",
		Nickname: "bla",
		Password: "foo",
		Role:     auth.RoleModerator,
	}
	user3 := &user.User{
		ID:       "2",
		Email:    "secret",
		Nickname: "blub",
		Password: "foo",
		Role:     auth.RoleNormal,
	}
	users := []*user.User{user1, user2, user3}
	for _, test := range []struct {
		name      string
		userID    string
		role      *auth.Role
		wantUsers []*user.User
		wantError error
	}{
		{
			name:      "UnauthorizedWithNoUserIDAndRole",
			userID:    "",
			role:      nil,
			wantUsers: nil,
			wantError: auth.ErrUnauthorized,
		}, {
			name:      "Authorized:Internal",
			userID:    "foo",
			role:      &roleInternal,
			wantUsers: users,
			wantError: nil,
		}, {
			name:   "Authorized:Admin",
			userID: "foo",
			role:   &roleAdmin,
			wantUsers: []*user.User{
				{
					ID:       "0",
					Email:    "secret",
					Nickname: "bli",
					Role:     auth.RoleAdmin,
				},
				{
					ID:       "1",
					Email:    "secret",
					Nickname: "bla",
					Role:     auth.RoleModerator,
				},
				{
					ID:       "2",
					Email:    "secret",
					Nickname: "blub",
					Role:     auth.RoleNormal,
				},
			},
			wantError: nil,
		}, {
			name:   "Authorized:Moderator",
			userID: "foo",
			role:   &roleModertor,
			wantUsers: []*user.User{
				{
					ID:       "0",
					Email:    "secret",
					Nickname: "bli",
					Role:     auth.RoleAdmin,
				},
				{
					ID:       "1",
					Email:    "secret",
					Nickname: "bla",
					Role:     auth.RoleModerator,
				},
				{
					ID:       "2",
					Email:    "secret",
					Nickname: "blub",
					Role:     auth.RoleNormal,
				},
			},
			wantError: nil,
		}, {
			name:   "Unauthorized:Normal",
			userID: "foo",
			role:   &roleNormal,
			wantUsers: []*user.User{
				{ID: "0", Nickname: "bli"},
				{ID: "1", Nickname: "bla"},
				{ID: "2", Nickname: "blub"},
			},
			wantError: nil,
		},
	} {
		t.Run(test.name, func(tt *testing.T) {
			ctx := auth.ContextWithID(context.Background(), test.userID)
			if test.role != nil {
				ctx = auth.ContextWithRole(ctx, *test.role)
			}

			memoryStore := user.NewMemoryStore()
			for _, u := range users {
				memoryStore.Create(context.Background(), &user.NewUser{
					Nickname: u.Nickname,
					Email:    u.Email,
					Password: u.Password,
					Role:     u.Role,
				})
			}
			authorizer := user.NewAuthorizer(memoryStore)

			users, _, err := authorizer.Find(ctx, nil)
			if err != test.wantError {
				tt.Errorf("err != test.wantError: %s != %s", err, test.wantError)
			}

			if test.wantError == nil {
				if diff := cmp.Diff(test.wantUsers, users, cmpopts.IgnoreFields(user.User{}, "CreatedAt")); diff != "" {
					tt.Errorf("users return mismatch: %s", diff)
				}
				if *test.role == auth.RoleInternal {
					for _, user := range users {
						if user.Password != "foo" {
							tt.Error("Password should not be empty for internal role")
						}
					}
				} else {
					for _, user := range users {
						if user.Password != "" {
							tt.Error("Password should be empty for non internal role")
						}
					}
				}
			}
		})
	}
}
