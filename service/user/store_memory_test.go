//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package user_test

import (
	"testing"

	"eintopf.info/service/user"
	"eintopf.info/service/user/testutil"
)

func TestMemoryStore(t *testing.T) {
	testutil.TestStore(t, func() (user.Storer, func(), error) {
		return user.NewMemoryStore(), func() {}, nil
	})
}
