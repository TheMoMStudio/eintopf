// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package dbmigration_test

import (
	"context"
	"testing"

	"eintopf.info/service/dbmigration"
	"eintopf.info/test"
)

func TestStore(t *testing.T) {
	testdb, cleanupDB, err := test.CreateSqliteTestDB(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	t.Cleanup(cleanupDB)

	store, err := dbmigration.NewSqlStore(testdb)
	if err != nil {
		t.Fatal(err)
	}

	hasMigration, err := store.HasMigration(context.Background(), "foo")
	if err != nil {
		t.Fatal(err)
	}
	if hasMigration {
		t.Fatal("should not have migration")
	}

	err = store.AddMigration(context.Background(), "foo")
	if err != nil {
		t.Fatal(err)
	}

	hasMigration, err = store.HasMigration(context.Background(), "foo")
	if err != nil {
		t.Fatal(err)
	}
	if !hasMigration {
		t.Fatal("should have migration")
	}
}
