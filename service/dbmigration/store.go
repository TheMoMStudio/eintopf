// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package dbmigration

import (
	"context"
	"database/sql"

	"github.com/jmoiron/sqlx"
)

type SqlStore struct {
	db *sqlx.DB
}

func NewSqlStore(db *sqlx.DB) (*SqlStore, error) {
	_, err := db.Exec(`
        CREATE TABLE IF NOT EXISTS migrations (
            name varchar(64) NOT NULL PRIMARY KEY UNIQUE
        );
    `)
	if err != nil {
		return nil, err
	}
	return &SqlStore{db: db}, nil
}

func (s *SqlStore) AddMigration(ctx context.Context, name string) error {
	_, err := s.db.ExecContext(ctx, `INSERT INTO migrations (name) VALUES ($1)`, name)
	return err
}

func (s *SqlStore) HasMigration(ctx context.Context, name string) (bool, error) {
	migrations := []string{}
	err := s.db.SelectContext(ctx, &migrations, `SELECT name FROM migrations WHERE name = $1`, name)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
		return false, err
	}
	if len(migrations) == 0 {
		return false, nil
	}
	return true, nil
}
