// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package dbmigration

import (
	"context"
	"fmt"
)

type Migration struct {
	Name          string
	MigrationFunc MigrationFunc
	RollbackFunc  MigrationFunc
}

type MigrationFunc func(ctx context.Context) error

func NewMigration(name string, migrationFunc, rollbackFunc MigrationFunc) Migration {
	return Migration{Name: name, MigrationFunc: migrationFunc, RollbackFunc: rollbackFunc}
}

type Service interface {
	RunMigration(ctx context.Context, migration Migration) error
	RunMigrations(ctx context.Context, migrations []Migration) error
}

type Store interface {
	AddMigration(ctx context.Context, name string) error
	HasMigration(ctx context.Context, name string) (bool, error)
}

type MigrationService struct {
	store Store
}

func NewService(store Store) *MigrationService {
	return &MigrationService{store: store}
}

func (m *MigrationService) RunMigration(ctx context.Context, migration Migration) error {
	hasMigrated, err := m.store.HasMigration(ctx, migration.Name)
	if err != nil {
		return fmt.Errorf("migration: %s: check for migration: %s", migration.Name, err)
	}
	if hasMigrated {
		return nil
	}
	err = migration.MigrationFunc(ctx)
	if err != nil {
		return fmt.Errorf("migration: %s: %s", migration.Name, err)
	}
	err = m.store.AddMigration(ctx, migration.Name)
	if err != nil {
		if migration.RollbackFunc != nil {
			rollbackErr := migration.RollbackFunc(ctx)
			if rollbackErr != nil {
				return fmt.Errorf("migration: %s: store: %s: failed rollback: %s", migration.Name, err, rollbackErr)
			}
			return fmt.Errorf("migration: %s: store: %s: rollaback succesfull", migration.Name, err)
		}
		return fmt.Errorf("migration: %s: store: %s: no rollback", migration.Name, err)
	}
	return nil
}

func (m *MigrationService) RunMigrations(ctx context.Context, migrations []Migration) error {
	for _, migration := range migrations {
		err := m.RunMigration(ctx, migration)
		if err != nil {
			return err
		}
	}
	return nil
}
