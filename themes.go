// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package eintopf

import (
	"context"
	"embed"
	"fmt"
	"io"
	"io/fs"
	"log"
	"os"
	"path"
	"path/filepath"
	"time"

	htmltemplate "html/template"

	"eintopf.info/internal/xhttp"

	"eintopf.info/internal/template"
	"gopkg.in/yaml.v3"
)

// BundeledThemes contains the bundled themes.
//
//go:embed themes
var BundeledThemes embed.FS

const DefaultTheme = "lauti"

const themeConfigFileName = "theme.yaml"

type Theme interface {
	xhttp.LastModifiedCache
	Render(ctx context.Context, w io.Writer, templates []string, data map[string]any, funcs htmltemplate.FuncMap, includeHelper bool) error
	AssetFS() fs.FS
	FS() fs.FS
}

func NewTheme(themeName string, themesPath string, data map[string]any, funcs htmltemplate.FuncMap) (*StaticTheme, error) {
	// When the theme name is empty use the default theme.
	if themeName == "" {
		themeName = DefaultTheme
	}

	themes := []fs.FS{}

	theme, bundeled, err := loadThemeFS(themeName, themesPath)
	if err != nil {
		return nil, err
	}
	themes = append(themes, theme)

	// Recursively look for a parent theme and prepend themen to the themes
	// list.
	for {
		themeConfig, err := readThemeConfig(theme)
		if err != nil {
			if os.IsNotExist(err) {
				// Stop looking for parent themes, when the config file does not
				// exist.
				break
			}
			return nil, fmt.Errorf("parse theme config: %s", err)
		}
		if themeConfig.Parent == "" {
			break
		}

		// Prepend the parent theme to the themes list.
		theme, _, err = loadThemeFS(themeConfig.Parent, themesPath)
		if err != nil {
			return nil, fmt.Errorf("find parent theme(%s): %s", themeConfig.Parent, err)
		}
		themes = append([]fs.FS{theme}, themes...)
		log.Println("loaded parent", themeConfig.Parent)
	}

	engine := template.NewEngine(themes, data, funcs)
	assetFS, _ := fs.Sub(engine.FS(), "assets")
	return &StaticTheme{
		name:    themeName,
		bundled: bundeled,
		engine:  engine,
		assetFS: assetFS,
		created: time.Now(),
	}, nil
}

// loadThemeFS tries to find the theme and load it into a fs. It returns true
// when the theme is a bundeled theme.
func loadThemeFS(themeName string, themesPath string) (fs.FS, bool, error) {
	_, err := BundeledThemes.ReadDir(filepath.Join("themes", themeName))
	if err == nil {
		subFS, err := fs.Sub(BundeledThemes, path.Join("themes", themeName))
		if err != nil {
			return nil, false, err
		}
		return subFS, true, nil
	}
	if err != nil && !os.IsNotExist(err) {
		return nil, false, err
	}

	f := os.DirFS(path.Join(themesPath, themeName))
	_, err = fs.ReadDir(f, ".")
	if err == nil {
		return f, false, nil
	}
	if err != nil && !os.IsNotExist(err) {
		return nil, false, err
	}

	return nil, false, fmt.Errorf("theme not found: %s", themeName)
}

type StaticTheme struct {
	name    string
	bundled bool
	engine  *template.Engine
	created time.Time
	assetFS fs.FS
}

func (st *StaticTheme) Name() string {
	return st.name
}

func (st *StaticTheme) IsBundeled() bool {
	return st.bundled
}

func (st *StaticTheme) LastModified() time.Time {
	return st.created
}

func (st *StaticTheme) AssetFS() fs.FS {
	return st.assetFS
}

func (st *StaticTheme) FS() fs.FS {
	return st.engine.FS()
}

func (st *StaticTheme) Render(ctx context.Context, w io.Writer, templates []string, data map[string]any, funcs htmltemplate.FuncMap, includeHelper bool) error {
	if funcs == nil {
		funcs = make(htmltemplate.FuncMap)
	}

	if data == nil {
		data = make(map[string]any)
	}

	return st.engine.Render(
		ctx,
		w,
		templates,
		data,
		funcs,
		includeHelper,
	)
}

type ThemeConfig struct {
	Parent string `json:"parent" yaml:"parent"`
}

func readThemeConfig(theme fs.FS) (ThemeConfig, error) {
	configContent, err := fs.ReadFile(theme, themeConfigFileName)
	if err != nil {
		return ThemeConfig{}, err
	}

	var config ThemeConfig
	err = yaml.Unmarshal(configContent, &config)
	if err != nil {
		return ThemeConfig{}, err
	}
	return config, nil
}
