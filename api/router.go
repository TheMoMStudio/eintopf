//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package api

import (
	"embed"
	"io/fs"
	"net/http"
	"path"

	"github.com/go-chi/chi/v5"
	"github.com/go-openapi/runtime/middleware"
)

// apiFS holds the static api content.
//
//go:embed swagger.json
var apiFS embed.FS

//go:embed swagger-ui-dist
var swaggerUI embed.FS

// AddRoutes adds routes to the given router that serve the openapi spec.
func AddRoutes(r chi.Router, apiPrefix string) {
	webFS := http.StripPrefix(apiPrefix, http.FileServer(http.FS(apiFS)))
	sub, _ := fs.Sub(swaggerUI, "swagger-ui-dist")
	swaggerUIFS := http.StripPrefix("/api/v1/swagger/dist/", http.FileServer(http.FS(sub)))

	r.Get("/swagger.json", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		webFS.ServeHTTP(w, r)
	}))
	r.Get("/swagger", func(w http.ResponseWriter, r *http.Request) {
		middleware.SwaggerUI(
			middleware.SwaggerUIOpts{
				Path:    path.Join(apiPrefix, "/swagger"),
				SpecURL: path.Join(apiPrefix, "/swagger.json"),

				SwaggerURL:       path.Join(apiPrefix, "/swagger/dist/swagger-ui-bundle.js"),
				SwaggerPresetURL: path.Join(apiPrefix, "/swagger/dist/swagger-ui-standalone-preset.js"),
				SwaggerStylesURL: path.Join(apiPrefix, "/swagger/dist/swagger-ui.css"),
				Favicon32:        path.Join(apiPrefix, "/swagger/dist/favicon-32x32.png"),
				Favicon16:        path.Join(apiPrefix, "/swagger/dist/favicon-16x16.png"),
			},
			http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {}),
		).ServeHTTP(w, r)
	})
	r.Get("/swagger/dist/*", func(w http.ResponseWriter, r *http.Request) {
		swaggerUIFS.ServeHTTP(w, r)
	})
}
