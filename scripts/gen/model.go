//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package gen

import (
	"fmt"
	"os"
	"strings"

	"gopkg.in/yaml.v3"
)

func ReadModelFile(path string) (*Model, error) {
	raw, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	model := &Model{}
	err = yaml.Unmarshal(raw, model)
	if err != nil {
		return nil, err
	}
	for name, field := range model.Fields {
		err := field.Validate(name)
		if err != nil {
			return nil, fmt.Errorf("field: %s, %s", name, err)
		}
		model.Fields[name] = field.Fill(name)
	}
	for name, field := range model.SearchFields {
		err := field.Validate(name)
		if err != nil {
			return nil, fmt.Errorf("searchfield: %s, %s", name, err)
		}
		model.SearchFields[name] = field.Fill(name)
	}
	return model, nil
}

type Model struct {
	Fields       map[string]Field `yaml:"fields"`
	SearchFields map[string]Field `yaml:"searchfields"`
}

type Field struct {
	Type            string `yaml:"type"`
	Name            string `yaml:"name"`
	GoName          string `yaml:"go_name"`
	JSONName        string `yaml:"json_name"`
	GoType          string `yaml:"go_type"`
	NotFilterable   bool   `yaml:"not_filterable"`
	NotAggregatable bool   `yaml:"not_aggregatable"`
}

func (f Field) Validate(name string) error {
	// TODO: validate type
	if name == "" && f.Name == "" {
		return fmt.Errorf("empty name")
	}
	return nil
}

func (f Field) Fill(name string) Field {
	if f.Name == "" {
		f.Name = name
	}
	if f.GoName == "" && len(f.Name) >= 2 {
		f.GoName = strings.ToUpper(string(f.Name[0])) + f.Name[1:len(f.Name)]
	}
	if f.JSONName == "" {
		f.JSONName = f.Name
	}
	return f
}
