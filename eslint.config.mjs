// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: CC0-1.0

import prettier from "eslint-plugin-prettier";
import tsParser from "@typescript-eslint/parser";
import path from "node:path";
import { fileURLToPath } from "node:url";
import js from "@eslint/js";
import { FlatCompat } from "@eslint/eslintrc";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const compat = new FlatCompat({
    baseDirectory: __dirname,
    recommendedConfig: js.configs.recommended,
    allConfig: js.configs.all
});

export default [{
    ignores: ["**/build/"],
}, ...compat.extends("preact", "plugin:@typescript-eslint/recommended", "prettier"), {
    plugins: {
        prettier,
    },

    languageOptions: {
        parser: tsParser,
    },

    rules: {
        "react/jsx-filename-extension": "off",
        "react/prop-types": "off",
        "react/require-default-props": "off",
        "react/button-has-type": "off",
        "react/jsx-one-expression-per-line": "off",
        "react/jsx-props-no-spreading": "off",
        "react/no-array-index-key": "off",
        "react/display-name": "off",
        "@typescript-eslint/explicit-function-return-type": "off",
        "@typescript-eslint/no-explicit-any": "off",
        "@typescript-eslint/no-empty-function": "off",
        "@typescript-eslint/no-empty-interface": "off",
        "@typescript-eslint/no-use-before-define": "off",
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "@typescript-eslint/ban-ts-comment": "off",
        "no-case-declarations": "off",
        "jsx-a11y/no-static-element-interactions": "off",
        "jsx-a11y/click-events-have-key-events": "off",
        "jsx-a11y/anchor-is-valid": "off",
        "no-await-in-loop": "off",
        "no-plusplus": "off",
        "no-confusing-arrow": "off",
        "function-paren-newline": "off",
        "no-console": "off",
        "no-restricted-syntax": ["error"],
        "prettier/prettier": ["error"],
        "no-continue": "off",
        "jest/no-deprecated-functions": "off",
    },
}];