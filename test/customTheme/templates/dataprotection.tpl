{{ define "dataprotection_address" }}
Maxi Musterperson</br>
c/o Musterorga</br>
Musterstraße 1</br>
12345 Musterstadt</br>
{{ end }}

{{ define "dataprotection_contact" }}
Mastodon: <a class="" href="https://sueden.social/@eintopf">https://sueden.social/@eintopf</a></br>
E-Mail: mail [at] eintopf.info</br>
{{ end }}

{{ define "dataprotection_responsible" }}
Maxi Musterperson</br>
c/o Musterorga</br>
Musterstraße 1</br>
12345 Musterstadt</br>
{{ end }}

{{ define "dataprotection_hoster" }}
<h3>Toller Anbieter</h3></br>
Anbieter ist ...
{{ end }}
