// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package test

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"net/url"
	"strconv"
	"testing"
	"time"

	"github.com/ory/dockertest/v3"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func RandStringBytes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

// RunMailhogWithSSL starts a mailhog docker container, which gets routed
// through an stunnel docker container for local ssl.
func RunMailhogWithSSL(t *testing.T, pool *dockertest.Pool) (string, MailHogClient) {
	t.Helper()

	networkName := fmt.Sprintf("mailhog_testing_%s", RandStringBytes(10))
	network, err := pool.CreateNetwork(networkName)
	if err != nil {
		t.Fatalf("could not create docker network: %s", err)
	}
	t.Cleanup(func() { network.Close() })

	mailhog, err := pool.RunWithOptions(&dockertest.RunOptions{
		Repository: "mailhog/mailhog",
		Tag:        "latest",
		// These ports are exposed in the container already. On some machines
		// this leads to docker trying to publish the same port twice on the
		// same port, which then errors.
		// ExposedPorts: []string{"1025", "8025"},
		Hostname: "mailhog",
		Networks: []*dockertest.Network{network},
	})
	if err != nil {
		t.Fatalf("Could not start mailhog: %s", err)
	}
	t.Cleanup(func() {
		if err := pool.Purge(mailhog); err != nil {
			t.Fatalf("Could not purge mailhog: %s", err)
		}
	})

	client := MailHogClient{URL: fmt.Sprintf("http://localhost:%s", mailhog.GetPort("8025/tcp"))}

	if err := pool.Retry(func() error {
		_, err := client.V1GetMessages()
		return err
	}); err != nil {
		t.Fatalf("Could not connect to mailhog: %s", err)
	}

	stunnel, err := pool.RunWithOptions(&dockertest.RunOptions{
		Repository: "dweomer/stunnel",
		Tag:        "latest",
		Env: []string{
			"STUNNEL_SERVICE=smtps",
			"STUNNEL_ACCEPT=465",
			"STUNNEL_CONNECT=mailhog:1025",
		},
		ExposedPorts: []string{"465"},
		Hostname:     "mailtunnel",
		Networks:     []*dockertest.Network{network},
	})
	if err != nil {
		t.Fatalf("Could not start stunnel: %s", err)
	}
	t.Cleanup(func() {
		if err := pool.Purge(stunnel); err != nil {
			t.Fatalf("Could not purge stunnel: %s", err)
		}
	})

	stunnelPort, err := strconv.ParseInt(stunnel.GetPort("465/tcp"), 10, 64)
	if err != nil {
		t.Fatalf("invalid stunnel port (%s): %s", stunnel.GetPort("465/tcp"), err)
	}

	if err := pool.Retry(func() error {
		conn, err := tls.Dial("tcp", fmt.Sprintf("localhost:%d", stunnelPort), &tls.Config{
			InsecureSkipVerify: true,
			ServerName:         "localhost",
		})
		if err != nil {
			return err
		}
		conn.Close()
		return nil
	}); err != nil {
		t.Fatalf("Could not connect to stunnel: %s", err)
	}

	return fmt.Sprintf("localhost:%d", stunnelPort), client
}

func (c MailHogClient) WantLastEmailToBe(t *testing.T, from string, to string, subject string, body string) {
	t.Helper()

	messages, err := c.V1GetMessages()
	if err != nil {
		t.Error(err)
	}

	if len(messages) == 0 {
		t.Error("no emails")
		return
	}

	m := messages[len(messages)-1]
	if m.Raw.From != from {
		t.Errorf("want FROM to be %s, got %s", from, messages[0].Raw.From)
	}
	if m.Raw.To[0] != to {
		t.Errorf("want TO to be %s, got %s", to, messages[0].Raw.To[0])
	}
	if m.Content.Headers["Subject"][0] != subject {
		t.Errorf("want SUBJECT to be '%s', got '%s'", subject, messages[0].Content.Headers["Subject"][0])
	}
	if m.Content.Body != body {
		t.Errorf("want BODY to be '%s', got '%s'", body, messages[0].Content.Body)
	}
}

func (c MailHogClient) GetLastEmail() (MailHogMessage, error) {
	messages, err := c.V1GetMessages()
	if err != nil {
		return MailHogMessage{}, err
	}

	if len(messages) == 0 {
		return MailHogMessage{}, fmt.Errorf("no emails received")
	}

	return messages[len(messages)-1], nil
}

type MailHogMessage struct {
	ID   string
	From struct {
		Relays  string
		Mailbox string
		Domain  string
		Params  string
	}
	To []struct {
		Relays  string
		Mailbox string
		Domain  string
		Params  string
	}
	Content struct {
		Headers map[string][]string
		Body    string
		Size    int
		Mime    string
	}
	Created time.Time
	Mime    string
	Raw     struct {
		From string
		To   []string
		Data string
		Helo string
	}
}

type MailHogClient struct {
	URL string
}

func (m MailHogClient) V1GetMessages() ([]MailHogMessage, error) {
	path, err := url.JoinPath(m.URL, "api/v1/messages")
	if err != nil {
		return nil, err
	}
	resp, err := http.Get(path)
	if err != nil {
		return nil, err
	}
	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var messages []MailHogMessage
	err = json.Unmarshal(data, &messages)
	if err != nil {
		return nil, err
	}
	return messages, nil
}
