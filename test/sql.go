//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package test

import (
	"fmt"
	"os"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

// CreateSqliteTestDB creates an in memory sqlite database.
func CreateSqliteTestDB(name string) (*sqlx.DB, func(), error) {
	file := fmt.Sprintf("%s.sqlite", name)
	db, err := sqlx.Connect("sqlite3", file)
	if err != nil {
		return nil, nil, fmt.Errorf("cannot connect to sqlite db: %s", err)
	}
	cleanup := func() {
		db.Close()
		os.Remove(file)
	}
	return db, cleanup, nil
}
