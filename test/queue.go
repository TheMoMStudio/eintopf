// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package test

import (
	"testing"

	"eintopf.info/service/oqueue"
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
)

type TestQueue struct {
	ops []oqueue.Operation
}

func NewTestQueue() *TestQueue {
	return &TestQueue{
		ops: []oqueue.Operation{},
	}
}

func (q *TestQueue) AddOperation(op oqueue.Operation) error {
	q.ops = append(q.ops, op)
	return nil
}

func (q *TestQueue) WasCalled(t *testing.T, op oqueue.Operation) {
	t.Helper()
	if len(q.ops) == 0 {
		t.Errorf("no operations")
		return
	}
	op2 := q.ops[0]
	if diff := cmp.Diff(op, op2, cmpopts.IgnoreUnexported(op)); diff != "" {
		t.Errorf("operation mismatch (-want +got):\n%s", diff)
	}
	if op.UserID() != op2.UserID() {
		t.Errorf("invalid userID: %s !== %s", op.UserID(), op2.UserID())
	}
	q.ops = q.ops[1:]
}

func (q *TestQueue) AddSubscriber(consume oqueue.OperationConsumer, worker int) {}

func (q *TestQueue) Close() {}
