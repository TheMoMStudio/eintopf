// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package test

import (
	"context"

	"eintopf.info/service/notification"
)

type Notification struct {
	UserID  string
	Subject string
	Message string
	Link    notification.Link
}

type Notifier struct {
	Notifications []Notification
}

func NewNotifier() *Notifier {
	return &Notifier{Notifications: []Notification{}}
}

func (n *Notifier) Notify(ctx context.Context, userID string, subject string, message string, link notification.Link) error {
	n.Notifications = append(n.Notifications, Notification{UserID: userID, Subject: subject, Message: message, Link: link})
	return nil
}
