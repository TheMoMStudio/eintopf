// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package test

import (
	"context"

	"eintopf.info/service/auth"
)

type AuthService struct{}

func NewAuthService() *AuthService {
	return &AuthService{}
}

func (a *AuthService) Login(ctx context.Context, email, password string) (token string, err error) {
	return "", nil
}

func (a *AuthService) Authenticate(token string) (id string, err error) { return "", nil }

func (a *AuthService) Authorize(ctx context.Context, id string) (role auth.Role, err error) {
	return auth.RoleNormal, nil
}
