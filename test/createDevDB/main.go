//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"flag"
	"fmt"
	"os"

	_ "github.com/mattn/go-sqlite3"

	"eintopf.info/test/data"
)

var (
	dbPath       string
	relativeTime bool
)

func init() {
	flag.StringVar(&dbPath, "dbpath", "eintopf.dev.db", "output path for the generated database")
	flag.BoolVar(&relativeTime, "relativeTime", false, "add the current day of month to the event dates")
}

func main() {
	flag.Parse()
	err := data.CreateDevDB("test/data", dbPath, relativeTime)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
