//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package data

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"

	"eintopf.info/internal/crud"
	"eintopf.info/service/dbmigration"
	"eintopf.info/service/event"
	"eintopf.info/service/group"
	"eintopf.info/service/place"
	"eintopf.info/service/revent"
	"eintopf.info/service/taxonomy"
	"eintopf.info/service/user"
)

func CreateDevDB(dataPath, dbPath string, relativeTime bool) error {
	db, err := sqlx.Connect("sqlite3", dbPath)
	if err != nil {
		return fmt.Errorf("cannot connect to sqlite db: %s: %s", dbPath, err)
	}
	defer db.Close()

	migrationStore, err := dbmigration.NewSqlStore(db)
	if err != nil {
		return err
	}
	migrationService := dbmigration.NewService(migrationStore)
	eventStore, err := event.NewSqlStore(db, migrationService)
	if err != nil {
		return err
	}
	groupStore, err := group.NewSqlStore(db, migrationService)
	if err != nil {
		return err
	}
	placeStore, err := place.NewSqlStore(db, migrationService)
	if err != nil {
		return fmt.Errorf("place db migration: %s", err)
	}
	userStore, err := user.NewSqlStore(db, migrationService)
	if err != nil {
		return fmt.Errorf("user db migration: %s", err)
	}
	categoryStore, err := taxonomy.NewCategorySqlStore(db, migrationService)
	if err != nil {
		return fmt.Errorf("category db migration: %s", err)
	}
	topicStore, err := taxonomy.NewTopicSqlStore(db, migrationService)
	if err != nil {
		return fmt.Errorf("topic db migration: %s", err)
	}
	userService := user.NewService(userStore)

	reventStore, err := revent.NewSqlStore(db, migrationService)
	if err != nil {
		return fmt.Errorf("revent db migration: %s", err)
	}

	reventService := revent.NewService(reventStore, eventStore)

	categoryService := taxonomy.NewCategoryService(categoryStore)
	if err = importCategories(dataPath, categoryStore); err != nil {
		return err
	}
	if err = importUsers(dataPath, userService); err != nil {
		return err
	}
	if err = importGroups(dataPath, groupStore, userStore); err != nil {
		return err
	}
	if err = importPlaces(dataPath, placeStore); err != nil {
		return err
	}
	topicService := taxonomy.NewTopicService(topicStore)
	if err = importTopics(dataPath, topicStore); err != nil {
		return err
	}
	if err = importRevents(dataPath, reventStore, eventStore, groupStore, reventService, userService, categoryService, topicService, relativeTime); err != nil {
		return err
	}
	if err = importEvents(dataPath, eventStore, groupStore, userService, categoryService, topicService, relativeTime); err != nil {
		return err
	}

	return nil
}

func importEvents(dataPath string, eventStore event.Storer, groupService group.Service, userService user.Service, categoryService taxonomy.CategoryService, topicService taxonomy.TopicService, relativeTime bool) error {
	eventsData, err := os.ReadFile(path.Join(dataPath, "/events.json"))
	if err != nil {
		return fmt.Errorf("reading events.json: %s", err)
	}

	events := make([]*event.NewEvent, 0)
	err = json.Unmarshal(eventsData, &events)
	if err != nil {
		return fmt.Errorf("decoding events: %s", err)
	}

	now := time.Now()

	createdEvents := []*event.Event{}
	for i, event := range events {
		event.Start = time.Date(
			now.Year(),
			now.Month(),
			event.Start.Day(),
			event.Start.Hour(),
			event.Start.Minute(),
			event.Start.Second(),
			0,
			time.UTC,
		)
		if event.End != nil {
			event.End = tptr(time.Date(
				now.Year(),
				now.Month(),
				event.End.Day(),
				event.End.Hour(),
				event.End.Minute(),
				event.End.Second(),
				0,
				time.UTC,
			))
		}

		if relativeTime {
			event.Start = event.Start.AddDate(0, 0, time.Now().Day())
			if event.End != nil {
				event.End = tptr(event.End.AddDate(0, 0, time.Now().Day()))
			}
		}

		if event.Parent != "" {
			parentIndex, err := strconv.Atoi(event.Parent)
			if err != nil {
				return fmt.Errorf("importing event %d: %s", i, err)
			}
			event.Parent = "id:" + createdEvents[parentIndex].ID
		}

		for i, organizer := range event.Organizers {
			if strings.Contains(organizer, "name:") {
				groupName := strings.TrimPrefix(organizer, "name:")
				groups, _, err := groupService.Find(context.Background(), &crud.FindParams[group.FindFilters]{
					Filters: &group.FindFilters{Name: &groupName},
				})
				if err != nil {
					return err
				}
				if len(groups) != 1 {
					return fmt.Errorf("was looking for group with name: %s, found: %d", groupName, len(groups))
				}
				event.Organizers[i] = fmt.Sprintf("id:%s", groups[0].ID)
			}
		}
		for i, owner := range event.OwnedBy {
			if strings.Contains(owner, "user:") {
				userNickname := strings.TrimPrefix(owner, "user:")
				users, _, err := userService.Find(context.Background(), &crud.FindParams[user.FindFilters]{
					Filters: &user.FindFilters{Nickname: &userNickname},
				})
				if err != nil {
					return err
				}
				if len(users) != 1 {
					return fmt.Errorf("was looking for user with name: %s, found: %d", userNickname, len(users))
				}
				event.OwnedBy[i] = users[0].ID
			}
		}

		if event.Category != "" {
			if strings.Contains(event.Category, "category:") {
				categoryName := strings.TrimPrefix(event.Category, "category:")
				categories, _, err := categoryService.Find(context.Background(), &crud.FindParams[taxonomy.Filters]{
					Filters: &taxonomy.Filters{Name: &categoryName},
				})
				if err != nil {
					return err
				}
				event.Category = categories[0].ID
			}
		}

		if event.Topic != "" {
			if strings.Contains(event.Topic, "topic:") {
				topicName := strings.TrimPrefix(event.Topic, "topic:")
				topics, _, err := topicService.Find(context.Background(), &crud.FindParams[taxonomy.Filters]{
					Filters: &taxonomy.Filters{Name: &topicName},
				})
				if err != nil {
					return err
				}
				event.Topic = topics[0].ID
			}
		}

		createdEvent, err := eventStore.Create(context.Background(), event)
		if err != nil {
			return fmt.Errorf("importing event %d: %s", i, err)
		}
		createdEvents = append(createdEvents, createdEvent)
	}
	return nil
}

func importGroups(dataPath string, groupStore group.Service, userStore user.Storer) error {
	groupsData, err := os.ReadFile(path.Join(dataPath, "/groups.json"))
	if err != nil {
		return fmt.Errorf("reading groups.json: %s", err)
	}

	groups := make([]*group.NewGroup, 0)
	err = json.Unmarshal(groupsData, &groups)
	if err != nil {
		return fmt.Errorf("decoding groups: %s", err)
	}

	for i, group := range groups {
		for i, owner := range group.OwnedBy {
			if strings.Contains(owner, "user:") {
				userNickname := strings.TrimPrefix(owner, "user:")
				users, _, err := userStore.Find(context.Background(), &crud.FindParams[user.FindFilters]{
					Filters: &user.FindFilters{Nickname: &userNickname},
				})
				if err != nil {
					return err
				}
				if len(users) != 1 {
					return fmt.Errorf("was looking for user with name: %s, found: %d", userNickname, len(users))
				}
				group.OwnedBy[i] = fmt.Sprintf("%s", users[0].ID)
			}
		}
		_, err = groupStore.Create(context.Background(), group)
		if err != nil {
			return fmt.Errorf("importing group %d: %s", i, err)
		}
	}
	return nil
}

func importCategories(dataPath string, categoryStorer taxonomy.CategoryStorer) error {
	categoriesData, err := os.ReadFile(path.Join(dataPath, "/categories.json"))
	if err != nil {
		return fmt.Errorf("reading categories.json: %s", err)
	}

	categories := make([]*taxonomy.NewCategory, 0)
	err = json.Unmarshal(categoriesData, &categories)
	if err != nil {
		return fmt.Errorf("decoding category: %s", err)
	}

	for i, category := range categories {
		_, err = categoryStorer.Create(context.Background(), category)
		if err != nil {
			return fmt.Errorf("importing category %d: %s", i, err)
		}
	}
	return nil
}

func importRevents(dataPath string, reventStore revent.Storer, eventStore event.Storer, groupService group.Service, reventService revent.Service, userService user.Service, categoryService taxonomy.CategoryService, topicService taxonomy.TopicService, relativeTime bool) error {
	eventsData, err := os.ReadFile(path.Join(dataPath, "/revents.json"))
	if err != nil {
		return fmt.Errorf("reading revents.json: %s", err)
	}

	revents := make([]*revent.NewRepeatingEvent, 0)
	err = json.Unmarshal(eventsData, &revents)
	if err != nil {
		return fmt.Errorf("decoding revents: %s", err)
	}

	now := time.Now()

	for i, revent := range revents {
		revent.Start = time.Date(
			now.Year(),
			now.Month(),
			revent.Start.Day(),
			revent.Start.Hour(),
			revent.Start.Minute(),
			revent.Start.Second(),
			0,
			time.UTC,
		)
		revent.End = time.Date(
			now.Year(),
			now.Month(),
			revent.End.Day(),
			revent.End.Hour(),
			revent.End.Minute(),
			revent.End.Second(),
			0,
			time.UTC,
		)

		if relativeTime {
			revent.Start = revent.Start.AddDate(0, 0, time.Now().Day())
			revent.End = revent.End.AddDate(0, 0, time.Now().Day())
		}

		for i, organizer := range revent.Organizers {
			if strings.Contains(organizer, "name:") {
				groupName := strings.TrimPrefix(organizer, "name:")
				groups, _, err := groupService.Find(context.Background(), &crud.FindParams[group.FindFilters]{
					Filters: &group.FindFilters{Name: &groupName},
				})
				if err != nil {
					return err
				}
				if len(groups) != 1 {
					return fmt.Errorf("was looking for group with name: %s, found: %d", groupName, len(groups))
				}
				revent.Organizers[i] = fmt.Sprintf("id:%s", groups[0].ID)
			}
		}
		for i, owner := range revent.OwnedBy {
			if strings.Contains(owner, "user:") {
				userNickname := strings.TrimPrefix(owner, "user:")
				users, _, err := userService.Find(context.Background(), &crud.FindParams[user.FindFilters]{
					Filters: &user.FindFilters{Nickname: &userNickname},
				})
				if err != nil {
					return err
				}
				if len(users) != 1 {
					return fmt.Errorf("was looking for user with name: %s, found: %d", userNickname, len(users))
				}
				revent.OwnedBy[i] = users[0].ID
			}
		}

		if revent.Category != "" {
			if strings.Contains(revent.Category, "category:") {
				categoryName := strings.TrimPrefix(revent.Category, "category:")
				categories, _, err := categoryService.Find(context.Background(), &crud.FindParams[taxonomy.Filters]{
					Filters: &taxonomy.Filters{Name: &categoryName},
				})
				if err != nil {
					return err
				}
				revent.Category = categories[0].ID
			}
		}

		if revent.Topic != "" {
			if strings.Contains(revent.Topic, "topic:") {
				topicName := strings.TrimPrefix(revent.Topic, "topic:")
				topics, _, err := topicService.Find(context.Background(), &crud.FindParams[taxonomy.Filters]{
					Filters: &taxonomy.Filters{Name: &topicName},
				})
				if err != nil {
					return err
				}
				revent.Topic = topics[0].ID
			}
		}

		createdREvent, err := reventStore.Create(context.Background(), revent)
		if err != nil {
			return fmt.Errorf("importing revent %d: %s", i, err)
		}

		// Generate events from repeated event for 1 month
		if len(revent.Intervals) > 0 {
			_, err := reventService.GenerateEvents(context.Background(), createdREvent.ID,
				revent.Start,
				revent.Start.AddDate(0, 1, 0))
			if err != nil {
				return fmt.Errorf("generating events from revent %d: %s", i, err)
			}
		}

	}
	return nil
}

func importTopics(dataPath string, topicStorer taxonomy.TopicStorer) error {
	topicsData, err := os.ReadFile(path.Join(dataPath, "/topics.json"))
	if err != nil {
		return fmt.Errorf("reading topics.json: %s", err)
	}

	topics := make([]*taxonomy.NewTopic, 0)
	err = json.Unmarshal(topicsData, &topics)
	if err != nil {
		return fmt.Errorf("decoding topic: %s", err)
	}

	for i, category := range topics {
		_, err = topicStorer.Create(context.Background(), category)
		if err != nil {
			return fmt.Errorf("importing topic  %d: %s", i, err)
		}
	}
	return nil
}

func importPlaces(dataPath string, placeStore place.Service) error {
	placesData, err := os.ReadFile(path.Join(dataPath, "/places.json"))
	if err != nil {
		return fmt.Errorf("reading places.json: %s", err)
	}

	places := make([]*place.NewPlace, 0)
	err = json.Unmarshal(placesData, &places)
	if err != nil {
		return fmt.Errorf("decoding places: %s", err)
	}

	for i, place := range places {
		_, err = placeStore.Create(context.Background(), place)
		if err != nil {
			return fmt.Errorf("importing place %d: %s", i, err)
		}
	}
	return nil
}

func importUsers(dataPath string, userStore user.Storer) error {
	usersData, err := os.ReadFile(path.Join(dataPath, "/users.json"))
	if err != nil {
		return fmt.Errorf("reading users.json: %s", err)
	}

	users := make([]*user.NewUser, 0)
	err = json.Unmarshal(usersData, &users)
	if err != nil {
		return fmt.Errorf("decoding users: %s", err)
	}

	for i, user := range users {
		_, err = userStore.Create(context.Background(), user)
		if err != nil {
			return fmt.Errorf("importing user %d: %s", i, err)
		}
	}
	return nil
}

func tptr(t time.Time) *time.Time {
	return &t
}
