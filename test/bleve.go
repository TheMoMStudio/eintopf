// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package test

import (
	"fmt"
	"os"
)

// CreateBleveTestIndex creates a bleve index name with the given testname and a
// function to remove the index.
func CreateBleveTestIndex(testname string) (string, func()) {
	indexName := fmt.Sprintf("%s.index", testname)
	return indexName, func() { os.RemoveAll(indexName) }
}
