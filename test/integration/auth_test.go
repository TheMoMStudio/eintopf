//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package integration_test

import (
	"context"
	"testing"
	"time"

	"eintopf.info/service/auth"
	"eintopf.info/service/dbmigration"
	"eintopf.info/service/user"
	"eintopf.info/test"
)

var privateKey = []byte(`-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEA0HuFkAxSZBfObComjS90aTPTU9x7zvi/HlZXeCZMC7RjI7gO
WrUEXRpIdWVumbyO9cvpTeRy0ojv02g0mTefT3GRD9I5MX6qlpfNIoJdXK5e54+s
waBLz2nOSvw8XMOLb0Ogz+7o2bm4nxe/ICvO43yh+jJVVdVfrGNCht4njfgk4X+K
tfY/3YsFEan5YXzIy08Bctwg19VoZ6N/uyJS4DXP5vrle1fkG8cM7rM6WCvQGt3H
Aa0qLx+885LR69DcCUeDvWUPc1vJBGoiowTra98KB58nWj3+cFvYAsHF2i581ntm
e4X7ENWLwdNt03L79yE+GDYkcKW/icACEYzZzwIDAQABAoIBADiR+9jtbwHX79wr
x9Axmz3coTA1OKcdmgZqWUux4JqgdPJBRCvayHdW5WhtgIhYwNXzYbgQEBQOsjzE
HtmgXSsaXslTOs+yVimMwU3m39yBOCFPPApCK6/5Ps/cB6kIKnKjgCRL6IUDvxP0
Rpk9KeNSc0VHmR40SsSlHm0oJMWBnVOBd0HXygKuiptvycV/vzgMxPlLlzunyh6Q
T55ERJ13jaLQ0+sIXreS0MxjxgOwZq76at74Sn85rV/G1ZE6lP+aa40g96+T4ccj
UohMS8RWzmUGi5oTHuNrta19ym7QN4vRK8zmi4kO04Ow3NKXKibvyOIGtynbQEgd
iyyoA9ECgYEA74H6h0xHagyFK3gvJPjrCkip2O7M6wGCFR6wpj9J8D1jjKTrFZmX
yoBM7Gh9FFVE7kFNcZu+Gr+dL+5kTk5k/QcpEVJ2N2VqXyCmx3SlO+FiBS1z+KYY
o/OyknZJyclnixkaByfKs35c1mauCb+PefOlzhEmAJWS+Z3ee0MvnhcCgYEA3tai
GKCz8qT6bhPsrtElqi+5xc2Zap96Lsld+vIUUQxXBOA4r3qdbJmKVNI2hFcVFliE
xqTmCrTStlgOHmIJYun//2bounpKtJp8uIsOZFBdR7VnGl4xWH4XmXVxb4Z7Y95z
lqnT49u9pDtl4GhOQZdQMf+6G7MTbu/XoezD7QkCgYAuenzRsL3TGKbnLqWHeT6z
ejJrxLvDh0FlM9V4DaQdmOwuqPpt8KaCR6+l2Qkc3wdHbSjIOT8FFdY9LPgOMixq
5e7US7phOa8Q0tdKsZo3Kp/I/W8Z+7Ggb3Rh886R+CG93pssdE5j0svVV4vCnIgN
VHkCqlVDm8Mv6bD5Hqqo9QKBgD4S6/SQT5A4BMrFIhlQiX1u7gD71NCuvApsbZGJ
/b7WASeKjj/TmCa4XqBiLm2PFIC1B8eOB8bTuB6zSLuN869qH7W1pP7NMiI9JqEB
m3C2MkXXcnEV7AkKluhYntDCVXzsaqZ9iFjgzWMCaM6qP6Jl1CEZa25r4+3zQc4J
dkqBAoGAdL0aIhofYwEIXI9ixOIAnU93f5G+K6Zprvt5Ugl6hUAjo0v/Jl5qMJmc
nxSVtq5SUCw+lb8S/FG2Vy3CLDJSMeJ/li+nH+XzphJeVbo7Q+oIhM3Cxd2oBxw1
PNmORLeYwkp4DAuX8Xykaj7YunUXVaY9k0mnmdXoJmOhetbUftE=
-----END RSA PRIVATE KEY-----`)

var publicKey = []byte(`-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0HuFkAxSZBfObComjS90
aTPTU9x7zvi/HlZXeCZMC7RjI7gOWrUEXRpIdWVumbyO9cvpTeRy0ojv02g0mTef
T3GRD9I5MX6qlpfNIoJdXK5e54+swaBLz2nOSvw8XMOLb0Ogz+7o2bm4nxe/ICvO
43yh+jJVVdVfrGNCht4njfgk4X+KtfY/3YsFEan5YXzIy08Bctwg19VoZ6N/uyJS
4DXP5vrle1fkG8cM7rM6WCvQGt3HAa0qLx+885LR69DcCUeDvWUPc1vJBGoiowTr
a98KB58nWj3+cFvYAsHF2i581ntme4X7ENWLwdNt03L79yE+GDYkcKW/icACEYzZ
zwIDAQAB
-----END PUBLIC KEY-----`)

func TestAuth(t *testing.T) {
	db, cleanupDB, err := test.CreateSqliteTestDB(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cleanupDB()

	migrationStore, err := dbmigration.NewSqlStore(db)
	if err != nil {
		t.Fatal(err)
	}
	migrationService := dbmigration.NewService(migrationStore)

	userStore, err := user.NewSqlStore(db, migrationService)
	if err != nil {
		t.Fatal(err)
	}
	userService := user.NewService(userStore)
	user, err := userService.Create(context.Background(), &user.NewUser{
		Email:    "admin@example.com",
		Nickname: "nickname",
		Password: "admin",
		Role:     auth.RoleAdmin,
	})
	if err != nil {
		t.Fatalf("userService.Create failed: %s", err)
	}
	authService, err := auth.NewService(userService, userService, privateKey, publicKey, time.UTC)
	if err != nil {
		t.Fatalf("auth.NewService failed: %s", err)
	}

	t.Run("reject with invalid credentials", func(tt *testing.T) {
		token, err := authService.Login(context.Background(), "foo", "bar")
		if err == nil {
			tt.Fatal("expected authService.Login to return an error")
		}
		if token != "" {
			tt.Fatalf("expected authService.Login to return an empty token. got %s", token)
		}
	})

	t.Run("login with valid credentials", func(tt *testing.T) {
		token, err := authService.Login(context.Background(), "admin@example.com", "admin")
		if err != nil {
			tt.Fatalf("expected authService.Login to not return an error. got %s", err.Error())
		}
		if token == "" {
			tt.Fatal("expected authService.Login to return a non empty token")
		}
	})

	t.Run("Authorize return correct role", func(tt *testing.T) {
		role, err := authService.Authorize(context.Background(), user.ID)
		if err != nil {
			tt.Fatalf("expected Authorize not return an error. got %s", err)
		}
		if role != auth.RoleAdmin {
			tt.Fatalf("expected role to be 'admin'. got %s", role)
		}
	})
}
