//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package integration_test

import (
	"context"
	"os"
	"testing"
	"time"

	"eintopf.info/service/auth"
	"eintopf.info/service/dbmigration"
	"eintopf.info/service/event"
	"eintopf.info/service/eventsearch"
	"eintopf.info/service/group"
	"eintopf.info/service/oqueue"
	"eintopf.info/service/place"
	"eintopf.info/service/revent"
	"eintopf.info/service/search"
	"eintopf.info/service/taxonomy"
	"eintopf.info/test"
)

func TestIndexEventAndEventSearch(t *testing.T) {
	db, cleanup, err := test.CreateSqliteTestDB(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	t.Cleanup(cleanup)

	queue := oqueue.NewQueue()

	migrationStore, err := dbmigration.NewSqlStore(db)
	if err != nil {
		t.Fatal(err)
	}
	migrationService := dbmigration.NewService(migrationStore)

	eventStore, err := event.NewSqlStore(db, migrationService)
	if err != nil {
		t.Fatal(err)
	}
	eventService := event.NewService(event.NewOperator(eventStore, queue), nil)

	reventStore, err := revent.NewSqlStore(db, migrationService)
	if err != nil {
		t.Fatal(err)
	}
	reventService := revent.NewOperator(revent.NewService(reventStore, eventService), queue)

	groupStore, err := group.NewSqlStore(db, migrationService)
	if err != nil {
		t.Fatal(err)
	}
	groupService := group.NewService(group.NewOperator(groupStore, queue))

	placeStore, err := place.NewSqlStore(db, migrationService)
	if err != nil {
		t.Fatal(err)
	}
	placeService := place.NewService(place.NewOperator(placeStore, queue))
	categoryService := taxonomy.NewCategoryMemoryStore()
	topicService := taxonomy.NewTopicMemoryStore()

	bleveIndex, cleanupIndex := test.CreateBleveTestIndex("TestIndexingAndSearch")
	searchService, err := search.NewService(bleveIndex, time.Second, 1, 1, time.UTC)
	if err != nil {
		t.Fatalf("seach.NewService: %s", err)
	}
	t.Cleanup(cleanupIndex)
	defer searchService.Stop()

	eventSearchService := eventsearch.NewService(searchService, eventService, groupService, placeService, reventService, categoryService, topicService)
	queue.AddSubscriber(eventSearchService.ConsumeOperation, 1)

	e, err := eventService.Create(
		auth.ContextWithID(context.Background(), "foo"),
		&event.NewEvent{Name: "test", Published: true},
	)
	if err != nil {
		t.Fatal(err)
	}

	queue.Close()

	result, err := eventSearchService.Search(context.Background(), eventsearch.Options{Query: ""})
	if err != nil {
		t.Fatalf("eventSearchService.Search: %s", err)
	}
	if len(result.Events) != 1 {
		t.Errorf("eventSearchService.Search: expected 1 hit. got %d", len(result.Events))
	}

	result, err = eventSearchService.Search(context.Background(), eventsearch.Options{
		Filters: []eventsearch.Filter{
			eventsearch.IDFilter{ID: e.ID},
		},
	},
	)
	if err != nil {
		t.Fatalf("eventSearchService.Search: %s", err)
	}
	if len(result.Events) != 1 {
		t.Errorf("eventSearchService.Search: expected 1 hit. got %d", len(result.Events))
	}
}

func TestIndexRepeatingEventAndEventSearch(t *testing.T) {
	db, cleanup, err := test.CreateSqliteTestDB(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer cleanup()

	queue := oqueue.NewQueue()

	migrationStore, err := dbmigration.NewSqlStore(db)
	if err != nil {
		t.Fatal(err)
	}
	migrationService := dbmigration.NewService(migrationStore)

	eventStore, err := event.NewSqlStore(db, migrationService)
	if err != nil {
		t.Fatal(err)
	}
	eventService := event.NewService(event.NewOperator(eventStore, queue), nil)

	reventStore, err := revent.NewSqlStore(db, migrationService)
	if err != nil {
		t.Fatal(err)
	}
	reventService := revent.NewOperator(revent.NewService(reventStore, eventService), queue)

	groupStore, err := group.NewSqlStore(db, migrationService)
	if err != nil {
		t.Fatal(err)
	}
	groupService := group.NewService(group.NewOperator(groupStore, queue))

	placeStore, err := place.NewSqlStore(db, migrationService)
	if err != nil {
		t.Fatal(err)
	}
	placeService := place.NewService(place.NewOperator(placeStore, queue))
	categoryService := taxonomy.NewCategoryMemoryStore()
	topicService := taxonomy.NewTopicMemoryStore()

	bleveIndex, cleanupIndex := test.CreateBleveTestIndex("TestRepeatingEventAndEventSearch")
	searchService, err := search.NewService(bleveIndex, time.Second, 1, 1, time.UTC)
	if err != nil {
		t.Fatalf("seach.NewService: %s", err)
	}
	t.Cleanup(cleanupIndex)
	defer searchService.Stop()

	eventSearchService := eventsearch.NewService(searchService, eventService, groupService, placeService, reventService, categoryService, topicService)
	queue.AddSubscriber(eventSearchService.ConsumeOperation, 1)

	e, err := reventService.Create(
		auth.ContextWithID(context.Background(), "foo"),
		&revent.NewRepeatingEvent{
			Name: "test",
			Intervals: []revent.Interval{
				{Type: revent.IntervalWeek, Interval: 1},
			},
		},
	)
	if err != nil {
		t.Fatal(err)
	}
	repeatingEvents, err := reventService.GenerateEvents(
		auth.ContextWithID(context.Background(), "foo"),
		e.ID,
		time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
		time.Date(2022, 3, 1, 0, 0, 0, 0, time.UTC),
	)
	if err != nil {
		t.Fatal(err)
	}

	queue.Close()

	result, err := eventSearchService.Search(context.Background(), eventsearch.Options{Query: ""})
	if err != nil {
		t.Fatalf("eventSearchService.Search: %s", err)
	}
	if len(result.Events) != len(repeatingEvents) {
		t.Errorf("eventSearchService.Search: expected %d hit. got %d", len(repeatingEvents), len(result.Events))
	}
}

func cleanupIndex(name string) error {
	return os.RemoveAll(name)
}
