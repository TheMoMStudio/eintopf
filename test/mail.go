// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package test

type Mail struct {
	Email   string
	Subject string
	Body    string
}

type Mailer struct {
	Mails []Mail
}

func NewMailer() *Mailer {
	return &Mailer{Mails: []Mail{}}
}

func (m *Mailer) Send(email, subject, body string) error {
	m.Mails = append(m.Mails, Mail{Email: email, Subject: subject, Body: body})
	return nil
}
