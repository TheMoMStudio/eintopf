# SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
#
# SPDX-License-Identifier: CC0-1.0

GOIMPORT_PACKAGE=golang.org/x/tools/cmd/goimports@v0.18.0 # renovate: datasource=go
PACKAGE=eintopf.info
VERSION=$(shell git describe --tags --always --abbrev=0 --match='v[0-9]*.[0-9]*.[0-9]*' 2> /dev/null | sed 's/^.//')
COMMIT_HASH=$(shell git rev-parse --short HEAD)
LDFLAGS="-X '${PACKAGE}/web.vNumber=${VERSION}' -X '${PACKAGE}/web.vCommitHash=${COMMIT_HASH}'"

.PHONY: help                  ## Show this help
help:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/ ##//' | sed -e 's/.PHONY: //'

.PHONY: all                   ## Download dependencies, build the project and run tests
all: assert-tools download-dependencies build test

.PHONY: watch
watch:
	go run github.com/cortesi/modd/cmd/modd -f scripts/modd.conf

.PHONY: debug
debug: ## Starts up the project with debugging options(see build-api-debug)
	go run github.com/cortesi/modd/cmd/modd -f scripts/modd-debug.conf

.PHONY: run-api-dev           ## Runs the dev api
run-api-dev: eintopf.dev.db
	EINTOPF_SQLITE_DB="eintopf.dev.db" \
	EINTOPF_KILLSWITCH_HASHES="foo" \
	EINTOPF_MEDIA_PATH="test/data/media" \
	EINTOPF_BACKSTAGE_ASSET_PATH="backstage/build" \
	EINTOPF_ADMIN_EMAIL="admin@example.com" \
	EINTOPF_ADMIN_PASSWORD="admin" \
	EINTOPF_AUTH_KEY_PATH="auth-key-dev" \
	EINTOPF_THEMES_PATH="test/themes/" \
	EINTOPF_THEME="eintopf" \
	EINTOPF_BASE_URL="http://localhost:3333/" \
	EINTOPF_TIMEZONE=Europe/Berlin \
	EINTOPF_HTTP_READ_TIMEOUT=35s \
	EINTOPF_HTTP_HANDLER_TIMEOUT=35s \
	EINTOPF_HTTP_WRITE_TIMEOUT=35s \
	EINTOPF_OSM_TILE_CACHE_DIR=osmcache \
	EINTOPF_OSM_TILE_SERVER="https://tile.openstreetmap.org/{z}/{x}/{y}.png" \
	EINTOPF_DEBUG=true \
	./out/eintopf

eintopf.dev.db:
	go run test/createDevDB/main.go -dbpath eintopf.dev.db

.PHONY: clean                 ## Removes all dev artifacts needed to run the dev server
clean:
	rm -rf index.bleve
	rm -f eintopf.dev.db
	rm -f auth-key-dev
	rm -f auth-key-dev.pub

.PHONY: generate              ## Generate mock stubs and swagger docs
generate:
	go generate ./...
	go run scripts/gen/ts/searchfilter/main.go -modelPath service/eventsearch/eventdocument.yaml -path backstage/src/api/eventsearch_filter.ts
	go run scripts/gen/ts/searchaggregation/main.go -modelPath service/eventsearch/eventdocument.yaml -path backstage/src/api/eventsearch_aggregation.ts
	cd backstage && npx msw-auto-mock ../api/swagger.json -o src/api-mock --base-url "/api/v1"
	go run $(GOIMPORT_PACKAGE) -w .

.PHONY: build                 ## Build the backstage and api
build:
	make build-backstage
	make build-api

.PHONY: build-api             ## Build the api (output is in ./out)
build-api:
	go build -ldflags=$(LDFLAGS) -o ./out/eintopf ./cmd/eintopf

.PHONY: build-api-debug
build-api-debug: ## Builds the api with a remote delve debugging server that uses dap to connect to on port 4040
	dlv debug --headless --listen :4040 --log --log-output="dap" --output ./out/eintopf --build-flags="-gcflags='all=-N -l'" ./cmd/eintopf

.PHONY: build-backstage             ## Build the backstage (output is in ./backstage/build)
build-backstage:
	cd backstage && yarn run build

.PHONY: fmt
fmt:
	goimports
	yarn workspaces run lint --fix

.PHONY: lint                  ## Run lint tests on the project
lint:
	yarn workspaces run lint

.PHONY: test                  ## Run api tests
test: test-api

.PHONY: test-api             ## Run api and backstage tests
test-api:
	go test -timeout 80s -cover ./...

.PHONY: test-backstage      ## Run backstage tests
test-backstage:
	cd backstage && yarn run test

TAG ?= latest

# PLATFORMS = amd64 arm64
.PHONY: image-build           ## Build the docker image locally
image-build:
	docker buildx build \
		--platform=linux/amd64 \
	    -f cmd/eintopf/Dockerfile \
		--tag codeberg.org/klasse-methode/eintopf:$(subst /,-,$(TAG)) \
		--build-arg LDFLAGS=${LDFLAGS} \
		${EXTRA_ARGS} .

image-push:                   ## Push the local docker image to codeberg
	docker login codeberg.org
	docker push codeberg.org/klasse-methode/eintopf:${TAG}

docker-local-build: ## Builds a docker image that is pushed to the local registry and is tagged with local
	docker buildx build \
	-f cmd/eintopf/Dockerfile \
	--tag codeberg.org/klasse-methode/eintopf:local \
	. \
	--load \
	--build-arg LDFLAGS=${LDFLAGS}
docker-local-run: ## Runs the build local image
	docker run --name eintopf_local -p 3333:3333 \
	--env EINTOPF_SQLITE_DB="eintopf.dev.db" \
	--env EINTOPF_KILLSWITCH_HASHES="foo" \
	--env EINTOPF_ADMIN_EMAIL="admin@example.com" \
	--env EINTOPF_ADMIN_PASSWORD="admin" \
	--env EINTOPF_AUTH_KEY_PATH="auth-key-dev" \
	--env EINTOPF_THEMES_PATH="/eintopf/themes/" \
	--env EINTOPF_THEME="eintopf" \
	--env EINTOPF_BASE_URL="http://localhost:3333/" \
	--env EINTOPF_TIMEZONE=Europe/Berlin \
	--env EINTOPF_HTTP_READ_TIMEOUT=35s \
	--env EINTOPF_HTTP_HANDLER_TIMEOUT=35s \
	--env EINTOPF_HTTP_WRITE_TIMEOUT=35s \
	--env EINTOPF_OSM_TILE_CACHE_DIR=osmcache \
	--env EINTOPF_OSM_TILE_SERVER="https://tile.openstreetmap.org/{z}/{x}/{y}.png" \
	--env EINTOPF_DEBUG=true \
	codeberg.org/klasse-methode/eintopf:local

.PHONY: download-dependencies ## Downloads api and backstage dependencies
download-dependencies:
	go mod download
	yarn install

.PHONY: assert-tools          ## Assert that all nececarry tools are installed
assert-tools:
	which go
	which yarn
	which openssl
