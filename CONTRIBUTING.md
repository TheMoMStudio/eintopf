<!--
SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>

SPDX-License-Identifier: CC0-1.0
-->

# Contributing to eintopf

## Quick Start

First you need to install [make](https://www.gnu.org/software/make/) for the build environment, [go](https://golang.org/doc/install) for the api, [yarn](https://classic.yarnpkg.com/lang/en/docs/install/) for the web, [Open SSL](https://openssl.org/) for certificate generation and [docker](https://www.docker.com/) for the tests.

Development helpers can be found in the `Makefile` located at the project root.
In order to get started check out the targets below:

* `make help` Show all make targets
* `make all`  Downloads deps, build and test the project

If `make all` succeeds your development environment should be setup correctly

## Project Layout

This shows the incomplete project structure, describing the most important
directories:

```text
├─ api/               api swagger specification
├─ cmd/               go binaries
│  └─ eintopf/        monolithic binary, serves the api and web frontend
├─ internal/          internal go packages
│  ├─ cache/          generic cache library
│  ├─ mock/           contains all mocked interfaces (used for testing)
│  ├─ xerror/         custom error types
│  ├─ xhttp/          provides helper functions for the http transport layer
│  └─ xhttptest/      provides helper functions to test the http transport layer
├─ service/           go services
│  ├─ api/            api gateway service
│  ├─ auth/           authentication service (via JWT)
│  ├─ event/          handles the event model
│  ├─ eventsearch/    search api for events, based on the search service
│  ├─ group/          handles the group model
│  ├─ indexo/         operates in an oqueue to index models into a search index
│  ├─ invitation/     provides invitations for the user model
│  ├─ killswitch/     provides a killswitch to shutdown the api
│  ├─ oqueue/         a model operation queue, that can be subscribed to
│  ├─ passwordrec/    a password recovery service for the user model
│  ├─ place/          handles the place model
│  ├─ revent/         handles the repeating event model
│  ├─ search/         search service
│  ├─ status/         provides information about the status of the api
│  └─ user/           handles the user model
├─ scripts/           misc scripts
├─ test/              integration and e2e tests
│  ├─ e2e/            e2e tests using cypress
│  └─ integration/    integration tests
└─ web/               preact frontend
   └─ src/
       ├─ api/         api client
       ├─ assets/      images, ...
       ├─ components/  frontend components
       ├─ hooks/       react hooks
       ├─ pages/       frontend pages
       ├─ searchStore/ session storage for search requests
       ├─ store/       local storage for login information etc.
       ├─ testing/     testing utilities
       └─ utils/       random utilities
```

### Environment

There is a preset environment for development in the `flake.nix` file. To use this preset you need to install
direnv with flakes support. A tutorial can be found at the [direnv repo](https://github.com/direnv/direnv/wiki/Nix).
The preset was tested with `nix-direnv`.
After that run the following command to allow direnv for this folder.
```shell
direnv allow
```

### Developing

```shell
make watch
```

This will start file watchers for the go api and the preact frontend and a local webserver running EINTOPF on port 3333. The default admin user for the backstage is "admin@example.com" and password "admin".

### Running tests

```shell
make test
```

Will run api and web tests.

### Api tests

```shell
make test-api
```

Will run api tests.

### Web tests

```shell
make test-web
```

### Updating Test-Database layout

A rule for updating the database needs to be added to ./service/*/store_sql.go
Afterwards running the test server (e.g. make watch) will trigger the database update once automatically.

### Updating Dependencies

Updating the go version:

```
go mod edit -go=1.20
```

List all dependencies and their latest version:
```
go list -m -u -f '{{if not .Indirect}}{{.Path}} {{.Version}} {{ with .Update}} -> {{.Version}}{{end}}{{end}}' all
```

Update each dependency with an update available:

```
go get -u <dep>
```

After regenerate files and run tests:

```
make generate
make test
```

### Debugging Performance

You can enable the [pprof profiler](https://pkg.go.dev/net/http/pprof) by setting `EINTOPF_DEBUG` to `true`.

Then run:
```sh
go tool pprof http://localhost:3333/debug/pprof/profile?seconds=30
```

### Iconset

The following icon set is used: [Kalai Oval Interface Icons](https://www.svgrepo.com/collection/kalai-oval-interface-icons/)
Please use icons from this set, or a similar style if no fitting icon is available.
