// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"

	"eintopf.info/internal/xhttp"
	"eintopf.info/service/action"
	"eintopf.info/service/auth"
	"eintopf.info/service/event"
	"eintopf.info/service/notification"
	"eintopf.info/service/passwordrec"
	"eintopf.info/service/revent"
	"eintopf.info/service/taxonomy"
	"eintopf.info/service/user"
	"eintopf.info/test"
	"eintopf.info/test/data"
)

func TestServer(t *testing.T) {
	var smtpHost string
	var client test.MailHogClient

	pool := test.InitDockerPool(t)
	smtpHost, client = test.RunMailhogWithSSL(t, pool)

	osmServerCalls := 0
	osmServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		osmServerCalls++
		fmt.Fprintf(w, "a tile")
	}))
	t.Cleanup(osmServer.Close)

	t.Cleanup(func() {
		os.RemoveAll("server.test.bleve")
		os.RemoveAll("server.test.sqlite")
		os.RemoveAll("test/osmcache")
	})
	err := data.CreateDevDB("../../test/data", "server.test.sqlite", false)
	if err != nil {
		t.Fatal(err)
	}
	server, err := NewServer(Config{
		SqliteDB:           "server.test.sqlite",
		Addr:               ":30303",
		HTTPReadTimeout:    1 * time.Second,
		HTTPWriteTimeout:   1 * time.Second,
		HTTPHandlerTimeout: 1 * time.Second,
		SearchTimeout:      1 * time.Second,
		AuthKeyPath:        "../../auth-key-dev",
		SearchIndexPath:    "server.test.bleve",
		AdminEmail:         "admin@example.com",
		AdminPassword:      "admin",
		KillSwitchHashes:   []string{"stopall"},
		MediaPath:          "../../test/data/media",

		MailSMTPHost:     smtpHost,
		MailSMTPPassword: "",
		MailAddress:      "eintopf@example.com",
		MailSMTPSecure:   "SSLInsecure",
		MailAllowList:    []string{"eintopf@example.com", "bar@bar.de"},

		BaseURL: "http://localhost:30303",

		OSMTileServer:   osmServer.URL + "/{z}/{x}/{y}.png",
		OSMTileCacheDir: "test/osmcache",

		Timezone: "Europe/Berlin",
	})
	if err != nil {
		t.Fatal(err)
	}
	go server.ListenAndServe()

	time.Sleep(time.Second)

	normalUser := auth.LoginResponsePayload{}
	normalUser2 := auth.LoginResponsePayload{}
	moderatorUser := auth.LoginResponsePayload{}
	adminUser := auth.LoginResponsePayload{}

	t.Run("swagger.json", func(t *testing.T) {
		swaggerJSON, err := os.ReadFile("../../api/swagger.json")
		if err != nil {
			t.Fatal("failed to read api/swagger.json")
		}
		Request(t, "/api/v1/swagger.json").Get().WantCode(200).WantData(string(swaggerJSON))
	})

	t.Run("Login", func(t *testing.T) {
		Request(t, "/api/v1/auth/login").Post(auth.Credentials{Email: "bar@bar.de", Password: "wrong password"}).WantCode(400)
		Request(t, "/api/v1/auth/login").Post(auth.Credentials{Email: "bar@bar.de", Password: "bar"}).WantCode(200).Unmarshal(&normalUser)
		Request(t, "/api/v1/auth/login").Post(auth.Credentials{Email: "foo@bar.de", Password: "bar"}).WantCode(200).Unmarshal(&normalUser2)
		Request(t, "/api/v1/auth/login").Post(auth.Credentials{Email: "moderator@example.com", Password: "moderator"}).WantCode(200).Unmarshal(&moderatorUser)
		Request(t, "/api/v1/auth/login").Post(auth.Credentials{Email: "admin@example.com", Password: "admin"}).WantCode(200).Unmarshal(&adminUser)
	})

	t.Run("Events", func(t *testing.T) {
		Request(t, "/api/v1/events").Get().WantCode(200).WantLength(18)

		// Test ownedBy filter, expect one directly owned event and one owned via group.
		Request(t, "/api/v1/events").WithURLParameterJSON("filters", event.FindFilters{OwnedBy: []string{normalUser.ID}}).Get().WantLength(2)
		Request(t, "/api/v1/events").WithURLParameterJSON("filters", event.FindFilters{LikeName: strptr("with o")}).Get().WantLength(1)
		Request(t, "/api/v1/groups").WithURLParameterJSON("filters", event.FindFilters{LikeName: strptr("oup1")}).Get().WantLength(1)
		Request(t, "/api/v1/places").WithURLParameterJSON("filters", event.FindFilters{LikeName: strptr("ace1")}).Get().WantLength(1)
	})

	t.Run("RepeatingEvents", func(t *testing.T) {
		repeatingEvent := revent.RepeatingEvent{}
		Request(t, "/api/v1/revents").Get().WantCode(200).WantLength(3)
		Request(t, "/api/v1/revents").WithAuthorization(normalUser.Token).Post(revent.NewRepeatingEvent{
			Name:      "foo repeating",
			Intervals: []revent.Interval{{Type: revent.IntervalDay, Interval: 1}},
			Category:  "1",
			Topic:     "2",
		}).WantCode(200).Unmarshal(&repeatingEvent)

		t.Cleanup(func() {
			Request(t, fmt.Sprintf("/api/v1/revents/%s", repeatingEvent.ID)).WithAuthorization(normalUser.Token).Delete().WantCode(200)
		})

		Request(t, "/api/v1/revents").Get().WantCode(200).WantLength(4)

		repeatingEvents := []revent.RepeatingEvent{}
		Request(t, fmt.Sprintf("/api/v1/revents/%s/generate", repeatingEvent.ID)).
			WithAuthorization(normalUser.Token).
			WithURLParameterTime("start", time.Now()).
			WithURLParameterTime("end", time.Now().AddDate(0, 0, 1)).
			Get().WantCode(200).WantLength(2).Unmarshal(&repeatingEvents)

		if repeatingEvents[0].Category != "1" {
			t.Errorf("repeatingEvents[0] wrong category: %s", repeatingEvents[0].Category)
		}
		if repeatingEvents[0].Topic != "2" {
			t.Errorf("repeatingEvents[0] wrong topic: %s", repeatingEvents[0].Topic)
		}
		if repeatingEvents[1].Category != "1" {
			t.Errorf("repeatingEvents[1] wrong category: %s", repeatingEvents[1].Category)
		}
		if repeatingEvents[1].Topic != "2" {
			t.Errorf("repeatingEvents[1] wrong topic: %s", repeatingEvents[1].Topic)
		}
	})

	t.Run("Groups", func(t *testing.T) {
		Request(t, "/api/v1/groups").Get().WantCode(200).WantLength(3)
	})

	t.Run("Places", func(t *testing.T) {
		Request(t, "/api/v1/places").Get().WantCode(200).WantLength(3)
	})

	t.Run("Users", func(t *testing.T) {
		Request(t, "/api/v1/users").Get().WantCode(401).WantJSON(xhttp.Error{Error: "missing authorization header"})

		users := []user.User{}
		Request(t, "/api/v1/users").WithAuthorization(normalUser.Token).Get().WantCode(200).WantLength(5).Unmarshal(&users)
		for _, u := range users {
			if u.Password != "" {
				t.Error("password should be empty")
			}
			if u.Email != "" {
				t.Error("email should be empty")
			}
			if u.Role != "" {
				t.Error("email should be empty")
			}
		}

		Request(t, "/api/v1/users").WithAuthorization(moderatorUser.Token).Get().WantCode(200).WantLength(5).Unmarshal(&users)
		for _, u := range users {
			if u.Password != "" {
				t.Error("password should be empty")
			}
		}

		Request(t, "/api/v1/users").WithAuthorization(adminUser.Token).Get().WantCode(200).WantLength(5).Unmarshal(&users)
		for _, u := range users {
			if u.Password != "" {
				t.Error("password should be empty")
			}
		}
	})

	t.Run("Actions", func(t *testing.T) {
		Request(t, "/api/v1/actions").Get().WantCode(401)
		Request(t, "/api/v1/actions").WithAuthorization(normalUser.Token).Get().WantCode(401)
		Request(t, "/api/v1/actions").WithAuthorization(moderatorUser.Token).Get().WantCode(401)
		Request(t, "/api/v1/actions").WithAuthorization(adminUser.Token).Get().WantCode(200)
	})

	t.Run("Notifications", func(t *testing.T) {
		Request(t, "/api/v1/notifications").Options().WantCode(200).WantData("").WantHeader("Access-Control-Allow-Methods", "GET,POST,PUT,PATCH,DELETE,OPTIONS")
		Request(t, "/api/v1/notifications").Get().WantCode(401).WantJSON(xhttp.Error{Error: "missing authorization header"})
		Request(t, "/api/v1/notifications").WithAuthorization(normalUser.Token).Get().WantCode(200).WantLength(0)

		Request(t, "/api/v1/notifications").WithAuthorization(normalUser.Token).Post(notification.NewNotification{
			UserID:  normalUser.ID,
			Subject: "hello",
			Message: "test message",
		}).WantCode(401).WantJSON(xhttp.Error{Error: "unauthorized"})

		n := notification.Notification{}
		Request(t, "/api/v1/notifications").WithAuthorization(moderatorUser.Token).Post(notification.NewNotification{
			UserID:  normalUser.ID,
			Subject: "hello",
			Message: "test message",
		}).WantCode(200).Unmarshal(&n)

		Request(t, "/api/v1/notifications").WithAuthorization(normalUser.Token).Get().WantCode(200).WantLength(1)
		Request(t, "/api/v1/notifications").WithAuthorization(moderatorUser.Token).Get().WantCode(200).WantLength(0)
		Request(t, "/api/v1/notifications").WithAuthorization(adminUser.Token).Get().WantCode(200).WantLength(1)
		Request(t, "/api/v1/notifications").WithAuthorization(adminUser.Token).WithURLParameterJSON("filters", notification.Filters{UserID: strptr(adminUser.ID)}).Get().WantCode(200).WantLength(0)

		Request(t, fmt.Sprintf("/api/v1/notifications/%s", n.ID)).Get().WantCode(401).WantJSON(xhttp.Error{Error: "missing authorization header"})
		Request(t, fmt.Sprintf("/api/v1/notifications/%s", n.ID)).WithAuthorization(normalUser.Token).Get().WantCode(200)
		Request(t, fmt.Sprintf("/api/v1/notifications/%s", n.ID)).WithAuthorization(normalUser2.Token).Get().WantCode(401)
		Request(t, fmt.Sprintf("/api/v1/notifications/%s", n.ID)).WithAuthorization(moderatorUser.Token).Get().WantCode(401)
		Request(t, fmt.Sprintf("/api/v1/notifications/%s", n.ID)).WithAuthorization(adminUser.Token).Get().WantCode(200)

		// TODO: test email received with mailhog
	})

	t.Run("RepeatingEventExpired", func(t *testing.T) {
		repeatingEvent := revent.RepeatingEvent{}
		Request(t, "/api/v1/revents").WithAuthorization(normalUser.Token).Post(revent.NewRepeatingEvent{
			Name: "MyRepeatingEvent",
			Intervals: []revent.Interval{
				{
					Type:     revent.IntervalDay,
					Interval: 1,
				},
			},
			OwnedBy: []string{normalUser.ID},
		}).WantCode(200).Unmarshal(&repeatingEvent)

		now := time.Date(2020, 3, 4, 0, 0, 0, 0, time.UTC)
		Request(t, fmt.Sprintf("/api/v1/revents/%s/generate", repeatingEvent.ID)).
			WithAuthorization(normalUser.Token).
			WithURLParameterTime("start", now.Add(-time.Hour*24*99)).
			WithURLParameterTime("end", now.Add(-time.Hour*24*90)).
			Get().WantCode(200).WantLength(10)

		Request(t, "/api/v1/actions/run").WithAuthorization(adminUser.Token).Post(action.RunRequest{
			Name: "Notify RepeatingEvent Expired",
		}).WantCode(200)

		notifications := []notification.Notification{}
		Request(t, "/api/v1/notifications").WithAuthorization(normalUser.Token).Get().WantCode(200).WantLength(2).Unmarshal(&notifications)

		notifications[1].CreatedAt = time.Time{}

		if diff := cmp.Diff(notification.Notification{
			ID:        notifications[1].ID,
			UserID:    normalUser.ID,
			Subject:   "Regelmäßige Veranstaltung abgelaufen",
			Message:   "Für die regelmäßige Veranstaltung 'MyRepeatingEvent' wurden keine neuen Veranstaltungen mehr erstellt. Die letzte Veranstaltung war am 05.12.2019",
			LinkTitle: "Zur Regelmäßigen Veranstaltung",
			LinkURL:   fmt.Sprintf("/backstage/repeatingevents/%s#generate", repeatingEvent.ID),
		}, notifications[1]); diff != "" {
			t.Errorf("notification mismatch (-want +got):\n%s", diff)
		}

		// Run the function again, to make sure no new notification gets send
		Request(t, "/api/v1/actions/run").WithAuthorization(adminUser.Token).Post(action.RunRequest{
			Name: "Notify RepeatingEvent Expired",
		}).WantCode(200)

		Request(t, "/api/v1/notifications").WithAuthorization(normalUser.Token).Get().WantCode(200).WantLength(2)

		// Regenerate events. This should create a new notification.
		Request(t, fmt.Sprintf("/api/v1/revents/%s/generate", repeatingEvent.ID)).
			WithAuthorization(normalUser.Token).
			WithURLParameterTime("start", now.Add(-time.Hour*24*89)).
			WithURLParameterTime("end", now.Add(-time.Hour*24*80)).
			Get().WantCode(200).WantLength(10)

		// Deactivate the repeating event, this should prevent notifications from being triggered.
		repeatingEvent.Deactivated = true
		Request(t, fmt.Sprintf("/api/v1/revents/%s", repeatingEvent.ID)).WithAuthorization(adminUser.Token).Put(repeatingEvent).WantCode(200)
		Request(t, "/api/v1/actions/run").WithAuthorization(adminUser.Token).Post(action.RunRequest{
			Name: "Notify RepeatingEvent Expired",
		}).WantCode(200)
		Request(t, "/api/v1/notifications").WithAuthorization(normalUser.Token).Get().WantCode(200).WantLength(2)

		// Activate the repeating event again.
		repeatingEvent.Deactivated = false
		Request(t, fmt.Sprintf("/api/v1/revents/%s", repeatingEvent.ID)).WithAuthorization(adminUser.Token).Put(repeatingEvent).WantCode(200)
		Request(t, "/api/v1/actions/run").WithAuthorization(adminUser.Token).Post(action.RunRequest{
			Name: "Notify RepeatingEvent Expired",
		}).WantCode(200)

		Request(t, "/api/v1/notifications").WithAuthorization(normalUser.Token).Get().WantCode(200).WantLength(3).Unmarshal(&notifications)
		notifications[2].CreatedAt = time.Time{}
		if diff := cmp.Diff(notification.Notification{
			ID:        notifications[2].ID,
			UserID:    normalUser.ID,
			Subject:   "Regelmäßige Veranstaltung abgelaufen",
			Message:   "Für die regelmäßige Veranstaltung 'MyRepeatingEvent' wurden keine neuen Veranstaltungen mehr erstellt. Die letzte Veranstaltung war am 15.12.2019",
			LinkTitle: "Zur Regelmäßigen Veranstaltung",
			LinkURL:   fmt.Sprintf("/backstage/repeatingevents/%s#generate", repeatingEvent.ID),
		}, notifications[2]); diff != "" {
			t.Errorf("notification mismatch (-want +got):\n%s", diff)
		}

		// Regenerate events and then run the action again. This should create no new notifications
		Request(t, fmt.Sprintf("/api/v1/revents/%s/generate", repeatingEvent.ID)).
			WithAuthorization(normalUser.Token).
			WithURLParameterTime("start", time.Now().AddDate(0, 0, 90)).
			WithURLParameterTime("end", time.Now().AddDate(0, 0, 99)).
			Get().WantCode(200)

		Request(t, "/api/v1/actions/run").WithAuthorization(adminUser.Token).Post(action.RunRequest{
			Name: "Notify RepeatingEvent Expired",
		}).WantCode(200)

		Request(t, "/api/v1/notifications").WithAuthorization(normalUser.Token).Get().WantCode(200).WantLength(3)
	})

	t.Run("PasswordRecovery return 200 with wrong email", func(t *testing.T) {
		Request(t, "/api/v1/passwordrec/requestRecovery").Post(passwordrec.RecoveryRequest{
			Email: "fooooooooooo@example.com",
		}).WantCode(200)
	})

	t.Run("PasswordRecovery disallowed email", func(t *testing.T) {
		Request(t, "/api/v1/passwordrec/requestRecovery").Post(passwordrec.RecoveryRequest{
			Email: "bar@baaaaaaaaaaaaaaaaaar.de",
		}).WantCode(200)

		time.Sleep(time.Millisecond * 10)

		_, err := client.GetLastEmail()
		if err == nil || err.Error() != "no emails received" {
			t.Error("should not send email")
		}
	})

	t.Run("PasswordRecovery", func(t *testing.T) {
		Request(t, "/api/v1/passwordrec/requestRecovery").Post(passwordrec.RecoveryRequest{
			Email: "bar@bar.de",
		}).WantCode(200)
		time.Sleep(time.Millisecond * 10)

		m, err := client.GetLastEmail()
		if err != nil {
			t.Error(err)
			return
		}
		from := "eintopf@example.com"
		to := "bar@bar.de"
		subject := "Passwort Zurücksetzten"
		if m.Raw.From != from {
			t.Errorf("want FROM to be %s, got %s", from, m.Raw.From)
		}
		if m.Raw.To[0] != to {
			t.Errorf("want TO to be %s, got %s", to, m.Raw.To[0])
		}
		if m.Content.Headers["Subject"][0] != subject {
			t.Errorf("want SUBJECT to be '%s', got '%s'", subject, m.Content.Headers["Subject"][0])
		}
		recoveryUrl := lastLine(m.Content.Body)

		// Check that the recovery url is callable
		Request(t, recoveryUrl).Get().WantCode(200)
		splitted := strings.Split(recoveryUrl, "=")
		if len(splitted) != 2 {
			t.Fatalf("recovery url is not correct: %s", recoveryUrl)
		}

		Request(t, "/api/v1/passwordrec/setPassword").Post(passwordrec.SetPasswordRequest{
			Token:    splitted[1],
			Password: "mynewpassword",
		}).WantCode(200)

		Request(t, "/api/v1/auth/login").Post(auth.Credentials{Email: "bar@bar.de", Password: "bar"}).WantCode(400)
		Request(t, "/api/v1/auth/login").Post(auth.Credentials{Email: "bar@bar.de", Password: "mynewpassword"}).WantCode(200).Unmarshal(&normalUser)
	})

	t.Run("category", func(t *testing.T) {
		Request(t, "/api/v1/categories").Get().WantCode(200).WantLength(2)
		Request(t, "/api/v1/categories").WithAuthorization(normalUser.Token).Post(taxonomy.NewCategory{Name: "foo", Headline: "Foo!", Description: "foooo"}).WantCode(401)
		Request(t, "/api/v1/categories").WithAuthorization(moderatorUser.Token).Post(taxonomy.NewCategory{Name: "foo", Headline: "Foo!", Description: "foooo"}).WantCode(200)
		Request(t, "/api/v1/categories").Get().WantCode(200).WantLength(3)
	})

	t.Run("topic", func(t *testing.T) {
		Request(t, "/api/v1/topics").Get().WantCode(200).WantLength(2)
		Request(t, "/api/v1/topics").WithAuthorization(normalUser.Token).Post(taxonomy.NewTopic{Name: "foo", Headline: "Foo!", Description: "foooo"}).WantCode(401)
		Request(t, "/api/v1/topics").WithAuthorization(moderatorUser.Token).Post(taxonomy.NewTopic{Name: "foo", Headline: "Foo!", Description: "foooo"}).WantCode(200)
		Request(t, "/api/v1/topics").Get().WantCode(200).WantLength(3)
	})

	t.Run("OSM", func(t *testing.T) {
		Request(t, "/api/v1/osm/tile/1/1/1.png").Get().WantCode(200).WantData("a tile").WantHeader("Content-Type", "image/png")
		if osmServerCalls != 1 {
			t.Errorf("should have one osm server calls, got %d", osmServerCalls)
		}

		// Should hit file cache
		Request(t, "/api/v1/osm/tile/1/1/1.png").Get().WantCode(200).WantData("a tile")
		if osmServerCalls != 1 {
			t.Errorf("should have one osm server calls, got %d", osmServerCalls)
		}
	})

	t.Run("cms", func(t *testing.T) {
		Request(t, "/api/v1/cms/themes").WithAuthorization(normalUser.Token).Get().WantCode(401)
		Request(t, "/api/v1/cms/themes").WithAuthorization(adminUser.Token).Get().WantCode(200).WantLength(2)
	})

	t.Run("web", func(t *testing.T) {
		Request(t, "/").Get().WantCode(200)
		Request(t, "/orte").Get().WantCode(200)

		t.Run("strips slash at the end", func(t *testing.T) {
			Request(t, "/orte/").Get().WantCode(200)
			Request(t, "//").Get().WantCode(200)
		})
	})
}

func lastLine(s string) string {
	split := strings.Split(s, "\n")
	return strings.TrimSpace(split[len(split)-1])
}

func Request(t *testing.T, uri string) request {
	t.Helper()
	if !strings.HasPrefix(uri, "http://") {
		uri = "http://localhost:30303" + uri
	}
	u, err := url.Parse(uri)
	if err != nil {
		t.Error(err)
	}
	return request{t: t, url: u, headers: http.Header{}}
}

type request struct {
	t       *testing.T
	url     *url.URL
	headers http.Header
}

func (r request) WithAuthorization(token string) request {
	r.headers["Authorization"] = []string{token}
	return r
}

func (r request) WithURLParameter(key string, value string) request {
	q := r.url.Query()
	q.Add(key, value)
	r.url.RawQuery = q.Encode()
	return r
}

func (r request) WithURLParameterTime(key string, data time.Time) request {
	return r.WithURLParameter(key, data.Format(time.RFC3339))
}

func (r request) WithURLParameterJSON(key string, data any) request {
	raw, err := json.Marshal(data)
	if err != nil {
		r.t.Error(err)
	}
	return r.WithURLParameter(key, url.QueryEscape(string(raw)))
}

func (r request) request(method string, body io.ReadCloser) response {
	r.t.Helper()

	client := http.Client{}
	resp, err := client.Do(&http.Request{Method: method, URL: r.url, Header: r.headers, Body: body})
	if err != nil {
		r.t.Error(err)
	}
	rbody := []byte{}
	if resp.Body != nil {
		rbody, err = io.ReadAll(resp.Body)
		if err != nil {
			r.t.Error(err)
		}
	}
	return response{
		t:       r.t,
		status:  resp.Status,
		code:    resp.StatusCode,
		body:    rbody,
		headers: resp.Header,
	}
}

func (r request) Get() response {
	r.t.Helper()

	return r.request("GET", nil)
}

func (r request) Post(payload any) response {
	r.t.Helper()
	payloadRaw, err := json.Marshal(payload)
	if err != nil {
		r.t.Error(err)
	}
	return r.request("POST", io.NopCloser(bytes.NewBuffer(payloadRaw)))
}

func (r request) Put(payload any) response {
	r.t.Helper()
	payloadRaw, err := json.Marshal(payload)
	if err != nil {
		r.t.Error(err)
	}
	return r.request("PUT", io.NopCloser(bytes.NewBuffer(payloadRaw)))
}

func (r request) Delete() response {
	return r.request("DELETE", nil)
}

func (r request) Options() response {
	return r.request("OPTIONS", nil)
}

type response struct {
	t       *testing.T
	status  string
	code    int
	body    []byte
	headers http.Header
}

func (r response) WantCode(code int) response {
	r.t.Helper()

	if r.code != code {
		r.t.Errorf("http status (want: %d): %d", code, r.code)
	}
	return r
}

func (r response) WantHeader(key, value string) response {
	r.t.Helper()

	v := r.headers.Get(key)
	if value != v {
		r.t.Errorf("header mismatch (%s): want %s, got %s", key, value, v)
	}
	return r
}

func (r response) WantData(data string) response {
	if string(r.body) != data {
		r.t.Errorf("want data %s, got %s", data, string(r.body))
	}
	return r
}

func (r response) WantLength(length int) response {
	r.t.Helper()

	var got []any
	err := json.Unmarshal(r.body, &got)
	if err != nil {
		r.t.Errorf("%s\nbody: %s", err, string(r.body))
	}
	if len(got) != length {
		r.t.Errorf("length mismatch: want: %d, got: %d", length, len(got))
	}
	return r
}

func (r response) WantJSON(want any) response {
	r.t.Helper()

	if want == nil {
		if len(r.body) != 0 {
			r.t.Error("body should be empty")
		}
		return r
	}

	var got any
	err := json.Unmarshal(r.body, &got)
	if err != nil {
		r.t.Error(err)
	}

	gotRaw, err := json.MarshalIndent(got, "", "  ")
	if err != nil {
		r.t.Error(err)
	}
	wantRaw, err := json.MarshalIndent(want, "", "  ")
	if err != nil {
		r.t.Error(err)
	}
	if diff := cmp.Diff(string(wantRaw), string(gotRaw)); diff != "" {
		r.t.Errorf("data mismatch (-want +got):\n%s", diff)
	}
	return r
}

func (r response) Unmarshal(v any) response {
	r.t.Helper()

	err := json.Unmarshal(r.body, v)
	if err != nil {
		r.t.Error(err)
	}
	return r
}

func strptr(str string) *string { return &str }
