//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"fmt"
	"log"

	"eintopf.info/internal/crud"
	"eintopf.info/service/auth"
	"eintopf.info/service/event"
	"eintopf.info/service/group"
	"eintopf.info/service/place"
	"eintopf.info/service/revent"
)

// doctorEventsSetOrganzierAndLocation checks all events and set the organizer
// or location to the group or location if they exist.
func doctorEventsRemoveEmptyOrganizers(eventService event.Storer) {
	events, _, err := eventService.Find(context.Background(), nil)
	if err != nil {
		log.Println(fmt.Errorf("doctorEventsRemoveEmptyOrganizers: find events: %s", err))
		return
	}
	for _, e := range events {
		needsUpdate := false
		organizers := []string{}
		for _, organizer := range e.Organizers {
			if organizer != "" {
				organizers = append(organizers, organizer)
			} else {
				log.Printf("doctorEventsRemoveEmptyOrganizers: remove empty organizer from %s", e.Name)
				needsUpdate = true
			}
		}
		if needsUpdate {
			e.Organizers = organizers
			_, err := eventService.Update(auth.ContextWithRole(context.Background(), auth.RoleInternal), e)
			if err != nil {
				log.Println(fmt.Errorf("doctorEventsRemoveEmptyOrganizers: update event %s: %s", e.Name, err))
			}
		}
	}
}

// doctorEventsSetOrganzierAndLocation checks all events and set the organizer
// or location to the group or location if they exist.
func doctorEventsSetOrganzierAndLocation(eventService event.Storer, groupService group.Service, placeService place.Service) {
	events, _, err := eventService.Find(context.Background(), nil)
	if err != nil {
		log.Println(fmt.Errorf("doctorEventsSetOrganzierAndLocation: find events: %s", err))
		return
	}
	for _, e := range events {
		needsUpdate := false
		for i, organizer := range e.Organizers {
			groups, _, err := groupService.Find(context.Background(), &crud.FindParams[group.FindFilters]{
				Filters: &group.FindFilters{
					Name: &organizer,
				},
			})
			if err != nil {
				log.Println(fmt.Errorf("doctorEventsSetOrganzierAndLocation: find groups %s: %s", organizer, err))
				continue
			}
			if len(groups) != 1 {
				continue
			}
			e.Organizers[i] = fmt.Sprintf("id:%s", groups[0].ID)
			log.Printf("doctorEventsSetOrganzierAndLocation: event %s: set group %s", e.Name, groups[0].Name)
			needsUpdate = true
		}
		if e.Location != "" {
			places, _, err := placeService.Find(context.Background(), &crud.FindParams[place.FindFilters]{
				Filters: &place.FindFilters{
					Name: &e.Location,
				},
			})
			if err != nil {
				log.Println(fmt.Errorf("doctorEventsSetOrganzierAndLocation: find places(%s): %s", e.Location, err))
			} else if len(places) == 1 {
				e.Location = fmt.Sprintf("id:%s", places[0].ID)
				log.Printf("doctorEventsSetOrganzierAndLocation: event %s: set place %s", e.Name, places[0].Name)
				needsUpdate = true
			}
		}
		if needsUpdate {
			_, err := eventService.Update(auth.ContextWithRole(context.Background(), auth.RoleInternal), e)
			if err != nil {
				log.Println(fmt.Errorf("doctorEventsSetOrganzierAndLocation: update event %s: %s", e.Name, err))
			}
		}
	}
}

// doctorRepeatingEventsSetOrganzierAndLocation checks all repeating events and
// set the organizer or location to the group or location if they exist.
func doctorRepeatingEventsSetOrganzierAndLocation(reventService revent.Service, groupService group.Service, placeService place.Service) {
	events, _, err := reventService.Find(context.Background(), nil)
	if err != nil {
		log.Println(fmt.Errorf("doctorRepeatingEventsSetOrganzierAndLocation: find events: %s", err))
		return
	}
	for _, e := range events {
		needsUpdate := false
		for i, organizer := range e.Organizers {
			groups, _, err := groupService.Find(context.Background(), &crud.FindParams[group.FindFilters]{
				Filters: &group.FindFilters{
					Name: &organizer,
				},
			})
			if err != nil {
				log.Println(fmt.Errorf("doctorRepeatingEventsSetOrganzierAndLocation: find groups %s: %s", organizer, err))
				continue
			}
			if len(groups) != 1 {
				continue
			}
			e.Organizers[i] = fmt.Sprintf("id:%s", groups[0].ID)
			log.Printf("doctorRepeatingEventsSetOrganzierAndLocation: event %s: set group %s", e.Name, groups[0].Name)
			needsUpdate = true
		}
		if e.Location != "" {
			places, _, err := placeService.Find(context.Background(), &crud.FindParams[place.FindFilters]{
				Filters: &place.FindFilters{
					Name: &e.Location,
				},
			})
			if err != nil {
				log.Println(fmt.Errorf("doctorRepeatingEventsSetOrganzierAndLocation: find places(%s): %s", e.Location, err))
			} else if len(places) == 1 {
				e.Location = fmt.Sprintf("id:%s", places[0].ID)
				log.Printf("doctorRepeatingEventsSetOrganzierAndLocation: event %s: set place %s", e.Name, places[0].Name)
				needsUpdate = true
			}
		}
		if needsUpdate {
			_, err := reventService.Update(auth.ContextWithRole(context.Background(), auth.RoleInternal), e)
			if err != nil {
				log.Println(fmt.Errorf("doctorRepeatingEventsSetOrganzierAndLocation: update event %s: %s", e.Name, err))
			}
		}
	}
}
