#!/bin/sh

# SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
#
# SPDX-License-Identifier: AGPL-3.0-only

set -e

# Read environment variables
if [[ ! -z "${EINTOPF_ADMIN_PASSWORD_FILE}" ]]; then
  EINTOPF_ADMIN_PASSWORD="$(cat $EINTOPF_ADMIN_PASSWORD_FILE)"
fi
if [[ ! -z "${EINTOPF_KILLSWITCH_HASHES_FILE}" ]]; then
  EINTOPF_KILLSWITCH_HASHES="$(cat $EINTOPF_KILLSWITCH_HASHES_FILE)"
fi
if [[ ! -z "${EINTOPF_MAIL_SMTP_PASSWORD_FILE}" ]]; then
  EINTOPF_MAIL_SMTP_PASSWORD="$(cat $EINTOPF_MAIL_SMTP_PASSWORD_FILE)"
fi

# Start eintopf
EINTOPF_ADMIN_PASSWORD=${EINTOPF_ADMIN_PASSWORD} \
EINTOPF_KILLSWITCH_HASHES=${EINTOPF_KILLSWITCH_HASHES} \
EINTOPF_MAIL_SMTP_PASSWORD=${EINTOPF_MAIL_SMTP_PASSWORD} \
    /eintopf/eintopf
