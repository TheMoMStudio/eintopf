//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-co-op/gocron/v2"
	"github.com/jmoiron/sqlx"
	"github.com/kelseyhightower/envconfig"
	_ "github.com/mattn/go-sqlite3"

	"eintopf.info/backstage"
	"eintopf.info/internal/plausible"
	"eintopf.info/internal/xhttp"
	"eintopf.info/service/action"
	"eintopf.info/service/api"
	"eintopf.info/service/auth"
	"eintopf.info/service/cms"
	"eintopf.info/service/dbmigration"
	"eintopf.info/service/email"
	"eintopf.info/service/event"
	"eintopf.info/service/eventsearch"
	"eintopf.info/service/group"
	"eintopf.info/service/ical"
	"eintopf.info/service/indexo"
	"eintopf.info/service/invitation"
	"eintopf.info/service/killswitch"
	"eintopf.info/service/media"
	"eintopf.info/service/notification"
	"eintopf.info/service/oqueue"
	"eintopf.info/service/osm"
	"eintopf.info/service/passwordrec"
	"eintopf.info/service/place"
	"eintopf.info/service/revent"
	"eintopf.info/service/rss"
	"eintopf.info/service/search"
	"eintopf.info/service/taxonomy"
	"eintopf.info/service/user"
	"eintopf.info/web"
)

func main() {
	if err := run(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func run() error {
	var conf Config
	if err := envconfig.Process("eintopf", &conf); err != nil {
		return fmt.Errorf("failed to load config: %s", err)
	}

	s, err := NewServer(conf)
	if err != nil {
		return fmt.Errorf("failed to create server: %s", err)
	}

	err = s.ListenAndServe()
	if err != nil {
		return fmt.Errorf("server: %s", err)
	}
	return s.Close()
}

// Config holds the configuration for the eintopf server.
type Config struct {
	SqliteDB string `envconfig:"SQLITE_DB" default:"eintopf.db"`

	Addr               string        `default:":3333"`
	HTTPReadTimeout    time.Duration `envconfig:"HTTP_READ_TIMEOUT" default:"5s"`
	HTTPWriteTimeout   time.Duration `envconfig:"HTTP_WRITE_TIMEOUT" default:"5s"`
	HTTPHandlerTimeout time.Duration `envconfig:"HTTP_HANDLER_TIMEOUT" default:"5s"`

	AuthKeyPath string `envconfig:"AUTH_KEY_PATH" default:"auth-key"`

	SearchIndexPath       string        `envconfig:"SEARCH_INDEX_PATH" default:"index.bleve"`
	SearchTimeout         time.Duration `envconfig:"SEARCH_TIMEAOUT" default:"10s"`
	SearchResultCacheSize int           `envconfig:"SEARCH_RESULT_CACHE_SIZE" default:"100"`
	SearchBucketCacheSize int           `envconfig:"SEARCH_BUCKET_CACHE_SIZE" default:"100"`

	BackstageAssetPath string   `envconfig:"BACKSTAGE_ASSET_PATH"`
	AdminEmail         string   `envconfig:"ADMIN_EMAIL"`
	AdminPassword      string   `envconfig:"ADMIN_PASSWORD"`
	KillSwitchHashes   []string `envconfig:"KILLSWITCH_HASHES"`
	MediaPath          string   `envconfig:"MEDIA_PATH"`
	ThemesPath         string   `envconfig:"THEMES_PATH" default:"themes/"`
	Theme              string   `envconfig:"THEME" default:"eintopf"`
	BaseURL            string   `envconfig:"BASE_URL"`

	Timezone string `envconfig:"TIMEZONE"`

	MailSMTPHost     string   `envconfig:"MAIL_SMTP_HOST"`
	MailSMTPPassword string   `envconfig:"MAIL_SMTP_PASSWORD"`
	MailSMTPUser     string   `envconfig:"MAIL_SMTP_USER"`
	MailSMTPSecure   string   `envconfig:"MAIL_SMTP_SECURE" default:"SSL"`
	MailAddress      string   `envconfig:"MAIL_ADDRESS"`
	MailAllowList    []string `envconfig:"MAIL_ALLOW_LIST"`

	PlausibleHost   string `envconfig:"PLAUSIBLE_HOST"`
	PlausibleDomain string `envconfig:"PLAUSIBLE_DOMAIN"`

	OSMTileServer   string `envconfig:"OSM_TILE_SERVER"`
	OSMTileCacheDir string `envconfig:"OSM_TILE_CACHE_DIR"`

	Debug bool `envconfig:"DEBUG"`
}

// Server defines a server.
type Server struct {
	s *http.Server

	searchService search.Service
	scheduler     gocron.Scheduler
}

func readAuthKeys(path string) ([]byte, []byte, error) {
	privateKey, err := os.ReadFile(path)
	if err != nil {
		return nil, nil, fmt.Errorf("read private key: %s: %w", path, err)
	}
	publicKey, err := os.ReadFile(path + ".pub")
	if err != nil {
		return nil, nil, fmt.Errorf("read public key: %s: %w", path+".pub", err)
	}
	return privateKey, publicKey, nil
}

func writeAuthKeys(path string, privateKey, publicKey []byte) error {
	err := os.WriteFile(path, privateKey, 0o666)
	if err != nil {
		return fmt.Errorf("write private key: %s: %w", path, err)
	}
	err = os.WriteFile(path+".pub", publicKey, 0o666)
	if err != nil {
		return fmt.Errorf("write public key: %s: %w", path+".pub", err)
	}
	return nil
}

// NewServer returns a new monolithic server.
func NewServer(conf Config) (*Server, error) {
	tz, err := time.LoadLocation(conf.Timezone)
	if err != nil {
		return nil, fmt.Errorf("invalid TIMEZONE: %s", err)
	}

	privateKey, publicKey, err := readAuthKeys(conf.AuthKeyPath)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			privateKey, publicKey, err = auth.CreateAuthKeys()
			if err != nil {
				return nil, err
			}
			err = writeAuthKeys(conf.AuthKeyPath, privateKey, publicKey)
			if err != nil {
				return nil, err
			}
		} else {
			return nil, err
		}
	}

	emailService := email.NewService(email.Config{
		SMTPHost:     conf.MailSMTPHost,
		SMTPPassword: conf.MailSMTPPassword,
		SMTPUser:     conf.MailSMTPUser,
		SMTPSecure:   conf.MailSMTPSecure,
		Mail:         conf.MailAddress,
		AllowList:    conf.MailAllowList,
	})

	db, err := sqlx.Connect("sqlite3", conf.SqliteDB)
	if err != nil {
		return nil, fmt.Errorf("cannot connect to sqlite db: %s: %s", conf.SqliteDB, err)
	}

	migrationStore, err := dbmigration.NewSqlStore(db)
	if err != nil {
		return nil, fmt.Errorf("failed to create migration store: %s", err)
	}
	migrationService := dbmigration.NewService(migrationStore)

	scheduler, err := gocron.NewScheduler()
	if err != nil {
		return nil, fmt.Errorf("failed to create scheduler: %s", err)
	}
	actionStore, err := action.NewSqlStore(db, migrationService)
	if err != nil {
		return nil, fmt.Errorf("failed to create action store: %s", err)
	}
	actionService := action.NewService(action.NewAuthorizer(actionStore), scheduler)

	searchService, err := search.NewService(conf.SearchIndexPath, conf.SearchTimeout, conf.SearchResultCacheSize, conf.SearchBucketCacheSize, tz)
	if err != nil {
		return nil, fmt.Errorf("failed to create search.Service: %s", err)
	}

	// User Service
	userStore, err := user.NewSqlStore(db, migrationService)
	if err != nil {
		return nil, err
	}
	userService := user.NewService(user.NewAuthorizer(userStore))

	notificationStore, err := notification.NewSqlStore(db, migrationService)
	if err != nil {
		return nil, fmt.Errorf("notificationStore: %s", err)
	}
	notificationSettingsStore, err := notification.NewSettingsSqlStore(db, migrationService)
	if err != nil {
		return nil, fmt.Errorf("notificationStore: %s", err)
	}
	notificationService := notification.NewService(
		emailService,
		notification.NewAuthorizer(notificationStore),
		notification.NewSettingsAuthorizer(notificationSettingsStore),
		userService,
	)

	queue := oqueue.NewQueue()

	// Group Service
	groupStore, err := group.NewSqlStore(db, migrationService)
	if err != nil {
		return nil, err
	}
	groupAuthorizer := group.NewAuthorizer(groupStore)
	groupService := group.NewService(
		group.NewOperator(
			groupAuthorizer,
			queue,
		),
	)

	// Place Service
	placeStore, err := place.NewSqlStore(db, migrationService)
	if err != nil {
		return nil, err
	}
	placeAuthorizer := place.NewAuthorizer(placeStore)
	placeService := place.NewService(
		place.NewOperator(
			placeAuthorizer,
			queue,
		),
	)

	// Event Service
	eventStore, err := event.NewSqlStore(db, migrationService)
	if err != nil {
		return nil, err
	}
	eventService := event.NewAuthorizer(
		event.NewOperator(
			event.NewService(eventStore, groupService),
			queue,
		),
		groupAuthorizer,
	)

	// Repeating Event Service
	repeatingEventStore, err := revent.NewSqlStore(db, migrationService)
	if err != nil {
		return nil, err
	}
	repeatingEventService := revent.NewAuthorizer(
		revent.NewOperator(
			revent.NewService(repeatingEventStore, eventService),
			queue,
		),
	)

	reventExpiredStore, err := revent.NewExpiredStateSqlStore(db, migrationService)
	if err != nil {
		return nil, err
	}
	reventExpired := revent.NewExpiredNotifier(reventExpiredStore, repeatingEventService, eventService, notificationService)
	queue.AddSubscriber(reventExpired.ConsumeOperation, 1)
	err = actionService.Register("Notify RepeatingEvent Expired", reventExpired.NotifyAction, "0 0 * * *")
	if err != nil {
		return nil, fmt.Errorf("failed to register action: %s", err)
	}

	cmsService, err := cms.NewService(conf.ThemesPath, conf.Theme)
	if err != nil {
		return nil, fmt.Errorf("failed to register action: %s", err)
	}
	cmsAuthService := cms.NewAuthorizer(cmsService)

	// Password Recovery Service
	passwordrecStore := passwordrec.NewSqlStore(db)
	err = passwordrecStore.RunMigrations(context.Background())
	if err != nil {
		return nil, fmt.Errorf("passwordrec db migration: %s", err)
	}
	passwordrecService := passwordrec.NewService(
		emailService,
		userService,
		cmsService,
		passwordrecStore,
		conf.BaseURL,
	)

	// Invitation Service
	invitationStore := invitation.NewSqlStore(db)
	err = invitationStore.RunMigrations(context.Background())
	if err != nil {
		return nil, fmt.Errorf("invitation db migration: %s", err)
	}
	invitationService := invitation.NewAuthorizer(
		invitation.NewService(
			invitationStore,
			userService,
		),
	)
	mediaService := media.NewAuthorizer(
		media.NewService(conf.MediaPath),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create passwordrec.Service: %s", err)
	}
	authService, err := auth.NewService(userService, userService, privateKey, publicKey, tz)
	if err != nil {
		return nil, fmt.Errorf("failed to create auth.Service: %s", err)
	}

	categoryStore, err := taxonomy.NewCategorySqlStore(db, migrationService)
	if err != nil {
		return nil, fmt.Errorf("categoryStore: %s", err)
	}
	categoryService := taxonomy.NewCategoryAuthorizer(taxonomy.NewCategoryOperator(categoryStore, queue))

	topicStore, err := taxonomy.NewTopicSqlStore(db, migrationService)
	if err != nil {
		return nil, fmt.Errorf("categoryStore: %s", err)
	}
	topicService := taxonomy.NewTopicAuthorizer(taxonomy.NewTopicOperator(topicStore, queue))

	eventsearchService := eventsearch.NewService(
		searchService,
		eventService,
		groupService,
		placeService,
		repeatingEventService,
		categoryService,
		topicService,
	)
	queue.AddSubscriber(eventsearchService.ConsumeOperation, 1)

	indexService := indexo.NewService(
		queue,
		searchService,
		eventService,
		groupService,
		placeService,
	)

	killswitchStore := killswitch.NewFileStore("killswitch", 5*time.Second)
	defer killswitchStore.Stop()
	killswitchService := killswitch.NewService(killswitchStore, conf.KillSwitchHashes)

	if conf.AdminEmail != "" && conf.AdminPassword != "" {
		err = user.CreateAdminAccount(userService, conf.AdminEmail, conf.AdminPassword)
		if err != nil {
			return nil, fmt.Errorf("failed to create admin account: %s", err)
		}
	}

	rssService := rss.NewService(eventsearchService, groupService, placeService, conf.BaseURL)

	icalService := ical.NewService(eventsearchService, conf.BaseURL)

	reindex := func(ctx context.Context) error {
		err := indexService.Reindex(ctx)
		if err != nil {
			return err
		}
		err = eventsearchService.Reindex(ctx)
		if err != nil {
			return err
		}
		return nil
	}

	actionService.Register("Reindex Search", reindex, "")

	scheduler.Start()

	go func() {
		doctorEventsRemoveEmptyOrganizers(eventStore)
		doctorEventsSetOrganzierAndLocation(eventStore, groupStore, placeStore)
		doctorRepeatingEventsSetOrganzierAndLocation(repeatingEventService, groupStore, placeStore)
		reindex(context.Background())
	}()

	var plausibleClient *plausible.Client
	if conf.PlausibleHost != "" && conf.PlausibleDomain != "" {
		plausibleClient, err = plausible.NewClient(conf.PlausibleHost, conf.PlausibleDomain)
		if err != nil {
			return nil, err
		}
	}

	renderer := web.NewRenderer(searchService, eventsearchService, categoryService, topicService, cmsService, conf.BaseURL, tz)

	r := chi.NewRouter()
	r.Use(middleware.StripSlashes)
	r.Use(xhttp.RequestIDMiddleware)
	r.Use(xhttp.LogMiddleware)

	if conf.Debug {
		r.Mount("/debug", middleware.Profiler())
	}

	osmService := osm.NewService(conf.OSMTileCacheDir, conf.OSMTileServer)
	actionService.Register("Clear OSM Cache", osmService.ClearCache, "* * 1 * *")

	r.Group(api.Routes(
		actionService,
		authService,
		categoryService,
		eventService,
		eventsearchService,
		groupService,
		invitationService,
		killswitchService,
		mediaService,
		notificationService.SettingsStore(),
		notificationService.Store(),
		passwordrecService,
		placeService,
		repeatingEventService,
		searchService,
		topicService,
		userService,
		osmService,
		conf.MediaPath,
		cmsAuthService,
	))
	r.Group(rss.Routes(rssService, plausibleClient))
	r.Group(ical.Routes(icalService, plausibleClient))
	r.Group(web.Routes(renderer, plausibleClient))
	r.Group(backstage.Routes(renderer, conf.BackstageAssetPath, plausibleClient))

	return &Server{
		s: &http.Server{
			Addr:         conf.Addr,
			ReadTimeout:  conf.HTTPReadTimeout,
			WriteTimeout: conf.HTTPWriteTimeout,
			Handler:      http.TimeoutHandler(r, conf.HTTPHandlerTimeout, ""),
		},

		searchService: searchService,
		scheduler:     scheduler,
	}, nil
}

// ListenAndServe starts the ListenAndServe method of the internal server.
func (s *Server) ListenAndServe() error {
	log.Printf("ListenAndServe on %s\n", s.s.Addr)
	return s.s.ListenAndServe()
}

// Close stops the server from running.
func (s *Server) Close() error {
	s.searchService.Stop()
	s.scheduler.Shutdown()
	return nil
}
