<!--
SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>

SPDX-License-Identifier: CC0-1.0
-->

# Eintopf

Eintopf is a calendar where one can publish events, groups and places. It is
deployed at [eintopf.info](https://eintopf.info)

## Deployment

See [DEPLOYMENT.md](./DEPLOYMENT.md)

## Contributing

See [CONTRIBUTING](./CONTRIBUTING.md)


## License

Eintopf is licensed under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE. See
[LICENSE](./LICENSES/AGPL-3.0-only.txt) for more info.
