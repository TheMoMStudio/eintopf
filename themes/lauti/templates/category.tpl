{{ define "head" }}
    <link rel="alternate" type="application/rss+xml" title="{{ markdownMeta "utility/general" "events"}} | {{ markdownMeta "utility/general" "feed_rss"}}" href="/rss">
    <link rel="alternate" type="text/calendar" title="{{ markdownMeta "utility/general" "events"}} | {{ markdownMeta "utility/general" "feed_ical"}}" href="/ical">

    <link rel="stylesheet" href="/assets/css/eventlist.css">
    <script src="/assets/js/eventlist.js"></script>
{{ end }}

{{ define "beforeHeader" }}{{ end }}

{{ define "beforeBurgerMenu" }}
    <form method="GET" action="/" class="search">
        <input placeholder="{{ markdownMeta "utility/general" "search"}}" name="query" value="{{ .Options.Query }}" />
        <button aria-label="{{ markdownMeta "utility/general" "search"}}" type="submit">
            <img src="/assets/images/icon_search.svg" alt="{{ markdownMeta "utility/images" "icon_search"}}" />
        </button>
    </form>
{{ end }}

{{ define "main" }}
    <div class="main invert-order-mobile">
        <div class="left">
            {{ template "left" . }}
        </div>
        <aside class="right">
            {{ template "right" . }}
        </aside>
    </div>
{{ end }}

{{ define "left" }}
    <div class="invert-order-mobile">
        {{ if not .Days }}
            <section class="container">
                <div class="container-header caps">
                    <h2>{{ markdownMeta "utility/general" "events"}}</h2>
                </div>
                <div class="container-content">
                    {{ if .Options.Date.IsZero }}
                        {{ markdownMeta "utility/messages" "no_event"}}
                    {{ else }}
                        {{ markdownMeta "utility/messages" "no_event_on_date"}}
                    {{ end }}
                </div>
            </section>
        {{ end }}
        {{ template "eventlistDays" dict "Days" .Days "Options" $.Options }}
        <div
            class="pagination"
            {{ if .Options.HasNextPage }}
                data-fetch-url="{{ joinPath .BaseURL "partial/eventlist" }}{{ .Options.URL }}"
                data-pages="{{ json .Options.Pages }}"
            {{ end }}
        >
            <div class="pagination-error hidden"></div>
            <div class="pagination-btn-container">
                {{ if .Options.HasPrevPage }}
                    <a class="prev" href="{{ (.Options.PrevPage).URL }}" title="{{ markdownMeta "utility/navigation" "previous_page"}}"><img src="/assets/images/icon_arrow_right.svg" alt="{{ markdownMeta "utility/images" "icon_arrow_left"}}" />{{ markdownMeta "utility/navigation" "previous_page"}}</a>
                {{ else }}
                    <span></span>
                {{ end }}
                {{ if .Options.HasNextPage }}
                    <a href="{{ (.Options.NextPage).URL }}" title="{{ markdownMeta "utility/navigation" "next_page"}}">{{ markdownMeta "utility/navigation" "next_page"}}<img src="/assets/images/icon_arrow_right.svg" alt="{{ markdownMeta "utility/images" "icon_arrow_right"}}" /></a>
                {{ else }}
                    <span></span>
                {{ end }}
            </div>
        </div>
        <div class="loading-box hidden">
            <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
        </div>
    </div>
{{ end }}

{{ define "right" }}
    <section class="container invert-order-mobile ">
        <div class="container-header caps">
            <div></div>
            <h1>{{ .Category.Headline }}</h1>
            <div></div>
        </div>
        <div class="container-content">
            {{ .Category.Description }}
        </div>
    </section>
{{ end }}
