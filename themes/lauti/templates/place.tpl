{{ define "head" }}
    <link rel="stylesheet" href="/assets/css/detail.css">
    <link rel="stylesheet" href="/assets/css/eventlist.css">

    {{ if placeShouldRenderMap .Place }}
        {{ template "mapIncludes" }}
    {{ end }}
{{ end }}

{{ define "beforeHeader" }}{{ end }}

{{ define "searchInput" }}{{ end }}

{{ define "beforeBurgerMenu" }}<h1 class="title">{{ .Place.Name }}</h1>{{ end }}

{{ define "main" }}
    <div class="main" itemscope itemtype="https://schema.org/Place">
        <div class="left {{ .PageClass }}">
            {{ template "left" . }}
        </div>
        <aside class="right">
            {{ template "right" . }}
        </aside>
    </div>
{{ end }}

{{ define "left" }}
    <section class="container detail group">
        <div class="container-header caps">
            <h1 class="p-name" itemprop="name">{{ .Place.Name }}</h1>
        </div>
        <div class="container-content">
            <div class="infos">
                <div>
                    {{ with .Place.Address }}
                        <div class="info info-address">
                            <span class="info-label">{{ markdownMeta "utility/general" "address"}}:</span>
                            <span class="info-data" itemprop="address">
                                {{- . -}}
                            </span>
                        </div>
                    {{ end }}
                    {{ with .Place.Link }}
                        <div class="info info-website">
                            <span class="info-label">{{ markdownMeta "utility/general" "website"}}:</span>
                            <span class="info-data">
                                <a href="{{ . }}" itemprop="url">
                                    {{ . }}
                                </a>
                            </span>
                        </div>
                    {{ end }}
                    {{ with .Place.Email }}
                        <div class="info info-email">
                            <span class="info-label">{{ markdownMeta "utility/general" "email"}}:</span>
                            <span class="info-data">
                                <a href="mailto:{{ . }}" itemprop="email">
                                    {{ attify . }}
                                </a>
                            </span>
                        </div>
                    {{ end }}
                </div>
                <div class="action-container">
                    <a class="link-rss" href="{{ joinPath "/places" .Place.ID "rss" }}" download="" rel="noreferrer" target="_blank">
                        {{ markdownMeta "utility/general" "feed_rss"}}
                    </a>
                    <a class="link-ical" href="{{ joinPath "/places" .Place.ID "ical" }}" download="" rel="noreferrer" target="_blank">
                        {{ markdownMeta "utility/general" "add_calendar"}}
                    </a>
                    <button class="share-btn hidden" title="{{ markdownMeta "utility/general" "share"}}" aria-label="{{ markdownMeta "utility/general" "share"}}" share-name="{{ .Place.Name }}" share-description="{{ .Place.Description }}">
                        <img src="/assets/images/icon_share.svg" alt="{{ markdownMeta "utility/images" "icon_share"}}"/>
                        {{ markdownMeta "utility/general" "share"}}
                    </button>
                </div>
            </div>

            {{ if .Place.Image }}
                <img class="image" src="/api/v1/media/{{ .Place.Image }}" itemprop="image" alt="{{ .Place.Alt }}">
            {{ end }}
            {{ with .Place.Description }}
                <div class="description" itemprop="description">{{ range split  . "\n" }}<p>{{ .  | linkify }}</p>{{ end }}</div>
            {{ end }}
            <div class="clear"></div>
            {{ template "foreignContent" }}
        </div>
    </section>

    {{ if .FutureEvents }}
        <section class="container container-eventlist-small future-events">
            <div class="container-header">
                {{ markdownMeta "utility/general" "future_event"}}
            </div>
            <div class="container-content eventlist">
                {{ range .FutureEvents }}
                    <div class="event" >
                        {{ template "eventlistEventInner" dict "Event" . "Options" $.Options "OmitPlace" true "TimeAsDate" true }}
                    </div>
                {{ end }}
            </div>
        </section>
    {{ end }}

    {{ if .PastEvents }}
        <section class="container container-eventlist-small past-events">
            <div class="container-header">
                {{ markdownMeta "utility/general" "past_event"}}
            </div>
            <div class="container-content eventlist">
                {{ range .PastEvents }}
                    <div class="event" >
                        {{ template "eventlistEventInner" dict "Event" . "Options" $.Options "OmitPlace" true "TimeAsDate" true }}
                    </div>
                {{ end }}
            </div>
        </section>
    {{ end }}
{{ end }}

{{ define "right" }}
    {{ if placeShouldRenderMap .Place }}
        <section class="container">
            <div class="container-header caps">
                {{ markdownMeta "utility/general" "map"}}
                {{ if and .Place.Lat .Place.Lng }}
                    <span class="info-data" itemprop="geo" itemscope itemtype="https://schema.org/GeoCoordinates">
                        (<span itemprop="latitude">{{ .Place.Lat }}</span>
                        |
                        <span itemprop="longitude">{{ .Place.Lng }}</span>)
                    </span>
                {{ end }}
            </div>
            <div
                id="map"
                class="container-content"
                data-markers='{{ placeDocumentToMapMarker .Place }}'
            ></div>
        </section>
    {{ end }}
{{ end }}
