{{ define "head" }}
    <link rel="alternate" type="text/calendar" title="{{ .Event.Name }} ical" href="{{ joinPath "/events" .Event.ID "ical"}}">

    <link rel="stylesheet" href="/assets/css/detail.css">
    <link rel="stylesheet" href="/assets/css/eventlist.css">

    {{ if eventShouldRenderMap .Event }}
        {{ template "mapIncludes" }}
    {{ end }}
{{ end }}

{{ define "beforeHeader" }}{{ end }}

{{ define "searchInput" }}{{ end }}
{{ define "beforeBurgerMenu" }}<h1 class="title">{{ .Event.Name }}</h1>{{ end }}

{{ define "main" }}
    <div class="main" itemscope itemtype="https://schema.org/Event">
        <div class="left {{ .PageClass }}">
            {{ template "left" . }}
        </div>
        <aside class="right">
            {{ template "right" . }}
        </aside>
    </div>
{{ end }}

{{ define "left" }}
    <section class="container detail event h-event">
        <div class="container-header caps">
            <h1 class="p-name" itemprop="name">{{ .Event.Name }}</h1>
        </div>
        <div class="container-content">
            <div class="infos">
                <div>
                    {{ with .Event.RepeatingEvent }}
                        <div class="info info-interval">
                            {{ range .Intervals }}
                                <i>{{ formatInterval . }}</i>
                            {{ end }}
                        </div>
                    {{ end }}
                    <div class="info info-date">
                        <span class="info-label">{{ markdownMeta "utility/general" "when"}}:</span>
                        <span class="info-data">
                            {{ if .Event.Canceled }}
                                <span class="canceled-info">{{ markdownMeta "utility/general" "canceled"}}</span>
                            {{ end }}
                            <time class="dt-start{{ if .Event.Canceled }} canceled{{ end }}" itemprop="startDate" datetime="{{ .Event.Start.Format "2006-01-02T15:04:05Z07:00"}}">
                                {{  weekdayGermanShort .Event.Start.Weekday }} {{ .Event.Start.Format "02.01.2006 15:04" }} {{ markdownMeta "utility/general" "clock"}}
                            </time>
                            {{ with .Event.End }}
                                <span> - </span>
                                <time class="dt-end" itemprop="endDate" datetime="{{ .Format "2006-01-02T15:04:05Z07:00"}}">
                                    {{ if sameDay $.Event.Start . }}
                                        {{ .Format "15:04" }}
                                    {{ else }}
                                        {{ weekdayGermanShort .Weekday }} {{ .Format "02.01.2006 15:04" }}
                                    {{ end }} {{ markdownMeta "utility/general" "clock"}}
                                </time>
                            {{ end }}
                        </span>
                    </div>
                    {{ with .Event.Location }}
                        {{ template "eventInfoLocation" dict "Location" . "Location2" $.Event.Location2 }}
                    {{ end }}
                    {{ with .Event.Organizers }}
                        {{ template "eventInfoOrganizers" . }}
                    {{ end }}
                    {{ with .Event }}
                        {{ template "eventInfoCategory" dict "Event" . }}
                    {{ end }}
                    {{ with .Event.Tags }}
                        {{ template "eventInfoTags" dict "Tags" . "Options" $.Options }}
                    {{ end }}
                </div>
                <div class="action-container">
                    <a class="link-ical" title="{{ markdownMeta "utility/general" "add_calendar"}}" href="{{ joinPath "/events" .Event.ID "ical" }}" download="" rel="noreferrer" target="_blank">
                        {{ markdownMeta "utility/general" "add_calendar"}}
                    </a>
                    <button class="share-btn hidden" title="{{ markdownMeta "utility/general" "share"}}" aria-label="{{ markdownMeta "utility/general" "share"}}" share-name="{{ .Event.Name }}" share-description="{{ .Event.Description }}">
                        <img src="/assets/images/icon_share.svg" alt="{{ markdownMeta "utility/images" "icon_share"}}"/>
                        {{ markdownMeta "utility/general" "share"}}
                    </button>
                </div>
            </div>
            {{ if .Event.Image }}
                <img class="image" src="/api/v1/media/{{ .Event.Image }}" itemprop="image" alt="{{ .Event.Alt }}">
            {{ end }}
            {{ with .Event.Description }}
                <div class="description" itemprop="description">{{ range split  . "\n" }}<p>{{ . | linkify}}</p>{{ end }}</div>
            {{ end }}
            {{ with .Event.Involved }}
                {{ template "eventInfoInvolvedWithDescription" . }}
            {{ end }}
            <div class="clear"></div>
            {{ template "foreignContent" }}
        </div>
    </section>

    {{ range $day, $events := .Children }}
        <section class="container">
            <div class="container-header caps">
                <h2>{{ markdownMeta "utility/general" "program"}} {{ formatDate $day "02. Jan / Monday" }}</h2>
            </div>
            <div class="container-content eventlist">
                {{ range $events }}
                    <div class="event" itemprop="subEvent" itemscope itemtype="https://schema.org/Event">
                        {{ template "eventlistEventInner" dict "Event" . "Options" $.Options }}
                    </div>
                {{ end }}
            </div>
        </section>
    {{ end }}

    {{ with .FutureEvents }}
        {{ range $organizer, $events := .}}
            <section class="container container-eventlist-small future-events">
                <div class="container-header">
                    {{ markdownMeta "utility/general" "future_event_by"}} <span class="name">{{ $organizer }}</span>
                </div>
                <div class="container-content eventlist">
                    {{ range $events }}
                        <div class="event" >
                            {{ template "eventlistEventInner" dict "Event" . "Options" $.Options "OmitOrganizers" true "TimeAsDate" true }}
                        </div>
                    {{ end }}
                </div>
            </section>
        {{ end }}
    {{ end }}

    {{ with .PastEvents }}
        {{ range $organizer, $events := .}}
            <section class="container container-eventlist-small past-events">
                <div class="container-header">
                    {{ markdownMeta "utility/general" "past_event_by"}} <span class="name">{{ $organizer }}</span>
                </div>
                <div class="container-content eventlist">
                    {{ range $events }}
                        <div class="event" >
                            {{ template "eventlistEventInner" dict "Event" . "Options" $.Options "OmitOrganizers" true "TimeAsDate" true }}
                        </div>
                    {{ end }}
                </div>
            </section>
        {{ end }}
    {{ end }}
{{ end }}

{{ define "right" }}
    {{ if eventShouldRenderMap .Event }}
        <section class="container">
            <div class="container-header caps">
                <span>{{ markdownMeta "utility/general" "map"}}</span>
                {{ $coords := getEventCoordinates .Event }}
                {{ if and (ne $coords.Lat 0.0) (ne $coords.Lng 0.0) }}
                    <span class="info-data" itemprop="geo" itemscope itemtype="https://schema.org/GeoCoordinates">
                        (<span itemprop="latitude">{{ $coords.Lat }}</span> | <span itemprop="longitude">{{ $coords.Lng }}</span>)
                    </span>
                {{ end }}
            </div>
            <div id="map" class="container-content" data-markers='{{ eventToMapMarker .Event }}'>
            </div>
        </section>
    {{ end }}
{{ end }}

{{ define "eventInfoInvolvedWithDescription" }}
    <div>
        {{- range $i, $involved := . -}}
            <div itemprop="contributor" itemscope itemtype="https://schema.org/Person">
                <div class="info info-involved">
                    <span class="info-label">Mit:</span>
                    <span class="info-data" itemprop="name">
                        {{- $involved.Name -}}
                    </span>
                </div>
                <div class="description" itemprop="description">
                    {{- $involved.Description -}}
                </div>
            </div>
        {{- end -}}
    <div>
{{ end }}
