{{ define "beforeHeader" }}{{ end }}
{{ define "head" }}{{ end }}
{{ define "beforeBurgerMenu" }}{{ end }}
{{ define "sitename" }}Eintopf Stuttgart{{ end }}
{{ define "logoAlt" }}eintopf.info Veranstaltungen Stuttgart{{ end }}
{{ define "defaultMetaImage" }}{{ joinPath .BaseURL "/assets/images/logo.png"}}{{ end }}
{{ define "defaultMetaTitle" }}Veranstaltungen in Stuttgart - emanzipatorische Politik und Kultur{{ end }}
{{ define "defaultMetaDescription" }}Eintopf Stuttgart ist ein Veranstaltungskalender für emanzipatorische Politik und Kultur in Stuttgart und Umgebung. Hier findet ihr Veranstaltungen, Gruppen und Orte.{{ end }}
{{ define "title" }}{{ if .Meta.Title }}{{ .Meta.Title }}{{ else }}{{ template "defaultMetaTitle" }}{{ end }} | {{ template "sitename" }}{{ end }}
{{ define "description" }}{{ if .Meta.Description }}{{ .Meta.Description }}{{ else }}{{ template "defaultMetaDescription" }}{{ end }}{{ end }}
{{ define "metaImage" }}{{ if .Meta.Image }}{{ joinPath .BaseURL "/api/v1/media/" .Meta.Image }}{{ else }}{{ template "defaultMetaImage" . }}{{ end }}{{ end }}
{{ define "mapIncludes" }}
    <link rel="stylesheet" href="/assets/dist/leaflet/leaflet_1_9_2.css">
    <script src="/assets/dist/leaflet/leaflet_1_9_2.js"></script>
    <script type="module" src="/assets/js/map.js"></script>
{{ end }}
{{ define "foreignContent" }}<div class="foreign-content">Die Inhalte dieser Seite wurden nicht von den Betreiben dieser Website erstellt. Solltest du einen Rechtsverstoß bemerken, melde diesen bitte per Mail an mod [at] eintopf.info</div>{{ end }}
{{ define "robots" }}{{ if .Meta.NotPublished }}noindex, nofollow{{ else }}index, follow{{ end }}{{ end }}