{{ define "head" }}
    <script type="module" crossorigin src="/backstage/index.js"></script>
{{ end }}
{{ define "main" }}
    <x-backstage apiurl="{{ .ApiURL }}" timezone="{{ .Timezone }}"></x-backstage>
{{ end }}
