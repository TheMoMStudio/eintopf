{{ define "head" }}
    <link rel="stylesheet" href="/assets/css/detail.css">
    <link rel="stylesheet" href="/assets/css/eventlist.css">
{{ end }}

{{ define "beforeHeader" }}{{ end }}

{{ define "searchInput" }}{{ end }}

{{ define "beforeBurgerMenu" }}<h2 class="title">{{ .Group.Name }}</h2>{{ end }}

{{ define "main" }}
    <div class="main" itemscope itemtype="https://schema.org/Organization">
        <div class="left {{ .PageClass }}">
            {{ template "left" . }}
        </div>
        <aside class="right">
            {{ template "right" . }}
        </aside>
    </div>
{{ end }}

{{ define "left" }}
    <section class="container detail group">
        <div class="container-header caps">
            <h2 class="p-name" itemprop="name">{{ .Group.Name }}</h2>
        </div>
        <div class="container-content">
            <div class="infos">
                <div>
                    {{ with .Group.Link }}
                        <div class="info info-website">
                            <span class="info-label">{{ markdownMeta "utility/general" "website"}}:</span>
                            <span class="info-data">
                                <a href="{{ . }}" itemprop="url">
                                    {{ . }}
                                </a>
                            </span>
                        </div>
                    {{ end }}
                    {{ with .Group.Email }}
                        <div class="info info-email">
                            <span class="info-label">{{ markdownMeta "utility/general" "email"}}:</span>
                            <span class="info-data">
                                <a href="mailto:{{ . }}" itemprop="email">
                                    {{ attify . }}
                                </a>
                            </span>
                        </div>
                    {{ end }}
                </div>
                <div class="action-container">
                    <a class="link-rss" href="{{ joinPath "/groups" .Group.ID "rss" }}" download="" rel="noreferrer" target="_blank">
                        {{ markdownMeta "utility/general" "feed_rss"}}
                    </a>
                    <a class="link-ical" href="{{ joinPath "/groups" .Group.ID "ical" }}" download="" rel="noreferrer" target="_blank">
                        {{ markdownMeta "utility/general" "add_calendar"}}
                    </a>
                    <button class="share-btn hidden" title="{{ markdownMeta "utility/general" "share"}}" aria-label="{{ markdownMeta "utility/general" "share"}}" share-name="{{ .Group.Name }}" share-description="{{ .Group.Description }}">
                        <img src="/assets/images/icon_share.svg" alt="Ein Share-Icon bestehend aus 3 verbundenen Punkten" />
                        {{ markdownMeta "utility/general" "share"}}
                    </button>
                </div>
            </div>
	    {{ if .Group.Image }}
                <img class="image" src="/api/v1/media/{{ .Group.Image }}" itemprop="image" alt="{{ .Group.Alt }}" >
            {{ end }}
            {{ with .Group.Description }}
                <div class="description" itemprop="description">{{ range split  . "\n" }}<p>{{ .  | linkify }}</p>{{ end }}</div>
            {{ end }}
            <div class="clear"></div>
            {{ template "foreignContent" }}
        </div>
    </section>

    {{ if .FutureEvents }}
        <section class="container container-eventlist-small future-events">
            <div class="container-header">
                {{ markdownMeta "utility/general" "future_event"}}
            </div>
            <div class="container-content eventlist">
                {{ range .FutureEvents }}
                    <div class="event" >
                        {{ template "eventlistEventInner" dict "Event" . "Options" $.Options "OmitOrganizers" true "TimeAsDate" true }}
                    </div>
                {{ end }}
            </div>
        </section>
    {{ end }}

    {{ if .PastEvents }}
        <section class="container container-eventlist-small past-events">
            <div class="container-header">
                {{ markdownMeta "utility/general" "past_event"}}
            </div>
            <div class="container-content eventlist">
                {{ range .PastEvents }}
                    <div class="event" >
                        {{ template "eventlistEventInner" dict "Event" . "Options" $.Options "OmitOrganizers" true "TimeAsDate" true }}
                    </div>
                {{ end }}
            </div>
        </section>
    {{ end }}
{{ end }}

{{ define "right" }}
{{ end }}
