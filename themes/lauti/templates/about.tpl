{{ define "head" }}
    <link rel="stylesheet" href="/assets/css/about.css">
{{ end }}

{{ define "beforeHeader" }}{{ end }}
{{ define "searchInput" }}{{ end }}
{{ define "beforeBurgerMenu" }}<h2 class="title">{{ markdownMeta "about/us" "title"}}</h2>{{ end }}

{{ define "main" }}
    <div class="main">
        <div class="left {{ .PageClass }}">
            <section class="container first-container">
                <div class="container-header caps ">
                    <h2>{{ markdownMeta "about/us" "title"}}</h2>
                </div>
                <div class="container-content">
		  {{ markdown "about/us" }}
		</div>
            </section>

            <section class="container">
                <div class="container-header caps">
                    <h2>{{ markdownMeta "about/faq" "title"}}</h2>
                </div>
                <div class="container-content">
		    {{ markdown "about/faq" }}
                </div>
            </section>

            <section class="container">
                <div class="container-header caps">
                    <h2>{{ markdownMeta "about/download" "title"}}</h2>
                </div>
                <div class="container-content">
		  {{ markdown "about/download" }}
		</div>
            </section>

            <section class="container">
                <div class="container-header caps">
                    <h2>{{ markdownMeta "about/contact" "title"}}</h2>
                </div>
                <div class="container-content">
		  {{ markdown "about/contact"}}
		</div>
            </section>

            <section class="container">
                <div class="container-header caps">
                    <h2>Software</h2>
                </div>
                <div class="container-content">
                    <p>
                        <a href="https://codeberg.org/Klasse-Methode/eintopf">LAUTI</a> maintained von <a href="https://klasse-methode.it">Klasse & Methode - IT Kollektiv Stuttgart</a></br>
                        Version: {{ .VersionNumber }}</br>
                        Commit: {{ .CommitHash }}</br>
                    </p>
		        </div>
            </section>
        </div>

        <aside class="right">
            <section class="container">
                <div class="container-header caps">
                    <h2>{{ markdownMeta "about/backstage" "title"}}</h2>
                </div>
                <div class="container-content">
		    {{ markdown "about/backstage" }}
                </div>
            </section>

            <section class="container other-calendar">
                <div class="container-header caps">
                    <h2>{{ markdownMeta "about/local_calendar" "title"}}</h2>
                </div>
                <div class="container-content">
		  {{ markdown "about/local_calendar"}}
		</div>
            </section>

            <section class="container other-calendar">
                <div class="container-header caps">
                    <h2>{{ markdownMeta "about/calendar" "title"}}</h2>
                </div>
                <div class="container-content">
		  {{ markdown "about/calendar"}}
		</div>
            </section>
        </aside>
    </div>
{{ end }}
