{{ define "eventInfoLocation" }}
    <div class="info info-location">
        <span class="info-label">{{ markdownMeta "utility/general" "where"}}:</span>
        <span class="info-data p-location" itemprop="location" itemscope itemtype="https://schema.org/Place">
            {{- if .Location.Place -}}
                <a href="/ort/{{ .Location.Place.ID }}" itemprop="url"><span itemprop="name">{{ .Location.Place.Name }}</span></a>
            {{- else -}}
                <span itemprop="name">{{ .Location.Name }}</span>
                {{- if .Location.Address -}}
                    , <span itemprop="address">{{ .Location.Address }}</span>
                {{- end -}}
            {{- end -}}
            {{- if .Location2 -}}
                <span>, {{ .Location2 }}</span>
            {{- end -}}
        </span>
    </div>
{{ end }}

{{ define "eventInfoOrganizers" }}
    <div class="info info-organizers">
        <span class="info-label">{{ markdownMeta "utility/general" "who"}}:</span>
        <span class="info-data">
            {{ range $i, $o := .}}
                {{if $i}}, {{end}}
                    {{ if $o.Group }}
                        <span class="p-organizer" itemprop="organizer" itemscope itemtype="https://schema.org/Organization">
                            <a href="/gruppe/{{ $o.Group.ID }}" itemprop="url"><span itemprop="name">{{ $o.Group.Name }}</span></a>
                        </span>
                    {{ else }}
                        <span class="p-organizer" itemprop="organizer">{{ $o.Name }}</span>
                    {{ end }}
            {{ end }}
        </span>
    </div>
{{ end }}

{{ define "eventInfoInvolved" }}
    <div class="info info-involved">
        <span class="info-label">{{ markdownMeta "utility/general" "with"}}:</span>
        <span class="info-data">
            {{- range $i, $involved := . -}}
                {{- if $i -}}, {{ end -}}
                <span itemprop="contributor">{{ $involved.Name }}</span>
            {{- end -}}
        </span>
    </div>
{{ end }}

{{ define "eventInfoTags" }}
    <div class="info info-tags">
        <span class="info-label">{{ markdownMeta "utility/general" "tags"}}:</span>
        <span>
            {{ range $i, $t := .Tags }}
                {{if $i}} {{end}}
                <a href="{{ ($.Options.SetTag $t).URL }}" itemprop="keywords">#{{ $t }}</a>
            {{ end }}
        </span>
    </div>
{{ end }}

{{ define "eventInfoCategory" }}
    <div class="info info-tags">
    {{ if or .Event.Category .Event.Topic }}
        <span class="info-label">{{ markdownMeta "utility/general" "what"}}:</span>
        {{ with .Event.Category }}
            <span class="info-data p-organizer">
                <a href="/kategorie/{{ .ID }}" itemprop="keywords">{{ .Name }}</a>
            </span>
        {{ end }}
        {{ if and .Event.Category .Event.Topic }}
            <span>, </span>
        {{ end }}
        {{ with .Event.Topic }}
            <span class="p-organizer">
                <a href="/thema/{{ .ID }}" itemprop="keywords">{{ .Name }}</a>
            </span>
        {{ end }}
    {{ end }}
    </div>
{{ end }}

{{ define "eventlistEventInner" }}
    <div class="event-time{{ if .Event.Canceled }} canceled{{ end }}">
        <meta itemprop="startDate" content="{{ .Event.Start.Format "2006-01-02T15:04:05Z07:00"}}">
        {{ if .Event.End }}<meta itemprop="endDate" content="{{ .Event.End.Format "2006-01-02T15:04:05Z07:00"}}">{{ end }}
        {{- if eq .Event.MultiDay -2 -}}
            <span>{{ formatDate .Event.Start "02.01.06" }}</span>
            <span class="event-time-multiday-seperator">-</span>
            <span>{{ formatDate .Event.End "02.01.06" }}</span>
        {{ else if ge .Event.MultiDay 0 }}
            <span class="event-time-multiday">{{ .Event.MultiDay }}.{{ markdownMeta "utility/time" "day"}}</span>
        {{ else if .TimeAsDate }}
            {{ formatDate .Event.Start "02.01.06" }}
        {{ else }}
            {{ formatDate .Event.Start "15:04" }}
        {{ end }}
    </div>
    <div class="event-content">
        <h3 class="title">
            <a class="u-url {{ if .Event.Canceled }} canceled{{ end }}" href="/event/{{ .Event.ID }}" itemprop="url">
                <span class="p-name" itemprop="name">{{ .Event.Name }}</span>
            </a>
            {{- if .Event.Canceled }} <span class="canceled-info">{{ markdownMeta "utility/general" "canceled"}}</span>{{ end -}}
        </h3>
        {{ with .Event.RepeatingEvent }}
            {{ range .Intervals }}
                <i>{{ formatInterval . }}</i>
            {{ end }}
        {{ end }}
        {{ with .Event.Location }}
            {{ template "eventInfoLocation" dict "Location" . }}
        {{ end }}
        {{ if not .OmitOrganizers }}
            {{ with .Event.Organizers }}
                {{ template "eventInfoOrganizers" . }}
            {{ end }}
        {{ end }}
        {{ with .Event.Involved }}
            {{ template "eventInfoInvolved" . }}
        {{ end }}
        {{ with .Event }}
            {{ template "eventInfoCategory" dict "Event" . }}
        {{ end }}
    </div>
{{ end }}

{{ define "eventlistDays" }}
    {{ range $day, $events := .Days }}
        <section class="container">
            <div class="container-header caps">
                <h2>
                    {{- formatDate $day "02. Jan / " -}}
                    {{- if isToday $day }}
                        {{ markdownMeta "utility/time" "today"}}
                    {{ else if isTomorrow $day }}
                        {{ markdownMeta "utility/time" "tomorrow"}}
                    {{- else -}}
                        {{ formatDate $day "Monday" }}
                    {{- end -}}
                </h2>
            </div>
            <div class="container-content eventlist">
                {{ range $events }}
                    <div class="event h-event" itemscope itemtype="https://schema.org/Event">
                        {{ template "eventlistEventInner" dict "Event" . "Options" $.Options }}
                        {{ if .Children }}
                            <div class="program">
                                <a class="program-link" href="/event/{{ .ID }}#program">{{ markdownMeta "utility/general" "program"}}</a>
                                <div class="program-content">
                                    {{ range .Children }}
                                        <div class="event" itemprop="subEvent" itemscope itemtype="https://schema.org/Event">
                                            {{ template "eventlistEventInner" dict "Event" . "Options" $.Options }}
                                        </div>
                                    {{ end }}
                                </div>
                            </div>
                        {{ end }}
                    </div>
                {{ end }}
            </div>
        </section>
    {{ end }}
{{ end }}

{{ define "eventlist-calendar" }}
    <section class="container calendar-widget" id="{{ .ID }}">
        <div class="container-header caps">
            <div></div>
            <div class="calendar-title">
                <a class="calendar-action calendar-action-prev" href="{{ (.Options.PrevCalendar).URL }}#calendar" rel="noindex,nofollow" data-fetch-url="{{ joinPath .BaseURL "partial/calendar" }}{{ (.Options.PrevCalendar).URL }}">&lt;&lt;</a>
                <div>{{ formatDate .Options.Calendar "January 2006" }}</div>
                <a class="calendar-action calendar-action-next" href="{{ (.Options.NextCalendar).URL }}#calendar" rel="noindex,nofollow" data-fetch-url="{{ joinPath .BaseURL "partial/calendar" }}{{ (.Options.NextCalendar).URL }}">&gt;&gt;</a>
            </div>
            <div></div>
        </div>
        <div class="container-content calendar">
            <div>{{ markdownMeta "utility/time" "monday_short"}}</div>
            <div>{{ markdownMeta "utility/time" "tuesday_short"}}</div>
            <div>{{ markdownMeta "utility/time" "wednesday_short"}}</div>
            <div>{{ markdownMeta "utility/time" "thursday_short"}}</div>
            <div>{{ markdownMeta "utility/time" "friday_short"}}</div>
            <div>{{ markdownMeta "utility/time" "saturday_short"}}</div>
            <div>{{ markdownMeta "utility/time" "sunday_short"}}</div>
            {{ range .Calendar }}
                {{ if .InMonth }}
                    <a
                        class="{{ with .HasEvents }}has-events{{ end }}{{ with sameDay .Date $.Today }} today{{ end }}{{ with sameDay .Date $.Options.Date }} current{{ end }}"
                        href="{{ ($.Options.ToggleDate .Date).URL }}"
                        rel="noindex,nofollow"
                    >{{ .Date.Day }}</a>
                {{ else }}
                    <div class="not-in-month">{{ .Date.Day }}</div>
                {{ end }}
            {{ end }}
        </div>
    </section>
{{ end }}
