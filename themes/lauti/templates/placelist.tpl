{{ define "head" }}
    <link rel="stylesheet" href="/assets/css/placelist.css">
    <link rel="stylesheet" href="/assets/css/list.css">
    <script type="module" src="/assets/js/placelist.js"></script>

    {{ template "mapIncludes" }}
{{ end }}

{{ define "beforeBurgerMenu" }}
    <form method="GET" action="/orte" class="search">
        <input placeholder="{{ markdownMeta "utility/general" "search"}}" name="query" value="{{ .Query }}" />
        <button aria-label="Suche" type="submit">
            <img src="/assets/images/icon_search.svg" alt="{{ markdownMeta "utility/images" "icon_search"}}" />
        </button>
    </form>
{{ end }}

{{ define "main" }}
    <div class="main invert-order-mobile">
        <div class="left">
            {{ template "left" . }}
        </div>
        <aside class="right">
            {{ template "right" . }}
        </aside>
    </div>
{{ end }}

{{ define "left" }}
    <section class="container map">
        <div class="container-header caps">
            {{ markdownMeta "utility/general" "map"}}
	</div>
        {{ $len := minus (len .Places) 1 }}
        <div
            id="map"
            class="container-content"
            data-markers='{{ placesToMapMarkers .Places }}'
        ></div>
    </section>

    <div class="list">
        {{ range .Places }}
            <section class="container" itemscope itemtype="https://schema.org/Place">
                <a href="/ort/{{ .ID }}" {{ if isCurrentLetter .StartLetterRune }}id="{{ .StartLetter }}"{{ end }} itemprop="url">
                    <div class="container-header caps">
                        <div></div><h2 itemprop="name">{{ .Name }}</h2><div></div>
                    </div>
                    <div class="container-content">
                        {{ if .Image }}
                            <img src="/api/v1/media/{{ .Image }}" alt="{{ .Alt }}" />
                        {{ else }}
                            <img src="/assets/images/placeholder.png" alt="Bild" />
                        {{ end }}
                    </div>
                </a>
            </section>
        {{ end }}
    </div>

    <div class="tabs">
        <button class="active tab-list">{{ markdownMeta "utility/general" "list"}}</button>
        <button class="tab-map">{{ markdownMeta "utility/general" "map"}}</button>
    </div>
{{ end }}

{{ define "right" }}
    <section class="container">
        <div class="container-header caps">
            <h1>{{ markdownMeta "placelist/intro" "title"}}</h1>
        </div>
        <div class="container-content">
          {{ markdown "placelist/intro"}}
        </div>
    </section>
    <section class="container">
        <div class="alphabet">
            {{ range .Alphabet }}
                {{ if .HasItems }}
                    <a href="#{{ .Letter }}">{{ .Letter }}</a>
                {{ else }}
                    <span>{{ .Letter }}</span>
                {{ end }}
            {{ end }}
        </div>
    </section>
{{ end }}