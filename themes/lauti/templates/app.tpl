{{ define "head" }}
    <script src="/assets/js/install.js"></script>
{{ end }}

{{ define "beforeHeader" }}{{ end }}
{{ define "searchInput" }}{{ end }}
{{ define "beforeBurgerMenu" }}
  <h2 class="title">{{ markdownMeta "app/app" "title"}}</h2>
{{ end }}

{{ define "main" }}
    <div class="main">
        <div class="left {{ .PageClass }}">
            <section class="container first-container">
                <div class="container-header caps only-desktop">
                    <h2>{{ markdownMeta "app/app" "title"}}</h2>
                </div>
                <div class="container-content">
		  {{ markdown "app/app"}}
		</div>
            </section>
        </div>
    </div>
{{ end }}
