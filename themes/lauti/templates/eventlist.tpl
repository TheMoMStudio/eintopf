{{ define "head" }}
    <link rel="alternate" type="application/rss+xml" title="{{ markdownMeta "utility/general" "events"}} | {{ markdownMeta "utility/general" "feed_rss"}}" href="/rss">
    <link rel="alternate" type="text/calendar" title="{{ markdownMeta "utility/general" "events"}} | {{ markdownMeta "utility/general" "feed_ical"}}" href="/ical">

    <link rel="stylesheet" href="/assets/css/eventlist.css">
    <script src="/assets/js/eventlist.js"></script>

    <script>
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.register('/serviceworker.js', { scope: '/' });
        }
    </script>
{{ end }}
{{ define "robots" }}{{ if not .Options.ShouldIndex }}noindex, nofollow{{ end }}{{ end }}

{{ define "beforeHeader" }}{{ end }}

{{ define "beforeBurgerMenu" }}
    <form method="GET" action="/" class="search">
        {{ if .Options.Tags }}
            {{ range .Options.Tags }}
                <input type="hidden" name="tags" value="{{ . }}">
            {{ end }}
        {{ end }}
        {{ if not .Options.DateIsDefault }}
            <input type="hidden" name="date" value="{{ .Options.Calendar.Format "2006-01-02" }}">
        {{ end }}
        {{ if not .Options.CalendarIsDefault }}
            <input type="hidden" name="calendar" value="{{ .Options.Calendar.Format "2006-01" }}">
        {{ end }}
        <input placeholder="Suche" name="query" value="{{ .Options.Query }}" />
        <button aria-label="Suche" type="submit">
            <img src="/assets/images/icon_search.svg" alt="{{ markdownMeta "utility/images" "icon_search"}}" />
        </button>
    </form>
{{ end }}

{{ define "main" }}
    <div class="main invert-order-mobile">
        <div class="left">
            {{ template "left" . }}
        </div>
        <aside class="right">
            {{ template "right" . }}
        </aside>
    </div>
{{ end }}

{{ define "left" }}
    {{ if not .Days }}
        <section class="container">
            <div class="container-header caps">
                <h2>{{ markdownMeta "utility/general" "events"}}</h2>
            </div>
            <div class="container-content">
                {{ if .Options.Date.IsZero }}
                    {{ markdownMeta "utility/messages" "no_events"}}
                {{ else }}
                    {{ markdownMeta "utility/messages" "no_events_on_date"}}
                {{ end }}
            </div>
        </section>
    {{ end }}
    {{ template "eventlistDays" dict "Days" .Days "Options" $.Options }}
    <div
        class="pagination"
        {{ if .Options.HasNextPage }}
            data-fetch-url="{{ joinPath .BaseURL "partial/eventlist" }}{{ .Options.URL }}"
            data-pages="{{ json .Options.Pages }}"
        {{ end }}
    >
        <div class="pagination-error hidden"></div>
        <div class="pagination-btn-container">
            {{ if .Options.HasPrevPage }}
                <a class="prev" href="{{ (.Options.PrevPage).URL }}" title="{{ markdownMeta "utility/navigation" "previous_page"}}"><img src="/assets/images/icon_arrow_right.svg" alt="{{ markdownMeta "utility/images" "icon_arrow_left"}}" />{{ markdownMeta "utility/navigation" "previous_page"}}</a>
            {{ else }}
                <span></span>
            {{ end }}
            {{ if .Options.HasNextPage }}
                <a href="{{ (.Options.NextPage).URL }}" title="{{ markdownMeta "utility/navigation" "next_page"}}">{{ markdownMeta "utility/navigation" "next_page"}}<img src="/assets/images/icon_arrow_right.svg" alt="{{ markdownMeta "utility/images" "icon_arrow_right"}}" /></a>
            {{ else }}
                <span></span>
            {{ end }}
        </div>
    </div>
    <div class="loading-box hidden">
        <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
    </div>
{{ end }}

{{ define "right" }}
    {{ $activeFilters := len .Options.Tags }}
    <div class="tabs">
        <nav class="open">
            <a href="#calendar">{{ markdownMeta "utility/general" "calendar"}}</a>
            <a href="#filter">{{ markdownMeta "utility/general" "filter"}} {{ if gt $activeFilters 0 }}({{ $activeFilters }}){{ end }}</a>
            <a href="#feed">{{ markdownMeta "utility/general" "feed"}}</a>
        </nav>
        <div id="calendar">
            <nav>
                <a href="#" class="active">{{ markdownMeta "utility/general" "calendar"}}</a>
                <a href="#filter">{{ markdownMeta "utility/general" "filter"}} {{ if gt $activeFilters 0 }}({{ $activeFilters }}){{ end }}</a>
                <a href="#feed">{{ markdownMeta "utility/general" "feed"}}</a>
            </nav>
            {{ template "eventlist-calendar" dict "Options" .Options "Calendar" .Calendar "Today" .Today "BaseURL" .BaseURL}}
        </div>
        <div id="filter">
            <nav>
                <a href="#calendar">{{ markdownMeta "utility/general" "calendar"}}</a>
                <a href="#" class="active">{{ markdownMeta "utility/general" "filter"}} {{ if gt $activeFilters 0 }}({{ $activeFilters }}){{ end }}</a>
                <a href="#feed">{{ markdownMeta "utility/general" "feed"}}</a>
            </nav>
        {{ template "eventlist-filter" dict "Options" .Options "Tags" .Tags "Categories" .Categories "Topics" .Topics}}
        </div>
        <div id="feed">
            <nav>
                <a href="#calendar">{{ markdownMeta "utility/general" "calendar"}}</a>
                <a href="#filter">{{ markdownMeta "utility/general" "filter"}} {{ if gt $activeFilters 0 }}({{ $activeFilters }}){{ end }}</a>
                <a href="#" class="active">{{ markdownMeta "utility/general" "feed"}}</a>
            </nav>
            {{ template "eventlist-feed"}}
        </div>
    </div>
{{ end }}

{{ define "eventlist-categories" }}
    <section class="container">
        <div class="container-header caps">
            <div></div>
            <div>{{ markdownMeta "utility/general" "categories"}}</div>
            <div></div>
        </div>
        <div class="container-content filter">
            {{ range .Categories }}
                {{ $categoriesList := len .Options.Tags }}
                <a href="/kategorie/{{ .ID }}">{{ .Name }}</a>
            {{ end }}
        </div>
    </section>
{{ end }}

{{ define "eventlist-topics" }}
    <section class="container">
        <div class="container-header caps">
            <div></div>
            <div>{{ markdownMeta "utility/general" "categories"}}</div>
            <div></div>
        </div>
        <div class="container-content filter">
            {{ range .Topics }}
                <a href="/thema/{{ .ID }}">{{ .Name }}</a>
            {{ end }}
        </div>
    </section>
{{ end }}

{{ define "eventlist-filter" }}
    <section class="container" id="{{ .ID }}">
        <div class="container-header caps">
            <div></div>
            <div>
                <div>{{ markdownMeta "utility/general" "filter"}}</div>
            </div>
            <div></div>
        </div>
        <div class="container-content">
            <h2 class="container-subheader">{{ markdownMeta "utility/general" "categories"}}</h2>
            <div class="container-subcontent filter" id="categories">
                {{ range .Categories }}
                    <a href="/kategorie/{{ .ID }}">{{ .Name }}</a>
                {{ end }}
            </div>
            <h2 class="container-subheader">{{ markdownMeta "utility/general" "topics"}}</h2>
            <div class="container-subcontent filter" id="topics">
                {{ range .Topics }}
                    <a href="/thema/{{ .ID }}">{{ .Name }}</a>
                {{ end }}
            </div>
            <h2 class="container-subheader tags">Tags</h2>
            <div class="tags-list">
                <a class="tags-link" href="#tags">{{ markdownMeta "utility/general" "show"}}</a>
                <div class="container-subcontent filter" id="tags" >
                    {{ range .Tags }}
                        {{ $options := $.Options.ToggleTag .Term }}
                        <a class="{{ if $.Options.HasTag .Term }}active{{ end }}" href="{{ $options.URL }}" {{ if not $options.ShouldIndex }}rel="noindex, nofollow"{{ end }}>#{{ .Term }}</a>
                    {{ end }}
                </div>
            </div>
            <h2 class="container-subheader regular">{{ markdownMeta "utility/general" "repeating_events"}}</h2>
            <div class="regular-filter">
                <div class="container-subcontent filter" id="regular-filter-container">
                    {{ $optionsNoRepeating := $.Options.ToggleNoRepeatingFilter }}
                    <a class="{{ if $.Options.HasNoRepeatingFilter }}active{{ end }}" href="{{ $optionsNoRepeating.URL }}">{{ markdownMeta "utility/general" "only_one_time"}}</a>
                    {{ $optionsOnlyRepeating := $.Options.ToggleOnlyRepeatingFilter }}
                    <a class="{{ if $.Options.HasOnlyRepeatingFilter }}active{{ end }}" href="{{ $optionsOnlyRepeating.URL }}">{{ markdownMeta "utility/general" "only_repeating"}}</a>
                </div>
            </div>
        </div>
    </section>
{{ end }}

{{ define "eventlist-feed" }}
    <section class="container" id="{{ .ID }}">
        <div class="container-header caps">
            <div></div>
            <div>
                <div>{{ markdownMeta "utility/general" "feed"}}</div>
            </div>
            <div></div>
        </div>
        <div class="container-content feed">
            <a class="link-rss" href="/rss" title="rss feed" download target="_blank">
                {{ markdownMeta "utility/general" "feed_rss"}}
            </a>
            <a class="link-ical" href="/ical" title="ical feed" download target="_blank">
                {{ markdownMeta "utility/general" "add_calendar"}}
            </a>
        </div>
    </section>
{{ end }}
