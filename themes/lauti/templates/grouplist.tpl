{{ define "head" }}
    <link rel="stylesheet" href="/assets/css/list.css">
{{ end }}

{{ define "beforeBurgerMenu" }}
    <form method="GET" action="/gruppen" class="search">
        <input placeholder="{{ markdownMeta "utility/general" "search"}}" name="query" value="{{ .Query }}" />
	<button aria-label="Suche" type="submit">
            <img src="/assets/images/icon_search.svg" alt="{{ markdownMeta "utility/images" "icon_search"}}" />
        </button>
    </form>
{{ end }}

{{ define "main" }}
    <div class="main invert-order-mobile">
        <div class="left">
            {{ template "left" . }}
        </div>
        <aside class="right">
            {{ template "right" . }}
        </aside>
    </div>
{{ end }}

{{ define "left" }}
    <div class="list">
        {{ range .Groups }}
            <section class="container" itemscope itemtype="https://schema.org/Organization">
                <a href="/gruppe/{{ .ID }}" {{ if isCurrentLetter .StartLetterRune }}id="{{ .StartLetter }}"{{ end }} itemprop="url">
                    <div class="container-header caps">
                        <div></div><h2 itemprop="name">{{ .Name }}</h2><div></div>
                    </div>
                    <div class="container-content">
                        {{ if .Image }}
                            <img src="/api/v1/media/{{ .Image }}" alt="{{ .Alt }}" />
                        {{ else }}
                            <img src="/assets/images/placeholder.png" alt="Bild" />
                        {{ end }}
                    </div>
                </a>
            </section>
        {{ end }}
    </div>
{{ end }}

{{ define "right" }}
    <section class="container">
        <div class="container-header caps">
            <h1>{{ markdownMeta "grouplist/intro" "title"}}</h1>
        </div>
        <div class="container-content">
           {{ markdown "grouplist/intro"}}
        </div>
    </section>
    <section class="container">
        <div class="alphabet">
            {{ range .Alphabet }}
                {{ if .HasItems }}
                    <a href="#{{ .Letter }}">{{ .Letter }}</a>
                {{ else }}
                    <span>{{ .Letter }}</span>
                {{ end }}
            {{ end }}
        </div>
    </section>
{{ end }}
