User-agent: *
Allow: /
Disallow: /backstage
Disallow: /partial/*
Disallow: /ical
Disallow: /rss
Disallow: */ical
Disallow: */rss

Sitemap: {{ joinPath .BaseURL "sitemap.xml"  }}
