{{ define "right" }}{{ end }}
{{ define "left" }}{{ end }}

{{ define "main" }}
    <div class="main">
        <div class="left">
            {{ template "left" . }}
        </div>
        <aside class="right">
            {{ template "right" . }}
        </aside>
    </div>
{{ end }}

<!DOCTYPE html>
<html lang="de">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="go-import" content="eintopf.info git https://codeberg.org/Klasse-Methode/eintopf">
    <title>{{ template "title" . }}</title>
    <meta name="description" content='{{ template "description" . }}'>
    <meta property="og:site_name" content='{{ template "sitename" }}'>
    <meta property="og:title" content='{{ template "title" . }}'>
    <meta property="og:description" content='{{ template "description" . }}'>
    <meta property="og:image" content='{{ template "metaImage" . }}'>
    <meta property="og:type" content="website">
    <meta property="og:url" content='{{ joinPath .BaseURL .URI  }}'>
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content='{{ template "title" . }}'>
    <meta name="twitter:description" content='{{ template "description" . }}'>
    <meta name="twitter:image" content='{{ template "metaImage" . }}'>

    <meta name="robots" content="{{ template "robots" . }}">

    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="theme-color" content="black">

    <link rel="manifest" href="/assets/manifest.json">

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <link rel="apple-touch-startup-image" href="/assets/images/icons-128.png">

    <link rel="stylesheet" href="/assets/css/index.css?lastModified={{ lastModified }}">
    <script src="/assets/js/index.js?lastModified={{ lastModified }}"></script>
    {{ template "head" . }}
<body class="{{ .PageClass }}">
    {{ template "beforeHeader" . }}
    <header class="header">
        <div class="main">
            <div class="left">
                <a class="logo" href="{{ joinPath .BaseURL "/"}}">
                    <picture>
                        <source srcset="/assets/images/logo.webp" type="image/webp">                   <img src="/assets/images/logo.png" alt="{{ template "logoAlt" }} Logo" width="244" height="67" />
                    </picture>
                </a>
                {{ template "beforeBurgerMenu" . }}
                <a class="burger burger-open" href="#nav" aria-label="{{ markdownMeta "utility/navigation" "open_menu"}}">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
            </div>
            <nav class="right" id="nav" aria-label="{{ markdownMeta "utility/navigation" "menu"}}">
                <a class="burger burger-close" href="#" aria-label="{{ markdownMeta "utility/navigation" "open_menu"}}">
                    <span></span>
                    <span></span>
                </a>
                <ul>
                    <li><a class="{{ with eq .URI "/" }}active{{ end }}" href="{{ joinPath .BaseURL "/"}}">{{ markdownMeta "utility/general" "calendar"}}</a></li>
                    <li><a class="{{ with eq .URI "/gruppen" }}active{{ end }}" href="{{ joinPath .BaseURL "/gruppen" }}">{{ markdownMeta "utility/general" "groups"}}</a></li>
                    <li><a class="{{ with eq .URI "/orte" }}active{{ end }}" href="{{ joinPath .BaseURL "/orte" }}">{{ markdownMeta "utility/general" "locations"}}</a></li>
                    <li><a class="{{ with eq .URI "/app" }}active{{ end }} only-mobile" href="{{ joinPath .BaseURL "/app" }}">{{ markdownMeta "utility/general" "locations"}}</a></li>
                    <li>
                        <a class="{{ with eq .URI "/about" }}active{{ end }}" href="{{ joinPath .BaseURL "/about" }}">{{ markdownMeta "about/us" "title"}}</a>
                        <div class="dropdown">
                            <ul>
                                <li><a class="{{ with eq .URI "/imprint" }}active{{ end }}" href="{{ joinPath .BaseURL "/imprint" }}">{{ markdownMeta "imprint/content" "title"}}</a>
                                <li><a class="{{ with eq .URI "/dataprotection" }}active{{ end }}" href="{{ joinPath .BaseURL "/dataprotection" }}">{{ markdownMeta "dataprotection/content" "title"}}</a>
                            </ul>
                        </div>
                    </li>
                    <li id="backstage-link"><a class="{{ with eq .URI "/backstage" }}active{{ end }}" href="{{ joinPath .BaseURL "/backstage" }}">{{ markdownMeta "utility/general" "backstage"}}</a></li>
                </ul>
            </nav>
        </div>
    </header>
    <main class="content">
        {{ template "main" . }}
    </main>
</body>
