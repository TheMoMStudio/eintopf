{{ define "head" }}
    <link rel="stylesheet" href="/assets/css/about.css">
{{ end }}
{{ define "robots" }}noindex, nofollow{{ end }}

{{ define "beforeHeader" }}{{ end }}
{{ define "searchInput" }}{{ end }}
{{ define "beforeBurgerMenu" }}<h2 class="title">{{ markdownMeta "dataprotection/content" "title"}}</h2>{{ end }}

{{ define "main" }}
    <div class="main">
        <div class="left {{ .PageClass }}">
            <section class="container first-container">
                <div class="container-header caps ">
                    <h2>{{ markdownMeta "dataprotection/content" "title"}}</h2>
                </div>
                <div class="container-content">
		  {{ markdown "dataprotection/content"}}
		</div>
            </section>
        </aside>
    </div>
{{ end }}
