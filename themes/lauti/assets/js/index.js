// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

/**
 * Make mobile header sticky once, the page gets scrolled past the logo height.
 */
const stickyMobileHeader = () => {
  if (window.innerWidth > 800) {
    return;
  }

  const body = document.querySelector('body');
  const header = document.querySelector('.header');
  const headerHeight = getComputedStyle(header, null).height;
  const content = document.querySelector('.content');

  window.addEventListener('scroll', () => {
    if (window.scrollY > 85) {
      body.classList.add('sticky');
      content.style.marginTop = headerHeight;
    } else {
      body.classList.remove('sticky');
      content.style.marginTop = 0;
    }
  });
};

/**
 * Open/Close the navigation on mobile. Should work without js but not so nice.
 */
const toggleBurgerMenu = () => {
  const nav = document.querySelector('#nav');
  const burgerOpen = document.querySelector('.burger-open');
  if (!nav || !burgerOpen) {
    return;
  }
  burgerOpen.addEventListener('click', (e) => {
    e.preventDefault();
    nav.style.display = 'block';
  });
  const burgerClose = document.querySelector('.burger-close');
  burgerClose.addEventListener('click', (e) => {
    e.preventDefault();
    nav.style.display = 'none';
  });
};

const setShareLinks = () => {
  const shareBtns = document.querySelectorAll('.share-btn');
  shareBtns.forEach((btn) => {
    if (!navigator.share) {
      return;
    }
    btn.classList.remove('hidden');
    btn.addEventListener('click', () => {
      if (!navigator.share) {
        return;
      }
      const shareDescription = btn.getAttribute('share-description');
      navigator.share({
        title: btn.getAttribute('share-name'),
        text:
          shareDescription.length > 150
            ? `${shareDescription.substring(0, 150)}...`
            : shareDescription,
        url: window.location.href,
      });
    });
  });
};

window.addEventListener('load', () => {
  stickyMobileHeader();
  toggleBurgerMenu();
  setShareLinks();
});
