// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { resizeMap } from './map.js';

// Tabs are only displayed when javascript is active. Otherwise it is display: none
// document.querySelector(".tabs").style.setProperty("display", "flex");
document.head.insertAdjacentHTML(
  'beforeend',
  `<style>body .placelist .tabs { display: flex; }</style>`,
);

const tabs = () => {
  const tabs = {
    list: {
      tab: document.querySelector('.tabs .tab-list'),
      el: document.querySelector('.list'),
    },
    map: {
      tab: document.querySelector('.tabs .tab-map'),
      el: document.querySelector('.map'),
    },
  };
  const click = (tab) => {
    if (tabs[tab].tab.classList.contains('active')) {
      return;
    }
    Object.values(tabs).forEach((tab) => {
      tab.el.style.setProperty('display', 'none');
      tab.tab.classList.remove('active');
    });

    tabs[tab].tab.classList.add('active');
    tabs[tab].el.style.setProperty('display', 'block');

    const rightElement = document.querySelector('aside.right');
    if (tab == 'map') {
      rightElement.classList.add('hidden');
    } else {
      rightElement.classList.remove('hidden');
    }

    // Make sure the map resizes properly.
    resizeMap();
  };

  tabs.list.tab.addEventListener('click', () => click('list'));
  tabs.map.tab.addEventListener('click', () => click('map'));
};

window.addEventListener('load', () => {
  tabs();
});
