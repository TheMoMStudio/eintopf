// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

window.addEventListener('load', () => {
  let installPrompt = null;
  const installButton = document.querySelector('#install');

  window.addEventListener('beforeinstallprompt', (event) => {
    event.preventDefault();
    installPrompt = event;
    if (installButton) {
      installButton.classList.remove('hidden');
    } else {
      console.log('Button not available');
    }
  });

  installButton.addEventListener('click', async () => {
    if (!installPrompt) {
      return;
    }
    const result = await installPrompt.prompt();
    console.log(`Install prompt was: ${result.outcome}`);
  });

  function disableInAppInstallPrompt() {
    installPrompt = null;
    if (installButton) {
      installButton.setAttribute('hidden', '');
    } else {
      console.log('Button not available');
    }
  }

  window.addEventListener('appinstalled', () => {
    disableInAppInstallPrompt();
  });
});
