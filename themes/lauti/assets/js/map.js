// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

const L = window.L;
let map;
let locationList;

export const resizeMap = () => {
  map.invalidateSize();
  // for events with a single location setView function needs to be used
  if (locationList.length === 1) {
    map.setView(locationList[0], 15);
  } else {
    map.fitBounds(L.latLngBounds(locationList), { padding: [40, 40] });
  }
};
/**
 * Initializes a leaflet map on the #map element. Marker data is taken from the
 * data-markers attribute.
 */
const initMap = () => {
  const mapEl = document.getElementById('map');
  /**
   * @type {Array.{name: String, placeId: String, lat: Number, lng: Number}}
   */
  locationList = JSON.parse(mapEl.dataset.markers);

  // configure map for two finger use on mobile so scrolling is still possible on map
  map = L.map('map', {
    dragging: !L.Browser.mobile,
    tap: !L.Browser.mobile,
  });
  // replace lat long information from dataset in HTML to leaflet LatLng Objects
  locationList.map((marker) => L.latLng(marker.lat, marker.lng));

  L.tileLayer('/api/v1/osm/tile/{z}/{x}/{y}.png', {
    minZoom: 9,
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
  }).addTo(map);

  const icon = L.divIcon({
    html: "<div class='marker-pin leaflet-default-icon-path'></div>",
    iconSize: [40, 40],
    iconAnchor: [20, 40],
  });

  locationList.map((marker) => {
    const m = L.marker([marker.lat, marker.lng], { icon }).addTo(map);
    if (marker.placeId) {
      m.bindPopup(`<a href="/ort/${marker.placeId}">${marker.name}</a>`);
    }
  });

  resizeMap();
  mapEl.addEventListener('resize', () => {
    resizeMap();
  });
};

window.addEventListener('load', () => {
  initMap();
});
