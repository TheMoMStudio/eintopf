// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

/**
 * Open/Close the program list.
 */
const toggleProgram = () => {
  const programElements = document.getElementsByClassName('program');

  for (let programElement of programElements) {
    const openElement = programElement.querySelector('.program-link');
    const clickhandler = (e) => {
      e.preventDefault();

      if (programElement.classList.contains('open')) {
        programElement.classList.remove('open');
      } else {
        programElement.classList.add('open');
      }
    };

    // First remove the event listener, since this function may be called
    // multiple times.
    openElement.removeEventListener('click', clickhandler);
    openElement.addEventListener('click', clickhandler);
  }
};

/**
 * Open/Close the tags list
 */
const toggleTags = () => {
  const programElements = document.getElementsByClassName('tags-list');

  for (let programElement of programElements) {
    const openElement = programElement.querySelector('.tags-link');
    const clickhandlerTags = (e) => {
      e.preventDefault();

      if (programElement.classList.contains('open')) {
        programElement.classList.remove('open');
      } else {
        programElement.classList.add('open');
      }
    };

    // First remove the event listener, since this function may be called
    // multiple times.
    openElement.removeEventListener('click', clickhandlerTags);
    openElement.addEventListener('click', clickhandlerTags);
  }
};

/**
 * Automatically loads more events, when scrolling to the botton.
 */
const infiniteScroll = () => {
  const paginationElement = document.querySelector('.pagination');
  const spinner = document.querySelector('.loading-box');

  // The pages is only set, when not all events were loaded on the first page.
  if (paginationElement.dataset.fetchUrl === undefined || spinner === null) {
    return;
  }
  paginationElement.style.display = 'none';

  const fetchUrl = new URL(decodeURI(paginationElement.dataset.fetchUrl));
  const searchParams = fetchUrl.searchParams;
  const pages = JSON.parse(paginationElement.dataset.pages);
  // Loading is set to true, while new events get loaded. This prevents loading
  // the same page multiple times at once.
  // Once all Pages are loaded, the value stays false, to prevent loading more
  // pages.
  let loading = false;
  let page = 0;

  window.addEventListener('scroll', async () => {
    const scrollElement = document.scrollingElement || document.documentElement;
    const { scrollTop, scrollHeight, clientHeight } = scrollElement;

    if (page >= pages.length - 1) {
      // All pages were loaded.
      return;
    }

    if (scrollTop + clientHeight >= scrollHeight - 100 && !loading) {
      loading = true;
      spinner.classList.remove('hidden');
      page++;

      searchParams.set('dateMin', pages[page].start);
      searchParams.set('dateMax', pages[page].end);

      try {
        const data = await fetch(`${fetchUrl.href.split('?')[0]}?${searchParams.toString()}`);
        const text = await data.text();
        const html = text.trim();
        if (data.status != 200) {
          const errorMessage = document.querySelector('.pagination-error');
          if (errorMessage) {
            errorMessage.insertAdjacentHTML('afterbegin', html);
            errorMessage.classList.remove('hidden');
          }
          throw new Error(data.statusText);
        }
        // If the html is empty, all pages were loaded. Returning early
        // keeps loading set to true and therefore prevents loading more
        // event.
        if (html == '') {
          return;
        }
        paginationElement.insertAdjacentHTML('beforebegin', html);
        loading = false;
        // Rerun toggleProgram to apply it for the newly loaded events.
        toggleProgram();
        spinner.classList.add('hidden');
      } catch {
        spinner.classList.add('hidden');
        paginationElement.style.display = 'flex';
      }
    }
  });
};

/**
 * Interactive calendar widget
 */
const calendarWidget = () => {
  const calendarWidgetElements = document.querySelectorAll('.calendar-widget');

  for (const widget of calendarWidgetElements) {
    // Loading is set to true, while new events get loaded. This prevents loading
    // the same page multiple times at once.
    // Once all Pages are loaded, the value stays false, to prevent loading more
    // pages.

    const addEventListener = (el) => {
      const fetchUrl = new URL(decodeURI(el.dataset.fetchUrl));
      // eslint-disable-next-line
      let loading = false;
      el.addEventListener('click', (e) => {
        e.preventDefault();
        loading = true;

        fetch(fetchUrl).then((response) => {
          response.text().then((html) => {
            const tempEl = document.createElement('div');
            tempEl.innerHTML = html.trim();
            widget.replaceWith(tempEl.firstChild);
            loading = false;
            calendarWidget();
          });
        });
      });
    };

    addEventListener(widget.querySelector('.calendar-action-prev'));
    addEventListener(widget.querySelector('.calendar-action-next'));
  }
};

window.addEventListener('load', () => {
  toggleProgram();
  toggleTags();
  infiniteScroll();
  calendarWidget();
});
