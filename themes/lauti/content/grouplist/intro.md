---
title: Politische Gruppen in Stuttgart
---

Hier findest du unkommerzielle, emanzipatorische und linke Gruppen, die in Stuttgart und Umgebung aktiv sind. Das breite Themenspektrum reicht von Klimagerechtigkeit über Feminismus, Antifaschismus, gewerkschaftliche Organisierung, Sozialismus über Migrationspolitik bis zu kulturellen Gruppen. Die meisten sind often für Mitstreiter\*innen, die sich politisch oder kulturell engagieren wollen.
