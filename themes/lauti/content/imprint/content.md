---
title: Impressum
---

### Angaben gemäß § 5 TMG

Maxi Musterperson

c/o Musterorga

Musterstraße 1

12345 Musterstadt

#### Kontakt:

Mastodon: [https://sueden.social/@eintopf](https://sueden.social/@eintopf")

E-Mail: mail [at] eintopf.info

#### Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV:

Maxi Musterperson

c/o Musterorga

Musterstraße 1

12345 Musterstadt
