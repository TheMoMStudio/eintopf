---
when: Wann
where: Wo
who: Wer
what: Was
with: Mit
canceled: Fällt aus
show: anzeigen
clock: Uhr
share: Teilen
search: Suche
event: Veranstaltung
future_event: Zukünftige Veranstaltung
future_event_by: Zukünftige Veranstaltung von
past_event: Vergangene Veranstaltung
past_event_by: Vergangene Veranstaltung von
events: Veranstaltungen
program: Programm
map: Karte
feed: Feed
feed_rss: RSS Feed
feed_ical: iCal Feed
filter: Filter
category: Kategorie
categories: Kategorien
topic: Thema
topics: Themen
calendar: Kalender
add_calendar: Zum Kalender hinzufügen
website: Website
email: E-Mail
address: Address
list: Liste
groups: Gruppen
locations: Orte
app: App
backstage: Backstage
tags: Tags
repeating_events: Regelmäßige Veranstaltungen
only_repeating:  Nur regelmäßig
only_one_time: Nur einmalig
---
