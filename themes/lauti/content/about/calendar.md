---
title: Kalender in anderen Städten
---
- [Aachen - Bewegungsmelder](https://bewegungsmelder-aachen.de/)
- [Augsburg - Auxpunks](https://auxpunks.uber.space/)
<br/><br/>
- [Berlin - Stressfaktor](https://stressfaktor.squat.net/)
- [Bielefeld - Linke LANDSCHAFT](https://www.lilabi.net/)
- [Bochum - Bewegung in Bochum](https://www.bo-alternativ.de)
- [Brandenburg - iNFORIOT](https://inforiot.de/)
- [Bonn - Flying High](https://flyinghigh-bonn.org/)
- [Bremen - FOMO](https://fomobremen.info/)
<br/><br/>
- [Darmstadt - Politnetz](https://politnetz-darmstadt.de)
- [Dortmund - Latscher.in](https://latscher.in)
- [Dresden - Terminal](https://terminal.digital)
<br/><br/>
- [Erfurt - sabotnik](http://sabotnik.blogsport.de/determine/)
<br/><br/>
- [Frankfurt a.M. - Desmosphere](https://ffm.demosphere.net)
- [Freiburg - Kultur Forum](https://www.kulturforum-freiburg.de/index.php/veranstaltungen)
- [Freiburg - Kultur Forum](https://www.kulturforum-freiburg.de/veranstaltungen)
- [Freiburg - Stadtwandler](https://www.stadtwandler.org/de/events)
- [Freiburg - Tacker](https://tacker.fr)
<br/><br/>
- [Hamburg - Bewegungsmelder](https://www.bewegungsmelder.org/)
- [Hannover - Rauszeit](https://rauszeit-termine.org/)
- [Heidelberg - Sozialforum](https://sofo-hd.de)
<br/><br/>
- [Jena - Was Tun](https://wastun.co)
<br/><br/>
- [Köln - Plotter](https://plotter.infoladen.de/)
<br/><br/>
- [Leipzig - Planlos](https://www.planlos-leipzig.org/)
- [Leipzig - Trichter](https://trichter.cc/)
<br/><br/>
- [Mannheim - Kummunalinfo](https://kommunalinfo-mannheim.de/kalender/)
- [München - Kalinka](https://www.kalinka-m.org/)
- [Münster - Alternative](https://ms-alternativ.de/)
- [Nürnberg - Red Side](https://www.red-side.net/)
<br/><br/>
- [Osnabrück - Alternative](https://osnabrueck-alternativ.de/)
<br/><br/>
- [Potsdam - Rotes Potsdam](https://rotes.potsda.mn/)
<br/><br/>
- [Rhein/Neckar - ausrasten](https://ausrasten.blackblogs.org/)
- [Rostock - Stadtgestalten](https://stadtgestalten.org/)
- [Ruhrpott - hermine](https://hermine-termine.net/)
- [Trier - Pol-Ver-Fass](https://mastodon.social/@Polverfass)
<br/><br/>
- [Würzburg - Demosphere](https://wuerzburg.demosphere.net/)
<br/><br/>
- [Wien - Zeitdiebin](https://zeit.diebin.at)
