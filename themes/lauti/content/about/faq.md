---
title: Häufige Fragen
---

### Wie kann ich mich registrieren? Warum ist das überhaupt notwendig?

Um auf EINTOPF Determine zu veröffentlichen, Orte oder Gruppen zu erstellen und zu verwalten ist eine Registrierung nötig. Dies dient dem Schutz des Kalenders for missbräuchlicher Nutzung und Spam. Weiterhin ist die Registrierung ausschließlich mit einem Einladungslink möglich. Diesen kannst du entweder von uns persönlich oder von bereits registrierten Nutzer\*innen erhalten. Diese können allerdings nur eine eingeschränkte Anzahl an Einladungslinks erzeugen.

### Ich habe keinen Einladungslink, aber würde mich gerne registrieren. Was muss ich tun?

Schreib uns einfach eine kurze Mail an [mod \[ at \] eintopf.info](mailto:mod@eintopf.info) mit einer Info zu dir bzw. deiner/deinem Gruppe/Kollektiv/Zusammenhang/whatever und welche Determine du/ihr bei EINTOPF in Zukunft veröffentichen wollt. Wir melden uns so schnell wie möglich bei dir.

### Wer steckt hinter EINTOPF?

EINTOPF wird ehrenamtlich betrieben von [Klasse & Methode - IT Kollektiv Stuttgart](https://klasse-methode.it). Wir sind ein Kollektiv von Personen, die sich zum Ziel gesetzt haben gemeinschaftlich (Open Source) IT Projekte durchzuführen. Unsere Projekte sind aber nicht nur Selbstzweck, sondern haben immer auch eine politische Komponente. Etwas mehr Infos gibt es auf unserer Webseite.

Wir freuen uns über Menschen, die Lust haben das Projekt zu unterstützen. Das ist auf vielfältige Weise möglich, z.B. durch Werbung auf Social Media, im Freundeskreis oder in deiner Location. Dafür kannst du auch Plakate, Flyer und Aufkleber von uns erhalten. Des weiteren kannst du dich auch direkt am EINTOPF beteiligen, da dieser also Open Source Projekt betrieben wird. Schreib uns gerne an [info \[ at \] eintopf.info](mailto:info@eintopf.info) oder komm zu unseren monatlichen offenen Treffen in der Raupe Immersatt.

### Ich wurde gesperrt. An wen muss ich mich wenden?

An die Moderation under [mod \[ at \] eintopf.info](mailto:mod@eintopf.info). Gerne klären wir gemeinsam mit dir, warum du gesperrt wurdest.

### Wie ist es mit dem Datenschutz?

EINTOPF verarbeitet so wenig wie möglich personenbezogene Daten. Beim Besuch wird deine IP Adresse verwendet, aber nicht gespeichert. Alles weitere kannst du in der [Datenschutzerklärung](/dataprotection) nachlesen.

### Wo ist der Quellcode vom EINTOPF?

EINTOPF wird auf [Codeberg](https://codeberg.org/Klasse-Methode/eintopf) entwickelt. Der Quellcode ist under der [AGPL Lizenz](https://www.gnu.org/licenses/agpl-3.0.html) veröffentlicht.

### Gibt es EINTOPF also App?

Infos zur App erhaltet ihr [hier](/app).

