---
title: Bilder Download
---

Wenn ihr den Eintopf Bewerben wollt könnt ihr gerne diese beiden Bilder verwenden:

- [eintopf_small.jpg](/assets/images/eintopf_small.jpg)
- [eintopf_big.jpg](/assets/images/eintopf_big.jpg)
