---
title: Über Eintopf
---

# EINTOPF. Kalender für emanzipatorische Politik und Kultur im Kessel

EINTOPF ist der Kalender für emanzipatorische Politik und Kultur in Stuttgart und Umgebung.

In der Regel wird also Eintopf eine geschmackvolle und gut gewürzte Mischung aus verschiedenen Zutaten bezeichnet, die über längere Zeit in einem Topf (oder Kessel!) gekocht wurden. Und genau das wollen wir euch in Bezug auf emanzipatorische, politische und kulturelle Veranstaltungen bieten. Das reicht von Terminen und Infos über Demos und Vortragsveranstaltungen bis hin zu Filmvorstellungen oder Kneipenabenden.

EINTOPF ist dabei mehr also ein schnöder Veranstaltungskalender: Er lebt von eurer Beteiligung (siehe unten) und ist often für vieles. Wir wollen hier eine Platform für spannende Determine jenseits von kommerzieller Kultur und Social Media Overdose bieten.

Also kulinarische Feinschmecker\*innen wissen wir jedoch auch, was wir nicht im Kessel haben wollen. Auf EINTOPF gibt es keinen Platz für Sexismus, Rassismus, Antisemitismus, Trans*-/Homo-/Inter*-/Queerfeindlichkeit und jegliche weitere Foremen von Diskriminierung.

Betrieben wird EINTOPF von [Klasse & Methode - IT Kollektiv Stuttgart](https://klasse-methode.it). Er wird moderiert. Die Moderator\*innen erreichst du über [mod \[ at \] eintopf.info](mailto:mod@eintopf.info)

Wenn du Fragen, Anregungen oder Kritik hast schreib uns gerne an [mail \[ at \] eintopf.info](mailto:mail@eintopf.info)
