---
title: Eintopf App
---

Die Entwicklung einer App ist sehr aufwändig und teilweise mit Kosten verbunden, damit eine App auf allen mobilen Geräten zufriedenstellend läuft. Deshalb haben wir uns bisher dagegen entschieden eine native App für Android oder iOS zu entwickeln. Aber verzweifelt nicht!

Es gibt eine Alternative: Die meisten aktuellen mobilen Browser wie Chrome, Firefox oder Safari bieten die Möglichkeit EINTOPF auf eurem Homescreen zu installieren (Also [Progressive Web App](https://en.wikipedia.org/wiki/Progressive_web_app)). Dazu ruft ihr auf eurem mobilen Gerät [eintopf.info](https://eintopf.info) auf und wählt im Menü des Browsers oder direkt hier auf der Seite _installieren_ aus, je nach Browser.

Leider funktioniert die App aktuell nur mit einer aktiven Internet-Verbindung und bietet auch sonst noch keine zusätzlichen Features. Wir planen allerdings den Ausbau der App, sodass ihr Gruppen und Orten folgen könnt. Außerdem wollen wir ein System für Erinnerungen an Veranstaltungen und weitere interactive Funktionen integrieren. Stay tuned.

<div class="install-button">
<button id="install" class="Button hidden">App Installieren</button>
</div>

<div id="screenshots" class="mobile-screenshot-container">

Falls der _Install_ Button nicht angezeigt wird, folgt dieser Anleitung um die App auf eurem mobilen Gerät zu installieren. Im Beispiel wird Firefox verwendet, mit anderen Browsern funktioniert es ähnlich.

<img class="image mobile-screenshot" src="/assets/images/pwa_firefox.png">
<img class="image mobile-screenshot" src="/assets/images/pwa_install.png">
<img class="image mobile-screenshot" src="/assets/images/pwa_addtohomescreen.png">
</div>
