<!--
SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>

SPDX-License-Identifier: CC0-1.0
-->

# Deployment

## Environment variables

\* mandatory

| ENV | Default | Example | Description |
| -------------------------------- | ----------- | ----------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------- |
| EINTOPF_SQLITE_DB\* | eintopf.db | eintopf.db | Path pointing to the sqlite database file |
| EINTOPF_ADDR | 3333 | 3333 | Http listen address |
| EINTOPF_HTTP_READ_TIMEOUT | 5s | 10s | Http read timeout |
| EINTOPF_HTTP_WRITE_TIMEOUT | 5s | 10s | Http write timeout |
| EINTOPF_HTTP_HANDLER_TIMEOUT | 5s | 10s | Http hanlder timeout |
| EINTOPF_AUTH_KEY_PATH | auth-key | /var/lib/eintopf/auth-key | Path pointing to the private key used by the auth service. It also expects a public key at {EINTOPF_AUTH_KEY_PATH}.pub |
| EINTOPF_SEARCH_INDEX_PATH\* | index.bleve | /var/lib/eintopf/index.bleve | Path pointing bleve index |
| EINTOPF_SEARCH_TIMEOUT | 10s | 1s | Maximum duration for a search request |
| EINTOPF_SEARCH_RESULT_CACHE_SIZE | 100 | 10 | Number of search results cached |
| EINTOPF_SEARCH_BUCKET_CACHE_SIZE | 100 | 10 | Number of search bucket cached |
| EINTOPF_ADMIN_EMAIL | | myadmin | Admin email address, used to generate an admin user, if it does not exist |
| EINTOPF_ADMIN_PASSWORD | | mypassword | Admin password, used to generate an admin user, if it does not exist |
| EINTOPF_KILLSWITCH_HASHES\* | | foo,bar | List of strings, that can be used to stop the eintopf server with an http request |
| EINTOPF_MEDIA_PATH\* | | /var/lib/eintopf/media | Path pointing to a directory, where all media files are uploaded to |
| EINTOPF_BASE_URL\* | | https://eintopf.info | Base url, used for linking to the frontend from the server |
| EINTOPF_TIMEZONE | UTC | Europe/Berlin | Time zone used in eintopf |
| EINTOPF_THEME | eintopf | eintopf | Select the active theme from the THEMES_PATH that will be used at startup. If this value is empty it will fallback to the embedded theme. |
| EINTOPF_THEMES_PATH | themes/ | themes/ | Path pointing to a directory, where the themes will be stored. |
| EINTOPF_PLAUSIBLE_HOST | | https://plausible.io | Host of the plausible server for analytics |
| EINTOPF_PLAUSIBLE_DOMAIN | | eintopf.info | Domain for plausible analytics |
| EINTOPF_OSM_TILE_SERVER | | https://tile.openstreetmap.org/{z}/{x}/{y}.png | Open Street Map tile server |
| EINTOPF_OSM_TILE_CACHE_DIR | | /var/lib/eintopf/osm | Cache Directory for Open Street Map tiles |
| EINTOPF_MAIL_SMTP_HOST | | mailserver.example.com:465 | Host for the mail server including the port |
| EINTOPF_MAIL_SMTP_PASSWORD | | supersecure | Password for mail authentication |
| EINTOPF_MAIL_SMTP_USER | | supersecure | User for mail authentication |
| EINTOPF_MAIL_SMTP_SECURE | | SSL | Either SSL or StartTLS |
| EINTOPF_MAIL_ADDRESS | | mail@example.com | Mail address for sendinig mails from |
| EINTOPF_MAIL_ALLOW_LIST | | foo@bar.com,bar@foo.com | List of email addresses. When set only those mail addresses will receive emails |

## Custom Themes

Currently there is one bundeled theme (`eintopf`), which is also the default one. It is possible to define custom themes by using the `EINTOPF_THEME` and `EINTOPF_THEMES_PATH` environment variables. `EINTOPF_THEMES_PATH` is the path to the directory where themes are stored. `EINTOPF_THEME` takes the name of the theme which is either the bundeled theme (`eintopf`) or directory in the `EINTOPF_THEMES_PATH` on the filesystem. You can specify a child theme in the `theme.yaml` with the `parent` key. The `parent` key will search for the name of the theme. When using custom themes each next theme overrides the previous one with the last one taking the highest priority. The priority from low to high is bundeled->theme->child. This means you don't need to edit or copy all files of a theme and make adjustments only where needed. The bundeled theme is used as fallback if something went wrong or files are missing.

**Example:**

```
EINTOPF_THEME=eintopf
EINTOPF_THEMES_PATH=themes/
```

### Custom Sitename

```
# /path/to/my/custom/theme/templates/globals.tpl


{{ define "sitename" }}Custom Site Name{{ end }}
```

## Building docker images
### Enable multi platform builds in docker
```bash
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
docker buildx create --name eintopf
docker buildx use eintopf
docker buildx inspect --bootstrap
```
### Build and push image local
```bash
make image-build
make image-push
```
