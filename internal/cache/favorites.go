//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package cache

import (
	"sync"
)

// Favorites implements a generic key value storage, with a maximum number of
// entries. It keeps track of reads to favor keys with more reads when the cache
// has reached the maximum of entries.
// It is save for concurrent usage.
type Favorites[T any] struct {
	data  map[string]T
	dataM *sync.RWMutex

	reads  map[string]uint
	readsM *sync.Mutex

	// maxEntries is the maximum number of entries in the data map.
	maxEntries int
}

// NewFavoritesCache returns a newly initialized favorites cache with a given
// maximum number of entries.
func NewFavoritesCache[T any](maxEntries int) *Favorites[T] {
	return &Favorites[T]{
		data:  make(map[string]T, maxEntries),
		dataM: &sync.RWMutex{},

		reads:      map[string]uint{},
		readsM:     &sync.Mutex{},
		maxEntries: maxEntries,
	}
}

// Get returns the value for the given key when available. The second return
// value indicates, wether a value was for for the given key.
func (c *Favorites[T]) Get(key string) (T, bool) {
	c.dataM.RLock()
	defer c.dataM.RUnlock()
	go c.trackRead(key)

	value, ok := c.data[key]
	if ok {
		return value, true
	}
	var zeroVal T
	return zeroVal, false
}

// Set takes a key and value an stores them. If the storage already has
// reached its maximum amount of entries, the entry with the least reads will be
// overridden.
func (c *Favorites[T]) Set(key string, value T) {
	c.dataM.Lock()
	defer c.dataM.Unlock()

	if len(c.data) >= c.maxEntries {
		c.readsM.Lock()
		defer c.readsM.Unlock()
		lowest := ^uint(0)
		lowestKey := ""
		for k := range c.data {
			l, ok := c.reads[k]
			if !ok {
				continue
			}
			if l < lowest {
				lowest = l
				lowestKey = k
			}
		}
		if lowestKey != "" {
			delete(c.data, lowestKey)
		}
	}

	c.data[key] = value
}

func (c *Favorites[T]) trackRead(key string) {
	c.readsM.Lock()
	defer c.readsM.Unlock()

	c.reads[key] += 1
}

// Clear resets the internal state.
func (c *Favorites[T]) Clear() {
	c.dataM.Lock()
	defer c.dataM.Unlock()
	c.readsM.Lock()
	defer c.readsM.Unlock()

	c.data = make(map[string]T, c.maxEntries)
	c.reads = make(map[string]uint)
}
