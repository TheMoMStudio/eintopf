//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package cache_test

import (
	"math/rand"
	"strconv"
	"testing"
	"time"

	"eintopf.info/internal/cache"
)

func TestFavoritesCache(t *testing.T) {
	c := cache.NewFavoritesCache[[]byte](10)
	v, ok := c.Get("foo")
	if ok {
		t.Error("ok should be false, when key was not set before")
	}
	if v != nil {
		t.Error("value should be nil, when key was not set before")
	}

	c.Set("foo", []byte("bar"))
	v, ok = c.Get("foo")
	if !ok {
		t.Error("ok should be ture, when key was set before")
	}
	if string(v) != "bar" {
		t.Error("value should be nil, when key was set before")
	}
}

func TestFavoritesCacheConcurrentReadsAndWrites(t *testing.T) {
	c := cache.NewFavoritesCache[[]byte](10)
	for i := 0; i < 100; i++ {
		go func(i int) {
			time.Sleep(time.Duration(rand.Intn(100)))
			c.Set(strconv.Itoa(i), []byte("aaaaaaa"))
		}(i)
	}
	for i := 0; i < 10000; i++ {
		go func(i int) {
			time.Sleep(time.Duration(rand.Intn(10)))
			c.Get(strconv.Itoa(i % 100))
		}(i)
	}
}
