// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package crud

import "context"

// SortOrder defines the order of sorting.
type SortOrder string

// Possible values for SortOrder.
const (
	OrderAsc  = SortOrder("ASC")
	OrderDesc = SortOrder("DESC")
)

// FindParams defines parameters used by Find methods.
type FindParams[Filters any] struct {
	Offset int64 `json:"offset,omitempty"`
	Limit  int64 `json:"limit,omitempty"`

	Sort  string    `json:"sort,omitempty"`
	Order SortOrder `json:"order,omitempty"`

	Filters *Filters `json:"filters,omitempty"`
}

// Identifiable is an object with an identifier.
type Identifiable interface {
	// Identifier returns the unique identifier of the object.
	Identifier() string
}

// Storer defines a generic crud store.
type Storer[NewModel any, Model Identifiable, Filters any] interface {
	Create(ctx context.Context, newModel *NewModel) (*Model, error)
	Update(ctx context.Context, model *Model) (*Model, error)
	Delete(ctx context.Context, id string) error
	FindByID(ctx context.Context, id string) (*Model, error)
	Find(ctx context.Context, params *FindParams[Filters]) ([]*Model, int, error)
}
