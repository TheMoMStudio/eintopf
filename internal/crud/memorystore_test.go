//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package crud_test

import (
	"context"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"

	"eintopf.info/internal/crud"
)

type MyModel struct {
	ID      string    `db:"id"`
	Name    string    `db:"name"`
	Number  int       `db:"number"`
	Time    time.Time `db:"time"`
	Pointer *string   `db:"pointer"`
	Array   []string  `db:"array"`
	// From is an sql keyword
	From string `db:"from"`
}

func (m MyModel) Identifier() string { return m.ID }

type NewMyModel struct {
	Name    string
	Number  int
	Time    time.Time
	Pointer *string
	Array   []string
	From    string
}

func modelFromNewModel(newMyModel *NewMyModel, id string) *MyModel {
	return &MyModel{
		ID:      id,
		Name:    newMyModel.Name,
		Number:  newMyModel.Number,
		Time:    newMyModel.Time,
		Pointer: newMyModel.Pointer,
		Array:   newMyModel.Array,
		From:    newMyModel.From,
	}
}

type MyModelFilters struct {
	ID         *string    `db:"id"`
	NotID      *string    `db:"id"`
	Name       *string    `db:"name"`
	LikeName   *string    `db:"name"`
	Number     *int       `db:"number"`
	Time       *time.Time `db:"time"`
	TimeBefore *time.Time `db:"time"`
	TimeAfter  *time.Time `db:"time"`
	Pointer    *string    `db:"pointer"`
	Array      []string   `db:"array"`
	From       *string    `db:"from"`
}

func TestMemoryStoreBasics(t *testing.T) {
	store := crud.NewMemoryStore[NewMyModel, MyModel, MyModelFilters](modelFromNewModel, nil)
	foo, err := store.Create(context.Background(), &NewMyModel{Name: "foo"})
	if err != nil {
		t.Fatal(err)
	}

	foo.Name = "bar"
	foo2, err := store.FindByID(context.Background(), foo.ID)
	if err != nil {
		t.Fatal(err)
	}
	if foo2.Name != "foo" {
		t.Errorf("name should not have changed, got: %s", foo2.Name)
	}

	bar, err := store.Update(context.Background(), foo)
	if err != nil {
		t.Fatal(err)
	}
	if foo.ID != bar.ID {
		t.Fatal("id changed")
	}

	foo.Name = "foobar" // Check that foo does not effect bar

	bar2, err := store.FindByID(context.Background(), bar.ID)
	if err != nil {
		t.Fatal(err)
	}

	if diff := cmp.Diff(bar, bar2); diff != "" {
		t.Fatalf("bar and bar2 mismatch (-want +got):\n%s", diff)
	}

	err = store.Delete(context.Background(), bar.ID)
	if err != nil {
		t.Fatal(err)
	}
}

func TestMemoryStoreFind(t *testing.T) {
	store := crud.NewMemoryStore[NewMyModel, MyModel, MyModelFilters](modelFromNewModel, nil)

	date1 := time.Date(2022, 10, 5, 12, 0, 0, 0, time.UTC)
	foo, err := store.Create(context.Background(), &NewMyModel{
		Name:    "foo",
		Number:  1,
		Time:    date1,
		Pointer: strptr("foo"),
		Array:   []string{"a", "b"},
	})
	if err != nil {
		t.Fatal(err)
	}

	date2 := time.Date(2023, 10, 6, 12, 0, 0, 0, time.UTC)
	bar, err := store.Create(context.Background(), &NewMyModel{
		Name:   "bar",
		Number: 2,
		Time:   date2,
		Array:  []string{"b", "c"},
	})
	if err != nil {
		t.Fatal(err)
	}

	tests := []struct {
		name   string
		params *crud.FindParams[MyModelFilters]
		total  int
		models []*MyModel
	}{
		{name: "NoParams", params: nil, total: 2, models: []*MyModel{foo, bar}},
		{
			name: "NotFilter",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{NotID: strptr(foo.ID)},
			},
			total:  1,
			models: []*MyModel{bar},
		},
		{
			name: "StringFilter",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{Name: strptr("foo")},
			},
			total:  1,
			models: []*MyModel{foo},
		},
		{
			name: "LikeStringFilter",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{LikeName: strptr("f")},
			},
			total:  1,
			models: []*MyModel{foo},
		},
		{
			name: "IntFilter",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{Number: intptr(2)},
			},
			total:  1,
			models: []*MyModel{bar},
		},
		{
			name: "PointerFilter",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{Pointer: strptr("foo")},
			},
			total:  1,
			models: []*MyModel{foo},
		},
		{
			name: "TimeFilter",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{Time: timeptr(date1)},
			},
			total:  1,
			models: []*MyModel{foo},
		},
		{
			name: "TimeAfter",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{TimeAfter: timeptr(date1)},
			},
			total:  1,
			models: []*MyModel{bar},
		},
		{
			name: "TimeBefore",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{TimeBefore: timeptr(date2)},
			},
			total:  1,
			models: []*MyModel{foo},
		},
		{
			name: "ArrayFilterOne",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{Array: []string{"a"}},
			},
			total:  1,
			models: []*MyModel{foo},
		},
		{
			name: "ArrayFilterMultiple",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{Array: []string{"a", "c"}},
			},
			total:  2,
			models: []*MyModel{foo, bar},
		},
		{
			name: "SortASC",
			params: &crud.FindParams[MyModelFilters]{
				Sort:  "name",
				Order: crud.OrderAsc,
			},
			total:  2,
			models: []*MyModel{bar, foo},
		},
		{
			name: "SortDesc",
			params: &crud.FindParams[MyModelFilters]{
				Sort:  "name",
				Order: crud.OrderDesc,
			},
			total:  2,
			models: []*MyModel{foo, bar},
		},
		{
			name: "SortTimeAsc",
			params: &crud.FindParams[MyModelFilters]{
				Sort:  "time",
				Order: crud.OrderAsc,
			},
			total:  2,
			models: []*MyModel{foo, bar},
		},
		{
			name: "SortTimeDesc",
			params: &crud.FindParams[MyModelFilters]{
				Sort:  "time",
				Order: crud.OrderDesc,
			},
			total:  2,
			models: []*MyModel{bar, foo},
		},
		{
			name: "Paginate",
			params: &crud.FindParams[MyModelFilters]{
				Sort:   "name",
				Order:  crud.OrderDesc,
				Offset: 1,
				Limit:  1,
			},
			total:  2,
			models: []*MyModel{bar},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			models, total, err := store.Find(context.Background(), tc.params)
			if err != nil {
				t.Fatal(err)
			}
			if total != tc.total {
				t.Errorf("total mismatch: want: %d, got: %d", tc.total, total)
			}
			if diff := cmp.Diff(tc.models, models); diff != "" {
				t.Errorf("models mismatch (-want +got):\n%s", diff)
			}
		})
	}
}

func strptr(str string) *string {
	return &str
}

func intptr(n int) *int {
	return &n
}

func timeptr(t time.Time) *time.Time {
	return &t
}
