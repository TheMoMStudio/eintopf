// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package crud

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"reflect"
	"strings"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"golang.org/x/exp/slices"
)

type SqlStore[NewModel any, Model Identifiable, Filter any] struct {
	db    *sqlx.DB
	table SqlTable[Filter]

	modelFromNewModel NewModelFunc[NewModel, Model]
	idGenerator       IDGeneratorFunc[NewModel]
}

// NewSqlStore returns a new generic sql store.
func NewSqlStore[NewModel any, Model Identifiable, Filters any](db *sqlx.DB, table SqlTable[Filters], modelFromNewModel NewModelFunc[NewModel, Model], idGenerator IDGeneratorFunc[NewModel]) *SqlStore[NewModel, Model, Filters] {
	return &SqlStore[NewModel, Model, Filters]{
		db:    db,
		table: table,

		modelFromNewModel: modelFromNewModel,
		idGenerator:       idGenerator,
	}
}

type SqlTable[Filter any] struct {
	Table          string
	IDField        string
	Fields         []string
	SortableFields []string
}

func (s *SqlStore[NewModel, Model, Filter]) Create(ctx context.Context, newModel *NewModel) (*Model, error) {
	id := uuid.New().String()
	if s.idGenerator != nil {
		id = s.idGenerator(newModel)
	}
	model := s.modelFromNewModel(newModel, id)
	_, err := s.db.NamedExecContext(ctx, s.table.createQuery(), model)
	if err != nil {
		return nil, fmt.Errorf("create %s: %s", s.table.Table, err)
	}
	return model, nil
}

func (s SqlTable[Filter]) fields() string {
	fields := []string{}
	for _, f := range s.Fields {
		fields = append(fields, fmt.Sprintf("`%s`", f))
	}
	return strings.Join(fields, ",")
}

func (s SqlTable[Filter]) fieldsWithPrefix(prefix string) string {
	fields := make([]string, 0, len(s.Fields))
	for _, f := range s.Fields {
		fields = append(fields, fmt.Sprintf("`%s`.`%s`", prefix, f))
	}
	return strings.Join(fields, ",")
}

func (s SqlTable[Filter]) createQuery() string {
	fieldValues := make([]string, 0, len(s.Fields))
	for _, f := range s.Fields {
		fieldValues = append(fieldValues, fmt.Sprintf(":%s", f))
	}
	return fmt.Sprintf("INSERT INTO %s (%s) VALUES (%s)", s.Table, s.fields(), strings.Join(fieldValues, ","))
}

func (s *SqlStore[NewModel, Model, Filter]) Update(ctx context.Context, model *Model) (*Model, error) {
	_, err := s.db.NamedExecContext(ctx, s.table.updateQuery(), model)
	if err != nil {
		return nil, fmt.Errorf("update %s: %s", s.table.Table, err)
	}
	return model, nil
}

func (s SqlTable[Filter]) updateQuery() string {
	sets := []string{}
	for _, field := range s.Fields {
		sets = append(sets, fmt.Sprintf("`%s`=:%s", field, field))
	}

	return fmt.Sprintf("UPDATE %s SET %s WHERE `%s`=:%s", s.Table, strings.Join(sets, ","), s.IDField, s.IDField)
}

func (s *SqlStore[NewModel, Model, Filter]) Delete(ctx context.Context, id string) error {
	_, err := s.db.ExecContext(ctx, s.table.deleteQuery(), id)
	if err != nil {
		return fmt.Errorf("delete %s: %s", s.table.Table, err)
	}
	return nil
}

func (s SqlTable[Filter]) deleteQuery() string {
	return fmt.Sprintf("DELETE FROM %s WHERE `%s` = $1", s.Table, s.IDField)
}

func (s *SqlStore[NewModel, Model, Filter]) FindByID(ctx context.Context, id string) (*Model, error) {
	model := new(Model)
	err := s.db.GetContext(ctx, model, s.table.findByIDQuery(), id)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, fmt.Errorf("findByID %s: %s", s.table.Table, err)
	}
	return model, nil
}

func (s SqlTable[Filter]) findByIDQuery() string {
	return fmt.Sprintf("SELECT %s FROM %s WHERE `%s` = $1", s.fields(), s.Table, s.IDField)
}

func (s *SqlStore[NewModel, Model, Filter]) Find(ctx context.Context, params *FindParams[Filter]) ([]*Model, int, error) {
	query, whereStatements, sqlParams, err := s.table.findQuery(params)
	if err != nil {
		return nil, 0, fmt.Errorf("find %s: %s", s.table.Table, err)
	}
	rows, err := s.db.NamedQueryContext(ctx, query, sqlParams)
	if err != nil && err != sql.ErrNoRows {
		// Log query when an error occurs.
		log.Println(query)
		return nil, 0, fmt.Errorf("find %s: %s", s.table.Table, err)
	}
	defer rows.Close()
	models := make([]*Model, 0)
	for rows.Next() {
		model := new(Model)
		err := rows.StructScan(model)
		if err != nil {
			return nil, 0, fmt.Errorf("find %s: %s", s.table.Table, err)
		}

		models = append(models, model)
	}

	total, err := s.total(ctx, whereStatements, sqlParams)
	if err != nil {
		return nil, 0, fmt.Errorf("find total: %s", err)
	}

	return models, total, nil
}

func (s SqlTable[Filter]) findQuery(params *FindParams[Filter]) (string, []string, map[string]any, error) {
	query := fmt.Sprintf("SELECT %s FROM %s", s.fieldsWithPrefix(s.Table), s.Table)
	if params == nil {
		return query, nil, nil, nil
	}

	whereStatements := []string{}
	sqlParams := make(map[string]any)

	if params.Filters != nil {
		zeroVal := reflect.Value{}
		filtersReflected := reflect.Indirect(reflect.ValueOf(params.Filters))
		// Iterate over all filter fields. When the filter is not nil, try
		// to match it to the coresponding model field.
		for i := 0; i < filtersReflected.NumField(); i++ {
			filterField := filtersReflected.Field(i)
			filterValues := []interface{}{filterField.Interface()}
			switch filterField.Kind() {
			case reflect.Ptr:
				// When the filter was not set it has a nil pointer as a value.
				// In that case the field gets ignored.
				if filterField.Elem() == zeroVal {
					continue
				}
				filterValues = []interface{}{filterField.Elem().Interface()}
			case reflect.Array, reflect.Slice:
				// When the filter was not set it has 0 length.
				// In that case the field gets ignored.
				if filterField.Len() == 0 {
					continue
				}

				filterValues = []interface{}{}
				for i := 0; i < filterField.Len(); i++ {
					filterValues = append(filterValues, filterField.Index(i).Interface())
				}
			}

			_, comparator := parseFieldName(filtersReflected.Type().Field(i).Name)
			fieldName := filtersReflected.Type().Field(i).Tag.Get("db")

			for i, filterValue := range filterValues {
				key := fmt.Sprintf("%s_%s_%d", fieldName, comparator, i)
				sqlParams[key] = filterValue

				if comparator == "like" {
					sqlParams[key] = fmt.Sprintf("%%%s%%", filterValue)
				}

				switch comparator {
				case "eq":
					whereStatements = append(whereStatements, fmt.Sprintf("`%s`.`%s`=:%s", s.Table, fieldName, key))
				case "neq":
					whereStatements = append(whereStatements, fmt.Sprintf("`%s`.`%s`!=:%s", s.Table, fieldName, key))
				case "timeAfter":
					whereStatements = append(whereStatements, fmt.Sprintf("`%s`.`%s`>:%s", s.Table, fieldName, key))
				case "timeBefore":
					whereStatements = append(whereStatements, fmt.Sprintf("`%s`.`%s`<:%s", s.Table, fieldName, key))
				case "like":
					whereStatements = append(whereStatements, fmt.Sprintf("`%s`.`%s` LIKE :%s", s.Table, fieldName, key))
				}
			}
		}
	}
	if len(whereStatements) > 0 {
		query += " WHERE " + strings.Join(whereStatements, " AND ")
	}

	if params.Sort != "" {
		if !slices.Contains(s.SortableFields, params.Sort) {
			return "", nil, nil, fmt.Errorf("invalid sort field: %s", params.Sort)
		}
		order := "ASC"
		if params.Order == "DESC" {
			order = "DESC"
		}
		query += fmt.Sprintf(" ORDER BY `%s`.`%s` %s", s.Table, params.Sort, order)
	}
	if params.Limit > 0 {
		query += fmt.Sprintf(" LIMIT %d", params.Limit)
	}
	if params.Offset > 0 {
		query += fmt.Sprintf(" OFFSET %d", params.Offset)
	}
	return query, whereStatements, sqlParams, nil
}

func (s SqlTable[Filter]) totalQuery(whereStatements []string) string {
	from := s.Table
	if len(whereStatements) > 0 {
		from = fmt.Sprintf("(SELECT `%s` FROM %s WHERE %s)", s.IDField, s.Table, strings.Join(whereStatements, " AND "))
	}
	return fmt.Sprintf("SELECT COUNT(*) as total FROM %s", from)
}

func (s *SqlStore[NewModel, Model, Filter]) total(ctx context.Context, whereStatements []string, sqlParams map[string]any) (int, error) {
	totalSettings := struct {
		Total int `db:"total"`
	}{}
	rows, err := s.db.NamedQueryContext(ctx, s.table.totalQuery(whereStatements), sqlParams)
	if err != nil && err != sql.ErrNoRows {
		return 0, err
	}
	defer rows.Close()
	if rows.Next() {
		err := rows.StructScan(&totalSettings)
		if err != nil {
			return 0, err
		}
	}
	return totalSettings.Total, nil
}
