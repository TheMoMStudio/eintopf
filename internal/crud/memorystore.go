//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package crud

import (
	"context"
	"fmt"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

// MemoryStore implements a generic model store in memory.
type MemoryStore[NewModel any, Model Identifiable, Filters any] struct {
	models map[string]*Model
	m      *sync.RWMutex

	idGenerator       IDGeneratorFunc[NewModel]
	modelFromNewModel NewModelFunc[NewModel, Model]
}

// NewModelFunc take a new model and an id and returns a model.
type NewModelFunc[NewModel any, Model Identifiable] func(newModel *NewModel, id string) *Model

// IDGeneratorFunc takes a new model and returns generates an identifier for it.
type IDGeneratorFunc[NewModel any] func(newModel *NewModel) string

// NewMemoryStore returns a new memory store.
func NewMemoryStore[NewModel any, Model Identifiable, Filters any](modelFromNewModel NewModelFunc[NewModel, Model], idGenerator IDGeneratorFunc[NewModel]) *MemoryStore[NewModel, Model, Filters] {
	return &MemoryStore[NewModel, Model, Filters]{
		models: make(map[string]*Model),
		m:      &sync.RWMutex{},

		modelFromNewModel: modelFromNewModel,
		idGenerator:       idGenerator,
	}
}

// Create adds a new model to the store.
func (m *MemoryStore[NewModel, Model, Filters]) Create(ctx context.Context, newModel *NewModel) (*Model, error) {
	m.m.Lock()
	defer m.m.Unlock()

	id := strconv.Itoa(len(m.models))
	if m.idGenerator != nil {
		id = m.idGenerator(newModel)
	}
	model := m.modelFromNewModel(newModel, id)
	id = (*model).Identifier()

	// Store a copy of the model.
	copyModel := *model
	m.models[id] = &copyModel

	// Return a copy of the model.
	copyModel2 := *model
	return &copyModel2, nil
}

// Update updates an existing model. It returns an error, if the model did not
// exist before.
func (m *MemoryStore[NewModel, Model, Filters]) Update(ctx context.Context, model *Model) (*Model, error) {
	m.m.Lock()
	defer m.m.Unlock()

	_, ok := m.models[(*model).Identifier()]
	if !ok {
		return nil, fmt.Errorf("model does not exist: %s", (*model).Identifier())
	}
	// Store a copy of the model.
	copyModel := *model
	m.models[(*model).Identifier()] = &copyModel

	// Return a copy of the model.
	copyModel2 := *model
	return &copyModel2, nil
}

// Delete deletes an existing model. It returns an error if the model did not
// exist before.
func (m *MemoryStore[NewModel, Model, Filters]) Delete(ctx context.Context, id string) error {
	m.m.Lock()
	defer m.m.Unlock()

	_, ok := m.models[id]
	if !ok {
		return fmt.Errorf("model does not exist: %s", id)
	}

	delete(m.models, id)
	return nil
}

// FindByID returns the model with the given id. It returns nil, when the model
// was not found.
func (m *MemoryStore[NewModel, Model, Filters]) FindByID(ctx context.Context, id string) (*Model, error) {
	m.m.RLock()
	defer m.m.RUnlock()

	model, ok := m.models[id]
	if !ok {
		return nil, nil
	}
	return model, nil
}

var zeroVal = reflect.Value{}

// Find returns a list of models.
func (m *MemoryStore[NewModel, Model, Filters]) Find(ctx context.Context, params *FindParams[Filters]) ([]*Model, int, error) {
	m.m.RLock()
	defer m.m.RUnlock()

	models := []*Model{}

MODELS:
	for _, model := range m.models {
		if params != nil && params.Filters != nil {
			filtersReflected := reflect.Indirect(reflect.ValueOf(params.Filters))
			// Iterate over all filter fields. When the filter is not nil, try
			// to match it to the coresponding model field.
			for i := 0; i < filtersReflected.NumField(); i++ {
				filterField := filtersReflected.Field(i)
				filterValues := []interface{}{filterField.Interface()}
				switch filterField.Kind() {
				case reflect.Ptr:
					// When the filter was not set it has a nil pointer as a value.
					// In that case the field gets ignored.
					if filterField.Elem() == zeroVal {
						continue
					}
					filterValues = []interface{}{filterField.Elem().Interface()}
				case reflect.Array, reflect.Slice:
					// When the filter was not set it has 0 length.
					// In that case the field gets ignored.
					if filterField.Len() == 0 {
						continue
					}

					filterValues = []interface{}{}
					for i := 0; i < filterField.Len(); i++ {
						filterValues = append(filterValues, filterField.Index(i).Interface())
					}
				}

				fieldName, comparator := parseFieldName(filtersReflected.Type().Field(i).Name)

				modelReflected := reflect.Indirect(reflect.ValueOf(model))
				modelField := modelReflected.FieldByName(fieldName)
				if modelField == zeroVal {
					return nil, 0, fmt.Errorf("invalid filter: %s", fieldName)
				}

				modelValues := []interface{}{modelField.Interface()}
				switch modelField.Kind() {
				case reflect.Ptr:
					if modelField.Elem() == zeroVal {
						modelValues = []interface{}{nil}
					} else {
						modelValues = []interface{}{modelField.Elem().Interface()}
					}
				case reflect.Array, reflect.Slice:
					// When the filter was not set it has a nil pointer as a value.
					// In that case the field gets ignored.
					if modelField.Len() == 0 {
						modelValues = []interface{}{}
					} else {
						modelValues = []interface{}{}
						for i := 0; i < modelField.Len(); i++ {
							modelValues = append(modelValues, modelField.Index(i).Interface())
						}
					}
				}

				match, err := matchModelAndFilterValues(comparator, modelValues, filterValues)
				if err != nil {
					return nil, 0, fmt.Errorf("invalid filter: %s: %s", filtersReflected.Type().Field(i).Name, err)
				}
				if !match {
					continue MODELS
				}
			}
		}
		models = append(models, model)
	}

	sortField := "ID" // TODO: default sort field from config
	sortOrder := OrderAsc
	start := 0
	end := len(models)
	if params != nil {
		if params.Offset > 0 {
			start = int(params.Offset)
		}
		if params.Limit > 0 {
			end = int(params.Offset + params.Limit)
			if end > len(models) {
				end = len(models)
			}
		}
		if params.Sort != "" {
			sortField = strings.Title(params.Sort)
		}
		if params.Order != "" {
			sortOrder = params.Order
		}
	}

	errorSortField := false

	switch sortOrder {
	case OrderAsc:
		sort.Slice(models, func(i, j int) bool {
			modelsIField := reflect.Indirect(reflect.ValueOf(models[i])).FieldByName(sortField)
			if modelsIField == zeroVal {
				errorSortField = true
				return false
			}

			modelsJField := reflect.Indirect(reflect.ValueOf(models[j])).FieldByName(sortField)
			if modelsJField == zeroVal {
				errorSortField = true
				return false
			}

			switch modelsIField.Interface().(type) {
			case string:
				return modelsIField.String() < modelsJField.String()
			case int:
				return modelsIField.Int() < modelsJField.Int()
			case time.Time:
				vI := modelsIField.Interface().(time.Time)
				vJ := modelsJField.Interface().(time.Time)
				return vI.Before(vJ)
			}
			return false
		})
	case OrderDesc:
		sort.Slice(models, func(i, j int) bool {
			modelsIField := reflect.Indirect(reflect.ValueOf(models[i])).FieldByName(sortField)
			if modelsIField == zeroVal {
				errorSortField = true
				return false
			}

			modelsJField := reflect.Indirect(reflect.ValueOf(models[j])).FieldByName(sortField)
			if modelsJField == zeroVal {
				errorSortField = true
				return false
			}

			switch modelsIField.Interface().(type) {
			case string:
				return modelsIField.String() > modelsJField.String()
			case int:
				return modelsIField.Int() > modelsJField.Int()
			case time.Time:
				vI := modelsIField.Interface().(time.Time)
				vJ := modelsJField.Interface().(time.Time)
				return vI.After(vJ)
			}
			return false
		})
	}
	if errorSortField && sortField != "ID" {
		return nil, 0, fmt.Errorf("invalid sort field: %s", sortField)
	}

	return models[start:end], len(models), nil
}

func parseFieldName(fieldName string) (string, string) {
	if strings.HasSuffix(fieldName, "Before") {
		return strings.TrimSuffix(fieldName, "Before"), "timeBefore"
	}
	if strings.HasSuffix(fieldName, "After") {
		return strings.TrimSuffix(fieldName, "After"), "timeAfter"
	}
	if strings.HasPrefix(fieldName, "Not") {
		return strings.TrimPrefix(fieldName, "Not"), "neq"
	}
	if strings.HasPrefix(fieldName, "Like") {
		return strings.TrimPrefix(fieldName, "Like"), "like"
	}
	return fieldName, "eq"
}

func matchModelAndFilterValues(comparator string, modelValues, filterValues []interface{}) (bool, error) {
	match := false
	for _, modelValue := range modelValues {
		if match {
			break
		}
		for _, filterValue := range filterValues {
			switch comparator {
			case "eq":
				if reflect.DeepEqual(filterValue, modelValue) {
					match = true
					break
				}
			case "neq":
				if !reflect.DeepEqual(filterValue, modelValue) {
					match = true
					break
				}
			case "like":
				fv, ok := filterValue.(string)
				if !ok {
					return false, fmt.Errorf("filter field not a time value")
				}
				mv, ok := modelValue.(string)
				if !ok {
					return false, fmt.Errorf("model field not a time value")
				}
				if strings.Contains(mv, fv) {
					match = true
					break
				}
			case "timeBefore":
				fv, ok := filterValue.(time.Time)
				if !ok {
					return false, fmt.Errorf("filter field not a time value")
				}
				mv, ok := modelValue.(time.Time)
				if !ok {
					return false, fmt.Errorf("model field not a time value")
				}
				if mv.Before(fv) {
					match = true
					break
				}
			case "timeAfter":
				fv, ok := filterValue.(time.Time)
				if !ok {
					return false, fmt.Errorf("filter field not a time value")
				}
				mv, ok := modelValue.(time.Time)
				if !ok {
					return false, fmt.Errorf("model field not a time value")
				}
				if mv.After(fv) {
					match = true
					break
				}
			}
		}
	}
	return match, nil
}
