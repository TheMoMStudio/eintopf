// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package crud

import (
	"errors"
	"net/http"
	"strconv"

	"eintopf.info/internal/xhttp"
	"github.com/go-chi/chi/v5"
)

type Handler[NewModel any, Model Identifiable, Filters any] struct {
	storer Storer[NewModel, Model, Filters]
}

func NewHandler[NewModel any, Model Identifiable, Filters any](storer Storer[NewModel, Model, Filters]) *Handler[NewModel, Model, Filters] {
	return &Handler[NewModel, Model, Filters]{storer: storer}
}

func (h *Handler[NewModel, Model, Filters]) Create(w http.ResponseWriter, r *http.Request) {
	newModel, err := xhttp.ReadJSON[NewModel](r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}
	model, err := h.storer.Create(r.Context(), newModel)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}
	xhttp.WriteJSON(r.Context(), w, model)
}

func (h *Handler[NewModel, Model, Filters]) Find(w http.ResponseWriter, r *http.Request) {
	params, err := readFindRequest[Filters](r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
		return
	}

	models, total, err := h.storer.Find(r.Context(), params)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}

	w.Header().Add("Access-Control-Expose-Headers", "X-Total-Count")
	w.Header().Add("X-Total-Count", strconv.Itoa(total))
	xhttp.WriteJSON(r.Context(), w, models)
}

func readFindRequest[Filters any](r *http.Request) (*FindParams[Filters], error) {
	params := &FindParams[Filters]{}
	var err error

	params.Offset, err = xhttp.ReadQueryInt64(r, "offset")
	if err != nil {
		return nil, err
	}
	params.Limit, err = xhttp.ReadQueryInt64(r, "limit")
	if err != nil {
		return nil, err
	}

	params.Sort, err = xhttp.ReadQueryString(r, "sorting")
	if err != nil {
		return nil, err
	}
	order, err := xhttp.ReadQueryString(r, "order")
	if err != nil {
		return nil, err
	}
	params.Order = SortOrder(order)

	filters := new(Filters)
	err = xhttp.ReadQueryJson(r, "filters", filters)
	if err != nil {
		return nil, err
	}
	params.Filters = filters

	return params, nil
}

func (h *Handler[NewModel, Model, Filters]) FindByID(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	if id == "" {
		xhttp.WriteBadRequest(r.Context(), w, errors.New("empty id"))
		return
	}

	model, err := h.storer.FindByID(r.Context(), id)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}

	xhttp.WriteJSON(r.Context(), w, model)
}

func (h *Handler[NewModel, Model, Filters]) Update(w http.ResponseWriter, r *http.Request) {
	model, err := xhttp.ReadJSON[Model](r)
	if err != nil {
		xhttp.WriteBadRequest(r.Context(), w, err)
	}

	model, err = h.storer.Update(r.Context(), model)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}

	xhttp.WriteJSON(r.Context(), w, model)
}

func (h *Handler[NewModel, Model, Filters]) Delete(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	if id == "" {
		xhttp.WriteBadRequest(r.Context(), w, errors.New("empty id"))
		return
	}

	err := h.storer.Delete(r.Context(), id)
	if err != nil {
		xhttp.WriteError(r.Context(), w, err)
		return
	}

	w.Write([]byte{})
}
