//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package crud_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"eintopf.info/internal/crud"
	"eintopf.info/test"
	"github.com/google/go-cmp/cmp"
	"github.com/jmoiron/sqlx"
)

var mymodelTable = crud.SqlTable[MyModelFilters]{
	Table:          "mymodel",
	Fields:         []string{"id", "name", "number", "time", "pointer", "from"},
	SortableFields: []string{"id", "name", "number", "time", "pointer", "from"},
	IDField:        "id",
}

func createTable(db *sqlx.DB) error {
	_, err := db.Exec(fmt.Sprintf(`
        CREATE TABLE IF NOT EXISTS mymodel (
            id varchar(32) NOT NULL PRIMARY KEY UNIQUE,
            name VARCHAR(128) NOT NULL,
            number INT NOT NULL,
            time TIMESTAMP NOT NULL,
            pointer VARCHAR(128),
            %s VARCHAR(64)
        );
    `, "`from`"))
	return err
}

func TestSQLStoreBasics(t *testing.T) {
	db, cleanupDB, err := test.CreateSqliteTestDB("TestSQLStoreBasics")
	if err != nil {
		t.Fatal(err)
	}
	defer func() { cleanupDB() }()
	err = createTable(db)
	if err != nil {
		t.Fatal(err)
	}

	store := crud.NewSqlStore(db, mymodelTable, modelFromNewModel, nil)
	foo, err := store.Create(context.Background(), &NewMyModel{Name: "foo", From: "bar"})
	if err != nil {
		t.Fatal(err)
	}

	foo.Name = "bar"
	bar, err := store.Update(context.Background(), foo)
	if err != nil {
		t.Fatal(err)
	}
	if foo.ID != bar.ID {
		t.Fatal("id changed")
	}

	bar2, err := store.FindByID(context.Background(), bar.ID)
	if err != nil {
		t.Fatal(err)
	}

	if diff := cmp.Diff(bar, bar2); diff != "" {
		t.Fatalf("bar and bar2 mismatch (-want +got):\n%s", diff)
	}

	err = store.Delete(context.Background(), bar.ID)
	if err != nil {
		t.Fatal(err)
	}
}

func TestSQLStoreFind(t *testing.T) {
	db, cleanupDB, err := test.CreateSqliteTestDB("TestSQLStoreBasics")
	if err != nil {
		t.Fatal(err)
	}
	defer func() { cleanupDB() }()
	err = createTable(db)
	if err != nil {
		t.Fatal(err)
	}

	store := crud.NewSqlStore(db, mymodelTable, modelFromNewModel, nil)

	date1 := time.Date(2022, 10, 5, 12, 0, 0, 0, time.UTC)
	foo, err := store.Create(context.Background(), &NewMyModel{
		Name:    "foo",
		Number:  1,
		Time:    date1,
		Pointer: strptr("foo"),
		From:    "foo",
	})
	if err != nil {
		t.Fatal(err)
	}

	date2 := time.Date(2023, 10, 6, 12, 0, 0, 0, time.UTC)
	bar, err := store.Create(context.Background(), &NewMyModel{
		Name:   "bar",
		Number: 2,
		Time:   date2,
		From:   "bar",
	})
	if err != nil {
		t.Fatal(err)
	}

	tests := []struct {
		name   string
		params *crud.FindParams[MyModelFilters]
		total  int
		models []*MyModel
	}{
		{name: "NoParams", params: nil, total: 2, models: []*MyModel{foo, bar}},
		{
			name: "NotFilter",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{NotID: strptr(foo.ID)},
			},
			total:  1,
			models: []*MyModel{bar},
		},
		{
			name: "StringFilter",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{Name: strptr("foo")},
			},
			total:  1,
			models: []*MyModel{foo},
		},
		{
			name: "LikeStringFilter",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{LikeName: strptr("f")},
			},
			total:  1,
			models: []*MyModel{foo},
		},
		{
			name: "IntFilter",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{Number: intptr(2)},
			},
			total:  1,
			models: []*MyModel{bar},
		},
		{
			name: "PointerFilter",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{Pointer: strptr("foo")},
			},
			total:  1,
			models: []*MyModel{foo},
		},
		{
			name: "TimeFilter",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{Time: timeptr(date1)},
			},
			total:  1,
			models: []*MyModel{foo},
		},
		{
			name: "TimeAfter",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{TimeAfter: timeptr(date1)},
			},
			total:  1,
			models: []*MyModel{bar},
		},
		{
			name: "TimeBefore",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{TimeBefore: timeptr(date2)},
			},
			total:  1,
			models: []*MyModel{foo},
		},
		{
			name: "MultipleFilter1",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{LikeName: strptr("f"), Number: intptr(2)},
			},
			total:  0,
			models: []*MyModel{},
		},
		{
			name: "MultipleFilter2",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{LikeName: strptr("f"), Number: intptr(1)},
			},
			total:  1,
			models: []*MyModel{foo},
		},
		{
			name: "FilterMysqlKeyword",
			params: &crud.FindParams[MyModelFilters]{
				Filters: &MyModelFilters{From: strptr("foo")},
			},
			total:  1,
			models: []*MyModel{foo},
		},
		{
			name: "SortASC",
			params: &crud.FindParams[MyModelFilters]{
				Sort:  "name",
				Order: crud.OrderAsc,
			},
			total:  2,
			models: []*MyModel{bar, foo},
		},
		{
			name: "SortDesc",
			params: &crud.FindParams[MyModelFilters]{
				Sort:  "name",
				Order: crud.OrderDesc,
			},
			total:  2,
			models: []*MyModel{foo, bar},
		},
		{
			name: "SortTimeAsc",
			params: &crud.FindParams[MyModelFilters]{
				Sort:  "time",
				Order: crud.OrderAsc,
			},
			total:  2,
			models: []*MyModel{foo, bar},
		},
		{
			name: "SortTimeDesc",
			params: &crud.FindParams[MyModelFilters]{
				Sort:  "time",
				Order: crud.OrderDesc,
			},
			total:  2,
			models: []*MyModel{bar, foo},
		},
		{
			name: "Paginate",
			params: &crud.FindParams[MyModelFilters]{
				Sort:   "name",
				Order:  crud.OrderDesc,
				Offset: 1,
				Limit:  1,
			},
			total:  2,
			models: []*MyModel{bar},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			models, total, err := store.Find(context.Background(), tc.params)
			if err != nil {
				t.Fatal(err)
			}
			if total != tc.total {
				t.Errorf("total mismatch: want: %d, got: %d", tc.total, total)
			}
			if diff := cmp.Diff(tc.models, models); diff != "" {
				t.Errorf("models mismatch (-want +got):\n%s", diff)
			}
		})
	}
}
