//
// SPDX-FileCopyrightText: 2024 Klasse & Method - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package template

import (
	"context"
	"errors"
	"fmt"
	"html/template"
	"io"
	"io/fs"
	"strings"
)

func NewEngine(themes []fs.FS, data map[string]any, funcs template.FuncMap) *Engine {
	return &Engine{
		fs:    mergeFS(themes),
		data:  data,
		funcs: funcs,
	}
}

type Engine struct {
	// fs contains templates from multiple themes. All templates should be in
	// the top level "templates" folder.
	fs    mergeFS
	data  map[string]any
	funcs template.FuncMap
}

// Render executes the templates with data and func to the writer.
// This function can be io and cpu intensive and therefore can abort when the
// context was canceled.
func (t *Engine) Render(ctx context.Context, w io.Writer, templates []string, data map[string]any, funcs template.FuncMap, includeHelper bool) error {
	if len(templates) < 1 {
		return fmt.Errorf("render: at least one template required")
	}

	// The first template defines the template name.
	tpl := template.New(templates[0])

	if funcs == nil {
		funcs = make(template.FuncMap)
	}
	for k, v := range t.funcs {
		funcs[k] = v
	}
	tpl.Funcs(funcs)

	for i := range templates {
		templates[i] = fmt.Sprintf("templates/%s.tpl", templates[i])
	}
	if includeHelper {
		// Add all helper templates.
		fs.WalkDir(t.fs, "templates/helper", func(path string, d fs.DirEntry, err error) error {
			if strings.HasSuffix(path, ".tpl") {
				templates = append(templates, path)
			}
			return nil
		})
	}

	for _, template := range templates {
		if errors.Is(ctx.Err(), context.Canceled) {
			return fmt.Errorf("render: %s", ctx.Err())
		}

		files, err := t.fs.OpenAll(template)
		if err != nil {
			return err
		}
		for _, f := range files {
			data, err := io.ReadAll(f)
			if err != nil {
				return fmt.Errorf("render: read %s: %s", template, err)
			}

			tpl, err = tpl.Parse(string(data))
			if err != nil {
				return fmt.Errorf("render: parse %s: %s", template, err)
			}
		}
	}

	if errors.Is(ctx.Err(), context.Canceled) {
		return fmt.Errorf("render: %s", ctx.Err())
	}

	if data == nil {
		data = make(map[string]interface{})
	}
	for k, v := range t.data {
		data[k] = v
	}

	if err := tpl.Execute(w, data); err != nil {
		return fmt.Errorf("render: %s: %s", templates[0], err)
	}
	return nil
}

func (t *Engine) FS() fs.FS {
	return t.fs
}

// mergeFS merges a list of fs into one. The first fs has the highest priority
// and the last fs has the lowest priority.
type mergeFS []fs.FS

// Open the name from the last fs, where the file was found.
func (m mergeFS) Open(name string) (fs.File, error) {
	for i := len(m) - 1; i >= 0; i-- {
		f, err := m[i].Open(name)
		if err != nil {
			// Try name in next fs.
			continue
		}
		if _, err := f.Stat(); err != nil {
			// Try name in next fs.
			continue
		}
		return f, nil
	}
	return nil, fs.ErrNotExist
}

// OpenAll returns all files from each fs where the file exists. The files are
// in the same order as the file systems.
func (m mergeFS) OpenAll(name string) ([]fs.File, error) {
	files := []fs.File{}
	for _, fs := range m {
		f, err := fs.Open(name)
		if err != nil {
			// Try name in next fs.
			continue
		}
		if _, err := f.Stat(); err != nil {
			// Try name in next fs.
			continue
		}
		files = append(files, f)
	}
	if len(files) == 0 {
		return nil, fs.ErrNotExist
	}
	return files, nil
}
