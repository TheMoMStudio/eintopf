//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package template

import (
	"bytes"
	"context"
	"os"
	"testing"
)

func TestRenderContextCancel(t *testing.T) {
	themeFS := mergeFS{}
	themeFS = append(themeFS, os.DirFS("testdata/themes/foo"))
	themeFS = append(themeFS, os.DirFS("testdata/themes/bar"))
	e := &Engine{fs: themeFS}

	w := &bytes.Buffer{}
	ctx, cancelFunc := context.WithCancel(context.Background())
	func() { cancelFunc() }()

	err := e.Render(ctx, w, []string{"bar", "foo", "foo1", "foo2", "foo3"}, nil, nil, true)
	if err == nil || err.Error() != "render: context canceled" {
		t.Errorf("err should be 'render: context canceled', got %s", err)
	}
}
