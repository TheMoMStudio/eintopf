//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package xhttptest

import (
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

// HttpTest defines an http test case.
type HttpTest struct {
	Name        string
	PreTest     func() error
	URI         string
	Method      string
	Headers     map[string]string
	Body        string
	WantStatus  int
	WantBody    string
	WantHeaders map[string]string
}

// RunHttpTests runs a set of http tests on the given test server.
func RunHttpTests(t *testing.T, testServer *httptest.Server, tests []HttpTest) {
	client := &http.Client{}

	for _, test := range tests {
		t.Run(test.Name, func(tt *testing.T) {
			if test.PreTest != nil {
				err := test.PreTest()
				if err != nil {
					tt.Errorf("PreTest(): %s", err)
				}
			}

			method := "GET"
			if test.Method != "" {
				method = test.Method
			}

			req, err := http.NewRequest(method, testServer.URL+test.URI, strings.NewReader(test.Body))
			if err != nil {
				tt.Errorf("http.NewRequest(%s, %s, nil): %s", method, testServer.URL+test.URI, err)
			}
			for key, value := range test.Headers {
				req.Header.Set(key, value)
			}

			resp, err := client.Do(req)
			if err != nil {
				tt.Errorf("client.Do(req): %s", err)
			}
			if resp != nil {
				defer resp.Body.Close()
			}

			if test.WantStatus != resp.StatusCode {
				tt.Errorf("status code != %d: %d", test.WantStatus, resp.StatusCode)
			}

			if test.WantBody != "" {
				body, err := io.ReadAll(resp.Body)
				if err != nil {
					tt.Errorf("io.ReadAll: %s", err)
				}
				if test.WantBody != string(body) {
					tt.Errorf("invalid body:\nwant: %s\ngot:  %s", test.WantBody, body)
				}
			}
			if test.WantHeaders != nil {
				for key, value := range test.WantHeaders {
					gotValue := resp.Header.Get(key)
					if value != gotValue {
						tt.Errorf("invalid header %s:\nwant: %s\ngot:  %s", key, value, gotValue)
					}
				}
			}
		})
	}
}
