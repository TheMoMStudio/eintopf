//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package xhttptest

import (
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi/v5"
)

// TestRouter tests a chi Router with a set of http tests.
func TestRouter(t *testing.T, router func(r chi.Router), tests []HttpTest) {
	r := chi.NewRouter()
	r.Route("/", router)

	testServer := httptest.NewServer(r)
	defer testServer.Close()

	RunHttpTests(t, testServer, tests)
}
