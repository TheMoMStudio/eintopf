//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package xhttptest

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func okHandler() http.HandlerFunc {
	fn := func(w http.ResponseWriter, req *http.Request) {
		w.WriteHeader(http.StatusOK)
	}
	return http.HandlerFunc(fn)
}

func TestMiddleware(t *testing.T, middleware func(next http.Handler) http.Handler, tests []HttpTest) {
	testServer := httptest.NewServer(middleware(okHandler()))
	defer testServer.Close()

	RunHttpTests(t, testServer, tests)
}
