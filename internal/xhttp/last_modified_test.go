// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package xhttp_test

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"eintopf.info/internal/xhttp"
)

func RequestIsNotModified(t *testing.T) {
	tests := []struct {
		lastModified time.Time
		header       string
		modified     bool
	}{
		{
			lastModified: time.Time{},
			header:       "",
			modified:     true,
		},
		{
			lastModified: time.Now().Add(time.Second),
			header:       time.Now().UTC().Format(http.TimeFormat),
			modified:     true,
		},
		{
			lastModified: time.Now(),
			header:       time.Now().Add(time.Second).UTC().Format(http.TimeFormat),
			modified:     false,
		},
	}
	for i, test := range tests {
		r := httptest.NewRequest("GET", "", nil)
		r.Header.Add("If-Modified-Since", test.header)
		if xhttp.RequestIsNotModified(r, test.lastModified) != test.modified {
			t.Errorf("%d: request should be %t", i, test.modified)
		}
	}
}

type lastModifiedCache struct {
	lastModified time.Time
}

func (c *lastModifiedCache) LastModified() time.Time { return c.lastModified }

func RequestLastModified(t *testing.T) {
	cache := &lastModifiedCache{lastModified: time.Now()}

	lastModified := time.Now().Add(time.Hour).UTC().Format(http.TimeFormat)
	handler := func(w http.ResponseWriter, r *http.Request) {
		if lastModified != "" {
			w.Header().Set("LastModified", lastModified)
		}
	}
	testServer := httptest.NewServer(xhttp.LastModified(cache)(http.HandlerFunc(handler)))
	defer testServer.Close()

	resp, err := http.Get(testServer.URL)
	if err != nil {
		t.Fatal(err)
	}
	if resp.Header.Get("LastModified") != lastModified {
		t.Errorf("Last-Modified should be taken from request, got: %s", resp.Header.Get("LastModified"))
	}

	lastModified = time.Now().Add(-time.Hour * 2).UTC().Format(http.TimeFormat)

	resp, err = http.Get(testServer.URL)
	if err != nil {
		t.Fatal(err)
	}
	if resp.Header.Get("LastModified") != cache.lastModified.UTC().Format(http.TimeFormat) {
		t.Errorf("Last-Modified should be taken from middleware, got: %s", resp.Header.Get("LastModified"))
	}

	lastModified = ""

	resp, err = http.Get(testServer.URL)
	if err != nil {
		t.Fatal(err)
	}
	if resp.Header.Get("LastModified") != cache.lastModified.UTC().Format(http.TimeFormat) {
		t.Errorf("Last-Modified should be taken from middleware, got: %s", resp.Header.Get("LastModified"))
	}
}
