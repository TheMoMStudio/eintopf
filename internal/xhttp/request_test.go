//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package xhttp_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"eintopf.info/internal/xhttp"
)

func TestRequestIDMiddleware(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := xhttp.RequestIDMiddleware(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		val := r.Context().Value("requestID")
		if val == nil {
			t.Error("requestID not present")
		}
	}))
	handler.ServeHTTP(rr, req)
}
