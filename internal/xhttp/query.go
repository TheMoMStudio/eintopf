//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package xhttp

import (
	"encoding/json"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/go-chi/chi/v5"
)

func ReadQueryString(r *http.Request, name string) (string, error) {
	value := r.URL.Query().Get(name)
	if value == "" {
		return "", nil
	}
	unescaped, err := url.QueryUnescape(value)
	if err != nil {
		return "", err
	}
	return unescaped, nil
}

func ReadQueryStringArray(r *http.Request, name string) ([]string, error) {
	values := r.URL.Query()[name]
	if values == nil {
		return nil, nil
	}
	unescaped := []string{}
	for _, v := range values {
		unescapedValue, err := url.QueryUnescape(v)
		if err != nil {
			return nil, err
		}
		unescaped = append(unescaped, unescapedValue)
	}
	return unescaped, nil
}

func ReadQueryInt(r *http.Request, name string) (int, error) {
	value := r.URL.Query().Get(name)
	if value == "" {
		return 0, nil
	}
	unescaped, err := url.QueryUnescape(value)
	if err != nil {
		return 0, err
	}
	return strconv.Atoi(unescaped)
}

func ReadQueryBool(r *http.Request, name string) (*bool, error) {
	value := r.URL.Query().Get(name)
	if value == "" {
		return nil, nil
	}
	unescaped, err := url.QueryUnescape(value)
	if err != nil {
		return nil, err
	}
	boolValue, err := strconv.ParseBool(unescaped)
	if err != nil {
		return nil, err
	}
	return &boolValue, nil
}

func ReadQueryInt64(r *http.Request, name string) (int64, error) {
	value := r.URL.Query().Get(name)
	if value == "" {
		return 0, nil
	}
	unescaped, err := url.QueryUnescape(value)
	if err != nil {
		return 0, err
	}
	return strconv.ParseInt(unescaped, 10, 64)
}

func ReadQueryJson(r *http.Request, name string, v interface{}) error {
	value := r.URL.Query().Get(name)
	if value == "" {
		return nil
	}
	unescaped, err := url.QueryUnescape(value)
	if err != nil {
		return err
	}

	return json.Unmarshal([]byte(unescaped), v)
}

func ReadQueryTime(r *http.Request, name, layout string, defaultTime time.Time, tz *time.Location) (time.Time, error) {
	valueRaw := r.URL.Query().Get(name)
	if valueRaw == "" {
		return defaultTime, nil
	}
	value, err := time.ParseInLocation(layout, valueRaw, tz)
	if err != nil {
		return time.Time{}, err
	}
	return value, nil
}

func ReadParamInt(r *http.Request, name string) (int, error) {
	value := chi.URLParam(r, name)
	if value == "" {
		return 0, nil
	}
	unescaped, err := url.QueryUnescape(value)
	if err != nil {
		return 0, err
	}
	return strconv.Atoi(unescaped)
}
