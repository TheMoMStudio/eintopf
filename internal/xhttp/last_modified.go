//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package xhttp

import (
	"net/http"
	"time"
)

type LastModifiedCache interface {
	// LastModified indicates the last time an entity was updated.
	LastModified() time.Time
}

// LastModified is a middleware, that implements a http cache using the
// Last-Modified header. It takes a list of LastModifiedCache always using the
// one that was last modified.
func LastModified(modifications ...LastModifiedCache) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			lastModified := modifications[0].LastModified()
			for _, l := range modifications {
				if l.LastModified().After(lastModified) {
					lastModified = l.LastModified()
				}
			}
			if RequestIsNotModified(r, lastModified) {
				WriteNotModified(w, lastModified)
				return
			}

			w.Header().Add("Cache-Control", "public, no-cache")
			next.ServeHTTP(w, r)

			// Set the Last-Modified header at the very end to be able to
			// compare it with Last-Modified set during the request.
			lastModRequest, err := http.ParseTime(r.Header.Get("Last-Modified"))
			if err != nil {
				return
			}
			// Either take last modified from this middleware or from the
			// request, which ever is the latest.
			if !lastModified.IsZero() && lastModified.After(lastModRequest) {
				w.Header().Del("Last-Modified")
				w.Header().Add("Last-Modified", lastModified.UTC().Format(http.TimeFormat))
			}
		})
	}
}

type lastModifiedAt struct{ lastModifiedAt time.Time }

func (l lastModifiedAt) LastModified() time.Time { return l.lastModifiedAt }

func LastModifiedAt(lastModified time.Time) func(next http.Handler) http.Handler {
	return LastModified(lastModifiedAt{lastModified})
}

// RequestIsNotModified checks for the If-Modified-Since header and compares
// it with the provided lastModified value.
// Returns true, if the header is set and its value is before or equal to the
// lastModified value.
func RequestIsNotModified(r *http.Request, lastModified time.Time) bool {
	if ifModSince, err := http.ParseTime(r.Header.Get("If-Modified-Since")); err == nil {
		// Round to seconds, since http.TimeFormat also has seconds precision.
		lastModified = lastModified.Truncate(time.Second)

		if !lastModified.IsZero() && lastModified.Before(ifModSince) || lastModified.Equal(ifModSince) {
			return true
		}
	}
	return false
}

// WriteNotModified writes 304 status code and sets the Last-Modified header.
func WriteNotModified(w http.ResponseWriter, lastModified time.Time) {
	w.Header().Add("Last-Modified", lastModified.UTC().Format(http.TimeFormat))
	w.WriteHeader(http.StatusNotModified)
}

// WriteWithLastModified wirtes a 200 status code with the given data and sets
// the Last-Modified and Cache-Control headers.
func WriteWithLastModified(w http.ResponseWriter, data []byte, lastModified time.Time) {
	w.Header().Add("Last-Modified", lastModified.UTC().Format(http.TimeFormat))
	w.Header().Add("Cache-Control", "public, no-cache")
	w.Write(data)
}
