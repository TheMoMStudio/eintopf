//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package xhttp

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"eintopf.info/internal/xerror"
)

// Error defines the structure of an error response.
type Error struct {
	// Error message.
	Error string `json:"error"`
}

func writeError(w http.ResponseWriter, statusCode int, msg string) error {
	w.WriteHeader(statusCode)
	data, err := json.Marshal(&Error{msg})
	if err != nil {
		return err
	}

	// Don't cache error responses.
	w.Header().Del("Last-Modified")
	w.Header().Del("Cache-Control")

	w.Write(data)
	return nil
}

func logError(ctx context.Context, err error) {
	log.Printf("requestID=%s err=\"%s\"\n", ctx.Value("requestID"), err.Error())
}

// WriteError writes a generic error. It checks wether it is an xerror.AuthError
// or an xerror.BadInputError. It writes an internal server error for all other
// cases.
func WriteError(ctx context.Context, w http.ResponseWriter, err error) {
	if err == nil {
		return
	}
	if xerror.IsAuthError(err) {
		WriteUnauthorized(w, err)
		return
	}
	if xerror.IsBadInputError(err) {
		WriteBadRequest(ctx, w, err)
		return
	}
	WriteInternalError(ctx, w, err)
}

// swagger:response badRequest
type badRequest struct {
	// in:body
	Body Error
}

// WriteBadRequest writes a http response with status code 400.
func WriteBadRequest(ctx context.Context, w http.ResponseWriter, err error) {
	logError(ctx, err)
	writeError(w, http.StatusBadRequest, err.Error())
}

// swagger:response unauthorizedError
type unauthorizedError struct {
	// in:body
	Body Error
}

// WriteUnauthorized writes a http response with status code 401.
func WriteUnauthorized(w http.ResponseWriter, err error) {
	writeError(w, http.StatusUnauthorized, err.Error())
}

// swagger:response forbiddenError
type forbiddenError struct {
	// in:body
	Body Error
}

// WriteForbidden writes a http response with status code 403.
func WriteForbidden(w http.ResponseWriter, err error) {
	writeError(w, http.StatusForbidden, err.Error())
}

// swagger:response internalError
type internalError struct {
	// in:body
	Body Error
}

// WriteInternalError writes a http response with status code 500.
// Since the error won't be written to the http response, it gets logged
// instead.
func WriteInternalError(ctx context.Context, w http.ResponseWriter, err error) {
	logError(ctx, err)
	writeError(w, http.StatusInternalServerError, "internal error")
}

// swagger:response serviceUnavailable
type serviceUnavailableError struct {
	// in:body
	Body Error
}

// WriteInternalError writes a http response with status code 503.
func WriteServiceUnavailable(w http.ResponseWriter) {
	writeError(w, http.StatusServiceUnavailable, "service unavailable")
}
