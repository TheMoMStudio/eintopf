//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package xhttp

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/go-chi/chi/v5"
)

// Test ReadQueryInt64
func TestReadQueryInt64(t *testing.T) {
	tests := []struct {
		name     string
		query    string
		expected int64
		hasError bool
	}{
		{"empty query", "", 0, false},
		{"valid int64", "param=9223372036854775807", 9223372036854775807, false}, // Max int64
		{"zero value", "param=0", 0, false},
		{"negative int64", "param=-9223372036854775807", -9223372036854775807, false}, // Min int64
		{"invalid int", "param=abc", 0, true},
		{"invalid int 2", "param=.", 0, true},
		{"invalid int 2", "param=.", 0, true},
        {"missing param", "another=value", 0, false},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			req := httptest.NewRequest(http.MethodGet, "/?"+test.query, nil)
			result, err := ReadQueryInt64(req, "param")

			if (err != nil) != test.hasError {
				t.Errorf("TestReadQueryInt64(%s): expected error: %v, got: %v", test.name, test.hasError, err)
			}

			if result != test.expected {
				t.Errorf("TestReadQueryInt64(%s): expected result: %d, got: %d", test.name, test.expected, result)
			}
		})
	}
}

// Test ReadQueryJson
func TestReadQueryJson(t *testing.T) {
	type TestStruct struct {
		Name  string `json:"name"`
		Value int    `json:"value"`
	}

	tests := []struct {
		name     string
		query    string
		expected TestStruct
		hasError bool
	}{
		{"valid JSON", "param=%7B%22name%22%3A%22test%22%2C%22value%22%3A123%7D", TestStruct{"test", 123}, false},
		{"empty query", "", TestStruct{}, false},
		{"invalid JSON", "param=%7B%22name%22%3A%22test%22", TestStruct{}, true}, // Missing closing bracket
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			req := httptest.NewRequest(http.MethodGet, "/?"+test.query, nil)
			var result TestStruct
			err := ReadQueryJson(req, "param", &result)

			if (err != nil) != test.hasError {
				t.Errorf("TestReadQueryJson(%s): expected error: %v, got: %v", test.name, test.hasError, err)
			}

			if !test.hasError && (result != test.expected) {
				t.Errorf("TestReadQueryJson(%s): expected result: %+v, got: %+v", test.name, test.expected, result)
			}
		})
	}
}

// Test ReadQueryTime
func TestReadQueryTime(t *testing.T) {
	layout := "2006-01-02T15:04:05"
	defaultTime := time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC)
	tests := []struct {
		name        string
		query       string
		layout      string
		expected    time.Time
		hasError    bool
	}{
		{"empty query", "", layout, defaultTime, false},
		{"valid time", "param=2023-02-20T10:30:00", layout, time.Date(2023, 2, 20, 10, 30, 0, 0, time.UTC), false},
		{"invalid time format", "param=20-02-2023", layout, time.Time{}, true}, // Incorrect format
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			req := httptest.NewRequest(http.MethodGet, "/?"+test.query, nil)
			result, err := ReadQueryTime(req, "param", test.layout, defaultTime, time.UTC)

			if (err != nil) != test.hasError {
				t.Errorf("TestReadQueryTime(%s): expected error: %v, got: %v", test.name, test.hasError, err)
			}

			if !test.hasError && !result.Equal(test.expected) {
				t.Errorf("TestReadQueryTime(%s): expected result: %v, got: %v", test.name, test.expected, result)
			}
		})
	}
}

// Test ReadParamInt
func TestReadParamInt(t *testing.T) {
	tests := []struct {
		name     string
		param    string
		expected int
		hasError bool
	}{
		{"valid integer", "42", 42, false},
		{"zero value", "0", 0, false},
		{"negative integer", "-15", -15, false},
		{"invalid integer", "abc", 0, true},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			req := httptest.NewRequest(http.MethodGet, "/", nil)
			rctx := chi.NewRouteContext()
			rctx.URLParams.Add("param", test.param)
			req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))

			result, err := ReadParamInt(req, "param")

			// Check for expected error
			if (err != nil) != test.hasError {
				t.Errorf("TestReadParamInt(%s): expected error: %v, got: %v", test.name, test.hasError, err)
			}

			// Check the result
			if result != test.expected {
				t.Errorf("TestReadParamInt(%s): expected result: %d, got: %d", test.name, test.expected, result)
			}
		})
	}
}

func TestReadQueryBool(t *testing.T) {
	tests := []struct {
		name     string
		query    string
		expected *bool
		hasError bool
	}{
		{"empty query", "", nil, false},
		{"valid true", "param=true", boolPtr(true), false},
		{"valid false", "param=false", boolPtr(false), false},
		{"valid 1 (true)", "param=1", boolPtr(true), false},
		{"valid 0 (false)", "param=0", boolPtr(false), false},
		{"valid TRUE (case-insensitive)", "param=TRUE", boolPtr(true), false},
		{"valid FALSE (case-insensitive)", "param=FALSE", boolPtr(false), false},
		{"missing param", "another=value", nil, false},
		{"invalid boolean", "param=abc", nil, true}, 
		{"invalid boolean 2", "param=-2", nil, true}, 
		{"invalid boolean 3", "param=.", nil, true}, 
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			req := httptest.NewRequest(http.MethodGet, "/?"+test.query, nil)
			result, err := ReadQueryBool(req, "param")

			if (err != nil) != test.hasError {
				t.Errorf("TestReadQueryBool(%s): expected error: %v, got: %v", test.name, test.hasError, err)
			}

			if !equalBoolPtrs(result, test.expected) {
				t.Errorf("TestReadQueryBool(%s): expected result: %v, got: %v", test.name, test.expected, result)
			}
		})
	}
}

// Helper function to create a pointer to a bool
func boolPtr(b bool) *bool {
	return &b
}

// Helper function to compare bool pointers
func equalBoolPtrs(a, b *bool) bool {
	if a == nil && b == nil {
		return true
	}
	if a == nil || b == nil {
		return false
	}
	return *a == *b
}


func TestReadQueryInt(t *testing.T) {
	tests := []struct {
		name     string
		query    string
		expected int
		hasError bool
	}{
		{"empty query", "", 0, false},
		{"valid integer", "param=42", 42, false},
		{"zero value", "param=0", 0, false},
		{"negative integer", "param=-15", -15, false},
		{"encoded integer", "param=%31%32%33", 123, false},
		{"missing param", "another=99", 0, false},
		{"invalid integer", "param=abc", 0, true},
		{"invalid integer 2", "param=.", 0, true},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			req := httptest.NewRequest(http.MethodGet, "/?"+test.query, nil)
			result, err := ReadQueryInt(req, "param")

			if (err != nil) != test.hasError {
				t.Errorf("TestReadQueryInt(%s): expected error: %v, got: %v", test.name, test.hasError, err)
			}

			if result != test.expected {
				t.Errorf("TestReadQueryInt(%s): expected result: %d, got: %d", test.name, test.expected, result)
			}
		})
	}
}


// Helper function to compare string slices
func equalStringSlices(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func TestReadQueryStringArray(t *testing.T) {
	tests := []struct {
		name     string
		query    string
		expected []string
		hasError bool
	}{
		{"empty query", "", nil, false},
		{"single value", "param=value", []string{"value"}, false},
		{"multiple values", "param=value1&param=value2", []string{"value1", "value2"}, false},
		{"encoded values", "param=value%201&param=value%202", []string{"value 1", "value 2"}, false},
		{"missing param", "another=value", nil, false},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			req := httptest.NewRequest(http.MethodGet, "/?"+test.query, nil)
			result, err := ReadQueryStringArray(req, "param")

			if (err != nil) != test.hasError {
				t.Errorf("TestReadQueryStringArray(%s): expected error: %v, got: %v", test.name, test.hasError, err)
			}

			if !equalStringSlices(result, test.expected) {
				t.Errorf("TestReadQueryStringArray(%s): expected result: %v, got: %v", test.name, test.expected, result)
			}
		})
	}
}


func TestReadQueryString(t *testing.T) {
    // Create a test table
    tests := []struct {
        name        string
        query       string
        expected    string
        expectError bool
    }{
        {"empty query", "", "", false},
        {"valid query", "param=value", "value", false},
        {"encoded query", "param=value%20with%20spaces", "value with spaces", false},
        {"missing param", "another=value", "", false},
    }

    for _, test := range tests {
        t.Run(test.name, func(t *testing.T) {

            req := httptest.NewRequest(http.MethodGet, "/?"+test.query, nil)

            result, err := ReadQueryString(req, "param")

            if (err != nil) != test.expectError {
                t.Errorf("TestReadQueryString(%s): expected error: %v, got: %v", test.name, test.expectError, err)
            }

            if result != test.expected {
                t.Errorf("TestReadQueryString(%s): expected result: %s, got: %s", test.name, test.expected, result)
            }
        })
    }
}
