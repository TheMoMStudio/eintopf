//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package xhttp

import (
	"context"
	"log"
	"net/http"

	"github.com/google/uuid"
)

// RequestID returns a new http middleware, that adds a unique requestID to the request context.
func RequestIDMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		requestID, err := uuid.NewRandom()
		if err != nil {
			log.Printf("failed to create requestID: %s", err)
		}
		ctx := context.WithValue(r.Context(), "requestID", requestID)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// swagger:response emptyResponse
type emptyResponse struct {
}
