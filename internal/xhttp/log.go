//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package xhttp

import (
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5/middleware"
)

// LogMiddleware returns a new http middleware, that logs every request.
func LogMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ww := middleware.NewWrapResponseWriter(w, r.ProtoMajor)

		t1 := time.Now()
		defer func() {
			log.Printf(
				"method=%s status=%d size=%d duration=%s requestID=%s url=%s\n",
				r.Method,
				ww.Status(),
				ww.BytesWritten(),
				time.Since(t1).String(),
				r.Context().Value("requestID"),
				r.URL.String(),
			)
		}()

		next.ServeHTTP(ww, r)
	})
}
