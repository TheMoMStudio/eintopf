//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package xhttp_test

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"eintopf.info/internal/xhttp"
)

type responseWriter func(w http.ResponseWriter, err error)

func TestResponseWriters(t *testing.T) {
	tests := []struct {
		description    string
		responseWriter responseWriter
		expectedBody   string
		expectedCode   int
	}{
		{
			description:    "Unauthorized",
			responseWriter: xhttp.WriteUnauthorized,
			expectedBody:   `{"error":"an error"}`,
			expectedCode:   401,
		},
	}

	for _, tc := range tests {
		t.Run(tc.description, func(tt *testing.T) {
			testHandler := func() http.HandlerFunc {
				fn := func(w http.ResponseWriter, req *http.Request) {
					tc.responseWriter(w, fmt.Errorf("an error"))
				}
				return http.HandlerFunc(fn)
			}

			ts := httptest.NewServer(testHandler())
			defer ts.Close()

			res, err := http.Get(ts.URL)
			if err != nil {
				tt.Fatalf("expected no error from http.Get. got %s", err)
			}
			if res != nil {
				defer res.Body.Close()
			}

			b, err := io.ReadAll(res.Body)
			if err != nil {
				tt.Fatalf("expected no error from io.ReadAll. got %s", err)
			}

			if tc.expectedCode != res.StatusCode {
				tt.Fatalf("expected status code %d. got %d", tc.expectedCode, res.StatusCode)
			}
			if tc.expectedBody != string(b) {
				tt.Fatalf("expected body to be %s. got %s", tc.expectedBody, string(b))
			}
		})
	}
}

type responseWriterWithContext func(ctx context.Context, w http.ResponseWriter, err error)

func TestResponseWritersWithContext(t *testing.T) {
	tests := []struct {
		description    string
		responseWriter responseWriterWithContext
		expectedBody   string
		expectedCode   int
	}{
		{
			description:    "BadRequest",
			responseWriter: xhttp.WriteBadRequest,
			expectedBody:   `{"error":"an error"}`,
			expectedCode:   400,
		}, {
			description:    "InternalError",
			responseWriter: xhttp.WriteInternalError,
			expectedBody:   `{"error":"internal error"}`,
			expectedCode:   500,
		},
	}

	for _, tc := range tests {
		t.Run(tc.description, func(tt *testing.T) {
			testHandler := func() http.HandlerFunc {
				fn := func(w http.ResponseWriter, req *http.Request) {
					ctx := context.WithValue(req.Context(), "requestID", "foo")
					tc.responseWriter(ctx, w, fmt.Errorf("an error"))
				}
				return http.HandlerFunc(fn)
			}

			ts := httptest.NewServer(testHandler())
			defer ts.Close()

			res, err := http.Get(ts.URL)
			if err != nil {
				tt.Fatalf("expected no error from http.Get. got %s", err)
			}
			if res != nil {
				defer res.Body.Close()
			}

			b, err := io.ReadAll(res.Body)
			if err != nil {
				tt.Fatalf("expected no error from io.ReadAll. got %s", err)
			}

			if tc.expectedCode != res.StatusCode {
				tt.Fatalf("expected status code %d. got %d", tc.expectedCode, res.StatusCode)
			}
			if tc.expectedBody != string(b) {
				tt.Fatalf("expected body to be %s. got %s", tc.expectedBody, string(b))
			}
		})
	}
}
