// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package xhttp

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
)

func ReadJSON[T any](r *http.Request) (*T, error) {
	data, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	v := new(T)
	err = json.Unmarshal(data, v)
	if err != nil {
		return nil, err
	}
	return v, err
}

func WriteJSON[T any](ctx context.Context, w http.ResponseWriter, data T) {
	body, err := json.Marshal(data)
	if err != nil {
		WriteInternalError(ctx, w, err)
		return
	}
	w.Write(body)
	return
}
