//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package xhttp

import (
	"net/http"
)

// CorsHandler handles an OPTION request for CORS checks.
// The Access-Control-Allow-Origin header should be set by the middleware.
func CorsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Methods", "GET,POST,PUT,PATCH,DELETE,OPTIONS")
	w.Header().Add("Access-Control-Allow-Headers", "authorization, origin, content-type, accept, if-modified-since")
	w.Header().Add("Allow", "HEAD,GET,POST,PUT,PATCH,DELETE,OPTIONS")
	w.Header().Add("Content-Type", "application/json")
	w.Write([]byte{})
}

// CorsMiddleware returns a new http middleware. It currently allows all
// origins.
func CorsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Access-Control-Allow-Origin", "*")
		next.ServeHTTP(w, r)
	})
}
