//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package xhttp_test

import (
	"bytes"
	"context"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"eintopf.info/internal/xhttp"
)

func TestLogMiddleware(t *testing.T) {
	var buf bytes.Buffer
	log.SetOutput(&buf)
	defer func() {
		log.SetOutput(os.Stderr)
	}()

	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := xhttp.LogMiddleware(simpleTestHandler())
	handler.ServeHTTP(rr, req.WithContext(context.WithValue(context.Background(), "requestID", "foo")))

	if !strings.Contains(buf.String(), "method=GET") {
		t.Fatalf("invalid log: method not found: %s", buf.String())
	}
	if !strings.Contains(buf.String(), "status=200") {
		t.Fatalf("invalid log: status not found: %s", buf.String())
	}
	if !strings.Contains(buf.String(), "size=0") {
		t.Fatalf("invalid log: size not found: %s", buf.String())
	}
	if !strings.Contains(buf.String(), "requestID=foo") {
		t.Fatalf("invalid log: requestID not found: %s", buf.String())
	}
	if !strings.Contains(buf.String(), "url=/") {
		t.Fatalf("invalid log: url not found: %s", buf.String())
	}
}
