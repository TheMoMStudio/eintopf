// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package plausible

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"github.com/go-chi/chi/v5/middleware"
)

type Client struct {
	apiURL string
	domain string
}

func NewClient(host string, domain string) (*Client, error) {
	apiURL, err := url.JoinPath(host, "/api/event")
	if err != nil {
		return nil, fmt.Errorf("invalid plausible url: %s", err)
	}
	return &Client{apiURL: apiURL, domain: domain}, nil
}

// Event makes an api request to the plausible evnt api.
// See: https://plausible.io/docs/events-api
func (c *Client) Event(ctx context.Context, name string, url string, ipAddress string, userAgent string, referer string) error {
	req := eventRequest{
		Name:     name,
		URL:      url,
		Domain:   c.domain,
		Referrer: referer,
	}

	body, err := json.Marshal(req)
	if err == nil {
		hc := http.Client{}
		req, err := http.NewRequest("POST", c.apiURL, bytes.NewBuffer(body))
		if err == nil {
			req.Header.Set("User-Agent", userAgent)
			req.Header.Set("X-Forwarded-For", ipAddress)
			req.Header.Set("Content-Type", "application/json")
			hc.Do(req)
		}
	}
	return nil
}

type eventRequest struct {
	Name     string `json:"name"`
	URL      string `json:"url"`
	Domain   string `json:"domain"`
	Referrer string `json:"referrer"`
}

// Middleware sends an event request to the plausible server for every http request.
func Middleware(c *Client) func(next http.Handler) http.Handler {
	return MiddlewareWithName(c, "pageview")
}

// MiddlewareWithName sends an event request to the plausible server for every http request.
func MiddlewareWithName(c *Client, name string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ww := middleware.NewWrapResponseWriter(w, r.ProtoMajor)
			next.ServeHTTP(ww, r)
			// Do not track 404 responses
			if ww.Status() != http.StatusNotFound {
				TrackRequest(c, r, name)
			}
		})
	}
}

func TrackPageView(c *Client, r *http.Request) {
	TrackRequest(c, r, "pageview")
}

func TrackRequest(c *Client, r *http.Request, name string) {
	if c == nil {
		return
	}
	ipAddress := r.RemoteAddr
	lastColonAt := strings.LastIndex(r.RemoteAddr, ":")
	if lastColonAt != -1 {
		ipAddress = ipAddress[:lastColonAt]
	}
	c.Event(context.Background(), name, r.URL.RequestURI(), ipAddress, r.Header.Get("User-Agent"), r.Header.Get("Referer"))
}
