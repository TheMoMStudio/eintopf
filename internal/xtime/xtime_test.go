//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package xtime_test

import (
	"testing"
	"time"

	"eintopf.info/internal/xtime"
	"github.com/google/go-cmp/cmp"
)

func TestStartOfDay(t *testing.T) {
	berlinLocation, _ := time.LoadLocation("Europe/Berlin")

	tests := []struct {
		name       string
		date       time.Time
		startOfDay time.Time
	}{
		{
			name:       "UTC",
			date:       time.Date(2022, 2, 2, 13, 0, 1, 1, time.UTC),
			startOfDay: time.Date(2022, 2, 2, 0, 0, 0, 0, time.UTC),
		},
		{
			name:       "Europe/Berlin",
			date:       time.Date(2022, 2, 2, 13, 0, 1, 1, berlinLocation),
			startOfDay: time.Date(2022, 2, 2, 0, 0, 0, 0, berlinLocation),
		},
	}

	for _, test := range tests {
		if diff := cmp.Diff(test.startOfDay, xtime.StartOfDay(test.date)); diff != "" {
			t.Errorf("%s: date mismatch (-want +got):\n%s", test.name, diff)
		}
	}
}

func TestEndOfDay(t *testing.T) {
	berlinLocation, _ := time.LoadLocation("Europe/Berlin")

	tests := []struct {
		name       string
		date       time.Time
		startOfDay time.Time
	}{
		{
			name:       "UTC",
			date:       time.Date(2022, 2, 2, 13, 0, 1, 1, time.UTC),
			startOfDay: time.Date(2022, 2, 2, 23, 59, 59, 0, time.UTC),
		},
		{
			name:       "Europe/Berlin",
			date:       time.Date(2022, 2, 2, 13, 0, 1, 1, berlinLocation),
			startOfDay: time.Date(2022, 2, 2, 23, 59, 59, 0, berlinLocation),
		},
	}

	for _, test := range tests {
		if diff := cmp.Diff(test.startOfDay, xtime.EndOfDay(test.date)); diff != "" {
			t.Errorf("%s: date mismatch (-want +got):\n%s", test.name, diff)
		}
	}
}
