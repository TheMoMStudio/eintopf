//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

// package xerror defines a set of errors types, each indicating a category of errors.
// They are supposed to be used application wide.
package xerror

// NotFoundError indicates something was not found
type NotFoundError struct {
	Err  error
	Type string
}

// Error returns the underlying error as a string.
func (n NotFoundError) Error() string {
	return n.Err.Error()
}

// IsNotFoundError returns true if the error is of type NotFoundError.
func IsNotFoundError(err error) bool {
	if err == nil {
		return false
	}
	_, ok := err.(NotFoundError)
	return ok
}

// BadInputError indicates a wrong user input.
type BadInputError struct {
	Err error
}

// Error returns the underlying error as a string.
func (b BadInputError) Error() string {
	return b.Err.Error()
}

// IsBadInputError returns true if the error is of type BadInputError.
func IsBadInputError(err error) bool {
	if err == nil {
		return false
	}
	_, ok := err.(BadInputError)
	return ok
}

// BadInputError indicates a invalid authorization.
type AuthError struct {
	Err error
}

// Error returns the underlying error as a string.
func (b AuthError) Error() string {
	return b.Err.Error()
}

// IsAuthError returns true if the error is of type AuthError.
func IsAuthError(err error) bool {
	if err == nil {
		return false
	}
	_, ok := err.(AuthError)
	return ok
}
