//
// SPDX-FileCopyrightText: 2024 Klasse & Methode - IT Kollektiv Stuttgart <member@klasse-methode.it>
//
// SPDX-License-Identifier: AGPL-3.0-only

package xerror_test

import (
	"fmt"
	"testing"

	"eintopf.info/internal/xerror"
)

func TestIsBadInputError(t *testing.T) {
	tests := []struct {
		err             error
		isBadInputError bool
	}{
		{err: nil, isBadInputError: false},
		{err: fmt.Errorf("foo"), isBadInputError: false},
		{err: xerror.BadInputError{}, isBadInputError: true},
		{err: xerror.BadInputError{Err: fmt.Errorf("foo")}, isBadInputError: true},
	}

	for i, test := range tests {
		isBadInputError := xerror.IsBadInputError(test.err)
		if test.isBadInputError != isBadInputError {
			t.Errorf("%d: %s: %t", i, test.err, isBadInputError)
		}
	}
}

func TestIsAuthError(t *testing.T) {
	tests := []struct {
		err         error
		isAuthError bool
	}{
		{err: nil, isAuthError: false},
		{err: fmt.Errorf("foo"), isAuthError: false},
		{err: xerror.AuthError{}, isAuthError: true},
		{err: xerror.AuthError{Err: fmt.Errorf("foo")}, isAuthError: true},
	}

	for i, test := range tests {
		isAuthError := xerror.IsAuthError(test.err)
		if test.isAuthError != isAuthError {
			t.Errorf("%d: %s: %t", i, test.err, isAuthError)
		}
	}
}
